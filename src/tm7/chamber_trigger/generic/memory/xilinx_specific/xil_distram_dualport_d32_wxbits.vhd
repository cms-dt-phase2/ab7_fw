-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Memory components
---- Design unit: Single port RAM
---- File       : xil_distram_dualport_d32_wxbits.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: A distributed RAM
----              Depth => 32
----              Width => "NUM_BITS"
----              
----              This is a wrapper for Xilinx-specific distributed RAM macro.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
--
library UNISIM;
use UNISIM.vcomponents.all;

entity XIL_DISTRAM_DUALPORT_D32_WXBITS is
  generic (NUM_BITS : integer := 16);
  port (
    clk     : in  std_logic;
    wr_ena  : in  std_logic;
    add_wr  : in  std_logic_vector(4 downto 0);
    add_rd  : in  std_logic_vector(4 downto 0);
    data_in : in  std_logic_vector(NUM_BITS - 1 downto 0);
    data_out: out std_logic_vector(NUM_BITS - 1 downto 0)
  );
end entity;

architecture RTL of XIL_DISTRAM_DUALPORT_D32_WXBITS is
begin

  RAM_GEN : for i in 0 to NUM_BITS - 1 generate
    RAM32X1D_INST : RAM32X1D
    generic map (
        INIT => X"00000000") -- Initial contents of RAM
    port map (
        DPO   => data_out(i), -- Read-only 1-bit data output
        SPO   => open,        -- R/W 1-bit data output
        A0    => add_wr(0),   -- RAM address[0] input
        A1    => add_wr(1),   -- RAM address[1] input
        A2    => add_wr(2),   -- RAM address[2] input
        A3    => add_wr(3),   -- RAM address[3] input
        A4    => add_wr(4),   -- RAM address[4] input
        D     => data_in(i),  -- Write 1-bit data input
        DPRA0 => add_rd(0),   -- Read-only address[0] input bit
        DPRA1 => add_rd(1),   -- Read-only address[1] input bit
        DPRA2 => add_rd(2),   -- Read-only address[2] input bit
        DPRA3 => add_rd(3),   -- Read-only address[3] input bit
        DPRA4 => add_rd(4),   -- Read-only address[4] input bit
        WCLK  => clk,         -- Write clock input
        WE    => wr_ena       -- Write enable input
    );
  end generate;

end architecture;
