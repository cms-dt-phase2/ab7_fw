-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Multi-channel data mixer
---- File       : priority_enc_8_3.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-10
-------------------------------------------------------------------------------
---- Description: Priority encoder 8:3
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;
use work.mixer_pkg.all;

entity PRIORITY_ENC_8_3 is
  port (
    sel   : in  std_logic_vector(7 downto 0);
    valid : out std_logic;
    code  : out std_logic_vector(2 downto 0)
  );
  
  attribute priority_extract: string;
  attribute priority_extract of PRIORITY_ENC_8_3: entity is "force";
end entity;

architecture Behavioural of PRIORITY_ENC_8_3 is
begin

  code <= std_logic_vector(to_unsigned(0, 3)) when sel(0) = '1' else
          std_logic_vector(to_unsigned(1, 3)) when sel(1) = '1' else
          std_logic_vector(to_unsigned(2, 3)) when sel(2) = '1' else
          std_logic_vector(to_unsigned(3, 3)) when sel(3) = '1' else
          std_logic_vector(to_unsigned(4, 3)) when sel(4) = '1' else
          std_logic_vector(to_unsigned(5, 3)) when sel(5) = '1' else
          std_logic_vector(to_unsigned(6, 3)) when sel(6) = '1' else
          std_logic_vector(to_unsigned(7, 3)) when sel(7) = '1' else
          "---";
          
  valid <= or_bits_slv(sel);
  
end architecture;
