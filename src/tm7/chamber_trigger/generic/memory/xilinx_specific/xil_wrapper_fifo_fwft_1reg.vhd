-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Memory components
---- Design unit: Single port FIFO (FWFT type) with 1 extra register
---- File       : xil_wrapper_fifo_fwft_1reg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-02
-------------------------------------------------------------------------------
---- Description: A generic FIFO for two clock domains. Data output is ready
----              one cycle after enabling reading.
----              
----              This implements 1 extra reg on its frontend read port to 
----              allow a better clock time-fitting during the synthesizing 
----              stage.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
--
library UNISIM;
use UNISIM.vcomponents.all;
-- 
library UNIMACRO;
use UNIMACRO.vcomponents.all;
--
-- use work.commonfunc_pkg.all;
use work.xil_memory_pkg.all;

entity XIL_WRAPPER_FIFO_FWFT_1REG is
  generic (
    -- Sets almost full threshold
    ALMOST_FULL_OFFSET      : bit_vector(15 downto 0):= X"0080";
    -- Target BRAM, "18Kb" or "36Kb" 
    FIFO_SIZE               : string  := "18Kb";
    -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
    DATAWIDTH               : integer := 36;  -- DATAWIDTH units -> bits
    DEPTH                   : integer := 512  -- DEPTH units -> num. words
  );
  port (
    -- Clock and resets
    rst         : in  std_logic;
    rdClk       : in  std_logic;
    wrClk       : in  std_logic;
    -- Control and data signals
    rdEna       : in  std_logic;
    wrEna       : in  std_logic;
    dataIn      : in  std_logic_vector(DATAWIDTH - 1 downto 0);
    dataOut     : out std_logic_vector(DATAWIDTH - 1 downto 0);
    almost_full : out std_logic;
    empty       : out std_logic;
    full        : out std_logic
  );
end entity;

architecture RTL of XIL_WRAPPER_FIFO_FWFT_1REG is
  signal pending_data : std_logic := '0';
  signal regEmpty     : std_logic := '0';
  signal regDataOut   : std_logic_vector(DATAWIDTH - 1 downto 0);
  --
  signal int_empty    : std_logic := '0';
  signal int_readEna  : std_logic := '0';
  signal int_dataOut  : std_logic_vector(DATAWIDTH - 1 downto 0);

begin
  dataOut <= regDataOut;
  empty   <= regEmpty;
  
  int_readEna <= ((not int_empty) and (not pending_data)) or
                 (rdEna and pending_data and (not int_empty));

  process(rdClk, rst)
  begin
    if rdClk'event and rdClk = '1' then
      if rst = '1' 
      then pending_data <= '0';
      else pending_data <= not int_empty;
      end if;
    
      if int_readEna = '1' or rdEna = '1' then
        regEmpty   <= int_empty;
        regDataOut <= int_dataOut;
      end if;
    end if;
  end process;

  FIFO : XIL_FIFO_BUILTIN_V7 
  generic map (
    ALMOST_FULL_OFFSET      => ALMOST_FULL_OFFSET,
    FIFO_SIZE               => FIFO_SIZE,
    DATAWIDTH               => DATAWIDTH,
    DEPTH                   => DEPTH,
    FIRST_WORD_FALL_THROUGH => TRUE
  )
  port map (
    rst          => rst,
    rdClk        => rdClk,
    wrClk        => wrClk,
    rdEna        => int_readEna,
    wrEna        => wrEna,
    dataIn       => dataIn,
    dataOut      => int_dataOut,
    almost_empty => open,
    almost_full  => almost_full,
    empty        => int_empty,
    full         => full   
  );

end architecture;
