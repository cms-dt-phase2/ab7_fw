----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_streamer - rtl
-- Description: 
-- 
--       Receives the data from the histogram, appends the header, masks and and streams the package
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- bril
use work.bril_histogram_package.all;

entity bril_histogram_streamer_sync is
Generic (
    -- identification of the histogram
    HISTOGRAM_TYPE              : natural;
    HISTOGRAM_ID                : natural;
    -- memory parameters
    MEMORY_BLOCK_PARAMETERS     : memory_block_parameters_type;
    -- histogram parameters
    NUMBER_OF_BINS              : natural;
    COUNTER_WIDTH               : natural;
    INCREMENT_WIDTH             : natural;
    -- error block parameters
    NUMBER_OF_UNITS             : natural;
    NUMBER_OF_WORDS_PER_ERROR   : natural
);
Port ( 
    -- basic
    reset_i                 : in std_logic;
    clk_i                   : in std_logic;
    -- input from the core
    iteration_counter_i     : in natural;
    increment_overflow_i    : in std_logic;
    overflow_i              : in std_logic;
    data_i                  : in std_logic_vector(31 downto 0);
    data_valid_i            : in std_logic;
    -- tcds signals
    new_histogram_i         : in std_logic;
    new_histogram_reset_i   : in std_logic;
    -- tcds data
    lhc_fill_i              : in std_logic_vector(31 downto 0);
    cms_run_i               : in std_logic_vector(31 downto 0);
    lumi_section_i          : in std_logic_vector(31 downto 0);
    lumi_nibble_i           : in std_logic_vector(31 downto 0);
    -- error block interface
    err_bl_n_msk_err_words_i    : in std_logic_vector(15 downto 0) := (others => '0');
    err_bl_err_word_i           : in std_logic_vector(31 downto 0) := (others => '0');
    err_bl_rd_en_o              : out std_logic;
    -- streaming output
    data_o                  : out std_logic_vector(31 downto 0);
    data_valid_o            : out std_logic
);
end bril_histogram_streamer_sync;

architecture rtl of bril_histogram_streamer_sync is

    -- state machine
    type streamer_fsm_state_type is (Idle, StreamingCounters, StreamingMaskAndErrors, DummyReadErrors);
    signal streamer_fsm_state       : streamer_fsm_state_type := Idle;

    -- delaying counter data
    signal circular_buffer          : register_block_32bit_type(MEMORY_BLOCK_PARAMETERS.header_size - 1 downto 0) := (others => (others => '0'));
    signal data_to_send_counter     : natural range 0 to MEMORY_BLOCK_PARAMETERS.header_size;
    signal data_wr_pointer          : natural range 0 to MEMORY_BLOCK_PARAMETERS.header_size-1; 
    signal data_sent_pointer        : natural range 0 to MEMORY_BLOCK_PARAMETERS.header_size-1; 
    signal data_sent_counter        : natural range 0 to MEMORY_BLOCK_PARAMETERS.total_package_size;

begin

    -- an independent state machine to prepend the header and delay the bram output by HEADER_SIZE clock cycles
    -- !!! IF YOU CHANGE THE HEADER FORMAT, DON'T forget to change the header size in the package !!!
    data_out_process: process(clk_i)
    begin
        if rising_edge(clk_i) then
            if reset_i = '1' then
                -- fsm
                streamer_fsm_state <= Idle;
                -- internal
                circular_buffer <= (others => (others => '0'));
                data_to_send_counter <= 0;
                data_wr_pointer <= 0;
                data_sent_pointer <= 0;
                data_sent_counter <= 0;
                -- output 
                data_o <= (others => '0');
                data_valid_o <= '0';
                -- errors
                err_bl_rd_en_o <= '0';
            else
                -- reset the output everytime
                data_o <= (others => '0');
                data_valid_o <= '0';  
                err_bl_rd_en_o <= '0';
                
                -- state machine
                case streamer_fsm_state is
                    when Idle =>
                        -- means that the fifo is not empty and we have to discard the new histogram and perform dummy read for errors
                        if new_histogram_reset_i = '1' then
                            if err_bl_n_msk_err_words_i > 0 then
                                -- reset counter
                                data_sent_counter <= 0;
                                -- now stream errors
                                streamer_fsm_state <= DummyReadErrors;
                                -- assert rd enable already now - fifo has a delay of 1 clock cycle to output the next word
                                err_bl_rd_en_o <= '1';
                            end if; 
                        else
                            -- this condition allows to buffer some signals already now and avoid allocating separate registers for them
                            if new_histogram_i = '1' then
                                circular_buffer(0) <= std_logic_vector(to_unsigned(HISTOGRAM_TYPE, 6)) & std_logic_vector(to_unsigned(HISTOGRAM_ID, 10)) & std_logic_vector(to_unsigned(NUMBER_OF_BINS, 16)); -- second word of the header, first in the que
                                circular_buffer(1) <= std_logic_vector(to_unsigned(COUNTER_WIDTH, 8)) & std_logic_vector(to_unsigned(INCREMENT_WIDTH, 8)) & std_logic_vector(to_unsigned(MEMORY_BLOCK_PARAMETERS.total_counter_words, 16));
                                circular_buffer(2) <= overflow_i & increment_overflow_i & "0000000000" & std_logic_vector(to_unsigned(iteration_counter_i+1,20)); -- counter of the iterations - has to be +1 as it was not incremented yet
                                circular_buffer(3) <= lhc_fill_i;
                                circular_buffer(4) <= cms_run_i;
                                circular_buffer(5) <= lumi_section_i;
                                circular_buffer(6) <= lumi_nibble_i;
                                circular_buffer(7) <= std_logic_vector(to_unsigned(NUMBER_OF_WORDS_PER_ERROR, 8)) & std_logic_vector(to_unsigned(NUMBER_OF_UNITS, 8)) & err_bl_n_msk_err_words_i;
                            end if;
                            -- time between new histogram and data valid is 2 clock cycles
                            if data_valid_i = '1' then -- wohoo we received first word from the memory
                                -- put first word of the header in the read-out
                                data_o <= x"FF" & std_logic_vector(to_unsigned(MEMORY_BLOCK_PARAMETERS.header_size, 8)) & std_logic_vector(to_unsigned(MEMORY_BLOCK_PARAMETERS.header_size+MEMORY_BLOCK_PARAMETERS.total_counter_words+to_integer(unsigned(err_bl_n_msk_err_words_i)), 16)); -- 12 bits for xFFF pattern, 8 bits for theheader size and 16 bits for total size of the data
                                data_valid_o <= '1'; 
                                circular_buffer(MEMORY_BLOCK_PARAMETERS.HEADER_SIZE-1) <= data_i; -- and here is the data from the bram, delayed:)
                                -- set the amount of words in the buffer
                                data_to_send_counter <= MEMORY_BLOCK_PARAMETERS.header_size; -- total amount of data in the buffer
                                data_wr_pointer <= 0; -- write pointer (next word to overwrite)
                                data_sent_pointer <= 0; -- read pointer (next word to read)
                                data_sent_counter <= 1; -- total counter - already includes header word 0
                                -- and of course start streaming
                                streamer_fsm_state <= StreamingCounters;                      
                            end if;
                        end if;
                    when StreamingCounters =>                        
                        -- check that we have not sent all the words
                        if data_sent_counter = MEMORY_BLOCK_PARAMETERS.header_size+MEMORY_BLOCK_PARAMETERS.total_counter_words then
                            -- reset the counters and data wr (not really needed)
                            data_o <= (others => '0');
                            data_valid_o <= '0';  
                            data_to_send_counter <= 0;
                            data_wr_pointer <= 0; 
                            data_sent_pointer <= 0; 
                            data_sent_counter <= 0; -- will be used to sent mask and errors
                            -- now check if there are some errors
                            if err_bl_n_msk_err_words_i > 0 then
                                -- now stream errors
                                streamer_fsm_state <= StreamingMaskAndErrors;
                                -- assert rd enable already now - fifo has a delay of 1 clock cycle to output the next word
                                err_bl_rd_en_o <= '1';
                            else 
                                -- do nothing
                                streamer_fsm_state <= Idle;
                            end if;
                            
                        -- otherwise if we still expect some data 
                        else
                            -- all this magic will only work if the rate(sending) >= rate(writing), which is always the case
                            -- send when available
                            if (data_to_send_counter > 0) then
                                -- send the data
                                data_o <= circular_buffer(data_sent_pointer);
                                data_sent_counter <= data_sent_counter + 1;
                                data_valid_o <= '1';
                                -- increment the pointer of course
                                if data_sent_pointer < MEMORY_BLOCK_PARAMETERS.header_size-1 then
                                    data_sent_pointer <= data_sent_pointer + 1;
                                else
                                    data_sent_pointer <= 0;
                                end if;
                            end if;
                            
                            -- appending new data
                            if data_valid_i = '1' then
                                circular_buffer(data_wr_pointer) <= data_i;
                                 -- increment the pointer of course
                                if data_wr_pointer < MEMORY_BLOCK_PARAMETERS.header_size-1 then
                                    data_wr_pointer <= data_wr_pointer + 1;
                                else
                                    data_wr_pointer <= 0;
                                end if;
                            end if;
                        
                            -- and finally magic with the data to send counter
                            if (data_to_send_counter > 0) and (data_valid_i) = '1' then -- means that the data was both written and stored - counter remains the same
                                data_to_send_counter <= data_to_send_counter;
                            else
                                if (data_valid_i = '1') then -- only write was performed
                                    data_to_send_counter <= data_to_send_counter + 1;
                                elsif (data_to_send_counter > 0) then -- only read was performed
                                    data_to_send_counter <= data_to_send_counter - 1;
                                end if;
                            end if;
                        end if;

                       
                    when StreamingMaskAndErrors =>
                        -- stream error words ( the fifo is fall through, so the data is ready)
                        data_o <= err_bl_err_word_i;
                        data_valid_o <= '1';
                        -- check what to do next
                        if data_sent_counter < err_bl_n_msk_err_words_i-1 then -- -1 because one word we already read
                            -- increment counter
                            data_sent_counter <= data_sent_counter + 1;
                            -- request new word
                            err_bl_rd_en_o <= '1';
                        else
                            -- reset counter
                            data_sent_counter <= 0;
                            -- done with new words
                            err_bl_rd_en_o <= '0';
                            -- and go back to idle
                            streamer_fsm_state <= Idle;
                        end if;
                        
                        
                    when DummyReadErrors =>
                        -- dummy errors read so no write here ( the fifo is fall through, so the data is ready)
                        if data_sent_counter < err_bl_n_msk_err_words_i-1 then -- -1 because one word we already read
                            -- increment counter
                            data_sent_counter <= data_sent_counter + 1;
                            -- request new word
                            err_bl_rd_en_o <= '1';
                        else
                            -- reset counter
                            data_sent_counter <= 0;
                            -- done with new words
                            err_bl_rd_en_o <= '0';
                            -- and go back to idle
                            streamer_fsm_state <= Idle;
                        end if;
                        
                        
                    when others =>
                        streamer_fsm_state <= Idle;
                end case;
            end if;
        end if;
    end process;

end rtl;
