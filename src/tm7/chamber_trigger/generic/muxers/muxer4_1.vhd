-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Multiplexers components
---- Design unit: 4:1 bits multiplexer
---- File       : muxer4_1.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: Multiplexer 4:1 bit, implemented as a building block to
----              create bigger muxers optimized for speed.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;

entity MUXER4_1 is
  port (
    sel   : in  std_logic_vector(1 downto 0);
    d_in  : in  std_logic_vector(3 downto 0);
    d_out : out std_logic
  );
end entity;

architecture Behavioural of MUXER4_1 is
begin
  process(sel, d_in)
  begin
    case sel is
      when "00"   => d_out <= d_in(0);
      when "01"   => d_out <= d_in(1);
      when "10"   => d_out <= d_in(2);
      when "11"   => d_out <= d_in(3);
      when others => d_out <= 'X';
    end case;
  end process;
end architecture;
