numlinks = 12


from pprint import pprint
regs = '''
  conf_link1_sc_enable       <= conf(16#08#)(0);
  conf_link1_tx_trid         <= conf(16#09#)(7 downto 0);
  conf_link1_tx_ch           <= conf(16#09#)(15 downto 8);
  conf_link1_tx_comm         <= conf(16#09#)(31 downto 24);
  conf_link1_ec_data         <= conf(16#0A#)(31 downto 0);
  conf_link1_send_reset      <= conf(16#0B#)(0);
  conf_link1_send_connect    <= conf(16#0B#)(1);
  conf_link1_reset_sc        <= conf(16#0B#)(2);
  conf_link1_gbtx_addr       <= conf(16#0C#)(7 downto 0);
  conf_link1_ic_rd_wr        <= conf(16#0C#)(8);
  conf_link1_read_fifo       <= conf(16#0C#)(9);
  conf_link1_nb_to_read      <= conf(16#0C#)(31 downto 16);
  conf_link1_reg_addr        <= conf(16#0D#)(15 downto 0);
  conf_link1_ic_data         <= conf(16#0E#)(7 downto 0);
'''

regs = [reg.strip() for reg in regs.split('\n') if reg.strip()]
regs = [reg.split(' ')[0] for reg in regs]
regs = [reg.split('_',2)[2] for reg in regs]

maxlen = max([len(reg) for reg in regs])

txt = ''

for reg in regs:
    txt +=  reg + (' '*(1 + (maxlen - len(reg))) ) + '<= ( '
    for link in range(1, numlinks + 1):
        txt += 'conf_link%i_'%link + reg
        txt += ' '*( 1 + (1 if link<10 and numlinks>9 else 0) + (maxlen - len(reg)) )
        txt += ', '
    txt = txt[:-2]
    txt += ' );\n'

print txt

