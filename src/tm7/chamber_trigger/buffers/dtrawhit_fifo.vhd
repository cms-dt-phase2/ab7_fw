-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Buffers
---- File       : dtrawhit_fifo.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-03
-------------------------------------------------------------------------------
---- Description: FIFO wrapper to store DT Raw Hits, using a common interface.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
--
use work.dt_common_pkg.all;
use work.dt_buffers_pkg.all;
use work.xil_memory_pkg.all;

entity DTRAWHIT_FIFO is
  port (
    clkRd   : in  std_logic;
    clkWr   : in  std_logic;
    rst     : in  std_logic;
    fifoIn  : in  DTRawHitFifoIn_t;
    fifoOut : out DTRawHitFifoOut_t
  );
end entity;

architecture Behavioural of DTRAWHIT_FIFO is
  signal dOut : std_logic_vector(DTRAWHIT_SIZE - 1 downto 0);
  signal dIn  : std_logic_vector(DTRAWHIT_SIZE - 1 downto 0);

  signal empty        : std_logic := '1';
  signal full         : std_logic := '0';
  signal almost_full  : std_logic := '0';
  signal statCountIn  : std_logic_vector(STAT_COUNT_LEN - 1 downto 0);
  signal statCountOut : std_logic_vector(STAT_COUNT_LEN - 1 downto 0);
  
  signal bxTimeConvOut : BXTimeConvOut_t;
begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
--   fifoOut.full        <= full;
  fifoOut.full        <= bxTimeConvOut.full;
  fifoOut.empty       <= empty;
  fifoOut.rawHit      <= dtRawHit_from_slv(dOut);
  --
  fifoOut.stat.count_in  <= statCountIn;
  fifoOut.stat.count_out <= statCountOut;

  ---------------------------------------------------------------
  -- Auxiliary signals
  --
--   dIn <= slv_from_dtRawHit(fifoIn.rawHit);
  dIn <= slv_from_dtRawHit(bxTimeConvOut.rawHit);

  ---------------------------------------------------------------
  -- Counters for statistics
  --
  RD_STAT : process(clkRd, rst, fifoIn)
  begin
    if clkRd'event and clkRd = '1' then
      if rst = '1' then 
        statCountOut <= (others => '0');
      elsif fifoIn.rdEna = '1' then
        statCountOut <= statCountOut  + '1';
      end if;
    end if;
  end process;

  WR_STAT : process(clkWr, rst, fifoIn)
  begin
    if clkWr'event and clkWr = '1' then
      if rst = '1' then 
        statCountIn <= (others => '0');
      elsif fifoIn.wrEna = '1' then
        statCountIn <= statCountIn  + '1';
      end if;
    end if;
  end process;
  
  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  BX_CONV : BX_TIME_CONVERTER port map(
    clk          => clkWr,
    rst          => rst,
    bxTimeConvIn => (
          rawHit               => fifoIn.rawHit,
          wrEna                => fifoIn.wrEna,
          rawHitsFF_full       => full,
          rawHitsFFalmost_full => almost_full,
          bx_time_convert_ena  => fifoIn.bx_time_convert_ena
    ),
    bxTimeConvOut => bxTimeConvOut
  );

  INTERNAL_FIFO : XIL_FIFO_GENERIC_WRAPPER 
  generic map (ALMOST_FULL_OFFSET => X"0020", -- 992
               DATAWIDTH          => DTRAWHIT_SIZE, 
               DEPTH              => 1024)
  port map (
    rst          => rst,
    rdClk        => clkRd,
    wrClk        => clkWr,
    rdEna        => fifoIn.rdEna,
--     wrEna        => fifoIn.wrEna,
    wrEna        => bxTimeConvOut.rawHitsFF_wr, 
    data_in      => dIn,
    data_out     => dOut,
    rd_count     => open,
    wr_count     => open,
    almost_empty => open,
    almost_full  => almost_full,
    empty        => empty,
    full         => full
  );
  
end architecture;
