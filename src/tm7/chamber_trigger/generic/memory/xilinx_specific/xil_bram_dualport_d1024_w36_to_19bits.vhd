-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Memory components
---- Design unit: Single port RAM
---- File       : xil_bram_dualport_d1024_w36_to_19bits.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-04
-------------------------------------------------------------------------------
---- Description: A Block RAM wrapper
----              Depth => 1024
----              Width => From 19 to 36 bits
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
--
library UNISIM;
use UNISIM.vcomponents.all;
--
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity XIL_BRAM_DUALPORT_D1024_W36_TO_19BITS is
  generic (
    DATA_WIDTH  : integer := 36; -- From 19 to 36
    ENA_REG_OUT : integer := 0   -- Enable => 1 / Disable => 0
  ); 
  port (
    rst     : in  std_logic;
    clk_wr  : in  std_logic;
    ena_wr  : in  std_logic;
    add_wr  : in  std_logic_vector(9 downto 0);
    data_in : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
    --
    clk_rd  : in  std_logic;
    ena_rd  : in  std_logic;
    add_rd  : in  std_logic_vector(9 downto 0);
    data_out: out std_logic_vector(DATA_WIDTH - 1 downto 0)
  );
end entity;

architecture RTL of XIL_BRAM_DUALPORT_D1024_W36_TO_19BITS is
  constant INIT_VALUE : bit_vector(255 downto 0) := 
        X"0000000000000000000000000000000000000000000000000000000000000000";
begin
  -----------------------------------------------------------------------
  --  READ_WIDTH | BRAM_SIZE | READ Depth  | RDADDR Width |            --
  -- WRITE_WIDTH |           | WRITE Depth | WRADDR Width |  WE Width  --
  -- ============|===========|=============|==============|============--
  --    37-72    |  "36Kb"   |      512    |     9-bit    |    8-bit   --
  --    19-36    |  "36Kb"   |     1024    |    10-bit    |    4-bit   --
  --    19-36    |  "18Kb"   |      512    |     9-bit    |    4-bit   --
  --    10-18    |  "36Kb"   |     2048    |    11-bit    |    2-bit   --
  --    10-18    |  "18Kb"   |     1024    |    10-bit    |    2-bit   --
  --     5-9     |  "36Kb"   |     4096    |    12-bit    |    1-bit   --
  --     5-9     |  "18Kb"   |     2048    |    11-bit    |    1-bit   --
  --     3-4     |  "36Kb"   |     8192    |    13-bit    |    1-bit   --
  --     3-4     |  "18Kb"   |     4096    |    12-bit    |    1-bit   --
  --       2     |  "36Kb"   |    16384    |    14-bit    |    1-bit   --
  --       2     |  "18Kb"   |     8192    |    13-bit    |    1-bit   --
  --       1     |  "36Kb"   |    32768    |    15-bit    |    1-bit   --
  --       1     |  "18Kb"   |    16384    |    14-bit    |    1-bit   --
  -----------------------------------------------------------------------

  BRAM_SDP_MACRO_INST : BRAM_SDP_MACRO
  generic map (
    BRAM_SIZE   => "36Kb",      -- Target BRAM, "18Kb" or "36Kb" 
    DEVICE      => "7SERIES",   -- "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
    WRITE_WIDTH => DATA_WIDTH,
    READ_WIDTH  => DATA_WIDTH,
    DO_REG      => ENA_REG_OUT, -- Optional output register (0 or 1)
    INIT_FILE   => "NONE",
    --
    SIM_COLLISION_CHECK => "ALL",
    --
    SRVAL       => X"000000000000000000",--  Set/Reset value for port output
    WRITE_MODE  => "WRITE_FIRST",        -- Specify "READ_FIRST" / "WRITE_FIRST"
    INIT        => X"000000000000000000",--  Initial values on output port
    --
    -- The following INIT_xx declarations => initial contents of the RAM
    INIT_00 => INIT_VALUE,
    INIT_01 => INIT_VALUE,
    INIT_02 => INIT_VALUE,
    INIT_03 => INIT_VALUE,
    INIT_04 => INIT_VALUE,
    INIT_05 => INIT_VALUE,
    INIT_06 => INIT_VALUE,
    INIT_07 => INIT_VALUE,
    INIT_08 => INIT_VALUE,
    INIT_09 => INIT_VALUE,
    INIT_0A => INIT_VALUE,
    INIT_0B => INIT_VALUE,
    INIT_0C => INIT_VALUE,
    INIT_0D => INIT_VALUE,
    INIT_0E => INIT_VALUE,
    INIT_0F => INIT_VALUE,
    INIT_10 => INIT_VALUE,
    INIT_11 => INIT_VALUE,
    INIT_12 => INIT_VALUE,
    INIT_13 => INIT_VALUE,
    INIT_14 => INIT_VALUE,
    INIT_15 => INIT_VALUE,
    INIT_16 => INIT_VALUE,
    INIT_17 => INIT_VALUE,
    INIT_18 => INIT_VALUE,
    INIT_19 => INIT_VALUE,
    INIT_1A => INIT_VALUE,
    INIT_1B => INIT_VALUE,
    INIT_1C => INIT_VALUE,
    INIT_1D => INIT_VALUE,
    INIT_1E => INIT_VALUE,
    INIT_1F => INIT_VALUE,
    INIT_20 => INIT_VALUE,
    INIT_21 => INIT_VALUE,
    INIT_22 => INIT_VALUE,
    INIT_23 => INIT_VALUE,
    INIT_24 => INIT_VALUE,
    INIT_25 => INIT_VALUE,
    INIT_26 => INIT_VALUE,
    INIT_27 => INIT_VALUE,
    INIT_28 => INIT_VALUE,
    INIT_29 => INIT_VALUE,
    INIT_2A => INIT_VALUE,
    INIT_2B => INIT_VALUE,
    INIT_2C => INIT_VALUE,
    INIT_2D => INIT_VALUE,
    INIT_2E => INIT_VALUE,
    INIT_2F => INIT_VALUE,
    INIT_30 => INIT_VALUE,
    INIT_31 => INIT_VALUE,
    INIT_32 => INIT_VALUE,
    INIT_33 => INIT_VALUE,
    INIT_34 => INIT_VALUE,
    INIT_35 => INIT_VALUE,
    INIT_36 => INIT_VALUE,
    INIT_37 => INIT_VALUE,
    INIT_38 => INIT_VALUE,
    INIT_39 => INIT_VALUE,
    INIT_3A => INIT_VALUE,
    INIT_3B => INIT_VALUE,
    INIT_3C => INIT_VALUE,
    INIT_3D => INIT_VALUE,
    INIT_3E => INIT_VALUE,
    INIT_3F => INIT_VALUE,
    
    -- The next set of INIT_xx are valid when configured as 36Kb
    INIT_40 => INIT_VALUE,
    INIT_41 => INIT_VALUE,
    INIT_42 => INIT_VALUE,
    INIT_43 => INIT_VALUE,
    INIT_44 => INIT_VALUE,
    INIT_45 => INIT_VALUE,
    INIT_46 => INIT_VALUE,
    INIT_47 => INIT_VALUE,
    INIT_48 => INIT_VALUE,
    INIT_49 => INIT_VALUE,
    INIT_4A => INIT_VALUE,
    INIT_4B => INIT_VALUE,
    INIT_4C => INIT_VALUE,
    INIT_4D => INIT_VALUE,
    INIT_4E => INIT_VALUE,
    INIT_4F => INIT_VALUE,
    INIT_50 => INIT_VALUE,
    INIT_51 => INIT_VALUE,
    INIT_52 => INIT_VALUE,
    INIT_53 => INIT_VALUE,
    INIT_54 => INIT_VALUE,
    INIT_55 => INIT_VALUE,
    INIT_56 => INIT_VALUE,
    INIT_57 => INIT_VALUE,
    INIT_58 => INIT_VALUE,
    INIT_59 => INIT_VALUE,
    INIT_5A => INIT_VALUE,
    INIT_5B => INIT_VALUE,
    INIT_5C => INIT_VALUE,
    INIT_5D => INIT_VALUE,
    INIT_5E => INIT_VALUE,
    INIT_5F => INIT_VALUE,
    INIT_60 => INIT_VALUE,
    INIT_61 => INIT_VALUE,
    INIT_62 => INIT_VALUE,
    INIT_63 => INIT_VALUE,
    INIT_64 => INIT_VALUE,
    INIT_65 => INIT_VALUE,
    INIT_66 => INIT_VALUE,
    INIT_67 => INIT_VALUE,
    INIT_68 => INIT_VALUE,
    INIT_69 => INIT_VALUE,
    INIT_6A => INIT_VALUE,
    INIT_6B => INIT_VALUE,
    INIT_6C => INIT_VALUE,
    INIT_6D => INIT_VALUE,
    INIT_6E => INIT_VALUE,
    INIT_6F => INIT_VALUE,
    INIT_70 => INIT_VALUE,
    INIT_71 => INIT_VALUE,
    INIT_72 => INIT_VALUE,
    INIT_73 => INIT_VALUE,
    INIT_74 => INIT_VALUE,
    INIT_75 => INIT_VALUE,
    INIT_76 => INIT_VALUE,
    INIT_77 => INIT_VALUE,
    INIT_78 => INIT_VALUE,
    INIT_79 => INIT_VALUE,
    INIT_7A => INIT_VALUE,
    INIT_7B => INIT_VALUE,
    INIT_7C => INIT_VALUE,
    INIT_7D => INIT_VALUE,
    INIT_7E => INIT_VALUE,
    INIT_7F => INIT_VALUE,
    
    -- The next set of INITP_xx are for the parity bits
    INITP_00 => INIT_VALUE,
    INITP_01 => INIT_VALUE,
    INITP_02 => INIT_VALUE,
    INITP_03 => INIT_VALUE,
    INITP_04 => INIT_VALUE,
    INITP_05 => INIT_VALUE,
    INITP_06 => INIT_VALUE,
    INITP_07 => INIT_VALUE,
    
    -- The next set of INIT_xx are valid when configured as 36Kb
    INITP_08 => INIT_VALUE,
    INITP_09 => INIT_VALUE,
    INITP_0A => INIT_VALUE,
    INITP_0B => INIT_VALUE,
    INITP_0C => INIT_VALUE,
    INITP_0D => INIT_VALUE,
    INITP_0E => INIT_VALUE,
    INITP_0F => INIT_VALUE)
  port map (
    DO     => data_out, -- Output read data port
    DI     => data_in,  -- Input write data port
    RDADDR => add_rd,   -- Input read address, width defined by read port depth
    RDCLK  => clk_rd,   -- 1-bit input read clock
    RDEN   => ena_rd,   -- 1-bit input read port enable
    REGCE  => '1',      -- 1-bit input read output register enable
    RST    => rst,      -- 1-bit input reset 
    -- Input write enable, width defined by write port depth
    WE     => "1111",
    WRADDR => add_wr,   -- Input write address
    WRCLK  => clk_wr,   -- 1-bit input write clock
    WREN   => ena_wr    -- 1-bit input write port enable
  );

end architecture;
