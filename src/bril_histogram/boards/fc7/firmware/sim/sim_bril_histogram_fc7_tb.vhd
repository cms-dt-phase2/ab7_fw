----------------------------------------------------------------------------------
-- Company: CERN, BRIL
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: sim_bril_histogram_fc7_tb - Behavioral
-- Description: 
-- 
--        Just a simple test to check the test bench
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

library work;
use work.bril_histogram_package.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sim_bril_histogram_fc7_tb is
--  Port ( );
end sim_bril_histogram_fc7_tb;

architecture Behavioral of sim_bril_histogram_fc7_tb is

    constant clk_fast_period        : time := 3.125 ns;
    constant clk_ipb_period         : time := 32.0 ns;
    signal reset                    : std_logic;
    signal reset_prev               : std_logic;
    signal clk_fast                 : std_logic;
    signal clk_ipb                  : std_logic;

    signal injection_data                   : std_logic_vector(31 downto 0);
    signal injection_reset_pointer          : std_logic;
    signal injection_wr_en                  : std_logic;
    signal injection_norbits_available      : natural := 1;
    signal injection_norbits_tocollect      : natural := 2;
    signal injection_start                  : std_logic;
    signal injection_stop                   : std_logic := '0';
    
    -- memory init counter
    signal memory_init_counter              : natural;
    signal do_init_memory                   : std_logic;
    signal started                          : std_logic;
    
    --
    signal bril_fifo_rd_en                  : std_logic;
    signal bril_fifo_empty                  : std_logic;
    signal clk_parity                       : std_logic;
            
begin

    -- clock
    process
    begin
        clk_fast <= '1';
        wait for clk_fast_period/2;
        clk_fast <= '0';
        wait for clk_fast_period/2;
    end process;
    
    process
    begin
        clk_ipb <= '1';
        wait for clk_ipb_period/2;
        clk_ipb <= '0';
        wait for clk_ipb_period/2;
    end process;
    
    -- in the beginning init memory a bit 
    process(clk_ipb)
    begin
        if rising_edge(clk_ipb) then
            -- always reset
            injection_wr_en <= '0';
            injection_start <= '0';
            injection_data <= (others => '0');
            injection_reset_pointer <= '0';
            -- normal runnign
            if reset = '1' then
                memory_init_counter <= 0;
                do_init_memory <= '0';
                started <= '0';
                injection_reset_pointer <= '1';
            elsif reset_prev = '1' and reset = '0' then
                do_init_memory <= '1';
            elsif do_init_memory = '1' and memory_init_counter < 64 then
                -- just a dummy condition
                if true then
                    if (memory_init_counter/8 = 0 or memory_init_counter/8 = 2 or memory_init_counter/8 = 29) then -- at 0
                        -- word 0
                        if (memory_init_counter rem 8) = 0 then
                            injection_data(2 downto 0)  <= std_logic_vector(to_unsigned(1,3));
                            injection_data(3) <= '0';
                        end if;
                    end if;
                    if (memory_init_counter/8) = 2 then
                        -- word 0
                        if (memory_init_counter rem 8) = 0 then
                            injection_data(6 downto 4)  <= std_logic_vector(to_unsigned(3,3));
                            injection_data(7) <= '0';
                        end if;
                    end if;
                    if (memory_init_counter/8) = 2 or (memory_init_counter/8) = 3 or (memory_init_counter/8) = 4 or (memory_init_counter/8) = 5 or (memory_init_counter/8) = 6 then
                        -- word 0
                        if (memory_init_counter rem 8) = 1 then
                            injection_data(6 downto 4)  <= std_logic_vector(to_unsigned(3,3));
                            injection_data(7) <= '1';
                        end if;
                    end if;
                else
                    if memory_init_counter/8 = 0 or memory_init_counter/8 = 2 then
                        if (memory_init_counter rem 8) = 0 then
                            injection_data(2 downto 0)  <= std_logic_vector(to_unsigned(1,3));
                            injection_data(3) <= '0';
                            injection_data(6 downto 4)  <= std_logic_vector(to_unsigned(2,3));
                            injection_data(7) <= '0';
                        end if;
                    end if;
                end if;
                injection_wr_en <= '1';
                memory_init_counter <= memory_init_counter + 1;
            elsif started = '0' then
                do_init_memory <= '0';
                started <= '1';
                injection_start <= '1';
            end if;
            -- store
            reset_prev <= reset;
        end if;   
    end process;
    
    -- stimulius
    process
    begin   
        wait for 100 ps;
        reset <= '1';
        wait for 8*25ns;
        reset <= '0';
        wait for 200000000ns;
    end process;
    
    -- tb itself
    bril_histogram_fc7_injector_inst: entity work.bril_histogram_fc7_injector
    port map ( 
        -- commong
        reset_i                             => reset,
        clk_i                               => clk_fast,
        ipb_clk_i                           => clk_ipb,
        -- readout interface
        bril_fifo_rd_en_i                   => bril_fifo_rd_en,
        bril_fifo_empty_o                   => bril_fifo_empty,
        bril_fifo_words_cnt_o               => open,
        bril_data_o                         => open,
        bril_data_valid_o                   => open,
        -- injection interface
        injection_data_i                    => injection_data,
        injection_reset_pointer_i           => injection_reset_pointer,
        injection_wr_en_i                   => injection_wr_en,
        injection_norbits_available_i       => injection_norbits_available,
        injection_norbits_tocollect_i       => injection_norbits_tocollect,
        injection_start_i                   => injection_start,
        injection_stop_i                    => injection_stop
    );
    
    -- read out fifo if not empty
    process (clk_ipb, reset)
    begin
        if reset = '1' then
            clk_parity <= '0';
        elsif rising_edge(clk_ipb) then
            clk_parity <= not clk_parity;
            if bril_fifo_empty = '0' and clk_parity = '0' then
                bril_fifo_rd_en <= '1';
            else
                bril_fifo_rd_en <= '0';
            end if;
        end if;
    end process;

end Behavioral;
