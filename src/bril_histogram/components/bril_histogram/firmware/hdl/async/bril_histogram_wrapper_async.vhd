----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_wrapper_sync - rtl
-- Description: 
--          Generates histogramming and streaming cores
--          Here we generate a second-level FIFO (optionally), which increases the usage of memory but reduces the bandwidth requirement
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- user macros to create fifo (device agnostic)
library xpm;
use xpm.vcomponents.all;

-- bril package
use work.bril_histogram_package.all;

entity bril_histogram_wrapper_async is
Generic (
    -- use external new histogram
    USE_EXTERNAL_NEW_HISTOGRAM      : boolean := false;
    
    -- identification of the histogram
    HISTOGRAM_TYPE                  : natural;
    HISTOGRAM_ID                    : natural;
    -- histogram parameters
    NUMBER_OF_BINS                  : natural;
    COUNTER_WIDTH                   : natural;
    INCREMENT_WIDTH                 : natural;
    -- delay of the increment with respect to the iteration/bin id signals
    INCREMENT_DELAY                 : natural := 0;
    -- counter width
    ORBIT_BX_COUNTER_WIDTH          : natural;
    -- error parameters
    NUMBER_OF_UNITS                 : natural := 1; -- number of units (modules, chips) connected to the histogrammer, can be zero without the accumulation layers
    NUMBER_OF_WORDS_PER_ERROR       : natural := 0;
    MAX_NUMBER_OF_MASKERROR_WORDS   : natural := 0
);
Port(
    -- core signals
    reset_i                     : in std_logic; -- sync reset
    clk_i                       : in std_logic; -- counting clock, typ. 40mhz
    -- external new histogram 
    new_histogram_i             : in std_logic := '0';      
    -- control signals
    valid_i                     : in std_logic;
    iteration_id_i	            : in std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    bin_id_i		            : in std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    -- increment signals (delayed)
    increment_overflow_i        : in std_logic := '0';
    increment_i                 : in std_logic_vector(INCREMENT_WIDTH-1 downto 0); -- increment command   
    -- tcds data
    lhc_fill_i                  : in std_logic_vector(31 downto 0);
    cms_run_i                   : in std_logic_vector(31 downto 0);
    lumi_section_i              : in std_logic_vector(31 downto 0);
    lumi_nibble_i               : in std_logic_vector(31 downto 0);  
    -- error block interface
    err_bl_n_msk_err_words_i    : in std_logic_vector(15 downto 0) := (others => '0');
    err_bl_err_word_i           : in std_logic_vector(31 downto 0) := (others => '0');
    err_bl_rd_en_o              : out std_logic;
    
    -- fifo interface
    fifo_rd_clk_i               : in std_logic;
    fifo_rd_en_i                : in std_logic;
    fifo_empty_o                : out std_logic;
    fifo_words_cnt_o            : out std_logic_vector(31 downto 0);
    fifo_data_o                 : out std_logic_vector(31 downto 0);
    fifo_data_valid_o           : out std_logic;
    fifo_not_empty_o            : out std_logic;
    
    -- ready to get data
    init_done_O                 : out std_logic
);
end bril_histogram_wrapper_async;

architecture rtl of bril_histogram_wrapper_async is

    -- calculate the amount of memory to be allocated for counters and for the fifo       
    constant MEMORY_BLOCK_PARAMETERS    : memory_block_parameters_type := get_memory_block_parameters(COUNTER_WIDTH, NUMBER_OF_BINS, MAX_NUMBER_OF_MASKERROR_WORDS); 
    -- bram parameter
    constant BRAM_OUTPUT_DELAY          : natural := 2;
    constant BRAM_PORTA_ADDR_WIDTH      : natural := logNceil(MEMORY_BLOCK_PARAMETERS.total_bram_size_async,2)+1;
    constant BRAM_PORTB_ADDR_WIDTH      : natural := logNceil(MEMORY_BLOCK_PARAMETERS.total_bram_size_async*MEMORY_BLOCK_PARAMETERS.bram_word_width/32,2)+1;
    
    -- reset state machine
    type reset_fsm_state_type is (Init, ResetMemory, Ready);
    signal reset_fsm_state              : reset_fsm_state_type;
    signal reset_from_init              : std_logic;
    
    -- buffered input signals
    signal reset_internal               : std_logic; 
    signal new_histogram_internal       : std_logic; 
    signal valid_internal               : std_logic;
    signal iteration_id_internal        : std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    signal bin_id_internal              : std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    signal increment_internal           : std_logic_vector(INCREMENT_WIDTH-1 downto 0);
    
    -- delayed inputs
    signal new_histogram_delayed        : std_logic;
    signal valid_delayed                : std_logic;
    signal iteration_id_delayed	        : std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    signal bin_id_delayed		        : std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    
    -- histogram info from core
    signal iteration_counter_from_core  : std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    signal increment_overflow_from_core : std_logic;
    signal overflow_from_core           : std_logic;
    
    -- memory interface types
    subtype ram_addr_a_type is natural range 0 to MEMORY_BLOCK_PARAMETERS.total_bram_size_async-1;
    type ram_addr_a_merged_type is array(1 downto 0) of ram_addr_a_type;
    subtype ram_data_a_type is std_logic_vector(MEMORY_BLOCK_PARAMETERS.bram_word_width-1 downto 0); 
    type ram_data_a_merged_type is array(1 downto 0) of ram_data_a_type;
    subtype ram_we_a_type is std_logic_vector(MEMORY_BLOCK_PARAMETERS.bram_word_width/8 - 1 downto 0);
    type ram_we_a_merged_type is array(1 downto 0) of ram_we_a_type;
    
    subtype ram_addr_b_type is natural range 0 to MEMORY_BLOCK_PARAMETERS.total_bram_size_async*MEMORY_BLOCK_PARAMETERS.bram_word_width/32-1;
    type ram_addr_b_merged_type is array(1 downto 0) of ram_addr_b_type;
    subtype ram_data_b_type is std_logic_vector(31 downto 0); 
    type ram_data_b_merged_type is array(1 downto 0) of ram_data_b_type;
    subtype ram_we_b_type is std_logic_vector(0 downto 0);
    type ram_we_b_merged_type is array(1 downto 0) of ram_we_b_type;
    
    -- memory interface itself
    -- port a (write and read)
    signal ram_addr_a                   : ram_addr_a_merged_type;
    signal ram_dout_a                   : ram_data_a_merged_type;
    signal ram_din_a                    : ram_data_a_merged_type;
    signal ram_we_a                     : ram_we_a_merged_type;  
    -- port a control from reset fsm
    signal ram_addr_a_from_reset        : ram_addr_a_type;
    signal ram_din_a_from_reset         : ram_data_a_type;
    signal ram_we_a_from_reset          : std_logic; 
    -- port a control from core 
    signal ram_addr_a_from_core         : ram_addr_a_type;
    signal ram_dout_a_to_core           : ram_data_a_type;
    signal ram_din_a_from_core          : ram_data_a_type;
    signal ram_we_a_from_core           : std_logic; 
    -- port a control from packager
    signal ram_addr_a_from_packer       : ram_addr_a_type;
    signal ram_dout_a_to_packer         : ram_data_a_type;
    signal ram_din_a_from_packer        : ram_data_a_type;
    signal ram_we_a_from_packer         : ram_we_a_type;    
    -- port b (ipbus read)
    signal ram_addr_b                   : ram_addr_b_merged_type;
    signal ram_dout_b                   : ram_data_b_merged_type;  
    signal ram_din_b                    : ram_data_b_merged_type;   
    signal ram_we_b                     : ram_we_b_merged_type;
    -- port b control from packer
    signal ram_addr_b_from_packer       : ram_addr_b_type;
    signal ram_dout_b_to_packer         : ram_data_b_type;
    signal ram_din_b_from_packer        : ram_data_b_type;
    signal ram_we_b_from_packer         : std_logic;         
    
    -- bram used by the core
    signal buffer_to_write              : natural range 0 to 1; 
    signal switch_memory_request        : std_logic; 
    
    -- fifo not resdy signal
    signal init_done                    : std_logic;
    signal fifo_not_empty               : std_logic; 
    
begin

    --====================================================================--
    -- reset fsm
    --====================================================================--
    process (clk_i)
    begin
        if rising_edge(clk_i) then
            if reset_i = '1' then
                reset_from_init <= '1';
                init_done <= '0';
                reset_fsm_state <= Init;
                --
                ram_addr_a_from_reset <= 0;
                ram_din_a_from_reset <= (others => '0');
                ram_we_a_from_reset <= '0';
            else
                case reset_fsm_state is
                    when Init =>
                        init_done <= '0';
                        reset_from_init <= '1';
                        --
                        ram_addr_a_from_reset <= 0;
                        ram_din_a_from_reset <= (others => '0');
                        ram_we_a_from_reset <= '1';
                        --
                        reset_fsm_state <= ResetMemory;
                    when ResetMemory =>
                        init_done <= '0';
                        reset_from_init <= '1';
                        -- we don't need to reset all the bram including errors - just the one with header an counters
                        if ram_addr_a_from_reset < divide_roundup(MEMORY_BLOCK_PARAMETERS.total_counter_words + MEMORY_BLOCK_PARAMETERS.header_size, MEMORY_BLOCK_PARAMETERS.bram_word_width/32)-1 then
                            ram_addr_a_from_reset <= ram_addr_a_from_reset + 1;
                            ram_din_a_from_reset <= (others => '0');
                            ram_we_a_from_reset <= '1';
                        else
                            ram_addr_a_from_reset <= 0;
                            ram_din_a_from_reset <= (others => '0');
                            ram_we_a_from_reset <= '0';
                            -- done
                            reset_fsm_state <= Ready;
                        end if;
                    when Ready =>
                        -- 
                        init_done <= '1';
                        
                        -- reset disabled
                        reset_from_init <= '0';
                        --
                        ram_addr_a_from_reset <= 0;
                        ram_din_a_from_reset <= (others => '0');
                        ram_we_a_from_reset <= '0';
                    when others =>
                        reset_fsm_state <= Init;    
                end case;
            end if;
        end if;
    end process;
    -- output init signal
    init_done_o <= init_done;
    
    --====================================================================--
    -- In case we have to generate our own new histogram signal - do it
    --====================================================================--
    gen_ext_new_hist: if USE_EXTERNAL_NEW_HISTOGRAM = true generate
       reset_internal         <= reset_from_init;
       new_histogram_internal <= new_histogram_i;
       valid_internal         <= valid_i;
       iteration_id_internal  <= iteration_id_i;
       bin_id_internal        <= bin_id_i;
       increment_internal     <= increment_i;        
    end generate gen_ext_new_hist;
    
    gen_int_new_hist: if USE_EXTERNAL_NEW_HISTOGRAM = false generate
        -- buffering (generates new histogram in async mode)
        bril_histogram_prebuffer_async_inst: entity work.bril_histogram_prebuffer_async
        generic map (
            -- orbit and bx counter width
            ORBIT_BX_COUNTER_WIDTH          => ORBIT_BX_COUNTER_WIDTH,
            -- accumulation parameters
            NUMBER_OF_UNITS                 => 1,
            UNIT_INCREMENT_WIDTH            => INCREMENT_WIDTH,
            -- no need to buffer errors and mask
            IN_ACCUMULATION_LAYER           => false
        ) 
        port map(
            -- core signals
            reset_i                 => reset_from_init,
            clk_i                   => clk_i,
            -- memory ready
            init_done_i             => init_done,
            fifo_not_empty_i        => fifo_not_empty,
            -- control signalsl     
            valid_i                 => valid_i,
            iteration_id_i          => iteration_id_i,
            bin_id_i                => bin_id_i,
            -- data input interface 
            increment_i             => increment_i,
            
            -- buffered output signals
            reset_o                 => reset_internal, 
            new_histogram_o         => new_histogram_internal, 
            valid_o                 => valid_internal, 
            iteration_id_o          => iteration_id_internal, 
            bin_id_o                => bin_id_internal, 
            increment_o             => increment_internal
        );
    end generate gen_int_new_hist; 
    
    --====================================================================--
    -- This part of the code delays the input (in order to make sure that we have enough time to calculate the increment)
    --====================================================================--
    -- in this case delaying is not needed at all
    gen_no_delay: if INCREMENT_DELAY <= BRAM_OUTPUT_DELAY+1 generate
    
        new_histogram_delayed <= new_histogram_internal;
        valid_delayed <= valid_internal;
        iteration_id_delayed <= iteration_id_internal;  
        bin_id_delayed <= bin_id_internal;	 
        
    end generate gen_no_delay;
    
    -- in this case we have to delay the bram addr signals (otherwise we don't have enough time to calculate the increment)
    gen_increment_is_slower: if INCREMENT_DELAY > BRAM_OUTPUT_DELAY+1 generate
    
        -- delayed input valid   
        shift_new_histogram: entity work.bril_histogram_shift_register
        generic map(
            REGISTER_WIDTH  => 1,
            DELAY           => (INCREMENT_DELAY - (BRAM_OUTPUT_DELAY+1))
        )
        port map (
            clk_i           => clk_i,
            input_i(0)      => new_histogram_internal,
            output_o(0)     => new_histogram_delayed
        );
      
        -- delayed input valid   
        shift_input_valid: entity work.bril_histogram_shift_register
        generic map(
            REGISTER_WIDTH  => 1,
            DELAY           => (INCREMENT_DELAY - (BRAM_OUTPUT_DELAY+1))
        )
        port map (
            clk_i           => clk_i,
            input_i(0)      => valid_internal,
            output_o(0)     => valid_delayed
        );
        
        -- delayed iteration id
        shift_iteration_id: entity work.bril_histogram_shift_register
        generic map(
            REGISTER_WIDTH  => ORBIT_BX_COUNTER_WIDTH,
            DELAY           => (INCREMENT_DELAY - (BRAM_OUTPUT_DELAY+1))
        )
        port map (
            clk_i           => clk_i,
            input_i         => iteration_id_internal,
            output_o        => iteration_id_delayed
        );  
        
        -- delayed bin id
        shift_bin_id: entity work.bril_histogram_shift_register
        generic map(
            REGISTER_WIDTH  => ORBIT_BX_COUNTER_WIDTH,
            DELAY           => (INCREMENT_DELAY - (BRAM_OUTPUT_DELAY+1))
        )
        port map (
            clk_i           => clk_i,
            input_i         => bin_id_internal,
            output_o        => bin_id_delayed
        );  
        
    end generate gen_increment_is_slower;
    
    --====================================================================--
    -- instantiate the histogramming core (async version)
    --====================================================================--
    bril_histogram_core_async_inst: entity work.bril_histogram_core_async
    generic map (
        -- memory 
        MEMORY_BLOCK_PARAMETERS => MEMORY_BLOCK_PARAMETERS,
        -- histogram parameters
        NUMBER_OF_BINS          => NUMBER_OF_BINS,
        COUNTER_WIDTH           => COUNTER_WIDTH,
        INCREMENT_WIDTH         => INCREMENT_WIDTH,
        -- counter width
        ORBIT_BX_COUNTER_WIDTH  => ORBIT_BX_COUNTER_WIDTH,
        -- delay of the increment with respect to the iteration/bin id signals
        BRAM_OUTPUT_DELAY       => BRAM_OUTPUT_DELAY -- constant of our bram (1 to output, 1 to pipeline)
    )
    port map (
        -- core signals       
        reset_i                 => reset_internal,            
        clk_i                   => clk_i,          
        -- new histogram
        new_histogram_i         => new_histogram_delayed,    
        -- async signals
        valid_i                 => valid_delayed,
        iteration_id_i	        => iteration_id_delayed,
        bin_id_i		        => bin_id_delayed,
        -- increment signals (delayed)
        increment_overflow_i    => increment_overflow_i,
        increment_i             => increment_internal,
        -- memory interfa       
        ram_addr_o              => ram_addr_a_from_core,
        ram_din_o               => ram_din_a_from_core,
        ram_we_o                => ram_we_a_from_core,
        ram_dout_i              => ram_dout_a_to_core,
        -- jistogram info
        iteration_counter_o     => iteration_counter_from_core,  
        increment_overflow_o    => increment_overflow_from_core,
        overflow_o              => overflow_from_core     
    );
    
    --====================================================================--
    -- bram conctroller
    --====================================================================--
    gen_bram_signals: for i in 0 to 1 generate
    
        -- signals to memmories (port a)
        ram_addr_a(i) <=    ram_addr_a_from_reset when (init_done = '0') else
                            ram_addr_a_from_core when (init_done = '1' and buffer_to_write = i) else
                            ram_addr_a_from_packer when (init_done = '1' and buffer_to_write /= i) else 
                            0;
       
        ram_din_a(i) <=     ram_din_a_from_reset when (init_done = '0') else
                            ram_din_a_from_core when (init_done = '1' and buffer_to_write = i) else
                            ram_din_a_from_packer when (init_done = '1' and buffer_to_write /= i) else 
                            (others => '0');
         
        ram_we_a(i) <=      (others => ram_we_a_from_reset) when (init_done = '0') else
                            (others => ram_we_a_from_core) when (init_done = '1' and buffer_to_write = i) else
                            ram_we_a_from_packer when (init_done = '1' and buffer_to_write /= i) else                    
                            (others => '0'); 
                            
         -- now port b
        ram_addr_b(i) <=    ram_addr_b_from_packer when (buffer_to_write /= i) else 
                            0;
       
        ram_din_b(i) <=     ram_din_b_from_packer when (buffer_to_write /= i) else 
                            (others => '0');
         
        ram_we_b(i) <=      (others => ram_we_b_from_packer) when (buffer_to_write /= i) else                    
                            (others => '0'); 
                           
    end generate;
    -- data from memories
    -- instead of bram  internal register - put it here, to relax timing on the following step
    process(clk_i) 
    begin
        if rising_edge(clk_i) then
            if init_done = '1' then
                ram_dout_a_to_core <= ram_dout_a(buffer_to_write);
                ram_dout_a_to_packer <= ram_dout_a((buffer_to_write+1) rem 2);
            else
                ram_dout_a_to_core <= ram_dout_a(buffer_to_write);
                ram_dout_a_to_packer <= ram_dout_a((buffer_to_write+1) rem 2);
            end if;
        end if;
    end process;
    --
    process(fifo_rd_clk_i)
    begin
        if rising_edge(fifo_rd_clk_i) then
            ram_dout_b_to_packer <= ram_dout_b((buffer_to_write+1) rem 2);
        end if;
    end process;
      
    --====================================================================--
    -- memory switching 
    --====================================================================--
    -- process to switch memory 
    process (clk_i) 
    begin
        if rising_edge(clk_i) then
            if reset_internal = '1' then
                buffer_to_write <= 0;
                switch_memory_request <= '0';
            else
                -- switch requested (delayed by one clock cycle to let core to write the last bin)
                if switch_memory_request = '1' then
                    -- switch memories
                    if buffer_to_write < 1 then
                        buffer_to_write <= buffer_to_write + 1;
                    else
                        buffer_to_write <= 0;
                    end if;
                end if;
                
                -- new histogram arrived and accepted 
                if new_histogram_delayed = '1' then
                    switch_memory_request <= '1';
                else 
                    switch_memory_request <= '0';
                end if;
           
            end if;
        end if;
    end process;                      
    
    --====================================================================--
    -- BRAMS
    --====================================================================--
    
    -- bram 0
--    bram0: entity work.bril_histogram_bram
--    generic map (
--        BRAM_WORD_WIDTH     => MEMORY_BLOCK_PARAMETERS.bram_word_width,
--        BRAM_DEPTH          => MEMORY_BLOCK_PARAMETERS.total_bram_size_async
--    )
--    port map ( 
--        clk_a_i             => clk_i,
--        clk_b_i             => fifo_rd_clk_i,
--        -- input port
--        ram_addr_a_i        => ram_addr_a(0),
--        ram_dout_a_o        => ram_dout_a(0),
--        ram_din_a_i         => ram_din_a(0),
--        ram_we_a_i          => ram_we_a(0),
--        -- output port
--        ram_addr_b_i        => ram_addr_b(0),
--        ram_dout_b_o        => ram_dout_b(0)
--    );
    bram0 : xpm_memory_tdpram
    generic map (
        ADDR_WIDTH_A => BRAM_PORTA_ADDR_WIDTH,               
        ADDR_WIDTH_B => BRAM_PORTB_ADDR_WIDTH,               
        AUTO_SLEEP_TIME => 0,            
        BYTE_WRITE_WIDTH_A => 8,        
        BYTE_WRITE_WIDTH_B => 32,        
        CLOCKING_MODE => "independent_clock", 
        ECC_MODE => "no_ecc",            
        MEMORY_INIT_FILE => "none",      
        MEMORY_INIT_PARAM => "0",        
        MEMORY_OPTIMIZATION => "true",   
        MEMORY_PRIMITIVE => "auto",      
        MEMORY_SIZE => MEMORY_BLOCK_PARAMETERS.total_bram_size_async*MEMORY_BLOCK_PARAMETERS.bram_word_width,             
        MESSAGE_CONTROL => 0,            
        READ_DATA_WIDTH_A => MEMORY_BLOCK_PARAMETERS.bram_word_width,         
        READ_DATA_WIDTH_B => 32,         
        READ_LATENCY_A => BRAM_OUTPUT_DELAY-1,             
        READ_LATENCY_B => BRAM_OUTPUT_DELAY-1,             
        READ_RESET_VALUE_A => "0",       
        READ_RESET_VALUE_B => "0",       
        USE_EMBEDDED_CONSTRAINT => 0,    
        USE_MEM_INIT => 1,               
        WAKEUP_TIME => "disable_sleep",  
        WRITE_DATA_WIDTH_A => MEMORY_BLOCK_PARAMETERS.bram_word_width,        
        WRITE_DATA_WIDTH_B => 32,        
        WRITE_MODE_A => "no_change",     
        WRITE_MODE_B => "read_first"      
    )
    port map (
    
        -- port a
        clka                => clk_i,
        ena                 => '1',
        regcea              => '1', 
        addra               => std_logic_vector(to_unsigned(ram_addr_a(0),BRAM_PORTA_ADDR_WIDTH)), 
        douta               => ram_dout_a(0), 
        dina                => ram_din_a(0),
        wea                 => ram_we_a(0),
        
        -- port b
        clkb                => fifo_rd_clk_i, 
        enb                 => '1',
        regceb              => '1',
        addrb               => std_logic_vector(to_unsigned(ram_addr_b(0),BRAM_PORTB_ADDR_WIDTH)), 
        doutb               => ram_dout_b(0), 
        dinb                => ram_din_b(0),
        web                 => ram_we_b(0),
        
        -- unused
        rsta                => '0', 
        rstb                => '0',
        injectdbiterra      => '0',
        injectdbiterrb      => '0',
        injectsbiterra      => '0',
        injectsbiterrb      => '0',
        sleep               => '0'
    );
      
    -- bram 1
--    bram1: entity work.bril_histogram_bram
--    generic map (
--        BRAM_WORD_WIDTH     => MEMORY_BLOCK_PARAMETERS.bram_word_width,
--        BRAM_DEPTH          => MEMORY_BLOCK_PARAMETERS.total_bram_size_async
--    )
--    port map ( 
--        clk_a_i             => clk_i,
--        clk_b_i             => fifo_rd_clk_i,
--        -- input port
--        ram_addr_a_i        => ram_addr_a(1),
--        ram_dout_a_o        => ram_dout_a(1),
--        ram_din_a_i         => ram_din_a(1),
--        ram_we_a_i          => ram_we_a(1),
--        -- output port
--        ram_addr_b_i        => ram_addr_b(1),
--        ram_dout_b_o        => ram_dout_b(1)
--    );

    bram1 : xpm_memory_tdpram
    generic map (
        ADDR_WIDTH_A => BRAM_PORTA_ADDR_WIDTH,               
        ADDR_WIDTH_B => BRAM_PORTB_ADDR_WIDTH,               
        AUTO_SLEEP_TIME => 0,            
        BYTE_WRITE_WIDTH_A => 8,        
        BYTE_WRITE_WIDTH_B => 32,        
        CLOCKING_MODE => "independent_clock", 
        ECC_MODE => "no_ecc",            
        MEMORY_INIT_FILE => "none",      
        MEMORY_INIT_PARAM => "0",        
        MEMORY_OPTIMIZATION => "true",   
        MEMORY_PRIMITIVE => "auto",      
        MEMORY_SIZE => MEMORY_BLOCK_PARAMETERS.total_bram_size_async*MEMORY_BLOCK_PARAMETERS.bram_word_width,             
        MESSAGE_CONTROL => 0,            
        READ_DATA_WIDTH_A => MEMORY_BLOCK_PARAMETERS.bram_word_width,         
        READ_DATA_WIDTH_B => 32,         
        READ_LATENCY_A => BRAM_OUTPUT_DELAY-1,             
        READ_LATENCY_B => BRAM_OUTPUT_DELAY-1,             
        READ_RESET_VALUE_A => "0",       
        READ_RESET_VALUE_B => "0",       
        USE_EMBEDDED_CONSTRAINT => 0,    
        USE_MEM_INIT => 1,               
        WAKEUP_TIME => "disable_sleep",  
        WRITE_DATA_WIDTH_A => MEMORY_BLOCK_PARAMETERS.bram_word_width,        
        WRITE_DATA_WIDTH_B => 32,        
        WRITE_MODE_A => "no_change",     
        WRITE_MODE_B => "read_first"      
    )
    port map (
    
        -- port a
        clka                => clk_i,
        ena                 => '1',
        regcea              => '1', 
        addra               => std_logic_vector(to_unsigned(ram_addr_a(1),BRAM_PORTA_ADDR_WIDTH)), 
        douta               => ram_dout_a(1), 
        dina                => ram_din_a(1),
        wea                 => ram_we_a(1),
        
        -- port b
        clkb                => fifo_rd_clk_i, 
        enb                 => '1',
        regceb              => '1',
        addrb               => std_logic_vector(to_unsigned(ram_addr_b(1),BRAM_PORTB_ADDR_WIDTH)), 
        doutb               => ram_dout_b(1), 
        dinb                => ram_din_b(1),
        web                 => ram_we_b(1),
        
        -- unused
        rsta                => '0', 
        rstb                => '0',
        injectdbiterra      => '0',
        injectdbiterrb      => '0',
        injectsbiterra      => '0',
        injectsbiterrb      => '0',
        sleep               => '0'
    );
    
    
    --====================================================================--
    -- packer
    --====================================================================--
    bril_histogram_packer_async_inst: entity work.bril_histogram_packer_async
    generic map (
        -- identification of the histogram
        HISTOGRAM_TYPE                  => HISTOGRAM_TYPE,               
        HISTOGRAM_ID                    => HISTOGRAM_ID,                 
        -- memory parameters           
        MEMORY_BLOCK_PARAMETERS         => MEMORY_BLOCK_PARAMETERS,      
        -- histogram parameters         
        NUMBER_OF_BINS                  => NUMBER_OF_BINS,               
        COUNTER_WIDTH                   => COUNTER_WIDTH,                
        INCREMENT_WIDTH                 => INCREMENT_WIDTH,              
        -- counter width                    
        ORBIT_BX_COUNTER_WIDTH          => ORBIT_BX_COUNTER_WIDTH,      
        -- error parameters             
        NUMBER_OF_UNITS                 => NUMBER_OF_UNITS,              
        NUMBER_OF_WORDS_PER_ERROR       => NUMBER_OF_WORDS_PER_ERROR,    
        MAX_NUMBER_OF_MASKERROR_WORDS   => MAX_NUMBER_OF_MASKERROR_WORDS,
        --
        BRAM_OUTPUT_DELAY               => BRAM_OUTPUT_DELAY
    )
    port map(
        -- core signals
        reset_i                         => reset_internal,
        clk_i                           => clk_i,
        -- new histogram detected by the wrapper
        new_histogram_i                 => new_histogram_delayed,
        -- histogram info
        iteration_counter_i             => iteration_counter_from_core,
        increment_overflow_i            => increment_overflow_from_core,
        overflow_i                      => overflow_from_core,
        -- memory interface (clk i)
        ram_addr_a_o                    => ram_addr_a_from_packer,
        ram_din_a_o                     => ram_din_a_from_packer,
        ram_we_a_o                      => ram_we_a_from_packer,
        ram_dout_a_i                    => ram_dout_a_to_packer,
        -- memory interface (fifo clk)
        ram_addr_b_o                    => ram_addr_b_from_packer,
        ram_din_b_o                     => ram_din_b_from_packer,
        ram_we_b_o                      => ram_we_b_from_packer,
        ram_dout_b_i                    => ram_dout_b_to_packer,
        -- error interface
        err_bl_n_msk_err_words_i        => err_bl_n_msk_err_words_i, 
        err_bl_err_word_i               => err_bl_err_word_i,        
        err_bl_rd_en_o                  => err_bl_rd_en_o,  
        -- tcds data
        lhc_fill_i                      => lhc_fill_i,
        cms_run_i                       => cms_run_i,
        lumi_section_i                  => lumi_section_i,
        lumi_nibble_i                   => lumi_nibble_i,    
        -- fifo interface                   
        fifo_rd_clk_i                   => fifo_rd_clk_i,            
        fifo_rd_en_i                    => fifo_rd_en_i,             
        fifo_empty_o                    => fifo_empty_o,             
        fifo_words_cnt_o                => fifo_words_cnt_o,
        fifo_data_o                     => fifo_data_o,
        fifo_data_valid_o               => fifo_data_valid_o,
        fifo_not_empty_o                => fifo_not_empty    
    );
    -- to the erorr block
    fifo_not_empty_o <= fifo_not_empty;
    
end rtl;
