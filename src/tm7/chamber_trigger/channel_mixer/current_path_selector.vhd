-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Multi-channel data mixer
---- File       : current_path_selector.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-10
-------------------------------------------------------------------------------
---- Description: Starting with a given channel/layer, this module gives 
----              "valid paths" considering that current channel has a hit and
----              consulting, at the same time, the "empty" signal from another 
----              3 additional (and straigth-line compatible) channels.
----
----              First time it is called after a reset, it takes 3 clock 
----              cycles to code the initial results, giving an initial "valid"
----              flag, an array of aligned channels groups, and the "index"
----              inside this group for the first valid trajectory.
----              If "valid" flag is '0' it means that there are no valid 
----              group of channels forming a muon trajectory, including the
----              current one.
----
----              After the initial post-reset processing other possible 
----              valid trajectories are computed setting "next_path" flag
----              for a clock cycle.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.encoders_pkg.all;
use work.mixer_pkg.all;

entity CURRENT_PATH_SELECTOR is
  port (
    clk                   : in  std_logic;
    rst                   : in  std_logic;
    next_path             : in  std_logic;
    channel               : in  std_logic_vector(6 downto 0);
    layer                 : in  std_logic_vector(1 downto 0);
    ch_empty_lay0         : in  std_logic_vector(127 downto 0);
    ch_empty_lay1         : in  std_logic_vector(127 downto 0);
    ch_empty_lay2         : in  std_logic_vector(127 downto 0);
    ch_empty_lay3         : in  std_logic_vector(127 downto 0);
    valid                 : out std_logic;
    current_path_idx      : out std_logic_vector(2 downto 0);
    current_path_channels : out channels_path_t
  );
end entity;

architecture Behavioural of CURRENT_PATH_SELECTOR is

  -- Internal signals to be registered.
  type reg_type is record
    current_valid         : std_logic;
    current_path_idx      : std_logic_vector(2 downto 0);
    current_path_channels : channels_path_t;
  end record;

  constant regDefault : reg_type := (
    current_valid         => '0',
    current_path_idx      => (others => '0'),
    current_path_channels => (others => (others => '0'))
  );
  signal reg : reg_type := regDefault;

  signal stage_control       : std_logic;
  signal valid_code          : std_logic;
  signal path_candidates     : path_candidates_arr_t;
  signal valid_paths         : std_logic_vector(7 downto 0);
  signal first_valid_path    : std_logic_vector(2 downto 0);
  signal initial_valid_paths : std_logic_vector(7 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  valid                 <= reg.current_valid;
  current_path_idx      <= reg.current_path_idx;
  current_path_channels <= reg.current_path_channels;

  ---------------------------------------------------------------
  -- Sequential processes
  --
  GEN_VALID_PATH : for i in 0 to 7 generate
    process(clk, rst)
    begin
      if clk = '1' and clk'event then
        if rst = '1' or 
           (next_path     = '1' and 
            stage_control = '1' and 
            to_integer(unsigned(first_valid_path)) = i) 
        then 
          valid_paths(i) <= '0';
        elsif stage_control = '0' then 
          valid_paths(i) <= initial_valid_paths(i);
        end if;
      end if;
    end process;
  end generate;

  STAGE_CTRL_OUT_REG : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1' then 
        stage_control <= '0';
      else 
        stage_control <= '1';

        -- Output registration
        reg.current_path_idx <= first_valid_path;
        reg.current_path_channels 
              <= path_candidates(to_integer(unsigned(first_valid_path)));

        reg.current_valid 
              <= valid_code and   
                 initial_valid_paths(to_integer(unsigned(first_valid_path)));
      end if;
    end if;
  end process;
  
  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  VAL_PATH_ENC : VALID_PATHS_ENCODER port map (
    clk             => clk,
    channel         => channel,
    layer           => layer,
    ch_empty_lay0   => ch_empty_lay0,
    ch_empty_lay1   => ch_empty_lay1,
    ch_empty_lay2   => ch_empty_lay2,
    ch_empty_lay3   => ch_empty_lay3,
    valid_paths     => initial_valid_paths,
    path_candidates => path_candidates
  );

  PRIENC_8_3 : PRIORITY_ENC_8_3 port map (
    sel   => valid_paths,
    valid => valid_code,
    code  => first_valid_path
  );
  
end architecture;
