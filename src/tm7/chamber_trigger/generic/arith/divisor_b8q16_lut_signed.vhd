-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Arithmetics
---- File       : divisor_b8q16_lut_signed.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-08
-------------------------------------------------------------------------------
---- Description: LUT to get the inverse of a number quatized by a 16 bit base
----              Values have an additional bit to consider the sign
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity DIVISOR_B8Q16_LUT_SIGNED is
  port(
    value          : in  signed(8 downto 0);
    value_inverted : out signed(16 downto 0)
  );
end entity;

architecture RTL of DIVISOR_B8Q16_LUT_SIGNED is
  constant LEN_VAL : integer:= value'length;
  constant LEN_INV : integer:= value_inverted'length;

  type data_lut_t is array (0 to 2**LEN_VAL - 1) 
                  of integer range -(2**(LEN_INV - 1)) to 2**(LEN_INV - 1) - 1;

  constant DATA_LUT : data_lut_t := (
    --
    -- This are the quantized values (a/b) for possitive numbers (value's 
    -- highest bit = 0, where a = 2^16 and b = (0 to 256) <=> 2^8
    --
    -- Values' order is crytical.
    --
    65535,
    65535, 32768, 21845, 16384, 13107, 10923, 9362, 8192, 7282, 6554, 
     5958,  5461,  5041,  4681,  4369,  4096, 3855, 3641, 3449, 3277, 
     3121,  2979,  2849,  2731,  2621,  2521, 2427, 2341, 2260, 2185,  
     2114,  2048,  1986,  1928,  1872,  1820, 1771, 1725, 1680, 1638, 
     1598,  1560,  1524,  1489,  1456,  1425, 1394, 1365, 1337, 1311,  
     1285,  1260,  1237,  1214,  1192,  1170, 1150, 1130, 1111, 1092,  
     1074,  1057,  1040,  1024,  1008,   993,  978,  964,  950,  936,   
      923,   910,   898,   886,   874,   862,  851,  840,  830,  819,  
      809,   799,   790,   780,   771,   762,  753,  745,  736,  728,   
      720,   712,   705,   697,   690,   683,  676,  669,  662,  655,   
      649,   643,   636,   630,   624,   618,  612,  607,  601,  596,   
      590,   585,   580,   575,   570,   565,  560,  555,  551,  546,   
      542,   537,   533,   529,   524,   520,  516,  512,  508,  504,   
      500,   496,   493,   489,   485,   482,  478,  475,  471,  468,   
      465,   462,   458,   455,   452,   449,  446,  443,  440,  437,   
      434,   431,   428,   426,   423,   420,  417,  415,  412,  410,   
      407,   405,   402,   400,   397,   395,  392,  390,  388,  386,   
      383,   381,   379,   377,   374,   372,  370,  368,  366,  364,   
      362,   360,   358,   356,   354,   352,  350,  349,  347,  345,   
      343,   341,   340,   338,   336,   334,  333,  331,  329,  328,   
      326,   324,   323,   321,   320,   318,  317,  315,  314,  312,   
      311,   309,   308,   306,   305,   303,  302,  301,  299,  298,   
      297,   295,   294,   293,   291,   290,  289,  287,  286,  285,   
      284,   282,   281,   280,   279,   278,  277,  275,  274,  273,   
      272,   271,   270,   269,   267,   266,  265,  264,  263,  262,   
      261,   260,   259,   258,   257,
    --               
    -- And here they are the quatized values associated to negative input
    -- values        
    --               
      -256,   -257,   -258,  -259,  -260,  -261,   -262,   -263,   -264, -265, 
      -266,   -267,   -269,  -270,  -271,  -272,   -273,   -274,   -275, -277, 
      -278,   -279,   -280,  -281,  -282,  -284,   -285,   -286,   -287, -289, 
      -290,   -291,   -293,  -294,  -295,  -297,   -298,   -299,   -301, -302, 
      -303,   -305,   -306,  -308,  -309,  -311,   -312,   -314,   -315, -317, 
      -318,   -320,   -321,  -323,  -324,  -326,   -328,   -329,   -331, -333, 
      -334,   -336,   -338,  -340,  -341,  -343,   -345,   -347,   -349, -350, 
      -352,   -354,   -356,  -358,  -360,  -362,   -364,   -366,   -368, -370, 
      -372,   -374,   -377,  -379,  -381,  -383,   -386,   -388,   -390, -392, 
      -395,   -397,   -400,  -402,  -405,  -407,   -410,   -412,   -415, -417, 
      -420,   -423,   -426,  -428,  -431,  -434,   -437,   -440,   -443, -446, 
      -449,   -452,   -455,  -458,  -462,  -465,   -468,   -471,   -475, -478, 
      -482,   -485,   -489,  -493,  -496,  -500,   -504,   -508,   -512, -516, 
      -520,   -524,   -529,  -533,  -537,  -542,   -546,   -551,   -555, -560, 
      -565,   -570,   -575,  -580,  -585,  -590,   -596,   -601,   -607, -612, 
      -618,   -624,   -630,  -636,  -643,  -649,   -655,   -662,   -669, -676, 
      -683,   -690,   -697,  -705,  -712,  -720,   -728,   -736,   -745, -753, 
      -762,   -771,   -780,  -790,  -799,  -809,   -819,   -830,   -840, -851, 
      -862,   -874,   -886,  -898,  -910,  -923,   -936,   -950,   -964, -978, 
      -993,  -1008,  -1024, -1040, -1057, -1074,  -1092,  -1111,  -1130, 
     -1150,  -1170,  -1192, -1214, -1237, -1260,  -1285,  -1311,  -1337, 
     -1365,  -1394,  -1425, -1456, -1489, -1524,  -1560,  -1598,  -1638, 
     -1680,  -1725,  -1771, -1820, -1872, -1928,  -1986,  -2048,  -2114, 
     -2185,  -2260,  -2341, -2427, -2521, -2621,  -2731,  -2849,  -2979, 
     -3121,  -3277,  -3449, -3641, -3855, -4096,  -4369,  -4681,  -5041, 
     -5461,  -5958,  -6554, -7282, -8192, -9362, -10923, -13107, -16384, 
    -21845, -32768, -65535    
  );
  
  -- Types: auto|block|distributed
  attribute ram_style: string;
  attribute ram_style of DATA_LUT : constant is "distributed";
  
  -- Signal to clarify the code.
  signal aux_u : unsigned(LEN_VAL - 1 downto 0);

begin
  aux_u          <= unsigned(std_logic_vector(value));
  value_inverted <= to_signed(DATA_LUT(to_integer(aux_u)), LEN_INV);
  
end architecture;