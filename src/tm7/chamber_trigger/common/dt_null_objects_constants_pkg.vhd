-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Common components
---- File       : dt_null_objects_constants_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-05
-------------------------------------------------------------------------------
---- Description: Package to join NULL constants for different object.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
use work.dt_common_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package dt_null_objects_constants_pkg is

  -- RAW Hit
  constant ZEROES_RAWHIT_LEN : std_logic_vector(DTRAWHIT_SIZE - 1 downto 0) 
                             := (others => '0');

  constant DT_RAWHIT_NULL : DTRawHit_t := dtRawHit_from_slv(ZEROES_RAWHIT_LEN);

  -- NULL Hit
  constant ZEROES_HIT_LEN : std_logic_vector(DTHIT_SIZE - 1 downto 0) 
                          := (others => '0');

  constant DT_HIT_NULL : DTHit_t := dtHit_from_slv(ZEROES_HIT_LEN);

  -- Hits for a segment
  constant NULL_MAIN_SEG : std_logic_vector(DTHITS_SEGMENT_SIZE - 1 downto 0)
                         := (others => '0');

  constant DT_HITS_SEG_NULL : HitsSegment_t
                            := hitsSegment_from_slv(NULL_MAIN_SEG);

  -- Segment candidate
  constant DT_SEG_CAND_SLV_EMPTY : 
      std_logic_vector(DTSEG_CANDIDATE_SIZE - 1 downto 0) := (others => '0');

  constant DT_SEG_CAND_NULL : DTSegCandidate_t 
                            := dtSegCandidate_from_slv(DT_SEG_CAND_SLV_EMPTY);

  -- NULL segment main parameters
  constant DT_SEG_MAIN_SLV_EMPTY : 
      std_logic_vector(DTSEG_MAINPARAMS_SIZE - 1 downto 0) := (others => '0');

  constant DT_SEG_MAIN_NULL : DTSegMainParams_t := 
      dtSegMainParams_from_slv(DT_SEG_MAIN_SLV_EMPTY);

  -- NULL segment
  constant DT_SEGMENT_SLV_EMPTY : 
      std_logic_vector(DTSEGMENT_SIZE - 1 downto 0) := (others => '0');

  constant DT_SEGMENT_NULL : DTSegment_t 
                           := dtSegment_from_slv(DT_SEGMENT_SLV_EMPTY);

  -- NULL primitive
  constant DT_PRIMITIVE_SLV_EMPTY : 
      std_logic_vector(DTPRIMITIVE_SIZE - 1 downto 0) := (others => '0');

  constant DT_PRIMITIVE_NULL : DTPrimitive_t 
                             := dtPrimitive_from_slv(DT_PRIMITIVE_SLV_EMPTY);

end package;

