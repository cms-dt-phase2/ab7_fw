----------------------------------------------------------------------------------
-- Multi-BRAM_SDP_MACRO memory.
-- �lvaro Navarro, CIEMAT, 2015
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

library work;
use work.minmax.all;
use work.bo2.all;

entity multi_bram_sdp is
  Generic (
    DATA_WIDTH        : positive := 8;
    DEPTH_LOG2        : positive := 11;
    MIN_OUT_MUX       : boolean := true; -- forces ignore of BR_ADDR_WIDTH and implement a minimum output multiplexing solution
    BR_ADDR_WIDTH     : positive range 15 downto 9 := 9 -- if 15 --> BR36, else BR18
  );
  Port (
    din  : IN   STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
    wadd : IN   STD_LOGIC_VECTOR(DEPTH_LOG2-1 DOWNTO 0);
    wen  : IN   STD_LOGIC;
    wclk : IN   STD_LOGIC;
    
    dout : OUT STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
    radd : IN   STD_LOGIC_VECTOR(DEPTH_LOG2-1 DOWNTO 0);
    ren  : IN   STD_LOGIC;
    rclk : IN   STD_LOGIC
  );
end entity;

architecture Behavioral of multi_bram_sdp is

  -- these are the values for the BRAM_SDP_MACRO
  -----------------------------------------------------------------------
  --  READ_WIDTH | BRAM_SIZE | READ Depth  | RDADDR Width |            --
  -- WRITE_WIDTH |           | WRITE Depth | WRADDR Width |  WE Width  --
  -- ============|===========|=============|==============|============--
  --    37-72    |  "36Kb"   |      512    |     9-bit    |    8-bit   --
  --    19-36    |  "36Kb"   |     1024    |    10-bit    |    4-bit   --
  --    19-36    |  "18Kb"   |      512    |     9-bit    |    4-bit   --
  --    10-18    |  "36Kb"   |     2048    |    11-bit    |    2-bit   --
  --    10-18    |  "18Kb"   |     1024    |    10-bit    |    2-bit   --
  --     5-9     |  "36Kb"   |     4096    |    12-bit    |    1-bit   --
  --     5-9     |  "18Kb"   |     2048    |    11-bit    |    1-bit   --
  --     3-4     |  "36Kb"   |     8192    |    13-bit    |    1-bit   --
  --     3-4     |  "18Kb"   |     4096    |    12-bit    |    1-bit   --
  --       2     |  "36Kb"   |    16384    |    14-bit    |    1-bit   --
  --       2     |  "18Kb"   |     8192    |    13-bit    |    1-bit   --
  --       1     |  "36Kb"   |    32768    |    15-bit    |    1-bit   --
  --       1     |  "18Kb"   |    16384    |    14-bit    |    1-bit   --
  -----------------------------------------------------------------------
  constant DEPTH_THAT_REQUIRES_36Kb : positive := 15;

  -- the macro's address width is the requested DEPTH (capped to 15) if MIN_OUT_MUX mode is selected, otherwise it is the specified BR_ADDR_WIDTH
  constant MACRO_DEPTH : positive :=    bo2int(MIN_OUT_MUX) * minimum(15, DEPTH_LOG2)    +     (1-bo2int(MIN_OUT_MUX)) * BR_ADDR_WIDTH ;

  -- Use 36Kb macros if maximum depth is needed; otherwise, use 18Kb
  type macro_size_switch_t is array(boolean) of string(1 to 4);
  constant macro_size_switch : macro_size_switch_t := (true => "36Kb", false => "18Kb");
  constant MACRO_SIZE : string := macro_size_switch( MACRO_DEPTH = DEPTH_THAT_REQUIRES_36Kb );

  -- Define the rest of the constraining constants 
  constant MACRO_WIDTH  : positive := 36 / 2**(minimum(DEPTH_THAT_REQUIRES_36Kb-1,MACRO_DEPTH) - 9);
  constant columns         : positive := 1 + (DATA_WIDTH-1) / MACRO_WIDTH;
  constant rows           : positive := 2**(maximum(DEPTH_LOG2 -MACRO_DEPTH,0));
  constant N_MACROS       : positive := columns * rows;
  constant we_size        : positive := 2**(maximum(11-MACRO_DEPTH,0));
  
  -- Internal signals
  type D_t is array (N_MACROS - 1 downto 0) of std_logic_vector(MACRO_WIDTH -1 downto 0);
  signal DO, DI : D_t;
  signal RDEN, WREN : std_logic_vector(rows - 1 downto 0);
  signal RDADDR, WRADDR : std_logic_vector(MACRO_DEPTH - 1 downto 0);

  signal rsel, wsel   : integer range rows - 1 downto 0 := 0;
  signal dinx, doutx   : std_logic_vector(columns*MACRO_WIDTH - 1 downto 0) := (others => '0');
  

begin
  
  dinx(din'range) <= din;
  dinx(dinx'high downto din'high+1) <= (others => '0');
  dout <= doutx(dout'range);
  
  RDADDR(minimum(RDADDR'left, radd'left) downto 0) <= radd(minimum(RDADDR'left, radd'left) downto 0);
  WRADDR(minimum(WRADDR'left, wadd'left) downto 0) <= wadd(minimum(WRADDR'left, wadd'left) downto 0);
  
  sel_gen : if rows > 1 generate
    rsel <= to_integer(unsigned(radd(DEPTH_LOG2 - 1 downto MACRO_DEPTH)));
    wsel <= to_integer(unsigned(wadd(DEPTH_LOG2 - 1 downto MACRO_DEPTH)));
  end generate;
  sel_gen2 : if rows = 1 generate
    rsel <= 0;
    wsel <= 0;
  end generate;
  
  RDEN <= std_logic_vector(to_unsigned(2**rsel,rows)) when ren = '1' else (others => '0');
  WREN <= std_logic_vector(to_unsigned(2**wsel,rows)) when wen = '1' else (others => '0');
  
  cols_gen : for col in columns - 1 downto 0 generate

    doutx((col+1)*MACRO_WIDTH-1 downto col*MACRO_WIDTH) <= DO(rsel*columns+col);

    rows2_gen : for row in rows - 1 downto 0 generate
      -- this one is left here because it doesnt harm that the Data Input is shared amongst all the BRAMs and it saves logic
      DI(row*columns+col) <= dinx((col+1)*MACRO_WIDTH-1 downto col*MACRO_WIDTH);

      BRAM_SDP_MACRO_inst : BRAM_SDP_MACRO
      generic map (
        BRAM_SIZE => MACRO_SIZE, -- Target BRAM, "18Kb" or "36Kb" 
        DEVICE => "7SERIES", -- Target device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
        WRITE_WIDTH => MACRO_WIDTH,    -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        READ_WIDTH => MACRO_WIDTH,     -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        DO_REG => 0, -- Optional output register (0 or 1)
        INIT_FILE => "NONE",
        SIM_COLLISION_CHECK => "ALL", -- Collision check enable "ALL", "WARNING_ONLY", 
                            -- "GENERATE_X_ONLY" or "NONE"       
        WRITE_MODE => "WRITE_FIRST") -- Specify "READ_FIRST" for same clock or synchronous clocks
                            --  Specify "WRITE_FIRST for asynchrononous clocks on ports
        -- INIT, SRVAL, INIT_xx and INITP_xx declarations not specified ==> all zeroes
      port map (
        DO => DO(row*columns + col),         -- Output read data port, width defined by READ_WIDTH parameter
        DI => DI(row*columns + col),         -- Input write data port, width defined by WRITE_WIDTH parameter
        RDADDR => RDADDR,   -- Input read address, width defined by read port depth
        RDCLK => rclk,       -- 1-bit input read clock
        RDEN => RDEN(row),     -- 1-bit input read port enable
        REGCE => '1',       -- 1-bit input read output register enable
        RST => '0',         -- 1-bit input reset 
        WE => (we_size-1 downto 0 => '1'),-- Input write enable, width defined by write port depth
        WRADDR => WRADDR,   -- Input write address, width defined by write port depth
        WRCLK => wclk,       -- 1-bit input write clock
        WREN => WREN(row));      -- 1-bit input write port enable
    end generate;
  end generate;
  
  
end architecture;