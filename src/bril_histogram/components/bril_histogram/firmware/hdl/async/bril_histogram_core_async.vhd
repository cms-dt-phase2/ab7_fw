----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_core_async - rtl
-- Description: 
-- 
--          Here we generate memory in optimized way: we pack data either 2 counters per words, or 3counters per 2 words, 4 counters per 3 words, or 1 counter per word
--          The data is streamed out without backpressure (32bit wide bus + data valid)
--          THIS IS THE ASYNC VERSION
--
--          The concept here is very simple: input_valid_i is '1' when iteration_id_i/bin_id_i are valid, whereas the increment itself for this moment of time takes ASYNC_INCREMENT_DELAY clock cycles to process (for example for accumulation)
--          We don't care about it as soon as this time is less or equals to BRAM_OUTPUT_DELAY. Otherwise the input_valid is delayed in the wrapper
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- bril package
use work.bril_histogram_package.all;

entity bril_histogram_core_async is
Generic (
    -- memory parameters
    MEMORY_BLOCK_PARAMETERS         : memory_block_parameters_type;
    -- histogram parameters
    NUMBER_OF_BINS                  : natural;
    COUNTER_WIDTH                   : natural;
    INCREMENT_WIDTH                 : natural;
    -- counter width
    ORBIT_BX_COUNTER_WIDTH          : natural;
    -- delay of the bram output
    BRAM_OUTPUT_DELAY               : natural
);
Port(
    -- core signals
    reset_i                         : in std_logic; -- sync reset
    clk_i                           : in std_logic; -- processing clock - has to be BRAM_OUTPUT_DELAY+2 (one to set addres, one to write) times faster then 40Mhz
    -- new histogram
    new_histogram_i                 : in std_logic;
    -- control signals
    valid_i                         : in std_logic;
    iteration_id_i	                : in std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    bin_id_i		                : in std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    -- increment signals  
    increment_overflow_i            : in std_logic;
    increment_i                     : in std_logic_vector(INCREMENT_WIDTH-1 downto 0); -- increment command
    -- memory interface
    ram_addr_o                      : out natural range 0 to MEMORY_BLOCK_PARAMETERS.total_bram_size_async-1; -- ram addres  
    ram_din_o                       : out std_logic_vector(MEMORY_BLOCK_PARAMETERS.bram_word_width-1 downto 0);   -- RAM input data
    ram_we_o                        : out std_logic; -- Write enable
    ram_dout_i                      : in std_logic_vector(MEMORY_BLOCK_PARAMETERS.bram_word_width-1 downto 0);   -- RAM output data
    -- histogram info
    iteration_counter_o             : out std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    increment_overflow_o            : out std_logic;
    overflow_o                      : out std_logic
);
end bril_histogram_core_async;

architecture rtl of bril_histogram_core_async is 

    -- bin counting
    signal iteration_counter            : std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    signal iteration_id_prev	        : std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);

    -- 
    signal waiting_for_data             : std_logic;
    signal time_counter                 : natural range 0 to BRAM_OUTPUT_DELAY;
    
    -- internal memory control signals
    signal sub_word_id                  : natural range 0 to MEMORY_BLOCK_PARAMETERS.counters_per_word-1;
    signal counter_current_data         : std_logic_vector(COUNTER_WIDTH-1 downto 0);   -- RAM output data    
    signal counter_updated_data         : std_logic_vector(COUNTER_WIDTH-1 downto 0);   -- RAM input data 
  
begin
    
    --====================================================================--
    -- generating new histogram and doing iteration counter
    --====================================================================--
    -- iteration
    process (clk_i)
    begin
        if rising_edge(clk_i) then
            if reset_i = '1' then
                iteration_counter <= (others => '0');
                iteration_id_prev <= (others => '0');
            else
                -- 
                if new_histogram_i = '1' then
                    iteration_counter <= (others => '0'); 
                    iteration_id_prev <= (others => '0'); -- to make sure that it's in sync
                else
                    -- if we received new iteration data
                    if valid_i = '1' then
                        -- iteration counter
                        if iteration_id_i /= iteration_id_prev then
                            iteration_counter <= iteration_counter + (iteration_id_i-iteration_id_prev);
                        end if;
                        -- store previoud iteration id
                        iteration_id_prev <= iteration_id_i;
                    end if;
                end if;
            end if;
        end if;
    end process;
    -- output iteration counter to the streamer
    iteration_counter_o <= iteration_counter;
    
    --====================================================================--
    -- fsm to request the current counter value and update it
    --====================================================================--
    -- fsm
    process (clk_i)            
    begin
        if rising_edge(clk_i) then

            -- reset of data in
            if reset_i = '1' then
                -- memory signals
                counter_updated_data <= (others => '0');
                ram_addr_o <= 0;
                ram_we_o <= '0';
                sub_word_id <= 0;
                --
                waiting_for_data <= '0';
                time_counter <= 0;
                
                -- overflow of the histogram
                increment_overflow_o <= '0';
                overflow_o <= '0';
            else          
                -- always reset din and wr en
                counter_updated_data <= (others => '0');
                ram_we_o <= '0';  
                
                -- reset overflow
                if new_histogram_i = '1' then
                    increment_overflow_o <= '0';
                    overflow_o <= '0';
                end if;
                    
                -- free and ready to accept new iteration
                if waiting_for_data = '0' then
                
                    -- new input (iteration/bin id) received
                    if valid_i = '1' then
                        if bin_id_i < NUMBER_OF_BINS then -- make sure that the bin id is correct
                            -- memory address to read
                            sub_word_id <= to_integer(unsigned(bin_id_i)) rem MEMORY_BLOCK_PARAMETERS.counters_per_word;
                            ram_addr_o <= MEMORY_BLOCK_PARAMETERS.HEADER_SIZE + to_integer(unsigned(bin_id_i))/MEMORY_BLOCK_PARAMETERS.counters_per_word;
                            -- 
                            time_counter <= 0;
                            waiting_for_data <= '1';
                        end if;
                    end if;
                    
                else
                
                    -- data is ready
                    if time_counter = BRAM_OUTPUT_DELAY then
                   
                        -- we can perform write
                        if increment_i > not(counter_current_data) then -- fast way check the overflow
                            counter_updated_data <= (others => '1');
                            overflow_o <= '1';
                        else
                            counter_updated_data <= counter_current_data + increment_i;
                        end if;
                        -- sync increment overfloe
                        increment_overflow_o <= increment_overflow_i;
                        -- write enable
                        ram_we_o <= '1';
                        -- leave waiting state
                        waiting_for_data <= '0';
                        
                    else
                        -- still have time - just increment
                        time_counter <= time_counter + 1;
                    end if;
                    
                end if; -- end waiting for increment
            end if;       
        end if;
    end process;
    
    --====================================================================--
    -- This part of the code packs counter data
    --====================================================================--
    -- packing density
    -- if counter width is w <= 16 - we pack 2 words per memory cell (will require 2x36k brams)
    -- if counter width is 16 < w <= 32 - only one word per memory cell (will require 4x36k brams)
    gen_2words_per_1cell: if COUNTER_WIDTH <= 16 generate   
        -- best we can do here is to split in 2x36k memory blocks
        -- two words per cell
      
        -- assign current data
        counter_current_data <= ram_dout_i(COUNTER_WIDTH-1 + sub_word_id*16 downto sub_word_id*16);
        -- write update counter
        ram_din_o <=    (ram_dout_i(31 downto 16) & append_zeros_left(counter_updated_data,16)) when sub_word_id = 0 else
                        (append_zeros_left(counter_updated_data,16) & ram_dout_i(15 downto 0)) when sub_word_id = 1 else
                        (others => '0');
        
    end generate gen_2words_per_1cell;
        
    gen_1word_per_1cell: if COUNTER_WIDTH > 16 and COUNTER_WIDTH <= 32 generate
        -- 1 word per 32bit cell
        
        -- assign current data
        counter_current_data <= ram_dout_i(COUNTER_WIDTH-1 downto 0);
        -- write update counter
        ram_din_o <= append_zeros_left(counter_updated_data,32);
           
    end generate gen_1word_per_1cell;
    
end rtl;
