-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Duplicated segments filter
---- File       : primitive_duplicated_shifting_filter.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-07
-------------------------------------------------------------------------------
---- Description: Rejects duplicated PRIMITIVES based on comparing different
----              primitive field values.
----              It uses a shifting mechanism with MEM_ELEMENTS positions 
----              to get in 1 cycle the resulting comparison.
----              Additionally, it spends one clock cycle (in the mean time
----              around BX = 0) to clean the internal memories to avoid
----              refusing segments with the same TDC values but belonging
----              to different orbits.
----
----              A new middle registration step has been added to allow 
----              clock fitting during synthesis.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.filters_pkg.all;

entity PRIMITIVE_DUPLICATED_SHIFTING_FILTER is
  generic (MEM_ELEMENTS : integer := 8);
  port (
    clk           : in  std_logic;
    rst           : in  std_logic;
    primFilterIn  : in  PrimitiveFilterIn_t;
    primFilterOut : out PrimitiveFilterOut_t
  );
end entity;

architecture Behavioural of PRIMITIVE_DUPLICATED_SHIFTING_FILTER is

  constant BX_ZERO : std_logic_vector(11 downto 0) := (others => '0');
  
  constant SEGMENT_HITS_SIZE : integer := DTHIT_SIZE * HitsSegment_t'length;
  constant LATCOMB_SIZE      : integer := 
        DTSegMainParams_t.laterality_combination'length;

  constant DATA_SIZE : integer := SEGMENT_HITS_SIZE  + LATCOMB_SIZE;
  constant NO_DUPS   : std_logic_vector(MEM_ELEMENTS - 1 downto 0) 
                     := (others => '0');

  subtype seghits_latcomb_slv is std_logic_vector(DATA_SIZE - 1 downto 0);
  type seghits_latcomb_shift_reg_t is array (MEM_ELEMENTS - 1 downto 0) 
                                   of seghits_latcomb_slv;

  -- Aliases
  signal main_seg0     : DTSegMainParams_t;
  signal main_seg1     : DTSegMainParams_t;
  signal data_in0      : std_logic_vector(DATA_SIZE - 1 downto 0);
  signal data_in1      : std_logic_vector(DATA_SIZE - 1 downto 0);
  --
  signal main_seg_aux0 : DTSegMainParams_t;
  signal main_seg_aux1 : DTSegMainParams_t;
  signal data_aux0     : std_logic_vector(DATA_SIZE - 1 downto 0);
  signal data_aux1     : std_logic_vector(DATA_SIZE - 1 downto 0);
  
  -- Auxiliary signals
  signal enable             : std_logic;
  signal send_data          : std_logic;
  signal clear_mems         : std_logic;
  signal read_new_data      : std_logic;
  signal there_are_dups     : std_logic;
  signal write_data_on_fifo : std_logic;
  signal bx_counter         : std_logic_vector(11 downto 0);
  signal duplicated0        : std_logic_vector(MEM_ELEMENTS - 1 downto 0);
  signal duplicated1        : std_logic_vector(MEM_ELEMENTS - 1 downto 0);
  
  -- Internal registers
  signal valid_data_reg     : std_logic;
  signal primitive_reg      : DTPrimitive_t;
  signal data_reg0          : seghits_latcomb_shift_reg_t;
  signal data_reg1          : seghits_latcomb_shift_reg_t;
  signal bx_counter_lsb     : std_logic;
  signal bx_counter_changed : std_logic;
  signal duplicated_reg0    : std_logic_vector(MEM_ELEMENTS - 1 downto 0);
  signal duplicated_reg1    : std_logic_vector(MEM_ELEMENTS - 1 downto 0);
  signal common_dups        : std_logic_vector(MEM_ELEMENTS - 1 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  primFilterOut.primitive  <= primitive_reg;
  primFilterOut.fifoIn_rd  <= read_new_data;
  primFilterOut.fifoOut_wr <= write_data_on_fifo;

  ---------------------------------------------------------------
  -- Useful aliases 
  --
  main_seg0  <= primFilterIn.primitive.paired_segments(0).main_params;
  main_seg1  <= primFilterIn.primitive.paired_segments(1).main_params;
  data_in0   <= slv_from_dtSegHits_latComb( main_seg0 );
  data_in1   <= slv_from_dtSegHits_latComb( main_seg1 );

  main_seg_aux0  <= primitive_reg.paired_segments(0).main_params;
  main_seg_aux1  <= primitive_reg.paired_segments(1).main_params;
  data_aux0      <= slv_from_dtSegHits_latComb( main_seg_aux0 );
  data_aux1      <= slv_from_dtSegHits_latComb( main_seg_aux1 );

  bx_counter <= primFilterIn.bx_counter;

  ---------------------------------------------------------------
  -- Combinatory logic
  --
  clear_mems <= '1' when bx_counter = BX_ZERO and bx_counter_changed = '1' 
                    else '0';

  enable        <= not primFilterIn.fifoOut_full and not clear_mems;
  read_new_data <= not primFilterIn.fifoIn_empty and enable;
  
  COMPARISIONS : for i in 0 to MEM_ELEMENTS - 1 generate
    --
    -- These signals will be registered to allow the synthesizer to fit 
    -- timing constraints. The length of the concatenated logic was preventing
    -- it. This forces to add an additional registration step on many signal
    -- and to perform a logic refactoring.
    --
    duplicated0(i) <= 
          segmentHits_and_latComb_slv_are_equal(data_in0, data_reg0(i));

    duplicated1(i) <= 
          segmentHits_and_latComb_slv_are_equal(data_in1, data_reg1(i));
  end generate;
  --
  -- If both segments in current primitive are equal to any other previously 
  -- stored, this one is considered duplicated.
  --
  common_dups    <= duplicated_reg0 and duplicated_reg1;
  there_are_dups <= '1' when common_dups /= NO_DUPS else '0';
                        
  send_data          <= valid_data_reg and (not there_are_dups);
  write_data_on_fifo <= send_data and enable;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst, primFilterIn, clear_mems, write_data_on_fifo)
  begin
    if clk = '1' and clk'event then
      --------------------------------------------
      -- BX value change logic
      --------------------------------------------
      if rst = '1' then 
        bx_counter_lsb     <= '0';
        bx_counter_changed <= '0';
      else 
        bx_counter_lsb <= bx_counter(0);

        -- BX counter value change detector.
        bx_counter_changed <= bx_counter_lsb and (not bx_counter(0));
      end if;
    
      --------------------------------------------
      -- General registers
      --------------------------------------------
      if rst = '1' then 
        valid_data_reg  <= '0';
        primitive_reg   <= DT_PRIMITIVE_NULL;
        duplicated_reg0 <= (others => '0');
        duplicated_reg1 <= (others => '0');

      elsif enable = '1' then
        valid_data_reg  <= not primFilterIn.fifoIn_empty;
        primitive_reg   <= primFilterIn.primitive;
        duplicated_reg0 <= duplicated0;
        duplicated_reg1 <= duplicated1;

      end if;
    
      --------------------------------------------
      -- Data memories logic
      --------------------------------------------
      if rst = '1' or clear_mems = '1' then 
        data_reg0 <= (others => (others => '0'));
        data_reg1 <= (others => (others => '0'));

      elsif write_data_on_fifo = '1' then 
        data_reg0 <= data_reg0(MEM_ELEMENTS - 2 downto 0) & data_aux0;
        data_reg1 <= data_reg1(MEM_ELEMENTS - 2 downto 0) & data_aux1;
      
      end if;
    end if;
  end process;

end architecture;
