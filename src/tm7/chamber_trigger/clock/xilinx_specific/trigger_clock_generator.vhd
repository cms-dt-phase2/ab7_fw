-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Memory components
---- Design unit: Single port RAM
---- File       : trigger_clock_generator.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-02
-------------------------------------------------------------------------------
---- Description: Wrapper for an additional clock manager in order to isolate
----              trigger modules clock management from the rest of the 
----              design.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
Library UNIMACRO;
use UNIMACRO.vcomponents.all;
--
library UNISIM;
use UNISIM.vcomponents.all;

entity TRIGGER_CLOCK_GENERATOR is
  port (
    clk_40_i_buffered : in  std_logic;
    rst_i             : in  std_logic;
    rst_mmcm          : in  std_logic;
    stopped           : out std_logic;
    locked            : out std_logic;
    clk40_o           : out std_logic;     -- 40     MHz
    clk1_o            : out std_logic;     -- 171.11 MHz
    clk2_o            : out std_logic;     -- 192.5  MHz
    clk3_o            : out std_logic;     -- 220.00 MHz
    rst40_o           : out std_logic;
    rst1_o            : out std_logic;
    rst2_o            : out std_logic;
    rst3_o            : out std_logic
  );
end entity;

architecture RTL of TRIGGER_CLOCK_GENERATOR is
  signal clk40_int       : std_logic;
  signal locked_int      : std_logic;
  signal phshift_ena     : std_logic;
  signal clk_feedback    : std_logic;
  signal clk40_generated : std_logic;
  
  signal clkout1     : std_logic;
  signal clkout2     : std_logic;
  signal clkout3     : std_logic;
  signal clkout1_int : std_logic;
  signal clkout2_int : std_logic;
  signal clkout3_int : std_logic;

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  clk40_o <= clk40_int;
  clk1_o  <= clkout1_int;
  clk2_o  <= clkout2_int;
  clk3_o  <= clkout3_int;
  --
  locked  <= locked_int;

  ---------------------------------------------------------------
  -- Reset signals synchronizers
  --
  process(clk40_int)
  begin
    if clk40_int = '1' and clk40_int'event then
      rst40_o <= rst_i or not locked_int;
    end if;
  end process;

  process(clkout1_int)
  begin
    if clkout1_int = '1' and clkout1_int'event then
      rst1_o <= rst_i or not locked_int;
    end if;
  end process;
  
  process(clkout2_int)
  begin
    if clkout2_int = '1' and clkout2_int'event then
      rst2_o <= rst_i or not locked_int;
    end if;
  end process;
  
  process(clkout3_int)
  begin
    if clkout3_int = '1' and clkout3_int'event then
      rst3_o <= rst_i or not locked_int;
    end if;
  end process;
  
  ---------------------------------------------------------------
  -- Clock buffers.
  --
  TRG_CLK40_BUFG : BUFG port map(i => clk40_generated, o => clk40_int);
  TRG_CLK1_BUFG  : BUFG port map(i => clkout1,         o => clkout1_int);
  TRG_CLK2_BUFG  : BUFG port map(i => clkout2,         o => clkout2_int);
  TRG_CLK3_BUFG  : BUFG port map(i => clkout3,         o => clkout3_int);
  
  ---------------------------------------------------------------
  -- Clock manager.
  --
  MMCME2_ADV_INST : MMCME2_ADV
  generic map (
    -- CLKIN_PERIOD: Input clock period in ns to ps resolution 
    -- (i.e. 33.333 is 30 MHz).
    CLKIN1_PERIOD    => 25.0,
    CLKIN2_PERIOD    => 25.0,
    -- Multiply for all CLKOUT (2.000-64.000)
    CLKFBOUT_MULT_F  => 38.5,
    -- Divide amount for CLKOUT0 (1.000-128.000)
    CLKOUT0_DIVIDE_F => 38.5,
    ---------------------------------------------------------
    -- CLKOUT1_DIVIDE - CLKOUT6_DIVIDE: Divide amount for CLKOUT (1-128)
    CLKOUT1_DIVIDE   => 9,
    CLKOUT2_DIVIDE   => 8,
    CLKOUT3_DIVIDE   => 7,
    CLKOUT4_DIVIDE   => 1,
    CLKOUT5_DIVIDE   => 1,
    CLKOUT6_DIVIDE   => 1
  )
  port map (
    ---------------------------------------------------------
    -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
    CLKOUT0      => clk40_generated,       -- 1-bit output: CLKOUT0
--     CLKOUT0B     => CLKOUT0B,         -- 1-bit output: Inverted CLKOUT0
    CLKOUT1      => clkout1,          -- 1-bit output: CLKOUT1
--     CLKOUT1B     => CLKOUT1B,         -- 1-bit output: Inverted CLKOUT1
    CLKOUT2      => clkout2,          -- 1-bit output: CLKOUT2
--     CLKOUT2B     => CLKOUT2B,         -- 1-bit output: Inverted CLKOUT2
    CLKOUT3      => clkout3,          -- 1-bit output: CLKOUT3
--     CLKOUT3B     => CLKOUT3B,         -- 1-bit output: Inverted CLKOUT3
--     CLKOUT4      => CLKOUT4,          -- 1-bit output: CLKOUT4
--     CLKOUT5      => CLKOUT5,          -- 1-bit output: CLKOUT5
--     CLKOUT6      => CLKOUT6,          -- 1-bit output: CLKOUT6
    ---------------------------------------------------------
    -- DRP Ports: 16-bit (each) output: Dynamic reconfiguration ports
--     DO           => DO,               -- 16-bit output: DRP data
--     DRDY         => DRDY,             -- 1-bit output: DRP ready
    ---------------------------------------------------------
    -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
    CLKFBOUT     => clk_feedback,     -- 1-bit output: Feedback clock
--     CLKFBOUTB    => CLKFBOUTB,        -- 1-bit output: Inverted CLKFBOUT
    ---------------------------------------------------------
    -- Status Ports: 1-bit (each) output: MMCM status ports
--     CLKFBSTOPPED => CLKFBSTOPPED,     -- 1-bit output: Feedback clock stopped
    CLKINSTOPPED => stopped,          -- 1-bit output: Input clock stopped
    LOCKED       => locked_int,       -- 1-bit output: LOCK
    ---------------------------------------------------------
    -- Clock Inputs: 1-bit (each) input: Clock inputs
    CLKIN1       => clk_40_i_buffered,-- 1-bit input: Primary clock
    CLKIN2       => clk_40_i_buffered,-- 1-bit input: Secondary clock
    ---------------------------------------------------------
    -- Control Ports: 1-bit (each) input: MMCM control ports
    --
    -- 1-bit input: Clock select, High=CLKIN1 Low=CLKIN2
    CLKINSEL     => '1',         
    PWRDWN       => '0',              -- 1-bit input: Power-down
    RST          => rst_mmcm,         -- 1-bit input: Reset
    ---------------------------------------------------------
    -- DRP Ports: 7-bit (each) input: Dynamic reconfiguration ports
    DADDR        => "0000000",        -- 7-bit input: DRP address
    DCLK         => '0',              -- 1-bit input: DRP clock
    DEN          => '0',              -- 1-bit input: DRP enable
    DI           => x"0000",          -- 16-bit input: DRP data
    DWE          => '0',              -- 1-bit input: DRP write enable
    ---------------------------------------------------------
    -- Dynamic Phase Shift Ports: 1-bit (each) input: 
    -- Ports used for dynamic phase shifting of the outputs
    PSCLK        => clk40_int,        -- 1-bit input: Phase shift clock
    PSEN         => '0',              -- 1-bit input: Phase shift enable
--     -- 1-bit input: Phase shift increment/decrement
    PSINCDEC     => '0',         
    -- Ports used for dynamic phase shifting of the outputs
--     PSDONE       => psdone,           -- 1-bit output: Phase shift done
    ---------------------------------------------------------
    -- Feedback Clocks: 1-bit (each) input: Clock feedback ports
    CLKFBIN      => clk_feedback      -- 1-bit input: Feedback clock
  );

end architecture;
