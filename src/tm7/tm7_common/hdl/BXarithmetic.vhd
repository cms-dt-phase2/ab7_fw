library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.top_decl.all; -- for LHC_BUNCH_COUNT

package BXarithmetic is

  function BXplus ( L, R: unsigned ) return unsigned;
  function BXplus ( L, R: std_logic_vector ) return std_logic_vector;
  function BXplus ( L: std_logic_vector; R: unsigned ) return std_logic_vector;
  
  function BXminus ( L, R: unsigned ) return unsigned;
  function BXminus ( L, R: std_logic_vector ) return std_logic_vector;
  function BXminus ( L: std_logic_vector; R: unsigned ) return std_logic_vector;

end package;

package body BXarithmetic is

  function BXplus ( L, R: unsigned ) return unsigned is
    variable res : unsigned(12 downto 0);
  begin
    res := ('0'&L) + ('0'&R);
    if res >= LHC_BUNCH_COUNT then res := res - LHC_BUNCH_COUNT ;
    end if;
    return res(11 downto 0);
  end function;
  
  function BXplus ( L, R: std_logic_vector ) return std_logic_vector is
  begin
    return std_logic_vector(BXplus(unsigned(L), unsigned(R)));
  end function;
  
  function BXplus ( L: std_logic_vector; R: unsigned ) return std_logic_vector is
  begin
    return std_logic_vector(BXplus(unsigned(L), R));
  end function;
  
  function BXminus ( L, R: unsigned ) return unsigned is
    variable res : unsigned(12 downto 0);
  begin
    res := '0'&L ;
    if L < R then
      res := ('0'&L) + LHC_BUNCH_COUNT ;
    end if;
    res := res - ('0'&R);
    return res(11 downto 0);
  end function;
  
  function BXminus ( L, R: std_logic_vector ) return std_logic_vector is
  begin
    return std_logic_vector(BXminus(unsigned(L), unsigned(R)));
  end function;

  function BXminus ( L: std_logic_vector; R: unsigned ) return std_logic_vector is
  begin
    return std_logic_vector(BXminus(unsigned(L), R));
  end function;
  
end package body;
