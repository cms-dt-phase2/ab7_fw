set_false_path -from [get_pins ttc/clocks/rsto_40_*/C] -to [get_pins -hierarchical -filter {NAME =~ trigger/*}]
set_false_path -from [get_pins ttc/clocks/rsto_40_*/C] -to [get_pins -hierarchical -filter {NAME =~ trigger/*/RST}]
# set_false_path -from [get_pins ttc/clocks/rsto_40_*/C] -to [get_pins -hierarchical -filter {NAME =~ trigger/*/R}]



#set_false_path -from [get_pins {trigger/ipbusreg/w_gen[*].wsync/q_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ trigger/*/FIFO_DUALCLOCK_MACRO_INST/*/DIP[*]}]
#set_false_path -from [get_pins {trigger/ipbusreg/w_gen[*].wsync/q_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ trigger/*/QENHANC_FILT_INST/*/reg_reg[*]/D}]
#set_false_path -from [get_pins {trigger/ipbusreg/w_gen[*].wsync/q_reg[*]*/C}] -to [get_pins -hierarchical -filter {NAME =~ trigger/*/FIFO_DUALCLOCK_MACRO_INST/bl.fifo_36_inst_bl_1.fifo_36_bl_1/DI[*]}]
#set_false_path -from [get_pins {trigger/ipbusreg/w_gen[*].wsync/q_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ trigger/CHAMBER_TRG/INST_SLPHI_MATCH/PRIM_PATH_CALC/*/D}]
#
##set_false_path -from [get_pins {trigger/ipbusreg/w_gen[*].wsync/q_reg[*]/C}] -to [get_pins {trigger/CHAMBER_TRG/PHI_MATCHING/FSM_onehot_reg_reg[*][*]/CE}]
#set_false_path -from [get_pins {trigger/ipbusreg/w_gen[*].wsync/q_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ trigger/CHAMBER_TRG/INST_SLPHI_MATCH/PRIM_PATH_CALC/ENA_DELAY/*/D}]
# 
# set_false_path -from [get_pins {trigger/ipbusreg/w_gen[*].wsync/q_reg[*]/C}] -to [get_pins -hierarchical -filter {NAME =~ trigger/CHAMBER_TRG/INST_SLPHI_MATCH/PRIM_PATH_CALC/ENA_DELAY/*/D}]
