--============================================================================--
--#######################   Module Information   #############################--
--============================================================================--
--
-- Company          : CERN (EP-ESE-BE)
-- Engineer         : Julian Mendez <julian.mendez@cern.ch>
--
-- Project Name     : GBT-SC module (IC and EC fields)
-- Module Name      : IC_tx
--
-- Language         : VHDL
--
-- Target Device    : Device agnostic
-- Tool version     : -
--
-- Version          : 1.0
--
-- Description      : GBTx internal control - TX management
--
-- Versions history : DATE      VERS.   AUTHOR      DESCRIPTION
--
--                    05/04/17  1.0     J. Mendez   First .vhd module definition
--
-- Add. Comments    : -
--                                                                              
--============================================================================--
--############################################################################--
--============================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--============================================================================--
--#############################   Entity   ###################################--
--============================================================================--
entity ic_tx is
    generic (
        g_FIFO_DEPTH        : integer := 10
    );
   port (
        -- Clock and reset
        tx_clk_i            : in std_logic;
        reset_i             : in std_logic;
        
        -- Status
        tx_ready_o          : out std_logic;
        
        -- Configuration
        GBTx_address_i      : in std_logic_vector(7 downto 0);
        Register_addr_i     : in std_logic_vector(15 downto 0);
        nb_to_be_read_i     : in std_logic_vector(15 downto 0);
        
        -- Internal FIFO
        wr_i                : in std_logic;
        data_i              : in std_logic_vector(7 downto 0);
        
        -- FSM Control
        start_read          : in std_logic;
        start_write         : in std_logic;

        -- IC lines
        tx_data_o           : out std_logic_vector(1 downto 0)
   );   
end ic_tx;

--============================================================================--
--############################   Architecture   ##############################--
--============================================================================--
architecture behaviour of ic_tx is
    
    -- Constant
    constant DEL_PACKET     : std_logic_vector(7 downto 0)    := "01111110";
    
    -- Types
    type reg8_arr_t  is array(integer range <>) of std_logic_vector(7 downto 0);
    type hdlcstate_t is (s0_waitForStart, 
                            s2_sendSOF, 
                            s3_sendCommand, 
                            s4_sendCRC, 
                            s5_sendEOF);
    
    -- Signals
    signal word_cnt         : integer range 0 to g_FIFO_DEPTH;
    signal wr_ptr           : integer range 7 to (8+g_FIFO_DEPTH);
    signal word_cnt_v       : std_logic_vector(15 downto 0);

    signal fifo             : reg8_arr_t(g_FIFO_DEPTH+8 downto 0);
    
    signal cmd_len          : integer range 0 to (8+g_FIFO_DEPTH);
    signal tx_parity        : std_logic_vector(7 downto 0);
    signal parity           : std_logic_vector(7 downto 0);
        
    
    signal hdlc_state       : hdlcstate_t := s0_waitForStart;
    
    
    signal start_write_s0   : std_logic    := '0';
    signal start_read_s0    : std_logic    := '0';
    signal reset_fifo       : std_logic := '0';
        
begin                 --========####   Architecture Body   ####========-- 

    -- TX Fifo
    wr_fifo: process(tx_clk_i, reset_i)
    
    begin
    
        if reset_i = '1' or reset_fifo = '1' then
            word_cnt    <= 0;
            wr_ptr      <= 7;
            cmd_len     <= 7;
            tx_parity   <= (others => '0');

        elsif rising_edge(tx_clk_i) then

            if start_write = '1' then
                fifo(0)     <= "00000000";
                fifo(1)     <= GBTx_address_i(6 downto 0) & '0';
                fifo(2)     <= "00000001";
                fifo(3)     <= (word_cnt_v(7 downto 0));
                fifo(4)     <= (word_cnt_v(15 downto 8));
                fifo(5)     <= (Register_addr_i(7 downto 0));
                fifo(6)     <= (Register_addr_i(15 downto 8));
            
                cmd_len             <= wr_ptr;
                
            elsif start_read = '1' then
                fifo(0)     <= "00000000";
                fifo(1)     <= GBTx_address_i(6 downto 0) & '1';
                fifo(2)     <= "00000001";
                fifo(3)     <= (nb_to_be_read_i(7 downto 0));
                fifo(4)     <= (nb_to_be_read_i(15 downto 8));
                fifo(5)     <= (Register_addr_i(7 downto 0));
                fifo(6)     <= (Register_addr_i(15 downto 8));
            
                cmd_len      <= 7;
                
            elsif wr_i = '1' then

                fifo(wr_ptr)        <= data_i;
                tx_parity           <= tx_parity xor data_i;
                word_cnt            <= word_cnt + 1;
                wr_ptr              <= wr_ptr + 1;

            end if;

        end if;        

    end process;
    
    word_cnt_v <= std_logic_vector(to_unsigned((word_cnt), 16));

    -- TX Serializer
    hdlcproc: process(tx_clk_i, reset_i)

        variable offset_pos         : integer range 0 to 8;
        variable reg_pos            : integer range 0 to 8+g_FIFO_DEPTH;
        variable high_lvl_cnter     : integer range 0 to 6;

    begin
    
        if reset_i = '1' then            
            offset_pos          := 0;
            high_lvl_cnter      := 0;
            
            reset_fifo          <= '1';
            tx_ready_o          <= '0';
            tx_data_o           <= "11";
            
            hdlc_state          <= s0_waitForStart;
            reg_pos             := 0;
            
        elsif rising_edge(tx_clk_i) then
            
            reset_fifo <= '0';
            tx_ready_o <= '0';
            
            case hdlc_state is
            
                when s0_waitForStart    =>    
                    
                    -- Send idle
                    reg_pos             := 0;
                    tx_ready_o          <= '1';

                    tx_data_o(1)        <= '1';
                    tx_data_o(0)        <= '1';

                    -- Check start signal
                    if start_write = '1' then

                        tx_ready_o <= '0';

                        -- Compute the parity word
                        parity <=
                            (   
                                tx_parity                       xor 
                                word_cnt_v(7 downto 0)          xor 
                                word_cnt_v(15 downto 8)         xor 
                                Register_addr_i(7 downto 0)     xor 
                                Register_addr_i(15 downto 8)    xor 
                                "00000001"
                            );
                        
                        tx_data_o(1)        <= DEL_PACKET(0);
                        tx_data_o(0)        <= DEL_PACKET(1);
                        offset_pos          := 2;
                        
                        hdlc_state <= s2_sendSOF;
                                                    
                    elsif start_read = '1' then
                                                    
                        tx_ready_o <= '0';
                        
                        -- Compute the parity word
                        parity <= 
                            (   
                                nb_to_be_read_i(7 downto 0)     xor 
                                nb_to_be_read_i(15 downto 8)    xor 
                                Register_addr_i(7 downto 0)     xor 
                                Register_addr_i(15 downto 8)    xor 
                                "00000001"
                            );
                            
                        tx_data_o(1)        <= DEL_PACKET(0);
                        tx_data_o(0)        <= DEL_PACKET(1);
                        offset_pos          := 2;

                        hdlc_state <= s2_sendSOF;
                        
                    end if;

                when s2_sendSOF         =>
                    -- Data have been copied into the vector buffer
                    --      -> The fifo can be reseted

                    -- Send delimiter without bit stuffing
                    tx_data_o(1)     <= DEL_PACKET(offset_pos);
                    tx_data_o(0)     <= DEL_PACKET(offset_pos+1);

                    offset_pos      := offset_pos + 2;

                    -- Delimiter has been fully sent?
                    if offset_pos > 7 then    
                        offset_pos  := 0;
                        hdlc_state  <= s3_sendCommand;
                    end if;

                when s3_sendCommand     =>    
                                
                
                    -- Enough data to send 2 bits?
                    if offset_pos < 7 then
                    
                        -- (MSB) Bit stuffing?
                        if high_lvl_cnter >= 5 then
                        
                            tx_data_o(1)        <= '0';
                            high_lvl_cnter     := 0;
                        
                        -- (MSB) Send data
                        else
                        
                            tx_data_o(1)        <= fifo(reg_pos)(offset_pos);
                            if fifo(reg_pos)(offset_pos) = '1' then
                                high_lvl_cnter := high_lvl_cnter + 1;
                            else
                                high_lvl_cnter := 0;
                            end if;
                            
                            offset_pos         := offset_pos + 1;
                        
                        end if;
                    
                        -- (LSB) Bit stuffing?
                        if high_lvl_cnter >= 5 then
                        
                            tx_data_o(0)        <= '0';
                            high_lvl_cnter     := 0;
                        
                        -- (LSB) Send data
                        else
                        
                            tx_data_o(0)        <= fifo(reg_pos)(offset_pos);
                            if fifo(reg_pos)(offset_pos) = '1' then
                                high_lvl_cnter := high_lvl_cnter + 1;
                            else
                                high_lvl_cnter := 0;
                            end if;
                            
                            offset_pos         := offset_pos + 1;
                        
                        end if;
                        
                        -- End of command?
                        if offset_pos = 8 then
                        
                            offset_pos      := 0;
                            
                            if reg_pos < (cmd_len-1) then
                                reg_pos         := reg_pos + 1;
                            else
                                hdlc_state      <= s4_sendCRC;
                            end if;
                            
                        end if;
                    
                    -- 1 bit still have to been sent, then CRC
                    elsif offset_pos < 8 then
                    
                        -- (MSB) Bit stuffing?
                        if high_lvl_cnter >= 5 then
                        
                            tx_data_o(1)     <= '0';
                            tx_data_o(0)     <= fifo(reg_pos)(7);
                            if fifo(reg_pos)(offset_pos) = '1' then
                                high_lvl_cnter     := high_lvl_cnter + 1;
                            else
                                high_lvl_cnter     := 0;
                            end if;
                            
                            offset_pos      := 0;
                            
                            if reg_pos < (cmd_len-1) then
                                reg_pos         := reg_pos + 1;
                            else
                                hdlc_state      <= s4_sendCRC;
                            end if;
                            
                        -- (MSB) Send data
                        else
                        
                            tx_data_o(1)        <= fifo(reg_pos)(7);
                            if fifo(reg_pos)(offset_pos) = '1' then
                                high_lvl_cnter := high_lvl_cnter + 1;
                            else
                                high_lvl_cnter := 0;
                            end if;
                        
                            -- (LSB) Bit stuffing?
                            if high_lvl_cnter >= 5 then
                                tx_data_o(0)     <= '0';
                                high_lvl_cnter  := 0;

                            elsif reg_pos < (cmd_len-1) then
                                
                                reg_pos         := reg_pos + 1;
                                
                                tx_data_o(0)     <= fifo(reg_pos)(0);
                                if fifo(reg_pos)(0) = '1' then
                                    high_lvl_cnter := high_lvl_cnter + 1;
                                else
                                    high_lvl_cnter := 0;
                                end if;
                                
                                offset_pos      := 1;
                                    
                            else
                                tx_data_o(0)     <= parity(0);
                                if parity(0) = '1' then
                                    high_lvl_cnter := high_lvl_cnter + 1;
                                else
                                    high_lvl_cnter := 0;
                                end if;

                                offset_pos      := 1;    
                                hdlc_state      <= s4_sendCRC;
                            
                            end if;
                            
                        end if;
                    
                    end if;
                
                when s4_sendCRC             =>
                
                    -- Enough data to send 2 bits?
                    if offset_pos < 7 then
                    
                        -- (MSB) Bit stuffing?
                        if high_lvl_cnter >= 5 then
                        
                            tx_data_o(1)        <= '0';
                            high_lvl_cnter     := 0;
                        
                        -- (MSB) Send data
                        else
                        
                            tx_data_o(1)         <= parity(offset_pos);
                            if parity(offset_pos) = '1' then
                                high_lvl_cnter := high_lvl_cnter + 1;
                            else
                                high_lvl_cnter := 0;
                            end if;
                            
                            offset_pos          := offset_pos + 1;
                        
                        end if;
                    
                        -- (LSB) Bit stuffing?
                        if high_lvl_cnter >= 5 then
                        
                            tx_data_o(0)        <= '0';
                            high_lvl_cnter     := 0;
                        
                        -- (LSB) Send data
                        else
                        
                            tx_data_o(0)        <= parity(offset_pos);
                            if parity(offset_pos) = '1' then
                                high_lvl_cnter := high_lvl_cnter + 1;
                            else
                                high_lvl_cnter := 0;
                            end if;
                            
                            offset_pos      := offset_pos + 1;
                        
                        end if;
                        
                        -- End of command?
                        if offset_pos = 8 then
                        
                            offset_pos      := 0;
                            reset_fifo      <= '1';
                            hdlc_state      <= s5_sendEOF;
                            
                        end if;
                    
                    -- 1 bit still have to been sent, then CRC
                    elsif offset_pos < 8 then
                    
                        -- (MSB) Bit stuffing?
                        if high_lvl_cnter >= 5 then
                        
                            tx_data_o(1)     <= '0';
                            tx_data_o(0)     <= parity(offset_pos);
                            
                            if parity(offset_pos) = '1' then
                                high_lvl_cnter     := 0;
                            end if;
                            
                            offset_pos      := 0;
                            reset_fifo      <= '1';
                            hdlc_state      <= s5_sendEOF;
                            
                        -- (MSB) Send data
                        else
                        
                            tx_data_o(1)        <= parity(offset_pos);
                            if parity(offset_pos) = '1' then
                                high_lvl_cnter := high_lvl_cnter + 1;
                            else
                                high_lvl_cnter := 0;
                            end if;
                        
                            -- (LSB) Bit stuffing?
                            if high_lvl_cnter >= 5 then
                                tx_data_o(0)     <= '0';
                                
                                high_lvl_cnter  := 0;
                                offset_pos      := 0;
                                reset_fifo      <= '1';
                                hdlc_state      <= s5_sendEOF;

                            else
                                
                                tx_data_o(0)     <= DEL_PACKET(0);
                                offset_pos      := 1;
                                reset_fifo      <= '1';
                                hdlc_state      <= s5_sendEOF;
                                
                            end if;
                        end if;
                    
                    end if;    
                    
                when s5_sendEOF         =>
                    
                    -- (MSB) Send delimiter
                    tx_data_o(1)            <= DEL_PACKET(offset_pos);
                    offset_pos              := offset_pos + 1;
                    
                    -- Fully sent?
                    if offset_pos > 7 then
                    
                        tx_data_o(0)        <= '1';
                        hdlc_state          <= s0_waitForStart;
                                                        
                    else
                        
                        -- (LSB) Send delimiter
                        tx_data_o(0)        <= DEL_PACKET(offset_pos);
                        offset_pos          := offset_pos + 1;
                        
                        -- Fully sent?
                        if offset_pos > 7 then
                            hdlc_state         <= s0_waitForStart;
                        end if;
                        
                    end if;
            end case;    
            
        end if;
        
    end process;

end behaviour;
--============================================================================--
--############################################################################--
--============================================================================--