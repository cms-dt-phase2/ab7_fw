-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Super-layer data path
---- File       : superlayer_datapath.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-11
-------------------------------------------------------------------------------
---- Description: Process HITS for a given super-layer, generating 
----              reconstructed segments.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
use work.analyzer_pkg.all;
use work.dt_common_pkg.all;
use work.dt_buffers_pkg.all;
use work.globalpath_pkg.all;
use work.mixer_pkg.all;

entity SUPERLAYER_DATAPATH is
  port (
    clk_fifo_rd          : in  std_logic;
    clk_fifo_wr          : in  std_logic;
    clk_mixer            : in  std_logic;
    clk_analyzer         : in  std_logic;
    clk_dup_filter       : in  std_logic;
    clk_qenhan_filter    : in  std_logic;
    --
    rst_fifo_rd          : in  std_logic;
    rst_fifo_wr          : in  std_logic;
    rst_mixer            : in  std_logic;
    rst_analyzer         : in  std_logic;
    rst_dup_filter       : in  std_logic;
    rst_qenhan_filter    : in  std_logic;
    --
    superLayerDatPathIn  : in  SuperLayerDataPathIn_t;
    superLayerDatPathOut : out SuperLayerDataPathOut_t
  );
end entity;

architecture Behavioural of SUPERLAYER_DATAPATH is

  signal ffDTRawHitOut        : DTRawHitFifoOut_t;
  signal mixerDatPathOut      : MixerDataPathOut_t;
  signal ffSegCandidateOut    : DTSegCandidateFifoOut_t;
  signal analyzerDatPathOut   : AnalyzerDataPathOut_t;
  signal ffOutgoingSegmentOut : DTSegmentFifoOut_t;

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  superLayerDatPathOut.segment            <= ffOutgoingSegmentOut.segment;
  superLayerDatPathOut.segmentFFOut_empty <= ffOutgoingSegmentOut.empty;
  superLayerDatPathOut.rawHitsFFIn_full   <= ffDTRawHitOut.full;
  --
  superLayerDatPathOut.statistics.ffIncomingRawHitsStat  
                        <= ffDTRawHitOut.stat;

  superLayerDatPathOut.statistics.ffRawSegCandidateStat 
                        <= mixerDatPathOut.ffRawSegCandidateStat;

  superLayerDatPathOut.statistics.ffMainParamStat  
                        <= analyzerDatPathOut.ffMainParamStat;

  superLayerDatPathOut.statistics.ffSegPostDupStat 
                        <= analyzerDatPathOut.ffSegPostDupStat;

  superLayerDatPathOut.statistics.ffSegAfterAnalysisStat 
                        <= analyzerDatPathOut.ffSegAfterAnalysisStat;

  superLayerDatPathOut.statistics.ffSegmentCandidateStat 
                        <= ffSegCandidateOut.stat;

  superLayerDatPathOut.statistics.ffOutgoingSegmentStat  
                        <= ffOutgoingSegmentOut.stat;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  INCOMING_RAWHITS_FIFO : DTRAWHIT_FIFO port map (
    clkRd   => clk_mixer,
    clkWr   => clk_fifo_wr,
    rst     => rst_fifo_wr,
    fifoIn  => (
          rawHit => superLayerDatPathIn.rawHit,
          wrEna  => superLayerDatPathIn.rawHitFFIn_wr,
          rdEna  => mixerDatPathOut.rawHitsFFIn_rd,
          bx_time_convert_ena => 
              superLayerDatPathIn.control_regs.bx_time_convert_ena
    ),
    fifoOut => ffDTRawHitOut
  );
  
  MIXER_DATAPATH_INST : MIXER_DATAPATH port map (
    clk_mixer       => clk_mixer,
    clk_filter      => clk_dup_filter,
    --
    rst_mixer       => rst_mixer,
    rst_filter      => rst_dup_filter,
    --
    mixerDatPathIn  => (
          rawHit            => ffDTRawHitOut.rawHit,
          rawHitFFIn_empty  => ffDTRawHitOut.empty,
          segCandFFOut_full => ffSegCandidateOut.full,
          reset_overflow    => superLayerDatPathIn.reset_overflow,
          bx_counter        => superLayerDatPathIn.bx_counter,
          control_regs      => superLayerDatPathIn.control_regs
    ),
    mixerDatPathOut => mixerDatPathOut
  );

  -- Switch between glue/normal fifo by commenting appropriate lines before rst
  -- Glue fifo is ok because the consumer is generally faster than the producer

  --FF_PRE_ANALYSIS : DTSEGCANDIDATE_FIFO port map (
  FF_PRE_ANALYSIS : DTSEGCANDIDATE_GLUE_FIFO port map (
    --clkRd   => clk_analyzer,
    --clkWr   => clk_dup_filter,
    clk     => clk_analyzer,
    rst     => rst_dup_filter,
    fifoIn  => (
          segment_candidate => 
                superLayerDatPathIn.segcandFFIntrcptIn.segment_candidate, 
          wrEna             => superLayerDatPathIn.segcandFFIntrcptIn.wrEna, 
          rdEna             => analyzerDatPathOut.segCandFFIn_rd
    ),
    fifoOut => ffSegCandidateOut
  );

  superLayerDatPathOut.segcandFFIntrcptOut <= (
          segment_candidate => mixerDatPathOut.segment_candidate, 
          wrEna             => mixerDatPathOut.segCandFFOut_wr, 
          clk               => clk_dup_filter
          );
      

  ANALYZER_DATAPATH_INST : ANALYSIS_DATAPATH port map (
    clk_analyzer       => clk_analyzer,
    clk_dup_filter     => clk_dup_filter,
    clk_qenhan_filter  => clk_qenhan_filter,
    --
    rst_analyzer       => rst_analyzer,
    rst_dup_filter     => rst_dup_filter,
    rst_qenhan_filter  => rst_qenhan_filter,
    --
    analyzerDatPathIn  => (
          control_regs      => superLayerDatPathIn.control_regs,
          bx_counter        => superLayerDatPathIn.bx_counter,
          segment_candidate => ffSegCandidateOut.segment_candidate,
          segCandFFIn_empty => ffSegCandidateOut.empty,
          segmentFFOut_full => ffOutgoingSegmentOut.full
    ),
    analyzerDatPathOut => analyzerDatPathOut
  );

  OUTGOING_SEGMENTS_FIFO : DTSEGMENT_FIFO port map (
    clkRd   => clk_fifo_rd,
    clkWr   => clk_qenhan_filter,
    rst     => rst_qenhan_filter,
    fifoIn  => (
          segment => analyzerDatPathOut.segment, 
          wrEna   => analyzerDatPathOut.segmentFFOut_wr, 
          rdEna   => superLayerDatPathIn.segmentFFOut_rd
    ),
    fifoOut => ffOutgoingSegmentOut
  );

end architecture;
    
