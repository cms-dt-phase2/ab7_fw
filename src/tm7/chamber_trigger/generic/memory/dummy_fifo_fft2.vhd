-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Registers
---- File       : dummy_fifo_fft2.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-05
-------------------------------------------------------------------------------
---- Description: It implements a vector register with a behaviour similar to
----              a standard FIFO, with an additional "empty", "full"... 
----              signals.
----              It's single clocked. It is useful as a "glue" module on those 
----              situations where it's expected a FIFO interface between 
----              two linked modules but we don't want/need to waste a real
----              fifo, because a bunch of registers are enough.
----              
----              It behaves as FIRST FALL THROW fifo and is an extension of
----              'DUMMY_FIFO_FFT' for those cases where more than one register
----              is needed.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;

entity DUMMY_FIFO_FFT2 is
  generic (
    DATA_SIZE : integer := 16; 
    NUM_ITEMS : integer := 4      -- Greater or equal (and power of) 2.
  );
  port(
    clk     : in  std_logic;
    rst     : in  std_logic;
    rdEna   : in  std_logic;
    wrEna   : in  std_logic;
    dataIn  : in  std_logic_vector(DATA_SIZE - 1 downto 0);
    dataOut : out std_logic_vector(DATA_SIZE - 1 downto 0);
    empty   : out std_logic;
    full    : out std_logic
  );
end entity;

architecture RTL of DUMMY_FIFO_FFT2 is

  constant LAST_ITEM : integer := NUM_ITEMS - 1;
  constant BUS_LINES : integer := getBusLines(NUM_ITEMS);
  
  constant REG_EMPTY : std_logic_vector(LAST_ITEM downto 0) := (others => '0');
  constant REG_FULL  : std_logic_vector(LAST_ITEM downto 0) := (others => '1');

  type reg_data_type is array (LAST_ITEM downto 0) 
                     of std_logic_vector(DATA_SIZE - 1 downto 0);

  signal reg_data   : reg_data_type;
  signal cycle_rd   : unsigned(BUS_LINES - 1 downto 0)     := (others => '0');
  signal cycle_wr   : unsigned(BUS_LINES - 1 downto 0)     := (others => '0');
  signal reg_usage  : std_logic_vector(LAST_ITEM downto 0) := (others => '0');
  signal enable_reg : std_logic_vector(LAST_ITEM downto 0) := (others => '0');

begin
  -- Outgoing
  dataOut <= reg_data(to_integer(cycle_rd));
  
  empty   <= '1' when reg_usage = REG_EMPTY else '0';
  full    <= '1' when reg_usage = REG_FULL  else '0';

  -- Auxiliary
  ENA_GEN : for i in 0 to LAST_ITEM generate
    enable_reg(i) <= '1' when 
                            (reg_usage(i) = '0' or 
                                (reg_usage(i) = '1' and 
                                 cycle_rd = to_unsigned(i, BUS_LINES) and 
                                 rdEna = '1'
                                )
                            )
                            and cycle_wr = to_unsigned(i, BUS_LINES) 
                            and wrEna    = '1'
                     else '0';
  end generate;
--   enable_reg(1) <= (not reg_usage(1) or 
--                       (reg_usage(1) and cycle_rd and rdEna))
--                     and cycle_wr and wrEna;

  process(clk, rst)
  begin
    if clk'event and clk = '1' then
      -----------------------------------------------------
      -- Read/write selectors management
      -----------------------------------------------------
      if rst = '1' then
        cycle_rd <= (others => '0');
        cycle_wr <= (others => '0');
      else
        if reg_usage /= REG_FULL and wrEna = '1' then
          cycle_wr <= cycle_wr + 1;
        end if;
      
        if reg_usage /= REG_EMPTY and rdEna = '1' then
          cycle_rd <= cycle_rd + 1;
        end if;
      end if;

      -----------------------------------------------------
      -- Valid data signals management
      -----------------------------------------------------
      for i in 0 to LAST_ITEM loop
        if rst = '1' or (rdEna = '1' and to_integer(cycle_rd) = i) then
          reg_usage(i) <= '0';
        elsif enable_reg(i) = '1' then
          reg_usage(i) <= '1';
        end if;
      end loop;

      -----------------------------------------------------
      -- Data storage management
      -----------------------------------------------------
      if rst = '1' then
        reg_data <= (others => (others => '0'));
      else 
        
        for i in 0 to LAST_ITEM loop
          if enable_reg(i) = '1' 
          then reg_data(i) <= dataIn;
          end if;
        end loop;

      end if;
    end if;
  end process;

end architecture;
