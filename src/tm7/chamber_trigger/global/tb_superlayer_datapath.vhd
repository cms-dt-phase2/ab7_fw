library IEEE;
USE IEEE.std_logic_1164.all;
USE IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
--
use work.globalpath_pkg.all;
use work.dt_common_pkg.all;

entity TB_SUPERLAYER_DATAPATH is
end entity;

architecture Behavior of TB_SUPERLAYER_DATAPATH is 

  type hit_t is array (0 to 2) of integer;
  type hit_list_t is array (integer range<>) of hit_t;

  constant hits : hit_list_t(0 to 35) := (
    (0, 20, 33692), (1, 21, 33469), (2, 20, 33611), (3, 21, 33545),
    (0, 44, 56177), (1, 44, 56120), (2, 44, 56149), (3, 44, 56146),
    (0, 34, 56068), (1, 34, 55820), (2, 34, 56082), (3, 34, 55792),
    (0, 25, 56146), (1, 26, 56200), (2, 25, 56088), (3, 26, 56264),
    
    (0, 41, 78068), (1, 41, 78389), (2, 41, 78036), (2, 41, 78507),
    (3, 42, 78378),

    (0,  8, 79075), (0, 44, 79444), (1, 44, 79294), (2,  8, 79060), 
    (2, 44, 79416), (3,  8, 78932), (3, 44, 79332), 
                                          
    (0, 21, 56412), (1, 22, 56720), (1, 21, 56651), (2, 21, 56381),
    (2, 20, 56403), (2, 21, 56693), (2, 20, 56753), (3, 21, 56722)
  );

  constant clk_period : time                         := 5 ns; -- Clock period
  constant MAX_COUNT  : std_logic_vector(1 downto 0) := "01";

  ------------------------------------------------
  -- Auxiliary signals
  --
  signal rst             : std_logic                     := '0';
  signal clk             : std_logic                     := '0';
  signal clk_counter     : std_logic_vector(15 downto 0) := (others => '0');
  signal old_clk_counter : std_logic_vector(15 downto 0) := (others => '0');
  --
  signal input_counter   : std_logic_vector( 1 downto 0) := (others => '0');
  signal bx_counter      : std_logic_vector(11 downto 0) := (others => '0');
  --                     
  signal rawHit          : DTRawHit_t;
  signal rawHitFFIn_wr   : std_logic := '0';
  signal segmentFFOut_rd : std_logic := '0';

  ------------------------------------------------
  -- Component signals
  --
  signal superLayerDatPathIn  : SuperLayerDataPathIn_t;
  signal superLayerDatPathOut : SuperLayerDataPathOut_t;

begin
  -- Instantiate the Unit Under Test (UUT)
  UUT: SUPERLAYER_DATAPATH port map (
    clk                  => clk, 
    clk_mixer            => clk, 
    clk_analyzer         => clk, 
    clk_dup_filter       => clk, 
    clk_qenhan_filter    => clk, 
    rst                  => rst, 
    superLayerDatPathIn  => superLayerDatPathIn,
    superLayerDatPathOut => superLayerDatPathOut 
  );

  superLayerDatPathIn.rawHit          <= rawHit;
  superLayerDatPathIn.rawHitFFIn_wr   <= rawHitFFIn_wr;
  superLayerDatPathIn.segmentFFOut_rd <= segmentFFOut_rd;
  superLayerDatPathIn.reset_overflow  <= '0';
  superLayerDatPathIn.control_regs    <= (
        bx_time_tolerance  => std_logic_vector(to_unsigned(30, 8)),
        chisqr_threshold   => std_logic_vector(to_unsigned(3, 16)),
        bx_counter         => bx_counter
  );

  -- Clock process
  CLK_PROCESS : process
  begin
    clk <= not clk;
    wait for clk_period/2;
    
    if clk = '1' then
      clk_counter <= clk_counter + '1';
    end if;
  end process;

  BX_PROCESS : process(clk)
  begin
    if clk'event and clk = '1' then
      if rst = '1' then
        bx_counter      <= (others => '0');
        old_clk_counter <= (others => '0');
      else
        if clk_counter >= old_clk_counter + 5 then
          bx_counter      <= bx_counter + '1';
          old_clk_counter <= clk_counter;
        end if;
      end if;
    end if;
  end process;

  RD_FIFO_PROCESS : process(clk)
  begin
    if clk'event and clk = '1'then
      if rst = '1' then
        segmentFFOut_rd <= '0';
      else
        -- Only one cycle active.
        if segmentFFOut_rd = '1' then
          segmentFFOut_rd <= '0';
        elsif superLayerDatPathOut.segmentFFOut_empty = '0' then
          segmentFFOut_rd <= '1';
        end if;

      end if;
    end if;
  end process;
  
  -- Stimulus process
  STIM_PROC : process
  begin
    -- Initial RESET
    rst <= '0';
    wait for 153 ns; -- hold reset
    rst <= '0';

    -- Loop along muons' list
    MUON_LOOP : for hit in 0 to hits'length - 1 loop
      wait for clk_period;
      wait until clk = '1';

      rawHit <= (
        tdc_time       => std_logic_vector(to_unsigned(hits(hit)(2), 17)),
        channel_id     => std_logic_vector(to_unsigned(hits(hit)(1), 7)),
        layer_id       => std_logic_vector(to_unsigned(hits(hit)(0), 2)),
        super_layer_id => (others => '0')
      );
      rawHitFFIn_wr <= '1';
      wait until clk = '1';
      rawHitFFIn_wr <= '0';
    end loop;

    wait;
  end process;

end architecture;
