-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Common function package
---- File       : commonfunc_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2012-06
-------------------------------------------------------------------------------
---- Description: Common useful functions
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

-------------------------------------------------------------------------------
-- Header
-------------------------------------------------------------------------------
package commonfunc_pkg is

  function getBusLines(NUMELEMENTS : integer)         return integer;
  function invertStdLogic(VALUE : std_logic)          return std_logic;
  function or_bits_slv(vector : std_logic_vector)     return std_logic;
  function and_bits_slv(vector : std_logic_vector)    return std_logic;
  function mirror_bits_slv(vector : std_logic_vector) return std_logic_vector;
  function equal_vectors(v1, v2: std_logic_vector)    return std_logic;
  function count_ones(s : std_logic_vector)           return integer;
  
end package;

-------------------------------------------------------------------------------
---- Body
-------------------------------------------------------------------------------
package body commonfunc_pkg is

  -- FUNCIONES diversas.
  function getBusLines(NUMELEMENTS : integer) return integer is
  begin
    return (integer(ceil(log2(real(NUMELEMENTS)))));
  end function;

  -- Devuelve el valor lógico inverso
  function invertStdLogic(VALUE : std_logic) return std_logic is
    variable result : std_logic;
  begin
    case VALUE is
      when '0'    => result := '1';
      when '1'    => result := '0';
      when 'L'    => result := 'H';
      when 'H'    => result := 'L';
      when 'X'    => result := 'X';
      when others => result := 'U';
    end case;

    return result;
  end function;

  -- Calcula un "or" con todos los bits de un "std_logic_vector"
  function or_bits_slv(vector : std_logic_vector) return std_logic is
    variable result : std_logic;
  begin
    -- Inicializamos la variable auxiliar.
    result := '0';

    -- Encadenamos operaciones "or" entre los elementos.
    for i in vector'range loop
      result := result or vector(i);
    end loop;

    return result;
  end function;

  -- Calcula un "and" con todos los bits de un "std_logic_vector"
  function and_bits_slv(vector : std_logic_vector) return std_logic is
    variable result : std_logic;
  begin
    -- Inicializamos la variable auxiliar.
    result := '1';

    -- Encadenamos operaciones "and" entre los elementos.
    for i in vector'range loop
      result := result and vector(i);
    end loop;

    return result;
  end function;
  
  -- Revierte los bits de un vector intercambiando los MSB con los LSB.
  function mirror_bits_slv(vector : std_logic_vector) return std_logic_vector 
  is
    variable result : std_logic_vector(vector'length - 1 downto 0);
  begin
    for i in 0 to vector'length - 1 loop
      result(i) := vector(vector'length - 1 - i);
    end loop;
      
    return result;
  end function;
  --
  -- Devuelve '1' si los vectores son iguales bit a bit. '0' en caso contrario.
  -- Los vectores 'v1' y 'v2' han de tener la misma longitud.
  --
  function equal_vectors(v1, v2: std_logic_vector) return std_logic is
    variable result : std_logic_vector(v1'length - 1 downto 0);
  begin
    result := v1 xor v2;
      
    return (not or_bits_slv(result));
  end function;
  
  function count_ones(s : std_logic_vector) return integer is
    variable temp : natural := 0;
  begin
    for i in s'range loop
      if s(i) = '1' then temp := temp + 1; 
      end if;
    end loop;
    
    return temp;
  end function count_ones;  
  
end package body;
