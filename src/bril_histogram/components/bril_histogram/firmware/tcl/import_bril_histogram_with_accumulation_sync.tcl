# basic source files
source import_bril_histogram_basic_sync.tcl

# ones needed at the accumulation stage
add_files -norecurse ../hdl/sync/bril_histogram_accumulation_layer_sync.vhd
add_files -norecurse ../hdl/bril_histogram_error_block.vhd
add_files -norecurse ../hdl/bril_histogram_recursive_accumulator.vhd
