-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Clock management components
---- Design unit: Xilinx specific clock package
---- File       : xil_clock_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-02
-------------------------------------------------------------------------------
---- Description: Clock management based on Xilinx macros/primitives
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package xil_clock_pkg is

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component TRIGGER_CLOCK_GENERATOR is
    port (
      clk_40_i : in  std_logic;
      rst_i    : in  std_logic;
      rst_mmcm : in  std_logic;
      stopped  : out std_logic;
      locked   : out std_logic;
      clk40_o  : out std_logic;     -- 40     MHz
      clk1_o   : out std_logic;     -- 171.11 MHz
      clk2_o   : out std_logic;     -- 192.5  MHz
      clk3_o   : out std_logic;     -- 220.00 MHz
      rst40_o  : out std_logic;
      rst1_o   : out std_logic;
      rst2_o   : out std_logic;
      rst3_o   : out std_logic
    );
  end component;
  
end package;
