-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Registers
---- File       : dummy_fifo_fft.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-05
-------------------------------------------------------------------------------
---- Description: It implements a vector register with a behaviour similar to
----              a standard FIFO, with an additional "empty", "full"... 
----              signals.
----              It's single clocked. It is useful as a "glue" module on those 
----              situations where it's expected a FIFO interface between 
----              two linked modules but we don't want/need to waste a real
----              fifo, because a single register is enough.
----              It behaves as FIRST FALL THROW fifo.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;

entity DUMMY_FIFO_FFT is
  generic (DATA_SIZE : integer := 16);
  port(
    clk     : in  std_logic;
    rst     : in  std_logic;
    rdEna   : in  std_logic;
    wrEna   : in  std_logic;
    dataIn  : in  std_logic_vector(DATA_SIZE - 1 downto 0);
    dataOut : out std_logic_vector(DATA_SIZE - 1 downto 0);
    empty   : out std_logic;
    full    : out std_logic
  );
end entity;

architecture RTL of DUMMY_FIFO_FFT is

  signal data_valid  : std_logic;
  signal enable_reg  : std_logic;
  signal reg_data_in : std_logic_vector(DATA_SIZE - 1 downto 0);

begin
  -- Outgoing
  full    <= data_valid;
  empty   <= not data_valid;
  dataOut <= reg_data_in;

  -- Auxiliary
  enable_reg <= (not data_valid or (data_valid and rdEna)) and wrEna;

  process(clk, rst)
  begin
    if clk'event and clk = '1' then
      if rst = '1' or rdEna = '1' then
        data_valid <= '0';
      elsif enable_reg = '1' then
        data_valid <= '1';
      end if;

      if rst = '1' then
        reg_data_in <= (others => '0');
      elsif enable_reg = '1' then
        reg_data_in <= dataIn;
      end if;

    end if;
  end process;

end architecture;
