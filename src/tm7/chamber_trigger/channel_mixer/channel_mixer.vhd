-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Multi-channel data mixer
---- File       : channel_mixer.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: Multi-channel data mixer core.
----              It makes hits' mixes from groups of channels compatible with
----              a straight path for a candidate muon.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.mixer_pkg.all;

entity CHANNEL_MIXER is
  port (
    clk        : in  std_logic;
    rst        : in  std_logic;
    chMixerIn  : in  ChannelMixerIn_t;
    chMixerOut : out ChannelMixerOut_t
  );
end entity;

architecture TwoProcess of CHANNEL_MIXER is

  constant LAST_PATH : std_logic_vector( 2 downto 0) := (others => '1');
  constant TIME_ZERO : std_logic_vector(16 downto 0) := (others => '0');

  -- FSM states
  type state_type is (STBY, READ_FIFO_IN, COMBINE_PATH_HITS, 
                      MEMORY_RD_WAIT_STATE, MEMORY_WR_WAIT_STATE, 
                      SELECT_PATH_WS, REGISTER_CHANNELS_WS, EXPIRE_STATE);

  type element_sel_arr_t is array (3 downto 0) of std_logic_vector(3 downto 0);

  -- Internal signals to be registered.
  type reg_type is record
    state            : state_type;
    mem_rd           : std_logic;
    ffIn_rd          : std_logic;
    bx_counter_lsb   : std_logic;
    changed_bx       : std_logic;
    expiration_cycle : std_logic;
    is_the_last_path : std_logic;
    hit_selector     : element_sel_arr_t;
  end record;

  constant regDefault : reg_type := (
    state            => STBY,
    mem_rd           => '0',
    ffIn_rd          => '0',
    bx_counter_lsb   => '0',
    changed_bx       => '0',
    expiration_cycle => '0',
    is_the_last_path => '0',
    hit_selector     => (others => (others => '0'))
  );
  signal reg, nextReg : reg_type := regDefault;

  -- Auxiliary signals
  signal ffOut_wr          : std_logic;
  signal storing_hit       : std_logic;
  signal layer_mem_wr      : std_logic_vector(3 downto 0);
  signal time_valid        : std_logic_vector(3 downto 0);
  signal segment_candidate : DTSegCandidate_t;

  signal muxer_horiz_layout : CellHorizLayout_arr_t;

  type SgnChannelId_arr_t is array(3 downto 0) of signed(7 downto 0);
  type ChannelId_arr_t    is array(3 downto 0) of std_logic_vector(6 downto 0);
  
  -- Useful aliases
  signal curr_channel_id : std_logic_vector(6 downto 0);

  -- Components inter-connection signals
  type DTTimeLayMemOut_arr_t is array (0 to 3) of DTTimeLayMemOut_t;
  signal dtTimeLayMemOut : DTTimeLayMemOut_arr_t;

  signal next_path             : std_logic;
  signal valid_path            : std_logic;
  signal rst_path_sel          : std_logic;
  signal current_path_idx      : std_logic_vector(2 downto 0);
  signal current_path_channels : channels_path_t;
  
begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  chMixerOut.rawHitsFFIn_rd    <= reg.ffIn_rd;
  chMixerOut.segCandFFOut_wr   <= ffOut_wr;
  chMixerOut.segment_candidate <= segment_candidate;

  segment_candidate.cell_horiz_layout <= muxer_horiz_layout;

  SEGMENT_OUT_HITS : for i in 0 to 3 generate
    --
    -- Current layer propagates its time value directly to the outgoing data
    -- vector instead of retrieving it from the memory.
    -- At the same time the associated layer index, that follow the memory 
    -- buffer, will not change its value in order to save clock cycles, and 
    -- also to avoid generating duplicates mixes.
    --
    segment_candidate.hits(i).dt_time.tdc_time 
          <= chMixerIn.rawHit.tdc_time 
                  when i = to_integer(unsigned(chMixerIn.rawHit.layer_id))
                  else
             dtTimeLayMemOut(i).tdc_time 
                  when time_valid(i) = '1' 
                  else 
             TIME_ZERO;

    segment_candidate.hits(i).dt_time.valid <= time_valid(i);
    segment_candidate.hits(i).channel_id    <= current_path_channels(i);
    --
    -- This two lines are only useful for simulation purposes, to avoid "U"
    -- results. In a brief period of time this two fields will be removed 
    -- from the design.
    --
    segment_candidate.hits(i).layer_id       <= (others => '0');
    segment_candidate.hits(i).super_layer_id <= (others => '0');
  end generate;

  ---------------------------------------------------------------
  -- Auxiliary signals
  --
  storing_hit <= dtTimeLayMemOut(0).storing_hit or 
                 dtTimeLayMemOut(1).storing_hit or
                 dtTimeLayMemOut(2).storing_hit or 
                 dtTimeLayMemOut(3).storing_hit;

  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, chMixerIn, dtTimeLayMemOut, storing_hit, current_path_idx,
                 valid_path, time_valid)
    variable current_bx  : std_logic_vector(11 downto 0);
  begin
    -- Default values
    nextReg      <= reg;
    ffOut_wr     <= '0';
    next_path    <= '0';
    rst_path_sel <= '0';
    
    -- Aliases
    current_bx := chMixerIn.bx_counter;
    
    --                                    
    -- WARNING !!! This is always relative to layer-0 base cell, following the
    -- design requirements of the 'analyzer'.
    --
    muxer_horiz_layout <= 
          MIXER_CELL_HORIZONTAL_LAYOUTS(to_integer(unsigned(current_path_idx)));

    -- Determines if the BX is changing refered to the previous one.
    nextReg.bx_counter_lsb <= current_bx(0);
    
    if reg.bx_counter_lsb = '1' and current_bx(0) = '0' then
      nextReg.changed_bx <= '1';
    end if;

    for i in 0 to 3 loop
      --
      -- It's important to avoid writing into the memory, at the same time,
      -- if there is an ongoing expiring data cycle, because, in that case,
      -- internal memory pointers will be automatically incremented, storing
      -- twice the same hit ('tdc_time') data.
      --
      if reg.state                  = STBY and 
         reg.changed_bx             = '0'  and 
         chMixerIn.rawHitFFIn_empty = '0'  and 
         chMixerIn.rawHit.layer_id  = std_logic_vector(to_unsigned(i, 2))
      then layer_mem_wr(i) <= '1';
      else layer_mem_wr(i) <= '0';
      end if;
      --
      -- This is only an auxliary signal to manipulate as a single vector
      -- the 'time_valid' flags. This vector is composed here to take advantage
      -- of the current "loop" instead of implementing another additional one.
      --
      time_valid(i) <= dtTimeLayMemOut(i).time_valid;

    end loop;
    
    -- FSM to control hits' mixing process
    case reg.state is
      when STBY =>
        rst_path_sel             <= '1';
        nextReg.is_the_last_path <= '0';
        nextReg.hit_selector     <= (others => (others => '0'));

        if reg.changed_bx = '1' then
          nextReg.changed_bx       <= '0';
          nextReg.expiration_cycle <= '1';
          nextReg.state            <= EXPIRE_STATE;
        elsif chMixerIn.rawHitFFIn_empty = '0' then

          if storing_hit = '1' then
            nextReg.mem_rd  <= '1';
            nextReg.state   <= MEMORY_WR_WAIT_STATE;
          else
            nextReg.ffIn_rd <= '1';
            nextReg.state   <= READ_FIFO_IN;
          end if;

        end if;

      when READ_FIFO_IN =>
        nextReg.ffIn_rd <= '0';
        nextReg.state   <= STBY;
        
      when COMBINE_PATH_HITS =>
        if valid_path = '0' or reg.is_the_last_path = '1' then
          rst_path_sel    <= '1';
          nextReg.mem_rd  <= '0';
          nextReg.ffIn_rd <= '1';
          nextReg.state   <= READ_FIFO_IN;

        else
          if chMixerIn.segCandFFOut_full = '0' then
            ffOut_wr <= '1';
            ----------------------------------------------
            -- Loop over layer 0 associated channel hits.
            --
            -- Flag 'buffer_out_full' indicates that there are 16 elements 
            -- inside the associated buffer, so it's possible to go one 
            -- element further in the counter to retrieve this last one 
            -- stored value.
            -- That's the reason for these double comparisons.
            --
            -- Additionally when the incoming hit's layer is coincident with
            -- the counter index this index do not move its value (kept = 0)
            -- to avoid making a lot of unnecessary mixture cycles, creating
            -- at the same time many duplicates.
            --
            if ((dtTimeLayMemOut(0).buffer_out_full = '0' and
                reg.hit_selector(0) + 1 < 
                    dtTimeLayMemOut(0).channelOut_occupancy) 
              or 
               (dtTimeLayMemOut(0).buffer_out_full = '1' and
                reg.hit_selector(0) < 
                    dtTimeLayMemOut(0).channelOut_occupancy))
              and to_integer(unsigned(chMixerIn.rawHit.layer_id)) /= 0
            then 
              nextReg.hit_selector(0) <= reg.hit_selector(0) + '1';
              nextReg.state           <= MEMORY_RD_WAIT_STATE;
            else 
              nextReg.hit_selector(0) <= (others => '0');
              ----------------------------------------------
              -- Loop over layer 1 associated channel hits.
              --
              if ((dtTimeLayMemOut(1).buffer_out_full = '0' and
                  reg.hit_selector(1) + 1 < 
                      dtTimeLayMemOut(1).channelOut_occupancy)
                or
                 (dtTimeLayMemOut(1).buffer_out_full = '1' and
                  reg.hit_selector(1) < 
                      dtTimeLayMemOut(1).channelOut_occupancy))
                and to_integer(unsigned(chMixerIn.rawHit.layer_id)) /= 1
              then 
                nextReg.hit_selector(1) <= reg.hit_selector(1) + '1';
                nextReg.state           <= MEMORY_RD_WAIT_STATE;
              else 
                nextReg.hit_selector(1) <= (others => '0');
                ----------------------------------------------
                -- Loop over layer 2 associated channel hits.
                --
                if ((dtTimeLayMemOut(2).buffer_out_full = '0' and
                    reg.hit_selector(2) + 1 < 
                        dtTimeLayMemOut(2).channelOut_occupancy)
                  or
                   (dtTimeLayMemOut(2).buffer_out_full = '1' and
                    reg.hit_selector(2) < 
                        dtTimeLayMemOut(2).channelOut_occupancy))
                  and to_integer(unsigned(chMixerIn.rawHit.layer_id)) /= 2
                then 
                  nextReg.hit_selector(2) <= reg.hit_selector(2) + '1';
                  nextReg.state           <= MEMORY_RD_WAIT_STATE;
                else 
                  nextReg.hit_selector(2) <= (others => '0');
                  ----------------------------------------------
                  -- Loop over layer 3 associated channel hits.
                  --
                  if ((dtTimeLayMemOut(3).buffer_out_full = '0' and
                      reg.hit_selector(3) + 1 < 
                          dtTimeLayMemOut(3).channelOut_occupancy)
                    or
                     (dtTimeLayMemOut(3).buffer_out_full = '1' and
                      reg.hit_selector(3) < 
                          dtTimeLayMemOut(3).channelOut_occupancy))
                    and to_integer(unsigned(chMixerIn.rawHit.layer_id)) /= 3
                  then 
                    nextReg.hit_selector(3) <= reg.hit_selector(3) + '1';
                    nextReg.state           <= MEMORY_RD_WAIT_STATE;
                  else 
                    if current_path_idx = LAST_PATH then
                      nextReg.is_the_last_path <= '1';
                    end if;
                  
                    nextReg.hit_selector(3) <= (others => '0');
                    next_path               <= '1';
                    nextReg.state           <= SELECT_PATH_WS;
                  end if;
                end if;
              end if;
            end if;
          end if;

        end if;
        
      when MEMORY_RD_WAIT_STATE =>
          nextReg.state <= REGISTER_CHANNELS_WS;
      --
      -- One WS to allow memory supply information. It takes 1 clock cycle
      -- after setting "wr ena" flag on the memory (See: "DTTIME_LAYER_MEM_GEN")
      --
      -- CAREFUL!! This state is also used as WS for one of the 2 clock cycles
      -- that "CURRENT_PATH_SELECTOR" needs after a change on the current
      -- channel/layer combination. After that, getting another "path" will
      -- take only one clock cycle.
      --
      when MEMORY_WR_WAIT_STATE =>
        nextReg.state <= SELECT_PATH_WS;
        
      when SELECT_PATH_WS =>
        nextReg.state <= REGISTER_CHANNELS_WS;

      when REGISTER_CHANNELS_WS =>
        nextReg.state <= COMBINE_PATH_HITS;
        
      when EXPIRE_STATE =>
        nextReg.expiration_cycle <= '0';
        nextReg.state            <= STBY;
        
      when others =>
        nextReg.state <= STBY;
    end case;
  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1'
      then reg <= regDefault;
      else reg <= nextReg;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  CURR_PATH_SEL_INST : CURRENT_PATH_SELECTOR port map (
    clk                   => clk,
    rst                   => (rst or rst_path_sel),
    next_path             => next_path,
    channel               => chMixerIn.rawHit.channel_id,
    layer                 => chMixerIn.rawHit.layer_id,
    ch_empty_lay0         => dtTimeLayMemOut(0).buffers_empty,
    ch_empty_lay1         => dtTimeLayMemOut(1).buffers_empty,
    ch_empty_lay2         => dtTimeLayMemOut(2).buffers_empty,
    ch_empty_lay3         => dtTimeLayMemOut(3).buffers_empty,
    valid                 => valid_path,
    current_path_idx      => current_path_idx,
    current_path_channels => current_path_channels
  );

  DTTIME_LAYER_MEM_GEN : for i in 0 to 3 generate
    LAYER_MEMORY : DTTIME_LAYER_MEMORY port map(
      clk             => clk,
      rst             => rst,
      dtTimeLayMemIn  => (
            channelIn_id     => chMixerIn.rawHit.channel_id,
            tdc_time         => chMixerIn.rawHit.tdc_time,
            ena_wr           => layer_mem_wr(i),
            --
            channelOut_id    => current_path_channels(i),
            dataOut_idx      => reg.hit_selector(i),
            ena_rd           => reg.mem_rd,
            --
            reset_overflow   => chMixerIn.reset_overflow,
            expiration_cycle => reg.expiration_cycle
      ),
      dtTimeLayMemOut => dtTimeLayMemOut(i)
    );
  end generate;

end architecture;
