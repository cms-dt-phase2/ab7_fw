-- IEEE VHDL standard library:
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- Library work
library work;
use work.mp7_ttc_decl.all;

entity ttc_lumi_decoder is
  port(
    clk                : in  std_logic;
    rst                : in  std_logic;  
    ttc_cmd            : in std_logic_vector(7 downto 0);
    oc0_signal         : out std_logic;
    bc0_signal         : out std_logic;
    bg0_signal         : out std_logic;
    lumi_section_o     : out std_logic_vector(31 downto 0);
    lumi_nibble_o      : out std_logic_vector(31 downto 0)
  );
end entity;

architecture Behavioural of ttc_lumi_decoder is

  signal bg0_flag               : std_logic;
  signal lumi_section           : unsigned(31 downto 0);
  signal lumi_nibble            : unsigned(31 downto 0);

begin

  lumi_nibble_o   <= std_logic_vector(lumi_nibble);
  lumi_section_o  <= std_logic_vector(lumi_section);  

  process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        lumi_nibble  <= to_unsigned(1,32);
        lumi_section <= to_unsigned(1,32);
        bg0_flag   <= '0';
        bc0_signal <= '0';
        bg0_signal <= '0';
        oc0_signal <= '0';
      else 
        
        if ttc_cmd = TTC_BCMD_OC0 then
          oc0_signal <= '1';
        else
          oc0_signal <= '0';
        end if;
        
        if ttc_cmd = TTC_BCMD_LUMI_NIBBLE then
          bg0_flag   <= '1';  
        end if;
        
        if ttc_cmd = TTC_BCMD_BC0 then
          if bg0_flag = '1' then
            bg0_flag <= '0';
            bg0_signal <= '1';
         
            if lumi_nibble < to_unsigned(64,32) then
              lumi_nibble <= lumi_nibble+ 1;
            else
              lumi_section <= lumi_section + 1;
              lumi_nibble <= to_unsigned(1,32);
            end if;
          end if;

          bc0_signal <= '1';
        else
          bc0_signal <= '0';
          bg0_signal <= '0';
        end if;

      end if;
    end if;
  end process;

  
end architecture;
