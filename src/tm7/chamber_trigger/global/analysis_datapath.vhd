-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Analyzer data path
---- File       : analysis_datapath.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-11
-------------------------------------------------------------------------------
---- Description: Process HITS for a given chamber, generating reconstructed
----              segments.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
use work.analyzer_pkg.all;
use work.dt_common_pkg.all;
use work.dt_buffers_pkg.all;
use work.filters_pkg.all;
use work.globalpath_pkg.all;
use work.registers_pkg.all;

entity ANALYSIS_DATAPATH is
  port (
    clk_analyzer       : in  std_logic;
    clk_dup_filter     : in  std_logic;
    clk_qenhan_filter  : in  std_logic;
    --
    rst_analyzer       : in  std_logic;
    rst_dup_filter     : in  std_logic;
    rst_qenhan_filter  : in  std_logic;
    --
    analyzerDatPathIn  : in  AnalyzerDataPathIn_t;
    analyzerDatPathOut : out AnalyzerDataPathOut_t
  );
end entity;

architecture Behavioural of ANALYSIS_DATAPATH is

  signal analyzerOut        : AnalyzerOut_t;
  signal ffAfterAnalyzerOut : DTSegmentFifoOut_t;
  signal segDupFilterOut    : SegmentFilterOut_t;
  signal ffPostDupOut       : DTSegmentFifoOut_t;
  signal chainQEnFilterOut  : SegmentFilterOut_t;
  --
  signal bx_counter_segdupfilt : std_logic_vector(11 downto 0);
  signal bx_counter_qenhfilt   : std_logic_vector(11 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  analyzerDatPathOut.segment         <= chainQEnFilterOut.segment;
  analyzerDatPathOut.segmentFFOut_wr <= chainQEnFilterOut.fifoOut_wr;
  analyzerDatPathOut.segCandFFIn_rd  <= analyzerOut.segCandFFIn_rd;
  --
  analyzerDatPathOut.ffMainParamStat        <= analyzerOut.ffMainParamStat;
  analyzerDatPathOut.ffSegAfterAnalysisStat <= ffAfterAnalyzerOut.stat;
  analyzerDatPathOut.ffSegPostDupStat       <= ffPostDupOut.stat;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  ANALYZ_INST : ANALYZER port map(
    clk      => clk_analyzer,
    rst      => rst_analyzer,
    segAnIn  => (
          control_regs       => analyzerDatPathIn.control_regs,
          segment_candidate  => analyzerDatPathIn.segment_candidate,
          segCandFFIn_empty  => analyzerDatPathIn.segCandFFIn_empty,
          segValidFFOut_full => ffAfterAnalyzerOut.full
    ),
    segAnOut => analyzerOut
  );

  -- Switch between glue/normal fifo by commenting appropriate lines before rst
  -- Glue fifo is ok because the consumer is generally faster than the producer

  --FF_AFTER_ANALYSIS : DTSEGMENT_FIFO port map (
  FF_AFTER_ANALYSIS : DTSEGMENT_GLUE_FIFO port map (
    --clkRd   => clk_dup_filter,
    --clkWr   => clk_analyzer,
    clk     => clk_analyzer,
    rst     => rst_analyzer,
    fifoIn  => (
          segment => analyzerOut.segment, 
          wrEna   => analyzerOut.segValidFFOut_wr, 
          rdEna   => chainQEnFilterOut.fifoIn_rd
    ),
    fifoOut => ffAfterAnalyzerOut
  );
  
--  BXCNT_SYNC_SEGDUPFILT : VECTOR_SHIFT_REGISTER
--  generic map (VECTOR_WIDTH => 12, SHIFT_STEPS => 2)
--  port map (clk => clk_dup_filter, enable => '1', 
--            vector_in  => analyzerDatPathIn.bx_counter,
--            vector_out => bx_counter_segdupfilt
--  );
--  
--  SEG_DUP_FILT_INST : SEG_DUPLICATED_SHIFTING_FILTER
--  generic map (MEM_ELEMENTS => 8)
--  port map (
--    clk          => clk_dup_filter,
--    rst          => rst_dup_filter,
--    segFilterIn  => (
--          segment      => ffAfterAnalyzerOut.segment,
--          fifoIn_empty => ffAfterAnalyzerOut.empty,
--          fifoOut_full => ffPostDupOut.full,
--          bx_counter   => bx_counter_segdupfilt
--    ),
--    segFilterOut => segDupFilterOut
--  );
--  
--  FF_POST_DUPLICATED : DTSEGMENT_FIFO port map (
--    clkRd   => clk_qenhan_filter,
--    clkWr   => clk_dup_filter,
--    rst     => rst_dup_filter,
--    fifoIn  => (
--          segment => segDupFilterOut.segment, 
--          wrEna   => segDupFilterOut.fifoOut_wr, 
--          rdEna   => chainQEnFilterOut.fifoIn_rd
--    ),
--    fifoOut => ffPostDupOut
--  );
  
  BXCNT_SYNC_QENHFILT : VECTOR_SHIFT_REGISTER
  generic map (VECTOR_WIDTH => 12, SHIFT_STEPS => 2)
  port map (clk => clk_qenhan_filter, enable => '1', 
            vector_in  => analyzerDatPathIn.bx_counter,
            vector_out => bx_counter_qenhfilt
  );
  
  QENHANC_FILT_INST : CHAIN_QENHANCER_FILTER port map (
    clk                  => clk_qenhan_filter,
    rst                  => rst_qenhan_filter,
    control_regs         => analyzerDatPathIn.control_regs,
    chainQEnhanFilterIn  => (
          segment      => ffAfterAnalyzerOut.segment,
          fifoIn_empty => ffAfterAnalyzerOut.empty,
          fifoOut_full => analyzerDatPathIn.segmentFFOut_full,
          bx_counter   => bx_counter_qenhfilt
    ),
    chainQEnhanFilterOut => chainQEnFilterOut
  );
  
end architecture;
    
