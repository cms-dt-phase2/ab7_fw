--=================================================================================================--
-- Module for communication with the GBTX chip             
-- Adapted from https://gitlab.cern.ch/gbtsc-fpga-support/gbtsc-example-design
-- By Javier Sastre Alvaro (javier.sastre@ciemat.es) & Alvaro Navarro Tobar
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Custom libraries and packages:
library work;
use work.obdt_decoder_decl.all;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_ab7_gbt.all;
use work.gbt_bank_package.all;
use work.bo2.all;

entity ab7_gbt is
  port (   
    clk40               : in std_logic; -- Frame clock
    -- vc707_gbt_example_design uses 156 MHz for DRP/SYSCLK, we replace it by 100 MHz 
    clk100              : in std_logic; -- Fabric clock 100 MHz   
    clk160              : in std_logic;
    --------------
    -- MGT --
    --------------
    -- MGT reference clock (from external Si5338 clock generator), 120MHz for the latency-optimized GBT Bank
    mgt117_clkn, mgt117_clkp    : in  std_logic;
    mgt117_rxn,  mgt117_rxp     : in  std_logic_vector(0 to 3);
    mgt117_txn,  mgt117_txp     : out std_logic_vector(0 to 3);
    mgt118_clkn, mgt118_clkp    : in  std_logic;
    mgt118_rxn,  mgt118_rxp     : in  std_logic_vector(0 to 3);
    mgt118_txn,  mgt118_txp     : out std_logic_vector(0 to 3);
    mgt119_clkn, mgt119_clkp    : in  std_logic;
    mgt119_rxn,  mgt119_rxp     : in  std_logic_vector(0 to 3);
    mgt119_txn,  mgt119_txp     : out std_logic_vector(0 to 3);
    --------------
    -- ipbus --
    --------------
    ipb_clk             : in std_logic;
    ipb_rst             : in std_logic;
    ipb_in              : in ipb_wbus;
    ipb_out             : out ipb_rbus;
    
    ----------------
    -- data ports --
    ----------------
    ttc_cmd             : in std_logic_vector(7 downto 0);
    bunch_ctr           : in std_logic_vector(11 downto 0);
    headerLock          : out std_logic_vector(1 to 12);
    obdt_data           : out slv32_v_t(1 to 3);
    txFrameClk_lock     : out std_logic;
    mgtrefclk_rst       : out std_logic
    
  );
end entity;

architecture structural of ab7_gbt is

  signal txFrameClk_from_txPll  : std_logic;
  signal rxData_s, txData_s     : gbt_reg84_A(1 to 12);
  signal rxIsData_s, txIsData_s : std_logic_vector(1 to 12);
  signal header_lock_v          : std_logic_vector(1 to 12);
  signal chsel                  : usgn4_v_t(1 to 3);
  signal stat_duplicate_word_ctr: usgn32_v_t(1 to 3);


  -- IPBUS BRANCHES
  signal ipb_to_slaves  : ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves: ipb_rbus_array(N_SLAVES - 1 downto 0);

  -- IPBUS REGISTER
  constant N_STAT : integer := 16;
  constant N_CTRL : integer := 16;
  signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);
  signal stat, csrd: ipb_reg_v(N_STAT-1 downto 0);
  signal conf, csrq: ipb_reg_v(N_CTRL-1 downto 0);

  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals declaration
  signal conf_mgtrefclk_rst, conf_resetgbtfpga, stat_frameClk_lock : std_logic := '0';

  signal stat_unlock_cool, stat_wrong_pattern_cool : std_logic_vector(2 downto 0) := (others => '0');

  signal conf_chsel1, conf_chsel2, conf_chsel3 : unsigned(3 downto 0) := (others => '0');
  signal conf_unlock_cooldown, conf_wrong_pattern_cooldown : unsigned(4 downto 0) := (others => '0');
  signal stat_sl1_unlock_ctr, stat_sl1_wrong_pattern_ctr, stat_sl2_unlock_ctr, stat_sl2_wrong_pattern_ctr,
    stat_sl3_unlock_ctr, stat_sl3_wrong_pattern_ctr : unsigned(15 downto 0) := (others => '0');
  signal stat_sl1_duplicate_word_ctr, stat_sl1_unlock_time, stat_sl1_wrong_pattern_time, stat_sl2_duplicate_word_ctr,
    stat_sl2_unlock_time, stat_sl2_wrong_pattern_time, stat_sl3_duplicate_word_ctr, stat_sl3_unlock_time,
    stat_sl3_wrong_pattern_time : unsigned(31 downto 0) := (others => '0');

  -- End of automatically-generated VHDL code for register "breakout" signals declaration
  --------------------------------------------------------------------------------


begin


  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals assignment

  conf_mgtrefclk_rst          <= conf(16#00#)(0);
  conf_resetgbtfpga           <= conf(16#00#)(1);
  conf_unlock_cooldown        <= unsigned( conf(16#01#)(4 downto 0) );
  conf_wrong_pattern_cooldown <= unsigned( conf(16#01#)(10 downto 6) );
  conf_chsel1                 <= unsigned( conf(16#01#)(15 downto 12) );
  conf_chsel2                 <= unsigned( conf(16#01#)(19 downto 16) );
  conf_chsel3                 <= unsigned( conf(16#01#)(23 downto 20) );
  stat(16#00#)(0)             <= stat_frameClk_lock;
  stat(16#01#)(2 downto 0)    <= stat_unlock_cool;
  stat(16#01#)(5 downto 3)    <= stat_wrong_pattern_cool;
  stat(16#04#)(15 downto 0)   <= std_logic_vector( stat_sl1_unlock_ctr );
  stat(16#04#)(31 downto 16)  <= std_logic_vector( stat_sl1_wrong_pattern_ctr );
  stat(16#05#)(31 downto 0)   <= std_logic_vector( stat_sl1_unlock_time );
  stat(16#06#)(31 downto 0)   <= std_logic_vector( stat_sl1_wrong_pattern_time );
  stat(16#07#)(31 downto 0)   <= std_logic_vector( stat_sl1_duplicate_word_ctr );
  stat(16#08#)(15 downto 0)   <= std_logic_vector( stat_sl2_unlock_ctr );
  stat(16#08#)(31 downto 16)  <= std_logic_vector( stat_sl2_wrong_pattern_ctr );
  stat(16#09#)(31 downto 0)   <= std_logic_vector( stat_sl2_unlock_time );
  stat(16#0A#)(31 downto 0)   <= std_logic_vector( stat_sl2_wrong_pattern_time );
  stat(16#0B#)(31 downto 0)   <= std_logic_vector( stat_sl2_duplicate_word_ctr );
  stat(16#0C#)(15 downto 0)   <= std_logic_vector( stat_sl3_unlock_ctr );
  stat(16#0C#)(31 downto 16)  <= std_logic_vector( stat_sl3_wrong_pattern_ctr );
  stat(16#0D#)(31 downto 0)   <= std_logic_vector( stat_sl3_unlock_time );
  stat(16#0E#)(31 downto 0)   <= std_logic_vector( stat_sl3_wrong_pattern_time );
  stat(16#0F#)(31 downto 0)   <= std_logic_vector( stat_sl3_duplicate_word_ctr );

  -- End of automatically-generated VHDL code for register "breakout" signals assignment
  --------------------------------------------------------------------------------

  
--------------------------------------
--------------------------------------
-- IPbus register
--------------------------------------
--------------------------------------

-- ipbus address decode
fabric: entity work.ipbus_fabric_sel
  generic map(
  	NSLV => N_SLAVES,
  	SEL_WIDTH => IPBUS_SEL_WIDTH)
  port map(
    ipb_in => ipb_in,
    ipb_out => ipb_out,
    sel => ipbus_sel_ab7_gbt(ipb_in.ipb_addr),
    ipb_to_slaves => ipb_to_slaves,
    ipb_from_slaves => ipb_from_slaves
  );

sync_reg: entity work.ipbus_syncreg_v
generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
port map(
  clk => ipb_clk,
  rst => ipb_rst,
  ipb_in => ipb_to_slaves(N_SLV_AB7),
  ipb_out => ipb_from_slaves(N_SLV_AB7),
  slv_clk => clk40,
  d => csrd,
  q => csrq,
  --rstb => stat_stb,
  stb => open
);

conf <= csrq;
csrd <= stat;

mgtrefclk_rst <= conf_mgtrefclk_rst;
txFrameClk_lock <= stat_frameClk_lock;

headerLock <= header_lock_v;

tm7_gbt_inst : entity work.tm7_gbt
generic map( 
  REMAP_RX => (
    1 =>  (mgt => 119, ch => 4 ),
    2 =>  (mgt => 119, ch => 1 ),
    3 =>  (mgt => 119, ch => 2 ),
    4 =>  (mgt => 119, ch => 3 ),
    5 =>  (mgt => 118, ch => 4 ),
    6 =>  (mgt => 118, ch => 3 ),
    7 =>  (mgt => 117, ch => 1 ),
    8 =>  (mgt => 118, ch => 2 ),
    9 =>  (mgt => 117, ch => 2 ),
    10 => (mgt => 118, ch => 1 ),
    11 => (mgt => 117, ch => 3 ),
    12 => (mgt => 117, ch => 4 )
  ),
  REMAP_TX => (
    1 =>  (mgt => 117, ch => 3 ),
    2 =>  (mgt => 117, ch => 4 ),
    3 =>  (mgt => 117, ch => 2 ),
    4 =>  (mgt => 118, ch => 1 ),
    5 =>  (mgt => 117, ch => 1 ),
    6 =>  (mgt => 118, ch => 2 ),
    7 =>  (mgt => 119, ch => 4 ),
    8 =>  (mgt => 118, ch => 3 ),
    9 =>  (mgt => 119, ch => 3 ),
    10 => (mgt => 118, ch => 4 ),
    11 => (mgt => 119, ch => 2 ),
    12 => (mgt => 119, ch => 1 )
  ),
  GENERATE_ILAS => x"FFF",
  GENERATE_SC => x"FFF"

 )
port map(   
  -- Clocks   --
  clk40           => clk40, -- Frame clock
  clk_fr          => clk100,
  -- MGTs --
  mgt117_clkp    => mgt117_clkp,
  mgt117_clkn    => mgt117_clkn,
  mgt117_txp     => mgt117_txp,
  mgt117_txn     => mgt117_txn,
  mgt117_rxp     => mgt117_rxp,
  mgt117_rxn     => mgt117_rxn,
  
  mgt118_clkp    => mgt118_clkp,
  mgt118_clkn    => mgt118_clkn,
  mgt118_txp     => mgt118_txp,
  mgt118_txn     => mgt118_txn,
  mgt118_rxp     => mgt118_rxp,
  mgt118_rxn     => mgt118_rxn,
  
  mgt119_clkp    => mgt119_clkp,
  mgt119_clkn    => mgt119_clkn,
  mgt119_txp     => mgt119_txp,
  mgt119_txn     => mgt119_txn,
  mgt119_rxp     => mgt119_rxp,
  mgt119_rxn     => mgt119_rxn,
  -- ipbus --
  ipb_clk         => ipb_clk,
  ipb_rst         => ipb_rst,
  ipb_in          => ipb_to_slaves(N_SLV_TM7),
  ipb_out         => ipb_from_slaves(N_SLV_TM7),
  -- data ports --
  resetgbtfpga    => conf_resetgbtfpga,
  txFrameClk_from_txPll_o => txFrameClk_from_txPll,
  rxData          => rxData_s,
  rxIsData        => rxIsData_s,
  txData          => txData_s,
  txIsData        => txIsData_s,
  header_lock     => header_lock_v,
  txFrameClk_lock => stat_frameClk_lock
);



obdt_decoder_inst : entity work.obdt_decoder
port map(   
  -- Configuration
  chsel                         => chsel,
  conf_unlock_cooldown          => conf_unlock_cooldown,
  conf_wrong_pattern_cooldown   => conf_wrong_pattern_cooldown,
  -- Stats
  stat_unlock_cool              => stat_unlock_cool,
  stat_wrong_pattern_cool       => stat_wrong_pattern_cool,
  -- Input data from gbt
  frameClk                      => txFrameClk_from_txPll,
  headerLocked12                => header_lock_v,
  rxData12                      => rxData_s,
  rxIsData12                    => rxIsData_s,
  -- Output data
  clk160                        => clk160,
  obdt_data                     => obdt_data
);

chsel <= ( 1 => conf_chsel1 , 2 => conf_chsel2 , 3 => conf_chsel3 );

(stat_sl1_duplicate_word_ctr, stat_sl2_duplicate_word_ctr, stat_sl3_duplicate_word_ctr) <= stat_duplicate_word_ctr;

-- Statistics (counter and timer) of unlocks and wrong_pattern detection

process(clk40)
  variable unlock_last, wrongpattern_last : std_logic_vector(2 downto 0);
begin
  if rising_edge(clk40) then
    stat_sl1_unlock_time        <= stat_sl1_unlock_time        + bo2int(stat_unlock_cool(0)='0');
    stat_sl2_unlock_time        <= stat_sl2_unlock_time        + bo2int(stat_unlock_cool(1)='0');
    stat_sl3_unlock_time        <= stat_sl3_unlock_time        + bo2int(stat_unlock_cool(2)='0');
    stat_sl1_wrong_pattern_time <= stat_sl1_wrong_pattern_time + bo2int(stat_wrong_pattern_cool(0)='0');
    stat_sl2_wrong_pattern_time <= stat_sl2_wrong_pattern_time + bo2int(stat_wrong_pattern_cool(1)='0');
    stat_sl3_wrong_pattern_time <= stat_sl3_wrong_pattern_time + bo2int(stat_wrong_pattern_cool(2)='0');
    
    stat_sl1_unlock_ctr        <= stat_sl1_unlock_ctr        + bo2int(stat_unlock_cool(0)='0'        and unlock_last(0)='1');
    stat_sl2_unlock_ctr        <= stat_sl2_unlock_ctr        + bo2int(stat_unlock_cool(1)='0'        and unlock_last(1)='1');
    stat_sl3_unlock_ctr        <= stat_sl3_unlock_ctr        + bo2int(stat_unlock_cool(2)='0'        and unlock_last(2)='1');
    stat_sl1_wrong_pattern_ctr <= stat_sl1_wrong_pattern_ctr + bo2int(stat_wrong_pattern_cool(0)='0' and wrongpattern_last(0)='1');
    stat_sl2_wrong_pattern_ctr <= stat_sl2_wrong_pattern_ctr + bo2int(stat_wrong_pattern_cool(1)='0' and wrongpattern_last(1)='1');
    stat_sl3_wrong_pattern_ctr <= stat_sl3_wrong_pattern_ctr + bo2int(stat_wrong_pattern_cool(2)='0' and wrongpattern_last(2)='1');
    
    unlock_last := stat_unlock_cool;
    wrongpattern_last := stat_wrong_pattern_cool;
  end if;
end process;




-- GBT
gbt : entity work.moco_gbt
port map(   
  -- Clocks   --
  clk40           => clk40, -- Frame clock
  txFrameClk_from_txPll => txFrameClk_from_txPll,
  -- ipbus --
  ipb_clk       => ipb_clk,
  ipb_rst       => ipb_rst,
  ipb_in        => ipb_to_slaves(N_SLV_MOCO),
  ipb_out       => ipb_from_slaves(N_SLV_MOCO),
  -- data ports --
  ttc_cmd               => ttc_cmd,
  bunch_ctr             => bunch_ctr,
  rxData                => rxData_s,
  rxIsData              => rxIsData_s,
  txData                => txData_s,
  txIsData              => txIsData_s,
  header_lock           => header_lock_v
);




end architecture;