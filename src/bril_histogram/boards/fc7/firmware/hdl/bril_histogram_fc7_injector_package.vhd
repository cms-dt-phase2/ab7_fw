----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package bril_histogram_fc7_injector_package is
    -- mode
    constant ASYNC_MODE                                 : boolean := false;
    -- histogram settings
    constant NUMBER_OF_BINS                             : natural := 3564;
    constant COUNTER_WIDTH                              : natural := 24;
    constant INCREMENT_WIDTH                            : natural := 8;
    -- accumulation layer settings
    constant NUMBER_OF_UNITS                            : natural := 16;
    constant UNIT_INCREMENT_WIDTH                       : natural := 3;
    constant NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE     : natural := 4;
    -- error block
    constant UNIT_ERROR_COUNTER_WIDTH                   : natural := 3;
    constant STORE_ERRORS                               : boolean := true;
                                                                                   
end bril_histogram_fc7_injector_package;