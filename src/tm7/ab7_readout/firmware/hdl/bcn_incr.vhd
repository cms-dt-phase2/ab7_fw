----------------------------------------------------------------------------------
-- increment the bunch counter
-- �lvaro Navarro, CIEMAT, 2015
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.bo2.all;
use work.BXarithmetic.all;
use work.bo2.all;
use work.mp7_ttc_decl.all;

entity bcn_incr is
  port(
    ttc_clk         : in std_logic;
    incr            : in bctr_t;
    bunch_ctr_in    : in bctr_t;
    bunch_ctr_out   : out bctr_t;
    orb_ctr_in      : in eoctr_t;
    orb_ctr_out     : out eoctr_t
  );
end entity;

architecture Behavioral of bcn_incr is

begin
 
  process (ttc_clk)
    variable bunch_ctr_tmp : unsigned(11 downto 0);
  begin
    if rising_edge(ttc_clk) then
      -- delay = 1 means no delay due to the latency of the sequential process
      bunch_ctr_tmp := BXplus( unsigned(bunch_ctr_in), unsigned(incr) );
      orb_ctr_out <=  eoctr_t( unsigned(orb_ctr_in) + bo2int(bunch_ctr_tmp < unsigned(bunch_ctr_in) ) );
      bunch_ctr_out <= std_logic_vector(bunch_ctr_tmp);
    end if;
  end process;

end architecture;
