-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Mixer data path
---- File       : mixer_datapath.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: Encapsulates the mixer (first) stage for the global 
----              algorithm.
----
----              It involves only the channel data mixer, a post-filter to
----              remove segment candidate duplicates and additional FIFO to 
----              join both components.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
use work.dt_common_pkg.all;
use work.dt_buffers_pkg.all;
use work.filters_pkg.all;
use work.globalpath_pkg.all;
use work.memory_pkg.all;
use work.mixer_pkg.all;
use work.registers_pkg.all;
use work.splitters_pkg.all;

entity MIXER_DATAPATH is
  port (
    clk_mixer       : in  std_logic;
    clk_filter      : in  std_logic;
    --
    rst_mixer       : in  std_logic;
    rst_filter      : in  std_logic;
    --
    mixerDatPathIn  : in  MixerDataPathIn_t;
    mixerDatPathOut : out MixerDataPathOut_t
  );
end entity;

architecture Behavioural of MIXER_DATAPATH is

  --signal rawHitFilterOut             : RawHitFilterOut_t;
  --signal notDupFifoOut               : DTRawHitFifoOut_t;
  signal glueDupFifoOut              : DTRawHitFifoOut_t;
  signal chMixerOut                  : ChannelMixerOut_t;
  signal ffDTRawSegCandOut           : DTSegCandidateFifoOut_t;
  signal candSplitterOut             : CandidateSplitterOut_t;
  signal ffSegCandidateOut_preDupe   : DTSegCandidateFifoOut_t;  
  signal segCandFilterOut            : SegCandidateFilterOut_t;
  --
  -- Signals for Candidate Splitter enable/bypass
  signal candidate_splitter_enable  : std_logic;
  signal segCandFFOut_full          : std_logic;
  signal ffPreDupeIn_segment        : DTSegCandidate_t;
  signal ffPreDupeIn_wrEna          : std_logic;
  --
  signal ff_transfer                : std_logic;
  signal bx_counter_mixer           : std_logic_vector(11 downto 0);
  signal bx_counter_segcand_dupfilt : std_logic_vector(11 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  mixerDatPathOut.segment_candidate <= segCandFilterOut.segment_candidate;
  mixerDatPathOut.segCandFFOut_wr   <= segCandFilterOut.fifoOut_wr;
  mixerDatPathOut.rawHitsFFIn_rd    <= ff_transfer;
  --
  mixerDatPathOut.ffRawSegCandidateStat <= ffDTRawSegCandOut.stat;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --RAW_HIT_DUP_FILT_INST : RAW_HIT_DUPLICATED_FILTER 
  --generic map (MEM_ELEMENTS => 16)
  --port map (
  --  clk             => clk_mixer,
  --  rst             => rst_mixer,
  --  rawHitFilterIn  => (
  --        rawHit        => mixerDatPathIn.rawHit,
  --        fifoIn_empty  => mixerDatPathIn.rawHitFFIn_empty,
  --        fifoOut_full  => notDupFifoOut.full,
  --        bx_counter    => bx_counter_mixer
  --  ),
  --  rawHitFilterOut => rawHitFilterOut
  --);
  --
  --NOTDUP_RAWHITS_FIFO : DTRAWHIT_FIFO port map (
  --  clkRd   => clk_mixer,
  --  clkWr   => clk_mixer,
  --  rst     => rst_mixer,
  --  fifoIn  => (
  --        rawHit              => rawHitFilterOut.rawHit,
  --        wrEna               => rawHitFilterOut.fifoOut_wr,
  --        rdEna               => ff_transfer,
  --        -- Dummy value. Unused in this fifo
  --        bx_time_convert_ena => '0'     
  --  ),
  --  fifoOut => notDupFifoOut
  --);

  ff_transfer <=
    (not mixerDatPathIn.rawHitFFIn_empty) and (not glueDupFifoOut.full);
  
  GLUE_RAWHITS_FIFO_INST : DTRAWHIT_GLUE_FIFO port map (
    clk     => clk_mixer,
    rst    => rst_mixer,
    fifoIn  => (
          rawHit              => mixerDatPathIn.rawHit,
          wrEna               => ff_transfer,
          rdEna               => chMixerOut.rawHitsFFIn_rd,
          -- Dummy value. Unused in this fifo
          bx_time_convert_ena => '0'     
    ),
    fifoOut => glueDupFifoOut
  );

  BXCNT_SYNC_MIXER : VECTOR_SHIFT_REGISTER
  generic map (VECTOR_WIDTH => 12, SHIFT_STEPS => 2)
  port map (clk => clk_mixer, enable => '1', 
            vector_in  => mixerDatPathIn.bx_counter,
            vector_out => bx_counter_mixer
  );

  MIXER_INST : CHANNEL_MIXER port map (
    clk      => clk_mixer,
    rst      => rst_mixer,
    chMixerIn  => (
          rawHit            => glueDupFifoOut.rawHit,
          rawHitFFIn_empty  => glueDupFifoOut.empty,
          --rawHit            => mixerDatPathIn.rawHit,
          --rawHitFFIn_empty  => mixerDatPathIn.rawHitFFIn_empty,
          segCandFFOut_full => segCandFFOut_full,
          reset_overflow    => mixerDatPathIn.reset_overflow,
          bx_counter        => bx_counter_mixer
    ),
    chMixerOut => chMixerOut
  );

  candidate_splitter_enable <=
    mixerDatPathIn.control_regs.candidate_splitter_enable;
    
  segCandFFOut_full <=  ffDTRawSegCandOut.full when
    candidate_splitter_enable = '1' else ffSegCandidateOut_preDupe.full;

  FF_PRE_SPLITTER : DTSEGCANDIDATE_FIFO port map (
    clkRd   => clk_filter,
    clkWr   => clk_mixer,
    rst     => rst_mixer,
    fifoIn  => (
          segment_candidate => chMixerOut.segment_candidate,
          wrEna             => chMixerOut.segCandFFOut_wr,
          rdEna             => candSplitterOut.fifoIn_rd
    ),
    fifoOut => ffDTRawSegCandOut
  );

  SPLITTER : CANDIDATE_SPLITTER port map (
    rst             => rst_filter,
    clk             => clk_filter,
    candSplitterIn  => (
          segment_candidate => ffDTRawSegCandOut.segment_candidate,
          fifoIn_empty      => ffDTRawSegCandOut.empty,
          fifoOut_full      => (ffSegCandidateOut_preDupe.full and 
                                  candidate_splitter_enable )
    ),
    candSplitterOut => candSplitterOut
  );  

  -- Switch between glue/normal fifo by commenting appropriate lines before rst
  -- Glue fifo is ok because the consumer is generally faster than the producer
  
  --FF_PRE_DUP_FILTER : DTSEGCANDIDATE_FIFO port map (
  GLUE_FF_PRE_DUP_FILTER : DTSEGCANDIDATE_GLUE_FIFO port map (
  --  clkRd   => clk_filter,
  --  clkWr   => clk_filter,
    clk     => clk_filter,
    rst     => rst_filter,
    fifoIn  => (
          segment_candidate => ffPreDupeIn_segment, 
          wrEna             => ffPreDupeIn_wrEna, 
          rdEna             => segCandFilterOut.fifoIn_rd
    ),
    fifoOut => ffSegCandidateOut_preDupe  
  );

  ffPreDupeIn_segment <= chMixerOut.segment_candidate
    when candidate_splitter_enable = '0' else
    candSplitterOut.segment_candidate;
  ffPreDupeIn_wrEna <= chMixerOut.segCandFFOut_wr
    when candidate_splitter_enable = '0' else
    candSplitterOut.fifoOut_wr;

  BXCNT_SYNC_CAND_DUPFILT : VECTOR_SHIFT_REGISTER
  generic map (VECTOR_WIDTH => 12, SHIFT_STEPS => 2)
  port map (clk => clk_filter, enable => '1', 
            vector_in  => mixerDatPathIn.bx_counter,
            vector_out => bx_counter_segcand_dupfilt
  );

  SEGCAND_DUP_FILTER_INST : SEGMENT_CANDIDATE_DUPLICATED_SHIFTING_FILTER
  generic map (MEM_ELEMENTS => 8)
  port map (
    clk              => clk_filter,
    rst              => rst_filter,
    segCandFilterIn  => (
          segment_candidate => ffSegCandidateOut_preDupe.segment_candidate,
          fifoIn_empty      => ffSegCandidateOut_preDupe.empty,
          fifoOut_full      => mixerDatPathIn.segCandFFOut_full,
          bx_counter        => bx_counter_segcand_dupfilt
    ),
    segCandFilterOut => segCandFilterOut
  );

end architecture;
