-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Common components
---- File       : dt_common_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-03
-------------------------------------------------------------------------------
---- Description: Package for common components/defined types for the global
----              design.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;
use work.float_point_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package dt_common_pkg is

  ---------------------------------------------------------------
  -- Useful constants
  ---------------------------------------------------------------
  -- Segment qualities. 
  -- They MUST HAVE the same length as 'DTSegment_t.quality'
  --
  constant HIGH       : std_logic_vector(2 downto 0) := "100";
  constant HIGH_GHOST : std_logic_vector(2 downto 0) := "011";
  constant LOW        : std_logic_vector(2 downto 0) := "010";
  constant LOW_GHOST  : std_logic_vector(2 downto 0) := "001";
  constant NO_PATH    : std_logic_vector(2 downto 0) := "000";

  -- Primitive qualities. 
  constant PRIM_4x4 : unsigned(2 downto 0) := to_unsigned(5, 3);
  constant PRIM_4x3 : unsigned(2 downto 0) := to_unsigned(4, 3);
  constant PRIM_3x3 : unsigned(2 downto 0) := to_unsigned(3, 3);
  constant PRIM_4x0 : unsigned(2 downto 0) := to_unsigned(2, 3);
  constant PRIM_3x0 : unsigned(2 downto 0) := to_unsigned(1, 3);
  constant PRIM_OFF : unsigned(2 downto 0) := to_unsigned(0, 3);
  
  -- Aliases for laterality
  constant LEFT  : std_logic := '0';
  constant RIGHT : std_logic := '1';

  ---------------------------------------------------------------
  -- Data types
  ---------------------------------------------------------------
  subtype TDCTime_t is std_logic_vector(16 downto 0);
  --
  -- This data type is inserted into the first trigger system FIFO.
  -- Mixer should use this type to get basic information from hit's tdc time
  -- and build a similar object ('DTHit_t') before grouping them into a
  -- segment candidate.
  --
  type DTRawHit_t is record
    tdc_time : TDCTime_t;

    ---------------------------------------------
    -- Hit configuration data
    ---------------------------------------------
    channel_id      : std_logic_vector(6 downto 0); -- ~ 96 channels per layer
    layer_id        : std_logic_vector(1 downto 0); -- 4 layers
    super_layer_id  : std_logic_vector(1 downto 0); -- 3 superlayer/chamber
  end record;

  -- Current value 28
  constant DTRAWHIT_SIZE : integer := DTRawHit_t.tdc_time'length        +
                                      DTRawHit_t.channel_id'length      +
                                      DTRawHit_t.layer_id'length        +
                                      DTRawHit_t.super_layer_id'length;

  type ChamberRawHit_arr_t is array (0 to 1) of DTRawHit_t;
                                      
  type DTTime_t is record
    ---------------------------------------------
    -- Cynematic parameters
    ---------------------------------------------
    tdc_time : TDCTime_t;
    -- 
    -- Bit to identify if this 'DTHit' is dummy or not.
    -- This is equivalent to minus sign of TDC value in the C++ test software 
    -- version
    --
    valid    : std_logic;
  end record;

  -- Current value 18
  constant DTTIME_SIZE : integer := DTTime_t.tdc_time'length + 1;

  -- Highest bit for tdc_time.
  constant T_MAXBIT : integer := DTTime_t.tdc_time'length - 1;

  type DTHit_t is record
    dt_time : DTTime_t;

    ---------------------------------------------
    -- Hit configuration data
    ---------------------------------------------
    channel_id      : std_logic_vector(6 downto 0); -- ~ 96 channels per layer
    layer_id        : std_logic_vector(1 downto 0); -- 4 layers
    super_layer_id  : std_logic_vector(1 downto 0); -- 3 superlayer/chamber
  end record;
  --
  -- WARNING !!!
  -- This constant must be changed if 'DTHit_t' fields change.
  -- Current value: 29 
  --
  constant DTHIT_SIZE : integer := DTTIME_SIZE                    +
                                   DTHit_t.channel_id'length      +
                                   DTHit_t.layer_id'length        +
                                   DTHit_t.super_layer_id'length;

  -- Array of hits comprised in a primitive.
  type HitsSegment_t is array (3 downto 0) of DTHit_t;

  -- Current value: 116 
  constant DTHITS_SEGMENT_SIZE : integer := DTHIT_SIZE * HitsSegment_t'length;

  ---------------------------------------------
  -- Constant and data types for containing 
  -- them, used inside mixer and analyzer
  ---------------------------------------------
  --
  -- Horizontal wire position differences with respect to the base wire,
  -- measured in cell's half-length units (adimensional).
  --
  type CellHorizLayout_arr_t is array (3 downto 0) of signed(2 downto 0);

  --
  -- This data type will be generated inside the mixer and sent 
  -- to the analyzer.
  --
  type DTSegCandidate_t is record
    hits              : HitsSegment_t;
    cell_horiz_layout : CellHorizLayout_arr_t;
  end record;

  -- Length for 4 hits plus 4 cell horizontal layouts (four 3-bit signed)
  constant CELL_LAYOUT_SIZE     : integer := 12;
  -- Current value: 128 
  constant DTSEG_CANDIDATE_SIZE : integer := DTHITS_SEGMENT_SIZE + 
                                             CELL_LAYOUT_SIZE;

  type DTSegMainParams_t is record
    hits : HitsSegment_t;
    --
    -- Laterality combination: 0 => 'Left'; 1 => 'Right'
    --
    laterality_combination : std_logic_vector( 3 downto 0);
    quality                : std_logic_vector( 2 downto 0);
    bx_time                : std_logic_vector(16 downto 0);
  end record;

  -- Current value: 140 
  constant DTSEG_MAINPARAMS_SIZE : integer := 
      DTHITS_SEGMENT_SIZE                             + 
      DTSegMainParams_t.laterality_combination'length + 
      DTSegMainParams_t.quality'length                + 
      DTSegMainParams_t.bx_time'length;

  -- 
  -- Distance magnitude is measured with 2 decimal bits of precision (bit 1-0)
  -- It's measured in "mm" (plus those 2 fixed-point additional bits).
  -- The length should be enough to fit more than 5 m in length.
  --
  subtype SignedDistance_t is signed(17 downto 0);
  constant SGN_DIST_SIZE : integer := SignedDistance_t'length;

  --
  -- Each element in this array will store the difference between horizontal
  -- hit position on each layer cell and the obtained position for the 
  -- associated segment ("horizontal_position").
  -- It's necessary to store them to allow later "chi square" calculations for
  -- the whole chamber, after phi superlayers' matching.
  -- Because those differences should not exceed 3 cell widths (in length)
  -- 13 bits ("DELTA_HIT_SIZE") should be enough to keep the value, its sign 
  -- and 4 additional precision bits.
  --
  subtype DeltaHit_t is signed(12 downto 0);
  
  type DeltaHit_arr_t is array (3 downto 0) of DeltaHit_t;
  constant DELTA_HIT_SIZE : integer := DeltaHit_t'length;
  
  type DTSegment_t is record
    main_params         : DTSegMainParams_t;
    phi_tangent_x4096   : signed(14 downto 0);
    --
    -- This is coded by a fixed-point value:
    --    horizontal_position(mm) => 1 sgn-bit + 13 main bits + 4 decimal bits.
    --
    horizontal_position : SignedDistance_t;
    delta_hit_x         : DeltaHit_arr_t;
    chi_square          : unsigned(23 downto 0);
    bx_id               : std_logic_vector(11 downto 0);
    bx_id_prec          : std_logic_vector(2 downto 0);
  end record;
  --
  -- WARNING !!!
  -- This constant must be changed if 'DTSegment_t' fields change.
  -- Software equivalent 'XCoorCell' can be obtained by adding/substracting 
  -- 'Hit driftDistance' to cell wire position (given by cellID) taking into 
  -- account the laterality combination.
  --
  -- Current value: 264 
  constant DTSEGMENT_SIZE : integer := 
      DTSEG_MAINPARAMS_SIZE                  + 
      DTSegment_t.phi_tangent_x4096'length   +
      DTSegment_t.horizontal_position'length +
      DELTA_HIT_SIZE * DeltaHit_arr_t'length +
      DTSegment_t.chi_square'length          +
      DTSegment_t.bx_id'length               +
      DTSegment_t.bx_id_prec'length;

  -- Segments array to store a whole superlayer final primitive.
  type DTSegment_arr_t is array (1 downto 0) of DTSegment_t;
  
  -- Current value: 528 
  constant DTSEGMENT_ARR_SIZE : integer := 2 * DTSEGMENT_SIZE;
  --
  -- This data type will store the final data for a primitive. Because it
  -- could be interesting to keep information from each individual segment that
  -- belongs to the primitive, all the information is still stored in the
  -- object, despite of its hugh size.
  -- In the end, those unused fields will be removed by the synthesizer during
  -- project compilation, so it's not a big problem.
  -- Here, trajectory specific fields are referred to the whole chamber, not
  -- to a defined superlayer.
  --
  type DTPrimitive_t is record
    paired_segments             : DTSegment_arr_t;
    valid_segments              : std_logic_vector(1 downto 0);
    horizontal_position_chamber : SignedDistance_t;
    phi_tangent_x4096_chamber   : signed(14 downto 0);
    chi_square_chamber          : unsigned(23 downto 0);
    bx_time_chamber             : std_logic_vector(16 downto 0);
    bx_id_chamber               : std_logic_vector(11 downto 0);
  end record;

  -- Current value: 616
  constant DTPRIMITIVE_SIZE : integer := 
      DTSEGMENT_ARR_SIZE                               +
      DTPrimitive_t.valid_segments'length              +
      DTPrimitive_t.horizontal_position_chamber'length +
      DTPrimitive_t.phi_tangent_x4096_chamber'length   +
      DTPrimitive_t.chi_square_chamber'length          +
      DTPrimitive_t.bx_time_chamber'length             +
      DTPrimitive_t.bx_id_chamber'length;

  type DTExtendedPrimitive_t is record
    primitive       : DTPrimitive_t;
    --
    -- These new angular tangent types are represented by a floating point 
    -- number following the standard IEEE-754. No subnormal numbers 
    -- are implemented.
    --
    -- These values should be expressed in "mm" as a base unit.
    --
    tan_cmsphi      : UFloat32_t;
    tan_cmsphi_bend : UFloat32_t;
  end record;

  constant DTEXT_PRIMITIVE_SIZE : integer := DTPRIMITIVE_SIZE + 2*UFLOAT32_SIZE;

  ---------------------------------------------
  -- Control registers
  ---------------------------------------------
  constant CHAMBER_OUTPUT  : std_logic_vector(1 downto 0) := "00";
  constant SL_PHI0_OUTPUT  : std_logic_vector(1 downto 0) := "01";
  constant SL_PHI1_OUTPUT  : std_logic_vector(1 downto 0) := "10";
  constant SL_THETA_OUTPUT : std_logic_vector(1 downto 0) := "11";

  type DTControlRegs_t is record
    candidate_splitter_enable       : std_logic;
    bx_time_convert_ena             : std_logic;
    bx_time_tolerance               : std_logic_vector(7 downto 0);
    chisqr_threshold                : std_logic_vector(15 downto 0);
    --
    qfilt_wait_expire_num_bx_stg1   : std_logic_vector(5 downto 0);
    qfilt_wait_expire_num_bx_stg2   : std_logic_vector(5 downto 0);
    --
    -- This is an absolute value, so it needs one bit less than its signed 
    -- counterpart "phi_tangent_x4096".
    --
    -- This first value is a cutting value tolerance for each superlayer.
    tanphi_x4096_threshold          : std_logic_vector(13 downto 0);
    -- This second value is the margin taken into account when matching 
    -- segments from two phi superlayers.
    slphi_match_bypass_tanphi_match : std_logic;
    slphi_match_tanphi_hardfilter   : std_logic;
    slphi_match_tanphi_x4096_thr    : std_logic_vector(13 downto 0);
    slphi_match_bx_wait_read_offset : std_logic_vector(8 downto 0);
    slphi_match_mem_bx_wait         : std_logic_vector(5 downto 0);
    --
    prim_chisqr_hardfilter          : std_logic;
    prim_chisqr_threshold           : std_logic_vector(15 downto 0);
    --
    primqfilt_wait_expire_num_bx    : std_logic_vector(5 downto 0);
    --
    ouput_trg_chamber_selector      : std_logic_vector(1 downto 0);
    --
    -- Horizontal SL3 offset measured from SL1's vertical axis.
    -- Signed, in "mm", without precision bits.
    --
    sl_phi3_x_offset                : signed(9 downto 0);
    --
    -- Parameters for chamber PHI and PHI-BEND calculations.
    --
    radio_mult_sin_delta_cmsphi     : UFloat32_t;
    radio_mult_cos_delta_cmsphi     : UFloat32_t;
  end record;

  ---------------------------------------------
  -- Status / information types
  ---------------------------------------------
  constant STAT_COUNT_LEN : integer := 32;

  type DTFifoStat_t is record
    count_in  : std_logic_vector(STAT_COUNT_LEN - 1 downto 0);
    count_out : std_logic_vector(STAT_COUNT_LEN - 1 downto 0);
  end record;

  ---------------------------------------------------------------
  -- Functions
  ---------------------------------------------------------------
  function slv_from_dtRawHit(rawHit : DTRawHit_t) return std_logic_vector;
  function dtRawHit_from_slv(data: std_logic_vector) return DTRawHit_t;

  function slv_from_dtTime(dtTime : DTTime_t) return std_logic_vector;
  function dtTime_from_slv(data: std_logic_vector) return DTTime_t;

  function slv_from_dtHit(hit : DTHit_t) return std_logic_vector;
  function dtHit_from_slv(data: std_logic_vector) return DTHit_t;

  function slv_from_hitsSegment(hitSeg : HitsSegment_t) 
    return std_logic_vector;

  function hitsSegment_from_slv(data: std_logic_vector) return HitsSegment_t;

  function slv_from_dtSegCandidate(segCandidate : DTSegCandidate_t) 
    return std_logic_vector;

  function dtSegCandidate_from_slv(data: std_logic_vector) 
    return DTSegCandidate_t;

  function slv_from_dtSegMainParams(segment_main : DTSegMainParams_t) 
    return std_logic_vector;

  function dtSegMainParams_from_slv(data: std_logic_vector) 
    return DTSegMainParams_t;

  function slv_from_DeltaHitArr(deltaArr : DeltaHit_arr_t) 
    return std_logic_vector;
    
  function DeltaHitArr_from_slv(data: std_logic_vector) 
    return DeltaHit_arr_t;
    
  function slv_from_dtSegment(segment : DTSegment_t) return std_logic_vector;
  function dtSegment_from_slv(data: std_logic_vector) return DTSegment_t;
  
  function slv_from_DTSegmentsArr(segment_arr : DTSegment_arr_t) 
    return std_logic_vector;

  function dtSegmentsArr_from_slv(data: std_logic_vector) 
    return DTSegment_arr_t;

  function slv_from_dtPrimitive(primitive : DTPrimitive_t) 
    return std_logic_vector;

  function dtPrimitive_from_slv(data: std_logic_vector) return DTPrimitive_t;

  function segment_is_high_q (segment: DTSegment_t) return std_logic;
  function segment_is_low_q  (segment: DTSegment_t) return std_logic;
  
  function segments_are_similar(s1, s2 : DTSegment_t) return std_logic;
  
end package;

-------------------------------------------------------------------------------
---- Body
-------------------------------------------------------------------------------
package body dt_common_pkg is
  ---------------------------------------------------------------
  -- 'DT Raw hits' flatten management
  ---------------------------------------------------------------
  function slv_from_dtRawHit(rawHit : DTRawHit_t) return std_logic_vector is
    variable dtRawHit_slv : std_logic_vector(DTRAWHIT_SIZE - 1 downto 0);
  begin
    dtRawHit_slv := rawHit.tdc_time & rawHit.channel_id &
                    rawHit.layer_id & rawHit.super_layer_id;

    return dtRawHit_slv;
  end function;
  
  function dtRawHit_from_slv(data: std_logic_vector) return DTRawHit_t is
    variable dtRawHit : DTRawHit_t;
  begin
    dtRawHit.tdc_time       := data(27 downto 11);
    dtRawHit.channel_id     := data(10 downto  4);
    dtRawHit.layer_id       := data( 3 downto  2);
    dtRawHit.super_layer_id := data( 1 downto  0);
    
    return dtRawHit;
  end function;

  ---------------------------------------------------------------
  -- 'DT Time' flatten management
  ---------------------------------------------------------------
  function slv_from_dtTime(dtTime : DTTime_t) return std_logic_vector is
    variable dtTime_slv : std_logic_vector(DTTIME_SIZE - 1 downto 0);
  begin
    dtTime_slv := dtTime.tdc_time & dtTime.valid;
      
    return dtTime_slv;
  end function;
  
  function dtTime_from_slv(data: std_logic_vector) return DTTime_t is
    variable dtTime : DTTime_t;
  begin
    dtTime.tdc_time := data(17 downto 1);
    dtTime.valid    := data(0);
  
    return dtTime;
  end function;

  ---------------------------------------------------------------
  -- HIT flatten management
  ---------------------------------------------------------------
  -- WARNING !!!
  -- This functions must be changed if 'DTHit_t' changes
  --
  -- Serializa un "DTHit" en forma de un "std_logic_vector" para poder 
  -- manipularlo en la diversas FIFO's
  --
  function slv_from_dtHit(hit : DTHit_t) return std_logic_vector is 
    variable dtHit_slv : std_logic_vector(DTHIT_SIZE - 1 downto 0);
  begin
    dtHit_slv := 
          slv_from_dtTime(hit.dt_time) & hit.channel_id &
                          hit.layer_id & hit.super_layer_id;

    return dtHit_slv;
  end function;
  --
  -- Operación contraria a la anterior. Convierte un "std_logic_vector" en
  -- un DTHit.
  --
  function dtHit_from_slv(data: std_logic_vector) return DTHit_t is 
    variable dtHit    : DTHit_t;
    variable time_slv : std_logic_vector(DTTIME_SIZE - 1 downto 0);
  begin
    time_slv := data(28 downto 11);

    dtHit.dt_time        := dtTime_from_slv(time_slv);
    dtHit.channel_id     := data(10 downto  4);
    dtHit.layer_id       := data( 3 downto  2);
    dtHit.super_layer_id := data( 1 downto  0);
    
    return dtHit;
  end function;

  ---------------------------------------------------------------
  -- HITS-SEGMENT flatten management
  ---------------------------------------------------------------
  --
  -- Converts a 'HitsSegment_t' into a serialized 'std_logic_vector'
  --
  function slv_from_hitsSegment(hitSeg : HitsSegment_t)
    return std_logic_vector
  is
    variable hitsSeg_slv : 
          std_logic_vector(DTHIT_SIZE * HitsSegment_t'length - 1 downto 0);
  begin
    for i in 0 to HitsSegment_t'length - 1 loop
      hitsSeg_slv(DTHIT_SIZE * (i+1) - 1 downto DTHIT_SIZE * i) := 
                  slv_from_dtHit( hitSeg(i) );
    end loop;
  
    return hitsSeg_slv;
  end function;
  --
  -- Performs the opposite operation from the previous function.
  --
  function hitsSegment_from_slv(data: std_logic_vector) 
    return HitsSegment_t
  is
    variable hitsSegment : HitsSegment_t;
    variable hit_slv     : std_logic_vector(DTHIT_SIZE - 1 downto 0);
  begin
    for i in 0 to HitsSegment_t'length - 1 loop
      hit_slv := data(DTHIT_SIZE * (i+1) - 1 downto DTHIT_SIZE * i);
    
      hitsSegment(i) := dtHit_from_slv( hit_slv );
    end loop;

    return hitsSegment;
  end function;

  ---------------------------------------------------------------
  -- SEGMENT CANDIDATE flatten management
  ---------------------------------------------------------------
  function slv_from_dtSegCandidate(segCandidate : DTSegCandidate_t) 
    return std_logic_vector
  is
    variable seg_candidate_slv : 
                  std_logic_vector(DTSEG_CANDIDATE_SIZE - 1 downto 0);
  begin
    seg_candidate_slv := 
          slv_from_hitsSegment(segCandidate.hits)             &
          std_logic_vector(segCandidate.cell_horiz_layout(3)) &
          std_logic_vector(segCandidate.cell_horiz_layout(2)) &
          std_logic_vector(segCandidate.cell_horiz_layout(1)) &
          std_logic_vector(segCandidate.cell_horiz_layout(0));
  
    return seg_candidate_slv;
  end function;

  function dtSegCandidate_from_slv(data: std_logic_vector) 
    return DTSegCandidate_t
  is
    variable seg_candidate : DTSegCandidate_t;
    variable hits_seg_slv  : std_logic_vector(DTHITS_SEGMENT_SIZE-1 downto 0);
  begin
    hits_seg_slv := data(DTSEG_CANDIDATE_SIZE - 1 downto CELL_LAYOUT_SIZE);

    seg_candidate.hits := hitsSegment_from_slv( hits_seg_slv );

    seg_candidate.cell_horiz_layout(3) := signed(data(11 downto 9));
    seg_candidate.cell_horiz_layout(2) := signed(data( 8 downto 6));
    seg_candidate.cell_horiz_layout(1) := signed(data( 5 downto 3));
    seg_candidate.cell_horiz_layout(0) := signed(data( 2 downto 0));

    return seg_candidate;
  end function;

  ---------------------------------------------------------------
  -- SEGMENT MAIN PARAMETERS flatten management
  ---------------------------------------------------------------
  --
  -- WARNING !!!
  -- This functions must be changed if 'DTSegMainParams_t' changes
  --
  function slv_from_dtSegMainParams(segment_main : DTSegMainParams_t) 
    return std_logic_vector
  is 
    variable dtSegMainPrms_slv : 
          std_logic_vector(DTSEG_MAINPARAMS_SIZE - 1 downto 0);
  begin
    dtSegMainPrms_slv(DTSEG_MAINPARAMS_SIZE - 1 downto 24) := 
          slv_from_hitsSegment(segment_main.hits);
    --
    dtSegMainPrms_slv(23 downto 0) := 
          segment_main.laterality_combination & 
          segment_main.quality                & 
          segment_main.bx_time;

    return dtSegMainPrms_slv;
  end function;
    
  function dtSegMainParams_from_slv(data: std_logic_vector) 
    return DTSegMainParams_t
  is 
    variable segment_main : DTSegMainParams_t;
    variable hits_seg_slv : std_logic_vector(DTHITS_SEGMENT_SIZE-1 downto 0);
  begin
    hits_seg_slv      := data(DTSEG_MAINPARAMS_SIZE - 1 downto 24);
    segment_main.hits := hitsSegment_from_slv(hits_seg_slv);
    --
    segment_main.laterality_combination := data(23 downto 20);
    segment_main.quality                := data(19 downto 17);
    segment_main.bx_time                := data(16 downto  0);
    
    return segment_main;
  end function;

  ---------------------------------------------------------------
  -- SEGMENT flatten management
  ---------------------------------------------------------------
  function slv_from_DeltaHitArr(deltaArr : DeltaHit_arr_t) 
    return std_logic_vector
  is
    variable arr_slv : signed((4 * DELTA_HIT_SIZE) - 1 downto 0);
  begin
    DELTA_HIT_ARR_LOOP : for i in 0 to 3 loop
    
        arr_slv(((i+1) * DELTA_HIT_SIZE) - 1 downto (DELTA_HIT_SIZE * i)) 
                := deltaArr(i);
    end loop;
    
    return std_logic_vector(arr_slv);
  end function;

  function DeltaHitArr_from_slv(data: std_logic_vector) 
    return DeltaHit_arr_t
  is
    variable answer : DeltaHit_arr_t;
  begin
    DELTA_HIT_ARR_LOOP : for i in 0 to 3 loop
      answer(i) := DeltaHit_t(
          data( ((i+1) * DELTA_HIT_SIZE) - 1 downto (DELTA_HIT_SIZE * i))
      );
    end loop;

    return answer;
  end function;
  --
  -- WARNING !!!
  -- This functions must be changed if 'DTSegment_t' changes
  --
  -- Serializa un "DTSegment_t" en forma de un "std_logic_vector" para poder 
  -- manipularlo en las diversas FIFO's
  --
  function slv_from_dtSegment(segment : DTSegment_t) 
    return std_logic_vector 
  is 
    variable dtSegment_slv : std_logic_vector(DTSEGMENT_SIZE - 1 downto 0);
  begin
    dtSegment_slv(DTSEGMENT_SIZE-1 downto DTSEGMENT_SIZE -DTSEG_MAINPARAMS_SIZE) 
          := slv_from_dtSegMainParams(segment.main_params);
    --
    dtSegment_slv(DTSEGMENT_SIZE - DTSEG_MAINPARAMS_SIZE - 1 downto 0) := 
          std_logic_vector(segment.phi_tangent_x4096)   &
          std_logic_vector(segment.horizontal_position) &
          --
          slv_from_DeltaHitArr(segment.delta_hit_x)     &
          --
          std_logic_vector(segment.chi_square)          &
          std_logic_vector(segment.bx_id)               &
          std_logic_vector(segment.bx_id_prec);

    return dtSegment_slv;
  end function;
  --
  -- Operación contraria a la anterior. Convierte un "std_logic_vector" en
  -- un 'DTSegment_t'.
  --
  function dtSegment_from_slv(data: std_logic_vector) 
    return DTSegment_t 
  is 
    variable segment       : DTSegment_t;
    variable seg_main_slv  : std_logic_vector(DTSEG_MAINPARAMS_SIZE-1 downto 0);
    variable delta_hit_slv : std_logic_vector((4*DELTA_HIT_SIZE) -1 downto 0);
  begin
    seg_main_slv        := data(DTSEGMENT_SIZE - 1 downto DTSEGMENT_SIZE - DTSEG_MAINPARAMS_SIZE);
    segment.main_params := dtSegMainParams_from_slv(seg_main_slv);
    --
    segment.phi_tangent_x4096   := signed(data(123 downto 109));
    segment.horizontal_position := signed(data(108 downto 91));
    --
    delta_hit_slv               := data(90 downto 39);
    segment.delta_hit_x         := DeltaHitArr_from_slv(delta_hit_slv);
    --
    segment.chi_square          := unsigned(data(38 downto 15));
    segment.bx_id               := data(14 downto 3);
    segment.bx_id_prec          := data(2 downto 0);

    return segment;
  end function;

  ---------------------------------------------------------------
  -- SEGMENTS' ARRAY flatten management
  ---------------------------------------------------------------
  --
  function slv_from_DTSegmentsArr(segment_arr : DTSegment_arr_t) 
    return std_logic_vector 
  is 
    variable slv_segments : std_logic_vector(DTSEGMENT_ARR_SIZE - 1 downto 0);
  begin
    slv_segments := slv_from_dtSegment( segment_arr(1) ) &
                    slv_from_dtSegment( segment_arr(0) );

    return slv_segments;
  end function;

  
  function dtSegmentsArr_from_slv(data: std_logic_vector) 
    return DTSegment_arr_t 
  is 
    variable segment     : std_logic_vector(DTSEGMENT_SIZE - 1 downto 0);
    variable segment_arr : DTSegment_arr_t;
  begin
    SEG_ARR_LOOP : for i in 0 to 1 loop
      segment := 
          data(((i + 1) * DTSEGMENT_SIZE) - 1 downto (DTSEGMENT_SIZE * i));

      segment_arr(i) := dtSegment_from_slv( segment );
    end loop;

    return segment_arr;
  end function;
  
  ---------------------------------------------------------------
  -- PRIMITIVE flatten management
  ---------------------------------------------------------------
  function slv_from_dtPrimitive(primitive : DTPrimitive_t) 
    return std_logic_vector 
  is 
    variable dtPrimitive_slv : std_logic_vector(DTPRIMITIVE_SIZE - 1 downto 0);
  begin
    dtPrimitive_slv(DTPRIMITIVE_SIZE - 1 downto (DTSEGMENT_SIZE + 88)) := 
          slv_from_dtSegment(primitive.paired_segments(1));

    dtPrimitive_slv(DTSEGMENT_SIZE + 87 downto 88) := 
          slv_from_dtSegment(primitive.paired_segments(0));
    --
    dtPrimitive_slv(87 downto 0) := primitive.valid_segments         &
             std_logic_vector(primitive.horizontal_position_chamber) &
             std_logic_vector(primitive.phi_tangent_x4096_chamber)   &
             std_logic_vector(primitive.chi_square_chamber)          &
             std_logic_vector(primitive.bx_time_chamber)             &
             std_logic_vector(primitive.bx_id_chamber);

    return dtPrimitive_slv;
  end function;

  function dtPrimitive_from_slv(data: std_logic_vector) 
    return DTPrimitive_t
  is 
    variable primitive : DTPrimitive_t;
    variable seg_phi1  : std_logic_vector(DTSEGMENT_SIZE - 1 downto 0);
    variable seg_phi0  : std_logic_vector(DTSEGMENT_SIZE - 1 downto 0);
  begin
    seg_phi1 := data(2 * DTSEGMENT_SIZE + 87 downto (DTSEGMENT_SIZE + 88));
    seg_phi0 := data(DTSEGMENT_SIZE + 87 downto 88);
    --
    primitive.paired_segments(1) := dtSegment_from_slv( seg_phi1 );
    primitive.paired_segments(0) := dtSegment_from_slv( seg_phi0 );
    --
    primitive.valid_segments              := data(87 downto 86);
    primitive.horizontal_position_chamber := signed(data(85 downto 68));
    primitive.phi_tangent_x4096_chamber   := signed(data(67 downto 53));
    primitive.chi_square_chamber          := unsigned(data(52 downto 29));
    primitive.bx_time_chamber             := data(28 downto 12);
    primitive.bx_id_chamber               := data(11 downto 0);

    return primitive;
  end function;
  
  ---------------------------------------------------------------
  -- SEGMENT COMPARISON functions
  ---------------------------------------------------------------
  --
  -- This function compares two segments, giving a single bit that indicates 
  -- if their are consider coincident or not 
  -- ('0' => not coincidents / '1' => coincidents)
  --
  -- In the mean time, the criteria is that they share, at least, one or 
  -- more TDC values in the same wire-layer combination.
  --
  function segments_are_similar(s1, s2 : DTSegment_t) return std_logic
  is
    variable similar_segments    : std_logic;
    variable tdcvalues_are_equal : std_logic_vector(3 downto 0);
  begin
    for i in 0 to 3 loop
      if 
--         (s1.main_params.hits(i).dt_time.tdc_time = 
--           s2.main_params.hits(i).dt_time.tdc_time)    and
          
         (s1.main_params.hits(i).channel_id = 
          s2.main_params.hits(i).channel_id)          and
          
         (s1.main_params.hits(i).super_layer_id = 
          s2.main_params.hits(i).super_layer_id)      and
          
          s1.main_params.hits(i).dt_time.valid = '1'  and 
          s2.main_params.hits(i).dt_time.valid = '1'
      then
        tdcvalues_are_equal(i) := '1';
      else
        tdcvalues_are_equal(i) := '0';
      end if;
    end loop;
    
    similar_segments := or_bits_slv( tdcvalues_are_equal );

    return similar_segments;
  end function;
  
  ---------------------------------------------------------------
  -- SEGMENT QUALITY measurement functions
  ---------------------------------------------------------------
  --
  function segment_is_high_q(segment: DTSegment_t) return std_logic
  is
    variable is_high : std_logic;
  begin

    if (segment.main_params.quality = HIGH_GHOST or 
        segment.main_params.quality = HIGH)
    then is_high := '1';
    else is_high := '0';
    end if;

    return is_high;
  end function;

  function segment_is_low_q(segment: DTSegment_t) return std_logic
  is
    variable is_low : std_logic;
  begin

    if (segment.main_params.quality = LOW_GHOST or 
        segment.main_params.quality = LOW) 
    then is_low := '1';
    else is_low := '0';
    end if;

    return is_low;
  end function;
  
end package body;
