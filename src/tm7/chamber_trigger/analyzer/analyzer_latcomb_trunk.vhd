-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Analyzer
---- File       : analyzer_latcomb_trunk.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Modified by: Alvaro Navarro <alvaro.navarro@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-08
-------------------------------------------------------------------------------
---- Description: First stage for the analyzer global processing unit.
----              This module extracts a segment candidate data from the input 
----              FIFO, and calculates all the possible fitted segment  
----              parameters for any of the 6 possible laterality combinations.
----              Finally, gives a computes a global quality for the candidate
----              taking into account the partial qualities given by each 
----              laterality combination analyzer.
----
----              This module takes 8 clock cycles (adds 2 in this module).
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.analyzer_pkg.all;
use work.dt_common_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.registers_pkg.all;
use work.minmax.all;
use work.bo2.all;

entity ANALYZER_LATCOMB_TRUNK is
  port (
    clk               : in  std_logic;
    rst               : in  std_logic;
    enable            : in  std_logic;
    AnLatcombTrunkIn  : in  AnLatcombTrunkIn_t;
    AnLatcombTrunkOut : out AnLatcombTrunkOut_t
  );
end entity;

architecture TwoProcess of ANALYZER_LATCOMB_TRUNK is

  type times_t is array(3 downto 0) of TDCTime_t;

  -- Internal signals to be registered.
  type reg_type is record
    c1_seg_candidate        : DTSegCandidate_t;
    c1_lat_consts_arr       : lat_consts_arr_t;
    c1_valid_sc             : std_logic;
    c1_times                : times_t;
    c1_min_coarse           : std_logic_vector(TDCTIME_COARSE_SIZE-1 downto 0);
    c1_max_coarse           : std_logic_vector(TDCTIME_COARSE_SIZE-1 downto 0);    
    c1_coarse_semicell_num  : signed(8 downto 0);-- neg in 3h without lyr 0?
    
    c2_seg_candidate        : DTSegCandidate_t;
    c2_coarse_offset        : std_logic_vector(TDCTIME_COARSE_SIZE-1 downto 0);
    c2_reduced_times        : reduced_times_arr_t;
    c2_lat_consts_arr       : lat_consts_arr_t;
    c2_xwire_mm             : xwire_mm_t;
    c2_coarse_pos           : SignedDistance_t;
  end record;

  constant regDefault : reg_type := (
    c1_seg_candidate        => DT_SEG_CAND_NULL,
    c1_lat_consts_arr       => LAT_CONSTS_ARR_NULL,
    c1_valid_sc             => '0',
    c1_times                => (others => (others => '0')),
    c1_min_coarse           => (others => '0'),
    c1_max_coarse           => (others => '0'),
    c1_coarse_semicell_num  => (others => '0'),

    c2_seg_candidate        => DT_SEG_CAND_NULL,
    c2_xwire_mm             => (others => (others => '0')),
    c2_coarse_offset        => (others => '0'),
    c2_reduced_times        => (others => (others => '0')),
    c2_lat_consts_arr       => LAT_CONSTS_ARR_NULL,
    c2_coarse_pos           => (others => '0')
  );
  signal reg, nextReg : reg_type := regDefault;

  -- Inter-components connection signals.
  signal segFitOut_arr         : segFitOut_arr_t;
  signal lat_consts_arr        : lat_consts_arr_t;
  signal slv_hits_vector_piped : 
                std_logic_vector(DTHITS_SEGMENT_SIZE - 1 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  AnLatcombTrunkOut.segCandFFIn_rd <= (not AnLatcombTrunkIn.segCandFFIn_empty)
                                   and (not rst) and enable;

  AnLatcombTrunkOut.hits <= hitsSegment_from_slv(slv_hits_vector_piped);
  --
  AnLatcombTrunkOut.segFitOut_arr <= segFitOut_arr;

  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, AnLatcombTrunkIn, lat_consts_arr)
    variable times : times_t; 
    variable max_coarse, min_coarse :
      std_logic_vector(TDCTIME_COARSE_SIZE-1 downto 0);
  begin
    -- Default values
    nextReg <= reg;
    
    -----------------
    -- Clock cycle #1
    -----------------

    --
    -- Muxer to fill with zeros the pipeline. At the same time it's used
    -- as a first-stage register (first-stage data pipeline) before sending
    -- them into CALC_BX_ONE_LATCOMB validators.
    --
    if AnLatcombTrunkIn.segCandFFIn_empty = '0' then
      --
      -- IMPORTANT: this first registered item is really a pipeline to 
      -- give time to 'LAYOUT_VALIDS_TO_LATCOMB_CONSTANTS_ENCODER' to convert 
      -- cell layout into laterality combinations.
      --
      nextReg.c1_seg_candidate <= AnLatcombTrunkIn.segment_candidate;
      --  
      -- This is a delayed (registered) value of lat-comb in order to set
      -- it's value in the same clock edge than it's associated 'cell layout'
      -- and 'hits' values.
      --
      nextReg.c1_lat_consts_arr <= lat_consts_arr;
      
      nextReg.c1_valid_sc <= '1';
      
    else
      nextReg.c1_seg_candidate <= DT_SEG_CAND_NULL;
      nextReg.c1_lat_consts_arr <= LAT_CONSTS_ARR_NULL;
      nextReg.c1_valid_sc <= '0';
    end if;

    -- Check if the valid hits are within a validity interval by checking that
    -- the difference in their coarse slices (downto bit 9 = 512) is 0 or 1.
    -- This in fact could allow for a maximum range of 1023 ns, but is just
    -- enough to guarantee that the reduction -> restoration works fine.
    max_coarse := (others => '0');
    min_coarse := (others => '1');
    for i in 3 downto 0 loop
      times(i) := AnLatcombTrunkIn.segment_candidate.hits(i).dt_time.tdc_time;
      if AnLatcombTrunkIn.segment_candidate.hits(i).dt_time.valid = '1' then
        min_coarse := minimum( min_coarse,
          times(i)(TDCTime_t'high downto TDCTIME_REDUCED_SIZE-1) );
        max_coarse := maximum( max_coarse,
          times(i)(TDCTime_t'high downto TDCTIME_REDUCED_SIZE-1) );
      end if;
    end loop;
    
    nextReg.c1_min_coarse <= min_coarse;
    nextReg.c1_max_coarse <= max_coarse;
    nextReg.c1_times      <= times ;
    
    
    -- we need to provide the number of semicells from the origin of the chamber
    -- to the local zero, which is the wire position of the layer 0's hit,
    -- no matter if it is a valid hit or not. Because the origin of the chamber
    -- matches the wirepos of layers 1 and 3 cell 0, we calculate it from 
    -- whichever of the 2 contains a valid hit, and correct this number with the
    -- cell_horiz_layout for that layer.
    if AnLatcombTrunkIn.segment_candidate.hits(1).dt_time.valid = '1' then
      nextReg.c1_coarse_semicell_num <=
        signed('0'& AnLatcombTrunkIn.segment_candidate.hits(1).channel_id &'0')
        - AnLatcombTrunkIn.segment_candidate.cell_horiz_layout(1);
    else
      nextReg.c1_coarse_semicell_num <=
        signed('0'& AnLatcombTrunkIn.segment_candidate.hits(3).channel_id &'0')
        - AnLatcombTrunkIn.segment_candidate.cell_horiz_layout(3);
    end if;

    -----------------
    -- Clock cycle #2
    -----------------
    for i in 3 downto 0 loop
      nextReg.c2_xwire_mm(i) <= resize(
        to_signed(21,6)*signed(reg.c1_seg_candidate.cell_horiz_layout(i)), 7);
    end loop;

    --
    -- If there is an input segment but their ranges are not within a validity
    -- interval (this will never happens if the mixer works fine but is
    -- anyway checked here), the pipeline is also null'ed, which has the effect
    -- of eliminating the segment (because it is read from the input FIFO).  
    --
    if 
      ( reg.c1_valid_sc = '1' and 
      (unsigned(reg.c1_max_coarse) - unsigned(reg.c1_min_coarse)) < 2 )
    then
      
      nextReg.c2_seg_candidate  <= reg.c1_seg_candidate ;
      nextReg.c2_lat_consts_arr <= reg.c1_lat_consts_arr;
      
      --
      -- Calculate the reduced version of the timestamps and the offset to 
      -- recover it: the offset is chosen so that there is at least one hit in
      -- the higher half of the reduced time range.
      --
      nextReg.c2_coarse_offset <=
        std_logic_vector(unsigned(reg.c1_max_coarse) - 1);
        
      for i in 3 downto 0 loop
        nextReg.c2_reduced_times(i) <= 
          (reg.c1_max_coarse(0) xnor reg.c1_times(i)(TDCTIME_REDUCED_SIZE-1)) &
          reg.c1_times(i)(TDCTIME_REDUCED_SIZE-2 downto 0);
      end loop;
      
      nextReg.c2_coarse_pos <= 
        resize( -- signed 18
          reg.c1_coarse_semicell_num * -- signed 9
          ( to_signed(21, 6) )  -- signed 6 + 4 precision
        , 14) & "0000";
    
    else
      nextReg.c2_seg_candidate <= DT_SEG_CAND_NULL;
      nextReg.c2_lat_consts_arr <= LAT_CONSTS_ARR_NULL;
      nextReg.c2_coarse_offset <= (others => '0');
      nextReg.c2_reduced_times <= (others => (others => '0'));
      nextReg.c2_coarse_pos    <= (others => '0');
    end if;

  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1' then 
        reg <= regDefault;
      elsif enable = '1' then
        reg <= nextReg;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  -- Calculates the horizontal distance amongst all wires involved in 
  -- calculations.
  --
  ENCODER_INST : LAYOUT_VALIDS_TO_LATCOMB_CONSTANTS_ENCODER port map (
    cell_horiz_layout => AnLatcombTrunkIn.segment_candidate.cell_horiz_layout, 
    valid             => (
      3 => AnLatcombTrunkIn.segment_candidate.hits(3).dt_time.valid,
      2 => AnLatcombTrunkIn.segment_candidate.hits(2).dt_time.valid,
      1 => AnLatcombTrunkIn.segment_candidate.hits(1).dt_time.valid,
      0 => AnLatcombTrunkIn.segment_candidate.hits(0).dt_time.valid),
    lat_consts_arr    => lat_consts_arr
  );
  --
  -- Laterality combination analyzers. 
  -- All possible 4-wire-group lateralites are analyzed in parallel. 
  -- This differs from software version, where only those compatible 
  -- with certain possible trajectory were calculated (tipically less 
  -- than 6 cases).
  --
  LAT_COMB_GEN : for i in 0 to 5 generate
    SEG_FIT : entity work.SEGMENT_FITTER port map (
      clk          => clk,
      rst          => rst,
      enable       => enable,
      control      => AnLatcombTrunkIn.control_regs,
      segFitIn  => (
            hits              => reg.c2_seg_candidate.hits,
            xwire_mm          => reg.c2_xwire_mm,
            coarse_offset     => reg.c2_coarse_offset,
            reduced_times     => reg.c2_reduced_times,
            coarse_pos        => reg.c2_coarse_pos,
            latcomb           => reg.c2_lat_consts_arr(i).latcomb,
            constants         => reg.c2_lat_consts_arr(i).constants
      ),
      segFitOut => segFitOut_arr(i)
    );
  end generate;
  --
  -- This pipeline needs 6 steps because the time taken by the module
  -- 'VALIDATE_4HITS_ONE_LATCOMB_REDUCED'
  --
  SEG_HIT_PIPELINE : VECTOR_SHIFT_REGISTER 
  generic map (VECTOR_WIDTH => DTHITS_SEGMENT_SIZE, SHIFT_STEPS => 6)
  port map (
    clk        => clk, 
    enable     => enable,
    vector_in  => slv_from_hitsSegment(reg.c2_seg_candidate.hits),
    vector_out => slv_hits_vector_piped
  );

end architecture;
