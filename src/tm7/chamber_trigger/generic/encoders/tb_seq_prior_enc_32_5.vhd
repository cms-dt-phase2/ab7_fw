library IEEE;
USE IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.all;
--
use work.encoders_pkg.all;
 
entity TB_SEQ_PRIOR_ENC_32_5 is
end entity;

architecture Behavior of TB_SEQ_PRIOR_ENC_32_5 is 

  -- Clock period definitions
  constant clk_period : time := 3 ns;

  ------------------------------------------------
  -- Auxiliary signals
  --
  signal clk       : std_logic := '0';
  signal rst       : std_logic := '0';
  signal load      : std_logic := '0';
  signal next_item : std_logic := '0';
  signal data      : std_logic_vector(31 downto 0);
  signal valid     : std_logic;
  signal code      : std_logic_vector(4 downto 0);

begin
  -- Instantiate the Unit Under Test (UUT)
  UUT: SEQUENTIAL_PRIOR_ENC_32_5 port map (
    clk       => clk,
    rst       => rst,
    load      => load,
    next_item => next_item ,
    data      => data,
    valid     => valid,
    code      => code      
  );

  -- Clock process definitions
  clk_process :process
  begin
    clk <= not clk;
    wait for clk_period/2;
  end process;

  -- Stimulus process
  stim_proc: process
  begin
    -- hold reset state for 100 ns.
    rst <= '1';
    wait for 100 ns;
    rst <= '0';
    
    data <= "00011000101010101000001000011010";
    wait for clk_period*10;

    wait until clk = '1';
    load <= '1';
    wait for 2*clk_period;
    load <= '0';
    
    for i in 1 to 20 loop
      wait until clk = '1';
      next_item <= '1';
      wait for 2*clk_period;
      next_item <= '0';
      
      wait for clk_period*10;
    end loop;


    wait until clk = '1';
    next_item <= '1';
    wait for clk_period;
    next_item <= '0';
    
    wait for clk_period*10;
    
    wait;
  end process;

end architecture;
