-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Duplicated segments filter
---- File       : primitive_chi2_splitting_filter.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-11
-------------------------------------------------------------------------------
---- Description: Rejects PRIMITIVES based their chi2 value.
----
----              Well, it really does not reject anything. It's a fake filter
----              because it splits a primitive into two one-segment-only
----              primitives in those cases where the chi2 threshold is 
----              overtaken.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.filters_pkg.all;

entity PRIMITIVE_CHI2_SPLITTING_FILTER is
  port (
    clk              : in  std_logic;
    rst              : in  std_logic;
    --
    -- I'm a bit bored of changing the code, so I add this flag that will force
    -- the filter to behave as a real filter that rejects primitives instead of 
    -- splitting them into their components.
    -- 
    hard_filtering   : in  std_logic := '0';
    chisqr_threshold : in  std_logic_vector(15 downto 0);
    primFilterIn     : in  PrimitiveFilterIn_t;
    primFilterOut    : out PrimitiveFilterOut_t
  );
end entity;

architecture TwoProcess of PRIMITIVE_CHI2_SPLITTING_FILTER is

  type state_type is (CHECK_CHI2, SEND_SECOND_SEGMENT);

  -- Internal signals to be registered.
  type reg_type is record
    state : state_type;
  end record;

  constant regDefault : reg_type := (
    state => CHECK_CHI2
  );
  signal reg, nextReg : reg_type := regDefault;
  
  signal low_chi2   : std_logic;
  signal fifoIn_rd  : std_logic := '0';
  signal fifoOut_wr : std_logic := '0';
  signal send_sl    : std_logic_vector(1 downto 0) := (others => '0');
  signal chi_square : unsigned(23 downto 0);  
  signal chi2_thrld : std_logic_vector(23 downto 0);
  
begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  primFilterOut.primitive  <= 
          primitive_splitter(primFilterIn.primitive, send_sl);

  primFilterOut.fifoIn_rd  <= fifoIn_rd;
  primFilterOut.fifoOut_wr <= fifoOut_wr;

  ---------------------------------------------------------------
  -- Auxiliary logic
  --
  chi2_thrld <= "0000" & chisqr_threshold & "0000";
  chi_square <= unsigned(primFilterIn.primitive.chi_square_chamber);
  --
  -- Here it's applied the same criteria as in "PATH_CALCULATOR" filtering 
  -- logic: it's assumed that no 3-hit-only primitive will have a chi_square
  -- value over the threshold for any reasonble limiting value used to 
  -- reject/split H-H/H-L/L-L primitives.
  --
  low_chi2 <= '1' when chi_square <= unsigned(chi2_thrld) else '0';

  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, primFilterIn, low_chi2, hard_filtering)
  begin
    -- Default values
    nextReg    <= reg;
    fifoIn_rd  <= '0';
    fifoOut_wr <= '0';
    send_sl    <= (others => '0');

    case reg.state is
      when CHECK_CHI2 =>
        if primFilterIn.fifoIn_empty = '0' then 
          send_sl(1) <= '1';
        
          if low_chi2 = '1' then
            send_sl(0) <= '1';

            if primFilterIn.fifoOut_full = '0' then 
              fifoIn_rd  <= '1';
              fifoOut_wr <= '1';
            end if;

          else
            if hard_filtering = '0' then
              -- SOFT filtering behaviour
              send_sl(0) <= '0';

              if primFilterIn.fifoOut_full = '0' then 
                fifoOut_wr    <= '1';
                nextReg.state <= SEND_SECOND_SEGMENT;
              end if;
            else 
              -- HARD (rejecting) filtering behaviour
              fifoIn_rd <= '1';
            end if ;
          end if;
        end if;

      when SEND_SECOND_SEGMENT =>
        send_sl(1) <= '0';
        send_sl(0) <= '1';

        if primFilterIn.fifoOut_full = '0' then 
          fifoOut_wr    <= '1';
          fifoIn_rd     <= '1';
          nextReg.state <= CHECK_CHI2;
        end if;
    
    end case;
  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1'
      then reg <= regDefault;
      else reg <= nextReg;
      end if;
    end if;
  end process;  

end architecture;
