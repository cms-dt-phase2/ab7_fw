-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Utilities package for encoders
---- File       : encoders_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-06
-------------------------------------------------------------------------------
---- Description: Encoders utilities package
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package encoders_pkg is

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component PRIORITY_ENC_8_3 is
  port (
    sel   : in  std_logic_vector(7 downto 0);
    valid : out std_logic;
    code  : out std_logic_vector(2 downto 0)
  );
  end component;  

  component SEQUENTIAL_PRIOR_ENC_32_5 is
  generic (BYPASS_INPUT_REG : boolean := false);
  port (
    clk       : in  std_logic;
    rst       : in  std_logic;
    load      : in  std_logic;
    next_item : in  std_logic;
    data      : in  std_logic_vector(31 downto 0);
    -- Registered data after "load".
    data_out  : out std_logic_vector(31 downto 0);
    valid     : out std_logic;
    code      : out std_logic_vector(4 downto 0)
  );
  end component;
  
end package;
