-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Filters
---- File       : filters_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-03
-------------------------------------------------------------------------------
---- Description: Package for components/defined types of different filter
----              units
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.commonfunc_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package filters_pkg is

  ---------------------------------------------------------------
  -- Data types
  ---------------------------------------------------------------
  type RawHitFilterIn_t is record
    rawHit        : DTRawHit_t;
    fifoIn_empty  : std_logic;
    fifoOut_full  : std_logic;
    bx_counter    : std_logic_vector(11 downto 0);
  end record;

  type RawHitFilterOut_t is record
    rawHit     : DTRawHit_t;
    fifoIn_rd  : std_logic;
    fifoOut_wr : std_logic;
  end record;

  type SegmentFilterIn_t is record
    segment       : DTSegment_t;
    fifoIn_empty  : std_logic;
    fifoOut_full  : std_logic;
    bx_counter    : std_logic_vector(11 downto 0);
  end record;

  type SegmentFilterOut_t is record
    segment    : DTSegment_t;
    fifoIn_rd  : std_logic;
    fifoOut_wr : std_logic;
  end record;

  type SegCandidateFilterIn_t is record
    segment_candidate : DTSegCandidate_t;
    fifoIn_empty      : std_logic;
    fifoOut_full      : std_logic;
    bx_counter        : std_logic_vector(11 downto 0);
  end record;

  type SegCandidateFilterOut_t is record
    segment_candidate : DTSegCandidate_t;
    fifoIn_rd         : std_logic;
    fifoOut_wr        : std_logic;
  end record;

  type PrimitiveFilterIn_t is record
    primitive     : DTPrimitive_t;
    fifoIn_empty  : std_logic;
    fifoOut_full  : std_logic;
    bx_counter    : std_logic_vector(11 downto 0);
  end record;

  type PrimitiveFilterOut_t is record
    primitive  : DTPrimitive_t;
    fifoIn_rd  : std_logic;
    fifoOut_wr : std_logic;
  end record;
  
  ---------------------------------------------------------------
  -- Functions
  ---------------------------------------------------------------
  function slv_from_dtSegHits_latComb(main_params : DTSegMainParams_t) 
      return std_logic_vector;

  function dummy_dtSegMainParams_from_slv(data: std_logic_vector) 
      return DTSegMainParams_t;
  
  function segment_cadidates_slv_are_similar(s1, s2 : std_logic_vector)
      return std_logic;

  function segmentHits_and_latComb_slv_are_equal(s1, s2 : std_logic_vector) 
      return std_logic;

  function raw_hits_slv_are_equal(h1, h2 : std_logic_vector) return std_logic;

  function primitives_are_similar(p1, p2 : DTPrimitive_t) return std_logic;
  function primitive_global_q(primitive: DTPrimitive_t) return unsigned;

  function primitive_splitter(primitive : DTPrimitive_t; 
                              selector  : std_logic_vector(1 downto 0)) 
      return DTPrimitive_t;
  
  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component RAW_HIT_DUPLICATED_FILTER is
  generic (MEM_ELEMENTS : integer := 8);
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    rawHitFilterIn  : in  RawHitFilterIn_t;
    rawHitFilterOut : out RawHitFilterOut_t
  );
  end component;

  component SEGMENT_CANDIDATE_DUPLICATED_FILTER is
  port (
    clk              : in  std_logic;
    rst              : in  std_logic;
    segCandFilterIn  : in  SegCandidateFilterIn_t;
    segCandFilterOut : out SegCandidateFilterOut_t
  );
  end component;

  component SEGMENT_CANDIDATE_DUPLICATED_SHIFTING_FILTER is
  generic (MEM_ELEMENTS : integer := 8);
  port (
    clk              : in  std_logic;
    rst              : in  std_logic;
    segCandFilterIn  : in  SegCandidateFilterIn_t;
    segCandFilterOut : out SegCandidateFilterOut_t
  );
  end component;
  
  component SEG_DUPLICATED_FILTER is
  port (
    clk          : in  std_logic;
    rst          : in  std_logic;
    segFilterIn  : in  SegmentFilterIn_t;
    segFilterOut : out SegmentFilterOut_t
  );
  end component;

  component SEG_DUPLICATED_SHIFTING_FILTER is
  generic (MEM_ELEMENTS : integer := 8);
  port (
    clk          : in  std_logic;
    rst          : in  std_logic;
    segFilterIn  : in  SegmentFilterIn_t;
    segFilterOut : out SegmentFilterOut_t
  );
  end component;

  component QUALITY_ENHANCER_FILTER is
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    expire_bx_num   : in  std_logic_vector(5 downto 0);
    qEnhanFilterIn  : in  SegmentFilterIn_t;
    qEnhanFilterOut : out SegmentFilterOut_t
  );
  end component;

  component CHAIN_QENHANCER_FILTER is
  port (
    clk                  : in  std_logic;
    rst                  : in  std_logic;
    control_regs         : in  DTControlRegs_t;
    chainQEnhanFilterIn  : in  SegmentFilterIn_t;
    chainQEnhanFilterOut : out SegmentFilterOut_t
  );
  end component;

  component PRIMITIVE_CHI2_SPLITTING_FILTER is
  port (
    clk              : in  std_logic;
    rst              : in  std_logic;
    hard_filtering   : in  std_logic := '0';
    chisqr_threshold : in  std_logic_vector(15 downto 0);
    primFilterIn     : in  PrimitiveFilterIn_t;
    primFilterOut    : out PrimitiveFilterOut_t
  );
  end component;
  
  component PRIMITIVE_DUPLICATED_SHIFTING_FILTER is
  generic (MEM_ELEMENTS : integer := 8);
  port (
    clk              : in  std_logic;
    rst              : in  std_logic;
    primFilterIn     : in  PrimitiveFilterIn_t;
    primFilterOut    : out PrimitiveFilterOut_t
  );
  end component;

  component PRIMITIVE_QUALITY_FILTER is
  port (
    clk               : in  std_logic;
    rst               : in  std_logic;
    expire_bx_num     : in  std_logic_vector(5 downto 0);
    primQualFilterIn  : in  PrimitiveFilterIn_t;
    primQualFilterOut : out PrimitiveFilterOut_t
  );
  end component;
  
end package;

-------------------------------------------------------------------------------
---- Body
-------------------------------------------------------------------------------
package body filters_pkg is

  ---------------------------------------------------------------
  -- Functions to flatten/unflatten certain segment main parameteres, 
  -- plus laterality.
  -- They are useful for the segment duplicated filter after 
  -- analysis.
  ---------------------------------------------------------------
  --
  -- This function returns an "std_logic_vector" by joining a flattened 
  -- 'HitsSegment_t' plus the laterality combination from a 
  -- 'DTSegMainParams_t'.
  --
  function slv_from_dtSegHits_latComb(main_params : DTSegMainParams_t) 
    return std_logic_vector 
  is 
    constant LATCOMB_LEN : integer := 
          DTSegMainParams_t.laterality_combination'length;

    constant VECTOR_LEN : integer := 
          DTHIT_SIZE * HitsSegment_t'length + LATCOMB_LEN;

    variable aux_slv : std_logic_vector(VECTOR_LEN - 1 downto 0);
  begin
    aux_slv(VECTOR_LEN - 1 downto LATCOMB_LEN) := 
          slv_from_hitsSegment(main_params.hits);
    --
    aux_slv(LATCOMB_LEN - 1 downto 0) := 
          std_logic_vector(main_params.laterality_combination);

    return aux_slv;
  end function;
  --
  -- Builds a 'DTSegMainParams_t' from a 'std_logic_vector' which contains 
  -- 4 'DTHit_t' and a 'laterality_combination'
  -- The rest of the 'DTSegMainParams_t' fields are filled (dummy) with '0'
  --
  function dummy_dtSegMainParams_from_slv(data: std_logic_vector) 
    return DTSegMainParams_t
  is 
    constant LATCOMB_LEN : integer := 
          DTSegMainParams_t.laterality_combination'length;

    constant VECTOR_LEN : integer := 
          DTHIT_SIZE * HitsSegment_t'length + LATCOMB_LEN;
  
    variable segment_main : DTSegMainParams_t;
    variable hits_seg_slv : std_logic_vector(DTHITS_SEGMENT_SIZE - 1 downto 0);
  begin
    hits_seg_slv      := data(VECTOR_LEN - 1 downto LATCOMB_LEN);
    segment_main.hits := hitsSegment_from_slv(hits_seg_slv);
    --
    segment_main.laterality_combination := data(LATCOMB_LEN -1 downto 0);
    segment_main.quality := (others => '0');
    segment_main.bx_time := (others => '0');
    
    return segment_main;
  end function;
  
  ---------------------------------------------------------------
  -- SEGMENTS, CANDIDATES, (and so on...) comparison functions
  ---------------------------------------------------------------
  --
  function segment_cadidates_slv_are_similar(s1, s2 : std_logic_vector) 
    return std_logic
  is
    variable similar_candidates  : std_logic;
    variable hits_are_equivalent : std_logic_vector(3 downto 0);
    variable seg_cand_1          : DTSegCandidate_t;
    variable seg_cand_2          : DTSegCandidate_t;
  begin
    seg_cand_1 := dtSegCandidate_from_slv(s1);
    seg_cand_2 := dtSegCandidate_from_slv(s2);

    for i in 0 to 3 loop
      if --seg_cand_1.cell_horiz_layout(i) = seg_cand_2.cell_horiz_layout(i) and
         (
           (
             (seg_cand_1.hits(i).dt_time.tdc_time = 
              seg_cand_2.hits(i).dt_time.tdc_time)   and
            
             (seg_cand_1.hits(i).channel_id = 
             seg_cand_2.hits(i).channel_id)         and
             
             (seg_cand_1.hits(i).super_layer_id = 
             seg_cand_2.hits(i).super_layer_id)     and
             
             seg_cand_1.hits(i).dt_time.valid = '1' and 
             seg_cand_2.hits(i).dt_time.valid = '1'
           )
        or 
           (seg_cand_1.hits(i).dt_time.valid = '0'  and 
            seg_cand_2.hits(i).dt_time.valid = '0'
           )
         )
      then
        hits_are_equivalent(i) := '1';
      else
        hits_are_equivalent(i) := '0';
      end if;
    end loop;

    similar_candidates := and_bits_slv( hits_are_equivalent );

    return similar_candidates;
  end function;

  function segmentHits_and_latComb_slv_are_equal(s1, s2 : std_logic_vector) 
    return std_logic
  is
    variable equal_segments : std_logic;
    variable hits_are_equal : std_logic_vector(3 downto 0);
    variable segmentMain_1  : DTSegMainParams_t;
    variable segmentMain_2  : DTSegMainParams_t;
  begin
    segmentMain_1 := dummy_dtSegMainParams_from_slv(s1);
    segmentMain_2 := dummy_dtSegMainParams_from_slv(s2);

    for i in 0 to 3 loop
      if (
          (segmentMain_1.hits(i).dt_time.tdc_time = 
           segmentMain_2.hits(i).dt_time.tdc_time)   and
           
          (segmentMain_1.hits(i).channel_id = 
           segmentMain_2.hits(i).channel_id)         and
           
          (segmentMain_1.hits(i).super_layer_id = 
           segmentMain_2.hits(i).super_layer_id)     and
           
          (segmentMain_1.laterality_combination(i) = 
           segmentMain_2.laterality_combination(i))  and
           
           segmentMain_1.hits(i).dt_time.valid = '1' and 
           segmentMain_2.hits(i).dt_time.valid = '1'
         ) 
        or 
         (segmentMain_1.hits(i).dt_time.valid = '0'  and 
          segmentMain_2.hits(i).dt_time.valid = '0')
      then
        hits_are_equal(i) := '1';
      else
        hits_are_equal(i) := '0';
      end if;
    end loop;

    equal_segments := and_bits_slv( hits_are_equal );

    return equal_segments;
  end function;

  --
  -- This function makes sense only to clarify the code. It would be more
  -- useful if logic to determine if two hits are equal or not increases in 
  -- complexity.
  --
  function raw_hits_slv_are_equal(h1, h2 : std_logic_vector) 
    return std_logic
  is
    variable equal_hits : std_logic;
  begin

    if h1 = h2 
    then equal_hits := '1';
    else equal_hits := '0';
    end if;

    return equal_hits;
  end function;

  ---------------------------------------------------------------
  -- PRIMITIVE COMPARISON functions
  ---------------------------------------------------------------
  --
  -- This function compares two primitives, giving a single bit that indicates 
  -- if their are consider coincident or not 
  -- ('0' => not coincidents / '1' => coincidents)
  --
  -- In the mean time, the criteria is that they share, at least, one or 
  -- more TDC values in the same wire-layer combination in any of the two
  -- gathered segments.
  --
  function primitives_are_similar(p1, p2 : DTPrimitive_t) return std_logic
  is
    constant ALL_DIFFERENT : std_logic_vector(7 downto 0) := (others => '0');
    --
    variable similar_primitives  : std_logic;
    variable tdcvalues_are_equal : std_logic_vector(7 downto 0);
    variable s0, s1              : DTSegment_t;
  begin
    for segnum in 0 to 1 loop
      s0 := p1.paired_segments(segnum);
      s1 := p2.paired_segments(segnum);

      for hitnum in 0 to 3 loop
        if (s0.main_params.hits(hitnum).dt_time.tdc_time = 
            s1.main_params.hits(hitnum).dt_time.tdc_time)   and
            
          (s0.main_params.hits(hitnum).channel_id = 
           s1.main_params.hits(hitnum).channel_id)          and
            
          (s0.main_params.hits(hitnum).super_layer_id = 
           s1.main_params.hits(hitnum).super_layer_id)      and
            
           s0.main_params.hits(hitnum).dt_time.valid = '1'  and 
           s1.main_params.hits(hitnum).dt_time.valid = '1'
        then
          tdcvalues_are_equal(4*segnum + hitnum) := '1';
        else
          tdcvalues_are_equal(4*segnum + hitnum) := '0';
        end if;
      end loop;
    end loop;

    if tdcvalues_are_equal = ALL_DIFFERENT
    then similar_primitives := '0';
    else similar_primitives := '1';
    end if;

    return similar_primitives;
  end function;

  ---------------------------------------------------------------
  -- PRIMITIVE QUALITY measurement functions
  ---------------------------------------------------------------
  --
  -- This returns a global quality for the primitive, considering that as the 
  -- sum of the segments qualities. It raises segment qualities form 1 to 2 and
  -- from 3 to 4. This way global quality will take values: 0, 2, 4, 6 or 8.
  --
  function primitive_global_q(primitive: DTPrimitive_t) return unsigned
  is
    variable qseg0, qseg1 : std_logic_vector(2 downto 0);
    variable qjoin        : unsigned(5 downto 0);
    variable quality      : unsigned(2 downto 0);
  begin
    -- Aliases.
    qseg0 := primitive.paired_segments(0).main_params.quality;
    qseg1 := primitive.paired_segments(1).main_params.quality;
    --
    -- Careful !!! This logic is linked side to side to the specific values
    -- defined for HIGH, ... LOW_GHOST constants.
    --
    qjoin := unsigned(qseg1 & qseg0);

    -- Explicit encoder to optimize synthesis.
    case qjoin is
      --------------------------------------------------
      -- 3 hits in only one of the segments
      --------------------------------------------------
      when unsigned(NO_PATH    & LOW_GHOST ) => quality := PRIM_3x0;
      when unsigned(NO_PATH    & LOW       ) => quality := PRIM_3x0;
      when unsigned(LOW_GHOST  & NO_PATH   ) => quality := PRIM_3x0;
      when unsigned(LOW        & NO_PATH   ) => quality := PRIM_3x0;
      --------------------------------------------------
      -- 4 hits in only one of the segments
      --------------------------------------------------
      when unsigned(NO_PATH    & HIGH_GHOST) => quality := PRIM_4x0;
      when unsigned(NO_PATH    & HIGH      ) => quality := PRIM_4x0;
      when unsigned(HIGH_GHOST & NO_PATH   ) => quality := PRIM_4x0;
      when unsigned(HIGH       & NO_PATH   ) => quality := PRIM_4x0;
      --------------------------------------------------
      -- 3 hits on both segments
      --------------------------------------------------
      when unsigned(LOW_GHOST  & LOW_GHOST ) => quality := PRIM_3x3;
      when unsigned(LOW_GHOST  & LOW       ) => quality := PRIM_3x3;
      when unsigned(LOW        & LOW_GHOST ) => quality := PRIM_3x3;
      when unsigned(LOW        & LOW       ) => quality := PRIM_3x3;
      --------------------------------------------------
      -- 3 hits on one segment and 4 hits on the other
      --------------------------------------------------
      when unsigned(LOW_GHOST  & HIGH_GHOST) => quality := PRIM_4x3;
      when unsigned(LOW_GHOST  & HIGH      ) => quality := PRIM_4x3;
      when unsigned(LOW        & HIGH_GHOST) => quality := PRIM_4x3;
      when unsigned(LOW        & HIGH      ) => quality := PRIM_4x3;
      when unsigned(HIGH_GHOST & LOW_GHOST ) => quality := PRIM_4x3;
      when unsigned(HIGH_GHOST & LOW       ) => quality := PRIM_4x3;
      when unsigned(HIGH       & LOW_GHOST ) => quality := PRIM_4x3;
      when unsigned(HIGH       & LOW       ) => quality := PRIM_4x3;
      --------------------------------------------------
      -- 4 hits on both segments
      --------------------------------------------------
      when unsigned(HIGH_GHOST & HIGH_GHOST) => quality := PRIM_4x4;
      when unsigned(HIGH_GHOST & HIGH      ) => quality := PRIM_4x4;
      when unsigned(HIGH       & HIGH_GHOST) => quality := PRIM_4x4;
      when unsigned(HIGH       & HIGH      ) => quality := PRIM_4x4;
      --
      when others => quality := PRIM_OFF;
    end case;

    return quality;
  end function;
  --
  -- Muxer to select which primitive components should be redirected to the
  -- output. It allows to send a whole primitive or only one of its segments
  -- taking into account the needed conversion for some of the global 
  -- data fields.
  --
  function primitive_splitter(primitive : DTPrimitive_t; 
                              selector  : std_logic_vector(1 downto 0))
  return DTPrimitive_t
  is
    variable prim_out : DTPrimitive_t;
  begin
    
    case selector is
      when "11" =>
        prim_out := primitive;

      when "10" =>
        prim_out.paired_segments(1)          := primitive.paired_segments(1);
        prim_out.valid_segments(1)           := primitive.valid_segments(1);
        prim_out.paired_segments(0)          := DT_SEGMENT_NULL;
        prim_out.valid_segments(0)           := '0';
        prim_out.horizontal_position_chamber := 
                primitive.paired_segments(1).horizontal_position;
        prim_out.phi_tangent_x4096_chamber   := 
                primitive.paired_segments(1).phi_tangent_x4096;
        prim_out.chi_square_chamber          := 
                primitive.paired_segments(1).chi_square;
        prim_out.bx_time_chamber             := 
                primitive.paired_segments(1).main_params.bx_time;
        prim_out.bx_id_chamber               := 
                primitive.paired_segments(1).bx_id;

      when "01" =>
        prim_out.paired_segments(1)          := DT_SEGMENT_NULL;
        prim_out.valid_segments(1)           := '0';
        prim_out.paired_segments(0)          := primitive.paired_segments(0);
        prim_out.valid_segments(0)           := primitive.valid_segments(0);
        prim_out.horizontal_position_chamber := 
                primitive.paired_segments(0).horizontal_position;
        prim_out.phi_tangent_x4096_chamber   := 
                primitive.paired_segments(0).phi_tangent_x4096;
        prim_out.chi_square_chamber          := 
                primitive.paired_segments(0).chi_square;
        prim_out.bx_time_chamber             := 
                primitive.paired_segments(0).main_params.bx_time;
        prim_out.bx_id_chamber               := 
                primitive.paired_segments(0).bx_id;

      when others =>
        prim_out := DT_PRIMITIVE_NULL;
    end case;

    return prim_out;
  end function;
  
end package body;
