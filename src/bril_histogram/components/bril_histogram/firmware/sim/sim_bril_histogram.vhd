----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: sim_bril_histogram - Behavioral
-- Description: 
-- 
--     Test bench for the histogrammer
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.bril_histogram_package.all;

entity sim_bril_histogram is
--  Port ( );
end sim_bril_histogram;

architecture Behavioral of sim_bril_histogram is

    -- constn
    constant NUMBER_OF_BINS     : natural := 3564;
    constant COUNTER_WIDTH      : natural := 21;
    constant INCREMENT_WIDTH    : natural := 3;

    signal clk_40MHz        : std_logic;
    signal reset            : std_logic;
    
    signal new_histogram    : std_logic;
    signal new_iteration    : std_logic;
    signal lumi_section     : std_logic_vector(31 downto 0);
    signal lumi_nibble      : std_logic_vector(31 downto 0);
    
    
    signal veto             : std_logic;
    signal increment        : std_logic_vector(2 downto 0);
    
    signal bin_counter      : natural range 0 to NUMBER_OF_BINS-1;
    
    signal fifo_rd_en       : std_logic := '0';
    signal fifo_empty       : std_logic;
    signal fifo_words_cnt   : std_logic_vector(31 downto 0);
    signal fifo_data        : std_logic_vector(31 downto 0);
    signal fifo_data_valid  : std_logic;
    signal package_size     : natural;
    signal readout_counter  : natural;

    type readout_state_type is (Idle, WaitingWords, ReadingOut);
    signal readout_state    : readout_state_type;    

begin

    clk_gen: process
    begin        
        clk_40MHz <= '1';
        wait for 12.5ns;
        clk_40MHz <= '0';
        wait for 12.5ns;
    end process;
   
    stimulus: process
        constant number_of_orbits : natural := 4;
    begin
        -- delay by just a bit so that the somulation is kind of consistent
        wait for 10ps;
        -- init
        new_histogram <= '0';
        new_iteration <= '0';
        lumi_section <= (others => '0');
        lumi_nibble <= (others => '0');
        -- reset
        reset <= '1';
        wait for 500ns;
        reset <= '0';
        -- time for c style - my first time in vhdl
        -- do several histograms 
        nhistos: for h in 0 to 2 loop
            -- do several orbits
            norbits: for o in 0 to number_of_orbits-1 loop
                -- now we started from bx0, new_histo and new_Iteration are not needed
                -- wait until the last been
                wait for (NUMBER_OF_BINS-1)*25ns;
                -- set the new iteration (orbit reset here)
                new_iteration <= '1';
                if o = number_of_orbits-1 then
                    new_histogram <= '1';
                    -- if
                    if lumi_nibble < 63 then
                        lumi_nibble <= lumi_nibble + 1;
                    else
                        lumi_nibble <= (others => '0');
                        lumi_section <= lumi_section + 1;
                    end if;
                end if;
                wait for 25ns;
                new_iteration <= '0';
                new_histogram <= '0';
            end loop norbits;
            
        end loop nhistos;   
    end process;
    
    bin_counter_prcs: process(clk_40MHz)
    begin
        if rising_edge(clk_40MHz) then
            if reset = '1' or new_iteration = '1' then
                bin_counter <= 0;
            else
                if bin_counter < NUMBER_OF_BINS-1 then
                    bin_counter <= bin_counter + 1;
                else
                    bin_counter <= 0;
                end if;
            end if;
        end if;
    end process;
    
    veto <= '0';
    increment_process: process(bin_counter)
    begin
        if bin_counter = 0 or bin_counter = 1 or bin_counter = 2 then
            increment <= "001";
        elsif bin_counter = 53 or bin_counter = 54 then
            increment <= "010";
        else
            increment <= (others => '0');
        end if;
    end process;
    
    bril_histogram_wrapper_inst: entity work.bril_histogram_wrapper
    Generic map (
        -- ident
        HISTOGRAM_TYPE          => 0,
        HISTOGRAM_ID            => 0,
        -- memory
        GENERATE_FIFO      => true,
        -- histogram parameters
        NUMBER_OF_BINS     => NUMBER_OF_BINS,
        COUNTER_WIDTH      => COUNTER_WIDTH,
        INCREMENT_WIDTH    => INCREMENT_WIDTH,
        -- error parameters
        NUMBER_OF_UNITS                 => 0,
        MAX_NUMBER_OF_MASKERROR_WORDS   => 0
    )
    Port map(
        --
        memory_reset_i      => reset,
        reset_i             => reset,
        clk_i               => clk_40MHz,
        -- tcds
        new_histogram_i     => new_histogram,
        new_iteration_i     => new_iteration,
        -- tcds data
        lhc_fill_i                      => (others => '0'),
        cms_run_i                       => (others => '0'),
        lumi_section_i                  => lumi_section,
        lumi_nibble_i                   => lumi_nibble,
        -- counter singals                
        increment_i         => increment,
        veto_i              => veto,
        -- fifo interface
        fifo_rd_clk_i       => clk_40MHz,
        fifo_rd_en_i        => fifo_rd_en,
        fifo_empty_o        => fifo_empty,
        fifo_words_cnt_o    => fifo_words_cnt,   
        --                  
        data_o              => fifo_data,
        data_valid_o        => fifo_data_valid
    );

    -- state machine which does the readout (let's do at 40)
    process(clk_40MHz)
    begin
        if rising_edge(clk_40MHz) then
            if reset = '1' then
                package_size <= 0;
                readout_counter <= 0;
                readout_state <= Idle;
                fifo_rd_en <= '0';
            else
                case readout_state is
                    when Idle => 
                        if fifo_empty = '0' and fifo_data_valid = '1' and fifo_words_cnt > 0 then
                            package_size <= to_integer(unsigned(fifo_data(15 downto 0)));
                            readout_counter <= 0;
                            fifo_rd_en <= '1';
                            readout_state <= WaitingWords;
                        end if;
                    when WaitingWords =>
                        fifo_rd_en <= '0';
                        if fifo_words_cnt >= package_size-1 then
                            fifo_rd_en <= '1';
                            readout_counter <= 1;
                            readout_state <= ReadingOut;
                        end if;
                    when ReadingOut =>
                        if readout_counter < package_size-1 then
                            readout_counter <= readout_counter + 1;
                            fifo_rd_en <= '1';
                        else
                            readout_counter <= 0;
                            fifo_rd_en <= '0';
                            readout_state <= Idle;
                        end if;
                    when others =>
                        readout_state <= Idle;
                end case;
            end if;
        end if;
    end process;
    
    
end Behavioral;
