-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Global data paths
---- File       : globalpath_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-05
-------------------------------------------------------------------------------
---- Description: Package for components/defined types of different splitters
----              units
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_constants_pkg.all;
use work.dt_common_pkg.all;
use work.sl_matcher_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package globalpath_pkg is

  ---------------------------------------------------------------
  -- Data types
  ---------------------------------------------------------------
  type MixerDataPathIn_t is record 
    rawHit            : DTRawHit_t;
    rawHitFFIn_empty  : std_logic;
    segCandFFOut_full : std_logic;
    --
    reset_overflow    : std_logic;
    bx_counter        : std_logic_vector(11 downto 0);
    control_regs      : DTControlRegs_t;
  end record;
  
  type MixerDataPathOut_t is record 
    segment_candidate : DTSegCandidate_t;
    segCandFFOut_wr   : std_logic;
    rawHitsFFIn_rd    : std_logic;
    --
    ffRawSegCandidateStat : DTFifoStat_t;
  end record;

  type AnalyzerDataPathIn_t is record 
    control_regs      : DTControlRegs_t;
    bx_counter        : std_logic_vector(11 downto 0);
    segment_candidate : DTSegCandidate_t;
    segCandFFIn_empty : std_logic;  
    segmentFFOut_full : std_logic;
  end record;

  type AnalyzerDataPathOut_t is record
    segment         : DTSegment_t;
    segmentFFOut_wr : std_logic;
    segCandFFIn_rd  : std_logic;
    --
    ffMainParamStat        : DTFifoStat_t;
    ffSegAfterAnalysisStat : DTFifoStat_t;
    ffSegPostDupStat       : DTFifoStat_t;
  end record;

  type SLDataPathStat_t is record 
    ffIncomingRawHitsStat  : DTFifoStat_t;
    ffRawSegCandidateStat  : DTFifoStat_t;
    ffSegmentCandidateStat : DTFifoStat_t;
    ffMainParamStat        : DTFifoStat_t;
    ffSegAfterAnalysisStat : DTFifoStat_t;
    ffSegPostDupStat       : DTFifoStat_t;
    ffOutgoingSegmentStat  : DTFifoStat_t;
  end record;
  
  type SegCandFifoInterceptIn_t is record
    segment_candidate : DTSegCandidate_t;
    wrEna             : std_logic;
  end record;
  
  type SegCandFifoInterceptIn_arr_t is array (0 to MAX_SL_UNITS_PER_DESIGN - 1) 
                                       of SegCandFifoInterceptIn_t;
  
  type SuperLayerDataPathIn_t is record 
    rawHit              : DTRawHit_t;
    rawHitFFIn_wr       : std_logic;
    segmentFFOut_rd     : std_logic;
    segcandFFIntrcptIn  : SegCandFifoInterceptIn_t;
    --
    reset_overflow      : std_logic;
    bx_counter          : std_logic_vector(11 downto 0);
    control_regs        : DTControlRegs_t;
  end record;

  type SegCandFifoInterceptOut_t is record
    segment_candidate : DTSegCandidate_t;
    wrEna             : std_logic;
    clk               : std_logic;
  end record;
  
  type SegCandFifoInterceptOut_arr_t is array (0 to MAX_SL_UNITS_PER_DESIGN - 1) 
                                        of SegCandFifoInterceptOut_t;
  
  type SuperLayerDataPathOut_t is record 
    segment             : DTSegment_t;
    segmentFFOut_empty  : std_logic;
    rawHitsFFIn_full    : std_logic;
    segcandFFIntrcptOut : SegCandFifoInterceptOut_t;
    statistics          : SLDataPathStat_t;
  end record;
  
  type SLPhiMatchDataPathIn_t is record
    segment_phi          : DTSegment_arr_t;
    segmentPhiFFIn_empty : std_logic_vector(1 downto 0);
    primitiveFFOut_full  : std_logic;
    bx_counter           : std_logic_vector(11 downto 0);
    control_regs         : DTControlRegs_t;
  end record;

  type SLPhiMatchDataPathOut_t is record
    primitive         : DTPrimitive_t;
    primValidFFOut_wr : std_logic;
    segmentFF_rd      : std_logic_vector(1 downto 0);
    ffPostDupPrimStat : DTFifoStat_t;
  end record;
  
  type SLDataPathIn_arr_t is array (0 to MAX_SL_UNITS_PER_DESIGN - 1) 
                           of SuperLayerDataPathIn_t;

  type SLDataPathOut_arr_t is array (0 to MAX_SL_UNITS_PER_DESIGN - 1) 
                           of SuperLayerDataPathOut_t;
                           
  --
  -- CHAMBER GLOBAL-DATA-PATH data types
  --
  type ChGlobalDataPathIn_t is record
    rawHitPhi           : ChamberRawHit_arr_t;
    rawHitFFPhi_wr      : std_logic_vector(1 downto 0);
    primitiveFFOut_rd   : std_logic;
    fifoInterceptIn     : SegCandFifoInterceptIn_arr_t;
    reset_overflow      : std_logic;
    bx_counter          : std_logic_vector(11 downto 0);
    control_regs        : DTControlRegs_t;
  end record;

  type ChGlobalDataPathOut_t is record
    primitive                  : DTPrimitive_t;
    primitiveFFOut_empty       : std_logic;
    rawHitsFFPhi_full          : std_logic_vector(1 downto 0);
    fifoInterceptOut           : SegCandFifoInterceptOut_arr_t;
    statistics_sl0             : SLDataPathStat_t;
    statistics_sl1             : SLDataPathStat_t;
    ffPostDupFiltPrimitiveStat : DTFifoStat_t;
    ffOutgoingPrimitiveStat    : DTFifoStat_t;
  end record;

  ---------------------------------------------------------------
  -- Functions
  ---------------------------------------------------------------
  function primitive_quality_encoder(primitive : DTPrimitive_t) 
    return std_logic_vector;
  
  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component MIXER_DATAPATH is
  port (
    clk_mixer       : in  std_logic;
    clk_filter      : in  std_logic;
    --
    rst_mixer       : in  std_logic;
    rst_filter      : in  std_logic;
    --
    mixerDatPathIn  : in  MixerDataPathIn_t;
    mixerDatPathOut : out MixerDataPathOut_t
  );
  end component;

  component ANALYSIS_DATAPATH is
  port (
    clk_analyzer       : in  std_logic;
    clk_dup_filter     : in  std_logic;
    clk_qenhan_filter  : in  std_logic;
    --
    rst_analyzer       : in  std_logic;
    rst_dup_filter     : in  std_logic;
    rst_qenhan_filter  : in  std_logic;
    --
    analyzerDatPathIn  : in  AnalyzerDataPathIn_t;
    analyzerDatPathOut : out AnalyzerDataPathOut_t
  );
  end component;

  component SUPERLAYER_DATAPATH is
    port (
      clk_fifo_rd          : in  std_logic;
      clk_fifo_wr          : in  std_logic;
      clk_mixer            : in  std_logic;
      clk_analyzer         : in  std_logic;
      clk_dup_filter       : in  std_logic;
      clk_qenhan_filter    : in  std_logic;
      --
      rst_fifo_rd          : in  std_logic;
      rst_fifo_wr          : in  std_logic;
      rst_mixer            : in  std_logic;
      rst_analyzer         : in  std_logic;
      rst_dup_filter       : in  std_logic;
      rst_qenhan_filter    : in  std_logic;
      --
      superLayerDatPathIn  : in  SuperLayerDataPathIn_t;
      superLayerDatPathOut : out SuperLayerDataPathOut_t
    );
  end component;

  component SLPHI_MATCHER_DATAPATH is
  port (
    clk                 : in  std_logic;
    rst                 : in  std_logic;
    slPhiMatcherPathIn  : in  SLPhiMatchDataPathIn_t;
    slPhiMatcherPathOut : out SLPhiMatchDataPathOut_t
  );
  end component;
  
  component CHAMBER_DATAPATH is
  port (
    clk_fifo_rd        : in  std_logic;
    clk_fifo_wr        : in  std_logic;
    clk_mixer          : in  std_logic;
    clk_analyzer       : in  std_logic;
    clk_dup_filter     : in  std_logic;
    clk_qenhan_filter  : in  std_logic;
    clk_sl_matcher     : in  std_logic;
    --
    rst_fifo_rd        : in  std_logic;
    rst_fifo_wr        : in  std_logic;
    rst_mixer          : in  std_logic;
    rst_analyzer       : in  std_logic;
    rst_dup_filter     : in  std_logic;
    rst_qenhan_filter  : in  std_logic;
    rst_sl_matcher     : in  std_logic;
    --
    chamGlobDatPathIn  : in  ChGlobalDataPathIn_t;
    chamGlobDatPathOut : out ChGlobalDataPathOut_t
  );
  end component;

end package;

-------------------------------------------------------------------------------
---- Body
-------------------------------------------------------------------------------
package body globalpath_pkg is

  function primitive_quality_encoder(primitive : DTPrimitive_t) 
  return std_logic_vector
  is
    variable quality     : std_logic_vector(3 downto 0);
    variable q_seg0      : std_logic_vector(2 downto 0);
    variable q_seg1      : std_logic_vector(2 downto 0);
    variable valid_hits0 : std_logic_vector(3 downto 0);
    variable valid_hits1 : std_logic_vector(3 downto 0);
    --
    variable all_valid : std_logic_vector(1 downto 0);
  begin
    -- Aliases
    q_seg0 := primitive.paired_segments(0).main_params.quality;
    q_seg1 := primitive.paired_segments(1).main_params.quality;

    -- Concatenate "valid" segment hits into a single vector.
    for i in 0 to 3 loop
      valid_hits0(i) := 
          primitive.paired_segments(0).main_params.hits(i).dt_time.valid;

      valid_hits1(i) := 
          primitive.paired_segments(1).main_params.hits(i).dt_time.valid;
    end loop;
  
    if valid_hits0 = "1111" 
    then all_valid(0) := '1';
    else all_valid(0) := '0';
    end if;

    if valid_hits1 = "1111" 
    then all_valid(1) := '1';
    else all_valid(1) := '0';
    end if;
  
    if primitive.valid_segments = "01" then
      quality := std_logic_vector(resize(unsigned(q_seg0), 4));

    elsif primitive.valid_segments = "10" then
      quality := std_logic_vector(resize(unsigned(q_seg1), 4));

    elsif primitive.valid_segments = "00" then
      quality := std_logic_vector(to_unsigned(0, 4));

    else
      -- 4 hits valid on each segment
      if all_valid = "11" then    
        quality := std_logic_vector(to_unsigned(9, 4));
      -- 3 hits valid on each segment
      elsif all_valid = "00" then
        quality := std_logic_vector(to_unsigned(6, 4));
      -- 4 hits valid on one segment and 3 valid on the other one
      else
        quality := std_logic_vector(to_unsigned(8, 4));
      end if;
    end if;

    return quality;
  end function;

end package body;
