-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Duplicated segments filter
---- File       : seg_duplicated_shifting_filter.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-11
-------------------------------------------------------------------------------
---- Description: Rejects duplicated SEGMENTS based on comparing different
----              segment field values.
----              It uses a shifting mechanism with MEM_ELEMENTS positions 
----              to get in 1 cycle the resulting comparison.
----              Additionally, it spends one clock cycle (in the mean time
----              around BX = 0) to clean the internal memories to avoid
----              refusing segments with the same TDC values but belonging
----              to different orbits.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;
use work.dt_common_pkg.all;
use work.filters_pkg.all;

entity SEG_DUPLICATED_SHIFTING_FILTER is
  generic (MEM_ELEMENTS : integer := 8);
  port (
    clk          : in  std_logic;
    rst          : in  std_logic;
    segFilterIn  : in  SegmentFilterIn_t;
    segFilterOut : out SegmentFilterOut_t
  );
end entity;

architecture Behavioural of SEG_DUPLICATED_SHIFTING_FILTER is

  constant BX_ZERO : std_logic_vector(11 downto 0) := (others => '0');
  
  constant SEGMENT_HITS_SIZE : integer := DTHIT_SIZE * HitsSegment_t'length;
  constant LATCOMB_SIZE      : integer := 
        DTSegMainParams_t.laterality_combination'length;

  constant DATA_SIZE : integer := SEGMENT_HITS_SIZE  + LATCOMB_SIZE;
  
  subtype seghits_latcomb_slv is std_logic_vector(DATA_SIZE - 1 downto 0);
  type seghits_latcomb_shift_reg_t is array (MEM_ELEMENTS - 1 downto 0) 
                                   of seghits_latcomb_slv;

  -- Internal registers
  signal data_reg           : seghits_latcomb_shift_reg_t;
  signal bx_counter_lsb     : std_logic;
  signal bx_counter_changed : std_logic;
  
  -- Auxiliary signals
  signal check_data : std_logic;
  signal send_data  : std_logic;
  signal clear_regs : std_logic;
  signal bx_counter : std_logic_vector(11 downto 0);
  signal duplicated : std_logic_vector(MEM_ELEMENTS - 1 downto 0);
  signal data_in    : std_logic_vector(DATA_SIZE - 1 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  segFilterOut.segment    <= segFilterIn.segment;
  segFilterOut.fifoIn_rd  <= check_data;
  segFilterOut.fifoOut_wr <= send_data;

  ---------------------------------------------------------------
  -- Useful aliases 
  --
  data_in    <= slv_from_dtSegHits_latComb(segFilterIn.segment.main_params);
  bx_counter <= segFilterIn.bx_counter;

  ---------------------------------------------------------------
  -- Combinatory logic
  --
  clear_regs <= '1' when bx_counter = BX_ZERO and bx_counter_changed = '1' 
                    else '0';
  
  check_data <= not segFilterIn.fifoOut_full and 
                not segFilterIn.fifoIn_empty and 
                not clear_regs;

  COMPARISIONS : for i in 0 to MEM_ELEMENTS - 1 generate
    duplicated(i) <= 
          segmentHits_and_latComb_slv_are_equal(data_in, data_reg(i));
  end generate;

  send_data <= check_data and (not or_bits_slv(duplicated));

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst, send_data)
  begin
    if clk = '1' and clk'event then
      if rst = '1' then 
        bx_counter_lsb     <= '0';
        bx_counter_changed <= '0';
      else 
        bx_counter_lsb <= bx_counter(0);

        -- BX counter value change detector.
        bx_counter_changed <= bx_counter_lsb and (not bx_counter(0));
      end if;
    
      if rst = '1' or clear_regs = '1' then 
        data_reg <= (others => (others => '0'));
      elsif send_data = '1' then 
        data_reg <= data_reg(MEM_ELEMENTS - 2 downto 0) & data_in;
      end if;

    end if;
  end process;

end architecture;
