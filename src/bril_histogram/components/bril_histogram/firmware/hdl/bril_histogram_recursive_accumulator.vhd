----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_recursive_accumulator - rtl
-- Description: 
-- 
--      Performs recursive vector accumulation
--      We use the usual constrcut to generate a sum of n elements, takes one clock cycle.
--      however the complexity rises very fast, that's why we have to limit it by setting NUMBER_OF_ADDS_PER_STAGE < NUMBER_OF_UNITS and apply recursion
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bril_histogram_recursive_accumulator is
Generic (
    NUMBER_OF_UNITS                 : natural;
    UNIT_INCREMENT_WIDTH            : natural;
    NUMBER_OF_DAUGHTERS_PER_STAGE   : natural;
    -- this two values are very important for timing (otherwise increment_i and mask_i go out of sync)
    TOTAL_NUMBER_OF_LAYERS          : natural;
    THIS_LAYER_ID                   : natural := 0 -- never use outside of recursion, it has to be zero for the first recursion level
);
Port (
    --
    clk_i                       : in std_logic;
    --
    increment_i                 : in std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0);
    mask_i                      : in std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    --
    total_o                     : out std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0)
);
end bril_histogram_recursive_accumulator;

architecture rtl of bril_histogram_recursive_accumulator is

    -- define the type which represnts number of units to be fed to each of subsequent stages
    type number_of_units_per_daughter_type is array(NUMBER_OF_DAUGHTERS_PER_STAGE-1 downto 0) of natural;
    -- function to get this number, will be executed only during the compilation
    function get_number_of_units_per_daughter return number_of_units_per_daughter_type is
        variable units_left         : natural;
        variable current_daughter   : natural;
        variable result             : number_of_units_per_daughter_type;
    begin
        -- start with n units
        result := (others => 0);
        units_left := NUMBER_OF_UNITS;
        current_daughter := 0;
        -- iterate over n daughters
        while units_left > 0 loop
            if units_left > NUMBER_OF_DAUGHTERS_PER_STAGE then
                -- increment number of units for this daughter
                result(current_daughter) := result(current_daughter) + NUMBER_OF_DAUGHTERS_PER_STAGE;
                units_left := units_left - NUMBER_OF_DAUGHTERS_PER_STAGE;
                -- go for the next one (start from the beginning if we collected NUMBER_OF_DAUGHTERS_PER_STAGE for each daughter)
                if current_daughter < NUMBER_OF_DAUGHTERS_PER_STAGE-1 then
                    current_daughter := current_daughter + 1;
                else
                    current_daughter := 0;
                end if;
            else
                -- in this case we are done
                result(current_daughter) := result(current_daughter) + units_left;
                units_left := 0;
            end if;
        end loop;
        -- return the result
        return result;
    end function;
    -- maximum size of the output vector
    function get_max_number_of_units_per_daughter ( number_of_units_per_daughter_i : in number_of_units_per_daughter_type ) return natural is
        variable result : natural;
    begin
        -- start from 0
        result := 0;
        -- calculate max
        for i in 0 to NUMBER_OF_DAUGHTERS_PER_STAGE-1 loop
            if number_of_units_per_daughter_i(i) > result then
                result := number_of_units_per_daughter_i(i);
            end if;
        end loop;
        -- 
        return result;
    end function;
    -- get the data offset for the daughters (to assign the input data
    function get_daughter_data_offsets ( number_of_units_per_daughter_i : in number_of_units_per_daughter_type ) return number_of_units_per_daughter_type is
        variable result : number_of_units_per_daughter_type;
    begin
        -- all are zeros
        result := (others => 0);
        -- result(0) remains 0, whereas the others are incremented
        for i in 1 to NUMBER_OF_DAUGHTERS_PER_STAGE-1 loop
            result(i) := result(i-1) + number_of_units_per_daughter_i(i-1);
        end loop;
        -- return
        return result;
    end function;
    
    -- get the constats
    constant NUMBER_OF_UNITS_PER_DAUGHTER       : number_of_units_per_daughter_type := get_number_of_units_per_daughter;
    constant MAX_NUMBER_OF_UNITS_PER_DAUGHTER   : natural := get_max_number_of_units_per_daughter(NUMBER_OF_UNITS_PER_DAUGHTER);
    constant DAUGHTER_DATA_OFFSETS              : number_of_units_per_daughter_type := get_daughter_data_offsets(NUMBER_OF_UNITS_PER_DAUGHTER);

    -- type to contain the outputs of the daughters
    type data_to_sum_type is array(NUMBER_OF_DAUGHTERS_PER_STAGE-1 downto 0) of std_logic_vector(MAX_NUMBER_OF_UNITS_PER_DAUGHTER*UNIT_INCREMENT_WIDTH-1 downto 0);
    signal data_to_sum : data_to_sum_type;

    -- the delayed data for the  (pre)last layer
    signal increment_delayed                    : std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0);
    signal mask_delayed                         : std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    
begin

    gen_recurs: if NUMBER_OF_UNITS > NUMBER_OF_DAUGHTERS_PER_STAGE generate
    
        -- recursive adders
        adders: for i in 0 to NUMBER_OF_DAUGHTERS_PER_STAGE-1 generate
        
            -- real adders
            non_zero: if NUMBER_OF_UNITS_PER_DAUGHTER(i) > 0 generate
                accumulator_inst: entity work.bril_histogram_recursive_accumulator
                generic map (
                    NUMBER_OF_UNITS                     => NUMBER_OF_UNITS_PER_DAUGHTER(i),
                    UNIT_INCREMENT_WIDTH                => UNIT_INCREMENT_WIDTH,
                    NUMBER_OF_DAUGHTERS_PER_STAGE       => NUMBER_OF_DAUGHTERS_PER_STAGE,
                    --
                    TOTAL_NUMBER_OF_LAYERS              => TOTAL_NUMBER_OF_LAYERS,
                    THIS_LAYER_ID                       => THIS_LAYER_ID + 1 -- next layer is of course +1
                )
                port map (
                    --
                    clk_i                       => clk_i,
                    --
                    increment_i                 => increment_i((NUMBER_OF_UNITS_PER_DAUGHTER(i) + DAUGHTER_DATA_OFFSETS(i))*UNIT_INCREMENT_WIDTH - 1 downto DAUGHTER_DATA_OFFSETS(i)*UNIT_INCREMENT_WIDTH),
                    mask_i                      => mask_i(NUMBER_OF_UNITS_PER_DAUGHTER(i) + DAUGHTER_DATA_OFFSETS(i) - 1 downto DAUGHTER_DATA_OFFSETS(i)),
                    -- we have to fill only part of the vector because MAX_NUMBER_OF_UNITS_PER_DAUGHTER can be 5, wheres this particular daighter may have only 4 units
                    total_o                     => data_to_sum(i)(NUMBER_OF_UNITS_PER_DAUGHTER(i)*UNIT_INCREMENT_WIDTH - 1 downto 0)
                );
                -- be careful with the leftover bits - just in case tie them to zero
                leftovers: if NUMBER_OF_UNITS_PER_DAUGHTER(i) < MAX_NUMBER_OF_UNITS_PER_DAUGHTER generate
                    data_to_sum(i)(MAX_NUMBER_OF_UNITS_PER_DAUGHTER*UNIT_INCREMENT_WIDTH - 1 downto NUMBER_OF_UNITS_PER_DAUGHTER(i)*UNIT_INCREMENT_WIDTH) <= (others => '0');
                end generate leftovers;
            end generate non_zero;
            
            -- zero values
            zero: if NUMBER_OF_UNITS_PER_DAUGHTER(i) = 0 generate
                data_to_sum(i) <= (others => '0');
            end generate zero;
          
        end generate adders;
        
        -- sum over the adders
        process(clk_i)
            variable sum : std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0);
        begin
            if rising_edge(clk_i) then
                -- start from zero
                sum := (others => '0');
                -- sum
                for i in 0 to NUMBER_OF_DAUGHTERS_PER_STAGE-1 loop
                    sum := sum + data_to_sum(i);
                end loop;
                -- output total
                total_o <= sum;
            end if;
        end process;
        
    end generate gen_recurs;
    
    -- in case this is actually the last element in the chain (no more recursion needed)
    gen_sum: if NUMBER_OF_UNITS <= NUMBER_OF_DAUGHTERS_PER_STAGE generate
    
        -- here is the trick. imagine there are 17 units, we in recursion level zero we generate 4 subsequent stage, with 5, 4, 4 and 4 units respectively.
        -- The last three will take 1 clock cycle to calculate, whereas the first one will take 2 clock cycles because it will have to create another recursion level
        -- thats why the data in the last three has to be delayed
        last_layer: if THIS_LAYER_ID = TOTAL_NUMBER_OF_LAYERS-1 generate
            -- here no delay needed
            increment_delayed <= increment_i;
            mask_delayed <= mask_i;
        end generate last_layer;
        -- due to the outermost if this condition will happen only for one precedent layer
        before_last_layer: if THIS_LAYER_ID < TOTAL_NUMBER_OF_LAYERS-1 generate
            process(clk_i)
            begin
                if rising_edge(clk_i) then
                    increment_delayed <= increment_i;
                    mask_delayed <= mask_i;
                end if;
            end process;    
        end generate before_last_layer;
    
        -- sum over existing
        process(clk_i)
            variable sum : std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0);
        begin
            if rising_edge(clk_i) then
                -- start from zero
                sum := (others => '0');
                -- sum
                for i in 0 to NUMBER_OF_UNITS-1 loop
                    if mask_delayed(i) = '0' then
                        sum := sum + increment_delayed(UNIT_INCREMENT_WIDTH*(i+1) - 1 downto UNIT_INCREMENT_WIDTH*i);
                    else
                        sum := sum;
                    end if;
                end loop;
                -- output total
                total_o <= sum;
            end if;
        end process;
        
    end generate gen_sum;    
                
end rtl;
