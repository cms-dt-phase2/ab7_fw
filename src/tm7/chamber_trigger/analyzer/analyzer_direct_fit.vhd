-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Analyzer
---- File       : analyzer_direct_fit.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Modified by: Alvaro Navarro <alvaro.navarro@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-08
-------------------------------------------------------------------------------
---- Description: This modules controls the (dis)enable status of the 
----              'ANALYZER_LATCOMB_TRUNK' module, stopping its internal
----              pipeline when more than one valid segment has been built.
----              When that happens, it writes the valid segments over 
----              the (up to) 6 one-laterality combination processor modules, 
----              and it determines if a segment has to be sent to the outgoing 
----              FIFO with the basic BX information computed.
----
----              This module calculates both the segment T0 and the muon
----              trajectory values.
----
----              On the worst case, the data-path takes 14 clock cycles
----              from the incoming fifo to the outgoing one connected after 
----              this module (8 of the underlying "ANALYZER_LATCOMB_TRUNK"
----              module plus up to 6 additional cycles, one for each segment). 
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.analyzer_pkg.all;
use work.dt_common_pkg.all;
use work.dt_null_objects_constants_pkg.all;

entity ANALYZER is
  port (
    clk       : in  std_logic;
    rst       : in  std_logic;
    segAnIn   : in  AnalyzerIn_t;
    segAnOut  : out AnalyzerOut_t
  );
end entity;

architecture Behavioural of ANALYZER is

  -- Auxiliary signals.
  signal valid_array    : std_logic_vector(segFitOut_arr_t'range);
  signal towrite_array  : std_logic_vector(segFitOut_arr_t'range);
  signal written_array  : std_logic_vector(segFitOut_arr_t'range);
  
  signal enable_bx_calc    : std_logic;
  signal ena_wr_fifo       : std_logic := '0';

  signal current_segment    : natural range segFitOut_arr_t'range;
  signal mux_currentLatComb : segFitOut_t;
  
  signal is_4_hit_segm      : std_logic;
  signal i_see_dead_people  : std_logic;
  signal segment_quality    : std_logic_vector(2 downto 0);
  
  -- Inter-components connection signals.
  signal AnLatcombTrunkOut : AnLatcombTrunkOut_t;

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  segAnOut.segValidFFOut_wr             <= ena_wr_fifo;
  segAnOut.segCandFFIn_rd               <= AnLatcombTrunkOut.segCandFFIn_rd;

  segAnOut.segment <= (
    main_params => (
      quality                 => segment_quality,
      hits                    => AnLatcombTrunkOut.hits,
      bx_time                 => mux_currentLatComb.bx_time,
      laterality_combination  => mux_currentLatComb.laterality_combination
    ),
    phi_tangent_x4096   => mux_currentLatComb.phi_tangent_x4096,
    horizontal_position => mux_currentLatComb.horizontal_position,
    delta_hit_x         => mux_currentLatComb.delta_hit_x,
    chi_square          => mux_currentLatComb.chi_square,
    bx_id               => mux_currentLatComb.bx_id,
    bx_id_prec          => mux_currentLatComb.bx_id_prec
  );

  ---------------------------------------------------------------
  -- Combinational logic
  --
  valid_gen: for i in valid_array'range generate
    valid_array(i) <= AnLatcombTrunkOut.segFitOut_arr(i).latcomb_valid;
  end generate;
  
  -- valids not yet written...
  towrite_array <= valid_array and not written_array;
    
  process (towrite_array, segAnIn)
    variable something_to_write_now, something_to_write_next : std_logic := '0';
  begin
    -- select the segment that will be written this cycle
    -- determine if apart from the one being written this cycle, there is
    -- something to write in the next cycle
    current_segment <= 0;
    something_to_write_now  := '0';
    something_to_write_next := '0';
    for i in towrite_array'reverse_range loop
      if towrite_array(i) = '1' then
        current_segment <= i;
        something_to_write_next := something_to_write_now;
        something_to_write_now := '1';
      end if;
    end loop;

    -- we write now if there is something to write and the fifo is not full
    ena_wr_fifo <=
      something_to_write_now and (not segAnIn.segValidFFOut_full);

    -- we will have to write something from current data next cycle if there
    -- was already something to write or wasn't have to freeze current data
    something_to_write_next := something_to_write_next or
      (something_to_write_now and segAnIn.segValidFFOut_full );
    enable_bx_calc <= not something_to_write_next;
  end process;
  
  -- Muxer to select the current "laterality combination unit" under evaluation
  mux_currentLatComb <= AnLatcombTrunkOut.segFitOut_arr(current_segment);

  is_4_hit_segm <= (
    AnLatcombTrunkOut.hits(0).dt_time.valid and
    AnLatcombTrunkOut.hits(1).dt_time.valid and
    AnLatcombTrunkOut.hits(2).dt_time.valid and
    AnLatcombTrunkOut.hits(3).dt_time.valid );
  
  i_see_dead_people <=  '0' when valid_array = "000000" else
                        '0' when valid_array = "000001" else
                        '0' when valid_array = "000010" else
                        '0' when valid_array = "000100" else
                        '0' when valid_array = "001000" else
                        '0' when valid_array = "010000" else
                        '0' when valid_array = "100000" else
                        '1';

  -- Computes the final global assigned quality for the segment candidate.
  segment_quality <=
    HIGH        when is_4_hit_segm = '1' and i_see_dead_people = '0' else
    HIGH_GHOST  when is_4_hit_segm = '1' and i_see_dead_people = '1' else
    LOW         when is_4_hit_segm = '0' and i_see_dead_people = '0' else
    LOW_GHOST   ;

  ---------------------------------------------------------------
  -- Sequential (counter) process
  --
  SEC : process (clk)
  begin
    if clk = '1' and clk'event then
      if rst = '1' then 
        written_array <= (others => '0');
      else
        -- if next cycle data will be new, reset the reg of written segments 
        if enable_bx_calc = '1' then
          written_array <= (others => '0');
        -- otherwise, if a segment is being written, register it
        elsif ena_wr_fifo = '1' then
          written_array(current_segment) <= '1';
        end if;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS  
  ---------------------------------------------------------------
  AN_LC_TRUNK : ANALYZER_LATCOMB_TRUNK port map (
    clk            => clk,
    rst            => rst,
    enable         => enable_bx_calc,
    AnLatcombTrunkIn  => (
          control_regs      => segAnIn.control_regs,
          segment_candidate => segAnIn.segment_candidate,
          segCandFFIn_empty => segAnIn.segCandFFIn_empty
    ),
    AnLatcombTrunkOut => AnLatcombTrunkOut
  );
  
end architecture;
