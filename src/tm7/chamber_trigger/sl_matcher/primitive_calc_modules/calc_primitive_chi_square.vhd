-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Primitive builder
---- File       : calc_primitive_chi_square.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-05
-------------------------------------------------------------------------------
---- Description: Calculates the value for "chi square" estimator for a whole
----              primitive.
----              The logic for this module is completely based on that 
----              developed to evaluete only one SuperLayer "chi^2"
----
----              It takes 8 clock cycles.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.dt_constants_pkg.all;

entity CALC_PRIMITIVE_CHI_SQUARE is
  port (
    clk                   : in  std_logic;
    rst                   : in  std_logic;
    enable                : in  std_logic;
    paired_segments       : in  DTSegment_arr_t;
    x_coor_chamber        : in  SignedDistance_t;
    sl_phi3_x_offset      : in  signed(9 downto 0);
    tan_phi_chamber_x4096 : in  signed(14 downto 0);
    chi_square_chamber    : out unsigned(23 downto 0)
  );
end entity;

architecture TwoProcess of CALC_PRIMITIVE_CHI_SQUARE is

  -- A constant with 10 zeroes.
  constant ZEROES_X10  : signed(9 downto 0) := (others => '0');
  constant OFFSET_ZERO : SignedDistance_t    := (others => '0');
  constant TANPHI_SIZE : integer := tan_phi_chamber_x4096'length;
  --
  -- Distances from the chamber's center to each of the middle lines (wires) on
  -- each layer (for the two superlayers).
  -- 
  --      Distance between centers (phi1 -> phi3): 235 mm
  --      From chamber center to each wire-line:
  --          Top    3 -> 137 mm
  --          Top    2 -> 124 mm
  --          Top    1 -> 111 mm
  --          Top    0 -> 98 mm 
  --
  --          Bottom 3 -> -98 mm
  --          Bottom 2 -> -111 mm
  --          Bottom 1 -> -124 mm
  --          Bottom 0 -> -137 mm
  --
  -- As well as in the case for only one superlayer, all these coeficients 
  -- will have 2 additional precision bits. Max. value is around 1018 (after 
  -- x4 factor) so it's necessary a 10 bits field to keep the value + 1 bit 
  -- for sign. For historical reasons I'll keep a 13-bits field width.
  --
  constant ZFACTOR_SIZE : integer := 13; 
  
  -- In "mm" with 2 precision bits (117.5 mm x 4)
  constant CH_CENTER_TO_MID_SL_P : integer := 470;
  
  constant ZTop3 : signed(ZFACTOR_SIZE-1 downto 0) := 
      to_signed( ( 6 * to_integer(CELL_HEIGHT)) + CH_CENTER_TO_MID_SL_P, ZFACTOR_SIZE );
      
  constant ZTop2 : signed(ZFACTOR_SIZE-1 downto 0) := 
      to_signed( ( 2 * to_integer(CELL_HEIGHT)) + CH_CENTER_TO_MID_SL_P, ZFACTOR_SIZE );
      
  constant ZTop1 : signed(ZFACTOR_SIZE-1 downto 0) := 
      to_signed( (-2 * to_integer(CELL_HEIGHT)) + CH_CENTER_TO_MID_SL_P, ZFACTOR_SIZE );
  
  constant ZTop0 : signed(ZFACTOR_SIZE-1 downto 0) := 
      to_signed( (-6 * to_integer(CELL_HEIGHT)) + CH_CENTER_TO_MID_SL_P, ZFACTOR_SIZE );
  
  constant ZBot3 : signed(ZFACTOR_SIZE-1 downto 0) := 
      to_signed( ( 6 * to_integer(CELL_HEIGHT)) - CH_CENTER_TO_MID_SL_P, ZFACTOR_SIZE );
  
  constant ZBot2 : signed(ZFACTOR_SIZE-1 downto 0) := 
      to_signed( ( 2 * to_integer(CELL_HEIGHT)) - CH_CENTER_TO_MID_SL_P, ZFACTOR_SIZE );
  
  constant ZBot1 : signed(ZFACTOR_SIZE-1 downto 0) := 
      to_signed( (-2 * to_integer(CELL_HEIGHT)) - CH_CENTER_TO_MID_SL_P, ZFACTOR_SIZE );
  
  constant ZBot0 : signed(ZFACTOR_SIZE-1 downto 0) := 
      to_signed( (-6 * to_integer(CELL_HEIGHT)) - CH_CENTER_TO_MID_SL_P, ZFACTOR_SIZE );
  --
  -- CAREFUL WITH THE ORDER !!!! These coeficients MUST BE SORTED according
  -- to the order in which "loop for" inside the combinational process 
  -- associates each constant to each wire calculations. So it depends on 
  -- the order of the loops (superlayer/layer) and their directions (to/downto)
  --
  type ZFactor_arr_t is array (0 to 7) of signed(ZFACTOR_SIZE-1 downto 0);
  constant Z_FACTOR : ZFactor_arr_t := (ZBot0, ZBot1, ZBot2, ZBot3, 
                                        ZTop0, ZTop1, ZTop2, ZTop3);

  constant CHISQR_LEN : integer := DTSegment_t.chi_square'length;
  constant SUMS_SIZE  : integer := TANPHI_SIZE + ZFACTOR_SIZE;    -- 28
  
  -- Sums are reduced in size, explained later
  constant REDUCED_SUMS_SIZE : integer := 24; 
  constant PRODUCTS_SIZE     : integer := 2*REDUCED_SUMS_SIZE;    -- 48

  type distance_arr_t   is array (0 to 7) of SignedDistance_t;
  type sum_arr_t        is array (0 to 7) of signed(SUMS_SIZE-1 downto 0);
  type product_arr_t    is array (0 to 7) of unsigned(PRODUCTS_SIZE-1 downto 0);

  type phi_offset_arr_t is array (0 to 1) of SignedDistance_t;
  
  -- Internal signals to be registered.
  type reg_type is record
    sum_A_minus      : std_logic_vector(7 downto 0);
    sum_A            : distance_arr_t;
    sum_A_stg1       : distance_arr_t;
    sum_B            : sum_arr_t;
    sum_B_stg1       : sum_arr_t;
    product_arr      : product_arr_t;
    --
    product_arr_stg1 : product_arr_t;
    product_arr_stg2 : product_arr_t;
    --
    product_factor   : sum_arr_t;
    hit_valid        : std_logic_vector(7 downto 0);
    hit_valid_stg1   : std_logic_vector(7 downto 0);
    chi_sum_01       : unsigned(PRODUCTS_SIZE-3 downto 0); -- 2 bits reduce precission
    chi_sum_23       : unsigned(PRODUCTS_SIZE-3 downto 0); -- 2 bits reduce precission
    chi_sum_45       : unsigned(PRODUCTS_SIZE-3 downto 0); -- 2 bits reduce precission
    chi_sum_67       : unsigned(PRODUCTS_SIZE-3 downto 0); -- 2 bits reduce precission
    chi_square       : unsigned(PRODUCTS_SIZE-1 downto 0);
  end record;

  constant regDefault : reg_type := (
    sum_A_minus      => (others => '0'),
    sum_A            => (others => (others => '0')),
    sum_A_stg1       => (others => (others => '0')),
    sum_B            => (others => (others => '0')),
    sum_B_stg1       => (others => (others => '0')),
    product_arr      => (others => (others => '0')),
    product_arr_stg1 => (others => (others => '0')),
    product_arr_stg2 => (others => (others => '0')),
    product_factor   => (others => (others => '0')),
    hit_valid        => (others => '0'),
    hit_valid_stg1   => (others => '0'),
    chi_sum_01       => (others => '0'),
    chi_sum_23       => (others => '0'),
    chi_sum_45       => (others => '0'),
    chi_sum_67       => (others => '0'),
    chi_square       => (others => '0')
  );

  signal reg, nextReg : reg_type := regDefault;

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  -- "reg.chi_square" will be in "mm^2 x 2^2 x 2^24"
  -- Outgoing result will have units: "mm^2 x 1024"
  --
  chi_square_chamber <= reg.chi_square(chi_square_chamber'length-1 + 16 downto 16);

  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, paired_segments, x_coor_chamber, tan_phi_chamber_x4096,
                 sl_phi3_x_offset)
    -- Auxiliary variables to clarify the code.
    variable index         : integer;
    variable segment       : DTSegment_t;
    variable hit_x_pos     : distance_arr_t;
    variable sum_A_aligned : sum_arr_t;
    variable sum_B_aligned : sum_arr_t;
    variable phi3_offset   : phi_offset_arr_t;    
  begin
    -- Default values.
    nextReg <= reg;

    if sl_phi3_x_offset < OFFSET_ZERO then
      phi3_offset(1) := (others => '0');
      -- CAREFUL !! Final is positive (sign changed)
      phi3_offset(0) := 
          resize( signed(std_logic_vector( -sl_phi3_x_offset ) & "0000"), 
                  SignedDistance_t'length );

    else
      -- CAREFUL !! Final is positive
      phi3_offset(1) := 
          resize( signed(std_logic_vector(sl_phi3_x_offset) & "0000"), 
                  SignedDistance_t'length );

      phi3_offset(0) := (others => '0');
    end if;

    -- Loop over both PHI superlayers
    SL_PRO_LOOP : for suplayer in 0 to 1 loop
      segment := paired_segments(suplayer);

      -- Loop over all layers on each superlayer
      ITEM_PROC_LOOP : for layer in 0 to 3 loop
        -- Registers index
        index := 4 * suplayer + layer;

        ---------------------------
        -- First clock cycle
        ---------------------------
        --
        -- This is implicitly multiplied by a factor of 16 because they have 
        -- 4 extra bits for precision.
        --
        nextReg.sum_A_stg1(index) <= (segment.horizontal_position -
                                      x_coor_chamber)             +
                                     phi3_offset(suplayer)        +
                                     segment.delta_hit_x(layer);

        -- Also this has additional factors: tan -> x4096
        nextReg.sum_B_stg1(index) <= Z_FACTOR(index) * tan_phi_chamber_x4096;

        -- Pipe to keep aligned "hit's valid" parameters
        nextReg.hit_valid_stg1(index) <= 
              segment.main_params.hits(layer).dt_time.valid;

        ---------------------------
        -- Second clock cycle (clk speed optimization data pipeline)
        ---------------------------
        if reg.sum_A_stg1(index) < 0 then 
          nextReg.sum_A(index)       <= -reg.sum_A_stg1(index);
          nextReg.sum_A_minus(index) <= '1';
        else 
          nextReg.sum_A(index)       <=  reg.sum_A_stg1(index);
          nextReg.sum_A_minus(index) <= '0';
        end if;
        
        nextReg.sum_B(index)     <= reg.sum_B_stg1(index);
        nextReg.hit_valid(index) <= reg.hit_valid_stg1(index);

        ---------------------------
        -- Third clock cycle
        ---------------------------
        -- Multiply by 1024
        -- sum_A has units of mm / 16 (SignedDistance_t resolution )
        -- sum_B has units of mm / 4096 (tanphi resolution) / 4 (Z_FACTOR resolution)
        -- result is aligned to the units of sum_B
        sum_A_aligned(index) := reg.sum_A(index) & ZEROES_X10;
        sum_B_aligned(index) := reg.sum_B(index);
        --
        -- When a hit is not valid, we replace any possible calculated value 
        -- (they should be zeroes) by a zero, because "- x_coor_chamber" term 
        -- on "sum_A" would invalidate the global addition.
        --
        if reg.hit_valid(index) = '1' then 
          -- Careful with the sign modifications chain.
          if reg.sum_A_minus(index) = '1' then 
            nextReg.product_factor(index) <= 
                  -sum_A_aligned(index) - sum_B_aligned(index);
          else 
            nextReg.product_factor(index) <=  
                  sum_A_aligned(index) - sum_B_aligned(index);
          end if;
        else 
          nextReg.product_factor(index) <= (others => '0');
        end if;

        ---------------------------
        -- Fourth  clock cycle
        ---------------------------
        --
        -- To avoid a very long product register, which could cause overflow in
        -- the DSP internal registers, here I'll cut the operands length.
        --
        -- It's possible considering that the longest value for each 
        -- "product_factor" should be lower that 3 cell widths ~ 126 mm 
        -- (in fact lower than 1 cell width, otherwise segments calculations 
        -- are being really pure garbage). 
        -- So final values should fit in a 24 bits field (10 for value; 
        -- 1 for sign; 12 additional for precision).
        --
        -- At this point "product_arr" units are "mm^2 x 2^4 x 2^24"
        --
        nextReg.product_arr(index) <= 
                unsigned( reg.product_factor(index)(REDUCED_SUMS_SIZE-1 downto 0) *
                          reg.product_factor(index)(REDUCED_SUMS_SIZE-1 downto 0) );

        ---------------------------
        -- Fifth clock cycle
        ---------------------------
        -- These extra (2) registrations (clock cycles) are inserted because XST
        -- recommeds it to optime the performance for the DSP used in the last
        -- product.
        nextReg.product_arr_stg1(index) <= reg.product_arr(index);

        ---------------------------
        -- Sixth clock cycle
        ---------------------------
        nextReg.product_arr_stg2(index) <= reg.product_arr_stg1(index);
      end loop;

    end loop;

    ---------------------------
    -- Seventh clock cycle
    ---------------------------
    --
    -- CAREFUL !!! Division by 4 to reduce fields size
    -- So after that, result is in "mm^2 x 2^2 x 2^24"
    --
    nextReg.chi_sum_01 <= resize(reg.product_arr_stg2(0)(PRODUCTS_SIZE-1 downto 2), 46) +
                          resize(reg.product_arr_stg2(1)(PRODUCTS_SIZE-1 downto 2), 46);

    nextReg.chi_sum_23 <= resize(reg.product_arr_stg2(2)(PRODUCTS_SIZE-1 downto 2), 46) +
                          resize(reg.product_arr_stg2(3)(PRODUCTS_SIZE-1 downto 2), 46);

    nextReg.chi_sum_45 <= resize(reg.product_arr_stg2(4)(PRODUCTS_SIZE-1 downto 2), 46) +
                          resize(reg.product_arr_stg2(5)(PRODUCTS_SIZE-1 downto 2), 46);

    nextReg.chi_sum_67 <= resize(reg.product_arr_stg2(6)(PRODUCTS_SIZE-1 downto 2), 46) +
                          resize(reg.product_arr_stg2(7)(PRODUCTS_SIZE-1 downto 2), 46);

    ---------------------------
    -- Eighth clock cycle
    ---------------------------
    nextReg.chi_square <= resize(reg.chi_sum_01, 48) +
                          resize(reg.chi_sum_23, 48) +
                          resize(reg.chi_sum_45, 48) +
                          resize(reg.chi_sum_67, 48);
  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1' then 
        reg <= regDefault;
      elsif enable = '1' then
        reg <= nextReg;
      end if;
    end if;
  end process;

end architecture;
