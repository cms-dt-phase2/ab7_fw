# basic source files
add_files -norecurse ../hdl/bril_histogram_package.vhd
add_files -norecurse ../hdl/bril_histogram_bram.vhd
add_files -norecurse ../hdl/sync/bril_histogram_core_sync.vhd
add_files -norecurse ../hdl/sync/bril_histogram_streamer_sync.vhd
add_files -norecurse ../hdl/sync/bril_histogram_wrapper_sync.vhd


# the timing constraint file
add_files -norecurse ../ucf/bril_histogram_timing.tcl
