-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Delayed enable
---- File       : delayed_enable.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-11
-------------------------------------------------------------------------------
---- Description: After a reset, and if 'enable' is active, waits 2^NUM_BITS
----              clock cycles after setting 'go_on' to '1'.
----              The outgoing signal is kept to '1' until a new reset is
----              performed.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

entity DELAYED_ENABLE is
  generic (NUM_BITS : integer := 2);
  port(
    clk    : in  std_logic;
    rst    : in  std_logic;
    enable : in  std_logic;
    go_on  : out std_logic
  );
end entity;

architecture RTL of DELAYED_ENABLE is
  constant LOCK_ALL_ONES : std_logic_vector(NUM_BITS - 1 downto 0) 
                         := (others => '1');
  
  signal lock_counter : std_logic_vector(NUM_BITS - 1 downto 0) 
                      := (others => '0');

begin
  ---------------------------------------------------------------
  -- Outgoing signals 
  --
  go_on <= '1' when lock_counter = LOCK_ALL_ONES else '0';

  ---------------------------------------------------------------
  -- Sequential logic
  --
  process(clk, rst)
  begin
    if clk'event and clk = '1' then
      if rst = '1' then
        lock_counter <= (others => '0');
      elsif lock_counter /= LOCK_ALL_ONES and enable = '1' then
        lock_counter <= lock_counter + '1';
      end if;
    end if;
  end process;
  
end architecture;
