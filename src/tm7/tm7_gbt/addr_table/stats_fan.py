numlinks = 12


from pprint import pprint
stats = '''
  stat(16#08#)(7 downto 0)   <= stat_link1_rx_trid;
  stat(16#08#)(15 downto 8)  <= stat_link1_rx_ch;
  stat(16#08#)(23 downto 16) <= stat_link1_rx_len;
  stat(16#08#)(31 downto 24) <= stat_link1_rx_error;
  stat(16#09#)(31 downto 0)  <= stat_link1_rx_ec_data;
  stat(16#0A#)(0)            <= stat_link1_rx_reply_flag;
  stat(16#0A#)(1)            <= stat_link1_tx_ready;
  stat(16#0A#)(2)            <= stat_link1_rx_empty;
  stat(16#0B#)(7 downto 0)   <= stat_link1_rx_gbtx_addr;
  stat(16#0C#)(15 downto 0)  <= stat_link1_rx_reg;
  stat(16#0C#)(31 downto 16) <= stat_link1_rx_nb;
  stat(16#0D#)(7 downto 0)   <= stat_link1_rx_ic_data;
  stat(16#0E#)(0)            <= stat_link1_link_ready;
  stat(16#0E#)(2)            <= stat_link1_tx_aligned;
  stat(16#0E#)(4)            <= stat_link1_rx_gbt_ready;
  stat(16#0E#)(5)            <= stat_link1_rx_gbt_ready_lost_flag;
  stat(16#0E#)(6)            <= stat_link1_rx_data_error_seen;
  stat(16#0E#)(7)            <= stat_link1_rx_match_flag;
  stat(16#0E#)(8)            <= stat_link1_rx_is_data;
  stat(16#0E#)(10)           <= stat_link1_rx_frame_ready;
  stat(16#0F#)(19 downto 0)  <= std_logic_vector( stat_link1_no_rxclkout_time );
  stat(16#0F#)(31 downto 20) <= std_logic_vector( stat_link1_no_rxclkout_ctr );
'''

stats = [stat.strip() for stat in stats.split('\n') if stat.strip()]
stats = [stat.split('<=')[-1].strip(' );').split()[-1] for stat in stats]
stats = [stat.split('_',2)[2] for stat in stats]

maxlen = max([len(stat) for stat in stats])

txt = ''

for stat in stats:
    txt +=  '( '
    for link in range(1, numlinks + 1):
        txt += 'stat_link%i_'%link
        txt += stat
        txt += ' '*( 1 + (1 if link<10 and numlinks>9 else 0) + (maxlen - len(stat)) )
        txt += ', '
    txt = txt[:-2]
    txt += ') <= ' + stat + ';\n'

print txt

