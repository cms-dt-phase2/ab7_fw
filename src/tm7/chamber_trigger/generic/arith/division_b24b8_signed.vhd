-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Arithmetics
---- File       : division_b24b8_signed.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-08
-------------------------------------------------------------------------------
---- Description: Fixed point, quantized, division 24 bits over 8 bits.
----              It adds an extra bit to take into account parameters' signs.
----              It takes 3 clock cycles because of input and output 
----              value registration and the internal stages.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.arith_pkg.all;

entity DIVISION_B24B8_SIGNED is
  port(
    clk         : in  std_logic;
    rst         : in  std_logic;
    enable      : in  std_logic;
    numerator   : in  signed(24 downto 0);
    denominator : in  signed( 8 downto 0);
    div_by_zero : out std_logic;
    result      : out signed(24 downto 0)
  );
end entity;

architecture TwoProcess of DIVISION_B24B8_SIGNED is
  --
  -- Attribute for this signal is to force synthetizer to implement 
  -- the RAM as a LUT, because when it determines that it can be implemented 
  -- as a block RAM, performance and speed drop down dramatically.
  --
  signal denominat_invert : signed(16 downto 0);
  -- Types: auto|block|distributed    
  attribute ram_style: string;    
  attribute ram_style of denominat_invert : signal is "distributed";

  -- Registers
  type reg_type is record
    numerator_in       : signed(24 downto 0);
    numerator          : signed(24 downto 0);
    denominat_in       : signed( 8 downto 0);
    denom_invert       : signed(16 downto 0);
    result             : signed(24 downto 0);
    denom_is_null      : std_logic;
    denom_is_one       : std_logic;
    denom_is_minus_one : std_logic;
    div_by_zero        : std_logic;
  end record;
  
  constant regDefault : reg_type := (
    numerator_in       => (others => '0'),
    numerator          => (others => '0'),
    denominat_in       => (others => '0'),
    denom_invert       => (others => '0'),
    result             => (others => '0'),
    denom_is_null      => '1',
    denom_is_one       => '0',
    denom_is_minus_one => '0',
    div_by_zero        => '1'
  );
  signal reg, nextReg : reg_type := regDefault;

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  result      <= reg.result;
  div_by_zero <= reg.div_by_zero;

  ---------------------------------------------------------------
  -- Combinational logic
  --
  process(reg, numerator, denominator, denominat_invert)
    variable my_result : signed(41 downto 0);
  begin
    nextReg <= reg;

    ---------------------------
    -- First clock cycle
    ---------------------------
    nextReg.numerator_in <= numerator;
    nextReg.denominat_in <= denominator;

    ---------------------------
    -- Second clock cycle
    ---------------------------
    nextReg.numerator    <= reg.numerator_in;
    nextReg.denom_invert <= denominat_invert;

    if reg.denominat_in = 0 
    then nextReg.denom_is_null <= '1';
    else nextReg.denom_is_null <= '0';
    end if;

    if reg.denominat_in = 1 
    then nextReg.denom_is_one <= '1';
    else nextReg.denom_is_one <= '0';
    end if;

    if reg.denominat_in = -1
    then nextReg.denom_is_minus_one <= '1';
    else nextReg.denom_is_minus_one <= '0';
    end if;

    ---------------------------
    -- Third clock cycle
    ---------------------------
    my_result := reg.numerator * reg.denom_invert;
    
    if reg.denom_is_null = '1' then 
      nextReg.div_by_zero <= '1';
      nextReg.result      <= (others => '0');

    elsif reg.denom_is_one = '1' then 
      nextReg.div_by_zero <= '0';
      nextReg.result      <= reg.numerator;

    elsif reg.denom_is_minus_one = '1' then 
      nextReg.div_by_zero <= '0';
      nextReg.result      <= -reg.numerator;

    else 
      nextReg.div_by_zero <= '0';
      -- Bit shift for quantized division.
      nextReg.result      <= my_result(40 downto 16);
    end if;
 end process;
  
  ---------------------------------------------------------------
  -- Sequential process
  --
  process(clk, rst, enable)
  begin
    if clk'event and clk = '1' then
      if rst = '1' then 
        reg <= regDefault;
      elsif enable = '1' then
        reg <= nextReg;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  DIV_LUT : DIVISOR_B8Q16_LUT_SIGNED
    port map(value => reg.denominat_in, value_inverted => denominat_invert);

end architecture;
