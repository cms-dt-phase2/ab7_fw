This file explains the relationship of the /cactusupgrades folder to the cactusupgrades folder of the mp7 project at https://gitlab.cern.ch/cms-cactus/firmware/mp7 and the ipbus sources at https://github.com/ipbus/firmware

This version of the AB7 fw is based on the 2.4.3 tag of the mp7 project.

## /src/tm7/

these folders contains board-level code both common to all tm7 firmwares (./tm7_common) and also specific to AB7 (./ab7_top). Many of the files in this folder have been modified from a mp7 file. This is indicated in the first line of each file, and should be taken into account when backporting changes from the mp7 repository.

## /src/cactusupgrades/boards/mp7/

every file in this folder is directly copied from its original mp7 file.

## /src/cactusupgrades/components/

this folder contains the code for the different firmware blocks used in the design. The ipbus and mp7 modules should be identical to the ones either at the mp7 repo tag on which this fw is based, or the ipbus github tag in which the mp7 tag is based. This is not, however true for all modules, and here comes a list of the changes to take into account:

- mp7_ttc/ we have modified the clocks so there are changes to 3 files here (mp7_ttc.vhd, ttc_clocks.vhd, ttc_cmd.vhd).




