----------------------------------------------------------------------------------
-- Multi-BRAM FIFO_DUALCLOCK_MACRO memory.
-- If more depth is needed, width is reduced and individual macros are put in parallel
-- �lvaro Navarro, CIEMAT, 2015
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

library work;
use work.minmax.all;

entity multi_bram_fifo_dualclock is
  Generic (
    -- Maximum depth using FIFO_DUALCLOCK_MACRO without concatenation is 13, with must use 36Kb
    -- For smaller depths, 18Kb is used, because of better granularity in resource utilization
    DATA_WIDTH              : positive := 8;
    DEPTH_LOG2              : positive range 13 downto 9 := 11;
    FIRST_WORD_FALL_THROUGH : boolean := FALSE;
    ALMOST_FULL_OFFSET      : bit_vector(15 downto 0) := X"0080";
    ALMOST_EMPTY_OFFSET     : bit_vector(15 downto 0) := X"0080"
  );
  port (
    ALMOSTEMPTY   : out std_logic;
    ALMOSTFULL    : out std_logic;
    RDCOUNT       : out std_logic_vector(DEPTH_LOG2 - 1 downto 0);
    WRCOUNT       : out std_logic_vector(DEPTH_LOG2 - 1 downto 0);
    DO            : out std_logic_vector(DATA_WIDTH-1 downto 0);
    EMPTY         : out std_logic;
    FULL          : out std_logic;
    DI            : in std_logic_vector(DATA_WIDTH-1 downto 0);
    RDCLK         : in std_logic;
    RDEN          : in std_logic;
    RST           : in std_logic;
    WRCLK         : in std_logic;
    WREN          : in std_logic
  );
end entity;

architecture Behavioral of multi_bram_fifo_dualclock is

  -- these are the values for the FIFO_DUALCLOCK_MACRO
  -----------------------------------------------------------------
  -- DATA_WIDTH | FIFO_SIZE | FIFO Depth | RDCOUNT/WRCOUNT Width --
  -- ===========|===========|============|=======================--
  --   37-72    |  "36Kb"   |     512    |         9-bit         --
  --   19-36    |  "36Kb"   |    1024    |        10-bit         --
  --   19-36    |  "18Kb"   |     512    |         9-bit         --
  --   10-18    |  "36Kb"   |    2048    |        11-bit         --
  --   10-18    |  "18Kb"   |    1024    |        10-bit         --
  --    5-9     |  "36Kb"   |    4096    |        12-bit         --
  --    5-9     |  "18Kb"   |    2048    |        11-bit         --
  --    1-4     |  "36Kb"   |    8192    |        13-bit         --
  --    1-4     |  "18Kb"   |    4096    |        12-bit         --
  -----------------------------------------------------------------
  constant DEPTH_THAT_REQUIRES_36Kb : positive := 13;
  
  -- the macro's address width is determined by DEPTH_LOG2
  constant MACRO_DEPTH : positive := DEPTH_LOG2;

  -- Use 36Kb macros if maximum depth is needed; otherwise, use 18Kb
  type macro_size_switch_t is array(boolean) of string(1 to 4);
  constant macro_size_switch : macro_size_switch_t := (true => "36Kb", false => "18Kb");
  constant MACRO_SIZE : string := macro_size_switch( MACRO_DEPTH = DEPTH_THAT_REQUIRES_36Kb );

  -- Define the rest of the constraining constants 
  constant MACRO_WIDTH  : positive := 36 / 2**(minimum(DEPTH_THAT_REQUIRES_36Kb-1,MACRO_DEPTH) - 9);
  constant N_MACROS     : positive := 1 + (DATA_WIDTH-1) / MACRO_WIDTH;

  -- Internal signals
  signal dinx, doutx   : std_logic_vector(N_MACROS*MACRO_WIDTH - 1 downto 0) := (others => '0');
  
  -- dummy declarations because FIFO_DUALCLOCK_MACRO's rdcount & wrcount cannot be left open
  type required_for_port_constraining_t is array(2*N_MACROS-1 downto 2) of std_logic_vector(MACRO_DEPTH-1 downto 0);
  signal required_for_port_constraining : required_for_port_constraining_t;


begin
  
  dinx(DI'range) <= DI;
  dinx(dinx'high downto DI'high+1) <= (others => '0');
  DO <= doutx(DO'range);
  

  FIFO_DUALCLOCK_MACRO_inst : FIFO_DUALCLOCK_MACRO
  generic map (
    DEVICE => "7SERIES",            -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES" 
    ALMOST_FULL_OFFSET => ALMOST_FULL_OFFSET(12 downto 0),  -- Sets almost full threshold
    ALMOST_EMPTY_OFFSET => ALMOST_EMPTY_OFFSET(12 downto 0), -- Sets the almost empty threshold
    DATA_WIDTH => MACRO_WIDTH,   -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
    FIFO_SIZE => MACRO_SIZE,            -- Target BRAM, "18Kb" or "36Kb" 
    FIRST_WORD_FALL_THROUGH => FIRST_WORD_FALL_THROUGH) -- Sets the FIFO FWFT to TRUE or FALSE
  port map (
    ALMOSTEMPTY => ALMOSTEMPTY,   -- 1-bit output almost empty
    ALMOSTFULL => ALMOSTFULL,     -- 1-bit output almost full
    DO => doutx(MACRO_WIDTH - 1 downto 0),                     -- Output data, width defined by DATA_WIDTH parameter
    EMPTY => EMPTY,               -- 1-bit output empty
    FULL => FULL,                 -- 1-bit output full
    RDCOUNT => RDCOUNT,           -- Output read count, width determined by FIFO depth
    RDERR => open,               -- 1-bit output read error
    WRCOUNT => WRCOUNT,           -- Output write count, width determined by FIFO depth
    WRERR => open,               -- 1-bit output write error
    DI => dinx(MACRO_WIDTH - 1 downto 0),                     -- Input data, width defined by DATA_WIDTH parameter
    RDCLK => RDCLK,               -- 1-bit input read clock
    RDEN => RDEN,                 -- 1-bit input read enable
    RST => RST,                   -- 1-bit input reset
    WRCLK => WRCLK,               -- 1-bit input write clock
    WREN => WREN                  -- 1-bit input write enable
  );

  
  macros_gen : for num in N_MACROS - 1 downto 1 generate
    FIFO_DUALCLOCK_MACRO_inst : FIFO_DUALCLOCK_MACRO
    generic map (
      DEVICE => "7SERIES",            -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES" 
      ALMOST_FULL_OFFSET => ALMOST_FULL_OFFSET,  -- Sets almost full threshold
      ALMOST_EMPTY_OFFSET => ALMOST_EMPTY_OFFSET, -- Sets the almost empty threshold
      DATA_WIDTH => MACRO_WIDTH,   -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
      FIFO_SIZE => MACRO_SIZE,            -- Target BRAM, "18Kb" or "36Kb" 
      FIRST_WORD_FALL_THROUGH => FIRST_WORD_FALL_THROUGH) -- Sets the FIFO FWFT to TRUE or FALSE
    port map (
      ALMOSTEMPTY => open,   -- 1-bit output almost empty
      ALMOSTFULL => open,     -- 1-bit output almost full
      DO => doutx((num+1)*MACRO_WIDTH - 1 downto num*MACRO_WIDTH),                     -- Output data, width defined by DATA_WIDTH parameter
      EMPTY => open,               -- 1-bit output empty
      FULL => open,                 -- 1-bit output full
      RDCOUNT => required_for_port_constraining(num*2),           -- Output read count, width determined by FIFO depth
      RDERR => open,               -- 1-bit output read error
      WRCOUNT => required_for_port_constraining(num*2+1),           -- Output write count, width determined by FIFO depth
      WRERR => open,               -- 1-bit output write error
      DI => dinx((num+1)*MACRO_WIDTH - 1 downto num*MACRO_WIDTH),                     -- Input data, width defined by DATA_WIDTH parameter
      RDCLK => RDCLK,               -- 1-bit input read clock
      RDEN => RDEN,                 -- 1-bit input read enable
      RST => RST,                   -- 1-bit input reset
      WRCLK => WRCLK,               -- 1-bit input write clock
      WREN => WREN                  -- 1-bit input write enable
    );
  end generate;
  
end architecture;
