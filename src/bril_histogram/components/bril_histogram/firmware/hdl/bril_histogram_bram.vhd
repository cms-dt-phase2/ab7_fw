----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_core_sync - rtl
-- Description: 
-- 
--   BRAM is generated here using xilinx template
--   Note : this memory is Read-First - very handy to reset memory content during ipbus read
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bril_histogram_bram is
Generic (
    BRAM_WORD_WIDTH     : natural;
    BRAM_DEPTH          : natural
);
Port ( 
    clk_a_i             : in std_logic;
    clk_b_i             : in std_logic;
    -- input port
    ram_addr_a_i        : in natural range 0 to BRAM_DEPTH-1;
    ram_dout_a_o        : out std_logic_vector(BRAM_WORD_WIDTH-1 downto 0);
    ram_din_a_i         : in std_logic_vector(BRAM_WORD_WIDTH-1 downto 0);
    ram_we_a_i          : in std_logic;
    -- output port
    ram_addr_b_i        : in natural range 0 to BRAM_DEPTH-1;
    ram_dout_b_o        : out std_logic_vector(BRAM_WORD_WIDTH-1 downto 0);
    ram_din_b_i         : in std_logic_vector(BRAM_WORD_WIDTH-1 downto 0) := (others => '0');
    ram_we_b_i          : in std_logic := '0'
);
end bril_histogram_bram;

architecture rtl of bril_histogram_bram is

    -- constructs to define block memory (according to the template from xilinx)
    subtype memory_cell_word_type is std_logic_vector(BRAM_WORD_WIDTH-1 downto 0); 
    type ram_type is array(0 to BRAM_DEPTH-1) of memory_cell_word_type;
    
    -- init (mostly for testing)
    function init_ram return ram_type is
        variable ram : ram_type;
    begin
        for i in 0 to BRAM_DEPTH-1 loop
            --ram(i) := std_logic_vector(to_unsigned(i,BRAM_WORD_WIDTH));
            ram(i) := (others => '0');
        end loop;
        return ram;
    end function init_ram; 
    
    -- now create
    signal memory                   : ram_type := init_ram; -- init with zeros
    attribute RAM_STYLE             : string;
    attribute RAM_STYLE of memory   : signal is "BLOCK";
    
    -- memory signals Port A  
    signal ram_data_a               : std_logic_vector(BRAM_WORD_WIDTH-1 downto 0);
    signal ram_addr_a               : natural range 0 to BRAM_DEPTH-1;   
    signal ram_en_a                 : std_logic;  -- RAM Enable, for additional power savings, disable port when not in use
    signal ram_din_a                : std_logic_vector(BRAM_WORD_WIDTH-1 downto 0);   -- RAM input data
    signal ram_we_a                 : std_logic; -- Byte-Write enable
    signal ram_dout_a               : std_logic_vector(BRAM_WORD_WIDTH-1 downto 0);   -- RAM output data
    signal ram_dout_a_reg           : std_logic_vector(BRAM_WORD_WIDTH-1 downto 0);   -- RAM output data
    
    -- memory signals Port B
    signal ram_data_b               : std_logic_vector(BRAM_WORD_WIDTH-1 downto 0);
    signal ram_addr_b               : natural range 0 to BRAM_DEPTH-1;
    signal ram_en_b                 : std_logic;  -- RAM Enable, for additional power savings, disable port when not in use
    signal ram_din_b                : std_logic_vector(BRAM_WORD_WIDTH-1 downto 0);   -- RAM input data
    signal ram_we_b                 : std_logic; -- Byte-Write enable
    -- read
    signal ram_dout_b               : std_logic_vector(BRAM_WORD_WIDTH-1 downto 0);   -- RAM output data
    signal ram_dout_b_reg           : std_logic_vector(BRAM_WORD_WIDTH-1 downto 0);   -- RAM output data

begin

    -- connecting the in out interfaxce
    ram_addr_a <= ram_addr_a_i;
    ram_dout_a_o <= ram_dout_a;
    ram_din_a <= ram_din_a_i;
    ram_we_a <= ram_we_a_i;
    --
    ram_addr_b <= ram_addr_b_i;
    ram_dout_b_o <= ram_dout_b;
    ram_din_b <= ram_din_b_i;
    ram_we_b <= ram_we_b_i;
    
    -- always enabled
    ram_en_a <= '1';
    ram_en_b <= '1';

    -- init port a
    process(clk_a_i)
    begin
        if rising_edge(clk_a_i) then
            if(ram_en_a = '1') then 
                ram_data_a <= memory(ram_addr_a); 
                if(ram_we_a = '1') then
                    memory(ram_addr_a) <= ram_din_a;
                end if;
            end if;
        end if;
    end process;
    
    -- output register to provide better timing Port A
    process(clk_a_i)
    begin
        if rising_edge(clk_a_i) then
            ram_dout_a_reg <= ram_data_a;
        end if;
    end process;
    ram_dout_a <= ram_dout_a_reg;
    
    -- init port b
    process(clk_b_i)
    begin
        if rising_edge(clk_b_i) then
            if(ram_en_b = '1') then  
                ram_data_b <= memory(ram_addr_b);          
                if(ram_we_b = '1') then
                    memory(ram_addr_b) <= ram_din_b;
                end if;                 
            end if;
        end if;
    end process;
    
    -- output register to provide better timing Port B
    process(clk_b_i)
    begin
        if rising_edge(clk_b_i) then
            ram_dout_b_reg <= ram_data_b;
        end if;
    end process;
    ram_dout_b <= ram_dout_b_reg;

end rtl;
