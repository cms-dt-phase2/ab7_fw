numlinks = 12


from pprint import pprint
regs = '''
  conf_link1_erase_elink_word          <= conf(16#04#)(0);
  conf_link1_txdata_sel                <= conf(16#04#)(1);
  conf_link1_elink_fire                <= conf(16#04#)(2);
  conf_link1_tp_fire                   <= conf(16#04#)(3);
  conf_link1_bunchctr_fire             <= conf(16#04#)(15 downto 4);
  conf_link1_tp_bctr                   <= conf(16#04#)(27 downto 16);
  conf_link1_txdata_reg   <= conf(16#05#)(31 downto 0);
'''

regs = [reg.strip() for reg in regs.split('\n') if reg.strip()]
regs = [reg.split(' ')[0] for reg in regs]
regs = [reg.split('_',2)[2] for reg in regs]

maxlen = max([len(reg) for reg in regs])

txt = ''

for reg in regs:
    txt +=  reg + (' '*(1 + (maxlen - len(reg))) ) + '<= ( '
    for link in range(1, numlinks + 1):
        txt += 'conf_link%i_'%link + reg
        txt += ' '*( 1 + (1 if link<10 and numlinks>9 else 0) + (maxlen - len(reg)) )
        txt += ', '
    txt = txt[:-2]
    txt += ' );\n'

print txt

