-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Common types package
---- File       : commontypes_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-05
-------------------------------------------------------------------------------
---- Description: Common useful types
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

-------------------------------------------------------------------------------
-- Header
-------------------------------------------------------------------------------
package commontypes_pkg is

  ---------------------------------------------------------------
  -- Data types
  ---------------------------------------------------------------
  type integer_array is array (natural range <>) of integer;
  
end package;
