----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_core_async - rtl
-- Description: 
--
--          Simple shift register with configurable delay and width
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bril_histogram_shift_register is
Generic(
    REGISTER_WIDTH  : natural;
    DELAY           : natural;
    --
    IS_CDC_REGISTER : string := "false" -- all cdc registers have to specify "true" here and have corresponding constraint set
);
Port (
    clk_i           : in std_logic;
    enable_i        : in std_logic := '1';
    input_i         : in std_logic_vector(REGISTER_WIDTH-1 downto 0);
    output_o        : out std_logic_vector(REGISTER_WIDTH-1 downto 0)
);
end bril_histogram_shift_register;

architecture rtl of bril_histogram_shift_register is

    -- all zeros constant
    constant ALL_ZEROS      : std_logic_vector(REGISTER_WIDTH-1 downto 0) := (others => '0');

begin

    -- no delay
    gen_no_delay: if DELAY = 0 generate
        output_o <= input_i when (enable_i = '1') else ALL_ZEROS;
    end generate gen_no_delay;

    -- 1 clock cycle delay
    gen_1clk_delay: if DELAY = 1 generate
        process(clk_i)
        begin
            if rising_edge(clk_i) then
                if enable_i = '1' then
                    output_o <= input_i;
                else
                    output_o <= ALL_ZEROS;
                end if;
            end if;
        end process;
    end generate gen_1clk_delay;
    
    -- N clock cycle delay
    gen_Nclk_delay: if DELAY > 1 generate
        -- define register
        type sr_type is array (DELAY-1 downto 0) of std_logic_vector(REGISTER_WIDTH-1 downto 0);
        signal shift_register : sr_type;
        -- cdc if needed
        attribute ASYNC_REG : string;
        attribute ASYNC_REG of shift_register : signal is IS_CDC_REGISTER;
    begin
        -- shift register
        process(clk_i)
        begin
            if rising_edge(clk_i) then
                if enable_i = '1' then
                    shift_register <= shift_register(shift_register'high - 1 downto 0) & input_i;
                else
                    shift_register <= shift_register(shift_register'high - 1 downto 0) & ALL_ZEROS;
                end if;
            end if;
        end process;
        -- assign output
        output_o <= shift_register(shift_register'high);
    end generate gen_Nclk_delay;

end rtl;
