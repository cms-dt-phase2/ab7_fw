----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/30/2018 02:11:54 PM
-- Design Name: 
-- Module Name: dt_roeb_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
--library UNIMACRO;
--use unimacro.Vcomponents.all;

library work;
--use work.ipbus.all;
--use work.TM7_data_types.all;
use work.TM7_readout_decl.all;
--use work.TM7_version.all;
--use work.ipbus_reg_types.all;
--use work.minmax.all;

use work.mp7_ttc_decl.all;
use work.mp7_data_types.all;


entity dt_roeb_tb is
--  Port ( );
end dt_roeb_tb;

architecture Behavioral of dt_roeb_tb is
    constant BXs : time := 25 ns;
    constant clk160_period : time := BXs/4;

    signal ttc_clk: std_logic;
    signal ttc_rst: std_logic;
    signal clk80: std_logic;
    signal clk160: std_logic;
    signal clk240: std_logic;

    signal l1a: std_logic := '0';
    signal bunch_ctr: bctr_t;
    signal evt_ctr_u: eoctr_t := (others => '0');

    signal hits_data : lword; 

    signal dtff_do : std_logic_vector(63 downto 0);
    signal dtff_empty, dtff_ae, dtff_rden : std_logic;
    signal dtff_evn: std_logic_vector(RO_EVN_WIDTH-1 downto 0);
    signal dtff_rdcount, dtff_wrcount: std_logic_vector(12 downto 0);


begin
        
dt_roeb: entity work.dt_roeb
port map(
    -- readout configuration (semi-static)
    latency         => to_unsigned(100,9),
    window          => to_unsigned(32,9),
    startpointer_cooldown => to_unsigned(50,6),
    max_disorder    => to_unsigned(2,4),
    -- reset & clocks
    ttc_rst         => ttc_rst, -- for all clocks
    ttc_clk         => ttc_clk, -- TTC and input data
    midclk          => clk160, -- Circ.Buf.-->FIFO
    rdclk           => clk80, --> FIFO read
    -- ttc inputs: ttc_clk clocked
    l1a             => l1a,
    evt_ctr         => evt_ctr_u,
    bunch_ctr       => bunch_ctr,
    -- input data: ttc_clk clocked
    input_clk       => clk240,
    hits_data       => hits_data,
    -- output fifo ports: rdclk clocked
    empty           => dtff_empty,
    almost_empty    => dtff_ae,
    rden            => '1',
    do              => dtff_do,
    evnout          => dtff_evn,
    ff_rdcount      => dtff_rdcount,
    ff_wrcount      => dtff_wrcount,
    -- debug & stats
    resetcounters   => '0',
    valid_hits_ctr  => open,
    waddradd        => open,
    bx_tdc_local    => open,
    l1aff_occ       => open,
    l1aff_wren_ctr  => open,
    startpntrff_occ => open,
    dbg_inwin       => open,
    dbg_outwin      => open,
    ff_wren_ctr     => open
    );

-- clocks
process 
begin
    ttc_clk <= '1';
    wait for BXs/2;
    ttc_clk <= '0';
    wait for BXs/2;
end process;

process 
begin
    clk80 <= '1';
    wait for BXs/4;
    clk80 <= '0';
    wait for BXs/4;
end process;

process 
begin
    clk160 <= '1';
    wait for BXs/8;
    clk160 <= '0';
    wait for BXs/8;
end process;

process 
begin
    clk240 <= '1';
    wait for BXs/12;
    clk240 <= '0';
    wait for BXs/12;
end process;





bunch_ctr_p: process
begin
   if unsigned(bunch_ctr) < 3563 then bunch_ctr <= std_logic_vector(unsigned(bunch_ctr) + 1);
   else bunch_ctr <= (others=>'0');
   end if;    
   wait for BXs;
end process;


stim_p: process
    constant l1a_latency: natural := 100;
begin
    dtff_rden <= '0','1' after 10*BXs;
    ttc_rst <= '0';--, '1' after 2*BXs, '0' after 8*BXs;

    hits_data <= -- .valid, .data = valid & "000000" & TDC hit (5bit) & bx couter (12hit) & TDC channel(8 bit) 
        (start=>'0',strobe=>'0', valid => '0', data => ('0' & "000000" & "00000" & std_logic_vector(to_unsigned(0,12)) & x"00")),
        (start=>'0',strobe=>'0', valid => '1', data => ('1' & "000000" & "00000" & std_logic_vector(to_unsigned(50,12)) & x"00")) after (70.0+0.0/6)*BXs,
        (start=>'0',strobe=>'0', valid => '1', data => ('1' & "000000" & "00000" & std_logic_vector(to_unsigned(48,12)) & x"00")) after (70.0+1.0/6)*BXs,
        (start=>'0',strobe=>'0', valid => '1', data => ('1' & "000000" & "00000" & std_logic_vector(to_unsigned(52,12)) & x"00")) after (70.0+2.0/6)*BXs,
        (start=>'0',strobe=>'0', valid => '1', data => ('1' & "000000" & "00000" & std_logic_vector(to_unsigned(53,12)) & x"00")) after (70.0+3.0/6)*BXs,
        (start=>'0',strobe=>'0', valid => '1', data => ('1' & "000000" & "00000" & std_logic_vector(to_unsigned(54,12)) & x"00")) after (70.0+4.0/6)*BXs,
        (start=>'0',strobe=>'0', valid => '1', data => ('1' & "000000" & "00000" & std_logic_vector(to_unsigned(55,12)) & x"00")) after (70.0+5.0/6)*BXs,
        (start=>'0',strobe=>'0', valid => '1', data => ('1' & "000000" & "00000" & std_logic_vector(to_unsigned(56,12)) & x"00")) after (70.0+6.0/6)*BXs,
        (start=>'0',strobe=>'0', valid => '1', data => ('1' & "000000" & "00000" & std_logic_vector(to_unsigned(57,12)) & x"00")) after (70.0+7.0/6)*BXs,
        (start=>'0',strobe=>'0', valid => '1', data => ('1' & "000000" & "00000" & std_logic_vector(to_unsigned(58,12)) & x"00")) after (70.0+8.0/6)*BXs,
        (start=>'0',strobe=>'0', valid => '1', data => ('1' & "000000" & "00000" & std_logic_vector(to_unsigned(59,12)) & x"00")) after (70.0+9.0/6)*BXs,
        (start=>'0',strobe=>'0', valid => '0', data => ('0' & "000000" & "00000" & std_logic_vector(to_unsigned(0,12)) & x"00")) after (70.0+10.0/6)*BXs
        ;

    l1a <= 
        '0',
        '1' after 150*BXs,
        '0' after 151*BXs,
        '1' after 160*BXs,
        '0' after 161*BXs,
        '1' after 170*BXs,
        '0' after 171*BXs
        ;
    evt_ctr_u <= 
        x"00000000",
        x"00000001" after 150*BXs,
        x"00000002" after 160*BXs,
        x"00000003" after 170*BXs
        ;

    wait;
end process;    
    
    
end Behavioral;
