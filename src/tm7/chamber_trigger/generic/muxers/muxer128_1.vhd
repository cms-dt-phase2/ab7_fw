-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Multiplexers components
---- Design unit: 128:1 bits multiplexer
---- File       : muxer128_1.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: Multiplexer 128:1 bit implemented to optimize synthesis 
----              resources and speed.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
use work.muxers_pkg.all;

entity MUXER128_1 is
  port (
    sel   : in  std_logic_vector(6 downto 0);
    d_in  : in  std_logic_vector(127 downto 0);
    d_out : out std_logic
  );
end entity;

architecture Behavioural of MUXER128_1 is
  signal int_d_out: std_logic_vector(1 downto 0);
begin
  MUX_CHAIN_GEN : for i in 0 to 1 generate
    MUX64_INST : MUXER64_1 port map (
      sel   => sel(5 downto 0),
      d_in  => d_in((64 * (i+1)) - 1 downto 64 * i),
      d_out => int_d_out(i)
    );
  end generate;

  MUX_OUT : MUXER2_1 port map (
    sel   => sel(6),
    d_in  => int_d_out,
    d_out => d_out
  );
  
end architecture;
