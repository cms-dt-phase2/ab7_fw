--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               CIEMAT                                                         
-- Engineer:              Javier Sastre Alvaro (javier.sastre@ciemat.es)
--                                                                                                 
-- Project Name:          TM7-GBTx                                                                
-- Module Name:           TM7_GBTx                                        
--                                                                                                 
-- Language:              VHDL'93                                                                  
--                                                                                                   
-- Target Device:         TM7 (Xilinx xc7vx330tffg1761-3 Virtex 7)                                                         
-- Tool version:          Vivado 2017.4                                                                
--                                                                                                   
-- Version:               1.0                                                                      
--
-- Description:            
--
-- Versions history:      DATE         VERSION   AUTHOR            DESCRIPTION
--
--                        06/03/2013   3.0       M. Barros Marin   First .vhd module definition
--                        13/12/2018   1.0       J. Sastre         GBT controlled trough IPBus registers. SCA commands not yet implemented on IPBus, currently AXI interface.           
--
-- Additional Comments:   Note!! Only ONE GBT Bank with ONE link can be used in this example design.     
--
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!                                                                                           !!
-- !! * The different parameters of the GBT Bank are set through:                               !!  
-- !!   (Note!! These parameters are vendor specific)                                           !!                    
-- !!                                                                                           !!
-- !!   - The MGT control ports of the GBT Bank module (these ports are listed in the records   !!
-- !!     of the file "<vendor>_<device>_gbt_bank_package.vhd").                                !! 
-- !!     (e.g. xlx_v6_gbt_bank_package.vhd)                                                    !!
-- !!                                                                                           !!  
-- !!   - By modifying the content of the file "<vendor>_<device>_gbt_bank_user_setup.vhd".     !!
-- !!     (e.g. xlx_v6_gbt_bank_user_setup.vhd)                                                 !! 
-- !!                                                                                           !! 
-- !! * The "<vendor>_<device>_gbt_bank_user_setup.vhd" is the only file of the GBT Bank that   !!
-- !!   may be modified by the user. The rest of the files MUST be used as is.                  !!
-- !!                                                                                           !!  
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--                                                                                              
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

-- Custom libraries and packages:
use work.ipbus.all;
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_exampledesign_package.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity TM7_GBTx is   
   port (   
      
      --===============--     
      -- General reset --     
      --===============--     

--      CPU_RESET                                      : in  std_logic;     
      
      --===============--
      -- Clocks scheme --
      --===============-- 
      
      -- Fabric clock:
      ----------------     
      
      clk_fr_p, clk_fr_n                                : in std_logic; -- 125 MHz   
      
      
      -- NOTE: the vc707_gbt_example_design use the user_clock at 156 MHz for DRP/SYSCLK. 
      -- In a first aproach, the eth_clk at 125 MHz will be used as free running clock for MGT.

      -- MGT(GTX) reference clock:
      ----------------------------
      
      -- Comment: * The MGT reference clock MUST be provided by an external clock generator.
      --
      --          * The MGT reference clock frequency must be 120MHz for the latency-optimized GBT Bank.      
      
      MGTHREFCLK0_P, MGTHREFCLK0_N                               : in  std_logic;

      -- Comment: * one of the TM7 Si5338 must be reprogrammed to provide a 120 MHZ clock to MGTHREFCLK0
      
        -- Frame clock:
        ---------------
        
        clk40_in_p, clk40_in_n                          : in std_logic;
      
      --==========--
      -- MGT(GTX) --
      --==========--                   
      
      -- Serial lanes:
      ----------------

        -- To be modified: SFP should be MGTHTXP0_117
      
      SFP_TX_P                                       : out std_logic;
      SFP_TX_N                                       : out std_logic;
      SFP_RX_P                                       : in  std_logic;
      SFP_RX_N                                       : in  std_logic;                  
      
      -- SFP control:
      ---------------
      
--      SFP_TX_DISABLE                                 : out std_logic;
        reset_mp_tx_hr                              : out std_logic;    --JSA--
        reset_mp_rx_hr                              : out std_logic;

        --=======--
        -- ipbus --
        --=======--
 
        eth_clkp, eth_clkn                               : in std_logic; -- 125 MHz   
        eth_txp, eth_txn: out std_logic;
        eth_rxp, eth_rxn: in std_logic;
        GA : in  STD_LOGIC_VECTOR(3 downto 0);
        dipswitch : in  STD_LOGIC_VECTOR(7 downto 0);
             
      --===============--      
      -- On-board LEDs --      
      --===============--

      GPIO_LED_0_LS                                  : out std_logic;
      GPIO_LED_1_LS                                  : out std_logic;
      GPIO_LED_2_LS                                  : out std_logic;
      GPIO_LED_3_LS                                  : out std_logic;
      GPIO_LED_4_LS                                  : out std_logic;
      GPIO_LED_5_LS                                  : out std_logic;
      GPIO_LED_6_LS                                  : out std_logic;
      GPIO_LED_7_LS                                  : out std_logic      
      
   );
end TM7_GBTx;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture structural of TM7_GBTx is
   
   --================================ Signal Declarations ================================--          

    signal board_id: std_logic_vector(31 downto 0);

   
   --===============--     
   -- General reset --     
   --===============--     

   signal reset_from_genRst                          : std_logic;    
   
   --===============--
   -- Clocks scheme -- 
   --===============--   
   
   -- Fabric clock:
   ----------------
   
    signal eth_clk, eth_clk_i, clk_fr, clk_fr_o, fabricClk_from_sysclkBufg, fabricClk_from_sysclkPll                 : std_logic; -- JSA --
    signal fabricClk_from_txPll : std_logic; -- JSA --

   -- MGT(GTX) reference clock:     
   ----------------------------     
  
   signal mgtRefClk_from_IbufdsGtxe2                : std_logic; -- JSA --

    -- Frame clock:   --JSA--
    ---------------
    signal clk40_bp_u, clk40_bp                     : std_logic;
    signal txFrameClk_from_txPll                    : std_logic;
    
    signal version : std_logic_vector (5 downto 0);
    
   --================--
   -- Clock component--
   --================--
   -- Vivado synthesis tool does not support mixed-language
   -- Solution: http://www.xilinx.com/support/answers/47454.html
   COMPONENT xlx_k7v7_tx_pll PORT(
      clk_in1: in std_logic;
      RESET: in std_logic;
      CLK_OUT1: out std_logic;
      LOCKED: out std_logic
   );
   END COMPONENT;
   
   --=========================--
   -- GBT Bank example design --
   --=========================--
   
   -- Control:
   -----------
   signal txPllReset                                 : std_logic;
   signal sysclkPllReset                               : std_logic; -- JSA --
   
   signal generalReset_from_user                     : std_logic;   
   signal resetgbtfpga_from_jtag                     : std_logic;
   signal resetgbtfpga_from_vio                      : std_logic;
      
   signal manualResetTx_from_user                    : std_logic; 
   signal manualResetRx_from_user                    : std_logic; 
   signal clkMuxSel_from_user                        : std_logic;       
   signal testPatterSel_from_user                    : std_logic_vector(1 downto 0); 
   signal loopBack_from_user                         : std_logic_vector(2 downto 0); 
   signal resetDataErrorSeenFlag_from_user           : std_logic; 
   signal resetGbtRxReadyLostFlag_from_user          : std_logic; 
   signal txIsDataSel_from_user                      : std_logic;
   signal rxBitSlipRstCount_from_gbtExmplDsgn        : std_logic_vector(7 downto 0);
      
   --------------------------------------------------      
   signal latOptGbtBankTx_from_gbtExmplDsgn          : std_logic;
   signal latOptGbtBankRx_from_gbtExmplDsgn          : std_logic;
   signal txFrameClkPllLocked_from_gbtExmplDsgn      : std_logic;
    signal sysclkPll_locked                             : std_logic;
   signal mgtReady_from_gbtExmplDsgn                 : std_logic; 
   signal rxWordClkReady_from_gbtExmplDsgn           : std_logic; 
   signal rxFrameClkReady_from_gbtExmplDsgn          : std_logic; 
   signal gbtRxReady_from_gbtExmplDsgn               : std_logic;    
   signal rxIsData_from_gbtExmplDsgn                 : std_logic;        
   signal gbtRxReadyLostFlag_from_gbtExmplDsgn       : std_logic; 
   signal rxDataErrorSeen_from_gbtExmplDsgn          : std_logic; 
   signal rxExtrDataWidebusErSeen_from_gbtExmplDsgn  : std_logic; 

   signal SFP_TX_DISABLE                             : std_logic; -- JSA --   
   
   -- Data:
   --------
   
   signal txData_from_gbtExmplDsgn                   : std_logic_vector(83 downto 0);
   signal rxData_from_gbtExmplDsgn                   : std_logic_vector(83 downto 0);
   
   signal txData_to_gbtExmplDsgn                     : std_logic_vector(83 downto 0);
   
   --------------------------------------------------      
   signal txExtraDataWidebus_from_gbtExmplDsgn       : std_logic_vector(115 downto 0);
   signal rxExtraDataWidebus_from_gbtExmplDsgn       : std_logic_vector(115 downto 0);
   
   signal mgt_rxword_s : std_logic_vector(39 downto 0);
   signal mgt_rxword_rev_s : std_logic_vector(39 downto 0);  
   signal mgt_txword_s : std_logic_vector(39 downto 0);
   signal mgt_txword_rev_s : std_logic_vector(39 downto 0);  
   signal gbt_txencdata_s : std_logic_vector(119 downto 0);
   signal gbt_rxencdata_s : std_logic_vector(119 downto 0);
   signal mgt_headerlocked_s : std_logic;
   
   --===========--
   -- Chipscope --
   --===========--
   
   signal vioControl_from_icon                       : std_logic_vector(35 downto 0); 
   signal txIlaControl_from_icon                     : std_logic_vector(35 downto 0); 
   signal rxIlaControl_from_icon                     : std_logic_vector(35 downto 0); 
   --------------------------------------------------
   signal sync_from_vio                              : std_logic_vector(11 downto 0);
   signal async_to_vio                               : std_logic_vector(17 downto 0);
      
   --=====================--
   -- BER                 --
   --=====================--
   signal modifiedBitsCnt                    : std_logic_vector(7 downto 0);
   signal countWordReceived                : std_logic_vector(31 downto 0);
   signal countBitsModified                : std_logic_vector(31 downto 0);
   signal countWordErrors                    : std_logic_vector(31 downto 0);
   signal gbtModifiedBitFlagFiltered    : std_logic_vector(127 downto 0);
   signal gbtErrorDetected                        : std_logic;
   signal gbtModifiedBitFlag                    : std_logic_vector(83 downto 0);
   
   --=====================--
   -- Latency measurement --
   --=====================--
   signal shiftTxClock_from_vio            : std_logic;
   signal txShiftCount_from_vio             : std_logic_vector(7 downto 0);
   signal DEBUG_CLK_ALIGNMENT_debug                  : std_logic_vector(2 downto 0);
   signal rxBitSlipRstOnEven_from_user              : std_logic;
   signal txAligned_from_gbtbank            : std_logic;
   signal txAlignComputed_from_gbtbank      : std_logic;
   signal txAligned_from_gbtbank_latched        : std_logic;
   signal txFrameClk_from_gbtExmplDsgn               : std_logic;
   signal txWordClk_from_gbtExmplDsgn                : std_logic;
   signal rxFrameClk_from_gbtExmplDsgn               : std_logic;
   signal rxWordClk_from_gbtExmplDsgn                : std_logic;
   --------------------------------------------------                                    
   signal txMatchFlag_from_gbtExmplDsgn              : std_logic;
   signal rxMatchFlag_from_gbtExmplDsgn              : std_logic;
   
   signal txEncodingSel                              : std_logic;
   signal rxEncodingSel                              : std_logic;
 
 
    -- Fabric clock generation --
    --=========================--
    
    COMPONENT sysclkPll PORT(
      clk_in1: in std_logic;
      RESET: in std_logic;
      CLK_OUT1: out std_logic;
      LOCKED: out std_logic
    );
    END COMPONENT;
   
     -- ILA component  --
     --================--
     -- Vivado synthesis tool does not support mixed-language
     -- Solution: http://www.xilinx.com/support/answers/47454.html
     COMPONENT xlx_k7v7_vivado_debug PORT(
        CLK: in std_logic;
        PROBE0: in std_logic_vector(83 downto 0);
        PROBE1: in std_logic_vector(39 downto 0);
        PROBE2: in std_logic_vector(119 downto 0);
        PROBE3: in std_logic_vector(1 downto 0)
     );
     END COMPONENT;
         
    -- IPBUS
	signal clk_ipb, rst_ipb, clk40ish, clk40, rst40, eth_refclk, clk720: std_logic;
 	signal ipb_in_ctrl, ipb_in_gbtx_ctrl: ipb_wbus;
 	signal ipb_out_ctrl, ipb_out_gbtx_ctrl: ipb_rbus;
	signal clk40_rst, clk40_sel, clk40_lock, clk40_stop, nuke, soft_rst: std_logic;
	signal oc_flag, ec_flag, payload_bc0, ttc_l1a_throttle, ttc_l1a_flag: std_logic;
    signal MAC_lsb : std_logic_vector(15 downto 0) := (others=>'0');

	signal leds: std_logic_vector(11 downto 0);
     
     -- Jtag to Axi component and signals:
     --     Used to control the design and monitor the signals in order to
     --     perform automatic tests.
     signal m_axi_awaddr 		:  STD_LOGIC_VECTOR(31 DOWNTO 0);
     signal m_axi_awprot         :  STD_LOGIC_VECTOR(2 DOWNTO 0);
     signal m_axi_awvalid         :  STD_LOGIC;
     signal m_axi_awready         :  STD_LOGIC;
     signal m_axi_wdata         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
     signal m_axi_wstrb         :  STD_LOGIC_VECTOR(3 DOWNTO 0);
     signal m_axi_wvalid         :  STD_LOGIC;
     signal m_axi_wready         :  STD_LOGIC;
     signal m_axi_bresp         :  STD_LOGIC_VECTOR(1 DOWNTO 0);
     signal m_axi_bvalid         :  STD_LOGIC;
     signal m_axi_bready         :  STD_LOGIC;
     signal m_axi_araddr         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
     signal m_axi_arprot         :  STD_LOGIC_VECTOR(2 DOWNTO 0);
     signal m_axi_arvalid         :  STD_LOGIC;
     signal m_axi_arready         :  STD_LOGIC;
     signal m_axi_rdata         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
     signal m_axi_rresp         :  STD_LOGIC_VECTOR(1 DOWNTO 0);
     signal m_axi_rvalid         :  STD_LOGIC;
     signal m_axi_rready         :  STD_LOGIC;
      
     COMPONENT jtagCtrl_gbtfpgaTest
       PORT (
         aclk : IN STD_LOGIC;
         aresetn : IN STD_LOGIC;
         m_axi_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
         m_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
         m_axi_awvalid : OUT STD_LOGIC;
         m_axi_awready : IN STD_LOGIC;
         m_axi_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
         m_axi_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
         m_axi_wvalid : OUT STD_LOGIC;
         m_axi_wready : IN STD_LOGIC;
         m_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
         m_axi_bvalid : IN STD_LOGIC;
         m_axi_bready : OUT STD_LOGIC;
         m_axi_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
         m_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
         m_axi_arvalid : OUT STD_LOGIC;
         m_axi_arready : IN STD_LOGIC;
         m_axi_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
         m_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
         m_axi_rvalid : IN STD_LOGIC;
         m_axi_rready : OUT STD_LOGIC
       );
     END COMPONENT;
     
         -- IC Debug
     ------------
     signal ic_ready                   : std_logic;
     signal ic_empty                   : std_logic;
     signal GBTx_address_to_gbtic      : std_logic_vector(7 downto 0);
     signal Register_addr_to_gbtic     : std_logic_vector(15 downto 0);
     signal nb_to_be_read_to_gbtic     : std_logic_vector(15 downto 0);
     signal start_write_to_gbtic       : std_logic;
     signal start_read_to_gbtic        : std_logic;
     signal data_to_gbtic              : std_logic_vector(7 downto 0);
     signal wr_to_gbtic                : std_logic;
     signal data_from_gbtic            : std_logic_vector(7 downto 0);
     signal rd_to_gbtic                : std_logic;
     signal ic_rd_gbtx_addr            : std_logic_vector(7 downto 0);
     signal ic_rd_mem_ptr              : std_logic_vector(15 downto 0);
     signal ic_rd_nb_of_words          : std_logic_vector(15 downto 0);
     
     signal GBTx_address         : std_logic_vector(7 downto 0);
     signal Register_ptr         : std_logic_vector(15 downto 0);
     signal ic_start_wr          : std_logic;
     signal ic_wr                : std_logic;
     signal ic_data_wr           : std_logic_vector(7 downto 0);
     signal ic_start_rd          : std_logic;
     signal ic_nb_to_be_read_rd  : std_logic_vector(15 downto 0);
     signal ic_empty_rd          : std_logic;
     signal ic_rd                : std_logic;
     signal ic_data_rd           : std_logic_vector(7 downto 0);
     
     signal GBTx_rd_addr         : std_logic_vector(7 downto 0);
     signal GBTx_rd_mem_ptr      : std_logic_vector(15 downto 0);
     signal GBTx_rd_mem_size     : std_logic_vector(15 downto 0);
 
     --SCA debug
     -----------
     signal reset_sca: std_logic;
     signal sca_ready: std_logic;
     signal start_sca: std_logic;
     signal start_reset: std_logic;
     signal start_connect: std_logic;
     
     signal sca_rx_parr: std_logic_vector(159 downto 0);
     signal sca_rx_done: std_logic;
     
     signal ec_line_tx  : std_logic_vector(1 downto 0);
     signal ec_line_rx  : std_logic_vector(1 downto 0);
     
     signal tx_ready: std_logic;
     signal tx_addr: std_logic_vector(7 downto 0);
     signal tx_ctrl: std_logic_vector(7 downto 0);
     signal tx_trid: std_logic_vector(7 downto 0);
     signal tx_ch: std_logic_vector(7 downto 0);
     signal tx_len: std_logic_vector(7 downto 0);
     signal tx_cmd: std_logic_vector(7 downto 0);
     signal tx_data: std_logic_vector(31 downto 0);
     
     signal rx_addr: std_logic_vector(7 downto 0);
     signal rx_ctrl: std_logic_vector(7 downto 0);
     signal rx_trid: std_logic_vector(7 downto 0);
     signal rx_ch: std_logic_vector(7 downto 0);
     signal rx_len: std_logic_vector(7 downto 0);
     signal rx_err: std_logic_vector(7 downto 0);
     signal rx_data: std_logic_vector(31 downto 0);
 
     signal m_axi_awaddr_to_gbtic : STD_LOGIC_VECTOR(31 DOWNTO 0);
     signal m_axi_awprot_to_gbtic : STD_LOGIC_VECTOR(2 DOWNTO 0);
     signal m_axi_awvalid_to_gbtic : STD_LOGIC;
     signal m_axi_awready_to_gbtic : STD_LOGIC;
     signal m_axi_wdata_to_gbtic : STD_LOGIC_VECTOR(31 DOWNTO 0);
     signal m_axi_wstrb_to_gbtic : STD_LOGIC_VECTOR(3 DOWNTO 0);
     signal m_axi_wvalid_to_gbtic : STD_LOGIC;
     signal m_axi_wready_to_gbtic : STD_LOGIC;
     signal m_axi_bresp_to_gbtic : STD_LOGIC_VECTOR(1 DOWNTO 0);
     signal m_axi_bvalid_to_gbtic : STD_LOGIC;
     signal m_axi_bready_to_gbtic : STD_LOGIC;
     signal m_axi_araddr_to_gbtic : STD_LOGIC_VECTOR(31 DOWNTO 0);
     signal m_axi_arprot_to_gbtic : STD_LOGIC_VECTOR(2 DOWNTO 0);
     signal m_axi_arvalid_to_gbtic : STD_LOGIC;
     signal m_axi_arready_to_gbtic : STD_LOGIC;
     signal m_axi_rdata_to_gbtic : STD_LOGIC_VECTOR(31 DOWNTO 0);
     signal m_axi_rresp_to_gbtic : STD_LOGIC_VECTOR(1 DOWNTO 0);
     signal m_axi_rvalid_to_gbtic : STD_LOGIC;
     signal m_axi_rready_to_gbtic : STD_LOGIC;
     
     signal m_axi_awaddr_to_gbtsc : STD_LOGIC_VECTOR(31 DOWNTO 0);
     signal m_axi_awprot_to_gbtsc : STD_LOGIC_VECTOR(2 DOWNTO 0);
     signal m_axi_awvalid_to_gbtsc : STD_LOGIC;
     signal m_axi_awready_to_gbtsc : STD_LOGIC;
     signal m_axi_wdata_to_gbtsc : STD_LOGIC_VECTOR(31 DOWNTO 0);
     signal m_axi_wstrb_to_gbtsc : STD_LOGIC_VECTOR(3 DOWNTO 0);
     signal m_axi_wvalid_to_gbtsc : STD_LOGIC;
     signal m_axi_wready_to_gbtsc : STD_LOGIC;
     signal m_axi_bresp_to_gbtsc : STD_LOGIC_VECTOR(1 DOWNTO 0);
     signal m_axi_bvalid_to_gbtsc : STD_LOGIC;
     signal m_axi_bready_to_gbtsc : STD_LOGIC;
     signal m_axi_araddr_to_gbtsc : STD_LOGIC_VECTOR(31 DOWNTO 0);
     signal m_axi_arprot_to_gbtsc : STD_LOGIC_VECTOR(2 DOWNTO 0);
     signal m_axi_arvalid_to_gbtsc : STD_LOGIC;
     signal m_axi_arready_to_gbtsc : STD_LOGIC;
     signal m_axi_rdata_to_gbtsc : STD_LOGIC_VECTOR(31 DOWNTO 0);
     signal m_axi_rresp_to_gbtsc : STD_LOGIC_VECTOR(1 DOWNTO 0);
     signal m_axi_rvalid_to_gbtsc : STD_LOGIC;
     signal m_axi_rready_to_gbtsc : STD_LOGIC;
             
     signal fast_clock_for_meas  : std_logic;
     signal cmd_delay            : std_logic_vector(31 downto 0);
          
     attribute mark_debug : string;
     
     attribute mark_debug of rxIsData_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of txFrameClkPllLocked_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of latOptGbtBankTx_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of mgtReady_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of rxWordClkReady_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of rxFrameClkReady_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of gbtRxReady_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of gbtRxReadyLostFlag_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of rxDataErrorSeen_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of rxExtrDataWidebusErSeen_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of latOptGbtBankRx_from_gbtExmplDsgn: signal is "TRUE";
     attribute mark_debug of generalReset_from_user: signal is "TRUE";
     attribute mark_debug of clkMuxSel_from_user: signal is "TRUE";
     attribute mark_debug of testPatterSel_from_user: signal is "TRUE";
     attribute mark_debug of loopBack_from_user: signal is "TRUE";
     attribute mark_debug of resetDataErrorSeenFlag_from_user: signal is "TRUE";
     attribute mark_debug of resetGbtRxReadyLostFlag_from_user: signal is "TRUE";
     attribute mark_debug of txIsDataSel_from_user: signal is "TRUE";
     attribute mark_debug of manualResetTx_from_user: signal is "TRUE";
     attribute mark_debug of manualResetRx_from_user: signal is "TRUE";
   --=====================================================================================--  

    signal clk240FromMgt_txwordclk_s : std_logic;
    signal clk40FromMgt_txwordclk_s : std_logic;

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
   
   --==================================== User Logic =====================================--
   
   --=============--
   -- SFP control -- 
   --=============-- 
      
    SFP_TX_DISABLE  <= '0'; 
    reset_mp_tx_hr  <= not SFP_TX_DISABLE;     -- Asserted Low: When asserted the optical outputs are disabled  
    reset_mp_rx_hr  <= '1';  
   --===============--
   -- General reset -- 
   --===============--
   generalReset_from_user <= resetgbtfpga_from_jtag or resetgbtfpga_from_vio;
   
   genRst: entity work.xlx_k7v7_reset
      generic map (
         CLK_FREQ                                    => 25e3)
      port map (     
         CLK_I                                       => txFrameClk_from_txPll, 
--         RESET1_B_I                                  => not CPU_RESET, 
         RESET1_B_I                                  => not generalReset_from_user, 
         RESET2_B_I                                  => not generalReset_from_user,
         RESET_O                                     => reset_from_genRst 
      ); 

   --===============--
   -- Clocks scheme -- 
   --===============--   
   
   -- Fabric clock:
   ----------------
   
	eth_clkIbufgds: IBUFDS_GTE2 port map(
          i => clk_fr_p,
          ib => clk_fr_n,
          o => clk_fr_o,
          ceb => '0'
      ); 
    
    bufg_fr: BUFG port map(
              i => clk_fr_o,
              o => clk_fr
          );
     
   -- MGT(GTX) reference clock:
   ----------------------------
   
   -- Comment: * The MGT reference clock MUST be provided by an external clock generator.
   --
   --          * The MGT reference clock frequency must be 120MHz for the latency-optimized GBT Bank. 
   
   mgtRefClk_ge: ibufds_gte2
      port map (
         O                                           => mgtRefClk_from_IbufdsGtxe2,
         ODIV2                                       => open,
         CEB                                         => '0',
         I                                           => MGTHREFCLK0_P,
         IB                                          => MGTHREFCLK0_N
      );
      
   -- Frame clock
   
	ibuf_clk40: IBUFGDS
      generic map (
        IBUF_LOW_PWR                                => FALSE,      
        IOSTANDARD                                  => "LVDS_25")
      port map(
			i => clk40_in_p,
			ib => clk40_in_n,
			o => clk40_bp_u
		);
		   
   txFrameclkGen_inst: entity work.xlx_k7v7_tx_phaligner
           Port map( 
               -- Reset
               RESET_IN              => txPllReset,
               
               -- Clocks
               CLK_IN                => clk40_bp_u, -- JSA --
               CLK_OUT               => txFrameClk_from_txPll,  -- JSA -- PLL bypass     
               sysclk_from_txPll    => open, -- fabricClk_from_txPll,
               
               -- Control
               SHIFT_IN              => shiftTxClock_from_vio,
               SHIFT_COUNT_IN        => txShiftCount_from_vio,
               
               -- Status
               LOCKED_OUT            => txFrameClkPllLocked_from_gbtExmplDsgn
           );
 
        
   --=========================--
   -- GBT Bank example design --
   --=========================--	
  gbtExmplDsgn_inst: entity work.xlx_k7v7_gbt_example_design
      generic map(
          NUM_LINKS                                              => NUM_LINK_Conf,                 -- Up to 4
          TX_OPTIMIZATION                                        => TX_OPTIMIZATION_Conf,          -- LATENCY_OPTIMIZED or STANDARD
          RX_OPTIMIZATION                                        => RX_OPTIMIZATION_Conf,          -- LATENCY_OPTIMIZED or STANDARD
          TX_ENCODING                                            => TX_ENCODING_Conf,         -- GBT_FRAME or WIDE_BUS
          RX_ENCODING                                            => RX_ENCODING_Conf,         -- GBT_FRAME or WIDE_BUS
          
          DATA_GENERATOR_ENABLE                                  => DATA_GENERATOR_ENABLE_Conf,
          DATA_CHECKER_ENABLE                                    => DATA_CHECKER_ENABLE_Conf,
          MATCH_FLAG_ENABLE                                      => MATCH_FLAG_ENABLE_Conf,
          CLOCKING_SCHEME                                        => CLOCKING_SCHEME_Conf
      )
      port map (
    
          --==============--
          -- Clocks       --
          --==============--
          FRAMECLK_40MHZ                                             => txFrameClk_from_txPll,
          XCVRCLK                                                    => mgtRefClk_from_IbufdsGtxe2,
          
          TX_FRAMECLK_O(1)                                           => txFrameClk_from_gbtExmplDsgn,        
          TX_WORDCLK_O(1)                                            => txWordClk_from_gbtExmplDsgn,          
          RX_FRAMECLK_O(1)                                           => rxFrameClk_from_gbtExmplDsgn,         
          RX_WORDCLK_O(1)                                            => rxWordClk_from_gbtExmplDsgn,      

            clk240FromMgt_txwordclk_o => clk240FromMgt_txwordclk_s,
            clk40FromMgt_txwordclk_o => clk40FromMgt_txwordclk_s,

          --==============--
          -- Reset        --
          --==============--
          GBTBANK_GENERAL_RESET_I                                    => reset_from_genRst,
          GBTBANK_MANUAL_RESET_TX_I                                  => manualResetTx_from_user,
          GBTBANK_MANUAL_RESET_RX_I                                  => manualResetRx_from_user,
          
          --==============--
          -- Serial lanes --
          --==============--
          GBTBANK_MGT_RX_P(1)                                        => SFP_RX_P,
          GBTBANK_MGT_RX_N(1)                                        => SFP_RX_N,
          GBTBANK_MGT_TX_P(1)                                        => SFP_TX_P,
          GBTBANK_MGT_TX_N(1)                                        => SFP_TX_N,
          
          --==============--
          -- Data             --
          --==============--        
          GBTBANK_GBT_DATA_I(1)                                      => txData_to_gbtExmplDsgn,
          GBTBANK_WB_DATA_I(1)                                       => (others => '0'),
          
          TX_DATA_O(1)                                               => txData_from_gbtExmplDsgn,            
          WB_DATA_O(1)                                               => open, --txExtraDataWidebus_from_gbtExmplDsgn,
          
          GBTBANK_GBT_DATA_O(1)                                      => rxData_from_gbtExmplDsgn,
          GBTBANK_WB_DATA_O(1)                                       => open, --rxExtraDataWidebus_from_gbtExmplDsgn,
          
          mgt_rxword_o(1) => mgt_rxword_s,
          mgt_txword_o(1) => mgt_txword_s,
          gbt_txencdata_o(1) => gbt_txencdata_s,
          gbt_rxencdata_o(1) => gbt_rxencdata_s,
          mgt_headerlocked_o(1) => mgt_headerlocked_s,
         
          --==============--
          -- Reconf.         --
          --==============--
          GBTBANK_MGT_DRP_RST                                        => '0',
          GBTBANK_MGT_DRP_CLK                                        => clk_fr, --
          
          --==============--
          -- TX ctrl        --
          --==============--
          TX_ENCODING_SEL_i(1)                                      => txEncodingSel,
          GBTBANK_TX_ISDATA_SEL_I(1)                                => txIsDataSel_from_user, --
          GBTBANK_TEST_PATTERN_SEL_I                                => testPatterSel_from_user, --
          
          --==============--
          -- RX ctrl      --
          --==============--
          RX_ENCODING_SEL_i(1)                                      => rxEncodingSel,
          GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I(1)                   => resetGbtRxReadyLostFlag_from_user, --
          GBTBANK_RESET_DATA_ERRORSEEN_FLAG_I(1)                    => resetDataErrorSeenFlag_from_user, --
          GBTBANK_RXFRAMECLK_ALIGNPATTER_I                          => DEBUG_CLK_ALIGNMENT_debug,
          GBTBANK_RXBITSLIT_RSTONEVEN_I(1)                          => rxBitSlipRstOnEven_from_user,
          --==============--
          -- TX Status    --
          --==============--
          GBTBANK_LINK_READY_O(1)                                   => mgtReady_from_gbtExmplDsgn, --
          GBTBANK_TX_MATCHFLAG_O                                    => txMatchFlag_from_gbtExmplDsgn,--
          GBTBANK_TX_ALIGNED_O(1)                                   => txAligned_from_gbtbank,
          GBTBANK_TX_ALIGNCOMPUTED_O(1)                             => txAlignComputed_from_gbtbank,
          
          --==============--
          -- RX Status    --
          --==============--
          GBTBANK_GBTRX_READY_O(1)                                  => gbtRxReady_from_gbtExmplDsgn, --
          GBTBANK_GBTRXREADY_LOST_FLAG_O(1)                         => gbtRxReadyLostFlag_from_gbtExmplDsgn, --
          GBTBANK_RXDATA_ERRORSEEN_FLAG_O(1)                        => rxDataErrorSeen_from_gbtExmplDsgn, --
          GBTBANK_RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O(1)           => rxExtrDataWidebusErSeen_from_gbtExmplDsgn, --
          GBTBANK_RX_MATCHFLAG_O(1)                                 => rxMatchFlag_from_gbtExmplDsgn, --
          GBTBANK_RX_ISDATA_SEL_O(1)                                => rxIsData_from_gbtExmplDsgn, --
                
          GBTBANK_RX_ERRORDETECTED_O(1)                             => gbtErrorDetected,
          GBTBANK_RX_BITMODIFIED_FLAG_O(1)                          => gbtModifiedBitFlag,
          
          RX_FRAMECLK_RDY_O(1)                                      => rxFrameClkReady_from_gbtExmplDsgn,
          
          GBTBANK_RXBITSLIP_RST_CNT_O(1)                            => rxBitSlipRstCount_from_gbtExmplDsgn,
          --==============--
          -- XCVR ctrl    --
          --==============--
          GBTBANK_LOOPBACK_I                                        => loopBack_from_user, --
          
          GBTBANK_TX_POL(1)                                         => '0', --
          GBTBANK_RX_POL(1)                                         => '0' --
     );     
            
    --=====================================--
    -- BER                                 --
    --=====================================--
    countWordReceivedProc: PROCESS(reset_from_genRst, rxframeclk_from_gbtExmplDsgn)
    begin
    
        if reset_from_genRst = '1' then
            countWordReceived <= (others => '0');
            countBitsModified <= (others => '0');
            countWordErrors    <= (others => '0');
            
        elsif rising_edge(rxframeclk_from_gbtExmplDsgn) then
            if gbtRxReady_from_gbtExmplDsgn = '1' then
                
                if gbtErrorDetected = '1' then
                    countWordErrors    <= std_logic_vector(unsigned(countWordErrors) + 1 );                
                end if;
                
                countWordReceived <= std_logic_vector(unsigned(countWordReceived) + 1 );
            end if;
            
            countBitsModified <= std_logic_vector(unsigned(modifiedBitsCnt) + unsigned(countBitsModified) );
        end if;
    end process;
    
    gbtModifiedBitFlagFiltered(127 downto 84) <= (others => '0');
    gbtModifiedBitFlagFiltered(83 downto 0) <= gbtModifiedBitFlag when gbtRxReady_from_gbtExmplDsgn = '1' else
                                                             (others => '0');
    
    countOnesCorrected: entity work.CountOnes
        Generic map (SIZE           => 128,
                    MAXOUTWIDTH        => 8)
        Port map( 
            Clock    => rxframeclk_from_gbtExmplDsgn,
         I        => gbtModifiedBitFlagFiltered,
         O        => modifiedBitsCnt);                              

-- Clocks and control IO

	infra: entity work.mp7_infra
	    generic map(
                MAC_ADDR => X"080030f30000", -- mac addr: 08:00:30:F3:00:00
                IP_ADDR =>  X"c0A80169"      -- fixed ip: 192.168.1.105
            )
		port map(
			gt_clkp => eth_clkp,
			gt_clkn => eth_clkn,
			gt_txp => eth_txp,
			gt_txn => eth_txn,
			gt_rxp => eth_rxp,
			gt_rxn => eth_rxn,
			leds => leds,
			clk_ipb => clk_ipb,
			rst_ipb => rst_ipb,
			clk40ish => clk40ish,
			refclk_out => eth_refclk,
			nuke => nuke,
			soft_rst => soft_rst,
			oc_flag => oc_flag,
			ec_flag => ec_flag,
			MAC_lsb => MAC_lsb,
			ipb_in_ctrl => ipb_out_ctrl,
			ipb_out_ctrl => ipb_in_ctrl,
			ipb_in_gbtx_ctrl => ipb_out_gbtx_ctrl,
            ipb_out_gbtx_ctrl => ipb_in_gbtx_ctrl
		);
		
		MAC_lsb <= x"02" & '0' & dipswitch(2 downto 0) & GA; -- range from 02:00 to 02:7F
 
  -- Control registers and board IO
                     
     ctrl: entity work.mp7_ctrl
         port map(
             clk => clk_ipb,
             rst => rst_ipb,
             ipb_in => ipb_in_ctrl,
             ipb_out => ipb_out_ctrl,
             nuke => nuke,
             soft_rst => soft_rst,
             board_id => board_id,
             clk40_rst => clk40_rst,
             clk40_sel => clk40_sel,
             clk40_lock => clk40_lock,
             clk40_stop => clk40_stop
         ); 
 
 	gbtx_ctrl: entity work.gbtx_ctrl
             port map(
                 clk => clk_ipb,
                 rst => rst_ipb,
                 ipb_in => ipb_in_gbtx_ctrl,
                 ipb_out => ipb_out_gbtx_ctrl,
                 version => version,
                 resetgbtfpga_from_vio => resetgbtfpga_from_vio,
                 testPatterSel_from_user => testPatterSel_from_user,
                 loopBack_from_user => loopBack_from_user(1 downto 0),
                 txIsDataSel_from_user => txIsDataSel_from_user,
                 manualResetTx_from_user => manualResetTx_from_user,
                 manualResetRx_from_user => manualResetRx_from_user
             );
   loopBack_from_user(2) <= '0';

   --==============--   
   -- Test control --   
   --==============--
   
   -- Chipscope:
   -------------   
   
   -- Comment: * Chipscope is used to control the example design as well as for transmitted and received data analysis.
   --
   --          * Note!! TX and RX DATA do not share the same ILA module (txIla and rxIla respectively) 
   --            because when receiving RX DATA from another board with a different reference clock, the 
   --            TX_FRAMECLK/TX_WORDCLK domains are asynchronous with respect to the RX_FRAMECLK/RX_WORDCLK domains.        
   --
   --          * After FPGA configuration using Chipscope, open the project "vc707_gbt_example_design.cpj" 
   --            that can be found in:
   --            "..\example_designs\xilix_k7v7\vc707\chipscope_project\".  
       
   --==============--   
   -- Test control --   
   --==============--
   jtag_master_inst : jtagCtrl_gbtfpgaTest
        PORT MAP (
          aclk => txFrameClk_from_txPll, --clk40FromMgt_txwordclk_s, -- 
          aresetn => txFrameClkPllLocked_from_gbtExmplDsgn,
          m_axi_awaddr => m_axi_awaddr,
          m_axi_awprot => m_axi_awprot,
          m_axi_awvalid => m_axi_awvalid,
          m_axi_awready => m_axi_awready,
          m_axi_wdata => m_axi_wdata,
          m_axi_wstrb => m_axi_wstrb,
          m_axi_wvalid => m_axi_wvalid,
          m_axi_wready => m_axi_wready,
          m_axi_bresp => m_axi_bresp,
          m_axi_bvalid => m_axi_bvalid,
          m_axi_bready => m_axi_bready,
          m_axi_araddr => m_axi_araddr,
          m_axi_arprot => m_axi_arprot,
          m_axi_arvalid => m_axi_arvalid,
          m_axi_arready => m_axi_arready,
          m_axi_rdata => m_axi_rdata,
          m_axi_rresp => m_axi_rresp,
          m_axi_rvalid => m_axi_rvalid,
          m_axi_rready => m_axi_rready
        );
               
  latOptGbtBankTx_from_gbtExmplDsgn                       <= '1';
  latOptGbtBankRx_from_gbtExmplDsgn                       <= '1';
   
   alignmenetLatchProc: process(clk40FromMgt_txwordclk_s)-- txFrameClk_from_txPll)
   begin
    
        if reset_from_genRst = '1' then
            txAligned_from_gbtbank_latched <= '0';
     
        elsif rising_edge(clk40FromMgt_txwordclk_s) then -- txFrameClk_from_txPll) then
    
            if txAlignComputed_from_gbtbank = '1' then
                txAligned_from_gbtbank_latched <= txAligned_from_gbtbank;
            end if;
    
        end if;
   end process;
                                                                                                                                                                        
   txILa: xlx_k7v7_vivado_debug
       port map (
          CLK => clk240FromMgt_txwordclk_s,
          PROBE0 => txData_from_gbtExmplDsgn,
          PROBE1 => mgt_txword_rev_s,
          PROBE2 => gbt_txencdata_s, --8b10b support removed
          PROBE3(0) => txIsDataSel_from_user,
          PROBE3(1) => '0');  
 
   rxIla: xlx_k7v7_vivado_debug
       port map (
          CLK => clk240FromMgt_txwordclk_s,
          PROBE0 => rxData_from_gbtExmplDsgn,
          PROBE1 => mgt_rxword_rev_s,
          PROBE2 => gbt_rxencdata_s, --8b10b support removed
          PROBE3(0) => rxIsData_from_gbtExmplDsgn,
          PROBE3(1) => mgt_headerlocked_s);

    mgt_word_rev_gen: for i in 0 to 39 generate 
        mgt_rxword_rev_s(i) <= mgt_rxword_s(39-i);
        mgt_txword_rev_s(i) <= mgt_txword_s(39-i);
    end generate;

---------------------------------- CONTROL -----------------------------------

   gbtsc_controller_inst: entity work.gbtsc_controller
       Port map( 
           -- AXI4LITE Interface
           tx_clock              => txFrameClk_from_txPll,
           meas_clock            => fast_clock_for_meas,
           
           -- AXI4LITE Interface
           S_AXI_ARESETN         => txFrameClkPllLocked_from_gbtExmplDsgn,
           S_AXI_AWADDR          => m_axi_awaddr(4 downto 0),
           S_AXI_AWVALID         => m_axi_awvalid,
           S_AXI_AWREADY         => m_axi_awready,
           S_AXI_WDATA           => m_axi_wdata,
           S_AXI_WSTRB           => m_axi_wstrb,
           S_AXI_WVALID          => m_axi_wvalid,
           S_AXI_WREADY          => m_axi_wready,
           S_AXI_BRESP           => m_axi_bresp,
           S_AXI_BVALID          => m_axi_bvalid,
           S_AXI_BREADY          => m_axi_bready,
           S_AXI_ARADDR          => m_axi_araddr(4 downto 0),
           S_AXI_ARVALID         => m_axi_arvalid,
           S_AXI_ARREADY         => m_axi_arready,
           S_AXI_RDATA           => m_axi_rdata,
           S_AXI_RRESP           => m_axi_rresp,
           S_AXI_RVALID          => m_axi_rvalid,
           S_AXI_RREADY          => m_axi_rready,
           
           -- To GBT-SC
           reset_gbtsc         => reset_sca,
           start_reset         => start_reset,
           start_connect       => start_connect,
           start_command       => start_sca,           
                   
           tx_address          => tx_addr,
           tx_transID          => tx_trid,
           tx_channel          => tx_ch,
           tx_len              => tx_len,
           tx_command          => tx_cmd,
           tx_data             => tx_data,           
           
           rx_reply_received_i => sca_rx_done,
           rx_address          => rx_addr,
           rx_transID          => rx_trid,
           rx_channel          => rx_ch,
           rx_len              => rx_len,
           rx_error            => rx_err,
           rx_data             => rx_data,
           
                          
          -- Data to GBT-IC State machine        
          ic_ready             => ic_ready,
          ic_empty             => ic_empty,
          
          -- Configuration
          GBTx_address         => GBTx_address_to_gbtic,
          Register_addr        => Register_addr_to_gbtic,
          nb_to_be_read        => nb_to_be_read_to_gbtic,
          
          -- Control        
          start_write          => start_write_to_gbtic,
          start_read           => start_read_to_gbtic,
          
          -- WRITE register(s)
          data_o               => data_to_gbtic,
          wr                   => wr_to_gbtic,
          
          -- READ register(s)        
          data_i               => data_from_gbtic,
          rd                   => rd_to_gbtic,
                  
          rd_gbtx_addr         => ic_rd_gbtx_addr,
          rd_mem_ptr           => ic_rd_mem_ptr,
          rd_nb_of_words       => ic_rd_nb_of_words,
          
           -- Status
           delay_cnter_o       => cmd_delay,
           wdt_error_o         => open,
           ready_o             => open
       );
   
   gbtsc_inst: entity work.gbtsc_top
   generic map(
       -- IC configuration
       g_IC_COUNT          => 1,
       g_IC_FIFO_DEPTH     => 1,
       
       -- EC configuration
       g_SCA_COUNT         => 1
   )
   port map(
       -- Clock & reset
       tx_clk_i                => txFrameClk_from_txPll,
       rx_clk_i                => txFrameClk_from_txPll,
       reset_i                 => reset_sca,
       
       -- IC configuration        
       tx_GBTx_address_i(0)    => GBTx_address_to_gbtic,
       tx_register_addr_i(0)   => Register_addr_to_gbtic,
       tx_nb_to_be_read_i(0)   => nb_to_be_read_to_gbtic,
       
       -- IC Status
       tx_ready_o(0)           => ic_ready,
       rx_empty_o(0)           => ic_empty,
       
       rx_gbtx_addr_o(0)       => ic_rd_gbtx_addr,
       rx_mem_ptr_o(0)         => ic_rd_mem_ptr,
       rx_nb_of_words_o(0)     => ic_rd_nb_of_words,
           
       -- IC FIFO control
       tx_wr_i(0)              => wr_to_gbtic,
       tx_data_to_gbtx_i(0)    => data_to_gbtic,
       
       rx_rd_i(0)              => rd_to_gbtic,
       rx_data_from_gbtx_o(0)  => data_from_gbtic,
       
       -- IC control
       ic_enable_i             => "1",
       tx_start_write_i        => start_write_to_gbtic,
       tx_start_read_i         => start_read_to_gbtic,
           
       -- SCA control
       sca_enable_i            => "1",
       start_reset_cmd_i       => start_reset,
       start_connect_cmd_i     => start_connect,
       start_command_i         => start_sca,
       inject_crc_error        => '0',
       
       -- SCA command
       tx_address_i            => tx_addr,
       tx_transID_i            => tx_trid,
       tx_channel_i            => tx_ch,
       tx_command_i            => tx_cmd,
       tx_data_i               => tx_data,
       
       rx_received_o(0)        => sca_rx_done,
       rx_address_o(0)         => rx_addr,
       rx_control_o(0)         => open,
       rx_transID_o(0)         => rx_trid,
       rx_channel_o(0)         => rx_ch,
       rx_len_o(0)             => rx_len,
       rx_error_o(0)           => rx_err,
       rx_data_o(0)            => rx_data,

       -- EC line
       ec_data_o(0)            => txData_to_gbtExmplDsgn(81 downto 80),
       ec_data_i(0)            => rxData_from_gbtExmplDsgn(81 downto 80),
       
       -- IC lines
       ic_data_o(0)            => txData_to_gbtExmplDsgn(83 downto 82),
       ic_data_i(0)            => rxData_from_gbtExmplDsgn(83 downto 82)
       
   );



   -- On-board LEDs:                   
   -----------------    
    
   GPIO_LED_0_LS                                     <= latOptGbtBankTx_from_gbtExmplDsgn and latOptGbtBankRx_from_gbtExmplDsgn;          
   GPIO_LED_1_LS                                     <= txFrameClkPllLocked_from_gbtExmplDsgn;
   GPIO_LED_2_LS                                     <= mgtReady_from_gbtExmplDsgn;
   GPIO_LED_3_LS                                     <= gbtRxReady_from_gbtExmplDsgn;
   GPIO_LED_4_LS                                     <= gbtRxReadyLostFlag_from_gbtExmplDsgn;
   GPIO_LED_5_LS                                     <= rxDataErrorSeen_from_gbtExmplDsgn;
   GPIO_LED_6_LS                                     <= rxExtrDataWidebusErSeen_from_gbtExmplDsgn;
   GPIO_LED_7_LS                                     <= '0';
         
version <= std_logic_vector(to_unsigned(6,6));   
   
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--