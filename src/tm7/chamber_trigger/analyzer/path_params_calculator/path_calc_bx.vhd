-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Analyzer
---- File       : path_calc_bx.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-03
-------------------------------------------------------------------------------
---- Description: Computes BX value (not BX time) for a given segment.
----              It takes 4 clock cycles (3 division + 1 final registration)
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.dt_constants_pkg.all;
use work.arith_pkg.all;

entity PATH_CALC_BX is
  port (
    clk        : in  std_logic;
    rst        : in  std_logic;
    enable     : in  std_logic;
    bx_time    : in  std_logic_vector(16 downto 0);
    bx_id      : out std_logic_vector(11 downto 0);
    bx_id_prec : out std_logic_vector(2 downto 0)
  );
end entity;

architecture TwoProcess of PATH_CALC_BX is

  -- Internal signals to be registered.
  type reg_type is record
    bx_id      : std_logic_vector(11 downto 0);
    bx_id_prec : std_logic_vector(2 downto 0);
  end record;

  constant regDefault : reg_type := (
    bx_id      => (others => '0'),
    bx_id_prec => (others => '0')
  );
  signal reg, nextReg : reg_type := regDefault;

  signal bx_id_long : std_logic_vector(23 downto 0);
  signal numerator  : std_logic_vector(23 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  bx_id      <= reg.bx_id;
  bx_id_prec <= reg.bx_id_prec;

  ---------------------------------------------------------------
  -- Auxiliary signals
  --
  -- 3 additional precision bits.
  numerator <= "0000" & bx_time & "000";

  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, bx_id_long)
  begin
    -- Default values.
    nextReg <= reg;

    -- If 2nd precision bit is enabled, value is rounded to the upper nearest.
    if bx_id_long(2) = '1' then 
      if unsigned(bx_id_long(14 downto 3)) < to_unsigned(MAX_BX_IDX, 12) then 
        nextReg.bx_id <= bx_id_long(14 downto 3) + '1';
      else 
        nextReg.bx_id <= bx_id_long(14 downto 3) - 
                         std_logic_vector(to_unsigned(MAX_BX_IDX, 12));
      end if;
    else 
      nextReg.bx_id <= bx_id_long(14 downto 3);
    end if;

    nextReg.bx_id_prec <= bx_id_long(2 downto 0);
  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1' then 
        reg <= regDefault;
      elsif enable = '1' then
        reg <= nextReg;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  -- Division takes 3 clock cycles.
  CALC_BX_DIVISION : DIVISION_B24B6 
  generic map ( DENOM_INV_BITS => 20 )
  port map (    clk         => clk,
    rst         => rst,
    enable      => enable,
    numerator   => numerator,
    denominator => std_logic_vector(to_unsigned(LHC_CLK_FREQ, 6)),
    div_by_zero => open,
    result      => bx_id_long
  );

end architecture;
