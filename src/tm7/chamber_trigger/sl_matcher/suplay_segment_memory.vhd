-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Super layers phi segment matcher
---- File       : suplay_segment_memory.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-03
-------------------------------------------------------------------------------
---- Description: Memory and additional logic to store segments for 
----              one superlayer. It's divided into up to 32 different
----              subunits (buffers), each one stores information associated to
----              a BX value.
----              Every buffer has 8 positions in depth.
----
----              Reading process takes 3 clock cycles after setting address
----              and read enable signal.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;
use work.dt_common_pkg.all;
use work.dt_constants_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.dt_buffers_pkg.all;
use work.sl_matcher_pkg.all;

entity SUPLAY_SEGMENT_MEMORY is
  port (
    clk         : in  std_logic;
    rst         : in  std_logic;
    slSegMemIn  : in  SupLaySegmMemIn_t;
    slSegMemOut : out SupLaySegmMemOut_t
  );
end entity;

architecture TwoProcess of SUPLAY_SEGMENT_MEMORY is

  constant NULL_BX_ID : std_logic_vector(11 downto 0) := (others => '0');
  
  type state_type is (STBY, GET_DATA_FROM_FIFO);

  -- Internal registers.
  type reg_type is record
    state                 : state_type;
    stopped               : std_logic;
    bx_counter_lsb        : std_logic;
    at_least_1_highq      : std_logic;
    mem_rd                : std_logic;
    valid_segment         : std_logic;
    valid_segment_piped   : std_logic;
    valid_segment_piped_2 : std_logic;
    -- Counts from 0 to 16 both included
    bx_cycle              : std_logic_vector(5 downto 0);
    buffers_empty         : std_logic_vector(BX_WINDOW_WIDTH - 1 downto 0);
    buffer_with_highq     : std_logic_vector(BX_WINDOW_WIDTH - 1 downto 0);
    next_seg_in_this_bx   : SegCount_arr_t;
    last_items_in_bx_buf  : ExtendedSegCount_arr_t;
    add_rd_main_part      : std_logic_vector(BX_PTR_LEN - 1 downto 0);
    add_rd_low_part       : std_logic_vector(SEGMENT_PTR_LEN - 1 downto 0);
    segmentOut            : DTSegment_t;
  end record;

  constant regDefault : reg_type := (
    state                 => STBY,
    stopped               => '0',
    bx_counter_lsb        => '0',
    at_least_1_highq      => '0',
    mem_rd                => '0',
    valid_segment         => '0',
    valid_segment_piped   => '0',
    valid_segment_piped_2 => '0',
    bx_cycle              => (others => '0'),
    buffers_empty         => (others => '1'),
    buffer_with_highq     => (others => '0'),
    next_seg_in_this_bx   => (others => (others => '0')),
    last_items_in_bx_buf  => (others => (others => '0')),
    add_rd_main_part      => (others => '0'),
    add_rd_low_part       => (others => '0'),
    segmentOut            => DT_SEGMENT_NULL
  );
  signal reg, nextReg : reg_type := regDefault;

  -- Auxiliary signals
  signal reset_cnt    : std_logic := '0';
  signal mem_wr       : std_logic := '0';
  signal fifo_rd      : std_logic := '0';
  signal buffers_full : std_logic_vector(BX_WINDOW_WIDTH - 1 downto 0);
  
  signal add_wr : std_logic_vector(BX_PTR_LEN + SEGMENT_PTR_LEN - 1 downto 0);
  signal add_rd : std_logic_vector(BX_PTR_LEN + SEGMENT_PTR_LEN - 1 downto 0);
  --
  signal last_ptr_rd_buffer_low : SegPointer_t;
  signal last_ptr_rd_buffer_mid : SegPointer_t;
  signal last_ptr_rd_buffer_hig : SegPointer_t;
  signal segmentOut             : DTSegment_t;

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  slSegMemOut.seg_valid            <= reg.valid_segment_piped_2;
  slSegMemOut.stopped              <= reg.stopped;
  slSegMemOut.at_least_1_highq     <= reg.at_least_1_highq;
  slSegMemOut.last_items_in_bx_buf <= reg.last_items_in_bx_buf;
  slSegMemOut.buffers_empty        <= reg.buffers_empty;
  slSegMemOut.buffer_with_highq    <= reg.buffer_with_highq;
  slSegMemOut.fifo_rd              <= fifo_rd;
  slSegMemOut.segment              <= reg.segmentOut;

  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, slSegMemIn, reset_cnt, buffers_full, 
                 last_ptr_rd_buffer_low, last_ptr_rd_buffer_mid,
                 last_ptr_rd_buffer_hig, segmentOut
  )
    variable var_segment              : DTSegment_t;
    variable bx_id_lsb                : std_logic_vector(4 downto 0);
    variable bx_wr_idx                : integer;
    variable bx_rd_idx                : integer;
    variable bx_rd_idx_low            : integer;
    variable bx_rd_idx_high           : integer;
    variable segment_within_bx_window : std_logic;
    variable final_ptr_rd_buffer_mid  : SegPointer_t;
    variable final_ptr_rd_buffer_hig  : SegPointer_t;
  begin
    -- Default values
    nextReg <= reg;
    fifo_rd <= '0';
    mem_wr  <= '0';

    -- Data aliases.
    var_segment := slSegMemIn.segment;

    -- General registers
    nextReg.bx_counter_lsb <= slSegMemIn.bx_counter(0);

    LAST_ITEMS_REG_LOOP : for i in 0 to BX_WINDOW_WIDTH - 1 loop
      -- Aliases
      buffers_full(i) <= reg.next_seg_in_this_bx( i )( SEGMENT_PTR_LEN );
      --
      -- In this version the number of items in each BX buffers MUST count, not
      -- only the real items, but also has to include the number of stored 
      -- pointers (pointing to next and previous BX items inside their 
      -- respective buffers).
      -- Both ammounts will be the total number of "fake" segments that must be
      -- matched against the other SL to confront BX/BX, BX/BX+1 and BX/BX-1.
      --
      if i = 0 then
        nextReg.last_items_in_bx_buf(i) 
                  <= ("00" & reg.next_seg_in_this_bx( i ))     +  
                     ("00" & reg.next_seg_in_this_bx( i + 1 )) + 
                     ("00" & reg.next_seg_in_this_bx( BX_WINDOW_WIDTH - 1 ));

      elsif i = BX_WINDOW_WIDTH - 1 then
        nextReg.last_items_in_bx_buf(i) 
                  <= ("00" & reg.next_seg_in_this_bx( i )) +  
                     ("00" & reg.next_seg_in_this_bx( 0 )) + 
                     ("00" & reg.next_seg_in_this_bx( i - 1 ));
      else
        nextReg.last_items_in_bx_buf(i) 
                  <= ("00" & reg.next_seg_in_this_bx( i ))     +  
                     ("00" & reg.next_seg_in_this_bx( i + 1 )) + 
                     ("00" & reg.next_seg_in_this_bx( i - 1 ));
      end if;
    end loop;
    --
    -- BX cycles counter. It will be used to limit the time this module will
    -- be retrieving data from fifo after it's triggered.
    --
    if reset_cnt = '1' then
      nextReg.bx_cycle <= (others => '0');
    -- BX is changing. 
    elsif reg.bx_counter_lsb /= slSegMemIn.bx_counter(0) then
      nextReg.bx_cycle <= reg.bx_cycle + '1';
    end if;
    
    -- Ordinary window (not around BX 3563)
    if (slSegMemIn.bx_window_low < slSegMemIn.bx_window_high and
        var_segment.bx_id        >= slSegMemIn.bx_window_low and
        var_segment.bx_id        <= slSegMemIn.bx_window_high) 
      or
       -- Window around BX 3563
       (slSegMemIn.bx_window_low > slSegMemIn.bx_window_high and
          (
            (var_segment.bx_id >= slSegMemIn.bx_window_low and 
             var_segment.bx_id <= std_logic_vector(to_unsigned(MAX_BX_IDX, 12)))
          or
            (var_segment.bx_id >= NULL_BX_ID and 
             var_segment.bx_id <= slSegMemIn.bx_window_high)
          )
       ) 
    then segment_within_bx_window := '1';
    else segment_within_bx_window := '0';
    end if;
    --
    -- The highest address part is devoted to determine the buffer position
    -- inside the whole memory space. The lower part identifies the element's
    -- index stored into a specific BX storage area.
    --
    -- This variable is only 5 bits in length because the maximun considered
    -- number of BX's is 32.
    -- It's necessary to consider separately those cases where window is 
    -- around last BX value because last 5 bits will be coincident on two 
    -- different BX's, within the window, due to the change from BX 3563 to
    -- BX 0 and following.
    --
    if slSegMemIn.bx_window_low < slSegMemIn.bx_window_high then 
      -- Ordinary case
      bx_id_lsb := slSegMemIn.segment.bx_id(4 downto 0);
    else
      --
      -- Case with BX 0 inside the window.
      -- To get an unique index (bits 4-0 from "bx_id") those identifiers 
      -- between 0 and 30 (both included) must be incremented in 12.
      --
      if slSegMemIn.segment.bx_id(4 downto 0) >= "00000" and 
         slSegMemIn.segment.bx_id(4 downto 0) <  "11111"
      then
        bx_id_lsb := slSegMemIn.segment.bx_id(4 downto 0) + 
                     std_logic_vector(to_unsigned(12, 5));
      else
        bx_id_lsb := slSegMemIn.segment.bx_id(4 downto 0);
      end if;
    end if;

    bx_wr_idx := to_integer(unsigned(bx_id_lsb));
    add_wr <= bx_id_lsb & 
              reg.next_seg_in_this_bx(bx_wr_idx)(SEGMENT_PTR_LEN - 1 downto 0);

    ---------------------------------------
    -- Data gathering FSM
    --
    case reg.state is
      when STBY =>
        reset_cnt <= '1';
        --
        -- 'start_storing' should be HIGH only for one clock cycle at the 
        -- beginning of each processing round.
        --
        if slSegMemIn.start_storing = '1' then
          nextReg.stopped                <= '0';
          nextReg.at_least_1_highq       <= '0';
          nextReg.buffers_empty          <= (others => '1');
          nextReg.buffer_with_highq      <= (others => '0');
          nextReg.next_seg_in_this_bx    <= (others => (others => '0'));
          nextReg.state                  <= GET_DATA_FROM_FIFO;
        end if;

      when GET_DATA_FROM_FIFO =>
        reset_cnt <= '0';
        --
        -- Whenever a segment is valid (within the BX window), we must retrieve 
        -- the next one from FIFO and decide if current one must be written 
        -- to memory or not.reset_cnt
        --
        if slSegMemIn.fifo_empty = '0' and segment_within_bx_window = '1' then
          fifo_rd   <= '1';
          reset_cnt <= '1';
          --
          -- This first condition ensures that no more than the maximum items 
          -- per buffer are stored. Additional segments will be lost.
          --
          if buffers_full(bx_wr_idx) = '0' then

            if segment_is_high_q(var_segment) = '1' then
              nextReg.at_least_1_highq             <= '1';
              nextReg.buffer_with_highq(bx_wr_idx) <= '1';
            end if;

            mem_wr <= '1';

            nextReg.buffers_empty(bx_wr_idx)       <= '0';
            nextReg.next_seg_in_this_bx(bx_wr_idx) <=
                  reg.next_seg_in_this_bx(bx_wr_idx) + '1';
          end if;

        ---------------------------------------
        --
        -- We wait a maximum value of programmed BX's until we stop retrieving 
        -- data from FIFO or until there is a segment in FIFO pending to be 
        -- processed but outside the BX margin window.
        --
        elsif slSegMemIn.fifo_empty = '0' or 
              reg.bx_cycle >= slSegMemIn.slphi_match_mem_bx_wait
        then
          nextReg.stopped <= '1';
          nextReg.state   <= STBY;
        end if;
    end case;

    ---------------------------------------
    -- Logic for reading memory
    -- 2 clock cycles to get data.
    ---------------------------------------
    --
    -- Points to current BX buffer and lower/higher associated buffers.
    bx_rd_idx      := to_integer(unsigned(slSegMemIn.bx_buffer_ptr));
    bx_rd_idx_low  := to_integer(unsigned(slSegMemIn.bx_buffer_ptr - '1'));
    bx_rd_idx_high := to_integer(unsigned(slSegMemIn.bx_buffer_ptr + '1'));
    
    ---------------------------
    -- First clock cycle
    ---------------------------
    last_ptr_rd_buffer_low <= "00" & reg.next_seg_in_this_bx( bx_rd_idx_low );
    last_ptr_rd_buffer_mid <= "00" & reg.next_seg_in_this_bx( bx_rd_idx );
    last_ptr_rd_buffer_hig <= "00" & reg.next_seg_in_this_bx( bx_rd_idx_high );
    
    final_ptr_rd_buffer_mid := slSegMemIn.seg_item_ptr - last_ptr_rd_buffer_low;
    final_ptr_rd_buffer_hig := slSegMemIn.seg_item_ptr - 
                               last_ptr_rd_buffer_low  - 
                               last_ptr_rd_buffer_mid;
    --
    -- Selects buffer from where segments will be retrieved.
    -- Whenever the the pointer is within a valid buffer/item pointer interval
    -- memory data is considered "valid".
    --
    -- Only in that cases where input "seg_item_ptr" points outside the valid
    -- data memory region, for a given "bx_buffer" outgoing data is 
    -- "invalidated".
    --
    if slSegMemIn.mem_rd = '1' then
      if slSegMemIn.seg_item_ptr < last_ptr_rd_buffer_low 
      then
        nextReg.add_rd_main_part  <= slSegMemIn.bx_buffer_ptr - '1';
        nextReg.add_rd_low_part  
                  <= slSegMemIn.seg_item_ptr(SEGMENT_PTR_LEN - 1 downto 0);
                  
        if reg.buffers_empty(bx_rd_idx_low) = '1'
        then nextReg.valid_segment <= '0';
        else nextReg.valid_segment <= '1';
        end if;

      elsif final_ptr_rd_buffer_mid < last_ptr_rd_buffer_mid
      then
        nextReg.add_rd_main_part  <= slSegMemIn.bx_buffer_ptr;
        nextReg.add_rd_low_part 
                  <= final_ptr_rd_buffer_mid(SEGMENT_PTR_LEN - 1 downto 0);

        if reg.buffers_empty(bx_rd_idx) = '1'
        then nextReg.valid_segment <= '0';
        else nextReg.valid_segment <= '1';
        end if;
                  
      elsif final_ptr_rd_buffer_hig < last_ptr_rd_buffer_hig
      then
        nextReg.add_rd_main_part <= slSegMemIn.bx_buffer_ptr + '1';
        nextReg.add_rd_low_part  
                  <= final_ptr_rd_buffer_hig(SEGMENT_PTR_LEN - 1 downto 0);

        if reg.buffers_empty(bx_rd_idx_high) = '1'
        then nextReg.valid_segment <= '0';
        else nextReg.valid_segment <= '1';
        end if;

      else
          nextReg.valid_segment    <= '0';
          nextReg.add_rd_main_part <= (others => '0');
          nextReg.add_rd_low_part  <= (others => '0');
      end if;
    end if;

    nextReg.mem_rd <= slSegMemIn.mem_rd;

    ---------------------------
    -- Second clock cycle
    ---------------------------
    nextReg.valid_segment_piped <= reg.valid_segment;

    ---------------------------
    -- Third clock cycle
    ---------------------------
    -- Extra registration for timing adjustment.
    nextReg.valid_segment_piped_2 <= reg.valid_segment_piped;

    if reg.valid_segment_piped = '1' 
    then nextReg.segmentOut <= segmentOut;
    else nextReg.segmentOut <= DT_SEGMENT_NULL;
    end if;

  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1' 
      then reg <= regDefault;
      else reg <= nextReg;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  -- Aliases
  add_rd <= reg.add_rd_main_part & reg.add_rd_low_part;
  
  --
  -- Memory block to store up to 512 segments. It will be partitioned
  -- into 32 sub-buffers with 8 memory positions each.
  --
  SEGMENTS_MEMORY : DTSEGMENT_MATCHER_MEMORY
  port map (
    rst        => rst,
    clk_wr     => clk,
    ena_wr     => mem_wr,
    add_wr     => '0' & add_wr,
    segmentIn  => slSegMemIn.segment,
    clk_rd     => clk,
    ena_rd     => reg.mem_rd,
    add_rd     => '0' & add_rd,
    segmentOut => segmentOut
  );

end architecture;
