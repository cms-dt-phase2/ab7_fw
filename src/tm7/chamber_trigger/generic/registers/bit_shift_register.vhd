-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Registers
---- File       : bit_shift_register.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-06
-------------------------------------------------------------------------------
---- Description: Shift register for a std_logic
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--

entity BIT_SHIFT_REGISTER is
  generic (SHIFT_STEPS : integer := 4);
  port(
    clk     : in  std_logic;
    enable  : in  std_logic;
    bit_in  : in  std_logic;
    bit_out : out std_logic
  );
end entity;

architecture RTL of BIT_SHIFT_REGISTER is
  signal shifter_vector : std_logic_vector(SHIFT_STEPS - 1 downto 0);

begin
  bit_out <= shifter_vector(SHIFT_STEPS - 1);
  
  process(clk)
  begin
    if clk'event and clk = '1' then
      if enable = '1' then
        shifter_vector <= shifter_vector(SHIFT_STEPS - 2 downto 0) & bit_in;
      end if;
    end if;
  end process;

end architecture;
