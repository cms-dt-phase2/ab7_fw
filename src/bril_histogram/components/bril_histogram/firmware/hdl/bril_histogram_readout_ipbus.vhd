----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_readout_ipbus - rtl
-- Description: 
--          Inspired by the ipbus_roreg_v module from the ipbus source code (Dave Newbold, April 2017)
--          This is the simple fifo readout interface for the bril histogram module. can be used when the DAQ system communication relies on ipbus

----------------------------------------------------------------------------------

-- standard include
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- ipbus definitions
use work.ipbus.all;

entity bril_histogram_readout_ipbus is
port(
    clk                     : in std_logic;
    reset                   : in std_logic;
    ipbus_in                : in ipb_wbus;
    ipbus_out               : out ipb_rbus;
    -- fifo status
    fifo_empty_i            : in std_logic;
    fifo_data_valid_i       : in std_logic;
    fifo_words_cnt_i        : in std_logic_vector(31 downto 0);
    -- fifo read interface
    fifo_rd_en_o            : out std_logic;
    fifo_data_i             : in std_logic_vector(31 downto 0)
);

end bril_histogram_readout_ipbus;

architecture rtl of bril_histogram_readout_ipbus is
    
    -- constants
    constant ADDR_WIDTH            : positive := 1; -- we have only two "registers"
    constant N_REG                 : positive := 2 ** ADDR_WIDTH;
    
    -- sels
    constant DATA_SEL              : natural := 0;
    constant STATUS_SEL            : natural := 1;
    
    -- signals
    signal sel                     : integer range 0 to 2 ** ADDR_WIDTH - 1 := 0;
    signal valid                   : std_logic;
    signal ack                     : std_logic;

begin

    -- address calculating
    sel <= to_integer(unsigned(ipbus_in.ipb_addr(ADDR_WIDTH - 1 downto 0))) when ADDR_WIDTH > 0 else 0;
    valid <= '1' when sel < N_REG else '0';
    
    -- process to read fifo
    process(clk)
    begin
        if reset = '1' then
            ack <= '0';
            fifo_rd_en_o <= '0';
        elsif rising_edge(clk) then
            -- rd en logic
            if ack = '0' and ipbus_in.ipb_strobe = '1' and ipbus_in.ipb_write = '0' and sel = DATA_SEL and fifo_empty_i = '0' then
                fifo_rd_en_o <= '1';
            else
                fifo_rd_en_o <= '0';
            end if;
            -- ack logic (two give more time to the fifo
            ack <= (ipbus_in.ipb_strobe and not ipbus_in.ipb_write and valid) and not ack;
        end if;
    end process;
    
    -- ipbus data assignment
    ipbus_out.ipb_rdata <=  fifo_data_i when sel = DATA_SEL else
                            (fifo_empty_i & fifo_data_valid_i & "00" & fifo_words_cnt_i(27 downto 0)) when sel = STATUS_SEL else 
                            (others => '0');
    ipbus_out.ipb_ack <= ack;
    ipbus_out.ipb_err <= ipbus_in.ipb_strobe and (ipbus_in.ipb_write or not valid);

end rtl;

