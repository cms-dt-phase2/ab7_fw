-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Quality-based filter for global primitives
---- File       : primitive_quality_filter.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-07
-------------------------------------------------------------------------------
---- Description: Rejects PRIMITIVES by comparing their chi-square value 
----              and other global parameters.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;
use work.dt_common_pkg.all;
use work.dt_constants_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.filters_pkg.all;
use work.xil_memory_pkg.all;

entity PRIMITIVE_QUALITY_FILTER is
  port (
    clk               : in  std_logic;
    rst               : in  std_logic;
    expire_bx_num     : in  std_logic_vector(5 downto 0);
    primQualFilterIn  : in  PrimitiveFilterIn_t;
    primQualFilterOut : out PrimitiveFilterOut_t
  );
end entity;

architecture TwoProcess of PRIMITIVE_QUALITY_FILTER is

  constant ADDRESS_ZERO : std_logic_vector(4 downto 0) := (others => '0');

  type states_t is (STDBY, WRITE_STORED_PRIMITIVES, WRITE_CURRENT_PRIMITIVE, 
                    RD_FIFO_WS);

  -- Internal registers
  type reg_type is record
    state             : states_t;
    clear_filter      : std_logic;
    filter_is_empty   : std_logic;
    ffIn_rd           : std_logic;
    bx_counter        : std_logic_vector(11 downto 0);
    mem_add_wr        : std_logic_vector(4 downto 0);
    mem_add_rd        : std_logic_vector(4 downto 0);
    current_primitive : DTPrimitive_t;
  end record;

  constant regDefault : reg_type := (
    state             => STDBY,
    clear_filter      => '0',
    -- Filter is empty after a reset, so no "current primitive" is available.
    filter_is_empty   => '1',
    ffIn_rd           => '0',
    --               
    bx_counter        => (others => '0'),
    mem_add_wr        => (others => '0'),
    mem_add_rd        => (others => '0'),
    current_primitive => DT_PRIMITIVE_NULL
  );                
  signal reg, nextReg : reg_type := regDefault;

  -- Components inter-connection signals
  signal mem_wr       : std_logic;
  signal mem_data_out : std_logic_vector(DTPRIMITIVE_SIZE - 1 downto 0);

  -- Auxiliary signals
  signal ffOut_wr           : std_logic;
  signal sel_current_to_out : std_logic;
  signal similar_primitives : std_logic;
  signal mux_primitive_out  : DTPrimitive_t;

  -- Aliases
  signal current_bx      : std_logic_vector(11 downto 0);
  signal bx_expire_value : std_logic_vector(11 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  primQualFilterOut.primitive  <= mux_primitive_out;
  primQualFilterOut.fifoIn_rd  <= reg.ffIn_rd;
  primQualFilterOut.fifoOut_wr <= ffOut_wr;

  ---------------------------------------------------------------
  -- Auxiliary signals
  --
  mux_primitive_out <= reg.current_primitive when sel_current_to_out = '1' else
                       dtPrimitive_from_slv(mem_data_out);
  --
  -- This logic compares the last (and best) value detected from a series
  -- with the last primitive read from the FIFO, and gives a single bit
  -- ('similar_primitives') that indicates if their are consider coincident 
  -- or not.
  --
  -- In the mean time, the criteria is that they share, at least, one or 
  -- more TDC values in the same wire/layer combination in any of the two 
  -- internal segments.
  --
  similar_primitives <= primitives_are_similar(primQualFilterIn.primitive, 
                                               reg.current_primitive);
                       
  -- Aliases
  bx_expire_value <= reg.bx_counter + expire_bx_num;
  current_bx      <= primQualFilterIn.bx_counter;
                     
  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, primQualFilterIn, bx_expire_value, current_bx, 
                 similar_primitives)
    variable currentprim_high_q : std_logic;
    variable currentprim_low_q  : std_logic;
    variable inputprim_high_q   : std_logic;
    --
    variable currentprim_global_q : unsigned(2 downto 0);
    variable inputprim_global_q   : unsigned(2 downto 0);
  begin
    -- Default values
    nextReg            <= reg;
    mem_wr             <= '0';
    ffOut_wr           <= '0';
    sel_current_to_out <= '0';

    ------------------------------------------

    currentprim_global_q := primitive_global_q(reg.current_primitive);
    inputprim_global_q   := primitive_global_q(primQualFilterIn.primitive);

    if currentprim_global_q >= PRIM_3x3
    then currentprim_high_q := '1';
    else currentprim_high_q := '0';
    end if;

    if inputprim_global_q >= PRIM_3x3
    then inputprim_high_q := '1';
    else inputprim_high_q := '0';
    end if;

    if currentprim_global_q = PRIM_3x0 or 
       currentprim_global_q = PRIM_4x0
    then currentprim_low_q := '1';
    else currentprim_low_q := '0';
    end if;

    if reg.filter_is_empty = '0' and ( 
      (bx_expire_value <= MAX_BX_IDX and 
          current_bx >= bx_expire_value) 
                                               or
         (bx_expire_value > MAX_BX_IDX and 
          current_bx >= (bx_expire_value - MAX_BX_IDX + 1))
       )
    then
      nextReg.clear_filter <= '1';
    end if; 
    
    -- FSM
    case reg.state is 
      when STDBY =>
        -- Always to "0" until transfering stored data.
        nextReg.mem_add_rd <= (others => '0');

        if primQualFilterIn.fifoIn_empty = '0' then
          nextReg.bx_counter <= current_bx;

          if reg.filter_is_empty = '1' then
            nextReg.ffIn_rd           <= '1';          
            nextReg.mem_add_wr        <= (others => '0');
            nextReg.filter_is_empty   <= '0';
            nextReg.current_primitive <= primQualFilterIn.primitive;
            nextReg.state             <= RD_FIFO_WS;

          else
            nextReg.clear_filter <= '0';

            if similar_primitives = '0' then
              if currentprim_high_q = '1' or reg.mem_add_wr = ADDRESS_ZERO then
                nextReg.state <= WRITE_CURRENT_PRIMITIVE;
              else
                nextReg.mem_add_wr <= reg.mem_add_wr - '1';
                nextReg.state      <= WRITE_STORED_PRIMITIVES;
              end if;

            elsif currentprim_high_q = '1' then
              nextReg.ffIn_rd <= '1';          -- Get next primitive
              nextReg.state   <= RD_FIFO_WS;

              if (currentprim_global_q < inputprim_global_q)
                  or 
                 (  (primQualFilterIn.primitive.chi_square_chamber < 
                     reg.current_primitive.chi_square_chamber) 
                        and 
                    (currentprim_global_q = inputprim_global_q)
                        and 
                     inputprim_high_q = '1'
                 )
              then
                -- Replace current primitive
                nextReg.current_primitive <= primQualFilterIn.primitive;
              end if;

            -- Store input primitive
            else
              nextReg.ffIn_rd           <= '1'; -- Always fetch a new one prim.         
              mem_wr                    <= '1';
              nextReg.mem_add_wr        <= reg.mem_add_wr + '1';
              nextReg.current_primitive <= primQualFilterIn.primitive;
              nextReg.state             <= RD_FIFO_WS;
            end if;
          end if;
        --
        -- This condition forces to send the last "current primitive" after a
        -- prefixed BX number and the last data also has been sent to 
        -- outgoing FIFO.
        -- This way I avoid keeping a single isolated primitive forever 
        -- inside the filter registers.
        -- After a couple of BX's this primitive won't be correlated with the
        -- next one incoming.
        --
        elsif reg.clear_filter = '1' then
          nextReg.filter_is_empty <= '1';
          nextReg.bx_counter      <= current_bx;

          if currentprim_low_q = '1' and reg.mem_add_wr /= ADDRESS_ZERO then
            nextReg.mem_add_wr <= reg.mem_add_wr - '1';
            nextReg.state      <= WRITE_STORED_PRIMITIVES;
          else
            nextReg.state      <= WRITE_CURRENT_PRIMITIVE;
          end if;
        end if;

      when WRITE_STORED_PRIMITIVES =>
        if primQualFilterIn.fifoOut_full = '0' then
          ffOut_wr <= '1';

          if reg.mem_add_rd < reg.mem_add_wr then
            nextReg.mem_add_rd <= reg.mem_add_rd + '1';
          else
            nextReg.state <= WRITE_CURRENT_PRIMITIVE;
          end if;
        end if;

      when WRITE_CURRENT_PRIMITIVE =>
        if primQualFilterIn.fifoOut_full = '0' then
          ffOut_wr           <= '1';
          sel_current_to_out <= '1';
          --
          -- If we arrive here because "clear filter" mechanism has been 
          -- triggered, it's only possible if input FIFO was empty. So,
          -- in this case, we **DO NOT** try to read the FIFO. It's necessary
          -- to wait until it signals, again, non-empty condition.
          --
          if reg.clear_filter = '0' then
            nextReg.ffIn_rd <= '1';   -- Read "next" fifo data.
          end if;

          nextReg.mem_add_wr        <= (others => '0');
          nextReg.bx_counter        <= current_bx;
          nextReg.clear_filter      <= '0';    -- Always reset "clear_filter".
          nextReg.current_primitive <= primQualFilterIn.primitive;
          nextReg.state             <= RD_FIFO_WS;
        end if;

      when RD_FIFO_WS =>
        nextReg.ffIn_rd <= '0';
        nextReg.state   <= STDBY;
      
    end case;

  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1'
      then reg <= regDefault;
      else reg <= nextReg;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  FILTER_MEM : XIL_DISTRAM_DUALPORT_D32_WXBITS
  generic map (NUM_BITS => DTPRIMITIVE_SIZE)
  port map (
    clk      => clk,
    wr_ena   => mem_wr,
    add_wr   => reg.mem_add_wr,
    add_rd   => reg.mem_add_rd,
    data_in  => slv_from_dtPrimitive(reg.current_primitive),
    data_out => mem_data_out
  );
  
end architecture;
