numlinks = 12


from pprint import pprint
stats = '''
  stat(16#04#)(31 downto 0)            <= stat_link1_last_elink_word;
  stat(16#05#)(7 downto 0)             <= stat_link1_last_elink_byte;
'''

stats = [stat.strip() for stat in stats.split('\n') if stat.strip()]
stats = [stat.split(' ')[-1][:-1] for stat in stats]
stats = [stat.split('_',2)[2] for stat in stats]

maxlen = max([len(stat) for stat in stats])

txt = ''

for stat in stats:
    txt +=  '( '
    for link in range(1, numlinks + 1):
        txt += 'stat_link%i_'%link
        txt += stat
        txt += ' '*( 1 + (1 if link<10 and numlinks>9 else 0) + (maxlen - len(stat)) )
        txt += ', '
    txt = txt[:-2]
    txt += ') <= ' + stat + ';\n'

print txt

