----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_accumulation_layer_async - rtl
-- Description: 
--          Performs cluster/stub/muon accumulation
--          Generates error handlingblock
--          And histogramming wrapper
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- 
use work.bril_histogram_package.all;

entity bril_histogram_accumulation_layer_async is
Generic (
    -- identification of the histogram
    HISTOGRAM_TYPE                  : natural;
    HISTOGRAM_ID                    : natural;
    -- histogram parameters
    NUMBER_OF_BINS                  : natural;
    COUNTER_WIDTH                   : natural;
    INCREMENT_WIDTH                 : natural;
    -- orbit and bx counter width
    ORBIT_BX_COUNTER_WIDTH          : natural;
    -- accumulation parameters
    NUMBER_OF_UNITS                          : natural; -- number of units (modules, chips) connected to the histogrammer
    UNIT_INCREMENT_WIDTH                     : natural; -- width of the data bus per ech unit
    NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE   : natural; -- defines the complexity of the accumualtion algorithm
    UNIT_ERROR_COUNTER_WIDTH                 : natural; -- width of the error counter
    STORE_ERRORS                             : boolean
);
Port(
    -- core signals
    reset_i             : in std_logic; -- sync reset
    clk_i               : in std_logic; -- counting clock, typ. 40mhz
    -- tcds data
    lhc_fill_i          : in std_logic_vector(31 downto 0);
    cms_run_i           : in std_logic_vector(31 downto 0);
    lumi_section_i      : in std_logic_vector(31 downto 0);
    lumi_nibble_i       : in std_logic_vector(31 downto 0); 
    -- async control input
    valid_i             : in std_logic;
    iteration_id_i      : in std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    bin_id_i            : in std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    -- data input interface (used both in sync and async mode)
    increment_i         : in std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0);
    error_i             : in std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    mask_i              : in std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    -- fifo interface (optional)
    fifo_rd_clk_i       : in std_logic := '0';
    fifo_rd_en_i        : in std_logic := '0';
    fifo_empty_o        : out std_logic;
    fifo_words_cnt_o    : out std_logic_vector(31 downto 0);
    fifo_data_o         : out std_logic_vector(31 downto 0); -- output of the memory
    fifo_data_valid_o   : out std_logic; -- write enable
    --
    init_done_o         : out std_logic
);
end bril_histogram_accumulation_layer_async;

architecture rtl of bril_histogram_accumulation_layer_async is

    -- accumulation delay
    constant ACCUMULATION_DELAY         : natural := logNceil(NUMBER_OF_UNITS, NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE) + 1; -- +1 stands for the delay of the increment checker
    
    -- 
    signal init_done                    : std_logic;
    signal fifo_not_empty               : std_logic;
    
    -- internally buffered input signals
    signal reset_internal               : std_logic; 
    signal new_histogram_internal       : std_logic; 
    signal valid_internal               : std_logic;
    signal iteration_id_internal        : std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    signal bin_id_internal              : std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    signal increment_internal           : std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0);
    signal error_internal               : std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    signal mask_internal                : std_logic_vector(NUMBER_OF_UNITS-1 downto 0);

    -- output
    signal increment_from_accumualtor   : std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0);
    signal checked_increment            : std_logic_vector(INCREMENT_WIDTH-1 downto 0);
    signal checked_increment_overflow   : std_logic;
    
    -- delaying new histogram to properly reset increment overflow
    -- valid is used to output only valid increments and data
    signal new_histogram_delayed        : std_logic;
    signal valid_delayed                : std_logic;
    
    -- error block parameters
    constant ERROR_BLOCK_PARAMETERS         : error_block_parameters_type := get_error_block_parameters(STORE_ERRORS, ORBIT_BX_COUNTER_WIDTH, NUMBER_OF_UNITS,UNIT_ERROR_COUNTER_WIDTH);

    -- and signals
    signal err_bl_processed_mask            : std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    signal err_bl_n_msk_err_words           : std_logic_vector(15 downto 0);
    signal err_bl_err_word                  : std_logic_vector(31 downto 0);
    signal err_bl_rd_en                     : std_logic;
    
    
begin

    -- buffering (generates new histogram in async mode)
    bril_histogram_prebuffer_async_inst: entity work.bril_histogram_prebuffer_async
    generic map (
        -- orbit and bx counter width
        ORBIT_BX_COUNTER_WIDTH          => ORBIT_BX_COUNTER_WIDTH,
        -- accumulation parameters
        NUMBER_OF_UNITS                 => NUMBER_OF_UNITS,
        UNIT_INCREMENT_WIDTH            => UNIT_INCREMENT_WIDTH,
        -- this instance is accumulation layer -- we need to buffer errors and mask
        IN_ACCUMULATION_LAYER           => true
    ) 
    port map(
        -- core signals
        reset_i                 => reset_i,
        clk_i                   => clk_i,
        -- memory ready
        init_done_i             => init_done,
        fifo_not_empty_i        => fifo_not_empty,
        -- control signalsl     
        valid_i                 => valid_i,
        iteration_id_i          => iteration_id_i,
        bin_id_i                => bin_id_i,
        -- data input interface 
        increment_i             => increment_i,
        error_i                 => error_i,
        mask_i                  => mask_i,
        
        -- buffered output signals
        reset_o                 => reset_internal, 
        new_histogram_o         => new_histogram_internal, 
        valid_o                 => valid_internal, 
        iteration_id_o          => iteration_id_internal, 
        bin_id_o                => bin_id_internal, 
        increment_o             => increment_internal, 
        error_o                 => error_internal, 
        mask_o                  => mask_internal
    );

    -- error block
    bril_histogram_error_block_inst: entity work.bril_histogram_error_block
    generic map (
        -- 
        ORBIT_BX_COUNTER_WIDTH      => ORBIT_BX_COUNTER_WIDTH,
        NUMBER_OF_UNITS             => NUMBER_OF_UNITS,
        UNIT_ERROR_COUNTER_WIDTH    => UNIT_ERROR_COUNTER_WIDTH,
        STORE_ERRORS                => STORE_ERRORS
    )
    port map ( 
        -- basic
        memory_reset_i              => reset_internal, -- in async mode data will not come before
        reset_i                     => reset_internal,
        clk_i                       => clk_i,
        -- tcds of course
        new_histogram_i             => new_histogram_internal,
        valid_i                     => valid_internal,
        iteration_id_i              => iteration_id_internal,
        bin_id_i                    => bin_id_internal,
        -- input from the outside
        unit_err_i                  => error_internal,
        ext_mask_i                  => mask_internal,
        -- output autoamatic mask
        auto_mask_o                 => err_bl_processed_mask,
        -- interface with the streamer
        n_msk_err_words_o           => err_bl_n_msk_err_words,                   
        err_word_o                  => err_bl_err_word,     
        rd_en_i                     => err_bl_rd_en
    );

    -- generate accimulator
    accumulator_inst: entity work.bril_histogram_recursive_accumulator
    generic map (
        NUMBER_OF_UNITS                     => NUMBER_OF_UNITS,
        UNIT_INCREMENT_WIDTH                => UNIT_INCREMENT_WIDTH,
        NUMBER_OF_DAUGHTERS_PER_STAGE       => NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE,
        -- important for the timing (dont touch)
        TOTAL_NUMBER_OF_LAYERS              => logNceil(NUMBER_OF_UNITS, NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE)
    )
    port map (
        --
        clk_i                       => clk_i,
        --
        increment_i                 => increment_internal,
        mask_i                      => err_bl_processed_mask,
        -- we have to fill only part of the vector because MAX_NUMBER_OF_UNITS_PER_DAUGHTER can be 5, wheres this particular daighter may have only 4 units
        total_o                     => increment_from_accumualtor
    );
    
    -- delaying new histogram  and valid to properly reset increment overflow
    shift_new_histogram: entity work.bril_histogram_shift_register
    generic map(
        REGISTER_WIDTH  => 1,
        DELAY           => ACCUMULATION_DELAY -- ome has to delay one clock cycle more because new histogram is earlier
    )
    port map (
        clk_i           => clk_i,
        input_i(0)      => new_histogram_internal,
        output_o(0)     => new_histogram_delayed
    );
    
    shift_valid: entity work.bril_histogram_shift_register
    generic map(
        REGISTER_WIDTH  => 1,
        DELAY           => ACCUMULATION_DELAY-1
    )
    port map (
        clk_i           => clk_i,
        input_i(0)      => valid_internal,
        output_o(0)     => valid_delayed
    );
    
    -- outputing the increment overflow and the checked increment
    process(clk_i) 
    begin
        if rising_edge(clk_i) then
            if reset_internal = '1' then
                checked_increment <= (others => '0');
                checked_increment_overflow <= '0';
            else
                -- new histogram 
                if new_histogram_internal = '1' then
                    checked_increment_overflow <= '0';
                end if;
                -- reset the overflow for a new histo (and also store the one for the previous histogram for readout)
                if valid_delayed = '1' then
                    -- in case more than 1
                    if increment_from_accumualtor > 2**INCREMENT_WIDTH-1 then
                        checked_increment <= (others => '1');
                        checked_increment_overflow <= '1';
                    else
                        checked_increment <= std_logic_vector(resize(unsigned(increment_from_accumualtor), INCREMENT_WIDTH));
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- histgoram itself (async mode)
    bril_histogram_wrapper_async_inst: entity work.bril_histogram_wrapper_async
    generic map (
        -- we generated one in accumualtion layer
        USE_EXTERNAL_NEW_HISTOGRAM      => true,
        -- identification of the histogram
        HISTOGRAM_TYPE                  => HISTOGRAM_TYPE,
        HISTOGRAM_ID                    => HISTOGRAM_ID,
        -- histogram parameters
        NUMBER_OF_BINS                  => NUMBER_OF_BINS,
        COUNTER_WIDTH                   => COUNTER_WIDTH,
        INCREMENT_WIDTH                 => INCREMENT_WIDTH,
        -- delay of the increment with respect to the iteration/bin id signals
        INCREMENT_DELAY                 => ACCUMULATION_DELAY,
        -- counter width
        ORBIT_BX_COUNTER_WIDTH          => ORBIT_BX_COUNTER_WIDTH,
        -- error parameters
        NUMBER_OF_UNITS                 => NUMBER_OF_UNITS,
        NUMBER_OF_WORDS_PER_ERROR       => ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD,
        MAX_NUMBER_OF_MASKERROR_WORDS   => ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD * ERROR_BLOCK_PARAMETERS.MAX_MASKERROR_WORDS
    )
    port map(
        -- core signals
        reset_i                         => reset_internal,
        clk_i                           => clk_i,
        -- tcds signals
        new_histogram_i                 => new_histogram_internal,
        -- control signals
        valid_i                         => valid_internal,
        iteration_id_i	                => iteration_id_internal,
        bin_id_i		                => bin_id_internal,
        -- counter signals  
        increment_overflow_i            => checked_increment_overflow,
        increment_i                     => checked_increment,
        -- tcds data
        lhc_fill_i                      => lhc_fill_i,
        cms_run_i                       => cms_run_i,
        lumi_section_i                  => lumi_section_i,
        lumi_nibble_i                   => lumi_nibble_i,
        -- erorr block
        err_bl_n_msk_err_words_i        => err_bl_n_msk_err_words,  
        err_bl_err_word_i               => err_bl_err_word,         
        err_bl_rd_en_o                  => err_bl_rd_en,           
        -- fifo interface (optional)
        fifo_rd_clk_i                   => fifo_rd_clk_i,   
        fifo_rd_en_i                    => fifo_rd_en_i,    
        fifo_empty_o                    => fifo_empty_o,    
        fifo_words_cnt_o                => fifo_words_cnt_o,
        fifo_data_o                     => fifo_data_o,
        fifo_data_valid_o               => fifo_data_valid_o,
        fifo_not_empty_o                => fifo_not_empty,
        
        -- ready to get data
        init_done_o                     => init_done
    );
    --
    init_done_o <= init_done;

end rtl;
