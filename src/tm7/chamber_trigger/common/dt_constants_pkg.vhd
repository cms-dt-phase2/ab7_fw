-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Common components
---- File       : dt_constants_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-03
-------------------------------------------------------------------------------
---- Description: Package for common constants
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package dt_constants_pkg is
  
  ---------------------------------------------------------------
  -- Constants for LHC parameters
  ---------------------------------------------------------------
  -- In nanoseconds
  constant LHC_CLK_FREQ : integer := 25;
  -- Adimensional 
  constant MAX_BX_IDX   : integer := 3563;

  ---------------------------------------------------------------
  -- Chamber geometry constants
  ---------------------------------------------------------------
  -- En nanosegundos (tiempo de deriva en la celda)
  -- In 'ns', C++ original value => float MAX_DRIFT_TIME = ((float)(386.74));
  -- 
  constant MAX_DRIFT_TIME_NS : signed(9 downto 0) := to_signed(387, 10);
  
  -- Here it's added 2 decimal fixed-point bits, rounding the value to get
  -- as higher precision as possible ~ 386.75 ns
  constant MAX_DRIFT_TIME_P : signed(11 downto 0) := to_signed(386, 10) & "11";

  -- En milímetros (dimensiones de la celda)
  constant CELL_HEIGHT : signed(4 downto 0) := to_signed(13, 5);
  --
  -- This value is a 15 bit quantizer for 10 times the previous constant
  -- ( 2^15 / (10 * CELL_HEIGHT)) = 252.
  -- It will be used to get a division, replacing it by a product followed by
  -- a bits' shift.
  --
  constant CELL_HEIGHT10_INV_Q15 : signed(8 downto 0) := to_signed(252, 9);
  --
  -- Original value => float CELL_SEMIHEIGHT = 6.5; (in mm)
  -- Fixed-point with 2 additional bits.
  --
  -- Careful, cell length and semilength upgraded to 4 bits fractional part
  constant CELL_LENGTH_P     : signed(10 downto 0) := to_signed(42, 7) & "0000";
  constant CELL_SEMILENGTH_P : signed( 9 downto 0) := to_signed(21, 6) & "0000";
  --
  constant NUM_SUPERLAYERS   : integer := 3;
  constant NUM_LAYERS        : integer := 4;
  constant NUM_CH_PER_LAYER  : integer := 96;
  -- Número total de canales en una superlayer
  constant NUM_CH_PER_SUPLAY : integer := (NUM_LAYERS * NUM_CH_PER_LAYER);
  
  ---------------------------------------------------------------
  -- Cinematic constants
  ---------------------------------------------------------------
  -- En milímetros / nanosegundo (velocidad de deriva)
  -- In mm/ns, C++ original value => 
  --     float DRIFT_SPEED = (CELL_SEMILENGTH/MAX_DRIFT_TIME); => 0.0543 mm/ns
  --
  -- Here should be rounded to a fixed-point value of 54.25 um/ns.
  -- But, in order to avoid division by factor "x1000" to convert "um" into
  -- "mm" in the drift distance calculation, it will be stored in "fake-micros".
  -- I.E. value in "mm/ns" is multiplied by x1024 (~ 1000) => 55.552 fake-um/ns
  -- and rounded to the nearest value with 4-bit precision => ~ 55.5625 fake-um/ns
  -- This is the value stored into this constant
  -- 
  constant DRIFT_SPEED_P_x1024 : signed(10 downto 0) := to_signed(889, 11);
  -- 
  -- This is the maximun value than internal time can take. This is because
  -- internal time is cyclical due to the limited size of the time counters and
  -- the limited value of the bunch crossing index.
  -- It should be, approximately, the LHC's clock frequency multiplied by the
  -- maximum BX index, plus an arbitrary ammount for taking into account the
  -- muon traveling time and muon's signal drift time.
  --  
  constant MAX_VALUE_OF_TIME : integer := (LHC_CLK_FREQ * MAX_BX_IDX + 5000);
  
  ---------------------------------------------------------------
  -- Generic design constants
  ---------------------------------------------------------------
  -- In the mean time, only two SL to cope with phi segment matching
  constant MAX_SL_UNITS_PER_DESIGN : integer := 2;

end package;
