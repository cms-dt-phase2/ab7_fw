-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Arithmetics
---- File       : division_b24b6.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-06
-------------------------------------------------------------------------------
---- Description: Fixed point, quantized, division 24 bits over 6 bits.
----              It takes 3 clock cycles because of input and output 
----              value registration and the internal stages.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
--
use work.arith_pkg.all;

entity DIVISION_B24B6 is
    generic(
    -- Only implemented for 15 and 20
      DENOM_INV_BITS: positive := 15
    );
  port(
    clk         : in  std_logic;
    rst         : in  std_logic;
    enable      : in  std_logic;
    numerator   : in  std_logic_vector(23 downto 0);
    denominator : in  std_logic_vector( 5 downto 0);
    div_by_zero : out std_logic;
    result      : out std_logic_vector(23 downto 0)
  );
end entity;

architecture RTL of DIVISION_B24B6 is

  constant PRODUCT_SIZE : integer := numerator'length + DENOM_INV_BITS;

  signal reg_numerator_in : std_logic_vector(23 downto 0);
  signal reg_numerator    : std_logic_vector(23 downto 0);
  signal reg_denominat_in : std_logic_vector( 5 downto 0);
  signal denominat_invert : std_logic_vector(DENOM_INV_BITS-1 downto 0);
  signal reg_denom_invert : std_logic_vector(DENOM_INV_BITS-1 downto 0);
  signal my_result        : std_logic_vector(PRODUCT_SIZE-1 downto 0);
  signal reg_result       : std_logic_vector(23 downto 0);
  signal denom_is_null    : std_logic;
  signal denom_is_one     : std_logic;
  signal reg_div_by_zero  : std_logic;

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  result      <= reg_result;
  div_by_zero <= reg_div_by_zero;

  ---------------------------------------------------------------
  -- Combinational logic
  --
  my_result <= reg_numerator * reg_denom_invert;

  ---------------------------------------------------------------
  -- Secuential process
  --
  process(clk)
  begin
    if clk'event and clk = '1' then
      if rst = '1' then
        reg_numerator_in <= (others => '0');        
        reg_numerator    <= (others => '0');        
        reg_denominat_in <= (others => '0');        
        reg_denom_invert <= (others => '0');
        denom_is_null    <= '1';

        reg_div_by_zero  <= '1';
        reg_result       <= (others => '0');

      elsif enable = '1' then
        reg_numerator_in <= numerator;
        reg_numerator    <= reg_numerator_in;

        reg_denominat_in <= denominator;
        reg_denom_invert <= denominat_invert;

        if reg_denominat_in = 0 
        then denom_is_null <= '1';
        else denom_is_null <= '0';
        end if;
      
        if reg_denominat_in = 1 
        then denom_is_one <= '1';
        else denom_is_one <= '0';
        end if;
        
        if denom_is_null = '1' then 
          reg_div_by_zero <= '1';
          reg_result      <= (others => '0');

        elsif denom_is_one = '1' then 
          reg_div_by_zero <= '0';
          reg_result      <= reg_numerator;

        else 
          reg_div_by_zero <= '0';
          -- Bit shift for quantized division.
          reg_result <=
            std_logic_vector(my_result(PRODUCT_SIZE-1 downto DENOM_INV_BITS));
        end if;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  gendivq15: if DENOM_INV_BITS = 15 generate
    DIV_LUT : DIVISOR_B6Q15_LUT 
      port map(value => reg_denominat_in, value_inverted => denominat_invert);
  end generate;
  
  gendivq20: if DENOM_INV_BITS = 20 generate
    DIV_LUT : DIVISOR_B6Q20_LUT 
      port map(value => reg_denominat_in, value_inverted => denominat_invert);
  end generate;
  


end architecture;
