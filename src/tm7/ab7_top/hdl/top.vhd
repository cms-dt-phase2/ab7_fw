-- Top-level design for TM7 board, AB7 flavour

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.VComponents.all;

--
use work.ipbus.all;
use work.ipbus_trans_decl.all;
use work.ipbus_decode_ab7_infra.all;
use work.mp7_ttc_decl.all;
use work.mp7_brd_decl.all;
use work.trigger_pkg.all;
use work.globalpath_pkg.all; -- for fifo intercept types
use work.dt_common_pkg.all; -- Cela's Trigger data types
use work.bo2.all;
use work.gbt_bank_package.all;
use work.obdt_decoder_decl.all;

--use work.TM7_data_types.all;

entity top is
  port(
    ------------------------------------------------------------------------
    -- Ethernet
    eth_clkp, eth_clkn: in std_logic;
    eth_txp, eth_txn: out std_logic;
    eth_rxp, eth_rxn: in std_logic;
    ------------------------------------------------------------------------
    -- LEDs
    led1_yellow,led1_green,led1_red,led1_orange: out std_logic;-- On mp7 leds 1,0,10,9 {led_q(4:0)}
    led2_yellow,led2_green,led2_red,led2_orange: out std_logic;-- On mp7 leds 1,0,n,n  {not(locked and onehz),locked) 
    ------------------------------------------------------------------------
    -- TTC
    clk40_in_p: in std_logic;
    clk40_in_n: in std_logic;
    clk40pll_p, clk40pll_n: in std_logic;
    ttc_in_p: in std_logic;
    ttc_in_n: in std_logic;
    ------------------------------------------------------------------------
    -- Misc
    reset_mp_rx_hr,reset_mp_tx_hr: out std_logic;
    refclkp: in std_logic_vector(N_REFCLK - 1 downto 0);
    refclkn: in std_logic_vector(N_REFCLK - 1 downto 0);
    clk_100 : in  STD_LOGIC;
    technical_trigger_P3_p : out  STD_LOGIC;
    technical_trigger_P3_n : out  STD_LOGIC;
--    SN_RXEN_HR : out  STD_LOGIC_VECTOR(5 downto 0);
--    SN_SD_HR   : in  STD_LOGIC_VECTOR(5 downto 0);
--    SN_SQEN_HR : out  STD_LOGIC_VECTOR(5 downto 0);
--    SN_ENSD_HR : out  STD_LOGIC_VECTOR(5 downto 0);
--    SEL0_MGT_CLK_18 : out  std_logic;
    GA        : in  STD_LOGIC_VECTOR(3 downto 0);
    dipswitch : in  STD_LOGIC_VECTOR(7 downto 0);
    AMC_P1_RX_P, AMC_P1_RX_N : in std_logic;
    AMC_P1_TX_N, AMC_P1_TX_P : out std_logic;        
    USR_2_HR : out STD_LOGIC;
    INTR_PLL1_HR : in STD_LOGIC;
    SEL1_MGT_CLK_18  : out  std_logic;
    ------------------------------------------------------------------------
    -- GBT ports
    mgt117_clkn, mgt117_clkp    : in  std_logic;
    mgt117_rxn,  mgt117_rxp     : in  std_logic_vector(0 to 3);
    mgt117_txn,  mgt117_txp     : out std_logic_vector(0 to 3);
    mgt118_clkn, mgt118_clkp    : in  std_logic;
    mgt118_rxn,  mgt118_rxp     : in  std_logic_vector(0 to 3);
    mgt118_txn,  mgt118_txp     : out std_logic_vector(0 to 3);
    mgt119_clkn, mgt119_clkp    : in  std_logic;
    mgt119_rxn,  mgt119_rxp     : in  std_logic_vector(0 to 3);
    mgt119_txn,  mgt119_txp     : out std_logic_vector(0 to 3);
    ------------------------------------------------------------------------
    -- SPI flash memory ports
    -- outSpiClk is output through STARTUPE2.USRCCLKO
    outSpiCsB        : out std_logic; -- Spi Chip Select. Connect to SPI Flash chip-select via FPGA FCS_B pin
    outSpiMosi       : out std_logic; -- Spi MOSI. Connect to SPI DQ0 pin via the FPGA D00_MOSI pin
    inSpiMiso        : in  std_logic; -- Spi MISO. Connect to SPI DQ1 pin via the FPGA D01_DIN pin
    outSpiWpB        : out std_logic; -- SPI flash write protect. Connect to SPI 'W_B/Vpp/DQ2'
    outSpiHoldB      : out std_logic  -- Connect to SPI 'HOLD_B/DQ3'. Fix to '1'.		
	);
end entity;

architecture rtl of top is

  signal clk_ipb, rst_ipb, clk40ish, clk40, clk40pll, rst40, eth_refclk, clk100: std_logic;
  signal clk40_rst, clk40_sel, clk40_lock, clk40_stop, nuke, soft_rst: std_logic;
  signal clk_p, rst_p: std_logic;
  signal clks_aux, rsts_aux: std_logic_vector(2 downto 0);

  signal ipb_out_root: ipb_wbus;
  signal ipb_in_root: ipb_rbus;
  signal ipb_to_slaves: ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves: ipb_rbus_array(N_SLAVES - 1 downto 0);

  -- AUX signals for loop-generated ipbus slaves
  type int_arr_t is array(natural range <>) of integer;
  constant N_SLV_FROM_HIT_SL : int_arr_t(1 to 3) := (N_SLV_HIT_PREPROCESSOR_1, N_SLV_HIT_PREPROCESSOR_2, N_SLV_HIT_PREPROCESSOR_3);

  signal board_id: std_logic_vector(31 downto 0);
  signal ttc_l1a, ttc_l1a_dist, dist_lock, oc_flag, ec_flag, payload_bc0, ttc_l1a_throttle, ttc_l1a_flag: std_logic;
  signal ttc_cmd, ttc_cmd_dist: ttc_cmd_t;
  signal bunch_ctr, bunch_ctr_mp7 : bctr_t;
  signal evt_ctr, orb_ctr, orb_ctr_mp7 : eoctr_t;
  signal tmt_sync         : tmt_sync_t;

  signal clkmon: std_logic_vector(2 downto 0);

  signal leds : std_logic_vector(11 downto 0);

  signal locked, d_locked, clk_stopped : std_logic;
  signal MAC_lsb : std_logic_vector(15 downto 0) := (others=>'0');

  -- GBT
  signal txFrameClk_lock    : std_logic;
  signal headerLock         : std_logic_vector(1 to 12);

  signal obdt_data, hit_data_hpp, hit_data_spied, hit_data_r, hit_data_t  : slv32_v_t(1 to 3);
  

  -- Trigger auxiliary signals
  signal fifoIn_full : std_logic_vector(1 downto 0);
  signal fifoIn_wr   : std_logic_vector(1 downto 0);
  signal rst_trg     : std_logic;
  signal tp_valid    : std_logic;
  signal tp          : std_logic_vector(DTPRIMITIVE_SIZE - 1 downto 0);
  signal tt          : std_logic;
  signal rawHit1, rawHit3 : DTRawHit_t;
  signal fifoInterceptIn  : SegCandFifoInterceptIn_arr_t;  
  signal fifoInterceptOut : SegCandFifoInterceptOut_arr_t;

begin
  BUFG_inst : BUFG
    port map ( I => clk_100, O => clk100 );

  -- Minipods enable
  reset_mp_rx_hr <= '1';
  reset_mp_tx_hr <= '1';

  -- Status LEDs
  ------------------------------
  led1_yellow <= leds(1);  -- not led_q(3)
  led1_green  <= leds(0);  -- not led_q(2)
  led1_red    <= leds(10); -- not led_q(1)
  led1_orange <= clk_stopped; --leds(9);  -- not led_q(0)
  led2_yellow <= leds(4);  -- not (locked and onehz)
  led2_green  <= leds(3);  -- locked
  led2_red    <= bo2sl( headerLock /= (headerLock'range => '0') );
  led2_orange <= txFrameClk_lock;

  -- Clocks and control IO
  infra: entity work.mp7_infra
  generic map(
      MAC_ADDR => X"080030f30000", -- mac addr: 08:00:30:F3:00:00
      IP_ADDR =>  X"c0A80169"      -- fixed ip: 192.168.1.105
  )
  port map(
    gt_clkp                 => eth_clkp,
    gt_clkn                 => eth_clkn,
    gt_txp                  => eth_txp,
    gt_txn                  => eth_txn,
    gt_rxp                  => eth_rxp,
    gt_rxn                  => eth_rxn,
    leds                    => leds,
    clk_ipb                 => clk_ipb,
    rst_ipb                 => rst_ipb,
    clk40ish                => clk40ish,
    refclk_out              => eth_refclk,
    nuke                    => nuke,
    soft_rst                => soft_rst,
    oc_flag                 => oc_flag,
    ec_flag                 => ec_flag,
    MAC_lsb                 => MAC_lsb,
    ipb_out_root            => ipb_out_root,
    ipb_in_root             => ipb_in_root
  );

  --MAC_lsb <= x"02" & '0' & dipswitch(2 downto 0) & GA; -- range from 02:00 to 02:7F
  --MAC_lsb <= x"02" & "11" & dipswitch(1 downto 0) & GA; -- uROS MAC
  MAC_lsb <= x"02" & "1111" & GA; -- range from 02:F0 to 02:FF <-- forces uROS slice crate regardless of microswitches
        
  -- ipbus address decode
	fabric: entity work.ipbus_fabric_sel
    generic map(
    	NSLV => N_SLAVES,
    	SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_out_root,
      ipb_out => ipb_in_root,
      sel => ipbus_sel_ab7_infra(ipb_out_root.ipb_addr),
      ipb_to_slaves => ipb_to_slaves,
      ipb_from_slaves => ipb_from_slaves
    );


  -- Control registers and board IO
  ctrl: entity work.mp7_ctrl
  port map(
    clk        => clk_ipb,
    rst        => rst_ipb,
    ipb_in     => ipb_to_slaves(N_SLV_CTRL),
    ipb_out    => ipb_from_slaves(N_SLV_CTRL),
    nuke       => nuke,
    soft_rst   => soft_rst,
    board_id   => board_id,
    clk40_rst  => clk40_rst,
    clk40_sel  => clk40_sel,
    clk40_lock => clk40_lock,
    clk40_stop => clk40_stop
  );

  -- TTC signal handling
  ttc: entity work.mp7_ttc
  port map(
    clk          => clk_ipb,
    rst          => rst_ipb,
    mmcm_rst     => clk40_rst,
    sel          => clk40_sel,
    lock         => clk40_lock,
    stop         => clk40_stop,
    ipb_in       => ipb_to_slaves(N_SLV_TTC),
    ipb_out      => ipb_from_slaves(N_SLV_TTC),
    clk40_in_p   => clk40_in_p,
    clk40_in_n   => clk40_in_n,
    clk40ish_in  => clk40ish,
    clk40        => clk40,
    rst40        => rst40,
    clk_p        => clk_p, -- 180
    rst_p        => rst_p,
    clks_aux     => clks_aux, --(0):80, (1):160, (2): 180
    rsts_aux     => rsts_aux, 
    ttc_in_p     => ttc_in_p,
    ttc_in_n     => ttc_in_n,
    ttc_cmd      => ttc_cmd,
    ttc_cmd_dist => ttc_cmd_dist,
    ttc_l1a      => ttc_l1a,
    ttc_l1a_flag => ttc_l1a_flag,
    ttc_l1a_dist => ttc_l1a_dist,
    l1a_throttle => ttc_l1a_throttle,
    dist_lock    => dist_lock,
    bunch_ctr    => bunch_ctr_mp7,
    evt_ctr      => evt_ctr,
    orb_ctr      => orb_ctr_mp7,
    oc_flag      => oc_flag,
    ec_flag      => ec_flag,
    tmt_sync     => tmt_sync,
    monclk       => clkmon
  );

  dist_lock <= '1';
  ttc_l1a_throttle <= '1';

  -------------
  -- DATA RX --
  -------------

  ibuf_clk40: IBUFGDS
  generic map ( IBUF_LOW_PWR   => FALSE, IOSTANDARD     => "LVDS_25" )
  port map( i => clk40pll_p, ib => clk40pll_n, o => clk40pll );

  gbt : entity work.ab7_gbt
  port map(   
    -- Clocks   --
    clk40           => clk40pll, -- Frame clock
    clk100          => clk100,
    clk160          => clks_aux(1),
    -- MGTs --
    mgt117_clkp    => mgt117_clkp,
    mgt117_clkn    => mgt117_clkn,
    mgt117_txp     => mgt117_txp,
    mgt117_txn     => mgt117_txn,
    mgt117_rxp     => mgt117_rxp,
    mgt117_rxn     => mgt117_rxn,
    
    mgt118_clkp    => mgt118_clkp,
    mgt118_clkn    => mgt118_clkn,
    mgt118_txp     => mgt118_txp,
    mgt118_txn     => mgt118_txn,
    mgt118_rxp     => mgt118_rxp,
    mgt118_rxn     => mgt118_rxn,
    
    mgt119_clkp    => mgt119_clkp,
    mgt119_clkn    => mgt119_clkn,
    mgt119_txp     => mgt119_txp,
    mgt119_txn     => mgt119_txn,
    mgt119_rxp     => mgt119_rxp,
    mgt119_rxn     => mgt119_rxn,
    -- ipbus --
    ipb_clk       => clk_ipb,
    ipb_rst       => rst_ipb,
    ipb_in        => ipb_to_slaves(N_SLV_GBT),
    ipb_out       => ipb_from_slaves(N_SLV_GBT),
    -- data ports --
    ttc_cmd           => ttc_cmd,
    bunch_ctr         => bunch_ctr_mp7,
    headerLock        => headerLock,
    obdt_data         => obdt_data,
    txFrameClk_lock   => txFrameClk_lock,
    mgtrefclk_rst     => USR_2_HR
  );


  -- Hit pre-processor
  hit_preprocessor_gen: for i in 3 downto 1 generate

    hit_preprocessor1: entity work.ab7_hitpreprocessor
    port map(
      clk160          => clks_aux(1),
      -- ipbus signals
      ipb_clk         => clk_ipb,
      ipb_rst         => rst_ipb,
      ipb_in          => ipb_to_slaves(N_SLV_FROM_HIT_SL(i)),
      ipb_out         => ipb_from_slaves(N_SLV_FROM_HIT_SL(i)),
      -- Input hit data
      obdt_data       => obdt_data(i),
      -- Output hit data 
      hit_data        => hit_data_hpp(i),
      hit_data_spied  => hit_data_spied(i),
      hit_data_r      => hit_data_r(i),
      hit_data_t      => hit_data_t(i),
      -- Other signals
      bunch_ctr       => bunch_ctr
    );
    
  end generate;

  -------------
  -- TRIGGER --
  -------------

  -- SL1 (inner phi) -> Idx = 0
  fifoIn_wr(0) <= hit_data_t(1)(31) and (not hit_data_t(1)(27)) and hit_data_t(1)(26);

  rawHit1 <= (
      tdc_time       => hit_data_t(1)(16 downto 0) ,
      channel_id     => hit_data_t(1)(25 downto 19),
      layer_id       => hit_data_t(1)(18 downto 17) ,
      super_layer_id => hit_data_t(1)(27 downto 26) -- Currently unused by the trigger module
  );

  -- SL3 (outer phi) -> Idx = 1
  fifoIn_wr(1) <= (hit_data_t(1)(31) and hit_data_t(1)(27) and hit_data_t(1)(26)) or  (hit_data_t(3)(31) and hit_data_t(3)(27) and hit_data_t(3)(26));

  rawHit3 <= rawHit1 when (hit_data_t(1)(27 downto 26) = "11") else (
      tdc_time       => hit_data_t(3)(16 downto 0) ,
      channel_id     => hit_data_t(3)(25 downto 19),
      layer_id       => hit_data_t(3)(18 downto 17) ,
      super_layer_id => hit_data_t(3)(27 downto 26) -- Currently unused by the trigger module
  );

  trigger : CHAMBER_TRIGGER port map (
    clk40MHz             => clk40,
    -- Trigger input FIFO is synchronized with 'clk_fifo_in'.
    clk_fifo_in          => clks_aux(1),
    -- Trigger output FIFO clock
    clk_fifo_out         => clk40,
    --
    -- 'clk_trg' is a clock for internal trigger components; the higher, the better.
    --
    clk_trg              => clks_aux(2),   -- 180 MHz
    clk_trg_high         => clks_aux(2),
    
    rst_40               => rst40,         -- Slowest RST
    rst_fifo_in          => rst40,
    rst_trg              => rst40,
    rst_trg_high         => rst40,
    -- Global inputs
    bunch_ctr            => bunch_ctr,
    orbit_ctr            => orb_ctr,
    -- IP-Bus
    clk_ipb              => clk_ipb,
    rst_ipb              => rst_ipb,
    ipb_in               => ipb_to_slaves(N_SLV_TRIGGER),
    ipb_out              => ipb_from_slaves(N_SLV_TRIGGER),
    -- Trigger IN/OUT
    fifoIn_wr            => fifoIn_wr,
    rawHitPhi            => (
          0 => rawHit1,
          1 => rawHit3
    ),
    -- FIFO Intercepts
    fifoInterceptIn      => fifoInterceptIn ,
    fifoInterceptOut     => fifoInterceptOut,
    --
    fifoIn_full          => fifoIn_full,
    dt_trig              => tt,
    readout_data_valid   => tp_valid,
    readout_data         => tp
  );


  P3_OBUFDS_INST : OBUFDS
  generic map (IOSTANDARD => "LVDS", SLEW => "SLOW")
  port map (
    O  => technical_trigger_P3_p,
    OB => technical_trigger_P3_n,
    I  => tt
  );



  -------------------
  -- READOUT & SPY --
  -------------------
  -- AB7 SPY
  spy: entity work.ab7_spy
  port map(
    clk40           => clk40,
    clk160          => clks_aux(1),
    -- ipbus signals
    ipb_clk         => clk_ipb,
    ipb_rst         => rst_ipb,
    ipb_in          => ipb_to_slaves(N_SLV_AB7_SPY),
    ipb_out         => ipb_from_slaves(N_SLV_AB7_SPY),
    -- Hit data
    hit_data_in     => hit_data_hpp,
    hit_data_out    => hit_data_spied,
    -- Primitives data
    tpg_clk         => clk40,
    tp              => tp,
    tp_valid        => tp_valid,
    -- Other fifo intercepts; note the in<-->out swap: outside of this module the naming corresponds to trigger module directions
    fifoInterceptIn => fifoInterceptOut,
    fifoInterceptOut=> fifoInterceptIn,
    -- Other signals
    orb_ctr         => orb_ctr,
    bunch_ctr       => bunch_ctr
  );

  -- Luminosity 
  
  lumi: entity work.ab7_lumi
  port map(
    -- ipbus signals
    ipb_clk         => clk_ipb,
    ipb_rst         => rst_ipb,
    ipb_in          => ipb_to_slaves(N_SLV_LUMI),
    ipb_out         => ipb_from_slaves(N_SLV_LUMI),
    -- ttc
    clk             => clk40,
    rst             => rst40,
    ttc_cmd         => ttc_cmd,
    tt              => tt
  );

  -- Readout
  readout: entity work.tm7_readout
  port map(
    -- ipbus signals
    ipb_clk          => clk_ipb,
    ipb_rst          => rst_ipb,
    ipb_in           => ipb_to_slaves(N_SLV_READOUT),
    ipb_out          => ipb_from_slaves(N_SLV_READOUT),
    -- ttc signals
    ttc_cmd          => ttc_cmd,
    l1a              => ttc_l1a,
    bunch_ctr        => bunch_ctr,
    bunch_ctr_mp7_in => bunch_ctr_mp7,
    bunch_ctr_incr   => bunch_ctr,
    orb_ctr          => orb_ctr,
    orb_ctr_mp7_in   => orb_ctr_mp7,
    orb_ctr_incr     => orb_ctr,
    evt_ctr          => evt_ctr,
    -- ttc clocks and resets
    ttc_clk          => clk40,
    ttc_rst          => rst40,
    clk80            => clks_aux(0), 
    clk160           => clks_aux(1),
    rst_p            => rst40,
    -- Hits input data
    hit_clk          => clks_aux(1),
    hit_data         => hit_data_r,
    -- Trigger primitives input data
    tpg_clk          => clk40,
    tp               => tp,
    tp_valid         => tp_valid,
    -- amc13 block signals
    amc13_refclk     => eth_refclk,
    AMC_P1_RX_P      => AMC_P1_RX_P,
    AMC_P1_RX_N      => AMC_P1_RX_N,
    AMC_P1_TX_N      => AMC_P1_TX_N,
    AMC_P1_TX_P      => AMC_P1_TX_P
  );

  SEL1_MGT_CLK_18  <= '0';

  ----------------
  -- TM7 COMMON --
  ----------------
  tm7_common : entity work.tm7_common
  port map(      
    -- ipbus --
    ipb_clk       => clk_ipb,
    ipb_rst       => rst_ipb,
    ipb_in        => ipb_to_slaves(N_SLV_COMMON),
    ipb_out       => ipb_from_slaves(N_SLV_COMMON),
    clk40MHz      => clk40,
    clk_locked    => leds(3)
  );

  --------------------------------------------------------------------------
  -- Spi Programmer controlled through IpBus
  tm7_ipbus_spiprogrammer: entity work.IpBus_SpiProgrammer
  generic map(
    FW_ID  => x"BEAF0001" -- TODO: tbv is there any other FW ID constant somewhere ?
  )
  port map(
    ------------------------------------------------------------------------
    -- IpBus
    clk     => clk_ipb,   -- clock: in the following assumes 125/4 MHz
    rst     => rst_ipb,
    ipb_in  => ipb_to_slaves(N_SLV_IPBUS_SPI_PROGRAMMER),
    ipb_out => ipb_from_slaves(N_SLV_IPBUS_SPI_PROGRAMMER),
    ------------------------------------------------------------------------
    -- SPI flash memory ports
    -- outSpiClk is output through STARTUPE2.USRCCLKO
    outSpiCsB   => outSpiCsB,
    outSpiMosi  => outSpiMosi,
    inSpiMiso   => inSpiMiso,
    outSpiWpB   => outSpiWpB,
    outSpiHoldB => outSpiHoldB
    ------------------------------------------------------------------------
    -- leds        => open  -- status Leds during programming phase (optional)
    ------------------------------------------------------------------------
  );

  


end architecture;
