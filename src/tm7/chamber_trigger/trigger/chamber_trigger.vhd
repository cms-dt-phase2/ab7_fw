-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Chamber trigger module
---- File       : chamber_trigger.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-12
-------------------------------------------------------------------------------
---- Description: Wraps trigger modules to present a common interface to
----              TM7-like infrastructure VHDL modules.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
library UNISIM;
use UNISIM.vcomponents.all;
--
use work.dt_common_pkg.all;
use work.float_point_pkg.all;
use work.globalpath_pkg.all;
use work.registers_pkg.all;
use work.trigger_pkg.all;
--
use work.ipbus.all;
use work.ipbus_reg_types.all;

entity CHAMBER_TRIGGER is
  port ( 
    clk40MHz             : in  std_logic;
    clk_fifo_in          : in  std_logic;
    clk_fifo_out         : in  std_logic;
    clk_trg              : in  std_logic;
    clk_trg_high         : in  std_logic;
    -- Resets synchronized with previous clocks.
    rst_40               : in  std_logic;
    rst_fifo_in          : in  std_logic;
    rst_trg              : in  std_logic;
    rst_trg_high         : in  std_logic;
    -- Global input parameters
    bunch_ctr            : in  std_logic_vector(11 downto 0);
    orbit_ctr            : in  std_logic_vector(31 downto 0);
    -- IP-Bus
    clk_ipb              : in  std_logic;
    rst_ipb              : in  std_logic;
    ipb_in               : in  ipb_wbus;
    ipb_out              : out ipb_rbus;
    -- Trigger IN/OUT information
    fifoIn_wr            : in  std_logic_vector(1 downto 0);
    rawHitPhi            : in  ChamberRawHit_arr_t;
    -- FIFO Intercepts
    fifoInterceptIn      : in  SegCandFifoInterceptIn_arr_t;
    fifoInterceptOut     : out SegCandFifoInterceptOut_arr_t;
    --
    fifoIn_full          : out std_logic_vector(1 downto 0);
    dt_trig              : out std_logic;
    readout_data_valid   : out std_logic;
    readout_data         : out std_logic_vector(DTPRIMITIVE_SIZE - 1 downto 0)
  );
end entity;

architecture TwoProcess of CHAMBER_TRIGGER is
  
  -- Interconnection module signals.
  signal go_on              : std_logic;
  signal primitiveFFOut_rd  : std_logic;
  signal chamGlobDatPathOut : ChGlobalDataPathOut_t;
  signal control_regs       : DTControlRegs_t;
  
  -- Auxiliary
  signal data_valid             : std_logic;
  signal bunch_ctr_fast_clk_dom : std_logic_vector(11 downto 0);

  --------------------------------------
  -- IPbus syncreg slave signals
  --------------------------------------
  constant N_STAT : integer := 64;
  constant N_CTRL : integer := 64;
  signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);
  signal stat: ipb_reg_v(N_STAT-1 downto 0);
  signal conf: ipb_reg_v(N_CTRL-1 downto 0);

  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals declaration
  signal conf_bx_time_convert_ena, conf_candidate_splitter_enable, conf_input_testmode_ena, conf_latch_configuration,
    conf_output_testmode_ena, conf_prim_chisqr_hardfilter, conf_slphi_match_bypass_tanphi_match,
    conf_slphi_match_tanphi_hardfilter, conf_ttgen_test_tt_enable, conf_ttgen_test_tt_manual,
    conf_ttgen_test_tt_periodic_enable, conf_ttgen_tpg_tt_enable : std_logic := '0';

  signal conf_ouput_trg_chamber_selector : std_logic_vector(1 downto 0) := (others => '0');
  signal conf_primqfilt_wait_expire_num_bx, conf_qfilt_wait_expire_num_bx_stg1, conf_qfilt_wait_expire_num_bx_stg2,
    conf_slphi_match_mem_bx_wait : std_logic_vector(5 downto 0) := (others => '0');
  signal conf_bx_time_tolerance : std_logic_vector(7 downto 0) := (others => '0');
  signal conf_slphi_match_bx_wait_read_offset : std_logic_vector(8 downto 0) := (others => '0');
  signal conf_sl_phi3_x_offset : std_logic_vector(9 downto 0) := (others => '0');
  signal conf_num_bx_test_wait, conf_ttgen_test_tt_bx : std_logic_vector(11 downto 0) := (others => '0');
  signal conf_slphi_match_tanphi_x4096_thr,
    conf_tanphi_x4096_threshold : std_logic_vector(13 downto 0) := (others => '0');
  signal conf_chisqr_threshold, conf_prim_chisqr_threshold : std_logic_vector(15 downto 0) := (others => '0');
  signal stat_debug_counters_chamb_OutgoingPrimitives_count_in, stat_debug_counters_chamb_OutgoingPrimitives_count_out,
    stat_debug_counters_chamb_PostDupPrimFilter_count_in, stat_debug_counters_chamb_PostDupPrimFilter_count_out,
    stat_debug_counters_phi1_IncomingRawHits_count_in, stat_debug_counters_phi1_IncomingRawHits_count_out,
    stat_debug_counters_phi1_MainParams_count_in, stat_debug_counters_phi1_MainParams_count_out,
    stat_debug_counters_phi1_OutgoingSegments_count_in, stat_debug_counters_phi1_OutgoingSegments_count_out,
    stat_debug_counters_phi1_RawSegmentCandidate_count_in, stat_debug_counters_phi1_RawSegmentCandidate_count_out,
    stat_debug_counters_phi1_SegmentAfterAnalysis_count_in, stat_debug_counters_phi1_SegmentAfterAnalysis_count_out,
    stat_debug_counters_phi1_SegmentCandidate_count_in, stat_debug_counters_phi1_SegmentCandidate_count_out,
    stat_debug_counters_phi1_SegmentPostDuplicate_count_in, stat_debug_counters_phi1_SegmentPostDuplicate_count_out,
    stat_debug_counters_phi3_IncomingRawHits_count_in, stat_debug_counters_phi3_IncomingRawHits_count_out,
    stat_debug_counters_phi3_MainParams_count_in, stat_debug_counters_phi3_MainParams_count_out,
    stat_debug_counters_phi3_OutgoingSegments_count_in, stat_debug_counters_phi3_OutgoingSegments_count_out,
    stat_debug_counters_phi3_RawSegmentCandidate_count_in, stat_debug_counters_phi3_RawSegmentCandidate_count_out,
    stat_debug_counters_phi3_SegmentAfterAnalysis_count_in, stat_debug_counters_phi3_SegmentAfterAnalysis_count_out,
    stat_debug_counters_phi3_SegmentCandidate_count_in, stat_debug_counters_phi3_SegmentCandidate_count_out,
    stat_debug_counters_phi3_SegmentPostDuplicate_count_in,
    stat_debug_counters_phi3_SegmentPostDuplicate_count_out : std_logic_vector(31 downto 0) := (others => '0');

  signal conf_ttgen_q_threshold_1, conf_ttgen_q_threshold_2 : unsigned(2 downto 0) := (others => '0');
  signal conf_ttgen_test_tt_orbit_prescale : unsigned(3 downto 0) := (others => '0');
  signal conf_ttgen_bx_delay, conf_ttgen_future_limit : unsigned(11 downto 0) := (others => '0');
  signal stat_ttgen_tt_ctr : unsigned(31 downto 0) := (others => '0');

  -- End of automatically-generated VHDL code for register "breakout" signals declaration
  --------------------------------------------------------------------------------


begin


  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals assignment

  conf_ttgen_bx_delay                  <= unsigned( conf(16#00#)(11 downto 0) );
  conf_ttgen_future_limit              <= unsigned( conf(16#00#)(23 downto 12) );
  conf_ttgen_q_threshold_1             <= unsigned( conf(16#00#)(26 downto 24) );
  conf_ttgen_q_threshold_2             <= unsigned( conf(16#00#)(29 downto 27) );
  conf_ttgen_tpg_tt_enable             <= conf(16#00#)(30);
  conf_ttgen_test_tt_bx                <= conf(16#01#)(11 downto 0);
  conf_ttgen_test_tt_orbit_prescale    <= unsigned( conf(16#01#)(15 downto 12) );
  conf_ttgen_test_tt_periodic_enable   <= conf(16#01#)(16);
  conf_ttgen_test_tt_manual            <= conf(16#01#)(17);
  conf_ttgen_test_tt_enable            <= conf(16#01#)(18);
  conf_bx_time_tolerance               <= conf(16#02#)(7 downto 0);
  conf_chisqr_threshold                <= conf(16#02#)(23 downto 8);
  conf_bx_time_convert_ena             <= conf(16#02#)(24);
  conf_latch_configuration             <= conf(16#02#)(25);
  conf_slphi_match_bypass_tanphi_match <= conf(16#02#)(26);
  conf_prim_chisqr_hardfilter          <= conf(16#02#)(27);
  conf_slphi_match_tanphi_hardfilter   <= conf(16#02#)(28);
  conf_candidate_splitter_enable       <= conf(16#02#)(29);
  conf_num_bx_test_wait                <= conf(16#03#)(11 downto 0);
  conf_input_testmode_ena              <= conf(16#03#)(12);
  conf_output_testmode_ena             <= conf(16#03#)(13);
  conf_tanphi_x4096_threshold          <= conf(16#03#)(29 downto 16);
  conf_ouput_trg_chamber_selector      <= conf(16#04#)(1 downto 0);
  conf_slphi_match_bx_wait_read_offset <= conf(16#04#)(10 downto 2);
  conf_slphi_match_tanphi_x4096_thr    <= conf(16#04#)(24 downto 11);
  conf_qfilt_wait_expire_num_bx_stg1   <= conf(16#05#)(5 downto 0);
  conf_qfilt_wait_expire_num_bx_stg2   <= conf(16#05#)(11 downto 6);
  conf_slphi_match_mem_bx_wait         <= conf(16#05#)(17 downto 12);
  conf_sl_phi3_x_offset                <= conf(16#05#)(27 downto 18);
  conf_primqfilt_wait_expire_num_bx    <= conf(16#06#)(5 downto 0);
  conf_prim_chisqr_threshold           <= conf(16#06#)(21 downto 6);
  stat(16#00#)(31 downto 0)            <= std_logic_vector( stat_ttgen_tt_ctr );
  stat(16#04#)(31 downto 0)            <= stat_debug_counters_phi1_IncomingRawHits_count_in;
  stat(16#05#)(31 downto 0)            <= stat_debug_counters_phi1_IncomingRawHits_count_out;
  stat(16#06#)(31 downto 0)            <= stat_debug_counters_phi1_RawSegmentCandidate_count_in;
  stat(16#07#)(31 downto 0)            <= stat_debug_counters_phi1_RawSegmentCandidate_count_out;
  stat(16#08#)(31 downto 0)            <= stat_debug_counters_phi1_SegmentCandidate_count_in;
  stat(16#09#)(31 downto 0)            <= stat_debug_counters_phi1_SegmentCandidate_count_out;
  stat(16#0A#)(31 downto 0)            <= stat_debug_counters_phi1_MainParams_count_in;
  stat(16#0B#)(31 downto 0)            <= stat_debug_counters_phi1_MainParams_count_out;
  stat(16#0C#)(31 downto 0)            <= stat_debug_counters_phi1_SegmentAfterAnalysis_count_in;
  stat(16#0D#)(31 downto 0)            <= stat_debug_counters_phi1_SegmentAfterAnalysis_count_out;
  stat(16#0E#)(31 downto 0)            <= stat_debug_counters_phi1_SegmentPostDuplicate_count_in;
  stat(16#0F#)(31 downto 0)            <= stat_debug_counters_phi1_SegmentPostDuplicate_count_out;
  stat(16#10#)(31 downto 0)            <= stat_debug_counters_phi1_OutgoingSegments_count_in;
  stat(16#11#)(31 downto 0)            <= stat_debug_counters_phi1_OutgoingSegments_count_out;
  stat(16#12#)(31 downto 0)            <= stat_debug_counters_phi3_IncomingRawHits_count_in;
  stat(16#13#)(31 downto 0)            <= stat_debug_counters_phi3_IncomingRawHits_count_out;
  stat(16#14#)(31 downto 0)            <= stat_debug_counters_phi3_RawSegmentCandidate_count_in;
  stat(16#15#)(31 downto 0)            <= stat_debug_counters_phi3_RawSegmentCandidate_count_out;
  stat(16#16#)(31 downto 0)            <= stat_debug_counters_phi3_SegmentCandidate_count_in;
  stat(16#17#)(31 downto 0)            <= stat_debug_counters_phi3_SegmentCandidate_count_out;
  stat(16#18#)(31 downto 0)            <= stat_debug_counters_phi3_MainParams_count_in;
  stat(16#19#)(31 downto 0)            <= stat_debug_counters_phi3_MainParams_count_out;
  stat(16#1A#)(31 downto 0)            <= stat_debug_counters_phi3_SegmentAfterAnalysis_count_in;
  stat(16#1B#)(31 downto 0)            <= stat_debug_counters_phi3_SegmentAfterAnalysis_count_out;
  stat(16#1C#)(31 downto 0)            <= stat_debug_counters_phi3_SegmentPostDuplicate_count_in;
  stat(16#1D#)(31 downto 0)            <= stat_debug_counters_phi3_SegmentPostDuplicate_count_out;
  stat(16#1E#)(31 downto 0)            <= stat_debug_counters_phi3_OutgoingSegments_count_in;
  stat(16#1F#)(31 downto 0)            <= stat_debug_counters_phi3_OutgoingSegments_count_out;
  stat(16#20#)(31 downto 0)            <= stat_debug_counters_chamb_PostDupPrimFilter_count_in;
  stat(16#21#)(31 downto 0)            <= stat_debug_counters_chamb_PostDupPrimFilter_count_out;
  stat(16#22#)(31 downto 0)            <= stat_debug_counters_chamb_OutgoingPrimitives_count_in;
  stat(16#23#)(31 downto 0)            <= stat_debug_counters_chamb_OutgoingPrimitives_count_out;

  -- End of automatically-generated VHDL code for register "breakout" signals assignment
  --------------------------------------------------------------------------------

  --------------------------------------
  -- IPbus register
  --------------------------------------
  ipbusreg: entity work.ipbus_ctrlreg_v
  generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
  port map(
    clk     => clk_ipb,
    reset     => rst_ipb,
    ipbus_in  => ipb_in,
    ipbus_out => ipb_out,
    --slv_clk => clk40MHz,
    d       => stat,
    q       => conf,
    stb     => ctrl_stb
  );

  ---------------------------------------------------------------
  -- Auxiliary signals
  --  
  stat_debug_counters_phi1_IncomingRawHits_count_in         <=  chamGlobDatPathOut.statistics_sl0.ffIncomingRawHitsStat.count_in;
  stat_debug_counters_phi1_IncomingRawHits_count_out        <=  chamGlobDatPathOut.statistics_sl0.ffIncomingRawHitsStat.count_out;
  stat_debug_counters_phi1_RawSegmentCandidate_count_in     <=  chamGlobDatPathOut.statistics_sl0.ffRawSegCandidateStat.count_in;
  stat_debug_counters_phi1_RawSegmentCandidate_count_out    <=  chamGlobDatPathOut.statistics_sl0.ffRawSegCandidateStat.count_out;
  stat_debug_counters_phi1_SegmentCandidate_count_in        <=  chamGlobDatPathOut.statistics_sl0.ffSegmentCandidateStat.count_in;
  stat_debug_counters_phi1_SegmentCandidate_count_out       <=  chamGlobDatPathOut.statistics_sl0.ffSegmentCandidateStat.count_out;
  stat_debug_counters_phi1_MainParams_count_in              <=  chamGlobDatPathOut.statistics_sl0.ffMainParamStat.count_in;
  stat_debug_counters_phi1_MainParams_count_out             <=  chamGlobDatPathOut.statistics_sl0.ffMainParamStat.count_out;
  stat_debug_counters_phi1_SegmentAfterAnalysis_count_in    <=  chamGlobDatPathOut.statistics_sl0.ffSegAfterAnalysisStat.count_in;
  stat_debug_counters_phi1_SegmentAfterAnalysis_count_out   <=  chamGlobDatPathOut.statistics_sl0.ffSegAfterAnalysisStat.count_out;
  stat_debug_counters_phi1_SegmentPostDuplicate_count_in    <=  chamGlobDatPathOut.statistics_sl0.ffSegPostDupStat.count_in;
  stat_debug_counters_phi1_SegmentPostDuplicate_count_out   <=  chamGlobDatPathOut.statistics_sl0.ffSegPostDupStat.count_out;
  stat_debug_counters_phi1_OutgoingSegments_count_in        <=  chamGlobDatPathOut.statistics_sl0.ffOutgoingSegmentStat.count_in;
  stat_debug_counters_phi1_OutgoingSegments_count_out       <=  chamGlobDatPathOut.statistics_sl0.ffOutgoingSegmentStat.count_out;
  stat_debug_counters_phi3_IncomingRawHits_count_in         <=  chamGlobDatPathOut.statistics_sl1.ffIncomingRawHitsStat.count_in;
  stat_debug_counters_phi3_IncomingRawHits_count_out        <=  chamGlobDatPathOut.statistics_sl1.ffIncomingRawHitsStat.count_out;
  stat_debug_counters_phi3_RawSegmentCandidate_count_in     <=  chamGlobDatPathOut.statistics_sl1.ffRawSegCandidateStat.count_in;
  stat_debug_counters_phi3_RawSegmentCandidate_count_out    <=  chamGlobDatPathOut.statistics_sl1.ffRawSegCandidateStat.count_out;
  stat_debug_counters_phi3_SegmentCandidate_count_in        <=  chamGlobDatPathOut.statistics_sl1.ffSegmentCandidateStat.count_in;
  stat_debug_counters_phi3_SegmentCandidate_count_out       <=  chamGlobDatPathOut.statistics_sl1.ffSegmentCandidateStat.count_out;
  stat_debug_counters_phi3_MainParams_count_in              <=  chamGlobDatPathOut.statistics_sl1.ffMainParamStat.count_in;
  stat_debug_counters_phi3_MainParams_count_out             <=  chamGlobDatPathOut.statistics_sl1.ffMainParamStat.count_out;
  stat_debug_counters_phi3_SegmentAfterAnalysis_count_in    <=  chamGlobDatPathOut.statistics_sl1.ffSegAfterAnalysisStat.count_in;
  stat_debug_counters_phi3_SegmentAfterAnalysis_count_out   <=  chamGlobDatPathOut.statistics_sl1.ffSegAfterAnalysisStat.count_out;
  stat_debug_counters_phi3_SegmentPostDuplicate_count_in    <=  chamGlobDatPathOut.statistics_sl1.ffSegPostDupStat.count_in;
  stat_debug_counters_phi3_SegmentPostDuplicate_count_out   <=  chamGlobDatPathOut.statistics_sl1.ffSegPostDupStat.count_out;
  stat_debug_counters_phi3_OutgoingSegments_count_in        <=  chamGlobDatPathOut.statistics_sl1.ffOutgoingSegmentStat.count_in;
  stat_debug_counters_phi3_OutgoingSegments_count_out       <=  chamGlobDatPathOut.statistics_sl1.ffOutgoingSegmentStat.count_out;
  
  stat_debug_counters_chamb_PostDupPrimFilter_count_in      <=  chamGlobDatPathOut.ffPostDupFiltPrimitiveStat.count_in;
  stat_debug_counters_chamb_PostDupPrimFilter_count_out     <=  chamGlobDatPathOut.ffPostDupFiltPrimitiveStat.count_out;
  stat_debug_counters_chamb_OutgoingPrimitives_count_in     <=  chamGlobDatPathOut.ffOutgoingPrimitiveStat.count_in;
  stat_debug_counters_chamb_OutgoingPrimitives_count_out    <=  chamGlobDatPathOut.ffOutgoingPrimitiveStat.count_out;

  ---------------------------------------------------------------
  -- Outgoing signals
  --  
  readout_data_valid <= data_valid;
  fifoIn_full        <= chamGlobDatPathOut.rawHitsFFPhi_full;
  readout_data       <= slv_from_dtPrimitive(chamGlobDatPathOut.primitive);
  fifoInterceptOut   <= chamGlobDatPathOut.fifoInterceptOut;
  ---------------------------------------------------------------
  -- Auxiliary signals
  --
  primitiveFFOut_rd <= data_valid;
  data_valid        <= go_on and (not chamGlobDatPathOut.primitiveFFOut_empty);

  ---------------------------------------------------------------
  -- Trigger components
  ---------------------------------------------------------------
  --
  TT_inst : TECHNICAL_TRIGGER port map( 
    -- ttc and clocks
    tpg_clk                 => clk_fifo_out,
    ttc_clk                 => clk40MHz,
    ttc_rst                 => rst_40,
    bunch_ctr               => bunch_ctr,
    bunch_ctr_tpg_clk       => bunch_ctr_fast_clk_dom,
    -- config
    bx_delay                => conf_ttgen_bx_delay,
    future_limit            => conf_ttgen_future_limit,
    q_threshold_1           => conf_ttgen_q_threshold_1,
    q_threshold_2           => conf_ttgen_q_threshold_2,
    tpg_tt_enable           => conf_ttgen_tpg_tt_enable,
    test_tt_bx              => conf_ttgen_test_tt_bx,
    test_tt_orbit_prescale  => conf_ttgen_test_tt_orbit_prescale,
    test_tt_periodic_enable => conf_ttgen_test_tt_periodic_enable,
    test_tt_manual          => conf_ttgen_test_tt_manual,
    test_tt_enable          => conf_ttgen_test_tt_enable,
    -- tp input
    tpg                     => chamGlobDatPathOut.primitive,
    tpg_valid               => data_valid,
    -- stats
    tt_ctr                  => stat_ttgen_tt_ctr,
    -- output
    dt_trig                 => dt_trig
  );
  
  --
  -- Synchronizer to cross clock domains for "bx_counter". It crosses from 
  -- a slow clock to a faster clock.
  --
  BXCNT_CROSS_CLK_SYNC : VECTOR_SHIFT_REGISTER
  generic map (VECTOR_WIDTH => 12, SHIFT_STEPS => 2)
  port map (
    clk        => clk_fifo_out,    -- Faster clock
    enable     => '1',        -- Always enabled
    vector_in  => bunch_ctr,
    vector_out => bunch_ctr_fast_clk_dom
  );

  control_regs <= (
      candidate_splitter_enable       => conf_candidate_splitter_enable,
      bx_time_convert_ena             => conf_bx_time_convert_ena,
      bx_time_tolerance               => conf_bx_time_tolerance,
      chisqr_threshold                => conf_chisqr_threshold,
      qfilt_wait_expire_num_bx_stg1   => conf_qfilt_wait_expire_num_bx_stg1,
      qfilt_wait_expire_num_bx_stg2   => conf_qfilt_wait_expire_num_bx_stg2, 
      tanphi_x4096_threshold          => conf_tanphi_x4096_threshold,
      slphi_match_bypass_tanphi_match => conf_slphi_match_bypass_tanphi_match,
      slphi_match_tanphi_hardfilter   => conf_slphi_match_tanphi_hardfilter,
      slphi_match_tanphi_x4096_thr    => conf_slphi_match_tanphi_x4096_thr,
      slphi_match_bx_wait_read_offset => 
            conf_slphi_match_bx_wait_read_offset,
      slphi_match_mem_bx_wait         => conf_slphi_match_mem_bx_wait,
      prim_chisqr_hardfilter          => conf_prim_chisqr_hardfilter,
      prim_chisqr_threshold           => conf_prim_chisqr_threshold,
      primqfilt_wait_expire_num_bx    => conf_primqfilt_wait_expire_num_bx,
      ouput_trg_chamber_selector      => conf_ouput_trg_chamber_selector,
      sl_phi3_x_offset                => signed(conf_sl_phi3_x_offset),
      radio_mult_sin_delta_cmsphi     => UFLOAT32_ZERO,
      radio_mult_cos_delta_cmsphi     => UFLOAT32_ZERO
  );
  
  -- Trigger CORE.
  CHAMBER_TRG : CHAMBER_DATAPATH port map (
    clk_fifo_rd          => clk_fifo_out,
    clk_fifo_wr          => clk_fifo_in,
    clk_mixer            => clk_trg_high,
    clk_analyzer         => clk_trg,-- Careful if clk_analyzer != clk_dup_filter 
    clk_dup_filter       => clk_trg,-- dual-clock fifo was replaced for glue!!!
    clk_qenhan_filter    => clk_trg,
    clk_sl_matcher       => clk_trg,
    --
    rst_fifo_rd          => rst_40,
    rst_fifo_wr          => rst_fifo_in,
    rst_mixer            => rst_trg_high,
    rst_analyzer         => rst_trg,
    rst_dup_filter       => rst_trg,
    rst_qenhan_filter    => rst_trg,
    rst_sl_matcher       => rst_trg,
    --
    chamGlobDatPathIn    => (
        rawHitPhi         => rawHitPhi,
        rawHitFFPhi_wr    => fifoIn_wr,
        primitiveFFOut_rd => primitiveFFOut_rd,
        reset_overflow    => '0',
        bx_counter        => bunch_ctr,
        control_regs      => control_regs,
        fifoInterceptIn   => fifoInterceptIn
    ),
    chamGlobDatPathOut => chamGlobDatPathOut
  );
  --
  -- After a "rst" it's necessary to wait a few clock cycles before
  -- activating FIFO reading mechanism.
  --
  DELAY_OUTPUT_INIT : DELAYED_ENABLE generic map (NUM_BITS => 5)
  port map (
    clk    => clk40MHz,
    rst    => rst_40,
    enable => '1',
    go_on  => go_on
  );

end architecture;
