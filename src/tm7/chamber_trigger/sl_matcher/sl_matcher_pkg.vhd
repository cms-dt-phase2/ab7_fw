-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Super layers phi segment matcher
---- File       : sl_matcher_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-03
-------------------------------------------------------------------------------
---- Description: Package for components/defined types for the superlayers 
----              phi segment matcher
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package sl_matcher_pkg is

  ---------------------------------------------------------------
  -- Constants
  ---------------------------------------------------------------
  constant BX_WINDOW_WIDTH : integer := 32;
  constant NUM_SEG_PER_BX  : integer := 16;
  --
  constant BX_PTR_LEN      : integer := 5;  -- MUST fit BX_WINDOW_WIDTH
  constant SEGMENT_PTR_LEN : integer := 4;  -- MUST fit NUM_SEG_PER_BX

  ---------------------------------------------------------------
  -- Data types
  ---------------------------------------------------------------
  -- Segment counters: 7 segments per bx buffer + 1 extra bit for full condition
  type SegCount_arr_t is array (BX_WINDOW_WIDTH - 1 downto 0) 
                      of std_logic_vector(SEGMENT_PTR_LEN downto 0);

  -- Three times the amount of elements in the previous defined type.
  subtype SegPointer_t is std_logic_vector(SEGMENT_PTR_LEN + 2 downto 0);
  type ExtendedSegCount_arr_t is array (BX_WINDOW_WIDTH - 1 downto 0) 
                              of SegPointer_t;
                      
  type SupLaySegmMemIn_t is record
    segment                 : DTSegment_t;
    fifo_empty              : std_logic;
    start_storing           : std_logic;
    slphi_match_mem_bx_wait : std_logic_vector(5 downto 0);    
    bx_counter              : std_logic_vector(11 downto 0);
    bx_window_low           : std_logic_vector(11 downto 0);
    bx_window_high          : std_logic_vector(11 downto 0);
    -- Reading signals
    mem_rd                  : std_logic;
    bx_buffer_ptr           : std_logic_vector(BX_PTR_LEN - 1 downto 0);
    seg_item_ptr            : SegPointer_t;
  end record;

  type SupLaySegmMemOut_t is record
    segment              : DTSegment_t;
    seg_valid            : std_logic;
    stopped              : std_logic;
    fifo_rd              : std_logic;
    at_least_1_highq     : std_logic;
    last_items_in_bx_buf : ExtendedSegCount_arr_t;
    buffers_empty        : std_logic_vector(BX_WINDOW_WIDTH - 1 downto 0);
    buffer_with_highq    : std_logic_vector(BX_WINDOW_WIDTH - 1 downto 0);
  end record;
  
  type SupLayPhiMatcherIn_t is record
    control_regs     : DTControlRegs_t;
    bx_counter       : std_logic_vector(11 downto 0);
    -- SL3 (upper phi) corresponds to index '1'. SL1 to index '0'
    fifo_segment_sl  : DTSegment_arr_t;
    fifo_empty_sl    : std_logic_vector(1 downto 0);
    primitiveFF_full : std_logic;
  end record;

  type SupLayPhiMatcherOut_t is record
    primitiveFF_wr  : std_logic;
    paired_segments : DTSegment_arr_t;
    valid_segments  : std_logic_vector(1 downto 0);
    segmentFF_rd    : std_logic_vector(1 downto 0);
  end record;

  type PrimitivePathCalculatorIn_t is record
    paired_segments       : DTSegment_arr_t;
    valid_segments        : std_logic_vector(1 downto 0);
    sl_phi3_x_offset      : signed(9 downto 0);
    primValidFFOut_full   : std_logic;
    pairSegmentFFIn_empty : std_logic;
  end record;

  type PrimitivePathCalculatorOut_t is record
    primitive          : DTPrimitive_t;
    primValidFFOut_wr  : std_logic;
    pairSegmentFFIn_rd : std_logic;
  end record;

  ---------------------------------------------------------------
  -- Functions
  ---------------------------------------------------------------
  function segments_with_good_bx(
      s1, s2         : DTSegment_t; 
      valid1, valid2 : std_logic
  ) return std_logic;
  
  function tangents_in_range(
      s1, s2                 : DTSegment_t; 
      valid1, valid2         : std_logic;
      tanphi_x4096_threshold : std_logic_vector(13 downto 0)
  ) return std_logic;

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component SUPLAY_SEGMENT_MEMORY is
    port (
      clk         : in  std_logic;
      rst         : in  std_logic;
      slSegMemIn  : in  SupLaySegmMemIn_t;
      slSegMemOut : out SupLaySegmMemOut_t
    );
  end component;  

  component SUPLAY_PHI_MATCHER is
    port (
      clk           : in  std_logic;
      rst           : in  std_logic;
      slphiMatchIn  : in  SupLayPhiMatcherIn_t;
      slphiMatchOut : out SupLayPhiMatcherOut_t
    );
  end component;  
  
  component PRIMITIVE_PATH_CALCULATOR is
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    primPathCalcIn  : in  PrimitivePathCalculatorIn_t;
    primPathCalcOut : out PrimitivePathCalculatorOut_t
  );
  end component;
  
end package;

-------------------------------------------------------------------------------
---- Body
-------------------------------------------------------------------------------
package body sl_matcher_pkg is

  function segments_with_good_bx(
      s1, s2         : DTSegment_t; 
      valid1, valid2 : std_logic
  ) 
  return std_logic
  is 
    variable good_bx     : std_logic;
    variable abs_bx_diff : signed(12 downto 0);
    variable bx_id1_long : signed(12 downto 0);
    variable bx_id2_long : signed(12 downto 0);
  begin
    bx_id1_long := signed('0' & s1.bx_id);
    bx_id2_long := signed('0' & s2.bx_id);
    abs_bx_diff := abs(bx_id1_long - bx_id2_long);

    if (valid1 = '0' and valid2 = '1') or
       (valid1 = '1' and valid2 = '0') or
          ( valid1 = '1' and 
            valid2 = '1' and
            abs_bx_diff <= to_signed(1, abs_bx_diff'length)
          )
    then good_bx := '1';
    else good_bx := '0';
    end if;

    return good_bx;
  end function;


  function tangents_in_range(
      s1, s2                 : DTSegment_t; 
      valid1, valid2         : std_logic;
      tanphi_x4096_threshold : std_logic_vector(13 downto 0)
  ) 
  return std_logic
  is
    variable in_range     : std_logic;
    variable abs_tan_diff : signed(14 downto 0);
  begin
    abs_tan_diff := abs(s1.phi_tangent_x4096 - s2.phi_tangent_x4096);
    --
    -- When only one is valid, I consider "tangent in range".
    -- Only when both are valid tangent comparison is performed.
    --
    if (valid1 = '0' and valid2 = '1') or
       (valid1 = '1' and valid2 = '0') or
       (
          (valid1 = '1' and valid2 = '1') and
          (std_logic_vector(abs_tan_diff(13 downto 0)) <= 
              tanphi_x4096_threshold              -- Positive sign is removed.
          )
       )
    then in_range := '1';
    else in_range := '0';
    end if;

    return in_range;
  end function;

end package body;
