library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

library work;
use work.dt_common_pkg.all;
use work.globalpath_pkg.all;
use work.top_decl.all; -- for LHC_BUNCH_COUNT
use work.BXArithmetic.all;
use work.minmax.all;
use work.bo2.all;

entity TECHNICAL_TRIGGER is
  port ( 
    -- ttc and clocks
    tpg_clk                 : in  std_logic;
    ttc_clk                 : in  std_logic;
    ttc_rst                 : in  std_logic;
    bunch_ctr               : in  std_logic_vector(11 downto 0);
    bunch_ctr_tpg_clk       : in  std_logic_vector(11 downto 0);
    -- config
    bx_delay                : in  unsigned(11 downto 0);
    future_limit            : in  unsigned(11 downto 0);
    q_threshold_1           : in  unsigned(2 downto 0);
    q_threshold_2           : in  unsigned(2 downto 0);
    tpg_tt_enable           : in  std_logic;
    test_tt_bx              : in  std_logic_vector(11 downto 0);
    test_tt_orbit_prescale  : in  unsigned(3 downto 0);
    test_tt_periodic_enable : in  std_logic;
    test_tt_manual          : in  std_logic;
    test_tt_enable          : in  std_logic;
    -- tp input
    tpg                      : in  dtprimitive_t;
    tpg_valid                : in  std_logic;
    -- stats
    tt_ctr                  : out unsigned(31 downto 0);
    -- output
    dt_trig                 : out std_logic
  );
end entity;

architecture Behavioral of TECHNICAL_TRIGGER is

  signal tpg_tt, test_tt : std_logic;
  signal pending_l1a_doa,  pending_l1a_dob, pending_l1a_dia, pending_l1a_dib,
    pending_l1a_wea, pending_l1a_web : std_logic_vector(0 downto 0);
  signal pending_l1a_rsta, pending_l1a_rstb : std_logic;
  signal pending_l1a_addra, pending_l1a_addrb : std_logic_vector(13 downto 0);
  
  signal stat_tt_ctr : unsigned(tt_ctr'range);

begin

dt_trig <= tpg_tt or test_tt;

------------
-- TPG TT --
------------


-- True Dual Port for storage of the pending L1As
-- Port A: read current l1a and erase pending L1A
-- Port B: schedule future L1As
pending_l1a_inst : BRAM_TDP_MACRO
generic map (
  BRAM_SIZE => "18Kb", -- Target BRAM, "18Kb" or "36Kb" 
  DEVICE => "7SERIES", -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
  DOA_REG => 1, -- Optional port A output register (0 or 1)
  DOB_REG => 1, -- Optional port B output register (0 or 1)
  INIT_FILE => "NONE",
  READ_WIDTH_A => 1,   -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
  READ_WIDTH_B => 1,   -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
  SIM_COLLISION_CHECK => "ALL", -- Collision check enable "ALL", "WARNING_ONLY", 
                                -- "GENERATE_X_ONLY" or "NONE" 
  WRITE_MODE_A => "READ_FIRST", -- "WRITE_FIRST", "READ_FIRST" or "NO_CHANGE" 
  WRITE_MODE_B => "READ_FIRST", -- "WRITE_FIRST", "READ_FIRST" or "NO_CHANGE" 
  WRITE_WIDTH_A => 1, -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
  WRITE_WIDTH_B => 1 -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
  )
port map (
  DOA => pending_l1a_doa,       -- Output port-A data, width defined by READ_WIDTH_A parameter
  DOB => pending_l1a_dob,       -- Output port-B data, width defined by READ_WIDTH_B parameter
  ADDRA => pending_l1a_addra,   -- Input port-A address, width defined by Port A depth
  ADDRB => pending_l1a_addrb,   -- Input port-B address, width defined by Port B depth
  CLKA => ttc_clk,     -- 1-bit input port-A clock
  CLKB => tpg_clk,     -- 1-bit input port-B clock
  DIA => pending_l1a_dia,       -- Input port-A data, width defined by WRITE_WIDTH_A parameter
  DIB => pending_l1a_dib,       -- Input port-B data, width defined by WRITE_WIDTH_B parameter
  ENA => '1',       -- 1-bit input port-A enable
  ENB => '1',       -- 1-bit input port-B enable
  REGCEA => '1', -- 1-bit input port-A output register enable
  REGCEB => '1', -- 1-bit input port-B output register enable
  RSTA => pending_l1a_rsta,     -- 1-bit input port-A reset
  RSTB => pending_l1a_rstb,     -- 1-bit input port-B reset
  WEA => pending_l1a_wea,       -- Input port-A write enable, width defined by Port A depth
  WEB => pending_l1a_web        -- Input port-B write enable, width defined by Port B depth
);

-- Port A
-- always read the data corresponding to the current bx and reset it to 0
pending_l1a_wea <= "1";
pending_l1a_dia <= "0";
pending_l1a_addra <= "00" & bunch_ctr;
pending_l1a_rsta <= ttc_rst;

-- Port B
-- Address and Write Enable will depend on input primitive, 
-- but in case they are set, the data to write is a 1.
pending_l1a_dib <= "1";

-- Read back L1As and erase memory
process(ttc_clk)
  variable rstctr : unsigned(11 downto 0);
begin
  if rising_edge(ttc_clk) then
    if ttc_rst = '1' then
      rstctr := (others => '1');
      tpg_tt <= '0';
      stat_tt_ctr <= (others => '0');
    elsif rstctr > 0 then
      rstctr := rstctr - 1;
    else
      tpg_tt <= pending_l1a_doa(0) and tpg_tt_enable;
      stat_tt_ctr <= stat_tt_ctr + bo2int( tpg_tt = '1' );
    end if;
  end if;
end process;

-- Schedule future L1As - tp_clk
process(tpg_clk)
  variable tmpbx : unsigned(11 downto 0);
  variable q1, q2 : unsigned(2 downto 0);
begin
  if rising_edge(tpg_clk) then
    if ttc_rst = '1' then
      pending_l1a_rstb <= '1';
      pending_l1a_web <= "0";
      pending_l1a_addrb <= (others => '0');
    else
      pending_l1a_rstb <= '0';
      pending_l1a_web <= "0";
    
      tmpbx := BXplus( unsigned( tpg.bx_id_chamber ), bx_delay );
      q1 := maximum( unsigned(tpg.paired_segments(0).main_params.quality),
                     unsigned(tpg.paired_segments(1).main_params.quality) );
      q2 := minimum( unsigned(tpg.paired_segments(0).main_params.quality),
                     unsigned(tpg.paired_segments(1).main_params.quality) );
      if  tpg_valid = '1' 
        -- only write if in interval [bx+5, bx+future_limit+5] 
        and BXminus( 
              BXminus( tmpbx, unsigned(bunch_ctr_tpg_clk) ),
              to_unsigned(5,12) 
            ) <= future_limit 
        -- only write if qualities pass the TT filter
        and q1 >= q_threshold_1 and q2 >= q_threshold_2 
      then
        pending_l1a_addrb <= "00" & std_logic_vector(tmpbx);
        pending_l1a_web <= "1";
      end if;
    end if;
  end if;
end process;

-------------
-- TEST TT --
-------------

process(ttc_clk)
  variable manual_armed : boolean ;
  variable tt : std_logic;
  variable orbit_prescale : unsigned(test_tt_orbit_prescale'range);
  variable sync_tt_manual : std_logic_vector(3 downto 0);
begin
  if rising_edge(ttc_clk) then
    tt := '0';
    
    if bunch_ctr = test_tt_bx then
      if orbit_prescale = (orbit_prescale'range => '0') then
        tt := test_tt_periodic_enable;
        orbit_prescale := test_tt_orbit_prescale;
      else
        orbit_prescale := orbit_prescale - 1;
      end if;
      
      if manual_armed then
        tt := '1';
        manual_armed := false;
      end if; 
    end if;
    
    -- cross clk domain: when the msb is different than the rest, arm the tt
    if sync_tt_manual(sync_tt_manual'high-1 downto 0) = 
      (sync_tt_manual'high-1 downto 0 =>not sync_tt_manual(sync_tt_manual'high))
    then
      manual_armed := true;
    end if;
    
    test_tt <= tt and test_tt_enable ;

    sync_tt_manual :=
      sync_tt_manual(sync_tt_manual'high-1 downto 0) & test_tt_manual;
  
  end if;
end process;

end architecture;
