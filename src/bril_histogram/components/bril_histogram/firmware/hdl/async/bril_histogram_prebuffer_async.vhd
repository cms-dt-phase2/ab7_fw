----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_prebuffer - rtl
-- Description: 
--          
--         Prebuffering of the input data - allows to generate new_histogram signal in asyncrhnosos mode
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bril_histogram_prebuffer_async is
Generic (
    -- orbit and bx counter width
    ORBIT_BX_COUNTER_WIDTH          : natural;
    -- accumulation parameters
    NUMBER_OF_UNITS                 : natural; -- number of units (modules, chips) connected to the histogrammer
    UNIT_INCREMENT_WIDTH            : natural; -- width of the data bus per ech unit
    -- when internal  no errors need to be delayed)
    IN_ACCUMULATION_LAYER           : boolean
);
Port(
    -- core signals
    reset_i             : in std_logic; -- sync reset
    clk_i               : in std_logic; -- counting clock, typ. 40mhz
    --
    init_done_i         : in std_logic;
    fifo_not_empty_i    : in std_logic;
    -- control
    valid_i             : in std_logic;
    iteration_id_i      : in std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    bin_id_i            : in std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    -- data input interface (used both in sync and async mode)
    increment_i         : in std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0);
    error_i             : in std_logic_vector(NUMBER_OF_UNITS-1 downto 0) := (others => '0');
    mask_i              : in std_logic_vector(NUMBER_OF_UNITS-1 downto 0) := (others => '0');
    
    -- buffered output signals
    reset_o             : out std_logic;                                                             
    new_histogram_o     : out std_logic;                                                             
    valid_o             : out std_logic;                                                             
    iteration_id_o      : out std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);                   
    bin_id_o            : out std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);                   
    increment_o         : out std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0);     
    error_o             : out std_logic_vector(NUMBER_OF_UNITS-1 downto 0);                          
    mask_o              : out std_logic_vector(NUMBER_OF_UNITS-1 downto 0)
);
end bril_histogram_prebuffer_async;

architecture rtl of bril_histogram_prebuffer_async is

    signal new_histogram_internal       : std_logic;
    signal new_histogram_accepted       : std_logic;
    signal new_histogram_skipped        : std_logic;
    signal skipped_histogram            : std_logic;
    signal fast_accept                  : std_logic;
    signal fast_skip                    : std_logic; -- immediate skip signal to avoid dummy bx0 data at all
    
begin

    --==========================================--
    -- internally generated new histogram signal
    --==========================================--
    -- generate new histogtam internal singal
    new_histogram_internal <= '1' when (init_done_i = '1' and valid_i = '1' and iteration_id_i = 0 and bin_id_i = 0) else '0';
    new_histogram_accepted <= '1' when (new_histogram_internal = '1' and fifo_not_empty_i = '0') else '0';
    new_histogram_skipped <= '1' when (new_histogram_internal = '1' and fifo_not_empty_i = '1') else '0';
    fast_skip <= (reset_i or new_histogram_skipped) when new_histogram_internal = '1' else skipped_histogram;
    fast_accept <= not fast_skip;
    
    -- generate skip signal for the whole histogram - all the coming valids until new histogram arrives
    process (clk_i) 
    begin
        if rising_edge(clk_i) then
            if reset_i = '1' then
                skipped_histogram <= '0';
                new_histogram_o <= '0';
            else
                -- new histogram arrived and accepted 
                if new_histogram_accepted = '1' then
                    -- accept new histogram
                    skipped_histogram <= '0';                    
                elsif new_histogram_skipped = '1' then
                    -- skip new histogram
                    skipped_histogram <= '1'; 
                end if;
                
                -- also buffer new histogram to give all these signals time for buffering
                new_histogram_o <= new_histogram_accepted;
            end if;
        end if;
    end process;        
    
    --==========================================--
    -- everything else has to be delayed by two clock cycles (new_histogram we need one clock cycle BEFORE orbit=0,bx=0)
    --==========================================--
    delay_reset: entity work.bril_histogram_shift_register
    generic map(
        REGISTER_WIDTH  => 1,
        DELAY           => 2
    )
    port map (
        clk_i           => clk_i,
        input_i(0)      => reset_i,
        output_o(0)     => reset_o
    );
    
    delay_valid: entity work.bril_histogram_shift_register
    generic map(
        REGISTER_WIDTH  => 1,
        DELAY           => 2
    )
    port map (
        clk_i           => clk_i,
        enable_i        => fast_accept,
        input_i(0)      => valid_i,
        output_o(0)     => valid_o
    );
    
    delay_iteration_id: entity work.bril_histogram_shift_register
    generic map(
        REGISTER_WIDTH  => ORBIT_BX_COUNTER_WIDTH,
        DELAY           => 2
    )
    port map (
        clk_i           => clk_i,
        enable_i        => fast_accept,
        input_i         => iteration_id_i,
        output_o        => iteration_id_o
    );
    
    delay_bin_id: entity work.bril_histogram_shift_register
    generic map(
        REGISTER_WIDTH  => ORBIT_BX_COUNTER_WIDTH,
        DELAY           => 2
    )
    port map (
        clk_i           => clk_i,
        enable_i        => fast_accept,
        input_i         => bin_id_i,
        output_o        => bin_id_o
    );
    
    delay_increment: entity work.bril_histogram_shift_register
    generic map(
        REGISTER_WIDTH  => NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH,
        DELAY           => 2
    )
    port map (
        clk_i           => clk_i,
        enable_i        => fast_accept,
        input_i         => increment_i,
        output_o        => increment_o
    );
    
    -- if accumulation layer
    gen_error_and_mask: if IN_ACCUMULATION_LAYER generate
    
        delay_error: entity work.bril_histogram_shift_register
        generic map(
            REGISTER_WIDTH  => NUMBER_OF_UNITS,
            DELAY           => 2
        )
        port map (
            clk_i           => clk_i,
            enable_i        => fast_accept,
            input_i         => error_i,
            output_o        => error_o
        );
    
        delay_mask: entity work.bril_histogram_shift_register
        generic map(
            REGISTER_WIDTH  => NUMBER_OF_UNITS,
            DELAY           => 2
        )
        port map (
            clk_i           => clk_i,
            enable_i        => fast_accept,
            input_i         => mask_i,
            output_o        => mask_o
        );        
      
    end generate gen_error_and_mask;

end rtl;
