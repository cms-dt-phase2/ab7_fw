----------------------------------------------------------------------------------
-- Takes hits received in the ab7 and does some pre-processing before delivering
-- them to the consumers (readout/trigger)
-- Alvaro Navarro, CIEMAT, 2018
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


library work;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_ab7_hitpreprocessor.all;
use work.mp7_ttc_decl.all;
use work.BXarithmetic.all;
use work.bo2.all;

entity ab7_hitpreprocessor is
  port(
    clk160          : in std_logic;
    -- ipbus signals
    ipb_clk         : in std_logic;
    ipb_rst         : in std_logic;
    ipb_in          : in ipb_wbus;
    ipb_out         : out ipb_rbus;
    -- Input hit data
    obdt_data       : in std_logic_vector(31 downto 0);
    -- Output hit data 
    hit_data        : out std_logic_vector(31 downto 0); -- valid & '0' & St & SL & Channel & BX & TDC
    hit_data_spied  : in  std_logic_vector(31 downto 0); -- valid & '0' & St & SL & Channel & BX & TDC
    hit_data_r      : out std_logic_vector(31 downto 0); -- valid & '0' & St & SL & Channel & BX & TDC
    hit_data_t      : out std_logic_vector(31 downto 0); -- valid & '0' & St & SL & Channel & BX & TDC
    -- Other signals
    bunch_ctr       : in bctr_t
  );
end entity;

architecture Behavioral of ab7_hitpreprocessor is

  signal ipb_to_slaves: ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves: ipb_rbus_array(N_SLAVES - 1 downto 0);

  -- Ipbus LUT
  constant LUT_ADDR_WIDTH : integer := 8;
	signal chproc_lut_q : std_logic_vector(31 downto 0);
	signal chproc_lut_addr : std_logic_vector(LUT_ADDR_WIDTH - 1 downto 0);
  
  -- Signals for the ipbus register
  constant N_REGS : integer := 4;
  constant N_STAT : integer := N_REGS;
  constant N_CTRL : integer := N_REGS;
  signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);
  signal stat : ipb_reg_v(N_STAT-1 downto 0);
  signal conf : ipb_reg_v(N_CTRL-1 downto 0);

  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals declaration
  signal conf_channel_process_enable, conf_station_filter_enable_readout, conf_station_filter_enable_trigger,
    conf_timestamp_filter_enable : std_logic := '0';

  signal conf_station_filter : std_logic_vector(1 downto 0) := (others => '0');

  signal conf_BXoffset, conf_timestamp_filter_latency,
    conf_timestamp_filter_window : unsigned(11 downto 0) := (others => '0');
  signal stat_lastBXdiff : unsigned(15 downto 0) := (others => '0');

  -- End of automatically-generated VHDL code for register "breakout" signals declaration
  --------------------------------------------------------------------------------


begin


  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals assignment

  conf_BXoffset                      <= unsigned( conf(16#00#)(11 downto 0) );
  conf_channel_process_enable        <= conf(16#00#)(25);
  conf_station_filter                <= conf(16#00#)(27 downto 26);
  conf_station_filter_enable_readout <= conf(16#00#)(28);
  conf_station_filter_enable_trigger <= conf(16#00#)(29);
  conf_timestamp_filter_latency      <= unsigned( conf(16#01#)(11 downto 0) );
  conf_timestamp_filter_window       <= unsigned( conf(16#01#)(23 downto 12) );
  conf_timestamp_filter_enable       <= conf(16#01#)(24);
  stat(16#00#)(15 downto 0)          <= std_logic_vector( stat_lastBXdiff );

  -- End of automatically-generated VHDL code for register "breakout" signals assignment
  --------------------------------------------------------------------------------

  -- ipbus address decode
	fabric: entity work.ipbus_fabric_sel
    generic map(
    	NSLV => N_SLAVES,
    	SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      sel => ipbus_sel_ab7_hitpreprocessor(ipb_in.ipb_addr),
      ipb_to_slaves => ipb_to_slaves,
      ipb_from_slaves => ipb_from_slaves
    );

  --------------------------------------
  -- IPbus LUT
  --------------------------------------
  channel_process_mem: entity work.ipbus_dpram
    generic map( ADDR_WIDTH => LUT_ADDR_WIDTH  )
    port map(
      clk     => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_CHANNEL_PROCESS_LUT),
      ipb_out => ipb_from_slaves(N_SLV_CHANNEL_PROCESS_LUT),
		  rclk    => clk160,
		  we      => '0',
		  d       => (31 downto 0 => '0'),
		  q       => chproc_lut_q,
		  addr    => chproc_lut_addr
    );

  chproc_lut_addr <=  obdt_data(24 downto 17);
  
  
  --------------------------------------
  -- IPbus register
  --------------------------------------
  reg: entity work.ipbus_syncreg_v
    generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
    port map(
      clk => ipb_clk,
      rst => ipb_rst,
      ipb_in => ipb_to_slaves(N_SLV_CSR),
      ipb_out => ipb_from_slaves(N_SLV_CSR),
      slv_clk => clk160,
      d => stat,
      q => conf,
      --rstb => stat_stb,
      stb => ctrl_stb
    );

  process(clk160)
    variable hits_data_pipe_1, hits_data_pipe_2, hits_data_pipe_3, hits_data_pipe_4 : std_logic_vector(31 downto 0);
    variable tdctmp : unsigned(5 downto 0);
  begin
    if rising_edge(clk160) then
      -- this section is filter for station, it operates on the data after going through the spy module,
      -- so that the spy module can work well in snapshot mode (capturing every input hit) and also inject hits
      -- that will be used by the trigger module
      if conf_station_filter_enable_readout = '0' or conf_station_filter = hit_data_spied(29 downto 28) then
        hit_data_r <= hit_data_spied;
      else
        hit_data_r <= (others => '0');
      end if;
      if conf_station_filter_enable_trigger = '0' or conf_station_filter = hit_data_spied(29 downto 28) then
        hit_data_t <= hit_data_spied;
      else
        hit_data_t <= (others => '0');
      end if;

      -- Pipeline processing stage #5: just register signal (and measure stat_lastBXdiff)
      hit_data <= hits_data_pipe_4;
      if hits_data_pipe_4(31) = '1' then
        stat_lastBXdiff(15 downto 12) <= stat_lastBXdiff(15 downto 12) + 1;
        stat_lastBXdiff(11 downto 0) <=  BXminus( unsigned(bunch_ctr), unsigned(hits_data_pipe_4(16 downto 5)) );
      end if;

      -- Pipeline processing stage #4: Timestamp Filter
      hits_data_pipe_4 := hits_data_pipe_3;
      if hits_data_pipe_4(31) = '1' then
        if BXminus( 
            unsigned(hits_data_pipe_4(16 downto 5)), 
            BXminus(unsigned(bunch_ctr) , conf_timestamp_filter_latency)
          ) > conf_timestamp_filter_window and conf_timestamp_filter_enable = '1'
        then
          hits_data_pipe_4(31) := '0';
        end if;
      end if;
      
      -- Pipeline processing stage #3: Part of the hit retiming to achieve design timing closure
      hits_data_pipe_3 := hits_data_pipe_2;
      if tdctmp > 30 and hits_data_pipe_3(31) = '1' and conf_channel_process_enable = '1' then
        tdctmp := tdctmp - 30;
        hits_data_pipe_3(16 downto 5) := BXplus( hits_data_pipe_3(16 downto 5), to_unsigned(1,12) );
        hits_data_pipe_3(4 downto 0) := std_logic_vector(tdctmp(4 downto 0));
      end if;

      -- Pipeline processing stage #2: Per-channel (LUT) operations: re-map, re-time
      hits_data_pipe_2 := hits_data_pipe_1;
      if hits_data_pipe_2(31) = '1' and conf_channel_process_enable = '1' then
        hits_data_pipe_2(31) := hits_data_pipe_2(31) and not chproc_lut_q(31);
        if hits_data_pipe_2(31) = '1' then 
          hits_data_pipe_2(29 downto 17) := chproc_lut_q(12 downto 0);
          hits_data_pipe_2(16 downto 5) := BXplus( hits_data_pipe_2(16 downto 5), chproc_lut_q(29 downto 18) );
          tdctmp := '0' & unsigned(hits_data_pipe_2(4 downto 0));
          tdctmp := tdctmp + unsigned( chproc_lut_q(17 downto 13) );
          --if tdctmp > 30 then
          --  tdctmp := tdctmp - 30;
          --  hits_data_pipe_2(16 downto 5) := BXplus( hits_data_pipe_2(16 downto 5), to_unsigned(1,12) );
          --end if;
          hits_data_pipe_2(4 downto 0) := std_logic_vector(tdctmp(4 downto 0));
        end if;
      end if;
      
      -- Pipeline processing stage #1: register signal and general re-timing
      -- this produces an extra delay cycle that is needed for the channel_process lut access
      hits_data_pipe_1 := obdt_data;
      if hits_data_pipe_1(31) = '1' then
        -- General re-timing
        hits_data_pipe_1(16 downto 5) := BXplus( hits_data_pipe_1(16 downto 5) , conf_BXoffset ) ;
      end if;
      
    end if;
  end process;

end architecture;
