-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Trigger general modules
---- File       : trigger_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-12
-------------------------------------------------------------------------------
---- Description: Package for trigger hierarchical top modules 
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.globalpath_pkg.all;
use work.ipbus.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package trigger_pkg is
  
  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component TECHNICAL_TRIGGER is
    port ( 
      -- ttc and clocks
      tpg_clk                 : in  std_logic;
      ttc_clk                 : in  std_logic;
      ttc_rst                 : in  std_logic;
      bunch_ctr               : in  std_logic_vector(11 downto 0);
      bunch_ctr_tpg_clk       : in  std_logic_vector(11 downto 0);
      -- config
      bx_delay                : in  unsigned(11 downto 0);
      future_limit            : in  unsigned(11 downto 0);
      q_threshold_1           : in  unsigned(2 downto 0);
      q_threshold_2           : in  unsigned(2 downto 0);
      tpg_tt_enable           : in  std_logic;
      test_tt_bx              : in  std_logic_vector(11 downto 0);
      test_tt_orbit_prescale  : in  unsigned(3 downto 0);
      test_tt_periodic_enable : in  std_logic;
      test_tt_manual          : in  std_logic;
      test_tt_enable          : in  std_logic;
      -- tp input
      tpg                     : in  dtprimitive_t;
      tpg_valid               : in  std_logic;
      -- stats
      tt_ctr                  : out unsigned(31 downto 0);
      -- output
      dt_trig                 : out std_logic
    );
  end component;
  
  component CHAMBER_TRIGGER is
  port ( 
    clk40MHz             : in  std_logic;
    clk_fifo_in          : in  std_logic;
    clk_fifo_out         : in  std_logic;
    clk_trg              : in  std_logic;
    clk_trg_high         : in  std_logic;
    --
    rst_40               : in  std_logic;
    rst_fifo_in          : in  std_logic;
    rst_trg              : in  std_logic;
    rst_trg_high         : in  std_logic;
    -- Global input parameters
    bunch_ctr            : in  std_logic_vector(11 downto 0);
    orbit_ctr            : in  std_logic_vector(31 downto 0);
    -- IP-Bus
    clk_ipb              : in  std_logic;
    rst_ipb              : in  std_logic;
    ipb_in               : in  ipb_wbus;
    ipb_out              : out ipb_rbus;
    -- Trigger IN/OUT information
    fifoIn_wr            : in  std_logic_vector(1 downto 0);
    rawHitPhi            : in  ChamberRawHit_arr_t;
    -- FIFO Intercepts
    fifoInterceptIn      : in  SegCandFifoInterceptIn_arr_t;
    fifoInterceptOut     : out SegCandFifoInterceptOut_arr_t;
    --
    fifoIn_full          : out std_logic_vector(1 downto 0);
    dt_trig              : out std_logic;
    readout_data_valid   : out std_logic;
    readout_data         : out std_logic_vector(DTPRIMITIVE_SIZE - 1 downto 0)
  );
  end component;
  
end package;
