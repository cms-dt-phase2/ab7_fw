-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Duplicated raw hits filter
---- File       : raw_hit_duplicated_filter.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-11
-------------------------------------------------------------------------------
---- Description: Rejects duplicated RAW HITS before 'MIXER' stage
----
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;
use work.dt_common_pkg.all;
use work.filters_pkg.all;

entity RAW_HIT_DUPLICATED_FILTER is
  generic (MEM_ELEMENTS : integer := 8);
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    rawHitFilterIn  : in  RawHitFilterIn_t;
    rawHitFilterOut : out RawHitFilterOut_t
  );
end entity;

architecture Behavioural of RAW_HIT_DUPLICATED_FILTER is

  constant BX_ZERO : std_logic_vector(11 downto 0) := (others => '0');
  constant NO_DUPS : std_logic_vector(MEM_ELEMENTS - 1 downto 0) 
                   := (others => '0');

  subtype raw_hit_slv is std_logic_vector(DTRAWHIT_SIZE - 1 downto 0);
  type raw_hit_shift_reg_t is array (MEM_ELEMENTS-1 downto 0) of raw_hit_slv;
  
  -- Internal registers
  signal data_reg           : raw_hit_shift_reg_t;
  signal bx_counter_lsb     : std_logic;
  signal bx_counter_changed : std_logic;

  -- Auxiliary signals 
  signal check_data     : std_logic;
  signal send_data      : std_logic;
  signal clear_regs     : std_logic;
  signal there_are_dups : std_logic;
  signal bx_counter     : std_logic_vector(11 downto 0);
  signal duplicated     : std_logic_vector(MEM_ELEMENTS - 1 downto 0);
  signal data_in        : std_logic_vector(DTRAWHIT_SIZE - 1 downto 0);
                                      
begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  rawHitFilterOut.rawHit     <= rawHitFilterIn.rawHit;
  rawHitFilterOut.fifoIn_rd  <= check_data;
  rawHitFilterOut.fifoOut_wr <= send_data;
  
  ---------------------------------------------------------------
  -- Useful aliases 
  --
  data_in    <= slv_from_dtRawHit(rawHitFilterIn.rawHit);
  bx_counter <= rawHitFilterIn.bx_counter;
  
  ---------------------------------------------------------------
  -- Combinatory logic
  --
  clear_regs <= '1' when bx_counter = BX_ZERO and bx_counter_changed = '1' 
                    else '0';
  
  check_data <= not rawHitFilterIn.fifoOut_full and 
                not rawHitFilterIn.fifoIn_empty and 
                not clear_regs;
  
  COMPARISIONS : for i in 0 to MEM_ELEMENTS - 1 generate
    duplicated(i) <= raw_hits_slv_are_equal( data_in, data_reg(i) );
  end generate;
    
  there_are_dups <= '0' when duplicated = NO_DUPS else '1';
  send_data      <= check_data and not there_are_dups;
  
  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst, send_data)
  begin
    if clk = '1' and clk'event then
      if rst = '1' then 
        bx_counter_lsb     <= '0';
        bx_counter_changed <= '0';
      else 
        bx_counter_lsb <= bx_counter(0);

        -- BX counter value change detector.
        bx_counter_changed <= bx_counter_lsb and (not bx_counter(0));
      end if;
      
      if rst = '1' or clear_regs = '1' then 
        data_reg <= (others => (others => '0'));
      elsif send_data = '1' then 
        data_reg <= data_reg(MEM_ELEMENTS - 2 downto 0) & data_in;
      end if;
    end if;
  end process;

end architecture;
