-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Clock utilities package
---- File       : clock_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-03
-------------------------------------------------------------------------------
---- Description: Clock utilities package
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package clock_pkg is

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component BIT_CLOCK_SYNCH is
  generic (FAST_REG_STEPS : integer := 2);
  port(
    slow_clk : in  std_logic;
    fast_clk : in  std_logic;
    bit_in   : in  std_logic;
    bit_out  : out std_logic
  );
  end component;
  
end package;
