-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Multi-channel data mixer
---- File       : valid_paths_encoder.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-10
-------------------------------------------------------------------------------
---- Description: This module wraps the 'VALIDATE_PATH' encoder, that operates
----              only on a single candidate path, and creates 8 instances
----              of this type of encoder to cope with all 8 possible 
----              paths, based on the layer and channel passed as input to this
----              module.
----              It also computes which should be the associated channels' 
----              indexes for every of those 8 possible trajectories, based on
----              the cell's relative horizontal displacements from the given
----              one.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.mixer_pkg.all;

entity VALID_PATHS_ENCODER is
  port (
    clk             : in  std_logic;
    channel         : in  std_logic_vector(6 downto 0);
    layer           : in  std_logic_vector(1 downto 0);
    ch_empty_lay0   : in  std_logic_vector(127 downto 0);
    ch_empty_lay1   : in  std_logic_vector(127 downto 0);
    ch_empty_lay2   : in  std_logic_vector(127 downto 0);
    ch_empty_lay3   : in  std_logic_vector(127 downto 0);
    valid_paths     : out std_logic_vector(7 downto 0);
    path_candidates : out path_candidates_arr_t
  );
--   attribute USE_DSP48 : string;
--   attribute USE_DSP48 of VALID_PATHS_ENCODER : entity is "yes";
end entity;

architecture Behavioural of VALID_PATHS_ENCODER is
  type path_group_t is array (0 to 7) of sgn_channels_path_t;
  signal path_candidates_int : path_group_t;
  
  -- Registers for signals.
  type reg_type is record
    valid_paths     : std_logic_vector(7 downto 0);
    path_candidates : path_candidates_arr_t;    
  end record;

  signal reg, nextReg : reg_type;

begin
  -- Outgoing signals
  valid_paths     <= reg.valid_paths;
  path_candidates <= reg.path_candidates;

  -- Registers for outgoing signals
  process(clk)
  begin
    if clk = '1' and clk'event then
      reg <= nextReg;
    end if;
  end process;

  -- Computes all possible channel indexes for the 8 trajectories.
  PATH_LOOP : for path in 0 to 7 generate
    CHANNEL_LOOP : for ch in 0 to 3 generate
      path_candidates_int(path)(ch) <= signed('0' & channel) +
        MIXER_CELL_RELATIVE_DISPLACEMENT(to_integer(unsigned(layer)))(path)(ch);

      -- Pre-registering signals
      nextReg.path_candidates(path)(ch) 
            <= std_logic_vector(path_candidates_int(path)(ch)(6 downto 0));

    end generate;
  end generate;

  -- Creates 8 encoder instances.
  VALIDATE_LOOP : for path in 0 to 7 generate
    VAL_PATH : VALIDATE_PATH port map (
      path_candidate => path_candidates_int(path),
      layer          => layer,
      ch_empty_lay0  => ch_empty_lay0,
      ch_empty_lay1  => ch_empty_lay1,
      ch_empty_lay2  => ch_empty_lay2,
      ch_empty_lay3  => ch_empty_lay3,
      valid          => nextReg.valid_paths(path)
    );
  end generate;

end architecture;
