-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Arithmetics
---- File       : arith_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-06
-------------------------------------------------------------------------------
---- Description: Package for arithmetic components
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package arith_pkg is

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component DIVISOR_B6Q12_LUT is
    port(
      value          : in  std_logic_vector(5 downto 0);
      value_inverted : out std_logic_vector(11 downto 0)
    );
  end component;

  component DIVISOR_B6Q15_LUT is
    port(
      value          : in  std_logic_vector(5 downto 0);
      value_inverted : out std_logic_vector(14 downto 0)
    );
  end component;

  component DIVISOR_B6Q20_LUT is
    port(
      value          : in  std_logic_vector(5 downto 0);
      value_inverted : out std_logic_vector(19 downto 0)
    );
  end component;

  component DIVISOR_B8Q16_LUT_SIGNED is
  port(
    value          : in  signed(8 downto 0);
    value_inverted : out signed(16 downto 0)
  );
  end component;
  
  component DIVISOR_B6Q15_LUT_SIGNED is
    port(
      value          : in  signed(6 downto 0);
      value_inverted : out signed(15 downto 0)
    );
  end component;
  
  component DIVISOR_B6Q18_LUT_SIGNED is
    port(
      value          : in  signed(6 downto 0);
      value_inverted : out signed(18 downto 0)
    );
  end component;
  
  component DIVISION_B20B6 is
    port(
      clk         : in  std_logic;
      rst         : in  std_logic;
      enable      : in  std_logic;
      numerator   : in  std_logic_vector(19 downto 0);
      denominator : in  std_logic_vector( 5 downto 0);
      div_by_zero : out std_logic;
      result      : out std_logic_vector(19 downto 0)
    );
  end component;

  component DIVISION_B22B6_SIGNED is
    generic(
      DENOM_INV_BITS: positive := 15
    );
    port(
      clk         : in  std_logic;
      rst         : in  std_logic;
      enable      : in  std_logic;
      numerator   : in  signed(22 downto 0);
      denominator : in  signed( 6 downto 0);
      div_by_zero : out std_logic;
      result      : out signed(22 downto 0)
    );
  end component;

  component DIVISION_B24B6 is
    generic(
      DENOM_INV_BITS: positive := 15
    );
    port(
      clk         : in  std_logic;
      rst         : in  std_logic;
      enable      : in  std_logic;
      numerator   : in  std_logic_vector(23 downto 0);
      denominator : in  std_logic_vector( 5 downto 0);
      div_by_zero : out std_logic;
      result      : out std_logic_vector(23 downto 0)
    );
  end component;
  
  component DIVISION_B24B6_SIGNED is
    port(
      clk         : in  std_logic;
      rst         : in  std_logic;
      enable      : in  std_logic;
      numerator   : in  signed(24 downto 0);
      denominator : in  signed( 6 downto 0);
      div_by_zero : out std_logic;
      result      : out signed(24 downto 0)
    );
  end component;
  
  component DIVISION_B24B8_SIGNED is
  port(
    clk         : in  std_logic;
    rst         : in  std_logic;
    enable      : in  std_logic;
    numerator   : in  signed(24 downto 0);
    denominator : in  signed( 8 downto 0);
    div_by_zero : out std_logic;
    result      : out signed(24 downto 0)
  );
  end component;
  
  
end package;
