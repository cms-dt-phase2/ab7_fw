----------------------------------------------------------------------------------
-- Company: CERN, BRIL
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_fc7_injector - rtl
-- Description: 
-- 
--          Test bench to be used with the fc7 firmware. Instantiates BRAM-controlled injections of the histograms
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- user macros to create fifo (device agnostic)
library xpm;
use xpm.vcomponents.all;

-- package with config
use work.bril_histogram_fc7_injector_package.all;

entity bril_histogram_fc7_injector is
Port ( 
    -- commong
    reset_i                             : in std_logic;
    clk_i                               : in std_logic;
    ipb_clk_i                           : in std_logic;
    -- readout interface
    bril_fifo_rd_en_i                   : in std_logic;
    bril_fifo_empty_o                   : out std_logic;
    bril_fifo_words_cnt_o               : out std_logic_vector(31 downto 0);
    bril_data_o                         : out std_logic_vector(31 downto 0);
    bril_data_valid_o                   : out std_logic;
    -- injection interface
    injection_data_i                    : in std_logic_vector(31 downto 0);
    injection_reset_pointer_i           : in std_logic;
    injection_wr_en_i                   : in std_logic;
    injection_norbits_available_i       : in natural;
    injection_norbits_tocollect_i       : in natural;
    injection_start_i                   : in std_logic;
    injection_stop_i                    : in std_logic
);
end bril_histogram_fc7_injector;

architecture rtl of bril_histogram_fc7_injector is

    signal memory_reset                 : std_logic;

    signal reset_histogram              : std_logic;
    signal reset_histogram_buffered     : std_logic;
    
    signal bril_new_iteration           : std_logic;
    signal bril_new_histogram           : std_logic;
    signal bril_increment               : std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0);
    signal bril_error                   : std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    signal bril_mask                    : std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    signal bril_lumi_section            : std_logic_vector(31 downto 0);        
    signal bril_lumi_section_buffered   : std_logic_vector(31 downto 0);        
    signal bril_lumi_nibble             : std_logic_vector(31 downto 0);        
    signal bril_lumi_nibble_buffered    : std_logic_vector(31 downto 0);   
    signal bril_valid                   : std_logic;
    signal bril_iteration_id            : std_logic_vector(15 downto 0);     
    signal bril_bin_id                  : std_logic_vector(15 downto 0);     

    signal injection_start_pre_sync         : std_logic;
    signal injection_start_sync         : std_logic;
    signal injection_stop_pre_sync          : std_logic;
    signal injection_stop_sync          : std_logic;
    signal injection_addr               : std_logic_vector(17 downto 0);
    attribute ASYNC_REG : string;
    attribute ASYNC_REG of injection_start_pre_sync : signal is "true";
    attribute ASYNC_REG of injection_start_sync : signal is "true";    
    attribute ASYNC_REG of injection_stop_pre_sync : signal is "true";
    attribute ASYNC_REG of injection_stop_sync : signal is "true";
    signal injection_nbx_available_int           : natural;
    signal injection_norbits_tocollect_int       : natural;
    signal bx_counter                   : natural;
    signal orbit_counter                : natural;
    signal async_init_done              : std_logic;
    signal current_bram_addr            : natural;
    signal current_bram_data            : std_logic_vector(255 downto 0);
    signal async_counter                : natural range 0 to 3;
    
    type injector_fsm_state_type is (Idle, PreInjecting, Injecting);
    signal injector_fsm_state           : injector_fsm_state_type;

begin

    -- injection memory
    -- Xilinx Parameterized Macro, version 2018.3
   injector_memory : xpm_memory_sdpram
   generic map (
      ADDR_WIDTH_A => 18,               -- DECIMAL
      ADDR_WIDTH_B => 15,               -- DECIMAL
      AUTO_SLEEP_TIME => 0,            -- DECIMAL
      BYTE_WRITE_WIDTH_A => 32,        -- DECIMAL
      CLOCKING_MODE => "independent_clock", -- String
      ECC_MODE => "no_ecc",            -- String
      MEMORY_INIT_FILE => "none",      -- String
      MEMORY_INIT_PARAM => "0",        -- String
      MEMORY_OPTIMIZATION => "true",   -- String
      MEMORY_PRIMITIVE => "auto",      -- String
      MEMORY_SIZE => 262144*32,             -- DECIMAL
      MESSAGE_CONTROL => 0,            -- DECIMAL
      READ_DATA_WIDTH_B => 256,         -- DECIMAL
      READ_LATENCY_B => 3,             -- DECIMAL
      READ_RESET_VALUE_B => "0",       -- String
      USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
      USE_MEM_INIT => 1,               -- DECIMAL
      WAKEUP_TIME => "disable_sleep",  -- String
      WRITE_DATA_WIDTH_A => 32,        -- DECIMAL
      WRITE_MODE_B => "no_change"      -- String
   )
   port map (
      dbiterrb => open,
      doutb => current_bram_data,
      sbiterrb => open,
      addra => injection_addr,
      addrb => std_logic_vector(to_unsigned(current_bram_addr,15)),
      clka => ipb_clk_i,
      clkb => clk_i,
      dina => injection_data_i,
      ena => '1',
      enb => '1',
      injectdbiterra => '0',
      injectsbiterra => '0',
      regceb => '1',
      rstb => reset_i,
      sleep => '0',
      wea => (others => injection_wr_en_i)
   );
   -- address comntrol
   process(ipb_clk_i)
   begin
        if rising_edge(ipb_clk_i) then
            if injection_reset_pointer_i = '1' then
                injection_addr <= (others => '0');
            elsif injection_wr_en_i = '1' then
                injection_addr <= injection_addr + 1;
            end if;
        end if;
   end process;

    -- set mask to zero
    bril_mask <= (others => '0');
    
    -- injector
    process(clk_i)
    begin
        if rising_edge(clk_i) then
            if reset_i = '1' then
                current_bram_addr <= 0;
                reset_histogram <= '1';
                memory_reset <= '1';
                injector_fsm_state <= Idle;
                -- syncs
                injection_start_pre_sync <= '0';
                injection_start_sync <= '0'; 
                injection_stop_pre_sync <= '0';
                injection_stop_sync <= '0';
                -- just in case
                bx_counter <= 0;
                orbit_counter <= 0;
                async_counter <= 0;
                -- 
                bril_lumi_section <= (others => '0');
                bril_lumi_nibble <= (others => '0');
                -- sync
                injection_nbx_available_int <= 0;
                injection_norbits_tocollect_int <= 0;
            else
               
                -- sync
                injection_start_pre_sync <= injection_start_i; 
                injection_start_sync <= injection_start_pre_sync; 
                injection_stop_pre_sync <= injection_stop_i; 
                injection_stop_sync <= injection_stop_pre_sync; 
                -- detect stop
                if injection_stop_sync = '1' then
                    injector_fsm_state <= Idle;
                end if;
                
                case injector_fsm_state is
                    when Idle =>
                        -- detect start
                        if injection_start_sync = '1' then
                            -- stob resettin memory
                            memory_reset <= '0';
                            -- store
                            injection_nbx_available_int <= NUMBER_OF_BINS*injection_norbits_available_i;
                            injection_norbits_tocollect_int <= injection_norbits_tocollect_i;
                            -- just in case
                            bx_counter <= 0;
                            orbit_counter <= 0;
                            bril_lumi_section <= (others => '0');
                            bril_lumi_nibble <= (others => '0');
                            if ASYNC_MODE then
                                -- we have more time to wait in this case
                                current_bram_addr <= 0;
                                async_counter <= 0;
                            else
                                -- also increment the address as it has dekay 1 clock cycle
                                current_bram_addr <= 1; -- 
                            end if;
                            -- start injecting
                            injector_fsm_state <= PreInjecting;
                        else
                            current_bram_addr <= 0;
                            reset_histogram <= '1';
                            memory_reset <= '1';
                        end if;
                    when PreInjecting =>
                        if ASYNC_MODE then
                            -- enable histogram
                            reset_histogram <= '0';
                            -- wait for the counter ready
                            if async_counter = 3 then
                                if async_init_done = '1' then
                                    async_counter <= 0;
                                    injector_fsm_state <= Injecting;
                                end if;
                            else
                                async_counter <= async_counter + 1;
                            end if;
                        else
                            current_bram_addr <= current_bram_addr + 1;
                            if current_bram_addr = 2 then -- delay of 3-1(already this clock cycle) = 2 
                                -- enable histogram
                                reset_histogram <= '0';
                                -- start injection
                                injector_fsm_state <= Injecting;
                            end if;
                        end if;
                    when Injecting =>
                        if (ASYNC_MODE = false) or async_counter = 0 then
                            -- address of the injection data
                             if current_bram_addr < injection_nbx_available_int-1 then
                                current_bram_addr <= current_bram_addr + 1;
                             else
                                current_bram_addr <= 0;
                             end if;
                             
                             -- counters orbit
                             if bx_counter = NUMBER_OF_BINS-1 then
                                -- orbit counter
                                if orbit_counter < injection_norbits_tocollect_int-1 then
                                    orbit_counter <= orbit_counter + 1;
                                else
                                    -- reset orbit counter
                                    orbit_counter <= 0;
                                    
                                    -- and increment the nibble
                                    if bril_lumi_nibble < 63 then -- random value here
                                        bril_lumi_nibble <= bril_lumi_nibble + 1;
                                    else
                                        -- reset nibble
                                        bril_lumi_nibble <= (others => '0');
                                        -- new lumi section
                                        bril_lumi_section <= bril_lumi_section + 1;
                                    end if;
                                end if;
                             end if;
                             
                             -- bx
                             if bx_counter < NUMBER_OF_BINS-1 then
                                bx_counter <= bx_counter + 1;
                             else
                                bx_counter <= 0;
                             end if; 
                        end if;
                        
                        if ASYNC_MODE then
                            if async_counter = 3 then
                                async_counter <= 0;
                            else
                                async_counter <= async_counter + 1;
                            end if;
                        end if;
                                              
                   
                    when others =>
                        injector_fsm_state <= Idle;
                end case;
            end if;
        end if;
    end process;
    -- here we assign the data
    process(clk_i)
    begin
        if rising_edge(clk_i) then
            if ASYNC_MODE = false or (injector_fsm_state = Injecting and async_counter = 0) then
                --
                bril_valid <= '1';
                bril_iteration_id <= std_logic_vector(to_unsigned(orbit_counter,16));
                bril_bin_id <= std_logic_vector(to_unsigned(bx_counter,16));
                -- buffering of all the signals to make timing a bit more simple
                for i in 0 to NUMBER_OF_UNITS-1 loop
                    bril_increment(UNIT_INCREMENT_WIDTH*(i+1) - 1 downto UNIT_INCREMENT_WIDTH*i + 0) <= current_bram_data(4*i + UNIT_INCREMENT_WIDTH - 1 downto 4*i + 0);
                    bril_error(i) <= current_bram_data(4*i+3);
                end loop;
                if bx_counter = NUMBER_OF_BINS-1 then
                    bril_new_iteration <= '1';
                    if orbit_counter = injection_norbits_tocollect_int-1 then
                        bril_new_histogram <= '1';
                        -- ipdate lumi nibble/section
                        bril_lumi_section_buffered <= bril_lumi_section;
                        bril_lumi_nibble_buffered <= bril_lumi_nibble;
                    else
                        bril_new_histogram <= '0';
                    end if;
                else
                    bril_new_iteration <= '0';
                    bril_new_histogram <= '0';
                end if;
            else
                bril_valid <= '0';
                bril_iteration_id <= (others => '0');
                bril_bin_id <= (others => '0');
            end if;
            -- 
            reset_histogram_buffered <= reset_histogram;
        end if;
    end process;

    gen_sync: if ASYNC_MODE = false generate
        bril_histogram_accumulation_layer_sync_inst: entity work.bril_histogram_accumulation_layer_sync
        generic map (
            -- identification of the histogram
            HISTOGRAM_TYPE                          => 0,
            HISTOGRAM_ID                            => 0,
            -- memory
            GENERATE_FIFO                           => true,
            -- histogram parameters
            NUMBER_OF_BINS                          => NUMBER_OF_BINS,
            COUNTER_WIDTH                           => COUNTER_WIDTH,
            INCREMENT_WIDTH                         => INCREMENT_WIDTH,
            --
            ORBIT_BX_COUNTER_WIDTH                  => 16,
            -- accumulation parameters
            NUMBER_OF_UNITS                         => NUMBER_OF_UNITS,
            UNIT_INCREMENT_WIDTH                    => UNIT_INCREMENT_WIDTH,
            NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE  => NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE,
            UNIT_ERROR_COUNTER_WIDTH                => UNIT_ERROR_COUNTER_WIDTH,
            STORE_ERRORS                            => STORE_ERRORS
        )
        port map(
            -- core signals
            memory_reset_i                  => memory_reset,
            reset_i                         => reset_histogram_buffered,
            clk_i                           => clk_i,
            -- tcds signals
            new_histogram_i                 => bril_new_histogram,
            new_iteration_i                 => bril_new_iteration,
            -- tcds data
            lhc_fill_i                      => (others => '0'),
            cms_run_i                       => (others => '0'),
            lumi_section_i                  => bril_lumi_section_buffered,
            lumi_nibble_i                   => bril_lumi_nibble_buffered,
            -- counter signals  
            increment_i                     => bril_increment,
            error_i                         => bril_error,
            mask_i                          => bril_mask,
            -- fifo interface (optional)
            fifo_rd_clk_i                   => ipb_clk_i,
            fifo_rd_en_i                    => bril_fifo_rd_en_i,
            fifo_empty_o                    => bril_fifo_empty_o,
            fifo_words_cnt_o                => bril_fifo_words_cnt_o,
            --
            data_o                          => bril_data_o,
            data_valid_o                    => bril_data_valid_o
        );
    end generate gen_sync;
    
    gen_async: if ASYNC_MODE = true generate
        bril_histogram_accumulation_layer_async_inst: entity work.bril_histogram_accumulation_layer_async
        generic map (
            -- identification of the histogram
            HISTOGRAM_TYPE                          => 0,
            HISTOGRAM_ID                            => 0,
            -- histogram parameters
            NUMBER_OF_BINS                          => NUMBER_OF_BINS,
            COUNTER_WIDTH                           => COUNTER_WIDTH,
            INCREMENT_WIDTH                         => INCREMENT_WIDTH,
            --
            ORBIT_BX_COUNTER_WIDTH                  => 16,
            -- accumulation parameters
            NUMBER_OF_UNITS                         => NUMBER_OF_UNITS,
            UNIT_INCREMENT_WIDTH                    => UNIT_INCREMENT_WIDTH,
            NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE  => NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE,
            UNIT_ERROR_COUNTER_WIDTH                => UNIT_ERROR_COUNTER_WIDTH,
            STORE_ERRORS                            => STORE_ERRORS
        )
        port map(
            -- core signals
            reset_i                         => reset_histogram_buffered,
            clk_i                           => clk_i,
            -- tcds data
            lhc_fill_i                      => (others => '0'),
            cms_run_i                       => (others => '0'),
            lumi_section_i                  => bril_lumi_section_buffered,
            lumi_nibble_i                   => bril_lumi_nibble_buffered,
            --
            valid_i                         => bril_valid,
            iteration_id_i                  => bril_iteration_id,
            bin_id_i                        => bril_bin_id,
            -- counter signals  
            increment_i                     => bril_increment,
            error_i                         => bril_error,
            mask_i                          => bril_mask,
            -- fifo interface (optional)
            fifo_rd_clk_i                   => ipb_clk_i,
            fifo_rd_en_i                    => bril_fifo_rd_en_i,
            fifo_empty_o                    => bril_fifo_empty_o,
            fifo_words_cnt_o                => bril_fifo_words_cnt_o,
            fifo_data_o                     => bril_data_o,
            fifo_data_valid_o               => bril_data_valid_o,
            --
            init_done_o                     => async_init_done
        );
    end generate gen_async;
    
    

end rtl;
