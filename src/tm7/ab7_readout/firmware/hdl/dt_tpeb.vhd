----------------------------------------------------------------------------------
-- AB7 event builder for readout of the trigger primitives
-- Alvaro Navarro, CIEMAT
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.bo2.all;
use work.minmax.all;
use work.TM7_readout_decl.all;
use work.top_decl.all; -- for LHC_BUNCH_COUNT
use work.mp7_ttc_decl.all;
use work.dt_common_pkg.all; -- Cela's Trigger data types

entity dt_tpeb is
  port(
    -- readout configuration (semi-static)
    latency                 : in unsigned( 11 downto 0 );
    window                  : in unsigned( 11 downto 0 );
    procrastination         : in unsigned( 11 downto 0 );
    startpointer_cooldown   : in unsigned( 11 downto 0 );
    max_disorder            : in unsigned( 11 downto 0 );
    fullchi2                : in std_logic;
    station                 : in std_logic_vector( 1 downto 0 );
    -- ttc inputs: ttc_clk clocked
    ttc_clk                 : in std_logic; -- TTC and input data
    ttc_rst                 : in std_logic; -- for all clocks
    l1a                     : in std_logic;
    evt_ctr                 : in std_logic_vector( RO_EVN_WIDTH-1 downto 0 );
    bunch_ctr               : in std_logic_vector( 11 downto 0 );
    -- input data
    input_clk               : in std_logic;
    tp                      : std_logic_vector(DTPRIMITIVE_SIZE-1 downto 0);
    tp_valid                : in std_logic; 
    -- intermediate clock for output fifo writing
    midclk                  : in std_logic;
    -- output fifo ports: rdclk clocked
    rdclk                   : in std_logic;
    empty                   : out std_logic;
    almost_empty            : out std_logic;
    rden                    : in std_logic;
    do                      : out std_logic_vector(63 downto 0);
    evnout                  : out std_logic_vector(RO_EVN_WIDTH-1 downto 0);
    ff_rdcount              : out std_logic_vector(12 downto 0);
    ff_wrcount              : out std_logic_vector(12 downto 0)
  );
end entity;

architecture Behavioral of dt_tpeb is
  ---------------------------------------------------
  -- Non-Monotonic-Data Window Read instance generics
  ---------------------------------------------------
  -- For each event, the stored data is the fine TDC field (5 bits) + channel number (8 bits) --> 13 bits 
  constant DATA_WIDTH : positive := 350; -- plus the 12 bits of the bx, which is input separately

  -- Worst case: tps @ 40 MHz (= 1 hit/bx), need to store up to 1000 bx of latency = 1000, (10 bits)
  constant NONMONOTO_MEMDEPTH : positive := 10;
  
  ------------------------------------------------
  -- Non-Monotonic-Data Window Read instance ports
  ------------------------------------------------
  -- inputs
  signal input_wr       : std_logic;
  signal input_bx       : std_logic_vector( 11 downto 0 );
  signal input_data     : std_logic_vector( DATA_WIDTH-1 downto 0 );
  -- outputs
  signal nmdwr_inevent  : boolean;
  signal nmdwr_valid    : boolean; -- marks pauses due to words being read but not needed (out of window due to non-monotonicity)
  signal nmdwr_evn      : std_logic_vector( RO_EVN_WIDTH-1 downto 0 );
  signal nmdwr_bx       : std_logic_vector( 11 downto 0 );
  signal nmdwr_data     : std_logic_vector( DATA_WIDTH-1 downto 0 );
  
  constant TP_NUM_WORDS : natural := 8;
  type ropayload_t is array(natural range <>) of std_logic_vector(63 downto 0);
  signal ropayload : ropayload_t(TP_NUM_WORDS-1 downto 0) := (others => (others => '0'));

  -- output fifo signals
  constant OUTFIFO_WIDTH : integer := 64 + RO_EVN_WIDTH;
  signal ff_RST, ff_af, ff_full, ff_wren : std_logic;
  signal ff_di, ff_do : std_logic_vector(OUTFIFO_WIDTH-1 downto 0) := (others => '0');
    
begin
  -----------------------------------------
  -- Instance of the Non-Monotonic-Data Window Read module
  -----------------------------------------
  nonmonoto_inst : entity work.nonmonotonic_data_window_read
  generic map(
    DATA_WIDTH      => DATA_WIDTH,
    EVN_WIDTH       => RO_EVN_WIDTH,
    MEMDEPTH        => NONMONOTO_MEMDEPTH,
    SPEAK_SLOWLY    => TP_NUM_WORDS
  )
  port map(
    -- readout configuration (semi-static)
    latency                 => latency                ,
    window                  => window                 ,
    procrastination         => procrastination        ,
    startpointer_cooldown   => startpointer_cooldown  ,
    max_disorder            => max_disorder           ,
    -- ttc inputs: ttc_clk clocked
    ttc_clk                 => ttc_clk                ,
    ttc_rst                 => ttc_rst                ,
    l1a                     => l1a                    ,
    evt_ctr                 => evt_ctr                ,
    bunch_ctr               => bunch_ctr              ,
    -- input data
    input_clk               => input_clk              ,
    input_wr                => input_wr               ,
    input_bx                => input_bx               ,
    input_data              => input_data             ,
    -- output ports: rdclk clocked
    output_clk              => midclk                 ,
    output_inevent          => nmdwr_inevent          ,
    output_valid            => nmdwr_valid            ,
    output_evn              => nmdwr_evn              ,
    output_bx               => nmdwr_bx               ,
    output_data             => nmdwr_data              -- same as input_data
  );
  
  input_wr <= tp_valid;
  --input_bx <= dtSegment_from_slv(tp).bx_id; --.main_params.bx_time(16 downto 5);
  input_bx <= std_logic_vector( minimum( unsigned( dtPrimitive_from_slv(tp).bx_id_chamber ), to_unsigned(LHC_BUNCH_COUNT-1,12) )  );
  
  process (tp, nmdwr_data, bunch_ctr)
    variable offset : natural range DATA_WIDTH downto 0 := 0;
  begin
    offset  := 0;
    
    -- Ser-deser data for PHI1 and PHI2 primitive
    for seg in 0 to 1 loop
      -- Bit 63 in TP words is always "1", so we use it to store the "valid" of the segment (if 0, word should not be written to fifo)
      input_data(                                    1  + offset-1 downto offset) <= (others => dtPrimitive_from_slv(tp).valid_segments(seg) ) ;
      ropayload(3*seg  )(63 downto 63) <= nmdwr_data(1  + offset-1 downto offset) ;
      ropayload(3*seg+1)(63 downto 63) <= nmdwr_data(1  + offset-1 downto offset) ;
      ropayload(3*seg+2)(63          ) <= nmdwr_data(1  + offset-1              ) and fullchi2;
      offset :=                                      1  + offset ;
      
      input_data(                                    17 + offset-1 downto offset) <= dtPrimitive_from_slv(tp).paired_segments(seg).main_params.bx_time ;
      ropayload(3*seg  )(57 downto 41) <= nmdwr_data(17 + offset-1 downto offset) ;
      offset :=                                      17 + offset ;
    
      input_data(                                    3  + offset-1 downto offset) <= dtPrimitive_from_slv(tp).paired_segments(seg).main_params.quality ;
      ropayload(3*seg  )(37 downto 35) <= nmdwr_data(3  + offset-1 downto offset) ;
      offset :=                                      3  + offset ;
    
      input_data(                                    24 + offset-1 downto offset) <= std_logic_vector( dtPrimitive_from_slv(tp).paired_segments(seg).chi_square ) ;
      ropayload(3*seg+2)(23 downto 0 ) <= nmdwr_data(24 + offset-1 downto offset) ; -- full chi2 data stored in optional 3rd word 
      offset :=                                      24 + offset ;
  
      input_data(                                    15 + offset-1 downto offset) <= std_logic_vector(dtPrimitive_from_slv(tp).paired_segments(seg).phi_tangent_x4096) ;
      ropayload(3*seg  )(31 downto 17) <= nmdwr_data(15 + offset-1 downto offset) ;
      offset :=                                      15 + offset ;
  
      input_data(                                    17 + offset-1 downto offset) <= std_logic_vector(dtPrimitive_from_slv(tp).paired_segments(seg).horizontal_position(16 downto 0)) ;
      ropayload(3*seg  )(16 downto  0) <= nmdwr_data(17 + offset-1 downto offset) ;
      offset :=                                      17 + offset ;
  
      input_data(                                    4  + offset-1 downto offset) <= dtPrimitive_from_slv(tp).paired_segments(seg).main_params.laterality_combination ;
      ropayload(3*seg+1)( 3 downto  0) <= nmdwr_data(4  + offset-1 downto offset) ;
      offset :=                                      4  + offset ;
  
      for h in 0 to 3 loop
        input_data(                                  1  + offset-1 downto offset) <= (others => dtPrimitive_from_slv(tp).paired_segments(seg).main_params.hits(h).dt_time.valid);
        ropayload(3*seg+1)(4+h downto 4+h) <=
                                          nmdwr_data(1  + offset-1 downto offset) ;
        offset :=                                    1  + offset ;
  
        input_data(                                  4  + offset-1 downto offset) <= std_logic_vector( minimum ( shift_right( 
                                                                                       unsigned(dtPrimitive_from_slv(tp).paired_segments(seg).main_params.hits(h).dt_time.tdc_time)
                                                                                       - unsigned(dtPrimitive_from_slv(tp).paired_segments(seg).main_params.bx_time)
                                                                                     ,5), to_unsigned(15, 17))(3 downto 0) );
        ropayload(3*seg+1)(8+h*4+3 downto 8+h*4) <=
                                            nmdwr_data(4  + offset-1 downto offset) ;
        offset :=                                      4  + offset ;
  
        input_data(                                    7  + offset-1 downto offset) <= dtPrimitive_from_slv(tp).paired_segments(seg).main_params.hits(h).channel_id;
        ropayload(3*seg+1)(24+h*7+6 downto 24+h*7) <=
                                            nmdwr_data(7  + offset-1 downto offset) ;
        offset :=                                      7  + offset ;
      end loop;
    end loop;

    -- Ser-deser data for chamber PHI primitive
    -- Bit 63 in TP words is always "1", so we use it to store the "valid" of the segment (if 0, word should not be written to fifo)
    input_data(                              1  + offset-1 downto offset) <= (others => (dtPrimitive_from_slv(tp).valid_segments(0) and dtPrimitive_from_slv(tp).valid_segments(1)) ) ;
    ropayload(6)(63 downto 63) <= nmdwr_data(1  + offset-1 downto offset) ;
    ropayload(7)(63          ) <= nmdwr_data(1  + offset-1              ) and fullchi2 ;
    offset :=                                1  + offset ;

    input_data(                              17 + offset-1 downto offset) <= dtPrimitive_from_slv(tp).bx_time_chamber ;
    ropayload(6)(57 downto 41) <= nmdwr_data(17 + offset-1 downto offset) ;
    offset :=                                17 + offset ;
    
    input_data(                              6  + offset-1 downto offset) <= std_logic_vector( 
                                                                                to_unsigned(6,6)
                                                                                + 2*bo2int((unsigned(dtPrimitive_from_slv(tp).paired_segments(0).main_params.quality)>2) or (unsigned(dtPrimitive_from_slv(tp).paired_segments(1).main_params.quality)>2))
                                                                                + bo2int((unsigned(dtPrimitive_from_slv(tp).paired_segments(0).main_params.quality)>2) and (unsigned(dtPrimitive_from_slv(tp).paired_segments(1).main_params.quality)>2))
                                                                              ) ;
    ropayload(6)(40 downto 35) <= nmdwr_data(6  + offset-1 downto offset) ;
    offset :=                                6  + offset ;
    
    input_data(                              24  + offset-1 downto offset) <= std_logic_vector( dtPrimitive_from_slv(tp).chi_square_chamber ) ;
    ropayload(7)(23 downto 0 ) <= nmdwr_data(24 + offset-1 downto offset) ; -- full chi2 data stored in optional 2nd word  
    offset :=                                24  + offset ;
    
    input_data(                              12 + offset-1 downto offset) <= bunch_ctr ;
    ropayload(2)(35 downto 24) <= nmdwr_data(12 + offset-1 downto offset) ; -- arrival bx of the primitive in all 3 full-chi2 words
    ropayload(5)(35 downto 24) <= nmdwr_data(12 + offset-1 downto offset) ;
    ropayload(7)(35 downto 24) <= nmdwr_data(12 + offset-1 downto offset) ;
    offset :=                                12 + offset ;
  
    input_data(                              15 + offset-1 downto offset) <= std_logic_vector(dtPrimitive_from_slv(tp).phi_tangent_x4096_chamber) ;
    ropayload(6)(31 downto 17) <= nmdwr_data(15 + offset-1 downto offset) ;
    offset :=                                15 + offset ;

    input_data(                              17 + offset-1 downto offset) <= std_logic_vector(dtPrimitive_from_slv(tp).horizontal_position_chamber(16 downto 0)) ;
    ropayload(6)(16 downto  0) <= nmdwr_data(17 + offset-1 downto offset) ;
    offset :=                                17 + offset ;

    -- uncomment the following line when changes are made to the serializer-deseriazer data;
    -- synthesis will break and return a number in the error which is 100 units more than the true width.
    --offset :=                              100  + offset ;
    
    -- Constant bits of ropayload which are not dependent on inputs.
    ropayload(0)(62) <= '0'; -- word identification (Bit 63 of word id should be 1, but is being used as a valid)
    ropayload(0)(61 downto 60) <= station; -- St
    ropayload(0)(59 downto 58) <= "01"; -- SL
    
    ropayload(1)(62 downto 60) <= "100"; -- word identification

    ropayload(2)(62 downto 60) <= "101"; -- word identification
    
    ropayload(3)(62) <= '0'; -- word identification (Bit 63 of word id should be 1, but is being used as a valid)
    ropayload(3)(61 downto 60) <= station; -- St
    ropayload(3)(59 downto 58) <= "11"; -- SL
    
    ropayload(4)(62) <= '1'; -- word identification

    ropayload(5)(62 downto 60) <= "101"; -- word identification
    
    ropayload(6)(62) <= '0'; -- word identification (Bit 63 of word id should be 1, but is being used as a valid)
    ropayload(6)(61 downto 60) <= station; -- St
    ropayload(6)(59 downto 58) <= "00"; -- SL

    ropayload(7)(62 downto 60) <= "101"; -- word identification
    

  end process;
  
  
  -----------------------------------------
  -- Reading and creating the output event
  -----------------------------------------
  process(midclk)
    variable word_tictoc : integer range TP_NUM_WORDS-1 downto 0;
  begin
    if rising_edge(midclk) then
      if ttc_rst = '1' then
        word_tictoc := 0;
        ff_wren <= '0';
        ff_di <= (others => '0');
      else
        -- Always compose the word, but it not always is written to fifo (ff_wren)
        -- the msb of all trigger words is always 1, but is being used in the memory to indicate valididty of the word
        ff_di <= nmdwr_evn & '1' & ropayload(word_tictoc)(62 downto 0);
        
        ff_wren <= '0';
        if nmdwr_inevent then
          if nmdwr_valid then
            ff_wren <= ropayload(word_tictoc)(63);
            word_tictoc := (word_tictoc + 1) mod TP_NUM_WORDS ;
          end if;
        end if;
        
      end if; -- rst vs regular operation
    end if; -- rising_edge
  end process;

  -----------------------------------------
  -- Output FIFO
  -----------------------------------------
  hits_out_fifo_inst : entity work.qwfifo
  generic map (
    DATA_WIDTH => OUTFIFO_WIDTH,
    DEPTH_LOG2 => 13
  )
  port map (
    RDCOUNT => ff_RDCOUNT,
    WRCOUNT => ff_WRCOUNT,
    
    RST => ff_RST,                   -- 1-bit input reset
    
    WRCLK => midclk,               -- 1-bit input write clock
    ALMOSTFULL => ff_af,
    FULL => ff_full,                 -- 1-bit output full
    WREN => ff_wren,                  -- 1-bit input write enable
    DI => ff_di, -- Input data, width defined by DATA_WIDTH parameter
    
    RDCLK => rdclk,               -- 1-bit input read clock
    ALMOSTEMPTY => almost_empty,
    EMPTY => empty,       -- 1-bit output empty
    RDEN => rden,         -- 1-bit input read enable
    DO => ff_do              -- Output data, width defined by DATA_WIDTH parameter
  );

  ff_RST <= ttc_rst;
  do     <= ff_do(63 downto 0);
  evnout <= ff_do(64+RO_EVN_WIDTH-1 downto 64);


end architecture;
