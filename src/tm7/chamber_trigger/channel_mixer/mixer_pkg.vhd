-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Multi-channel data mixer
---- File       : mixer_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: Package for components/defined types for the multi-channel
----              data mixer
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;
use work.dt_constants_pkg.all;
use work.dt_common_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package mixer_pkg is

  ---------------------------------------------------------------
  -- Package general constants
  ---------------------------------------------------------------
  constant NUM_DTTIME_BUFFERS : integer := 128;
  constant CH_IDX_WIDTH       : integer := getBusLines(NUM_DTTIME_BUFFERS);
  --
  constant LAST_AVAILABLE_WIRE_IDX : std_logic_vector(6 downto 0) 
                    := std_logic_vector(to_unsigned(NUM_CH_PER_LAYER - 1, 7));

  ---------------------------------------------------------------
  -- Data types
  ---------------------------------------------------------------
  --
  -- Type to define an array with one counter per memory subunit (buffer for 
  -- one channel.
  --
  type counters_arr_t is array (0 to NUM_DTTIME_BUFFERS - 1) 
                      of std_logic_vector(3 downto 0);

  --
  -- CAREFUL !!! These 2 types MUST be "downto" in order to be coordinated with
  -- "MIXER_CELL_RELATIVE_DISPLACEMENT" and "MIXER_CELL_HORIZONTAL_LAYOUTS"
  -- constants.
  --
  type sgn_channels_path_t is array (3 downto 0) of signed(7 downto 0);
  type channels_path_t is array (3 downto 0) of std_logic_vector(6 downto 0);
  type path_candidates_arr_t is array (0 to 7) of channels_path_t;
                      
  type MixCellHorizLayout_arr_t is array (0 to 7) of CellHorizLayout_arr_t;

  constant MIXER_CELL_HORIZONTAL_LAYOUTS : MixCellHorizLayout_arr_t := (
    (to_signed(-3, 3), to_signed(-2, 3), to_signed(-1, 3), to_signed(0, 3)), 
    (to_signed(-1, 3), to_signed(-2, 3), to_signed(-1, 3), to_signed(0, 3)), 
    (to_signed(-1, 3), to_signed( 0, 3), to_signed(-1, 3), to_signed(0, 3)), 
    (to_signed( 1, 3), to_signed( 0, 3), to_signed(-1, 3), to_signed(0, 3)),
    (to_signed(-1, 3), to_signed( 0, 3), to_signed( 1, 3), to_signed(0, 3)), 
    (to_signed( 1, 3), to_signed( 0, 3), to_signed( 1, 3), to_signed(0, 3)), 
    (to_signed( 1, 3), to_signed( 2, 3), to_signed( 1, 3), to_signed(0, 3)), 
    (to_signed( 3, 3), to_signed( 2, 3), to_signed( 1, 3), to_signed(0, 3))
  );

  type AllLayerHorizLayout_arr_t is array (0 to 3) of MixCellHorizLayout_arr_t;
  --
  -- For any mixer cell's combinations, this array stores the
  -- associated cell's displacement, relative to the cell's layer under 
  -- study measured in index-length units. This matrix takes into account the
  -- relative displacement amongst layers,
  --   
  --    
  --  Layer    
  --          -------------------------------------------
  --    3     |   0   |   1   |   2   |   3   |   4   |
  --          -------------------------------------------
  --    2         |   0   |   1   |   2   |   4   |
  --          -------------------------------------------
  --    1     |   0   |   1   |   2   |   3   |   4   |
  --          -------------------------------------------
  --    0         |   0   |   1   |   2   |   3   |   
  --              ---------------------------------------
  --                                 
  -- For example: 
  --
  --        Trajectory over cells => [2, 2, 1, 1] (bottom to top) 
  --        has relative index displacements:
  --
  --              From layer 0 =>  ( 0, 0, -1, -1)
  --              From layer 1 =>  ( 0, 0, -1, -1)
  --              From layer 2 =>  (+1, +1, 0,  0)
  --              From layer 3 =>  (+1, +1, 0,  0)
  --
  --
  constant MIXER_CELL_RELATIVE_DISPLACEMENT : AllLayerHorizLayout_arr_t := (
    -- For layer 0
    (
      (to_signed(-1, 3), to_signed(-1, 3), to_signed( 0, 3), to_signed(0, 3)), 
      (to_signed( 0, 3), to_signed(-1, 3), to_signed( 0, 3), to_signed(0, 3)), 
      (to_signed( 0, 3), to_signed( 0, 3), to_signed( 0, 3), to_signed(0, 3)), 
      (to_signed( 1, 3), to_signed( 0, 3), to_signed( 0, 3), to_signed(0, 3)),
      (to_signed( 0, 3), to_signed( 0, 3), to_signed( 1, 3), to_signed(0, 3)), 
      (to_signed( 1, 3), to_signed( 0, 3), to_signed( 1, 3), to_signed(0, 3)), 
      (to_signed( 1, 3), to_signed( 1, 3), to_signed( 1, 3), to_signed(0, 3)), 
      (to_signed( 2, 3), to_signed( 1, 3), to_signed( 1, 3), to_signed(0, 3))
    ),
    -- For layer 1
    (
      (to_signed(-1, 3), to_signed(-1, 3), to_signed(0, 3), to_signed( 0, 3)), 
      (to_signed( 0, 3), to_signed(-1, 3), to_signed(0, 3), to_signed( 0, 3)), 
      (to_signed( 0, 3), to_signed( 0, 3), to_signed(0, 3), to_signed( 0, 3)), 
      (to_signed( 1, 3), to_signed( 0, 3), to_signed(0, 3), to_signed( 0, 3)),
      (to_signed(-1, 3), to_signed(-1, 3), to_signed(0, 3), to_signed(-1, 3)), 
      (to_signed( 0, 3), to_signed(-1, 3), to_signed(0, 3), to_signed(-1, 3)), 
      (to_signed( 0, 3), to_signed( 0, 3), to_signed(0, 3), to_signed(-1, 3)), 
      (to_signed( 1, 3), to_signed( 0, 3), to_signed(0, 3), to_signed(-1, 3))
    ),
    -- For layer 2
    (
      (to_signed( 0, 3), to_signed(0, 3), to_signed( 1, 3), to_signed( 1, 3)), 
      (to_signed( 1, 3), to_signed(0, 3), to_signed( 1, 3), to_signed( 1, 3)), 
      (to_signed( 0, 3), to_signed(0, 3), to_signed( 0, 3), to_signed( 0, 3)), 
      (to_signed( 1, 3), to_signed(0, 3), to_signed( 0, 3), to_signed( 0, 3)),
      (to_signed( 0, 3), to_signed(0, 3), to_signed( 1, 3), to_signed( 0, 3)), 
      (to_signed( 1, 3), to_signed(0, 3), to_signed( 1, 3), to_signed( 0, 3)), 
      (to_signed( 0, 3), to_signed(0, 3), to_signed( 0, 3), to_signed(-1, 3)), 
      (to_signed( 1, 3), to_signed(0, 3), to_signed( 0, 3), to_signed(-1, 3))
    ),
    -- For layer 3
    (
      (to_signed(0, 3), to_signed( 0, 3), to_signed( 1, 3), to_signed( 1, 3)), 
      (to_signed(0, 3), to_signed(-1, 3), to_signed( 0, 3), to_signed( 0, 3)), 
      (to_signed(0, 3), to_signed( 0, 3), to_signed( 0, 3), to_signed( 0, 3)), 
      (to_signed(0, 3), to_signed(-1, 3), to_signed(-1, 3), to_signed(-1, 3)),
      (to_signed(0, 3), to_signed( 0, 3), to_signed( 1, 3), to_signed( 0, 3)), 
      (to_signed(0, 3), to_signed(-1, 3), to_signed( 0, 3), to_signed(-1, 3)), 
      (to_signed(0, 3), to_signed( 0, 3), to_signed( 0, 3), to_signed(-1, 3)), 
      (to_signed(0, 3), to_signed(-1, 3), to_signed(-1, 3), to_signed(-2, 3))
    )
  );

  type DTTimeLayMemIn_t is record
    channelIn_id     : std_logic_vector(CH_IDX_WIDTH - 1 downto 0);
    tdc_time         : std_logic_vector(TDCTime_t'length - 1 downto 0);
    ena_wr           : std_logic;
    --               
    channelOut_id    : std_logic_vector(CH_IDX_WIDTH - 1 downto 0);
    dataOut_idx      : std_logic_vector(3 downto 0);
    ena_rd           : std_logic;
    --               
    reset_overflow   : std_logic;
    expiration_cycle : std_logic;
  end record;

  type DTTimeLayMemOut_t is record
    tdc_time             : std_logic_vector(TDCTime_t'length - 1 downto 0);
    time_valid           : std_logic;
    storing_hit          : std_logic;
    buffer_out_full      : std_logic;
    buffer_out_empty     : std_logic;
    buffers_empty        : std_logic_vector(NUM_DTTIME_BUFFERS - 1 downto 0);
    buffers_overflow     : std_logic_vector(NUM_DTTIME_BUFFERS - 1 downto 0);
    channelOut_occupancy : std_logic_vector(3 downto 0);
  end record;

  type ChannelMixerIn_t is record
    rawHit            : DTRawHit_t;
    rawHitFFIn_empty  : std_logic;
    segCandFFOut_full : std_logic;
    -- Forces a reset of the overflow channels' memory flags
    reset_overflow    : std_logic;
    bx_counter        : std_logic_vector(11 downto 0);
  end record;

  type ChannelMixerOut_t is record
    segment_candidate : DTSegCandidate_t;
    rawHitsFFIn_rd    : std_logic;
    segCandFFOut_wr   : std_logic;
  end record;

  ---------------------------------------------------------------
  -- Functions
  ---------------------------------------------------------------
  function at_least_3_valid_time(time_valid : std_logic_vector(3 downto 0)) 
    return std_logic;

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component DTTIME_LAYER_MEMORY is
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    dtTimeLayMemIn  : in  DTTimeLayMemIn_t;
    dtTimeLayMemOut : out DTTimeLayMemOut_t
  );
  end component;

  component VALIDATE_PATH is
  port (
    path_candidate : in  sgn_channels_path_t;
    layer          : in  std_logic_vector(1 downto 0);
    ch_empty_lay0  : in  std_logic_vector(127 downto 0);
    ch_empty_lay1  : in  std_logic_vector(127 downto 0);
    ch_empty_lay2  : in  std_logic_vector(127 downto 0);
    ch_empty_lay3  : in  std_logic_vector(127 downto 0);
    valid          : out std_logic
  );
  end component;  

  component VALID_PATHS_ENCODER is
  port (
    clk             : in  std_logic;
    channel         : in  std_logic_vector(6 downto 0);
    layer           : in  std_logic_vector(1 downto 0);
    ch_empty_lay0   : in  std_logic_vector(127 downto 0);
    ch_empty_lay1   : in  std_logic_vector(127 downto 0);
    ch_empty_lay2   : in  std_logic_vector(127 downto 0);
    ch_empty_lay3   : in  std_logic_vector(127 downto 0);
    valid_paths     : out std_logic_vector(7 downto 0);
    path_candidates : out path_candidates_arr_t
  );
  end component;  

  component CURRENT_PATH_SELECTOR is
  port (
    clk                   : in  std_logic;
    rst                   : in  std_logic;
    next_path             : in  std_logic;
    channel               : in  std_logic_vector(6 downto 0);
    layer                 : in  std_logic_vector(1 downto 0);
    ch_empty_lay0         : in  std_logic_vector(127 downto 0);
    ch_empty_lay1         : in  std_logic_vector(127 downto 0);
    ch_empty_lay2         : in  std_logic_vector(127 downto 0);
    ch_empty_lay3         : in  std_logic_vector(127 downto 0);
    valid                 : out std_logic;
    current_path_idx      : out std_logic_vector(2 downto 0);
    current_path_channels : out channels_path_t
  );
  end component;  

  component CHANNEL_MIXER is
    port (
      clk        : in  std_logic;
      rst        : in  std_logic;
      chMixerIn  : in  ChannelMixerIn_t;
      chMixerOut : out ChannelMixerOut_t
    );
  end component;  

end package;

-------------------------------------------------------------------------------
---- Body
-------------------------------------------------------------------------------
package body mixer_pkg is

  function at_least_3_valid_time(time_valid : std_logic_vector(3 downto 0))
  return std_logic
  is
    variable valid : std_logic;
  begin
    case time_valid is
      when "0000" => valid := '0';
      when "0001" => valid := '0';
      when "0010" => valid := '0';
      when "0011" => valid := '0';
      when "0100" => valid := '0';
      when "0101" => valid := '0';
      when "0110" => valid := '0';
      when "0111" => valid := '1';
      when "1000" => valid := '0';
      when "1001" => valid := '0';
      when "1010" => valid := '0';
      when "1011" => valid := '1';
      when "1100" => valid := '0';
      when "1101" => valid := '1';
      when "1110" => valid := '1';
      when "1111" => valid := '1';
      when others => valid := 'X';
    end case;

    return valid;
  end function;

end package body;
