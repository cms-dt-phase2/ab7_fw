-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Buffers
---- File       : dtsegment_glue_fifo.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: Single element (one register) fifo to store a DT Segment 
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
--
--use work.commonfunc_pkg.all;
use work.dt_common_pkg.all;
use work.dt_buffers_pkg.all;
use work.memory_pkg.all;

entity DTSEGMENT_GLUE_FIFO is
  port (
    clk     : in  std_logic;
    rst     : in  std_logic;
    fifoIn  : in  DTSegmentFifoIn_t;
    fifoOut : out DTSegmentFifoOut_t
  );
end entity;

architecture Behavioural of DTSEGMENT_GLUE_FIFO is
  signal dOut : std_logic_vector(DTSEGMENT_SIZE - 1 downto 0);
  signal dIn  : std_logic_vector(DTSEGMENT_SIZE - 1 downto 0);

  signal empty : std_logic;
  signal full  : std_logic;
  
  signal statCountIn  : std_logic_vector(STAT_COUNT_LEN - 1 downto 0);
  signal statCountOut : std_logic_vector(STAT_COUNT_LEN - 1 downto 0);
begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  fifoOut.full              <= full;
  fifoOut.empty             <= empty;
  fifoOut.segment           <= dtSegment_from_slv(dOut);
  --
  fifoOut.stat.count_in  <= statCountIn;
  fifoOut.stat.count_out <= statCountOut;

  ---------------------------------------------------------------
  -- Auxiliary signals
  --
  dIn <= slv_from_dtSegment(fifoIn.segment);

  ---------------------------------------------------------------
  -- Counters for statistics
  --
  STATISTICS : process(clk, rst, fifoIn)
  begin
    if clk'event and clk = '1' then
      if rst = '1' then 
        statCountIn  <= (others => '0');
        statCountOut <= (others => '0');
      else
        if fifoIn.wrEna = '1' then
          statCountIn <= statCountIn  + '1';
        end if;

        if fifoIn.rdEna = '1' then
          statCountOut <= statCountOut  + '1';
        end if;

      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  GLUE_DUMMY_FIFO : DUMMY_FIFO_FFT2
  generic map (DATA_SIZE => DTSEGMENT_SIZE, NUM_ITEMS => 2)
  port map (
    clk     => clk,
    rst     => rst,
                     
    rdEna   => fifoIn.rdEna,
    wrEna   => fifoIn.wrEna,
    dataIn  => dIn,
    dataOut => dOut,
    empty   => empty,
    full    => full
                                  
  );
  
end architecture;
