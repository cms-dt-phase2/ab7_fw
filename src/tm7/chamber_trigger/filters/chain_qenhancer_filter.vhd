-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Quality-based segments filters chais
---- File       : chain_qenhancer_filter.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: A chain of QUALITY_ENHANCER_FILTER, joined by FIFO's, to 
----              get the filtration level required by the algorithm output.
----
----              This version uses three filtering stages.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
use work.dt_buffers_pkg.all;
use work.dt_common_pkg.all;
use work.filters_pkg.all;

entity CHAIN_QENHANCER_FILTER is
  port (
    clk                  : in  std_logic;
    rst                  : in  std_logic;
    control_regs         : in  DTControlRegs_t;
    chainQEnhanFilterIn  : in  SegmentFilterIn_t;
    chainQEnhanFilterOut : out SegmentFilterOut_t
  );
end entity;

architecture Behavioural of CHAIN_QENHANCER_FILTER is
  -- Components inter-connection signals
  signal middleFifoOut1    : DTSegmentFifoOut_t;
  signal qenFilterOut_stg1 : SegmentFilterOut_t;
  signal qenFilterOut_stg2 : SegmentFilterOut_t;

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  -- !!! CAREFUL !!! with the "stages" in charge of each signal.
  chainQEnhanFilterOut.fifoIn_rd  <= qenFilterOut_stg1.fifoIn_rd;
  --
  chainQEnhanFilterOut.segment    <= qenFilterOut_stg2.segment;
  chainQEnhanFilterOut.fifoOut_wr <= qenFilterOut_stg2.fifoOut_wr;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  QEN_FILT_STG1 : QUALITY_ENHANCER_FILTER 
  port map(
    clk             => clk,
    rst             => rst,
    expire_bx_num   => control_regs.qfilt_wait_expire_num_bx_stg1,
    qEnhanFilterIn  => (
          segment       => chainQEnhanFilterIn.segment,
          fifoIn_empty  => chainQEnhanFilterIn.fifoIn_empty,
          fifoOut_full  => middleFifoOut1.full,
          bx_counter    => chainQEnhanFilterIn.bx_counter
    ),
    qEnhanFilterOut => qenFilterOut_stg1
  );

  -- Switch between glue/normal fifo by commenting appropriate lines before rst
  -- Glue fifo is ok because the consumer is generally faster than the producer

  --CHAIN_QEN_FILT_FIFO1 : DTSEGMENT_FIFO port map (
  CHAIN_QEN_FILT_FIFO1 : DTSEGMENT_GLUE_FIFO port map (
    --clkRd   => clk,
    --clkWr   => clk,
    clk     => clk,
    rst     => rst,
    fifoIn  => (
          segment => qenFilterOut_stg1.segment, 
          wrEna   => qenFilterOut_stg1.fifoOut_wr, 
          rdEna   => qenFilterOut_stg2.fifoIn_rd
    ),
    fifoOut => middleFifoOut1
  );
  
  QEN_FILT_STG2 : QUALITY_ENHANCER_FILTER 
  port map (
    clk             => clk,
    rst             => rst,
    expire_bx_num   => control_regs.qfilt_wait_expire_num_bx_stg2,
    qEnhanFilterIn  => (
          segment       => middleFifoOut1.segment,
          fifoIn_empty  => middleFifoOut1.empty,
          fifoOut_full  => chainQEnhanFilterIn.fifoOut_full,
          bx_counter    => chainQEnhanFilterIn.bx_counter
    ),
    qEnhanFilterOut => qenFilterOut_stg2
  );
  
end architecture;
    
