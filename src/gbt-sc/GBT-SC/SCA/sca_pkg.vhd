--============================================================================--
--#######################   Module Information   #############################--
--============================================================================--
--
-- Company          : CERN (EP-ESE-BE)
-- Engineer         : Julian Mendez <julian.mendez@cern.ch>
--
-- Project Name     : GBT-SC module (IC and EC fields)
-- Module Name      : SCA_PKG
--
-- Language         : VHDL
--
-- Target Device    : Device agnostic
-- Tool version     : -
--
-- Version          : 1.0
--
-- Description      : SCA control - SCA Package (types and CRC func.)
--
-- Versions history : DATE      VERS.   AUTHOR      DESCRIPTION
--
--                    06/04/17  1.0     J. Mendez   First .vhd module definition
--
-- Add. Comments    : -
--                                                                              
--============================================================================--
--############################################################################--
--============================================================================--

-- IEEE VHDL standard library:
library IEEE;
use IEEE.std_logic_1164.all;

--============================================================================--
--#############################   Package   ##################################--
--============================================================================--
package SCA_PKG is

    -- Types & Subtypes
    subtype reg2    is std_logic_vector(1 downto 0);
    subtype reg8    is std_logic_vector(7 downto 0);
    subtype reg16   is std_logic_vector(15 downto 0);
    subtype reg32   is std_logic_vector(31 downto 0);

    type reg8_arr   is array(integer range <>) of reg8;
    type reg16_arr  is array(integer range <>) of reg16;
    type reg2_arr   is array(integer range <>) of reg2;
    type reg32_arr  is array(integer range <>) of reg32;

    -- Functions
    function reverse_v (
        a: in std_logic_vector)
    return std_logic_vector;

    function nextCRC16_D8 (
        Data: std_logic_vector(7 downto 0);
        crc:  std_logic_vector(15 downto 0))
    return std_logic_vector;

end SCA_PKG;

--============================================================================--
--##############################   Body   ####################################--
--============================================================================--
package body SCA_PKG is

    -- Vector reverse function
    function reverse_v (
        a: in std_logic_vector)
    return std_logic_vector is
    
        variable result: std_logic_vector(a'RANGE);
        alias aa: std_logic_vector(a'REVERSE_RANGE) is a;
      
    begin    
        for i in aa'RANGE loop
            result(i) := aa(i);
        end loop;
      
        return result;      
    end;
    
    -- CRC16 - 8bits data
    function nextCRC16_D8 (
        Data: std_logic_vector(7 downto 0);
        crc:  std_logic_vector(15 downto 0))
    return std_logic_vector is
    
        variable d:      std_logic_vector(7 downto 0);
        variable c:      std_logic_vector(15 downto 0);
        variable newcrc: std_logic_vector(15 downto 0);
        
    begin
    
        d := Data;
        c := crc;
        
        newcrc(0) := d(4) xor d(0) xor c(8) xor c(12);
        newcrc(1) := d(5) xor d(1) xor c(9) xor c(13);
        newcrc(2) := d(6) xor d(2) xor c(10) xor c(14);
        newcrc(3) := d(7) xor d(3) xor c(11) xor c(15);
        newcrc(4) := d(4) xor c(12);
        newcrc(5) := d(5) xor d(4) xor d(0) xor c(8) xor c(12) xor c(13);
        newcrc(6) := d(6) xor d(5) xor d(1) xor c(9) xor c(13) xor c(14);
        newcrc(7) := d(7) xor d(6) xor d(2) xor c(10) xor c(14) xor c(15);
        newcrc(8) := d(7) xor d(3) xor c(0) xor c(11) xor c(15);
        newcrc(9) := d(4) xor c(1) xor c(12);
        newcrc(10) := d(5) xor c(2) xor c(13);
        newcrc(11) := d(6) xor c(3) xor c(14);
        newcrc(12) := d(7) xor d(4) xor d(0) xor c(4) xor c(8) xor c(12) xor c(15);
        newcrc(13) := d(5) xor d(1) xor c(5) xor c(9) xor c(13);
        newcrc(14) := d(6) xor d(2) xor c(6) xor c(10) xor c(14);
        newcrc(15) := d(7) xor d(3) xor c(7) xor c(11) xor c(15);
        
        return newcrc;
        
    end nextCRC16_D8;

end SCA_PKG;