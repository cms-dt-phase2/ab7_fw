from time import sleep

def read_fifo(board):
	timeout = 10 # in sec
	single_wait = 0.01 # in sec
	timeout_in_counts = timeout/single_wait
	
	
	# wait for the fifo to become non-empty
	state = board.read("bril_histogram.readout.fifo_stat")
	fifo_empty = ((state >> 31) & 0x1)
	fifo_valid = ((state >> 30) & 0x1)
	fifo_words_cnt = ((state >> 0) & 0xfffffff)
	timeout_counter = 0
	while (fifo_empty or (not fifo_valid) or fifo_words_cnt == 0):
		if timeout_counter == timeout_in_counts:
			print("ERROR Reached fifo empty timeout, [empty,valid,words_cnt]: [", fifo_empty, ",", fifo_valid, ",", fifo_words_cnt, "]")
			return None
		else:
			sleep(single_wait)
			state = board.read("bril_histogram.readout.fifo_stat")
			fifo_empty = ((state >> 31) & 0x1)
			fifo_valid = ((state >> 30) & 0x1)
			fifo_words_cnt = ((state >> 0) & 0xfffffff)
			timeout_counter += 1
	
	# now read one word and get the data size
	data = [board.read("bril_histogram.readout.fifo_data")]
	if ((data[0] >> 24) & 0xff) != 0xff :
		print("ERROR header data is corrupted: ", hex(data[0]))
		print("State [empty,valid,words_cnt]: [", fifo_empty, ",", fifo_valid, ",", fifo_words_cnt, "]")
		return None
	total_number_of_words = (data[0] >> 0) & 0xFFFF
	
	# now wait for the fifo to get enough data
	fifo_words_cnt = board.read("bril_histogram.readout.fifo_stat.words_cnt")
	timeout_counter = 0
	while(fifo_words_cnt < total_number_of_words-1):
		if timeout_counter == timeout_in_counts:
			print("Reach timeout waiting for the full package, Current words counter: ", fifo_words_cnt)
			return None
		else:
			sleep(single_wait)
			fifo_words_cnt = board.read("bril_histogram.readout.fifo_stat.words_cnt")
			timeout_counter += 1

	# now read the data finally
	data.extend(board.readBlock("bril_histogram.readout.fifo_data", total_number_of_words-1))

	# return
	return data
