-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Clock utilities
---- File       : bit_clock_synch.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-03
-------------------------------------------------------------------------------
---- Description: Single signal synchronizer from a slow clock domain to a
----              fast one.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;

entity BIT_CLOCK_SYNCH is
  generic (FAST_REG_STEPS : integer := 2);
  port(
    slow_clk : in  std_logic;
    fast_clk : in  std_logic;
    bit_in   : in  std_logic;
    bit_out  : out std_logic
  );
end entity;

architecture RTL of BIT_CLOCK_SYNCH is
  signal bit_in_reg  : std_logic;
  signal bit_out_reg : std_logic_vector(FAST_REG_STEPS - 1 downto 0);

begin
  bit_out <= bit_out_reg(FAST_REG_STEPS - 1);
  
  SLOW_PROC : process(slow_clk)
  begin
    if slow_clk'event and slow_clk = '1' then
      bit_in_reg <= bit_in;
    end if;
  end process;

  FAST_PROC : process(fast_clk)
  begin
    if fast_clk'event and fast_clk = '1' then
      bit_out_reg <= bit_out_reg(FAST_REG_STEPS - 2 downto 0) & bit_in_reg;
    end if;
  end process;
  
end architecture;
