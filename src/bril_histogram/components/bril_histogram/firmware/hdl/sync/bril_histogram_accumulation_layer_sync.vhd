----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_accumulation_layer_sync - rtl
-- Description: 
--          Performs cluster/stub/muon accumulation
--          Generates error handlingblock
--          And histogramming wrapper
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- 
use work.bril_histogram_package.all;

entity bril_histogram_accumulation_layer_sync is
Generic (
    -- identification of the histogram
    HISTOGRAM_TYPE                  : natural;
    HISTOGRAM_ID                    : natural;
    -- memory
    GENERATE_FIFO                   : boolean;
    -- histogram parameters
    NUMBER_OF_BINS                  : natural;
    COUNTER_WIDTH                   : natural;
    INCREMENT_WIDTH                 : natural;
    -- orbit and bx counter width
    ORBIT_BX_COUNTER_WIDTH          : natural;
    -- accumulation parameters
    NUMBER_OF_UNITS                          : natural; -- number of units (modules, chips) connected to the histogrammer
    UNIT_INCREMENT_WIDTH                     : natural; -- width of the data bus per ech unit
    NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE   : natural; -- defines the complexity of the accumualtion algorithm
    UNIT_ERROR_COUNTER_WIDTH                 : natural; -- width of the error counter
    STORE_ERRORS                             : boolean
);
Port(
    -- core signals
    memory_reset_i      : in std_logic; -- reset the memory, has to be released at least 4 clock cycles earlier
    reset_i             : in std_logic; -- sync reset
    clk_i               : in std_logic; -- counting clock, typ. 40mhz
    -- tcds data
    lhc_fill_i          : in std_logic_vector(31 downto 0);
    cms_run_i           : in std_logic_vector(31 downto 0);
    lumi_section_i      : in std_logic_vector(31 downto 0);
    lumi_nibble_i       : in std_logic_vector(31 downto 0); 
    -- SYNC MODE - tcds signals
    new_histogram_i     : in std_logic; -- start outputting data (orbit reset)
    new_iteration_i     : in std_logic; -- resetting the bin counter (if number of bins is 3564 corresponding to 3564bx - this signal is the bx reset)
    -- data input interface (used both in sync and async mode)
    increment_i         : in std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0);
    error_i             : in std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    mask_i              : in std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    -- fifo interface (optional)
    fifo_rd_clk_i       : in std_logic := '0';
    fifo_rd_en_i        : in std_logic := '0';
    fifo_empty_o        : out std_logic;
    fifo_words_cnt_o    : out std_logic_vector(31 downto 0);
    --
    data_o              : out std_logic_vector(31 downto 0); -- output of the memory
    data_valid_o        : out std_logic -- write enable
);
end bril_histogram_accumulation_layer_sync;

architecture rtl of bril_histogram_accumulation_layer_sync is

    -- accumulation delay
    constant ACCUMULATION_DELAY         : natural := logNceil(NUMBER_OF_UNITS, NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE) + 1; -- +1 stands for the delay of the increment checker
    
    -- internally buffered input signals
    signal iteration_id_internal        : std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    signal bin_id_internal              : std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);

    -- output
    signal increment_from_accumualtor   : std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0);
   
    -- muxed output
    signal checked_increment            : std_logic_vector(INCREMENT_WIDTH-1 downto 0);
    signal increment_overflow           : std_logic;
    
    -- delayed signals
    signal reset_delayed                : std_logic_vector(ACCUMULATION_DELAY-1 downto 0);
    signal new_histogram_delayed        : std_logic_vector(ACCUMULATION_DELAY-1 downto 0);
    signal new_iteration_delayed        : std_logic_vector(ACCUMULATION_DELAY-1 downto 0);
    signal valid_delayed                : std_logic_vector(ACCUMULATION_DELAY-1 downto 0);
    signal lhc_fill_delayed             : std_logic_vector(31 downto 0);
    signal cms_run_delayed              : std_logic_vector(31 downto 0);
    signal lumi_section_delayed         : std_logic_vector(31 downto 0);
    signal lumi_nibble_delayed          : std_logic_vector(31 downto 0);
    signal increment_overflow_delayed   : std_logic;
    
    -- error block parameters
    constant ERROR_BLOCK_PARAMETERS         : error_block_parameters_type := get_error_block_parameters(STORE_ERRORS, ORBIT_BX_COUNTER_WIDTH, NUMBER_OF_UNITS,UNIT_ERROR_COUNTER_WIDTH);

    -- and signals
    signal err_bl_processed_mask            : std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    signal err_bl_n_msk_err_words           : std_logic_vector(15 downto 0);
    signal err_bl_err_word                  : std_logic_vector(31 downto 0);
    signal err_bl_rd_en                     : std_logic;
    
    
begin

    -- generate counter for the error block
    process(clk_i)
    begin
        if rising_edge(clk_i) then
            if reset_i = '1' then
                iteration_id_internal <= (others => '0');
                bin_id_internal <= (others => '0');  
            else 
                if new_iteration_i = '1' then
                    -- reset bin counter
                    bin_id_internal <= (others => '0');
                    -- assign iteration counter
                    if new_histogram_i = '1' then
                        iteration_id_internal <= (others => '0');
                    else
                        iteration_id_internal <= iteration_id_internal + 1;
                    end if;
                else
                    -- increment bin counter
                    bin_id_internal <= bin_id_internal + 1;
                end if;
            end if;  
        end if;
    end process;
    
    -- error block
    bril_histogram_error_block_inst: entity work.bril_histogram_error_block
    generic map (
        -- 
        ORBIT_BX_COUNTER_WIDTH      => ORBIT_BX_COUNTER_WIDTH,
        NUMBER_OF_UNITS             => NUMBER_OF_UNITS,
        UNIT_ERROR_COUNTER_WIDTH    => UNIT_ERROR_COUNTER_WIDTH,
        STORE_ERRORS                => STORE_ERRORS
    )
    port map ( 
        -- basic
        memory_reset_i              => memory_reset_i,
        reset_i                     => reset_i,
        clk_i                       => clk_i,
        -- tcds of course
        new_histogram_i             => new_histogram_i,
        iteration_id_i              => iteration_id_internal,
        bin_id_i                    => bin_id_internal,
        -- input from the outside
        unit_err_i                  => error_i,
        ext_mask_i                  => mask_i,
        -- output autoamatic mask
        auto_mask_o                 => err_bl_processed_mask,
        -- interface with the streamer
        n_msk_err_words_o           => err_bl_n_msk_err_words,                   
        err_word_o                  => err_bl_err_word,     
        rd_en_i                     => err_bl_rd_en
    );

    -- generate accimulator
    accumulator_inst: entity work.bril_histogram_recursive_accumulator
    generic map (
        NUMBER_OF_UNITS                     => NUMBER_OF_UNITS,
        UNIT_INCREMENT_WIDTH                => UNIT_INCREMENT_WIDTH,
        NUMBER_OF_DAUGHTERS_PER_STAGE       => NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE,
        -- important for the timing (dont touch)
        TOTAL_NUMBER_OF_LAYERS              => logNceil(NUMBER_OF_UNITS, NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE)
    )
    port map (
        --
        clk_i                       => clk_i,
        --
        increment_i                 => increment_i,
        mask_i                      => err_bl_processed_mask,
        -- we have to fill only part of the vector because MAX_NUMBER_OF_UNITS_PER_DAUGHTER can be 5, wheres this particular daighter may have only 4 units
        total_o                     => increment_from_accumualtor
    );
    
    -- outputing (this is one clock cycles ahead histogram, thats why we are using reset_delayed(1), new_histogram_delayed(1))
    process(clk_i) 
    begin
        if rising_edge(clk_i) then
            if reset_delayed(1) = '1' then
                checked_increment <= (others => '0');
                increment_overflow <= '0';
                increment_overflow_delayed <= '0';
            else
                -- reset the overflow for a new histo (and also store the one for the previous histogram for readout)
                if new_histogram_delayed(1) = '1' then
                    -- also make sure to note the overflow of this bx (note that here we output increment_overflow_delayed)
                    if increment_from_accumualtor > 2**INCREMENT_WIDTH-1 then
                        checked_increment <= (others => '1');
                        increment_overflow_delayed <= '1';
                    else
                        checked_increment <= std_logic_vector(resize(unsigned(increment_from_accumualtor), INCREMENT_WIDTH));
                        increment_overflow_delayed <= increment_overflow;
                    end if;
                    -- reset
                    increment_overflow <= '0';
                else -- normal bx
                    -- in case more than 1
                    if increment_from_accumualtor > 2**INCREMENT_WIDTH-1 then
                        checked_increment <= (others => '1');
                        increment_overflow <= '1';
                    else
                        checked_increment <= std_logic_vector(resize(unsigned(increment_from_accumualtor), INCREMENT_WIDTH));
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- delaying the rest of the variables
    process(clk_i)
    begin
        if rising_edge(clk_i) then
            -- handled immediately
            reset_delayed <= reset_i & reset_delayed(ACCUMULATION_DELAY-1 downto 1);
            new_histogram_delayed <= new_histogram_i & new_histogram_delayed(ACCUMULATION_DELAY-1 downto 1);
            new_iteration_delayed <= new_iteration_i & new_iteration_delayed(ACCUMULATION_DELAY-1 downto 1);
            -- 
            if reset_i = '1' then
                --
                lhc_fill_delayed            <= (others => '0');
                cms_run_delayed             <= (others => '0');
                lumi_section_delayed        <= (others => '0');
                lumi_nibble_delayed         <= (others => '0');
            else
                -- hist params
                if (new_histogram_i = '1') then
                    lhc_fill_delayed            <= lhc_fill_i;
                    cms_run_delayed             <= cms_run_i;
                    lumi_section_delayed        <= lumi_section_i;
                    lumi_nibble_delayed         <= lumi_nibble_i;
                end if;
            end if;
        end if;
    end process;

    -- histgoram itself (sync mode)
    bril_histogram_wrapper_sync_inst: entity work.bril_histogram_wrapper_sync
    generic map (
        -- identification of the histogram
        HISTOGRAM_TYPE                  => HISTOGRAM_TYPE,
        HISTOGRAM_ID                    => HISTOGRAM_ID,
        -- memory
        GENERATE_FIFO                   => GENERATE_FIFO,
        -- histogram parameters
        NUMBER_OF_BINS                  => NUMBER_OF_BINS,
        COUNTER_WIDTH                   => COUNTER_WIDTH,
        INCREMENT_WIDTH                 => INCREMENT_WIDTH,
        -- error parameters
        NUMBER_OF_UNITS                 => NUMBER_OF_UNITS,
        NUMBER_OF_WORDS_PER_ERROR       => ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD,
        MAX_NUMBER_OF_MASKERROR_WORDS   => ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD * ERROR_BLOCK_PARAMETERS.MAX_MASKERROR_WORDS
    )
    port map(
        -- core signals
        memory_reset_i                  => memory_reset_i,
        reset_i                         => reset_delayed(0),
        clk_i                           => clk_i,
        -- tcds signals
        new_histogram_i                 => new_histogram_delayed(0),
        new_iteration_i                 => new_iteration_delayed(0),
        -- tcds data
        lhc_fill_i                      => lhc_fill_delayed,
        cms_run_i                       => cms_run_delayed,
        lumi_section_i                  => lumi_section_delayed,
        lumi_nibble_i                   => lumi_nibble_delayed,
        -- counter signals  
        increment_overflow_i            => increment_overflow_delayed,
        increment_i                     => checked_increment,
        -- erorr block
        err_bl_n_msk_err_words_i        => err_bl_n_msk_err_words,  
        err_bl_err_word_i               => err_bl_err_word,         
        err_bl_rd_en_o                  => err_bl_rd_en,           
        -- fifo interface (optional)
        fifo_rd_clk_i                   => fifo_rd_clk_i,   
        fifo_rd_en_i                    => fifo_rd_en_i,    
        fifo_empty_o                    => fifo_empty_o,    
        fifo_words_cnt_o                => fifo_words_cnt_o,
        --
        data_o                          => data_o,
        data_valid_o                    => data_valid_o
    );

end rtl;
