-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Analyzer
---- File       : segment_fitter.vhd
----
-------------------------------------------------------------------------------
---- Description: Calculates the T0 value that corresponds to a group of 4 hits,
----              of which all or only 3 of them may be valid, as well as
----              the complete set of geometric parameters.
----              The T0, pos and slope are calculated by linerarly combining 
----              the input timestamps with a set of pre-calculated parameters
----              according to the analytical fit.
----              The bx_id and chi2 are calculated from these on a second stage.
----
----              Most of the signal widths in this file are calculated by an
----              an external python script
----
----              It takes 6 clock cycles.
-------------------------------------------------------------------------------
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.dt_common_pkg.all;
use work.dt_constants_pkg.all; --for MAX_DRIFT_TIME_NS
use work.analyzer_pkg.all;
use work.registers_pkg.all;
use work.minmax.all;
use work.bo2.all;
use work.BXArithmetic.all;

entity segment_fitter is
  port(
    clk          : in  std_logic;
    rst          : in  std_logic;
    enable       : in  std_logic;
    control      : in  DTControlRegs_t;
    segFitIn     : in  segFitIn_t;
    segFitOut    : out segFitOut_t
	);
end entity;

architecture Behavioural of segment_fitter is

  type sgn10_arr_t is array (3 downto 0) of signed(9 downto 0);
  type sgn26_arr_t is array (3 downto 0) of signed(25 downto 0);

  ---------------------------------------
  -- Registers
  --

  type reg_type is record
    c1_segFitIn     : segFitIn_t;
    c1_numer_t0     : signed(16 downto 0);
    c1_numer_pos    : signed(15 downto 0);
    c1_numer_slope  : signed(12 downto 0);

    c2_segFitIn     : segFitIn_t;
    c2_t0           : signed(17 downto 0);
    c2_pos          : signed(10 downto 0);
    c2_slope        : signed(14 downto 0);
    c2_slope_xhh    : signed( 9 downto 0);
    
    c3_segFitIn     : segFitIn_t;
    c3_segm_valid   : std_logic;
    c3_bx_time      : TDCTime_t;
    c3_pos          : signed(10 downto 0);
    c3_slope        : signed(14 downto 0);
    c3_slope_xhh    : signed( 9 downto 0);
    c3_drift_dists  : sgn10_arr_t;

    c4_segFitIn     : segFitIn_t;
    c4_segm_valid   : std_logic;
    c4_bx_time      : TDCTime_t;
    c4_pos          : signed(10 downto 0);
    c4_slope        : signed(14 downto 0);
    c4_bx_id        : std_logic_vector(11 downto 0);
    c4_delta_hit_x  : DeltaHit_arr_t;
    c4_res          : DeltaHit_arr_t;

    c5_segFitIn     : segFitIn_t;
    c5_segm_valid   : std_logic;
    c5_bx_time      : TDCTime_t;
    c5_pos          : SignedDistance_t;
    c5_slope        : signed(14 downto 0);
    c5_bx_id        : std_logic_vector(11 downto 0);
    c5_delta_hit_x  : DeltaHit_arr_t;
    c5_res2         : sgn26_arr_t;

    c6_segFitIn     : segFitIn_t;
    c6_segm_valid   : std_logic;
    c6_bx_time      : TDCTime_t;
    c6_pos          : SignedDistance_t;
    c6_slope        : signed(14 downto 0);
    c6_bx_id        : std_logic_vector(11 downto 0);
    c6_delta_hit_x  : DeltaHit_arr_t;
    c6_chi2         : unsigned(DTSegment_t.chi_square'high downto 0);
  end record;

  constant regDefault : reg_type := (
    c1_segFitIn     => segFitIn_NULL,
    c1_numer_t0     => (others => '0'),
    c1_numer_pos    => (others => '0'),
    c1_numer_slope  => (others => '0'),
    
    c2_segFitIn     => segFitIn_NULL,
    c2_t0           => (others => '0'),
    c2_pos          => (others => '0'),
    c2_slope        => (others => '0'),
    c2_slope_xhh    => (others => '0'),
    
    c3_segFitIn     => segFitIn_NULL,
    c3_segm_valid   => '0',
    c3_bx_time      => (others => '0'),
    c3_pos          => (others => '0'),
    c3_slope        => (others => '0'),
    c3_slope_xhh    => (others => '0'),
    c3_drift_dists  => (others => (others => '0')),
    
    c4_segFitIn     => segFitIn_NULL,
    c4_segm_valid   => '0',
    c4_bx_time      => (others => '0'),
    c4_pos          => (others => '0'),
    c4_slope        => (others => '0'),
    c4_bx_id        => (others => '0'),
    c4_delta_hit_x  => (others => (others => '0')),
    c4_res          => (others => (others => '0')),

    c5_segFitIn     => segFitIn_NULL,
    c5_segm_valid   => '0',
    c5_bx_time      => (others => '0'),
    c5_pos          => (others => '0'),
    c5_slope        => (others => '0'),
    c5_bx_id        => (others => '0'),
    c5_delta_hit_x  => (others => (others => '0')),
    c5_res2         => (others => (others => '0')),

    c6_segFitIn     => segFitIn_NULL,
    c6_segm_valid   => '0',
    c6_bx_time      => (others => '0'),
    c6_pos          => (others => '0'),
    c6_slope        => (others => '0'),
    c6_bx_id        => (others => '0'),
    c6_delta_hit_x  => (others => (others => '0')),
    c6_chi2         => (others => '0')
  );
  
  signal reg, nextReg : reg_type := regDefault;

  -- This signal needs to have a keep attribute to prevent DSP absorption
  signal c5_res2 : sgn26_arr_t;
  attribute keep : string;
  attribute keep of c5_res2 : signal is "true";  

  -- Used to be able to index the result of an algebraic operation, like doing
  -- the shift right after a multiplication without defining an intermediate
  -- signal
  function noop (X : signed) return signed is
  begin
    return X;
  end function;

begin

  -- Outputs from last clock cycle
  segFitOut.latcomb_valid          <= reg.c6_segm_valid;
  segFitOut.laterality_combination <= reg.c6_segFitIn.latcomb;
  segFitOut.bx_time                <= reg.c6_bx_time;
  segFitOut.phi_tangent_x4096      <= reg.c6_slope ;
  segFitOut.horizontal_position    <= reg.c6_pos ;
  segFitOut.delta_hit_x            <= reg.c6_delta_hit_x;
  segFitOut.chi_square             <= reg.c6_chi2;
  segFitOut.bx_id                  <= reg.c6_bx_id;
  -- at the time this module was written, this signal was not used anywhere
  segFitOut.bx_id_prec             <= (others => '0');


  ---------------------------------------------------------------
  -- Combinational logic
  --
  process(reg, segFitIn)
    -- temp vars for clk cycle #3
    variable valid : std_logic_vector(3 downto 0);
    variable drift_time : signed(17 downto 0);
    
    -- temp vars for clk cycle #4
    variable xdist : signed(12 downto 0);
    
    -- temp vars for clk cycle #6
    variable chi2 : unsigned(DTSegment_t.chi_square'length-1 downto 0);
  begin
    -- Default values.
    nextReg <= reg;
    
    ---------------------------
    -- clock cycle 1
    ---------------------------
    nextReg.c1_segFitIn  <= segFitIn ;
    
    -- In this clock cycle, we multiply each input timestamp by the coefficients and store the sum of the 
    -- partial results. This is done for the calculations of:
    -- t0, pos and slope (slope numerator is later use for two different slopes).
    
    -- we allow the numerator of t0 to increase in size by 2 bits to ensure no overflows in this sum,
    -- because it is relatively small (it has no additional precission) and also because it will be
    -- used to verify legal (physical drift times)
    nextReg.c1_numer_t0 <=
      resize(signed('0'&segFitIn.reduced_times(0))*decode_fit_constants(segFitIn.constants).t0.coeff(0),17) + 
      resize(signed('0'&segFitIn.reduced_times(1))*decode_fit_constants(segFitIn.constants).t0.coeff(1),17) + 
      resize(signed('0'&segFitIn.reduced_times(2))*decode_fit_constants(segFitIn.constants).t0.coeff(2),17) + 
      resize(signed('0'&segFitIn.reduced_times(3))*decode_fit_constants(segFitIn.constants).t0.coeff(3),17) ; 
    
    -- for pos and slope, we resize the result back down to the maximum size that is expected granted
    -- that the drift times are physical (0 < drift time < 387 ns) as calculated in the auxiliary python
    nextReg.c1_numer_pos <= resize(
      resize(signed('0'&segFitIn.reduced_times(0))*decode_fit_constants(segFitIn.constants).pos.coeff(0),18) + 
      resize(signed('0'&segFitIn.reduced_times(1))*decode_fit_constants(segFitIn.constants).pos.coeff(1),18) + 
      resize(signed('0'&segFitIn.reduced_times(2))*decode_fit_constants(segFitIn.constants).pos.coeff(2),18) + 
      resize(signed('0'&segFitIn.reduced_times(3))*decode_fit_constants(segFitIn.constants).pos.coeff(3),18)
      ,  16 );

    nextReg.c1_numer_slope <= resize(
      resize(signed('0'&segFitIn.reduced_times(0))*decode_fit_constants(segFitIn.constants).slope.coeff(0),17) + 
      resize(signed('0'&segFitIn.reduced_times(1))*decode_fit_constants(segFitIn.constants).slope.coeff(1),17) + 
      resize(signed('0'&segFitIn.reduced_times(2))*decode_fit_constants(segFitIn.constants).slope.coeff(2),17) + 
      resize(signed('0'&segFitIn.reduced_times(3))*decode_fit_constants(segFitIn.constants).slope.coeff(3),17)
      , 13 );

    ---------------------------
    -- clock cycle 2
    ---------------------------
    nextReg.c2_segFitIn  <= reg.c1_segFitIn ;
    
    -- In this clock cycle, for t0, pos, slope and slope_xhh, we take the result from the previous step,
    -- add an additive constant, and the perform a multiplication followed by a shift right. The results
    -- are the desired values for those magnitudes
    -- Slope has two variants.
    --  - slope is the final result of the fit
    --  - slope_xhh is in units of distance(mm+4 bits)/half-cell-height. This is convenient for
    --    calculating the residuals of the fit. 

    nextReg.c2_t0 <= noop(
        ( 
          (reg.c1_numer_t0 & "" ) -- size 10 +1 +4 +2 +0 = 17
          + decode_fit_constants(reg.c1_segFitIn.constants).t0.add -- size 13
        ) * signed('0'&decode_fit_constants(reg.c1_segFitIn.constants).t0.mult) -- size 16 +1
      ) (33 downto 16); -- shift right 16

    -- same as before, for pos, slope and slope_xhh we reduce the size to the maximum possible sizes calculated
    -- in the python for legal (physical) drift times    
    nextReg.c2_pos <= resize(
        ( 
          (reg.c1_numer_pos & x"0" ) -- size 16 +4 = 20
          + decode_fit_constants(reg.c1_segFitIn.constants).pos.add -- size 19
        ) * signed('0'&decode_fit_constants(reg.c1_segFitIn.constants).pos.mult) -- size 16 +1
      , 32 ) (31 downto 21); -- shift right 21

    nextReg.c2_slope <= resize(
        ( 
          (reg.c1_numer_slope & x"000" ) -- size 13 +12 = 25
          + decode_fit_constants(reg.c1_segFitIn.constants).slope.add -- size 24
        ) * signed('0'&decode_fit_constants(reg.c1_segFitIn.constants).slope.mult) -- size 14 +1
      , 36 ) (35 downto 21); -- shift right 21

    nextReg.c2_slope_xhh <= resize(
        ( 
          (reg.c1_numer_slope & x"0" ) -- size 13 +4 = 17
          + decode_fit_constants(reg.c1_segFitIn.constants).slope_xhh.add -- size 16
        ) * signed('0'&decode_fit_constants(reg.c1_segFitIn.constants).slope_xhh.mult) -- size 13 +1
      , 28 ) (27 downto 18); -- shift right 18

    ---------------------------
    -- clock cycle 3
    ---------------------------    
    nextReg.c3_segFitIn     <= reg.c2_segFitIn    ;
    nextReg.c3_pos          <= reg.c2_pos         ;
    nextReg.c3_slope        <= reg.c2_slope       ;
    nextReg.c3_slope_xhh    <= reg.c2_slope_xhh   ;
    
    -- T0 was calculated on the reduced (10 bits) timestamp widths. Now it is restored to full
    -- width by adding back the coarse offset.
    nextReg.c3_bx_time <= std_logic_vector(
      unsigned(reg.c2_t0(TDCTIME_REDUCED_SIZE-1 downto 0)) + 
      (unsigned(reg.c2_segFitIn.coarse_offset) & "000000000") ); 
    
    
    -- With the input timestamps and the T0 calculated in previous cycle, drift times
    -- can be calculated for the valid hits.
    nextReg.c3_segm_valid <= '1';
    for i in 3 downto 0 loop 
      valid(i) := reg.c2_segFitIn.hits(i).dt_time.valid;
      drift_time := signed('0'&reg.c2_segFitIn.reduced_times(i)) - reg.c2_t0; -- 18 bits because of c2_t0;

      -- The drift times must be physical positive and less than MAX_DRIFT_TIME_NS
      if valid(i) = '1' and (drift_time < 0 or drift_time >= MAX_DRIFT_TIME_NS) then
        nextReg.c3_segm_valid <= '0';
      end if;
      
      -- The drift times are multiplied by the drift speed to get the drift distances
      -- These magic numbers (9, 445, 23, 13) are calculated by the auxiliary python script.
      nextReg.c3_drift_dists(i) <= resize(
          ( -- drift time just been verified to be < 387... so truncate is safe
            (drift_time(9 downto 0) & x"0" ) -- size 10 +4 = 14
            + to_signed(9,5) -- size 5
          ) * to_signed(445, 10) -- size 10
        , 23) -- it will be always positive, so could be converted to unsigned, but it's not worth the hassle
             (22 downto 13); -- shift right 13
    end loop;
    
    -- one additional filter can be applied this clock cycle: the maximum angle.
    if abs(signed(reg.c2_slope)) > signed('0' & control.tanphi_x4096_threshold) then
      nextReg.c3_segm_valid <= '0';
    end if;

    ---------------------------
    -- clock cycle 4
    ---------------------------
    nextReg.c4_segFitIn     <= reg.c3_segFitIn ;
    nextReg.c4_bx_time      <= reg.c3_bx_time     ;
    nextReg.c4_pos          <= reg.c3_pos         ;
    nextReg.c4_slope        <= reg.c3_slope       ;
    
    -- From T0 we divide by 25 ns to get the bx id. We want this number rounded
    -- because it is later used to check for matches between superlayers.
    -- The magic numbers in the operation below (13, 41943, 32, 20)
    -- are calculated by the auxiliary python script
    nextReg.c4_bx_id <= std_logic_vector( noop(
      ( signed('0' & reg.c3_bx_time ) -- 18+1 = 19 bits
        + to_signed(13, 19) )
      * to_signed(41943, 17) -- round(2**20 / 25), 17 bits
      )(31 downto 20) ); -- always positive, no need to resize to 32 first
    
    -- This filter makes sure that null laterality combinations from the encoder file
    -- always result in invalid segments. It is done in this clock cycles because no
    -- other filter is applied this clock cycle, the segm_valid bit would otherwise
    -- just be pipelined.
    if reg.c3_segFitIn.latcomb /= "0000" then
      nextReg.c4_segm_valid   <= reg.c3_segm_valid  ;
    else 
      nextReg.c4_segm_valid   <= '0' ;
    end if; 

    -- Taking the drift distances calculated in the previous clock cycle,
    -- we substract the real position of each hit and the fit's estimated
    -- position to get the residuals.
    -- The different summands are added in steps, so that we can store
    -- delta_hit_x value, which is used in the matcher to calculate the
    -- residuals of the correlated primitive. 
    for i in 3 downto 0 loop
      xdist := (
        resize(reg.c3_segFitIn.xwire_mm(i) & x"0",13) -- size 7 + 4 = 11 -> 13 
        - resize(reg.c3_pos, 13) ) ;-- size 11 -> 13
      if reg.c3_segFitIn.latcomb(i) = '1' then
        xdist := xdist + reg.c3_drift_dists(i); -- size 10
      else
        xdist := xdist - reg.c3_drift_dists(i);
      end if;
      
      -- If the hit is invalid, we store 0
      if reg.c3_segFitIn.hits(i).dt_time.valid = '1' then
        nextReg.c4_delta_hit_x(i) <= xdist;
      else
        nextReg.c4_delta_hit_x(i) <= (others => '0');
      end if;
      
      -- We take advantage of the slope_xhh to get the slope-related term
      -- by simply multiplying by the height, expressed in half-cells:
      -- layer 0, 1, 2, 3 --> -3, -1, +1, +3
      xdist := 
        ( xdist - -- size 13
        resize(to_signed(3-2*(3-i),3)* reg.c3_slope_xhh,13) ); -- size (3+10) --> 11

      -- If the hit is invalid, we store 0
      if reg.c3_segFitIn.hits(i).dt_time.valid = '1' then
        nextReg.c4_res(i) <= xdist;
      else
        nextReg.c4_res(i) <= (others => '0');
      end if;
      
    end loop;
    
    ---------------------------
    -- clock cycle 5
    ---------------------------
    nextReg.c5_segFitIn     <= reg.c4_segFitIn    ;
    nextReg.c5_bx_time      <= reg.c4_bx_time     ;
    nextReg.c5_slope        <= reg.c4_slope       ;
    nextReg.c5_delta_hit_x  <= reg.c4_delta_hit_x ;
    nextReg.c5_segm_valid   <= reg.c4_segm_valid  ;
    
    -- Overflows 3564 -> 0
    nextReg.c5_bx_id        <= BXplus(reg.c4_bx_id , to_unsigned(0,12) );

    -- Restore position to full scale by adding the coarse position
    nextReg.c5_pos          <= reg.c4_pos + reg.c4_segFitIn.coarse_pos ;
    
    -- Square the residuals. No truncation because full resolution is used.
    for i in 3 downto 0 loop
      c5_res2(i) <= reg.c4_res(i) * reg.c4_res(i) ;
    end loop;
    
    nextReg.c5_res2 <= c5_res2;

    ---------------------------
    -- clock cycle 6
    ---------------------------
    nextReg.c6_segFitIn     <= reg.c5_segFitIn    ;
    nextReg.c6_bx_time      <= reg.c5_bx_time     ;
    nextReg.c6_pos          <= reg.c5_pos         ;
    nextReg.c6_slope        <= reg.c5_slope       ;
    nextReg.c6_bx_id        <= reg.c5_bx_id       ;
    nextReg.c6_delta_hit_x  <= reg.c5_delta_hit_x ;

    -- The sum of residuals squared. It is truncated to the width defined for it
    chi2 := unsigned( noop(
        reg.c5_res2(0) +
        reg.c5_res2(1) +
        reg.c5_res2(2) +
        reg.c5_res2(3) 
      )(21 downto 0)) & "00";
    
    nextReg.c6_chi2 <= chi2;
    
    
    -- The last filter, acceptance depending on chi2 threshold
    if chi2 > ( unsigned(control.chisqr_threshold) & "0000" ) then 
      nextReg.c6_segm_valid   <= '0';
    else 
      nextReg.c6_segm_valid   <= reg.c5_segm_valid  ; 
    end if;

  end process;

  ---------------------------------------------------------------
  -- Sequential logic
  --
  SEC : process(clk, rst)
  begin
    if rising_edge(clk) then
      if rst = '1' then 
        reg <= regDefault;
      elsif enable = '1' then
        reg <= nextReg;
      end if;
    end if;
  end process;


end architecture;
