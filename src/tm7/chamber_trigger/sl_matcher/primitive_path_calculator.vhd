-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Primitive builder
---- File       : primitive_path_calculator.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-05
-------------------------------------------------------------------------------
---- Description: Calculates all muon's trajectory parameters starting from
----              main parameters of a valid primitive.
----              This is mainly a wrapper that joins togethers each element
----              in charge of every partial calculation.
----              Also adds the necessary pipes (shift registers) to keep 
----              synchronized all the data path along the processing.
----
----              Additionally it compares the absolute value of 'tanPhi' 
----              primitive parameter with certain programmable threshold, 
----              removing those segments with a value over the limit.
----              Instead of developing an extra filter, I have inserted this 
----              functionality here to save resources.
----
----              It takes 12 clock cycles.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;
use work.dt_common_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.primitive_calc_modules_pkg.all;
use work.registers_pkg.all;
use work.sl_matcher_pkg.all;
--
use work.path_param_calc_pkg.all;   -- To reuse "PATH_CALC_BX"

entity PRIMITIVE_PATH_CALCULATOR is
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    primPathCalcIn  : in  PrimitivePathCalculatorIn_t;
    primPathCalcOut : out PrimitivePathCalculatorOut_t
  );
end entity;

architecture Behavioural of PRIMITIVE_PATH_CALCULATOR is

  -- Inter-components signals 
  signal valid_segments       : std_logic_vector(1 downto 0);
  signal valid_segments_piped : std_logic_vector(1 downto 0);
  --
  signal slv_segments_piped_1 : std_logic_vector(DTSEGMENT_ARR_SIZE-1 downto 0); 
  signal slv_segments_piped   : std_logic_vector(DTSEGMENT_ARR_SIZE-1 downto 0);
  --
  signal bx_time_chamber       : std_logic_vector(16 downto 0);
  signal bx_time_chamber_piped : std_logic_vector(16 downto 0);
  --
  signal bx_id_chamber        : std_logic_vector(11 downto 0);
  signal bx_id_chamber_piped  : std_logic_vector(11 downto 0);
  --
  signal x_coor_chamber       : SignedDistance_t;
  signal x_coor_chamber_piped : std_logic_vector(SGN_DIST_SIZE - 1 downto 0);
  --
  signal tan_phi_chamber_x4096       : signed(14 downto 0);
  signal tan_phi_chamber_x4096_piped : std_logic_vector(14 downto 0);
  --
  signal chi_square_chamber : 
                      unsigned(DTSegment_t.chi_square'length-1 downto 0);

  -- Registers
  signal primitive           : DTPrimitive_t;
  signal primValidFFOut_wr   : std_logic := '0';
  signal pairSegmentFFIn_rd  : std_logic := '0';
  
  --Auxiliary signals
  signal primitive_out      : DTPrimitive_t;
  signal go_on_fifo         : std_logic := '0';
  signal enable             : std_logic := '0';
  signal both_seg_valid     : std_logic := '0';
  signal at_least_one_valid : std_logic := '0';
  signal paired_segments    : DTSegment_arr_t;

begin
  ---------------------------------------------------------------
  -- Outgoing signals 
  --
  primPathCalcOut.primitive          <= primitive;
  primPathCalcOut.primValidFFOut_wr  <= primValidFFOut_wr;
  primPathCalcOut.pairSegmentFFIn_rd <= pairSegmentFFIn_rd;
  
  ---------------------------------------------------------------
  -- Auxiliary signals 
  --
  both_seg_valid     <= and_bits_slv(valid_segments_piped);
  at_least_one_valid <= or_bits_slv(valid_segments_piped);
  paired_segments    <= dtSegmentsArr_from_slv( slv_segments_piped );

  ---------------------------------------------------------------
  -- Outgoing multiplexers
  -- 
  primitive_out.paired_segments(1) 
          <= dtSegmentsArr_from_slv( slv_segments_piped )(1) 
                      when valid_segments_piped(1) = '1' else 
              DT_SEGMENT_NULL;

  primitive_out.paired_segments(0) 
          <= dtSegmentsArr_from_slv( slv_segments_piped )(0) 
                      when valid_segments_piped(0) = '1' else 
              DT_SEGMENT_NULL;

  primitive_out.valid_segments <= valid_segments_piped;

  -- Position 
  primitive_out.horizontal_position_chamber 
    <= SignedDistance_t(x_coor_chamber_piped) when both_seg_valid = '1' 
          else
       paired_segments(1).horizontal_position when valid_segments_piped(1) = '1' 
          else
       paired_segments(0).horizontal_position when valid_segments_piped(0) = '1' 
          else
       (others => '0');

  primitive_out.phi_tangent_x4096_chamber
      <= signed(tan_phi_chamber_x4096_piped)  when both_seg_valid = '1'
            else
         paired_segments(1).phi_tangent_x4096 when valid_segments_piped(1) = '1'
            else
         paired_segments(0).phi_tangent_x4096 when valid_segments_piped(0) = '1' 
            else
         (others => '0');

  primitive_out.chi_square_chamber
      <= chi_square_chamber            when both_seg_valid = '1'          else
         paired_segments(1).chi_square when valid_segments_piped(1) = '1' else
         paired_segments(0).chi_square when valid_segments_piped(0) = '1' else
         (others => '0');

  primitive_out.bx_time_chamber
      <= bx_time_chamber_piped 
                when both_seg_valid = '1'           else
         paired_segments(1).main_params.bx_time
                when valid_segments_piped(1) = '1'  else
         paired_segments(0).main_params.bx_time 
                when valid_segments_piped(0) = '1'  else
         (others => '0');

  primitive_out.bx_id_chamber
      <= bx_id_chamber_piped      when both_seg_valid = '1'          else
         paired_segments(1).bx_id when valid_segments_piped(1) = '1' else
         paired_segments(0).bx_id when valid_segments_piped(0) = '1' else
         (others => '0');

  ---------------------------------------------------------------
  -- Combinational logic
  --
  enable <= (not primPathCalcIn.primValidFFOut_full) and go_on_fifo;
  --
  -- Because pipe-line should not be stopped, even when there are no data
  -- in the incoming FIFO, a INVALID value is inserted in the datapath so,
  -- at the end of the chain, no data is written in the outgoing fifo.
  --
  valid_segments <= primPathCalcIn.valid_segments 
                        when primPathCalcIn.pairSegmentFFIn_empty = '0' 
                        else (others => '0');
  
  ---------------------------------------------------------------
  -- Registration logic
  --
  process(clk, rst)
  begin
    if clk'event and clk = '1' then
      if rst = '1' then 
        primitive          <= DT_PRIMITIVE_NULL;
--         pairSegmentFFIn_rd <= '0';
        primValidFFOut_wr  <= '0';
      else
        primitive          <= primitive_out;
        primValidFFOut_wr  <= enable and at_least_one_valid;
--         pairSegmentFFIn_rd <= enable and 
--                               (not primPathCalcIn.pairSegmentFFIn_empty);
      end if;
    end if;
  end process;

  pairSegmentFFIn_rd <= enable and 
                        (not primPathCalcIn.pairSegmentFFIn_empty);
  
  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  ENA_DELAY : DELAYED_ENABLE generic map (NUM_BITS => 2)
  port map (
    clk    => clk,
    rst    => rst,
    enable => '1',
    go_on  => go_on_fifo
  );
  
  ---------------------------------------
  -- First stage
  ---------------------------------------
  --
  -- Pipeline from 1st. to the last stage
  PIPELINE_VALID_SEG : VECTOR_SHIFT_REGISTER 
  generic map (VECTOR_WIDTH => 2, SHIFT_STEPS => 11)
  port map (
    clk        => clk,
    enable     => enable,
    vector_in  => valid_segments,
    vector_out => valid_segments_piped
  );

  -- Pipeline from 1st. to 2nd. stage
  PIPELINE_PAIR_SEG_1 : VECTOR_SHIFT_REGISTER 
  generic map (VECTOR_WIDTH => DTSEGMENT_ARR_SIZE, SHIFT_STEPS => 3)
  port map (
    clk        => clk,
    enable     => enable,
    vector_in  => slv_from_DTSegmentsArr( primPathCalcIn.paired_segments ),
    vector_out => slv_segments_piped_1
  );

  -- This module takes 3 clock cycles.
  INST_CALC_TAN_POS_BX_TIME : CALC_PRIMITIVE_TAN_POS_BX_TIME port map (
    clk                   => clk, 
    rst                   => rst, 
    enable                => enable,
    paired_segments       => primPathCalcIn.paired_segments,
    sl_phi3_x_offset      => primPathCalcIn.sl_phi3_x_offset,
    bx_time_chamber       => bx_time_chamber,
    x_coor_chamber        => x_coor_chamber,
    tan_phi_chamber_x4096 => tan_phi_chamber_x4096
  );

  ---------------------------------------
  -- Second stage
  ---------------------------------------

  ------------------
  -- Pipelines from 2nd. to the last stage
  --
  PIPELINE_PAIR_SEG_2 : VECTOR_SHIFT_REGISTER 
  generic map (VECTOR_WIDTH => DTSEGMENT_ARR_SIZE, SHIFT_STEPS => 8)
  port map (
    clk        => clk,
    enable     => enable,
    vector_in  => slv_segments_piped_1,
    vector_out => slv_segments_piped
  );

  PIPELINE_BX_TIME : VECTOR_SHIFT_REGISTER 
  generic map (VECTOR_WIDTH => 17, SHIFT_STEPS => 8)
  port map (
    clk        => clk,
    enable     => enable,
    vector_in  => bx_time_chamber,
    vector_out => bx_time_chamber_piped
  );

  PIPELINE_X_COOR : VECTOR_SHIFT_REGISTER 
  generic map (VECTOR_WIDTH => x_coor_chamber'length, SHIFT_STEPS => 8)
  port map (
    clk        => clk,
    enable     => enable,
    vector_in  => std_logic_vector(x_coor_chamber),
    vector_out => x_coor_chamber_piped
  );

  PIPELINE_TAN_PHI : VECTOR_SHIFT_REGISTER 
  generic map (VECTOR_WIDTH => 15, SHIFT_STEPS => 8)
  port map (
    clk        => clk,
    enable     => enable,
    vector_in  => std_logic_vector(tan_phi_chamber_x4096),
    vector_out => tan_phi_chamber_x4096_piped
  );
  ------------------
  
  INST_PRIM_BX_ID : PATH_CALC_BX port map (
    clk        => clk,
    rst        => rst,
    enable     => enable,
    bx_time    => bx_time_chamber,
    bx_id      => bx_id_chamber,
    bx_id_prec => open
  );

  PIPELINE_BX_ID : VECTOR_SHIFT_REGISTER 
  generic map (VECTOR_WIDTH => 12, SHIFT_STEPS => 4)
  port map (
    clk        => clk,
    enable     => enable,
    vector_in  => bx_id_chamber,
    vector_out => bx_id_chamber_piped
  );

  INST_CALC_CHI_SQUARE : CALC_PRIMITIVE_CHI_SQUARE port map (
    clk                   => clk,
    rst                   => rst,
    enable                => enable,
    paired_segments       => dtSegmentsArr_from_slv( slv_segments_piped_1),
    x_coor_chamber        => x_coor_chamber,
    sl_phi3_x_offset      => primPathCalcIn.sl_phi3_x_offset,
    tan_phi_chamber_x4096 => tan_phi_chamber_x4096,
    chi_square_chamber    => chi_square_chamber
  );

end architecture;
