----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_error_block - rtl
-- Description: 
-- 
--          Handles errors from the connected units and does the respective masking
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_MISC.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- user macros to create fifo (device agnostic)
library xpm;
use xpm.vcomponents.all;

-- bril package 
use work.bril_histogram_package.all;

entity bril_histogram_error_block is
Generic (
    -- 
    ORBIT_BX_COUNTER_WIDTH      : natural;
    NUMBER_OF_UNITS             : natural; -- number of units (modules, chips) connected to the histogramm
    UNIT_ERROR_COUNTER_WIDTH    : natural; -- width of the per module error counter 
    STORE_ERRORS                : boolean
);
Port ( 
    -- basic
    memory_reset_i              : in std_logic; -- reset the memory, has to be released at least 4 clock cycles earlier
    reset_i                     : in std_logic;
    clk_i                       : in std_logic;
    -- tcds of course
    new_histogram_i             : in std_logic;
    valid_i                     : in std_logic := '1';
    iteration_id_i              : in std_logic_vector(ORBIT_BX_COUNTER_WIDTH - 1 downto 0);
    bin_id_i                    : in std_logic_vector(ORBIT_BX_COUNTER_WIDTH - 1 downto 0);
    -- input from the outside
    unit_err_i                  : in std_logic_vector(NUMBER_OF_UNITS-1 downto 0); -- errors received from units
    ext_mask_i                  : in std_logic_vector(NUMBER_OF_UNITS-1 downto 0); -- external mask for the modules (will be ored with the internally generated one)
    -- output autoamatic mask
    auto_mask_o                 : out std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    -- interface with the streamer
    n_msk_err_words_o           : out std_logic_vector(15 downto 0);                                      
    err_word_o                  : out std_logic_vector(31 downto 0);      
    rd_en_i                     : in std_logic
);
end bril_histogram_error_block;

architecture rtl of bril_histogram_error_block is

    -- define basic constants
    constant ERROR_BLOCK_PARAMETERS         : error_block_parameters_type := get_error_block_parameters(STORE_ERRORS, ORBIT_BX_COUNTER_WIDTH, NUMBER_OF_UNITS,UNIT_ERROR_COUNTER_WIDTH);
    
    --
    signal is_iter0_bin0                    : std_logic;
    
    -- internal mask
    signal internal_mask                    : std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    signal requested_mask                   : std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    signal unit_err_masked                  : std_logic_vector(NUMBER_OF_UNITS-1 downto 0);
    
    -- write and read pointers
    constant MAX_POINTER_VALUE              : natural := 1;
    signal wr_pointer                       : natural range 0 to MAX_POINTER_VALUE;
    signal rd_pointer                       : natural range 0 to MAX_POINTER_VALUE;
    type data_buffer_type is array(0 to MAX_POINTER_VALUE) of std_logic_vector(ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD*32 - 1 downto 0);
    signal data_buffer                      : data_buffer_type;
    
    -- per chip erro counter
    type error_counter_per_unit_type is array(NUMBER_OF_UNITS-1 downto 0) of std_logic_vector(UNIT_ERROR_COUNTER_WIDTH-1 downto 0);
    signal error_counter_per_unit          : error_counter_per_unit_type;
    
    -- fifo signals
    signal fifo_wr_en                  : std_logic;
    signal fifo_din                    : std_logic_vector(ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD*32 - 1 downto 0);
    signal fifo_din_reordered          : std_logic_vector(ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD*32 - 1 downto 0);
    signal fifo_empty                  : std_logic;
    signal fifo_full                   : std_logic;
    
    -- counter of the total words
    signal n_msk_err_words             : natural range 0 to ERROR_BLOCK_PARAMETERS.ERROR_FIFO_DEPTH;
    
    -- to avoid confusion have different variables for "no store errors" scenario
    signal no_store_buffer             : std_logic_vector(ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD*32 - 1 downto 0);
    signal no_store_word_pointer       : natural range 0 to ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD-1;

begin

    -- automask to the accumulator
    auto_mask_o <= unit_err_i or internal_mask; -- itneral mask is updated at the beginnign of each histogram

    -- internal error bus (masked) - to control generation of errors
    unit_err_masked <= unit_err_i and (not (internal_mask or requested_mask)); -- this has to be always up to date, so the modules stop rpoudcing the errors immidiately
    
    -- this part of the code increments the error counter
    gen_error_counters: for I in 0 to NUMBER_OF_UNITS-1 generate
        proc: process(clk_i)
        begin
            if rising_edge(clk_i) then
                if reset_i = '1' or new_histogram_i = '1' then
                    error_counter_per_unit(I) <= (others => '0');
                    requested_mask(I) <= '0';
                else
                    if valid_i = '1' and unit_err_i(I) = '1' then
                        if error_counter_per_unit(I) < 2**UNIT_ERROR_COUNTER_WIDTH-1 then
                            error_counter_per_unit(I) <= error_counter_per_unit(I) + 1;
                        else
                            requested_mask(I) <= '1';
                        end if;
                    end if;
                end if;
            end if;
        end process;
    end generate gen_error_counters;
    
    -- this part of the code instead operates with not delayed condoition - because it has to provides mask for the accimulator
    mask_generator: process(clk_i)
    begin
        if rising_edge(clk_i) then
            if reset_i = '1' then -- on reset assign external mask
                internal_mask <= ext_mask_i;
            elsif new_histogram_i = '1' then -- new histgoram on the next clock cycle                
                -- apply a new mask for the new clock histogram
                internal_mask <= ext_mask_i or requested_mask;
            end if;
        end if;
    end process;
    
    -- if no errors are stored - we just output the mask, to having a register is absolutely sufficient
    gen_only_mask: if NOT STORE_ERRORS generate
    
        -- number of words
        n_msk_err_words_o <= std_logic_vector(to_unsigned(ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD, n_msk_err_words_o'length)); -- always constant - only mask                                 
        err_word_o <= no_store_buffer(32*(no_store_word_pointer+1) - 1 downto 32*no_store_word_pointer);
        -- process to output the mask (it has n words)
        process(clk_i)
        begin
            if rising_edge(clk_i) then
                if reset_i = '1' then
                    no_store_word_pointer <= ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD-1;
                    no_store_buffer <= (others => '0');
                else
                    -- increment the pointer 
                    if rd_en_i = '1' then
                        if no_store_word_pointer > 0 then
                            no_store_word_pointer <= no_store_word_pointer - 1;
                        else
                            no_store_word_pointer <= ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD - 1;
                        end if;
                    end if;
                    -- storing the old buffer once new histogram received (internal mask is not yet changed)
                    if new_histogram_i = '1' then
                        no_store_word_pointer <= ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD - 1;
                        no_store_buffer <= append_zeros_right( (x"A" & internal_mask) , ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD*32); 
                    end if;
                end if;
            end if;
        end process;
        
    end generate gen_only_mask;
    
    -- this complicated structure including fifo we use if it was requesred to store errors
    gen_store: if STORE_ERRORS generate
        -- send errors to the fifo writer
        error_detector: process(clk_i)
        begin
            if rising_edge(clk_i) then
                if reset_i = '1' then
                    wr_pointer <= 0;
                    data_buffer <= (others => (others => '0'));
                else
                    -- store the data
                    if valid_i = '1' and or_reduce(unit_err_masked) = '1' then
                        -- form the data
                        data_buffer(wr_pointer) <= append_zeros_right( (x"5" & iteration_id_i & bin_id_i & unit_err_masked) , ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD*32);
                        -- increment the poointer
                        if wr_pointer < MAX_POINTER_VALUE then
                            wr_pointer <= wr_pointer + 1;
                        else
                            wr_pointer <= 0;
                        end if;
                    end if;
                end if;
            end if;
        end process;
        
        -- needed as a separate processor in order to store the mask (so in fact the buffer is needed) 
        fifo_writer: process(clk_i)
        begin
            if rising_edge(clk_i) then
                if reset_i = '1' then
                    rd_pointer <= 0;
                    fifo_wr_en <= '0';
                    fifo_din <= (others => '0');
                    n_msk_err_words <= 0;
                    n_msk_err_words_o <= (others => '0');
                    -- the next bin after reset is always iter0 bin0
                    is_iter0_bin0 <= '1';
                else
                    -- always reset
                    fifo_wr_en <= '0';
                    
                    -- when new histogram - store the mask
                    if is_iter0_bin0 = '1' then
                        -- here we are not incremeting the rd_pointer because its a mask (the rd pointer existst in fact to delay the data and allow to store this mask)
                        fifo_din <= append_zeros_right( (x"A" & internal_mask) , ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD*32);
                        fifo_wr_en <= '1';         
                        -- increment number of words
                        n_msk_err_words <= n_msk_err_words + 1;           
                    else
                        if rd_pointer /= wr_pointer then
                            -- send the data
                            fifo_din <= data_buffer(rd_pointer);
                            fifo_wr_en <= '1';
                            -- increment number of words
                            n_msk_err_words <= n_msk_err_words + 1;
                            -- an increment the pointer
                            if rd_pointer < MAX_POINTER_VALUE then
                                rd_pointer <= rd_pointer + 1;
                            else
                                rd_pointer <= 0;
                            end if;
                        end if;
                    end if;
                    
                    -- mark iter0 bin0 as the one next to the new_histogram_i
                    is_iter0_bin0 <= new_histogram_i;
                    
                    -- output the number of words and also reset the internal counter
                    if new_histogram_i = '1' then
                        -- below we first convert the n_msk_err_words into unsigned by appending a lot of zeros, and then multiply it by N_32BIT_WORDS_PER_ERROR_WORD - by shifting left by log2(N_32BIT_WORDS_PER_ERROR_WORD)
                        n_msk_err_words_o <= std_logic_vector(shift_left(to_unsigned(n_msk_err_words, n_msk_err_words_o'length), logNceil(ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD, 2)));
                        n_msk_err_words <= 0;
                    end if;
                end if;
            end if;
        end process;
        
        -- reordering words - because we are writing N*32, reading 32, it will read LSword, instead of MSword
        reordering: for I in 0 to ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD-1 generate
            fifo_din_reordered((ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD-1-I+1)*32 - 1 downto (ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD-1-I)*32) <= fifo_din((I+1)*32 - 1 downto I*32);
        end generate;
            
        -- intermediate fifo store the bx data
        -- Xilinx Parameterized Macro, version 2018.3
        error_fifo : xpm_fifo_sync
        generic map (
          DOUT_RESET_VALUE => "0",    -- String
          ECC_MODE => "no_ecc",       -- String
          FIFO_MEMORY_TYPE => "auto", -- String
          FIFO_READ_LATENCY => 0,     -- DECIMAL, has to be 0 for the FWFT
          FIFO_WRITE_DEPTH => ERROR_BLOCK_PARAMETERS.ERROR_FIFO_DEPTH,   -- DECIMAL, depth that we need
          FULL_RESET_VALUE => 0,      -- DECIMAL
          PROG_EMPTY_THRESH => 10,    -- DECIMAL, we will not use it anyways
          PROG_FULL_THRESH => 10,     -- DECIMAL, will also not use this flag
          RD_DATA_COUNT_WIDTH => logNceil(ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD*ERROR_BLOCK_PARAMETERS.ERROR_FIFO_DEPTH,2) + 1,   -- DECIMAL
          READ_DATA_WIDTH => 32,      -- DECIMAL
          READ_MODE => "fwft",        -- String, first word fall through
          USE_ADV_FEATURES => "1405",
          WAKEUP_TIME => 0,           -- DECIMAL
          WRITE_DATA_WIDTH => ERROR_BLOCK_PARAMETERS.N_32BIT_WORDS_PER_ERROR_WORD*32,     -- DECIMAL
          WR_DATA_COUNT_WIDTH => logNceil(ERROR_BLOCK_PARAMETERS.ERROR_FIFO_DEPTH,2)+1    -- DECIMAL
        )
        port map (
          -- read
          rd_en => rd_en_i,
          data_valid => open,    
          dout => err_word_o,
          empty => fifo_empty,
          full => fifo_full,
          rd_data_count => open,
          rd_rst_busy => open,
          -- write
          wr_clk => clk_i, 
          wr_en => fifo_wr_en,
          din => fifo_din_reordered, 
          wr_rst_busy => open,
          rst => memory_reset_i, -- wr clk
          -- not needed ports
          sleep => '0',
          injectsbiterr => '0',
          injectdbiterr => '0'
        );
    end generate gen_store;

end rtl;
