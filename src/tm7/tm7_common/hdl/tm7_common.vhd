--=================================================================================================--
-- Entity to read out the FPGA DNA identifier             
-- By Javier Sastre Alvaro
--=================================================================================================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

-- Custom libraries and packages:  
use work.ipbus.all;                
use work.ipbus_reg_types.all;      

entity tm7_common is
  Port (
    -- ipbus --  
    ipb_clk : IN STD_LOGIC;      
    ipb_rst : IN STD_LOGIC;     
    ipb_in  : IN ipb_wbus;      
    ipb_out : OUT ipb_rbus;
    
    clk40MHz   : in  std_logic;
    clk_locked : IN std_logic
  );
end tm7_common;

architecture structural of tm7_common is

  -- IPBUS REGISTER                                       
  constant N_STAT : integer := 4;                          
  constant N_CTRL : integer := 4;                        
  signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);   
  signal stat, csrd: ipb_reg_v(N_STAT-1 downto 0);        
  signal conf, csrq: ipb_reg_v(N_CTRL-1 downto 0);
  
  signal dna_s: std_logic_vector(63 downto 0);
  signal dna_start: std_logic;       

  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals declaration
  signal conf_dna_start, stat_dna_ready : std_logic := '0';

  signal stat_dna_lsb, stat_dna_msb : unsigned(31 downto 0) := (others => '0');

  -- End of automatically-generated VHDL code for register "breakout" signals declaration
  --------------------------------------------------------------------------------

begin

  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals assignment

  conf_dna_start            <= conf (16#00#)(0);
  stat(16#00#)(31 downto 0) <= dna_s(31 downto 0);
  stat(16#01#)(31 downto 0) <= dna_s(63 downto 32);
  stat(16#02#)(0)           <= stat_dna_ready;

  -- End of automatically-generated VHDL code for register "breakout" signals assignment
  --------------------------------------------------------------------------------

--------------------------------------
  -- IPbus register
  --------------------------------------
  ipbusreg: entity work.ipbus_syncreg_v
  generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
  port map(
    clk     => ipb_clk,
    rst     => ipb_rst,
    ipb_in  => ipb_in,
    ipb_out => ipb_out,
    slv_clk => clk40MHz,
    d       => stat,
    q       => conf,
    stb     => open
  );
  
  dna_start <= conf_dna_start or clk_locked;
  
  dna : entity work.dna               
  port map(                                         
    -- ipbus --                                     
    clk         => clk40MHz,                       
    start       => dna_start,                       
    dna         => dna_s,   
    ready       => stat_dna_ready 
                                                   
  );                                                
   
end structural;
