-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Registers
---- File       : registers_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-06
-------------------------------------------------------------------------------
---- Description: Package for register related components
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package registers_pkg is

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component DELAYED_ENABLE is
  generic (NUM_BITS : integer := 2);
  port(
    clk    : in  std_logic;
    rst    : in  std_logic;
    enable : in  std_logic;
    go_on  : out std_logic
  );
  end component;

  component BIT_SHIFT_REGISTER is
    generic (SHIFT_STEPS : integer := 4);
    port(
      clk     : in  std_logic;
      enable  : in  std_logic;
      bit_in  : in  std_logic;
      bit_out : out std_logic
    );
  end component;
  
  component VECTOR_SHIFT_REGISTER is
    generic (VECTOR_WIDTH : integer := 16; SHIFT_STEPS : integer := 4);
    port(
      clk        : in  std_logic;
      enable     : in  std_logic;
      vector_in  : in  std_logic_vector(VECTOR_WIDTH - 1 downto 0);
      vector_out : out std_logic_vector(VECTOR_WIDTH - 1 downto 0)
    );
  end component;
  
end package;
