----------------------------------------------------------------------------------
-- AB7 event builder for readout of the hits received from TDC
-- Alvaro Navarro, CIEMAT
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.bo2.all;
use work.minmax.all;
use work.TM7_readout_decl.all;
use work.top_decl.all; -- for LHC_BUNCH_COUNT
use work.mp7_ttc_decl.all;

entity dt_roeb is
  port(
    -- readout configuration (semi-static)
    latency                 : in unsigned( 11 downto 0 );
    window                  : in unsigned( 11 downto 0 );
    startpointer_cooldown   : in unsigned( 11 downto 0 );
    max_disorder            : in unsigned( 11 downto 0 );
    -- ttc inputs: ttc_clk clocked
    ttc_clk                 : in std_logic; -- TTC and input data
    ttc_rst                 : in std_logic; -- for all clocks
    l1a                     : in std_logic;
    evt_ctr                 : in std_logic_vector( RO_EVN_WIDTH-1 downto 0 );
    bunch_ctr               : in std_logic_vector( 11 downto 0 );
    -- input data
    input_clk               : in std_logic;
    input_data              : in std_logic_vector(31 downto 0);  -- valid & '0' & St & SL & Channel & BX & TDC
    -- intermediate clock for output fifo writing
    midclk                  : in std_logic;
    -- output fifo ports: rdclk clocked
    rdclk                   : in std_logic;
    empty                   : out std_logic;
    almost_empty            : out std_logic;
    rden                    : in std_logic;
    do                      : out std_logic_vector(63 downto 0);
    evnout                  : out std_logic_vector(RO_EVN_WIDTH-1 downto 0);
    ff_rdcount              : out std_logic_vector(12 downto 0);
    ff_wrcount              : out std_logic_vector(12 downto 0)
  );
end entity;

architecture Behavioral of dt_roeb is
  ---------------------------------------------------
  -- Non-Monotonic-Data Window Read instance generics
  ---------------------------------------------------
  -- For each event, the stored data is the fine TDC field (5 bits) + channel number (8 bits) --> 13 bits 
  constant DATA_WIDTH : positive := 18; -- plus the 12 bits of the bx, which is input separately

  -- Worst case: hits @ 240 MHz (= 6 hit/bx), need to store up to 1000 bx of latency = 6000, round down to 4096 (12 bits)
  constant NONMONOTO_MEMDEPTH : positive := 12;
  
  ------------------------------------------------
  -- Non-Monotonic-Data Window Read instance ports
  ------------------------------------------------
  -- inputs
  signal nmdwr_wr       : std_logic;
  signal nmdwr_bx_in    : std_logic_vector( 11 downto 0 );
  signal nmdwr_data_in  : std_logic_vector( DATA_WIDTH-1 downto 0 );
  -- outputs
  signal nmdwr_inevent  : boolean;
  signal nmdwr_valid    : boolean; -- marks pauses due to words being read but not needed (out of window due to non-monotonicity)
  signal nmdwr_evn      : std_logic_vector( RO_EVN_WIDTH-1 downto 0 );
  signal nmdwr_bx       : std_logic_vector( 11 downto 0 );
  signal nmdwr_data     : std_logic_vector( DATA_WIDTH-1 downto 0 );

  -- output fifo signals
  constant OUTFIFO_WIDTH : integer := 64 + RO_EVN_WIDTH;
  signal ff_RST, ff_af, ff_full, ff_wren : std_logic;
  signal ff_di, ff_do : std_logic_vector(OUTFIFO_WIDTH-1 downto 0) := (others => '0');
    
begin
  -----------------------------------------
  -- Instance of the Non-Monotonic-Data Window Read module
  -----------------------------------------
  nonmonoto_inst : entity work.nonmonotonic_data_window_read
  generic map(
    DATA_WIDTH      => DATA_WIDTH,
    EVN_WIDTH       => RO_EVN_WIDTH,
    MEMDEPTH        => NONMONOTO_MEMDEPTH
  )
  port map(
    -- readout configuration (semi-static)
    latency                 => latency                ,
    window                  => window                 ,
    procrastination         => (others => '0')        ,
    startpointer_cooldown   => startpointer_cooldown  ,
    max_disorder            => max_disorder           ,
    -- ttc inputs: ttc_clk clocked
    ttc_clk                 => ttc_clk                ,
    ttc_rst                 => ttc_rst                ,
    l1a                     => l1a                    ,
    evt_ctr                 => evt_ctr                ,
    bunch_ctr               => bunch_ctr              ,
    -- input data
    input_clk               => input_clk              ,
    input_wr                => nmdwr_wr               ,
    input_bx                => nmdwr_bx_in            ,
    input_data              => nmdwr_data_in          ,
    -- output ports: rdclk clocked
    output_clk              => midclk                 ,
    output_inevent          => nmdwr_inevent          ,
    output_valid            => nmdwr_valid            ,
    output_evn              => nmdwr_evn              ,
    output_bx               => nmdwr_bx               ,
    output_data             => nmdwr_data             
  );
  
  nmdwr_wr <= input_data(31);
  nmdwr_bx_in <= std_logic_vector( minimum( unsigned( input_data(16 downto 5) ), to_unsigned(LHC_BUNCH_COUNT-1,12) )  );
  nmdwr_data_in <= input_data(29 downto 17) & input_data(4 downto 0) ;
  
  -----------------------------------------
  -- Reading and creating the output event
  -----------------------------------------
  process(midclk)
    variable in_event_pipe : boolean := false;
    variable semiword_odd : integer range 1 downto 0 := 1;
  begin
    if rising_edge(midclk) then
      if ttc_rst = '1' then
        in_event_pipe := false;
        semiword_odd := 1;
        ff_wren <= '0';
        ff_di <= (others => '0');
      else
        ff_wren <= '0';
        ff_di(63 downto 60) <= x"1";
        
        if nmdwr_inevent or in_event_pipe then
          if not in_event_pipe then -- starting the event
            ff_di(64+RO_EVN_WIDTH-1 downto 64) <= nmdwr_evn;
            semiword_odd := 1;
          end if;
          
          -- write  if    ( word complete    AND (was completed with valid OR event finished )
          ff_wren <= bo2sl( semiword_odd = 0 and (nmdwr_valid or not nmdwr_inevent) );

          if nmdwr_valid then -- regular case
            ff_di( semiword_odd*30 + 29 downto semiword_odd*30 ) <= nmdwr_data(17 downto 5) & std_logic_vector(nmdwr_bx) & nmdwr_data(4 downto 0);
            semiword_odd := 1 - semiword_odd ; -- if a hit is stored, next hit changes semiword 
          else -- finishing event, complete semiword if needed
            ff_di( semiword_odd*30 + 29 downto semiword_odd*30 ) <=  (others => '1');
          end if;
          
        end if;
        
        in_event_pipe := nmdwr_inevent ;
      end if; -- rst vsr regular operation
    end if; -- rising_edge
  end process;

  -----------------------------------------
  -- Output FIFO
  -----------------------------------------
  hits_out_fifo_inst : entity work.qwfifo
  generic map (
    DATA_WIDTH => OUTFIFO_WIDTH,
    DEPTH_LOG2 => 13
  )
  port map (
    RDCOUNT => ff_RDCOUNT,
    WRCOUNT => ff_WRCOUNT,
    
    RST => ff_RST,                   -- 1-bit input reset
    
    WRCLK => midclk,               -- 1-bit input write clock
    ALMOSTFULL => ff_af,
    FULL => ff_full,                 -- 1-bit output full
    WREN => ff_wren,                  -- 1-bit input write enable
    DI => ff_di, -- Input data, width defined by DATA_WIDTH parameter
    
    RDCLK => rdclk,               -- 1-bit input read clock
    ALMOSTEMPTY => almost_empty,
    EMPTY => empty,       -- 1-bit output empty
    RDEN => rden,         -- 1-bit input read enable
    DO => ff_do              -- Output data, width defined by DATA_WIDTH parameter
  );

  ff_RST <= ttc_rst;
  do     <= ff_do(63 downto 0);
  evnout <= ff_do(64+RO_EVN_WIDTH-1 downto 64);


end architecture;