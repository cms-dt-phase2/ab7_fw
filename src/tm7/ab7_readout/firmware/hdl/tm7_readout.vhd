----------------------------------------------------------------------------------
-- Everything related to readout
-- Alvaro Navarro, CIEMAT, 2015
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.bo2.all;
use work.ipbus.all;
use work.TM7_readout_decl.all;
use work.TM7_version.all;
use work.ipbus_reg_types.all;
use work.minmax.all;
use work.mp7_ttc_decl.all;
use work.dt_common_pkg.all; -- Cela's Trigger data types
use work.obdt_decoder_decl.all;
use work.BXarithmetic.all;

entity tm7_readout is
  port(
    -- ipbus signals
    ipb_clk: in std_logic;
    ipb_rst: in std_logic;
    ipb_in: in ipb_wbus;
    ipb_out: out ipb_rbus;
    -- ttc signals
    ttc_cmd: in ttc_cmd_t;
    l1a: in std_logic;
    bunch_ctr: in bctr_t;
    bunch_ctr_mp7_in: in bctr_t;
    bunch_ctr_incr: out bctr_t;
    evt_ctr: out eoctr_t;
    orb_ctr: in eoctr_t;
    orb_ctr_mp7_in: in eoctr_t;
    orb_ctr_incr: out eoctr_t;
    -- ttc clocks and resets
    ttc_clk: in std_logic;
    ttc_rst: in std_logic;
    clk80: in std_logic;
    clk160: in std_logic;
    rst_p: in std_logic;
    -- Hits input data
    hit_clk : in std_logic;
    hit_data : in slv32_v_t(1 to 3);  -- valid & '0' & St & SL & Channel & BX & TDC
    -- Trigger primitives input data
    tpg_clk : in std_logic;
    tp : in std_logic_vector(DTPRIMITIVE_SIZE-1 downto 0);
    tp_valid : in std_logic; 
    -- amc13 block signals
    amc13_refclk: in std_logic;
    AMC_P1_RX_P, AMC_P1_RX_N : in std_logic;
    AMC_P1_TX_N, AMC_P1_TX_P : out std_logic
  );
end tm7_readout;

architecture Behavioral of tm7_readout is

  signal evt_ctr_i: unsigned(eoctr_t'range);
  signal evt_ctr_u: eoctr_t;
  signal resync, ec0: std_logic;

  --L1A FIFO
  constant FIFO_WIDTH : integer := 12 + 16 + 1 + RO_EVN_WIDTH + RO_ORN_WIDTH + RO_BXN_WIDTH;
  constant FIFO_DEPTH : integer := 10;
  signal l1ff_DO, l1ff_DI : std_logic_vector(FIFO_WIDTH-1 downto 0) := (others => '0');
  signal l1ff_RDCOUNT,l1ff_WRCOUNT : std_logic_vector(FIFO_DEPTH-1 downto 0) := (others => '0');
  signal l1ff_EMPTY, l1ff_RDEN, l1ff_WREN, l1ff_AF, l1ff_AE, l1ff_FULL, l1ff_RST: std_logic := '0';
  signal l1ff_do_bxn : bctr_t;
  signal l1ff_do_evn : std_logic_vector(RO_EVN_WIDTH-1 downto 0);
  signal l1ff_do_orn : std_logic_vector(RO_ORN_WIDTH-1 downto 0);
  signal l1ff_do_resync : std_logic;
  signal l1ff_do_tptimer : std_logic_vector(15 downto 0);
  signal tptimer : unsigned(15 downto 0);
  signal lasttpbx, l1ff_do_lasttpbx : std_logic_vector(11 downto 0);

  signal clk_tts  : std_logic;
  signal tts      : std_logic_vector (3 downto 0);
  signal amc13_valid, amc13_hdr, amc13_trl : std_logic;
  signal amc13_d  : std_logic_vector (63 downto 0);
  signal amc13_af, amc13_rdy, amc13_resync : std_logic;
  signal rst_link : std_logic;
  
  signal dtff_do_1, dtff_do_2, dtff_do_3, tpff_do : std_logic_vector(63 downto 0);
  signal dtff_empty_1, dtff_rden_1, dtff_ae_1 : std_logic;
  signal dtff_empty_2, dtff_rden_2, dtff_ae_2 : std_logic;
  signal dtff_empty_3, dtff_rden_3, dtff_ae_3 : std_logic;
  signal tpff_empty, tpff_rden, tpff_ae : std_logic;
  signal dtff_evn_1, dtff_evn_2, dtff_evn_3, tpff_evn: std_logic_vector(RO_EVN_WIDTH-1 downto 0);
  signal dtff_rdcount_1, dtff_wrcount_1 : std_logic_vector(12 downto 0);
  signal dtff_rdcount_2, dtff_wrcount_2 : std_logic_vector(12 downto 0);
  signal dtff_rdcount_3, dtff_wrcount_3 : std_logic_vector(12 downto 0);
  signal tpff_rdcount, tpff_wrcount: std_logic_vector(12 downto 0);
  
  constant DEBUGPAYLOAD_HITS : integer := 8;
  constant DEBUGPAYLOAD_TPS : integer := 64;
  constant DEBUGPAYLOAD_TOTAL : integer := 3*DEBUGPAYLOAD_HITS + DEBUGPAYLOAD_TPS;
  type debugpayload_t is array (natural range DEBUGPAYLOAD_TOTAL-1 downto 0) of std_logic_vector(59 downto 0);
  signal debugpayload : debugpayload_t;

  constant N_STAT : integer := 64;
  constant N_CTRL : integer := 64;
  signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);
  signal stat, csrd: ipb_reg_v(N_STAT-1 downto 0);
  signal conf, csrq: ipb_reg_v(N_CTRL-1 downto 0);
  signal freeze_stats_pipe : std_logic_vector(2 downto 0);
    
  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals declaration
  signal conf_amc13_rst_edge, conf_amc13_rst_hold, conf_debug_payload, conf_hits1_disable, conf_hits2_disable,
    conf_hits3_disable, conf_stat_freeze, conf_tps_disable, conf_tps_fullchi2, stat_amc13_af, stat_amc13_rdy,
    stat_hits1_ff_ae, stat_hits1_ff_empty, stat_hits1_ff_rden, stat_hits2_ff_ae, stat_hits2_ff_empty,
    stat_hits2_ff_rden, stat_hits3_ff_ae, stat_hits3_ff_empty, stat_hits3_ff_rden, stat_l1afifo_ae, stat_l1afifo_af,
    stat_l1afifo_empty, stat_l1afifo_full, stat_l1afifo_qw_empty, stat_l1afifo_qw_rd, stat_l1afifo_rden,
    stat_l1afifo_resync, stat_l1afifo_wren, stat_tps_ff_ae, stat_tps_ff_empty, stat_tps_ff_rden : std_logic := '0';

  signal conf_tps_station : std_logic_vector(1 downto 0) := (others => '0');
  signal stat_TTS, stat_evb_fsm_state : std_logic_vector(3 downto 0) := (others => '0');
  signal conf_bcn_incr, stat_l1afifo_bxn : std_logic_vector(11 downto 0) := (others => '0');
  signal conf_boardID, stat_l1afifo_orn : std_logic_vector(15 downto 0) := (others => '0');
  signal stat_hits1_ff_evn, stat_hits2_ff_evn, stat_hits3_ff_evn, stat_l1afifo_evn, stat_l1afifo_qw_evn,
    stat_tps_ff_evn : std_logic_vector(23 downto 0) := (others => '0');
  signal conf_userData, stat_gth : std_logic_vector(31 downto 0) := (others => '0');

  signal conf_amc13_rst_len : unsigned(3 downto 0) := (others => '0');
  signal conf_hits1_evb_keepfuture, conf_hits2_evb_keepfuture, conf_hits3_evb_keepfuture, conf_tps_evb_keepfuture,
    stat_resync_queued, stat_resync_queued_max : unsigned(7 downto 0) := (others => '0');
  signal conf_tps_procrastination, stat_l1afifo_occupancy,
    stat_l1afifo_occupancy_max : unsigned(9 downto 0) := (others => '0');
  signal conf_hits1_evb_timeout, conf_hits1_latency, conf_hits1_max_disorder, conf_hits1_startpointer_cooldown,
    conf_hits1_window, conf_hits2_evb_timeout, conf_hits2_latency, conf_hits2_max_disorder,
    conf_hits2_startpointer_cooldown, conf_hits2_window, conf_hits3_evb_timeout, conf_hits3_latency,
    conf_hits3_max_disorder, conf_hits3_startpointer_cooldown, conf_hits3_window, conf_tps_evb_timeout,
    conf_tps_latency, conf_tps_max_disorder, conf_tps_startpointer_cooldown,
    conf_tps_window : unsigned(11 downto 0) := (others => '0');
  signal stat_hits1_ff_occupancy, stat_hits1_ff_occupancy_max, stat_hits2_ff_occupancy, stat_hits2_ff_occupancy_max,
    stat_hits3_ff_occupancy, stat_hits3_ff_occupancy_max, stat_tps_ff_occupancy,
    stat_tps_ff_occupancy_max : unsigned(12 downto 0) := (others => '0');
  signal conf_tts_thresholds_bsy2ofw, conf_tts_thresholds_ofw2bsy, conf_tts_thresholds_ofw2rdy,
    conf_tts_thresholds_rdy2ofw : unsigned(15 downto 0) := (others => '0');
  signal stat_amc13_reset_ctr : unsigned(31 downto 0) := (others => '0');
  signal stat_amc13_af_ctr, stat_amc13_nrdy_ctr, stat_amc13_rdy_ctr, stat_header_ctr, stat_hits1_ff_rden_ctr,
    stat_hits2_ff_rden_ctr, stat_hits3_ff_rden_ctr, stat_l1a_ctr, stat_resync_ctr, stat_tps_ff_rden_ctr,
    stat_trailer_ctr, stat_tts_bsy_ctr, stat_tts_ofw_ctr, stat_tts_rdy_ctr,
    stat_valid_ctr : unsigned(47 downto 0) := (others => '0');

  -- End of automatically-generated VHDL code for register "breakout" signals declaration
  --------------------------------------------------------------------------------


begin


  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals assignment

  conf_userData                    <= conf(16#00#)(31 downto 0);
  conf_boardID                     <= conf(16#01#)(15 downto 0);
  conf_bcn_incr                    <= conf(16#01#)(27 downto 16);
  conf_stat_freeze                 <= conf(16#01#)(28);
  conf_debug_payload               <= conf(16#01#)(29);
  conf_amc13_rst_len               <= unsigned( conf(16#02#)(3 downto 0) );
  conf_amc13_rst_edge              <= conf(16#02#)(4);
  conf_amc13_rst_hold              <= conf(16#02#)(5);
  conf_tts_thresholds_ofw2rdy      <= unsigned( conf(16#03#)(15 downto 0) );
  conf_tts_thresholds_rdy2ofw      <= unsigned( conf(16#03#)(31 downto 16) );
  conf_tts_thresholds_bsy2ofw      <= unsigned( conf(16#04#)(15 downto 0) );
  conf_tts_thresholds_ofw2bsy      <= unsigned( conf(16#04#)(31 downto 16) );
  conf_hits1_evb_timeout           <= unsigned( conf(16#10#)(11 downto 0) );
  conf_hits1_evb_keepfuture        <= unsigned( conf(16#10#)(19 downto 12) );
  conf_hits1_disable               <= conf(16#10#)(31);
  conf_hits1_latency               <= unsigned( conf(16#11#)(11 downto 0) );
  conf_hits1_window                <= unsigned( conf(16#11#)(23 downto 12) );
  conf_hits1_startpointer_cooldown <= unsigned( conf(16#12#)(11 downto 0) );
  conf_hits1_max_disorder          <= unsigned( conf(16#12#)(23 downto 12) );
  conf_hits2_evb_timeout           <= unsigned( conf(16#14#)(11 downto 0) );
  conf_hits2_evb_keepfuture        <= unsigned( conf(16#14#)(19 downto 12) );
  conf_hits2_disable               <= conf(16#14#)(31);
  conf_hits2_latency               <= unsigned( conf(16#15#)(11 downto 0) );
  conf_hits2_window                <= unsigned( conf(16#15#)(23 downto 12) );
  conf_hits2_startpointer_cooldown <= unsigned( conf(16#16#)(11 downto 0) );
  conf_hits2_max_disorder          <= unsigned( conf(16#16#)(23 downto 12) );
  conf_hits3_evb_timeout           <= unsigned( conf(16#18#)(11 downto 0) );
  conf_hits3_evb_keepfuture        <= unsigned( conf(16#18#)(19 downto 12) );
  conf_hits3_disable               <= conf(16#18#)(31);
  conf_hits3_latency               <= unsigned( conf(16#19#)(11 downto 0) );
  conf_hits3_window                <= unsigned( conf(16#19#)(23 downto 12) );
  conf_hits3_startpointer_cooldown <= unsigned( conf(16#1A#)(11 downto 0) );
  conf_hits3_max_disorder          <= unsigned( conf(16#1A#)(23 downto 12) );
  conf_tps_evb_timeout             <= unsigned( conf(16#1C#)(11 downto 0) );
  conf_tps_evb_keepfuture          <= unsigned( conf(16#1C#)(19 downto 12) );
  conf_tps_disable                 <= conf(16#1C#)(31);
  conf_tps_latency                 <= unsigned( conf(16#1D#)(11 downto 0) );
  conf_tps_window                  <= unsigned( conf(16#1D#)(23 downto 12) );
  conf_tps_startpointer_cooldown   <= unsigned( conf(16#1E#)(11 downto 0) );
  conf_tps_max_disorder            <= unsigned( conf(16#1E#)(23 downto 12) );
  conf_tps_procrastination         <= unsigned( conf(16#1F#)(9 downto 0) );
  conf_tps_station                 <= conf(16#1F#)(11 downto 10);
  conf_tps_fullchi2                <= conf(16#1F#)(12);
  stat(16#00#)(31 downto 0)        <= std_logic_vector( stat_resync_ctr(31 downto 0) );
  stat(16#01#)(15 downto 0)        <= std_logic_vector( stat_resync_ctr(47 downto 32) );
  stat(16#01#)(23 downto 16)       <= std_logic_vector( stat_resync_queued );
  stat(16#01#)(31 downto 24)       <= std_logic_vector( stat_resync_queued_max );
  stat(16#02#)(31 downto 0)        <= std_logic_vector( stat_l1a_ctr(31 downto 0) );
  stat(16#03#)(15 downto 0)        <= std_logic_vector( stat_l1a_ctr(47 downto 32) );
  stat(16#04#)(31 downto 0)        <= std_logic_vector( stat_header_ctr(31 downto 0) );
  stat(16#05#)(15 downto 0)        <= std_logic_vector( stat_header_ctr(47 downto 32) );
  stat(16#06#)(31 downto 0)        <= std_logic_vector( stat_trailer_ctr(31 downto 0) );
  stat(16#07#)(15 downto 0)        <= std_logic_vector( stat_trailer_ctr(47 downto 32) );
  stat(16#08#)(31 downto 0)        <= std_logic_vector( stat_valid_ctr(31 downto 0) );
  stat(16#09#)(15 downto 0)        <= std_logic_vector( stat_valid_ctr(47 downto 32) );
  stat(16#0A#)(3 downto 0)         <= stat_TTS;
  stat(16#0A#)(7 downto 4)         <= stat_evb_fsm_state;
  stat(16#0B#)(31 downto 0)        <= std_logic_vector( stat_tts_rdy_ctr(31 downto 0) );
  stat(16#0C#)(15 downto 0)        <= std_logic_vector( stat_tts_rdy_ctr(47 downto 32) );
  stat(16#0D#)(31 downto 0)        <= std_logic_vector( stat_tts_ofw_ctr(31 downto 0) );
  stat(16#0E#)(15 downto 0)        <= std_logic_vector( stat_tts_ofw_ctr(47 downto 32) );
  stat(16#0F#)(31 downto 0)        <= std_logic_vector( stat_tts_bsy_ctr(31 downto 0) );
  stat(16#10#)(15 downto 0)        <= std_logic_vector( stat_tts_bsy_ctr(47 downto 32) );
  stat(16#11#)(9 downto 0)         <= std_logic_vector( stat_l1afifo_occupancy );
  stat(16#11#)(23 downto 14)       <= std_logic_vector( stat_l1afifo_occupancy_max );
  stat(16#12#)(23 downto 0)        <= stat_l1afifo_evn;
  stat(16#12#)(24)                 <= stat_l1afifo_empty;
  stat(16#12#)(25)                 <= stat_l1afifo_rden;
  stat(16#12#)(26)                 <= stat_l1afifo_ae;
  stat(16#12#)(27)                 <= stat_l1afifo_full;
  stat(16#12#)(28)                 <= stat_l1afifo_af;
  stat(16#12#)(29)                 <= stat_l1afifo_wren;
  stat(16#13#)(23 downto 0)        <= stat_l1afifo_qw_evn;
  stat(16#13#)(24)                 <= stat_l1afifo_qw_empty;
  stat(16#13#)(25)                 <= stat_l1afifo_qw_rd;
  stat(16#14#)(15 downto 0)        <= stat_l1afifo_orn;
  stat(16#14#)(27 downto 16)       <= stat_l1afifo_bxn;
  stat(16#14#)(28)                 <= stat_l1afifo_resync;
  stat(16#18#)(0)                  <= stat_amc13_rdy;
  stat(16#18#)(1)                  <= stat_amc13_af;
  stat(16#19#)(31 downto 0)        <= std_logic_vector( stat_amc13_reset_ctr );
  stat(16#1A#)(31 downto 0)        <= std_logic_vector( stat_amc13_rdy_ctr(31 downto 0) );
  stat(16#1B#)(15 downto 0)        <= std_logic_vector( stat_amc13_rdy_ctr(47 downto 32) );
  stat(16#1C#)(31 downto 0)        <= std_logic_vector( stat_amc13_nrdy_ctr(31 downto 0) );
  stat(16#1D#)(15 downto 0)        <= std_logic_vector( stat_amc13_nrdy_ctr(47 downto 32) );
  stat(16#1E#)(31 downto 0)        <= std_logic_vector( stat_amc13_af_ctr(31 downto 0) );
  stat(16#1F#)(15 downto 0)        <= std_logic_vector( stat_amc13_af_ctr(47 downto 32) );
  stat(16#20#)(31 downto 0)        <= stat_gth;
  stat(16#28#)(12 downto 0)        <= std_logic_vector( stat_hits1_ff_occupancy );
  stat(16#28#)(28 downto 16)       <= std_logic_vector( stat_hits1_ff_occupancy_max );
  stat(16#29#)(23 downto 0)        <= stat_hits1_ff_evn;
  stat(16#29#)(24)                 <= stat_hits1_ff_rden;
  stat(16#29#)(25)                 <= stat_hits1_ff_empty;
  stat(16#29#)(26)                 <= stat_hits1_ff_ae;
  stat(16#2A#)(31 downto 0)        <= std_logic_vector( stat_hits1_ff_rden_ctr(31 downto 0) );
  stat(16#2B#)(15 downto 0)        <= std_logic_vector( stat_hits1_ff_rden_ctr(47 downto 32) );
  stat(16#2C#)(12 downto 0)        <= std_logic_vector( stat_hits2_ff_occupancy );
  stat(16#2C#)(28 downto 16)       <= std_logic_vector( stat_hits2_ff_occupancy_max );
  stat(16#2D#)(23 downto 0)        <= stat_hits2_ff_evn;
  stat(16#2D#)(24)                 <= stat_hits2_ff_rden;
  stat(16#2D#)(25)                 <= stat_hits2_ff_empty;
  stat(16#2D#)(26)                 <= stat_hits2_ff_ae;
  stat(16#2E#)(31 downto 0)        <= std_logic_vector( stat_hits2_ff_rden_ctr(31 downto 0) );
  stat(16#2F#)(15 downto 0)        <= std_logic_vector( stat_hits2_ff_rden_ctr(47 downto 32) );
  stat(16#30#)(12 downto 0)        <= std_logic_vector( stat_hits3_ff_occupancy );
  stat(16#30#)(28 downto 16)       <= std_logic_vector( stat_hits3_ff_occupancy_max );
  stat(16#31#)(23 downto 0)        <= stat_hits3_ff_evn;
  stat(16#31#)(24)                 <= stat_hits3_ff_rden;
  stat(16#31#)(25)                 <= stat_hits3_ff_empty;
  stat(16#31#)(26)                 <= stat_hits3_ff_ae;
  stat(16#32#)(31 downto 0)        <= std_logic_vector( stat_hits3_ff_rden_ctr(31 downto 0) );
  stat(16#33#)(15 downto 0)        <= std_logic_vector( stat_hits3_ff_rden_ctr(47 downto 32) );
  stat(16#34#)(12 downto 0)        <= std_logic_vector( stat_tps_ff_occupancy );
  stat(16#34#)(28 downto 16)       <= std_logic_vector( stat_tps_ff_occupancy_max );
  stat(16#35#)(23 downto 0)        <= stat_tps_ff_evn;
  stat(16#35#)(24)                 <= stat_tps_ff_rden;
  stat(16#35#)(25)                 <= stat_tps_ff_empty;
  stat(16#35#)(26)                 <= stat_tps_ff_ae;
  stat(16#36#)(31 downto 0)        <= std_logic_vector( stat_tps_ff_rden_ctr(31 downto 0) );
  stat(16#37#)(15 downto 0)        <= std_logic_vector( stat_tps_ff_rden_ctr(47 downto 32) );

  -- End of automatically-generated VHDL code for register "breakout" signals assignment
  --------------------------------------------------------------------------------

  --------------------------------------
  --------------------------------------
  -- IPbus register
  --------------------------------------
  --------------------------------------
  reg: entity work.ipbus_ctrlreg_v
  generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
  port map(
    clk => ipb_clk,
    reset => ipb_rst,
    ipbus_in => ipb_in,
    ipbus_out => ipb_out,
    d => csrd,
    q => csrq,
    stb => ctrl_stb
  );
  
  conf <= csrq;
  csrd <= stat when (freeze_stats_pipe(0) = '0') else csrd;
  
  freeze_stats_pipe <= conf_stat_freeze & freeze_stats_pipe(freeze_stats_pipe'high downto 1) when rising_edge(ttc_clk) else freeze_stats_pipe;
  
  stat_TTS <= tts;

  --------------------------------------
  --------------------------------------
  -- Event counter, resync management, bunch_ctr
  --------------------------------------
  --------------------------------------
  -- This logic comes from MP7, but the dependence of ec0 on resync and resync on readout_done has been removed

  resync  <= '1' when ttc_cmd = TTC_BCMD_RESYNC else '0';
  ec0     <= '1' when ttc_cmd = TTC_BCMD_EC0 else '0';
  
  evt_ctr_u   <= eoctr_t(evt_ctr_i);
  evt_ctr     <= evt_ctr_u;

  process(ttc_clk)
  begin
    if rising_edge(ttc_clk) then
      if ec0 = '1' or ttc_rst = '1' then
        evt_ctr_i <= to_unsigned(1, evt_ctr_i'length); -- CMS rules; first event is 1, not zero
      elsif l1a = '1' then
        evt_ctr_i <= evt_ctr_i + 1;
      end if;
      
      if ttc_rst = '1' then -- just counters
        stat_l1a_ctr <= (others => '0');
        stat_resync_ctr <= (others => '0');
      else
        stat_l1a_ctr <= stat_l1a_ctr + bo2int(l1a = '1');
        stat_resync_ctr <= stat_resync_ctr + bo2int(resync = '1');
      end if;
    end if;
   end process;

  bcn_incr: entity work.bcn_incr
  port map(
    ttc_clk         => ttc_clk,
    incr            => conf_bcn_incr,
    bunch_ctr_in    => bunch_ctr_mp7_in,
    bunch_ctr_out   => bunch_ctr_incr,
    orb_ctr_in      => orb_ctr_mp7_in,
    orb_ctr_out     => orb_ctr_incr);

  -----------------------------------------
  -- FIFO for the L1As & Resync
  -----------------------------------------
  -- * There's no easy way for this memory to get full, because the dt trigger event builder would overflow first
  -- and events would start to be very small, thus emptying this fifo faster
  l1a_fifo_inst : entity work.qwfifo
  generic map (
    DATA_WIDTH => FIFO_WIDTH,
    DEPTH_LOG2 => FIFO_DEPTH)
  port map (
    ALMOSTEMPTY => l1ff_AE,
    ALMOSTFULL => l1ff_AF,
    RDCOUNT => l1ff_RDCOUNT,
    WRCOUNT => l1ff_WRCOUNT,
    --RDERR => open,               -- 1-bit output read error
    --WRERR => open,               -- 1-bit output write error
    
    RST => l1ff_RST,                   -- 1-bit input reset
    
    WRCLK => ttc_clk,               -- 1-bit input write clock
    FULL => l1ff_FULL,                 -- 1-bit output full
    WREN => l1ff_WREN,                  -- 1-bit input write enable
    DI => l1ff_DI, -- Input data, width defined by DATA_WIDTH parameter
    
    RDCLK => clk80,               -- 1-bit input read clock
    EMPTY => l1ff_EMPTY,       -- 1-bit output empty
    RDEN => l1ff_RDEN,         -- 1-bit input read enable
    DO => l1ff_DO              -- Output data, width defined by DATA_WIDTH parameter
  );
  
  -- There is no overflow detection implemented, to in theory an excess of l1as could pass undetected and L1As get lost
  -- However, this fifo is 4 times as deep as AMC13's, so long before we should have gone OOS, the AMC13 has
  -- Also, we have our own busy threshold, and no more L1As should arrive when we are BSY
  l1ff_WREN <= (l1a or resync) and not l1ff_FULL;
  l1ff_RST <= ttc_rst;
  
  l1ff_DI( 11 downto 0  )   <= bunch_ctr;
  l1ff_DI( 35 downto 12 )   <= evt_ctr_u(RO_EVN_WIDTH-1 downto 0);
  l1ff_DI( 51 downto 36 )   <= orb_ctr(RO_ORN_WIDTH-1 downto 0);
  l1ff_DI( 52           )   <= resync;
  l1ff_DI( 64 downto 53 )   <= lasttpbx;
  l1ff_DI( 80 downto 65 )   <= std_logic_vector(tptimer);
  l1ff_do_bxn               <= l1ff_DO( 11 downto 0  );
  l1ff_do_evn               <= l1ff_DO( 35 downto 12 );
  l1ff_do_orn               <= l1ff_DO( 51 downto 36 );
  l1ff_do_resync            <= l1ff_DO( 52           );
  l1ff_do_lasttpbx          <= l1ff_DO( 64 downto 53 );
  l1ff_do_tptimer           <= l1ff_DO( 80 downto 65 );

  stat_l1afifo_rden   <= l1ff_RDEN;
  stat_l1afifo_ae     <= l1ff_AE;
  stat_l1afifo_full   <= l1ff_FULL;
  stat_l1afifo_af     <= l1ff_AF;
  stat_l1afifo_wren   <= l1ff_WREN;
  stat_l1afifo_empty  <= l1ff_EMPTY;
  stat_l1afifo_resync <= l1ff_do_resync;                                       
  stat_l1afifo_orn    <= l1ff_do_orn;    
  stat_l1afifo_evn    <= l1ff_do_evn;
  stat_l1afifo_bxn    <= l1ff_do_bxn;                            

  ---------------------------------------------------
  ---------------------------------------------------
  -- Subsystems Trigger Event Builder instances
  ---------------------------------------------------
  ---------------------------------------------------
  -- READOUT EVENT BUILDER
  dt_roeb1: entity work.dt_roeb
  port map(
    -- readout configuration (semi-static)
    latency                 => conf_hits1_latency,
    window                  => conf_hits1_window,
    startpointer_cooldown   => conf_hits1_startpointer_cooldown,
    max_disorder            => conf_hits1_max_disorder,
    -- ttc inputs: ttc_clk clocked
    ttc_clk                 => ttc_clk, -- TTC and input data
    ttc_rst                 => ttc_rst, -- for all clocks
    l1a                     => l1a,
    evt_ctr                 => evt_ctr_u(RO_EVN_WIDTH-1 downto 0),
    bunch_ctr               => bunch_ctr,
    -- input data
    input_clk               => hit_clk,
    input_data              => hit_data(1), 
    -- intermediateclock for output fifo writing
    midclk                  => clk160, -- Circ.Buf.-->FIFO
    -- output fifo ports: rdclk clocked
    rdclk                   => clk80, --> FIFO read
    empty                   => dtff_empty_1,
    almost_empty            => dtff_ae_1,
    rden                    => dtff_rden_1,
    do                      => dtff_do_1,
    evnout                  => dtff_evn_1,
    ff_rdcount              => dtff_rdcount_1,
    ff_wrcount              => dtff_wrcount_1
  );
  
  stat_hits1_ff_evn    <= dtff_evn_1;
  stat_hits1_ff_rden   <= dtff_rden_1;
  stat_hits1_ff_empty  <= dtff_empty_1;
  stat_hits1_ff_ae     <= dtff_ae_1;
  
  dt_roeb2: entity work.dt_roeb
  port map(
    -- readout configuration (semi-static)
    latency                 => conf_hits2_latency,
    window                  => conf_hits2_window,
    startpointer_cooldown   => conf_hits1_startpointer_cooldown,
    max_disorder            => conf_hits2_max_disorder,
    -- ttc inputs: ttc_clk clocked
    ttc_clk                 => ttc_clk, -- TTC and input data
    ttc_rst                 => ttc_rst, -- for all clocks
    l1a                     => l1a,
    evt_ctr                 => evt_ctr_u(RO_EVN_WIDTH-1 downto 0),
    bunch_ctr               => bunch_ctr,
    -- input data
    input_clk               => hit_clk,
    input_data              => hit_data(2), 
    -- intermediateclock for output fifo writing
    midclk                  => clk160, -- Circ.Buf.-->FIFO
    -- output fifo ports: rdclk clocked
    rdclk                   => clk80, --> FIFO read
    empty                   => dtff_empty_2,
    almost_empty            => dtff_ae_2,
    rden                    => dtff_rden_2,
    do                      => dtff_do_2,
    evnout                  => dtff_evn_2,
    ff_rdcount              => dtff_rdcount_2,
    ff_wrcount              => dtff_wrcount_2
  );
  
  stat_hits2_ff_evn    <= dtff_evn_2;
  stat_hits2_ff_rden   <= dtff_rden_2;
  stat_hits2_ff_empty  <= dtff_empty_2;
  stat_hits2_ff_ae     <= dtff_ae_2;
  
  dt_roeb3: entity work.dt_roeb
  port map(
    -- readout configuration (semi-static)
    latency                 => conf_hits3_latency,
    window                  => conf_hits3_window,
    startpointer_cooldown   => conf_hits3_startpointer_cooldown,
    max_disorder            => conf_hits3_max_disorder,
    -- ttc inputs: ttc_clk clocked
    ttc_clk                 => ttc_clk, -- TTC and input data
    ttc_rst                 => ttc_rst, -- for all clocks
    l1a                     => l1a,
    evt_ctr                 => evt_ctr_u(RO_EVN_WIDTH-1 downto 0),
    bunch_ctr               => bunch_ctr,
    -- input data
    input_clk               => hit_clk,
    input_data              => hit_data(3), 
    -- intermediateclock for output fifo writing
    midclk                  => clk160, -- Circ.Buf.-->FIFO
    -- output fifo ports: rdclk clocked
    rdclk                   => clk80, --> FIFO read
    empty                   => dtff_empty_3,
    almost_empty            => dtff_ae_3,
    rden                    => dtff_rden_3,
    do                      => dtff_do_3,
    evnout                  => dtff_evn_3,
    ff_rdcount              => dtff_rdcount_3,
    ff_wrcount              => dtff_wrcount_3
  );
  
  stat_hits3_ff_evn    <= dtff_evn_3;
  stat_hits3_ff_rden   <= dtff_rden_3;
  stat_hits3_ff_empty  <= dtff_empty_3;
  stat_hits3_ff_ae     <= dtff_ae_3;
  
  hit_debug_payload_gen : for sl in 1 to 3 generate
    process (hit_clk)
      variable hit_counter : unsigned(14 downto 0);
    begin
      if rising_edge(hit_clk) then
        if ttc_rst = '1' then
          debugpayload(sl*DEBUGPAYLOAD_HITS-1 downto (sl-1)*DEBUGPAYLOAD_HITS) <= (others => (others => '0'));
          hit_counter := (others => '0');
        else
          if hit_data(sl)(31) = '1' then  -- valid & '0' & St & SL & Channel & BX & TDC
            for i in sl*DEBUGPAYLOAD_HITS-1 downto (sl-1)*DEBUGPAYLOAD_HITS+1 loop
              debugpayload(i) <= debugpayload(i-1);
            end loop;
            debugpayload((sl-1)*DEBUGPAYLOAD_HITS) <= std_logic_vector( hit_counter ) & orb_ctr(7 downto 0) & bunch_ctr & hit_data(sl)(24 downto 0);
            hit_counter := hit_counter + 1;
          end if; 
        end if;
      end if;
    end process;
  end generate; 
 
 
  -- TRIGGER PRIMITIVES EVENT BUILDER
  dt_tpeb: entity work.dt_tpeb
  port map(
    -- readout configuration (semi-static)
    latency                 => conf_tps_latency,
    window                  => conf_tps_window,
    procrastination         => "00" & conf_tps_procrastination,
    startpointer_cooldown   => conf_tps_startpointer_cooldown,
    max_disorder            => conf_tps_max_disorder,
    fullchi2                => conf_tps_fullchi2, 
    station                 => conf_tps_station,
    -- ttc inputs: ttc_clk clocked
    ttc_clk                 => ttc_clk, -- TTC
    ttc_rst                 => ttc_rst, -- for all clocks
    l1a                     => l1a,
    evt_ctr                 => evt_ctr_u(RO_EVN_WIDTH-1 downto 0),
    bunch_ctr               => bunch_ctr,
    -- input data
    input_clk               => tpg_clk,
    tp                      => tp,
    tp_valid                => tp_valid,
    -- intermediateclock for output fifo writing
    midclk                  => clk160, -- Circ.Buf.-->FIFO
    -- output fifo ports: rdclk clocked
    rdclk                   => clk80, --> FIFO read
    empty                   => tpff_empty,
    almost_empty            => tpff_ae,
    rden                    => tpff_rden,
    do                      => tpff_do,
    evnout                  => tpff_evn,
    ff_rdcount              => tpff_rdcount,
    ff_wrcount              => tpff_wrcount
  );
    
  stat_tps_ff_evn     <= tpff_evn;
  stat_tps_ff_rden    <= tpff_rden;
  stat_tps_ff_empty   <= tpff_empty;
  stat_tps_ff_ae      <= tpff_ae;
  
  process(tpg_clk)
    variable tp_counter : unsigned(10 downto 0);
  begin
    if rising_edge(tpg_clk) then
      if ttc_rst = '1' then
        tptimer <= (others => '1');
        lasttpbx <= (others => '1');
        debugpayload(DEBUGPAYLOAD_TOTAL-1 downto 3*DEBUGPAYLOAD_HITS) <= (others => (others => '0'));
        tp_counter := (others => '0');
      else
        if tp_valid = '1' then
          tptimer <= (others => '0');
          lasttpbx <= dtPrimitive_from_slv(tp).bx_id_chamber;
          
          for i in DEBUGPAYLOAD_TOTAL-1 downto 3*DEBUGPAYLOAD_HITS+1 loop
            debugpayload(i) <= debugpayload(i-1);
          end loop;
          debugpayload(3*DEBUGPAYLOAD_HITS) <= std_logic_vector( tp_counter ) & orb_ctr(7 downto 0) & bunch_ctr & dtPrimitive_from_slv(tp).bx_time_chamber & dtPrimitive_from_slv(tp).bx_id_chamber;
          tp_counter := tp_counter + 1;
          
        elsif tptimer /= (tptimer'range => '1') then  tptimer <= tptimer + 1;
        end if;
      end if;
    end if;
  end process;
  
  ---------------------------------------------------
  ---------------------------------------------------
  -- DAQlink event formatter and TTS status
  ---------------------------------------------------
  ---------------------------------------------------
  clk_tts <= clk80;

  process(clk80)
    variable l1ff_occupancy : unsigned(FIFO_DEPTH-1 downto 0) := (others => '0');
    -- status management
    type status_t is (idle, second_header, dtro_payload1, dtro_payload2, dtro_payload3, dttp_payload, debug_payload, trailer);
    variable status : status_t := idle;
    variable timer : unsigned(conf_hits1_evb_timeout'range);
    -- for setting DL's valid, header and trailer
    type wordtype_t is (none, header, trailer, data);
    variable wordtype : wordtype_t := none;
    -- data lengths to report in event
    variable data_length_trailer : unsigned(19 downto 0) := (others => '0');
    -- for management of the resync status
    variable resync_pipe : std_logic := '0';
    variable queued_resyncs : unsigned(7 downto 0) := (others => '0');
--        variable resync_timer : integer range 2**40 - 1 downto 0;
    
  begin
    if rising_edge(clk80) then
      if rst_p = '1' then
        l1ff_rden <= '0';
        dtff_rden_1 <= '0';
        dtff_rden_2 <= '0';
        dtff_rden_3 <= '0';
        tpff_rden <= '0';
        
        status := idle;
        amc13_resync <= '0';
        amc13_d <= (others => '0');
        amc13_valid     <= '0';
        amc13_hdr     <= '0';
        amc13_trl <= '0';
        amc13_resync <= '0';
        resync_pipe := '0';
        queued_resyncs := (others => '0');
        tts <= x"8";

        stat_evb_fsm_state          <= (others => '0');
        stat_resync_queued          <= (others => '0');
        stat_resync_queued_max      <= (others => '0'); 
        stat_header_ctr             <= (others => '0');
        stat_trailer_ctr            <= (others => '0');
        stat_valid_ctr              <= (others => '0');
        stat_tts_rdy_ctr            <= (others => '0');
        stat_tts_ofw_ctr            <= (others => '0');
        stat_tts_bsy_ctr            <= (others => '0');
        stat_l1afifo_occupancy      <= (others => '0');
        stat_l1afifo_occupancy_max  <= (others => '0');
        stat_hits1_ff_occupancy     <= (others => '0');
        stat_hits1_ff_occupancy_max <= (others => '0');
        stat_hits1_ff_rden_ctr      <= (others => '0');
        stat_hits2_ff_occupancy     <= (others => '0');
        stat_hits2_ff_occupancy_max <= (others => '0');
        stat_hits2_ff_rden_ctr      <= (others => '0');
        stat_hits3_ff_occupancy     <= (others => '0');
        stat_hits3_ff_occupancy_max <= (others => '0');
        stat_hits3_ff_rden_ctr      <= (others => '0');
        stat_tps_ff_occupancy       <= (others => '0');
        stat_tps_ff_occupancy_max   <= (others => '0');
        stat_tps_ff_rden_ctr        <= (others => '0');
        
      else
        wordtype := none;
        l1ff_rden <= '0';
        dtff_rden_1 <= '0';
        dtff_rden_2 <= '0';
        dtff_rden_3 <= '0';
        tpff_rden <= '0';
        
        stat_evb_fsm_state <= x"0";
        if amc13_rdy = '1' and amc13_af = '0' and  l1ff_do_resync = '0' then
          case status is
            when idle =>
              stat_evb_fsm_state <= x"1";
              if l1ff_empty = '0' then
                stat_evb_fsm_state <= x"2";
                wordtype := header;
                amc13_d <= x"00" & l1ff_do_evn(23 downto 0) & l1ff_do_bxn & x"FFFFF";-- length unknown a priori
                status := second_header;
                data_length_trailer := (others => '0');
              end if;
            when second_header =>
              stat_evb_fsm_state <= x"3";
              wordtype := data;
              amc13_d <= x"0" & l1ff_do_tptimer(15 downto 0) & l1ff_do_lasttpbx(11 downto 0) & l1ff_do_orn & std_logic_vector(to_unsigned(SVN_REVISION,9)) & conf_boardID(6 downto 0);
              status := dtro_payload1;
              timer := conf_hits1_evb_timeout;
            when dtro_payload1 =>
              stat_evb_fsm_state <= x"4";
              if dtff_empty_1 = '0' and  dtff_evn_1 = l1ff_do_evn then
                stat_evb_fsm_state <= x"5";
                if conf_hits1_disable = '0' then wordtype := data;
                else wordtype := none;
                end if;
                amc13_d <= dtff_do_1;
                dtff_rden_1 <= '1';
              elsif dtff_empty_1 = '0' and (unsigned(dtff_evn_1) - unsigned(l1ff_do_evn) > conf_hits1_evb_keepfuture)  then -- if there is data from another event outside of the keepfuture range...
                dtff_rden_1 <= '1';
              elsif dtff_empty_1 = '0' or timer = 0 then -- if there is data from another event or timeout...
                stat_evb_fsm_state <= x"6";
                status := dtro_payload2;
                timer := conf_hits2_evb_timeout;
              else timer := timer - 1;
              end if;
            when dtro_payload2 =>
              stat_evb_fsm_state <= x"4";
              if dtff_empty_2 = '0' and  dtff_evn_2 = l1ff_do_evn then
                stat_evb_fsm_state <= x"5";
                if conf_hits2_disable = '0' then wordtype := data;
                else wordtype := none;
                end if;
                amc13_d <= dtff_do_2;
                dtff_rden_2 <= '1';
              elsif dtff_empty_2 = '0' and (unsigned(dtff_evn_2) - unsigned(l1ff_do_evn) > conf_hits2_evb_keepfuture)  then -- if there is data from another event outside of the keepfuture range...
                dtff_rden_2 <= '1';
              elsif dtff_empty_2 = '0' or timer = 0 then -- if there is data from another event or timeout...
                stat_evb_fsm_state <= x"6";
                status := dtro_payload3;
                timer := conf_hits3_evb_timeout;
              else timer := timer - 1;
              end if;
            when dtro_payload3 =>
              stat_evb_fsm_state <= x"4";
              if dtff_empty_3 = '0' and  dtff_evn_3 = l1ff_do_evn then
                stat_evb_fsm_state <= x"5";
                if conf_hits3_disable = '0' then wordtype := data;
                else wordtype := none;
                end if;
                amc13_d <= dtff_do_3;
                dtff_rden_3 <= '1';
              elsif dtff_empty_3 = '0' and (unsigned(dtff_evn_3) - unsigned(l1ff_do_evn) > conf_hits3_evb_keepfuture)  then -- if there is data from another event outside of the keepfuture range...
                dtff_rden_3 <= '1';
              elsif dtff_empty_3 = '0' or timer = 0 then -- if there is data from another event or timeout...
                stat_evb_fsm_state <= x"6";
                status := dttp_payload ;
                timer := conf_tps_evb_timeout;
              else timer := timer - 1;
              end if;
            when dttp_payload =>
              stat_evb_fsm_state <= x"7";
              if tpff_empty = '0' and  tpff_evn = l1ff_do_evn then
                stat_evb_fsm_state <= x"8";
                if conf_tps_disable = '0' then wordtype := data;
                else wordtype := none;
                end if;
                amc13_d <= tpff_do;
                tpff_rden <= '1';
              elsif tpff_empty = '0' and (unsigned(tpff_evn) - unsigned(l1ff_do_evn) > conf_tps_evb_keepfuture)  then -- if there is data from another event outside of the keepfuture range...
                tpff_rden <= '1';
              elsif tpff_empty = '0' or timer = 0 then -- if there is data from another event or timeout...
                stat_evb_fsm_state <= x"9";
                if conf_debug_payload = '1' then
                  status := debug_payload;
                  timer := to_unsigned(debugpayload'length-1, timer'length);
                else
                  status := trailer;
                end if;
              else timer := timer - 1;
              end if;
            when debug_payload =>
              amc13_d <= x"F" & debugpayload( to_integer(timer) );
              wordtype := data;
              if timer > 0 then timer := timer - 1;
              else status := trailer;
              end if;
            when others => --trailer
              stat_evb_fsm_state <= x"F";
              wordtype := trailer;
              amc13_d <= x"00000000" & l1ff_do_evn(7 downto 0) & x"0" & std_logic_vector(data_length_trailer + 1);
              status := idle;
              l1ff_rden <= '1';
          end case;
        end if;
        
        amc13_valid <= bo2sl(wordtype /= none);
        amc13_hdr   <= bo2sl(wordtype = header);
        amc13_trl   <= bo2sl(wordtype = trailer);
        if wordtype /= none then data_length_trailer := data_length_trailer + 1;
        end if;

        -- when BGo = resync, increase the pending resyncs (pipe to account for different clocks)
        if resync_pipe = '0' and resync = '1' then 
          queued_resyncs := queued_resyncs + 1;
        end if;
        resync_pipe := resync;
        -- Processing a ReSync:
        if l1ff_do_resync = '1' and l1ff_empty = '0' then 
          queued_resyncs := queued_resyncs - 1;
          l1ff_rden <= '1';
        end if;
        amc13_resync <= l1ff_do_resync and not l1ff_empty;
        
        -- to-do:
        -- if queued_resyncs >= 2 or resync_timer = 0 or l1ff_occupancy >= threshold
        -- you should force the resync: reset all memories, then assert amc13_resync
        
        l1ff_occupancy := unsigned(l1ff_WRCOUNT) - unsigned(l1ff_RDCOUNT);
        --TTS: 8-ready, 4-busy, 2-oos, 1-warning_overflow, C-error, F-disconnected
        case tts is
          when x"4" =>
            if l1ff_do_resync = '1' and queued_resyncs = 0 then
              tts <= x"8";
            elsif l1ff_occupancy < conf_tts_thresholds_bsy2ofw then
              tts <= x"1";
            end if;
          when x"1" =>
            if l1ff_occupancy < conf_tts_thresholds_ofw2rdy then
              tts <= x"8";
            elsif l1ff_occupancy > conf_tts_thresholds_ofw2bsy then
              tts <= x"4";
            end if;
          when x"8" =>
            if l1ff_occupancy > conf_tts_thresholds_rdy2ofw then
              tts <= x"1";
            end if;
          when others =>
            tts <= x"8";
        end case;
        
        if queued_resyncs > 0 then tts <= x"4";
        end if;
        
        stat_resync_queued <= queued_resyncs;
        stat_resync_queued_max <= maximum( stat_resync_queued_max, queued_resyncs );
        
        stat_header_ctr <= stat_header_ctr + bo2int(amc13_hdr = '1' and amc13_valid = '1');
        stat_trailer_ctr <= stat_trailer_ctr + bo2int(amc13_trl = '1' and amc13_valid = '1');
        stat_valid_ctr <= stat_valid_ctr + bo2int(amc13_valid = '1');
        
        stat_tts_rdy_ctr <= stat_tts_rdy_ctr + bo2int(tts = x"8");
        stat_tts_ofw_ctr <= stat_tts_ofw_ctr + bo2int(tts = x"1");
        stat_tts_bsy_ctr <= stat_tts_bsy_ctr + bo2int(tts = x"4");
        
        stat_l1afifo_occupancy <= l1ff_occupancy;
        stat_l1afifo_occupancy_max <= maximum( stat_l1afifo_occupancy_max, stat_l1afifo_occupancy );
        stat_l1afifo_qw_evn <= l1ff_do_evn;
        stat_l1afifo_qw_empty <= l1ff_empty;
        stat_l1afifo_qw_rd <= l1ff_rden;
        
        stat_hits1_ff_occupancy <= unsigned(dtff_wrcount_1) - unsigned(dtff_rdcount_1);
        stat_hits1_ff_occupancy_max <= maximum(stat_hits1_ff_occupancy_max , stat_hits1_ff_occupancy);
        stat_hits1_ff_rden_ctr <= stat_hits1_ff_rden_ctr + bo2int(dtff_rden_1 = '1');
        
        stat_hits2_ff_occupancy <= unsigned(dtff_wrcount_2) - unsigned(dtff_rdcount_2);
        stat_hits2_ff_occupancy_max <= maximum(stat_hits2_ff_occupancy_max , stat_hits2_ff_occupancy);
        stat_hits2_ff_rden_ctr <= stat_hits2_ff_rden_ctr + bo2int(dtff_rden_2 = '1');
        
        stat_hits3_ff_occupancy <= unsigned(dtff_wrcount_3) - unsigned(dtff_rdcount_3);
        stat_hits3_ff_occupancy_max <= maximum(stat_hits3_ff_occupancy_max , stat_hits3_ff_occupancy);
        stat_hits3_ff_rden_ctr <= stat_hits3_ff_rden_ctr + bo2int(dtff_rden_3 = '1');
        
        stat_tps_ff_occupancy <= unsigned(tpff_wrcount) - unsigned(tpff_rdcount);
        stat_tps_ff_occupancy_max <= maximum(stat_tps_ff_occupancy_max , stat_tps_ff_occupancy);
        stat_tps_ff_rden_ctr <= stat_tps_ff_rden_ctr + bo2int(tpff_rden = '1');
      end if;
    end if;
  end process;


  --------------------------------------
  --------------------------------------
  -- AMC13 interface
  --------------------------------------
  --------------------------------------

  process (ipb_clk)
    variable counter : integer range 2**15-1 downto 0;
    variable rstfire_pipe : std_logic := '0';
  begin
    if rising_edge(ipb_clk) then
      if ipb_rst = '1' then
        rst_link <= '0';
        rstfire_pipe := '0';
        counter := 0;

        stat_amc13_reset_ctr    <= (others => '0');
        stat_amc13_rdy_ctr      <= (others => '0');
        stat_amc13_nrdy_ctr     <= (others => '0');
        stat_amc13_af_ctr       <= (others => '0');
      else
        if rstfire_pipe /= conf_amc13_rst_edge then    counter := 2**(to_integer(conf_amc13_rst_len))-1;  
        elsif counter > 0 then              counter := counter - 1;
        end if;
        rstfire_pipe := conf_amc13_rst_edge;
        
        rst_link <= bo2sl(counter > 0) or conf_amc13_rst_hold;
        
        stat_amc13_reset_ctr    <= stat_amc13_reset_ctr    + bo2int(rst_link = '1');
        stat_amc13_rdy_ctr      <= stat_amc13_rdy_ctr      + bo2int(amc13_rdy = '1');
        stat_amc13_nrdy_ctr     <= stat_amc13_nrdy_ctr     + bo2int(amc13_rdy = '0');
        stat_amc13_af_ctr       <= stat_amc13_af_ctr       + bo2int(amc13_af = '1');
      end if;
    end if;
  end process;

  -- My amc13link... Original MP7's is in amc13_link.vhd
  amc13: entity work.tm7_amc13link
  port map(
    sysclk            => ipb_clk            ,
    reset             => rst_link       ,  -- asynchronous reset, assert reset until GTX REFCLK stable
    -- MGT ports
    AMC_P1_RX_P       => AMC_P1_RX_P    ,
    AMC_P1_RX_N       => AMC_P1_RX_N    ,
    AMC_P1_TX_N       => AMC_P1_TX_N    ,
    AMC_P1_TX_P       => AMC_P1_TX_P    ,
    amc13_refclk      => amc13_refclk   ,
    --MGT_CLK_115_1_P        => MGT_CLK_115_1_P    ,
    --MGT_CLK_115_1_N        => MGT_CLK_115_1_N    ,
    -- TTS port
    TTSclk            => clk_tts        ,  -- clock source which clocks TTS signals
    TTS               => tts            ,
    -- Data port
    ReSyncAndEmpty    => amc13_resync   ,-- asserted for two clk80 cycles, because amc13_rsae has a >10 ns requirement
    EventDataClk      => clk80          ,
    EventData_valid   => amc13_valid    , -- used as data write enable
    EventData_header  => amc13_hdr      , -- first data word
    EventData_trailer => amc13_trl      , -- last data word
    EventData         => amc13_d        ,
    AlmostFull        => amc13_af       , -- buffer almost full
    Ready             => amc13_rdy      ,
    debug             => stat_gth
    );

  stat_amc13_rdy <= amc13_rdy;
  stat_amc13_af <= amc13_af;

end Behavioral;
