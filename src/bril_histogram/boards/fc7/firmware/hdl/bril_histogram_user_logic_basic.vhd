library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


use work.ipbus.all;
use work.system_package.all;
--! user packages
use work.user_package.all;
use work.user_version_package.all;

library unisim;
use unisim.vcomponents.all;

entity user_logic is 
port
(

	--# led
	usrled1_r						: out	std_logic; -- fmc_l12_spare[8]
	usrled1_g						: out	std_logic; -- fmc_l12_spare[9]
	usrled1_b						: out	std_logic; -- fmc_l12_spare[10]
	usrled2_r						: out	std_logic; -- fmc_l12_spare[11]
	usrled2_g						: out	std_logic; -- fmc_l12_spare[12]
	usrled2_b						: out	std_logic; -- fmc_l12_spare[13]

	--# on-board fabric clk
    fabric_clk_p                    : in    std_logic; -- new port [PV 2015.08.19]
    fabric_clk_n                    : in    std_logic; -- new port [PV 2015.08.19]
    fabric_coax_or_osc_p 			: in 	std_logic;
	fabric_coax_or_osc_n 			: in 	std_logic;

	--# on-board mgt clk
	pcie_clk_p						: in	std_logic;
    pcie_clk_n                      : in    std_logic;
	osc_xpoint_a_p					: in	std_logic;
	osc_xpoint_a_n					: in	std_logic;
	osc_xpoint_b_p					: in	std_logic;
	osc_xpoint_b_n					: in	std_logic;
	osc_xpoint_c_p					: in	std_logic;
	osc_xpoint_c_n					: in	std_logic;
	osc_xpoint_d_p					: in	std_logic;
	osc_xpoint_d_n					: in	std_logic;
	ttc_mgt_xpoint_a_p				: in	std_logic;
	ttc_mgt_xpoint_a_n				: in	std_logic;
	ttc_mgt_xpoint_b_p				: in	std_logic;
	ttc_mgt_xpoint_b_n				: in	std_logic;
	ttc_mgt_xpoint_c_p				: in	std_logic;
	ttc_mgt_xpoint_c_n				: in	std_logic;
			
	--# fmc mgt clk		
	fmc_l12_gbtclk0_a_p				: in	std_logic; 
	fmc_l12_gbtclk0_a_n				: in	std_logic; 
	fmc_l12_gbtclk1_a_p				: in	std_logic; 
	fmc_l12_gbtclk1_a_n				: in	std_logic; 
	fmc_l12_gbtclk0_b_p				: in	std_logic; 
	fmc_l12_gbtclk0_b_n				: in	std_logic; 
	fmc_l12_gbtclk1_b_p				: in	std_logic; 
	fmc_l12_gbtclk1_b_n				: in	std_logic; 
	fmc_l8_gbtclk0_p				: in	std_logic; 
	fmc_l8_gbtclk0_n				: in	std_logic; 
	fmc_l8_gbtclk1_p				: in	std_logic; 
	fmc_l8_gbtclk1_n				: in	std_logic; 

	--# fmc mgt
	fmc_l12_dp_c2m_p				: out	std_logic_vector(11 downto 0);
	fmc_l12_dp_c2m_n				: out	std_logic_vector(11 downto 0);
	fmc_l12_dp_m2c_p				: in	std_logic_vector(11 downto 0);
	fmc_l12_dp_m2c_n				: in	std_logic_vector(11 downto 0);
	fmc_l8_dp_c2m_p					: out	std_logic_vector( 7 downto 0);
	fmc_l8_dp_c2m_n					: out	std_logic_vector( 7 downto 0);
	fmc_l8_dp_m2c_p					: in	std_logic_vector( 7 downto 0);
	fmc_l8_dp_m2c_n					: in	std_logic_vector( 7 downto 0);
	
	--# fmc fabric clk	
    fmc_l8_clk0                     : in    std_logic; 
    fmc_l8_clk1                     : in    std_logic;
    fmc_l12_clk0                    : in    std_logic;
    fmc_l12_clk1                    : in    std_logic;    

	--# fmc gpio		
	fmc_l8_la_p						: inout	std_logic_vector(33 downto 0);
	fmc_l8_la_n						: inout	std_logic_vector(33 downto 0);
	fmc_l12_la_p					: inout	std_logic_vector(33 downto 0);
	fmc_l12_la_n					: inout	std_logic_vector(33 downto 0);
	
	--# amc mgt		
	k7_amc_rx_p						: inout	std_logic_vector(15 downto 1);
	k7_amc_rx_n						: inout	std_logic_vector(15 downto 1);
	amc_tx_p						: inout	std_logic_vector(15 downto 1);
	amc_tx_n						: inout	std_logic_vector(15 downto 1);
	
	--# amc fabric
	k7_fabric_amc_rx_p03			: inout	std_logic;
	k7_fabric_amc_rx_n03    		: inout	std_logic;
	k7_fabric_amc_tx_p03    		: inout	std_logic;
	k7_fabric_amc_tx_n03    		: inout	std_logic;

	--# ddr3
	ddr3_sys_clk_p 					: in	std_logic;
	ddr3_sys_clk_n 					: in	std_logic;
	ddr3_dq                 		: inout std_logic_vector( 31 downto 0);
	ddr3_dqs_p              		: inout std_logic_vector(  3 downto 0);
	ddr3_dqs_n              		: inout std_logic_vector(  3 downto 0);
	ddr3_addr               		: out   std_logic_vector( 13 downto 0);
	ddr3_ba                 		: out   std_logic_vector(  2 downto 0);
	ddr3_ras_n              		: out   std_logic;
	ddr3_cas_n              		: out   std_logic;
	ddr3_we_n               		: out   std_logic;
	ddr3_reset_n            		: out   std_logic;
	ddr3_ck_p               		: out   std_logic_vector(  0 downto 0);
	ddr3_ck_n               		: out   std_logic_vector(  0 downto 0);
	ddr3_cke                		: out   std_logic_vector(  0 downto 0);
	ddr3_cs_n               		: out   std_logic_vector(  0 downto 0);
	ddr3_dm                 		: out   std_logic_vector(  3 downto 0);
	ddr3_odt                		: out   std_logic_vector(  0 downto 0);

    --# cdce
	cdce_pll_lock_i                 : in    std_logic; -- new port [PV 2015.08.19]  
    cdce_pri_clk_bufg_o 		    : out 	std_logic; -- new port [PV 2015.08.19] 
    cdce_ref_sel_o                  : out   std_logic; -- new port [PV 2015.08.19]   
    cdce_pwrdown_o                  : out   std_logic; -- new port [PV 2015.08.19]  
    cdce_sync_o                     : out   std_logic; -- new port [PV 2015.08.19]  
    cdce_sync_clk_o                 : out   std_logic; -- new port [PV 2015.08.19]  

	--# system clk		
	osc125_a_bufg_i					: in	std_logic;
	osc125_a_mgtrefclk_i			: in	std_logic;
	osc125_b_bufg_i					: in 	std_logic;
	osc125_b_mgtrefclk_i			: in	std_logic;
    clk_31_250_bufg_i		        : in	std_logic; -- new port [PV 2015.08.19]
    
    --# ipbus comm    
	ipb_clk_o				        : out	std_logic;
	ipb_rst_i				        : in	std_logic;
	ipb_miso_o			            : out	ipb_rbus_array(0 to nbr_usr_slaves-1);
	ipb_mosi_i			            : in	ipb_wbus_array(0 to nbr_usr_slaves-1);

    --# ipbus conf
	ip_addr_o						: out	std_logic_vector(31 downto 0);
    mac_addr_o                      : out   std_logic_vector(47 downto 0);
    rarp_en_o                       : out   std_logic;
    use_i2c_eeprom_o                : out   std_logic
);
end user_logic;

architecture usr of user_logic is

    signal user_reset                   : std_logic;
    signal global_reset_ipb             : std_logic;
    
    signal global_reset_clk40_presync   : std_logic;
    signal global_reset_clk40           : std_logic;
    attribute ASYNC_REG : string;
    attribute ASYNC_REG of global_reset_clk40_presync : signal is "true";
    attribute ASYNC_REG of global_reset_clk40 : signal is "true";

	signal ipb_clk					: std_logic;
    
    signal fabric_clk_pre_buf       : std_logic;                
    signal fabric_clk               : std_logic;
    
    signal clk_40MHz                : std_logic;
    signal clk_320MHz               : std_logic;
    
    signal ctrl_reg		            : array_32x32bit;
	signal stat_reg		            : array_32x32bit;

    signal cdce_pri_clk_bufg        : std_logic;

    signal cdce_sync_from_ipb       : std_logic;     
    signal cdce_sel_from_ipb        : std_logic;                    
    signal cdce_pwrdown_from_ipb    : std_logic;
    signal cdce_ctrl_from_ipb       : std_logic;  
--  signal cdce_sel_from_fabric     : std_logic;                    
--  signal cdce_pwrdown_from_fabric : std_logic;
--  signal cdce_ctrl_from_fabric    : std_logic; 


    -- bril histo
    signal bril_fifo_rd_en          : std_logic;    
    signal bril_fifo_empty          : std_logic;
    signal bril_fifo_words_cnt      : std_logic_vector(31 downto 0);                
    signal bril_data                : std_logic_vector(31 downto 0);
    signal bril_data_valid          : std_logic;
    
    signal injection_data                   : std_logic_vector(31 downto 0);
    signal injection_reset_pointer          : std_logic;
    signal injection_wr_en                  : std_logic;
    signal injection_norbits_available      : natural;
    signal injection_norbits_tocollect      : natural;
    signal injection_start                  : std_logic;
    signal injection_stop                   : std_logic := '0';


begin

	--===========================================--
	-- ipbus management
	--===========================================--
	ipb_clk 		      <= clk_31_250_bufg_i; 	-- select the frequency of the ipbus clock 
	ipb_clk_o 	          <= ipb_clk;				-- always forward the selected ipb_clk to system core
    --
	ip_addr_o 	          <= x"c0_a8_00_50";
	mac_addr_o	          <= x"aa_bb_cc_dd_ee_50";
	rarp_en_o 		      <= '1';
	use_i2c_eeprom_o      <= '1';
	--===========================================--


    --===========================================--
	-- other clocks 
	--===========================================--
	fclk_ibuf:      ibufgds     port map (i => fabric_clk_p, ib => fabric_clk_n, o => fabric_clk_pre_buf);
    fclk_bufg:      bufg        port map (i => fabric_clk_pre_buf,               o => fabric_clk);
	--===========================================--
        


    --===================================--
    cdce_synch: entity work.cdce_synchronizer
    --===================================--
    generic map
    (    
        pwrdown_delay     => 1000,
        sync_delay        => 1000000    
    )
    port map
    (    
        reset_i           => ipb_rst_i,
        ------------------
        ipbus_ctrl_i      => not cdce_ctrl_from_ipb,          -- default: 1 (ipb controlled) 
        ipbus_sel_i       =>     cdce_sel_from_ipb,           -- default: 0 (select secondary clock)
        ipbus_pwrdown_i   => not cdce_pwrdown_from_ipb,       -- default: 1 (powered up)
        ipbus_sync_i      => not cdce_sync_from_ipb,          -- default: 1 (disable sync mode), rising edge needed to resync
        ------------------                                                                       
        user_sel_i        => '1', -- cdce_sel_from_fabric     -- effective only when ipbus_ctrl_i = '0'            
        user_pwrdown_i    => '1', -- cdce_pwrdown_from_fabric -- effective only when ipbus_ctrl_i = '0'
        user_sync_i       => '1', -- cdce_sync_from_fabric    -- effective only when ipbus_ctrl_i = '0'  
        ------------------
        pri_clk_i         => cdce_pri_clk_bufg,
        sec_clk_i         => fabric_clk,         -- copy of what is actually send to the secondary clock input
        pwrdown_o         => cdce_pwrdown_o,
        sync_o            => cdce_sync_o,
        ref_sel_o         => cdce_ref_sel_o,    
        sync_clk_o        => cdce_sync_clk_o
    );
        
        cdce_pri_clk_bufg_o <= cdce_pri_clk_bufg; cdce_pri_clk_bufg   <= '0'; -- [note: in this example, the cdce_pri_clk_bufg is not used]
    --===================================--

	
	
	--===========================================--
	stat_regs_inst: entity work.ipb_user_status_regs
	--===========================================--
	port map
	(
		clk					=> ipb_clk,
		reset				=> global_reset_ipb,
		ipb_mosi_i			=> ipb_mosi_i(user_ipb_stat_regs),
		ipb_miso_o			=> ipb_miso_o(user_ipb_stat_regs),
		regs_i				=> stat_reg
	);
	--===========================================--



	--===========================================--
	ctrl_regs_inst: entity work.ipb_user_control_regs
	--===========================================--
	port map
	(
		clk					=> ipb_clk,
		reset				=> global_reset_ipb,
		ipb_mosi_i			=> ipb_mosi_i(user_ipb_ctrl_regs),
		ipb_miso_o			=> ipb_miso_o(user_ipb_ctrl_regs),
		regs_o				=> ctrl_reg,
		--
		user_reset_o        => user_reset       
	);
	--===========================================--
		
    --===========================================--
	injector_regs_inst: entity work.bril_histogram_injector_ipbus
	--===========================================--
    port map(
        clk				                => ipb_clk,                              
        reset				            => global_reset_ipb,                    
        ipbus_in			            => ipb_mosi_i(user_ipb_injector_regs),  
        ipbus_out			            => ipb_miso_o(user_ipb_injector_regs),  
        --
        injection_data_o                => injection_data,                   
        injection_reset_pointer_o       => injection_reset_pointer,    
        injection_wr_en_o               => injection_wr_en,            
        injection_norbits_available_o   => injection_norbits_available,
        injection_norbits_tocollect_o   => injection_norbits_tocollect,
        injection_start_o               => injection_start,            
        injection_stop_o                => injection_stop              
    );
    
    --===========================================--
	readout_regs_inst: entity work.bril_histogram_readout_ipbus
	--===========================================--
    port map(
        clk				                => ipb_clk,                              
        reset				            => global_reset_ipb,                    
        ipbus_in			            => ipb_mosi_i(user_ipb_readout_regs),  
        ipbus_out			            => ipb_miso_o(user_ipb_readout_regs),  
        -- fifo status
        fifo_empty_i                    => bril_fifo_empty,
        fifo_data_valid_i               => bril_data_valid,
        fifo_words_cnt_i                => bril_fifo_words_cnt,
        -- fifo read interfac          
        fifo_rd_en_o                    => bril_fifo_rd_en,
        fifo_data_i                     => bril_data    
    );


   --===========================================--
	-- example register mapping
	--===========================================--
									stat_reg(0)					<= usr_id_0;
									stat_reg(1)					<= x"0000_0000";
									stat_reg(2)					<= firmware_id;
									
	usrled1_r				<= not ctrl_reg(0)(0);
	usrled1_g				<= not ctrl_reg(0)(1);
	usrled1_b				<= not ctrl_reg(0)(2);
	
	usrled2_r				<= not ctrl_reg(0)(4);
	usrled2_g				<= not ctrl_reg(0)(5);
	usrled2_b				<= not ctrl_reg(0)(6);
	
	-- clock generatoion
	clock_generator_inst: entity work.clock_generator
	port map (
	   reset           => '0',
	   clk_in1         => fabric_clk,
	   --
	   clk_40MHz_0     => clk_40MHz,
	   clk_320MHz_0    => clk_320MHz
	);
	
	-- buffering the reset
	global_reset_ipb <= ipb_rst_i or user_reset;
	process(clk_320MHz)
	begin
	   if rising_edge(clk_320MHz) then
	       global_reset_clk40_presync <= global_reset_ipb;
	       global_reset_clk40 <= global_reset_clk40_presync;
	   end if;
	end process;
	
	
	
    bril_histogram_fc7_injector_inst: entity work.bril_histogram_fc7_injector
    port map ( 
        -- commong
        reset_i                             => global_reset_clk40,
        clk_i                               => clk_320MHz,
        ipb_clk_i                           => ipb_clk,
        -- readout interface
        bril_fifo_rd_en_i                   => bril_fifo_rd_en,
        bril_fifo_empty_o                   => bril_fifo_empty,
        bril_fifo_words_cnt_o               => bril_fifo_words_cnt,
        bril_data_o                         => bril_data,
        bril_data_valid_o                   => bril_data_valid,
        -- injection interface
        injection_data_i                    => injection_data,
        injection_reset_pointer_i           => injection_reset_pointer,
        injection_wr_en_i                   => injection_wr_en,
        injection_norbits_available_i       => injection_norbits_available,
        injection_norbits_tocollect_i       => injection_norbits_tocollect,
        injection_start_i                   => injection_start,
        injection_stop_i                    => injection_stop
    );

end usr;
