-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Primitive builder
---- File       : calc_primitive_tan_pos_bx_time.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-05
-------------------------------------------------------------------------------
---- Description: Calculates the value for chamber position and angle's
----              tangent (multiplied by a 4096 factor) for the muon's 
----              trajectory, considering 2 superlayers.
----
----              It is pipelined and takes 3 clock cycles.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;

entity CALC_PRIMITIVE_TAN_POS_BX_TIME is
  port (
    clk                   : in  std_logic;
    rst                   : in  std_logic;
    enable                : in  std_logic;
    paired_segments       : in  DTSegment_arr_t;
    sl_phi3_x_offset      : in  signed(9 downto 0);
    bx_time_chamber       : out std_logic_vector(16 downto 0);
    x_coor_chamber        : out SignedDistance_t;
    tan_phi_chamber_x4096 : out signed(14 downto 0)
  );
end entity;

architecture TwoProcess of CALC_PRIMITIVE_TAN_POS_BX_TIME is

  constant X_LEN       : integer          := SignedDistance_t'length;
  constant T_LEN       : integer          := DTSegMainParams_t.bx_time'length;
  constant OFFSET_ZERO : SignedDistance_t := (others => '0');
  --
  -- Vertical distance between PHI1 and PHI3 centers is around 235 mm (237 ??).
  --
  -- Division to calculate chamber global tan(phi) will use a quantizer for 
  -- a multiplication and a bits' shift. 
  --
  -- Base quantizer will be 2^15, so division will be obtained by multiying 
  -- Q and the X differences, and right-shifting 15 positions.
  --
  -- Q = ( 2^15/ 235 ) = 139.44 (~ 139.50)
  --
  -- To get an extra of precision, instead of rounding to 139 (integer), I add
  -- 2 additional bits to keep ".50" digits. 
  --
  constant VERT_PHI1_PHI3_INV_Q15 : signed(10 downto 0) 
                                  := to_signed(139, 9) & "10";

  constant TANPHI_DIV_LEN : integer := X_LEN + VERT_PHI1_PHI3_INV_Q15'length; --29

  -- Internal signals to be registered.
  type reg_type is record
    diff_x_pos         : SignedDistance_t;
    base_x_stg1        : SignedDistance_t;
    base_x             : SignedDistance_t;
    sum_bx_time        : unsigned(T_LEN   downto 0); -- +1 bit (2 terms in sum)
    bx_time_stg1       : unsigned(T_LEN-1 downto 0);
    bx_time            : unsigned(T_LEN-1 downto 0);
    tan_phi_q_division : signed(TANPHI_DIV_LEN-1 downto 0);
    tan_phi_x4096      : signed(tan_phi_chamber_x4096'length-1 downto 0);
    x_offset           : SignedDistance_t;
    x_coor             : SignedDistance_t;
  end record;

  constant regDefault : reg_type := (
    diff_x_pos         => (others => '0'),
    base_x_stg1        => (others => '0'),
    base_x             => (others => '0'),
    sum_bx_time        => (others => '0'),
    bx_time_stg1       => (others => '0'),
    bx_time            => (others => '0'),
    tan_phi_q_division => (others => '0'),
    tan_phi_x4096      => (others => '0'),
    x_offset           => (others => '0'),
    x_coor             => (others => '0')
  );

  signal reg, nextReg : reg_type := regDefault;

begin
  ---------------------------------------------------------------
  -- Outgoing signals 
  --
  x_coor_chamber        <= reg.x_coor;
  bx_time_chamber       <= std_logic_vector(reg.bx_time);
  tan_phi_chamber_x4096 <= reg.tan_phi_x4096;
  
  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, paired_segments, sl_phi3_x_offset)
    variable phi3_x_pos  : SignedDistance_t;
    variable phi1_x_pos  : SignedDistance_t;
    variable phi3_offset : SignedDistance_t;
  begin
    -- Default values
    nextReg <= reg;

    ---------------------------
    -- First clock cycle
    ---------------------------
    --- For TAN_PHI ---
    phi3_x_pos  := paired_segments(1).horizontal_position;
    phi1_x_pos  := paired_segments(0).horizontal_position;

    -- Two additional (null) precision bits.
    phi3_offset := resize( signed(std_logic_vector(sl_phi3_x_offset) &"0000"), 
                           SignedDistance_t'length );

    -- All values have 2 additional precision bits.
    nextReg.diff_x_pos  <= (phi3_x_pos + phi3_offset) - phi1_x_pos;

    -- For BX Time
    nextReg.sum_bx_time <= 
            unsigned('0' & paired_segments(1).main_params.bx_time) + 
            unsigned('0' & paired_segments(0).main_params.bx_time);
            
    -- Pipeline and axis displacement to guarantee a positive final X value.
    if phi3_offset < OFFSET_ZERO
    then nextReg.base_x_stg1 <= (phi1_x_pos - phi3_offset);
    else nextReg.base_x_stg1 <= phi1_x_pos;
    end if;

    ---------------------------
    -- Second clock cycle
    ---------------------------
    -- 16 bits 1st operand + 11 bits 2nd operand -> 27 bits result
    -- Result's last 4 bits will be precision bits.
    nextReg.tan_phi_q_division <= reg.diff_x_pos * VERT_PHI1_PHI3_INV_Q15;
    --
    -- Because the vertical center of the chamber is in the middle of the 
    -- distance (half distance) between PHI SL's, by taking half of the 
    -- horizontal difference we obtain the offset to calculate the horizontal
    -- position in the chamber center.
    --
    -- It would be necessary to add an offset between reference frameworks (the 
    -- usual one in the left-side of the SL, and the CMS standard in the middle
    -- of the chamber) to get the final reference value.
    --
    -- 1 shift to divide by 2.
    nextReg.x_offset <= resize(reg.diff_x_pos(X_LEN - 1 downto 1), X_LEN);
    
    -- Pipeline
    nextReg.base_x <= reg.base_x_stg1;
    -- Division by 2 and pipeline
    nextReg.bx_time_stg1 <= reg.sum_bx_time(reg.sum_bx_time'length-1 downto 1);

    ---------------------------
    -- Third clock cycle
    ---------------------------
    -- Final TAN PHI. 
    -- Removed 6 extra precision bits (4 from positions + 2 from quatizer) 
    -- + 3 bits (=15-12) to get x4096
    nextReg.tan_phi_x4096 <= 
            resize( reg.tan_phi_q_division(TANPHI_DIV_LEN-1 downto 9), 
                    tan_phi_chamber_x4096'length );

    -- Final X coord (in algorithm's own reference framework).
    nextReg.x_coor  <= reg.base_x + reg.x_offset;

    nextReg.bx_time <= reg.bx_time_stg1;
  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1' then 
        reg <= regDefault;
      elsif enable = '1' then
        reg <= nextReg;
      end if;
    end if;
  end process;

end architecture;
