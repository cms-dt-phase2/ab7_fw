# BRIL Histogramming IP

This code is a generic histogramming IP to be used for the BRIL luminosity meauserement in the Phase-2 operation stage of the CMS detector. The project is targetting Xillinx 7-series FPGA (or newer) and used IPbus Builder to manage source files.

## Table of Contents
- [IP Structure](#ip-structure)
- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Folder Structure](#folder-structure)
    - [Adding the module to an exisiting project](#adding-the-module-to-an-exisiting-project)
      - [IPbus Builder Project](#ipbus-builder-project)
      - [Any Vivado Project](#any-vivado-project)
    - [Instantiating the synchronous implementation](#instantiating-the-synchronous-implementation)
      - With accumulation
      - Without accumulation
      - Port Description
    - [Instantiating the asynchronous implementation](#instantiating-the-asynchronous-implementation)
      - With accumulation
      - Without accumulation
      - Port Description
    - [Firmware readout interface](#firmware-readout-interface)
      - IPbus
    - [Software readout interface](#software-readout-interface)
- [APPENDIX: Building the FC7 test bench project from scratch](#appendix-building-the-fc7-test-bench-project-from-scratch)

## IP Structure

The IP is represented by two separate implementations: **synchronous** and **asynchronous**. The first assumes data coming every bunch-crossing, and that data from the bunch-crossing N will come 1 clock cycle after the bunch-crossing N-1. This is not true for the asychronous implementation, where user has to provide orbit/bx id and valid signals to properly increment data in a bin. This allows variable processing times for the increment data (as for example when counting clusters in triggered data). The following two diagrams represent the structure of both implementations:

![BRIL Histogram Diagram Sync](doc/bril_histogram_diagram_sync.png)
![BRIL Histogram Diagram Async](doc/bril_histogram_diagram_async.png)

In both cases the IP can be instantiated with Accumulation layer or without it (options are shown in read). 

The accumulation layer receives data from N units (modules, chips, channels) and accumualates using the recursive accumulator. Additionally, the error block is handling errors from the connected units and generates a "mask" of modules which will be applied to the following histograms.

The histogram wrapper receives the accumulated data either from the accumulation layer or from "outside" (when instantiated without the accumulation layers) and forwards them to the histogramming core. The histogramming core performs the data counting. From now the data path differs significantly. 

**Synchronous implementation** The histogramming core is controlled by two signals: new_histogram (to start a new histogram and send out the previous) and new_iteration (basically BX0 signal) - both signals are sent on BX(NUMBER_OF_BINS-1), last BX. New histogram is sent on the last orbit. Data are packed in the internal BRAM in 32 bit words with different formats which depend on the counter width (max number of counts in every bin). The data format is given in the doc folder. Once new_histogram is received by the histogramming core, it streams the counts (and simultanously resets them) to the data streamer. The latter appends the header and error data to the histogram and streams it outwards. An optional FIFO can be used to store the streaming data for read-out (allows to reduce the required bandwidth allocating the whole period of instagram accumulation for read-out). Either streamed or buffered in the FIFO data are sent together with a valid signal at the output of the top-level block (histogram wrapper of accumulation layer).

**Asynchronous implementation** The histogramming core is data-driven in this case. The new_histogram is formed internally in the accumulation layer (or wrapper) when received data from ORBIT0, BX0. Due to earlier defined convention to send the data on the last BX of th, which is not known in this case, the input data is delayed by one bunch-crossing with respect to the new_histogram signal. In this way the new_histogram still comes on the last BX of the last orbit. Instead of a single BRAM in the core - in this case two BRAMs are generated in the wrapper. And the memory is switched when new_histogram is received. This allows extended times for processing of the incoming data - in particular in async case one needs several clock cycles to read the previous count. Once core is done with the current histogram and the memory is switched, the packer gets an access to the freshly update memory. It prepends header data, appends error data and issues a ready signal. Here, an internally generated FIFO interface takes over the control of the BRAM. It imitates a first-word-fall-through FIFO, and resets the words which were read by the software. Once the FIFO is empty, the packer waits for the new_histogram signal to get access to another BRAM which was filled by the core.

## Getting Started

### Prerequisites

- Xilinx FPGA (7-series or newer generations)
- Vivado Suite (tested on 2018.3)
- IPbus Builder (tested with v0.5.2)

### Folder Structure

```bash
|-- components - ```different submodules are contained here```
|   `-- bril_histogram - ```the histogramming IP itself```
|       |-- software - ```C++ library to decode the payload (includes python wrapper)```
|       `-- firmware
|           |-- ucf - ```timing constraints for the BrilHistogram IP```
|           |-- sim - ```core functionality simulation```
|           |-- hdl - ```IP HDL code```
|           `-- cfg - ```IPbus Builder dependency files```
`-- boards - ```code for various platforms```
    `-- fc7 - ```fc7-based testbench code```
        |-- software - ```python scripts used to test the functionality of the module```
        `-- firmware - ```fc7 testbench firmware code```
            |-- ucf - ```timing constraints for the test bench```
            |-- sim - ```simulation to quick test the test bench```
            |-- hdl - ```test bench HDL code```
            |-- cgn - ```other IPs used in the test bench project```
            |   `-- clock_generator
            `-- cfg - ```IPbus builder dependency files```
```

### Adding the module to an exisiting project

#### IPbus Builder Project

- Go to the project folder:
    > `cd your_project_folder`
- Add this repository
    > `ipbb add git ssh://git@gitlab.cern.ch:7999/myharank/bril_histogram.git`
- In the dependency file of your top project add a line:
    - For an instance **with** the accumulation layer:
        > `include -c bril_histogram:components/bril_histogram bril_histogram_with_accumulation_sync.dep`

        or

        > `include -c bril_histogram:components/bril_histogram bril_histogram_with_accumulation_async.dep`
    - For an instance **without** the accumulation layer (just a wrapper):
        > `include -c bril_histogram:components/bril_histogram bril_histogram_basic_sync.dep` 

        or

        > `include -c bril_histogram:components/bril_histogram bril_histogram_basic_async.dep`

#### Any Vivado Project

- Open Vivado
- Switch to the TCL console
- Go to the folder with bril histogram tcl files
  > `cd your_project_folder/bril_histogram/components/bril_histogram/firmware/tcl`
- Source the TCL file matching your request:
    - For an instance **with** the accumulation layer:
        > `source ./import_bril_histogram_with_accumulation_sync.tcl`

        or

        > `source ./import_bril_histogram_with_accumulation_async.tcl`
    - For an instance **without** the accumulation layer (just a wrapper):
        > `source ./import_bril_histogram_basic_sync.tcl`

        or

        > `source ./import_bril_histogram_basic_async.tcl`

### Instantiating the synchronous implementation

#### With accumulation

Example of instantiation of the **synchronous** module **with** the accumulation layer (scroll down for pin description):

```bash
bril_histogram_accumulation_layer_sync_inst: entity work.bril_histogram_accumulation_layer_sync
	generic map (
        -- identification of the histogram
        HISTOGRAM_TYPE                          => HISTOGRAM_TYPE,
        HISTOGRAM_ID                            => HISTOGRAM_ID,
        -- memory
        GENERATE_FIFO                           => GENERATE_FIFO,
        -- histogram parameters
        NUMBER_OF_BINS                          => NUMBER_OF_BINS,
        COUNTER_WIDTH                           => COUNTER_WIDTH,
        INCREMENT_WIDTH                         => INCREMENT_WIDTH,
        -- counter width
        ORBIT_BX_COUNTER_WIDTH			=> ORBIT_BX_COUNTER_WIDTH,
        -- accumulation parameters
        NUMBER_OF_UNITS                         => NUMBER_OF_UNITS,
        UNIT_INCREMENT_WIDTH                    => UNIT_INCREMENT_WIDTH,
        NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE  => NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE,
        UNIT_ERROR_COUNTER_WIDTH                => UNIT_ERROR_COUNTER_WIDTH,
        STORE_ERRORS                            => STORE_ERRORS
    )
    port map(
        -- core signals
        memory_reset_i                  => memory_reset,
        reset_i                         => reset_histogram,
        clk_i                           => clk_i,
        -- tcds signals
        new_histogram_i                 => bril_new_histogram,
        new_iteration_i                 => bril_new_iteration,
        -- tcds data
        lhc_fill_i                      => tcds.lhc_fill,
        cms_run_i                       => tcds.cms_run,
        lumi_section_i                  => tcds.lumi_section,
        lumi_nibble_i                   => tcds.lumi_nibble,
        -- counter signals  
        increment_i                     => bril_increment,
        error_i                         => bril_error,
        mask_i                          => bril_mask,
        -- fifo interface (optional)
        fifo_rd_clk_i                   => bril_fifo_rd_clk_i,
        fifo_rd_en_i                    => bril_fifo_rd_en_i,
        fifo_empty_o                    => bril_fifo_empty_o,
        fifo_words_cnt_o                => bril_fifo_words_cnt_o,
        --
        data_o                          => bril_data_o,
        data_valid_o                    => bril_data_valid_o
    );
```

#### Without accumulation

Example of instantiation of the **synchronous** module **without** the accumulation layer (scroll down for pin description):

```bash
bril_histogram_wrapper_sync_inst: entity work.bril_histogram_wrapper_sync
	generic map (
        -- identification of the histogram
        HISTOGRAM_TYPE                  => HISTOGRAM_TYPE,
        HISTOGRAM_ID                    => HISTOGRAM_ID,
        -- memory
        GENERATE_FIFO                   => GENERATE_FIFO,
        -- histogram parameters
        NUMBER_OF_BINS                  => NUMBER_OF_BINS,
        COUNTER_WIDTH                   => COUNTER_WIDTH,
        INCREMENT_WIDTH                 => INCREMENT_WIDTH
    )
    port map(
        -- core signals
        memory_reset_i                  => memory_reset_i,
        reset_i                         => reset_i,
        clk_i                           => clk_i,
        -- tcds signals
        new_histogram_i                 => bril_new_histogram,
        new_iteration_i                 => bril_new_iteration,
        -- tcds data
        lhc_fill_i                      => tcds.lhc_fill,
        cms_run_i                       => tcds.cms_run,
        lumi_section_i                  => tcds.lumi_section,
        lumi_nibble_i                   => tcds.lumi_nibble,
        -- counter signals  
        increment_i                     => bril_increment,          
        -- fifo interface (optional)
        fifo_rd_clk_i                   => bril_fifo_rd_clk_i,   
        fifo_rd_en_i                    => bril_fifo_rd_en_i,    
        fifo_empty_o                    => bril_fifo_empty_o,    
        fifo_words_cnt_o                => bril_fifo_words_cnt_o,
        --
        data_o                          => bril_data_o,
        data_valid_o                    => bril_data_valid_o
    );
```

#### Port Description

Descriptiong of the module generics:

<table class="tg">
  <tr>
    <th class="tg-9wq8">Generic Port Name</th>
    <th class="tg-9wq8">Type</th>
    <th class="tg-9wq8">Used<br>when no<br>accumulation<br>layer</th>
    <th class="tg-baqh">Description</th>
  </tr>
  <tr>
    <td class="tg-lboi">HISTOGRAM_TYPE</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">Y</td>
    <td class="tg-0lax">Reserved for the type of the histogram, range (0 to 63)</td>
  </tr>
  <tr>
    <td class="tg-lboi">HISTOGRAM_ID</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">Y</td>
    <td class="tg-0lax">Reserved for the ID of the histogram range (0 to 1023)</td>
  </tr>
  <tr>
    <td class="tg-lboi">GENERATE_FIFO</td>
    <td class="tg-lboi">boolean</td>
    <td class="tg-9wq8">Y</td>
    <td class="tg-0lax">Generate readout FIFO or not</td>
  </tr>
  <tr>
    <td class="tg-lboi">NUMBER_OF_BINS</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">Y</td>
    <td class="tg-0lax">Number of bins in the histogram (number of clock cycles between new_iteration_i signals)</td>
  </tr>
  <tr>
    <td class="tg-lboi">COUNTER_WIDTH</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">Y</td>
    <td class="tg-0lax">Width of the counter, range (1 to 32)</td>
  </tr>
  <tr>
    <td class="tg-lboi">INCREMENT_WIDTH</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">Y</td>
    <td class="tg-0lax">Width of the increment, range (1 to ...), 2**INCREMENT_WIDTH-1 is the maximal increment per clock cycle </td>
  </tr>
  <tr>
    <td class="tg-lboi">ORBIT_BX_COUNTER_WIDTH</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">N</td>
    <td class="tg-0lax">Use 16. Width internally generated orbit/bx counter later stored in the error block </td>
  </tr>
  <tr>
    <td class="tg-lboi">NUMBER_OF_UNITS</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">N</td>
    <td class="tg-0lax">Number of connected units (modules, chips, channels)</td>
  </tr>
  <tr>
    <td class="tg-lboi">UNIT_INCREMENT_WIDTH</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">N</td>
    <td class="tg-0lax">Maximal increment per each units; NUMBER_OF_UNITS * (2**UNIT_INCREMENT_WIDTH - 1) should (but not must) be less than 2**INCREMENT_WIDTH-1</td>
  </tr>
  <tr>
    <td class="tg-lboi">NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">N</td>
    <td class="tg-0lax">"4" is the golden value. Defines the complexity of the accumulation (number of additions per clock cycle).</td>
  </tr>
  <tr>
    <td class="tg-0lax">UNIT_ERROR_COUNTER_WIDTH</td>
    <td class="tg-0lax">natural</td>
    <td class="tg-baqh">N</td>
    <td class="tg-0lax">2**UNIT_ERROR_COUNTER_WIDTH is the maximal number of errors that will be processed from a given units, further errors will be suppressed, module will be disabled for the subsequent histograms (nibbles)</td>
  </tr>
  <tr>
    <td class="tg-0lax">STORE_ERRORS</td>
    <td class="tg-0lax">boolean</td>
    <td class="tg-baqh">N</td>
    <td class="tg-0lax">False will suppress all errors from being stored in the event frame. However, automatic disabling of the faulty modules is still enabled and configured by the previous constant</td>
  </tr>
</table>

Description of the module ports:
<table class="tg">
  <tr>
    <th class="tg-c3ow">Port Name</th>
    <th class="tg-c3ow">Type</th>
    <th class="tg-c3ow">I/O</th>
    <th class="tg-c3ow">Used<br>when no<br>accumulation<br>layer</th>
    <th class="tg-c3ow">Description</th>
  </tr>
  <tr>
    <td class="tg-0pky">memory_reset_i</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Active-high reset of the FIFOs, has to be released at least 8 clk_i cycles before releasing the reset_i</td>
  </tr>
  <tr>
    <td class="tg-0pky">reset_i</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Active-high reset of the histogram module. Kept high until data collection begins. The following bunch crossing after releasing reset_i is BX0</td>
  </tr>
  <tr>
    <td class="tg-0pky">clk_i</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Free-running clock, maximum frequency is 320MHz. All the input signals (apart from FIFO control) are synchronised to this clock</td>
  </tr>
  <tr>
    <td class="tg-0pky">new_histogram_i</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Also known as new_nibble - start the new iteration of the histogram, normally sent on BX3563 together with new_iteration_i (next clock cycle is orbit 0, bx0)</td>
  </tr>
  <tr>
    <td class="tg-0pky">new_iteration_i</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Also known as bx0 - the next clock cycle is bx0 (typically sent at BX3563)</td>
  </tr>
  <tr>
    <td class="tg-0pky">lhc_fill_i</td>
    <td class="tg-0pky">std_logic_vector(31 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">LHC Fill Number - stored by the module when new_histogram_i is received</td>
  </tr>
  <tr>
    <td class="tg-0pky">cms_run_i</td>
    <td class="tg-0pky">std_logic_vector(31 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">CMS Run Number <span style="font-weight:400;font-style:normal">- stored by the module when new_histogram_i is received</span></td>
  </tr>
  <tr>
    <td class="tg-0pky">lumi_section_i</td>
    <td class="tg-0pky">std_logic_vector(31 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Lumi Section <span style="font-weight:400;font-style:normal">- stored by the module when new_histogram_i is received</span></td>
  </tr>
  <tr>
    <td class="tg-0pky">lumi_nibble_i</td>
    <td class="tg-0pky">std_logic_vector(31 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Lumi Nibble - - stored by the module when new_histogram_i is received</td>
  </tr>
  <tr>
    <td class="tg-0pky">increment_i</td>
    <td class="tg-0pky">std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0) - accumulation level<br>or<br>std_logic_vector(INCREMENT_WIDTH-1 downto 0) - wrapper level</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y;<br>different<br>width</td>
    <td class="tg-0pky">Increment signal from N units (modules, chips, channels). </td>
  </tr>
  <tr>
    <td class="tg-0pky">error_i</td>
    <td class="tg-0pky">std_logic_vector(NUMBER_OF_UNITS-1 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">N</td>
    <td class="tg-0pky">Error signal from N units</td>
  </tr>
  <tr>
    <td class="tg-0pky">mask_i</td>
    <td class="tg-0pky">std_logic_vector(NUMBER_OF_UNITS-1 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">N</td>
    <td class="tg-0pky">Mask configuration from N units (disabled modules)</td>
  </tr>
  <tr>
    <td class="tg-0pky">fifo_rd_clk_i</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y;<br>if with<br>FIFO</td>
    <td class="tg-0pky">FIFO read clock; for example, IPbus clock</td>
  </tr>
  <tr>
    <td class="tg-0pky">fifo_rd_en_i</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y;<br>if with<br>FIFO</td>
    <td class="tg-0pky">FIFO read enable; must be synchronous to fifo_rd_clk_i</td>
  </tr>
  <tr>
    <td class="tg-0pky">fifo_empty_o</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">O</td>
    <td class="tg-c3ow">Y;<br>if with<br>FIFO</td>
    <td class="tg-0pky">FIFO empty signal; synchronous to fifo_rd_clk_i</td>
  </tr>
  <tr>
    <td class="tg-0pky">fifo_words_cnt_o</td>
    <td class="tg-0pky">std_logic_vector(31 downto 0)</td>
    <td class="tg-c3ow">O</td>
    <td class="tg-c3ow">Y;<br>if with<br>FIFO</td>
    <td class="tg-0pky">Number of words available in FIFO; synchronous to fifo_rd_clk_i</td>
  </tr>
  <tr>
    <td class="tg-0pky">data_o</td>
    <td class="tg-0pky">std_logic_vector(31 downto 0)</td>
    <td class="tg-c3ow">O</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Data output. In case direct streaming sent independently together with data_valid_o signal, receiver has to accept all the data (no back pressure possible). In case of GENERATE_FIFO=TRUE - output of the FIFO; NOTE(!) the FIFO is First Word Fall Through (FWFT)</td>
  </tr>
  <tr>
    <td class="tg-0pky">data_valid_o</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">O</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Data valid signal from streamer, or from the FIFO</td>
  </tr>
</table>

### Instantiating the asynchronous implementation

#### With accumulation

Example of instantiation of the **asynchronous** module **with** the accumulation layer (scroll down for pin description):

```bash
bril_histogram_accumulation_layer_async_inst: entity work.bril_histogram_accumulation_layer_async
    generic map (
        -- identification of the histogram
        HISTOGRAM_TYPE                          => HISTOGRAM_TYPE,
        HISTOGRAM_ID                            => HISTOGRAM_ID,
        -- memory
        GENERATE_FIFO                           => GENERATE_FIFO,
        -- histogram parameters
        NUMBER_OF_BINS                          => NUMBER_OF_BINS,
        COUNTER_WIDTH                           => COUNTER_WIDTH,
        INCREMENT_WIDTH                         => INCREMENT_WIDTH,
        -- counter width
        ORBIT_BX_COUNTER_WIDTH			            => ORBIT_BX_COUNTER_WIDTH,
        -- accumulation parameters
        NUMBER_OF_UNITS                         => NUMBER_OF_UNITS,
        UNIT_INCREMENT_WIDTH                    => UNIT_INCREMENT_WIDTH,
        NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE  => NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE,
        UNIT_ERROR_COUNTER_WIDTH                => UNIT_ERROR_COUNTER_WIDTH,
        STORE_ERRORS                            => STORE_ERRORS 
    )
    port map(
        -- core signals
        reset_i                         => reset_histogram,
        clk_i                           => clk_i,   
        -- tcds data
        lhc_fill_i                      => tcds.lhc_fill,
        cms_run_i                       => tcds.cms_run,
        lumi_section_i                  => tcds.lumi_section,
        lumi_nibble_i                   => tcds.lumi_nibble,
        -- control signals
        valid_i		  		                => bril_valid,
        iteration_id_i                  => bril_iteration_id,
        bin_id_i                        => bril_bin_id,
        increment_i                     => bril_increment,
        error_i                         => bril_error,
        mask_i                          => bril_mask,
        -- fifo interface
        fifo_rd_clk_i                   => bril_fifo_rd_clk_i,
        fifo_rd_en_i                    => bril_fifo_rd_en_i,
        fifo_empty_o                    => bril_fifo_empty_o,
        fifo_words_cnt_o                => bril_fifo_words_cnt_o,
        fifo_data_o                     => bril_fifo_data_o,
        fifo_data_valid_o               => bril_fifo_data_valid_o,
        -- module is ready
        init_done_o			                => bril_init_done
    );
```

#### Without accumulation

Example of instantiation of the **synchronous** module **without** the accumulation layer (scroll down for pin description):

```bash
bril_histogram_wrapper_async_inst: entity work.bril_histogram_wrapper_async
     generic map (
        -- identification of the histogram
        HISTOGRAM_TYPE                  => HISTOGRAM_TYPE,
        HISTOGRAM_ID                    => HISTOGRAM_ID,
        -- memory
        ORBIT_BX_COUNTER_WIDTH          => ORBIT_BX_COUNTER_WIDTH,
        -- histogram parameters
        NUMBER_OF_BINS                  => NUMBER_OF_BINS,
        COUNTER_WIDTH                   => COUNTER_WIDTH,
        INCREMENT_WIDTH                 => INCREMENT_WIDTH
    )
    port map(
        -- core signals
        reset_i                         => reset_i,
        clk_i                           => clk_i,
        -- tcds data
        lhc_fill_i                      => tcds.lhc_fill,
        cms_run_i                       => tcds.cms_run,
        lumi_section_i                  => tcds.lumi_section,
        lumi_nibble_i                   => tcds.lumi_nibble,
        -- counter signals  
        valid_i		  		                => bril_valid,
        iteration_id_i                  => bril_iteration_id,
        bin_id_i                        => bril_bin_id,
        increment_i                     => bril_increment,          
        -- fifo interface
        fifo_rd_clk_i                   => bril_fifo_rd_clk_i,   
        fifo_rd_en_i                    => bril_fifo_rd_en_i,    
        fifo_empty_o                    => bril_fifo_empty_o,    
        fifo_words_cnt_o                => bril_fifo_words_cnt_o,
        fifo_data_o                     => bril_fifo_data_o,
        fifo_data_valid_o               => bril_fifo_data_valid_o,
        -- module is ready
        init_done_o			                => bril_init_done
    );
```

#### Port Description

Descriptiong of the module generics:

<table class="tg">
  <tr>
    <th class="tg-9wq8">Generic Port Name</th>
    <th class="tg-9wq8">Type</th>
    <th class="tg-9wq8">Used<br>when no<br>accumulation<br>layer</th>
    <th class="tg-baqh">Description</th>
  </tr>
  <tr>
    <td class="tg-lboi">HISTOGRAM_TYPE</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">Y</td>
    <td class="tg-0lax">Reserved for the type of the histogram, range (0 to 63)</td>
  </tr>
  <tr>
    <td class="tg-lboi">HISTOGRAM_ID</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">Y</td>
    <td class="tg-0lax">Reserved for the ID of the histogram range (0 to 1023)</td>
  </tr>
  <tr>
    <td class="tg-lboi">NUMBER_OF_BINS</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">Y</td>
    <td class="tg-0lax">Number of bins in the histogram</td>
  </tr>
  <tr>
    <td class="tg-lboi">COUNTER_WIDTH</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">Y</td>
    <td class="tg-0lax">Width of the counter, range (1 to 32)</td>
  </tr>
  <tr>
    <td class="tg-lboi">INCREMENT_WIDTH</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">Y</td>
    <td class="tg-0lax">Width of the increment, range (1 to ...), 2**INCREMENT_WIDTH-1 is the maximal increment per valid signal </td>
  </tr>
  <tr>
    <td class="tg-lboi">ORBIT_BX_COUNTER_WIDTH</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">Y</td>
    <td class="tg-0lax">Use 16. Width internally generated orbit/bx counter later stored in the error block </td>
  </tr>
  <tr>
    <td class="tg-lboi">NUMBER_OF_UNITS</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">N</td>
    <td class="tg-0lax">Number of connected units (modules, chips, channels)</td>
  </tr>
  <tr>
    <td class="tg-lboi">UNIT_INCREMENT_WIDTH</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">N</td>
    <td class="tg-0lax">Maximal increment per each units; NUMBER_OF_UNITS * (2**UNIT_INCREMENT_WIDTH - 1) should (but not must) be less than 2**INCREMENT_WIDTH-1</td>
  </tr>
  <tr>
    <td class="tg-lboi">NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE</td>
    <td class="tg-lboi">natural</td>
    <td class="tg-9wq8">N</td>
    <td class="tg-0lax">"4" is the golden value. Defines the complexity of the accumulation (number of additions per clock cycle).</td>
  </tr>
  <tr>
    <td class="tg-0lax">UNIT_ERROR_COUNTER_WIDTH</td>
    <td class="tg-0lax">natural</td>
    <td class="tg-baqh">N</td>
    <td class="tg-0lax">2**UNIT_ERROR_COUNTER_WIDTH is the maximal number of errors that will be processed from a given units, further errors will be suppressed, module will be disabled for the subsequent histograms (nibbles)</td>
  </tr>
  <tr>
    <td class="tg-0lax">STORE_ERRORS</td>
    <td class="tg-0lax">boolean</td>
    <td class="tg-baqh">N</td>
    <td class="tg-0lax">False will suppress all errors from being stored in the event frame. However, automatic disabling of the faulty modules is still enabled and configured by the previous constant</td>
  </tr>
</table>

Description of the module ports:
<table class="tg">
  <tr>
    <th class="tg-c3ow">Port Name</th>
    <th class="tg-c3ow">Type</th>
    <th class="tg-c3ow">I/O</th>
    <th class="tg-c3ow">Used<br>when no<br>accumulation<br>layer</th>
    <th class="tg-c3ow">Description</th>
  </tr>
  <tr>
    <td class="tg-0pky">reset_i</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Active-high reset of the histogram module. Has to be released approximately NUMBER_OF_BINS bunch-crossings before starting the data taking (please, refer to the description of the init_done_o signal)</td>
  </tr>
  <tr>
    <td class="tg-0pky">clk_i</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Free-running clock, maximum frequency is 320MHz. Has to be 4 times faster than the sampling frequency. Means if your data comes at 40MHz, clk_i clock has to be at least 160MHz, valid signals should come at maximum once every 4 clock cycles. All the input signals (apart from FIFO control) are synchronised to this clock.</td>
  </tr>
  <tr>
    <td class="tg-0pky">lhc_fill_i</td>
    <td class="tg-0pky">std_logic_vector(31 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">LHC Fill Number - value for the PREVIOUS histogram stored by the module around orbit=0 bx=0 (assuiming that the tcds data normally takes some time to be transferred)</td>
  </tr>
  <tr>
    <td class="tg-0pky">cms_run_i</td>
    <td class="tg-0pky">std_logic_vector(31 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">CMS Run Number - value for the PREVIOUS histogram stored by the module around orbit=0 bx=0 (assuiming that the tcds data normally takes some time to be transferred)</td>
  </tr>
  <tr>
    <td class="tg-0pky">lumi_section_i</td>
    <td class="tg-0pky">std_logic_vector(31 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Lumi Section - value for the PREVIOUS histogram stored by the module around orbit=0 bx=0 (assuiming that the tcds data normally takes some time to be transferred)</td>
  </tr>
  <tr>
    <td class="tg-0pky">lumi_nibble_i</td>
    <td class="tg-0pky">std_logic_vector(31 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Lumi Nibble - value for the PREVIOUS histogram stored by the module around orbit=0 bx=0 (assuiming that the tcds data normally takes some time to be transferred)</td>
  </tr>
  <tr>
    <td class="tg-0pky">valid_i</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Data valid signal. Can be sent at fastest one in 4 clk_i clock cycles due to processing times. </td>
  </tr>
  <tr>
    <td class="tg-0pky">iteration_id_i</td>
    <td class="tg-0pky">std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Orbit id of the coming increment data. Has to be sent synchronous to the valid_i signal.</td>
  </tr>
  <tr>
    <td class="tg-0pky">bin_id_i</td>
    <td class="tg-0pky">std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Bunch-crossing id of the coming increment data. Has to be sent synchronous to the valid_i signal.</td>
  </tr>
  <tr>
    <td class="tg-0pky">increment_i</td>
    <td class="tg-0pky">std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0) - accumulation level<br>or<br>std_logic_vector(INCREMENT_WIDTH-1 downto 0) - wrapper level</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y;<br>different<br>width</td>
    <td class="tg-0pky">Increment signal from N units (modules, chips, channels). Has to be sent synchronous to the valid_i signal. </td>
  </tr>
  <tr>
    <td class="tg-0pky">error_i</td>
    <td class="tg-0pky">std_logic_vector(NUMBER_OF_UNITS-1 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">N</td>
    <td class="tg-0pky">Error signal from N units. Has to be sent synchronous to the valid_i signal.</td>
  </tr>
  <tr>
    <td class="tg-0pky">mask_i</td>
    <td class="tg-0pky">std_logic_vector(NUMBER_OF_UNITS-1 downto 0)</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">N</td>
    <td class="tg-0pky">Mask configuration from N units (disabled modules)</td>
  </tr>
  <tr>
    <td class="tg-0pky">fifo_rd_clk_i</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y;<br>if with<br>FIFO</td>
    <td class="tg-0pky">FIFO read clock; for example, IPbus clock</td>
  </tr>
  <tr>
    <td class="tg-0pky">fifo_rd_en_i</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">I</td>
    <td class="tg-c3ow">Y;<br>if with<br>FIFO</td>
    <td class="tg-0pky">FIFO read enable; must be synchronous to fifo_rd_clk_i</td>
  </tr>
  <tr>
    <td class="tg-0pky">fifo_empty_o</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">O</td>
    <td class="tg-c3ow">Y;<br>if with<br>FIFO</td>
    <td class="tg-0pky">FIFO empty signal; synchronous to fifo_rd_clk_i</td>
  </tr>
  <tr>
    <td class="tg-0pky">fifo_words_cnt_o</td>
    <td class="tg-0pky">std_logic_vector(31 downto 0)</td>
    <td class="tg-c3ow">O</td>
    <td class="tg-c3ow">Y;<br>if with<br>FIFO</td>
    <td class="tg-0pky">Number of words available in FIFO; synchronous to fifo_rd_clk_i</td>
  </tr>
  <tr>
    <td class="tg-0pky">fifo_data_o</td>
    <td class="tg-0pky">std_logic_vector(31 downto 0)</td>
    <td class="tg-c3ow">O</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">FIFO data output. NOTE(!) the FIFO is First Word Fall Through (FWFT); synchronous to fifo_rd_clk_i</td>
  </tr>
  <tr>
    <td class="tg-0pky">fifo_data_valid_o</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">O</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Data valid signal from the FIFO; synchronous to fifo_rd_clk_i</td>
  </tr>
  <tr>
    <td class="tg-0pky">init_done_o</td>
    <td class="tg-0pky">std_logic</td>
    <td class="tg-c3ow">O</td>
    <td class="tg-c3ow">Y</td>
    <td class="tg-0pky">Ready signal for the histogramming block. NOTE (!) it takes around NUMBER_OF_BINS clock cycles to initialize the memory after reset_i is released. NEVER send data before this gets high - it will be ignored. </td>
  </tr>
</table>

### Firmware readout interface

It is up to the developer to implement any readout interface for the BRIL histograms. At the end, the question is to properly read the fifo. An IPbus slave is already implemented as a part of the repository.

#### IPbus

This guide assumes the developer to be already familiar with IPbus. In order to add the slave to the project one should:
- In an IPbus Builder project:
    > `include -c bril_histogram:components/bril_histogram bril_histogram_readout_ipbus.dep`
- In a tcl-managed project:
    - Go the tcl folder
      > `cd your_project_folder/bril_histogram/components/bril_histogram/firmware/tcl` 
    - Source the tcl file
      > `source ./bril_histogram_readout_ipbus.tcl`

In addition to the above mentioned module instantion, instantiate the ipbus slave:
```bash
readout_regs_inst: entity work.bril_histogram_readout_ipbus
port map(
	clk				=> ipb_clk, -- ipbus clock                            
	reset				=> reset_ipb, -- reset in the ipbus clock domain                 
	ipbus_in			=> ipb_mosi_i(SLV_BRIL_HISTOGRAM_READOUT),  -- ipbus input (SLV_BRIL_HISTOGRAM_READOUT is the slave id)
	ipbus_out			=> ipb_miso_o(SLV_BRIL_HISTOGRAM_READOUT),  -- ipbus output (SLV_BRIL_HISTOGRAM_READOUT is the slave id)
	-- fifo status
	fifo_empty_i                    => bril_fifo_empty, -- connect to the respective bril histogram port
	fifo_data_valid_i               => bril_data_valid, -- connect to the respective bril histogram port
	fifo_words_cnt_i                => bril_fifo_words_cnt, -- connect to the respective bril histogram port
	-- fifo read interfac          
	fifo_rd_en_o                    => bril_fifo_rd_en, -- connect to the respective bril histogram port
	fifo_data_i                     => bril_data -- connect to the respective bril histogram port
);
``` 

The address table to be included in the project address table is located in ```your_project_folder/bril_histogram/components/bril_histogram/addr_table```

### Software readout interface

The histogram parsing module is implemented in C++ and located in ```your_project_folder/bril_histogram/components/bril_histogram/software```. It is represented by a class called BrilHistogram which accepts std::vector received from the FIFO and containing a single histogram. The parser is interface-agnostic. In order to compile the parser one should:
- Go to the software folder
  > `your_project_folder/bril_histogram/components/bril_histogram/software`
- Compile
  > `make BrilHistogram`

Python bindings are also provided to simplify testing and can be compiled using (previous step is not necessary in this case):
  > `make py`

## APPENDIX: Building the FC7 test bench project from scratch

- Activate the IPbus Builder environment:
    > `source /path_to_the_ipbb/env.sh`
- Go to the location where you want to build the project
- Init the project (you can use any desired name):
    > `ipbb init bril_histo_ipbb`
- Switch to the project directory:
    > `cd bril_histo_ipbb`
- Checkout the FC7 repository:
    > `ipbb add git ssh://git@gitlab.cern.ch:7999/myharank/fc7-firmware.git -b ipbb_dev`
- Checkout the IPbus v1.4 (required by the fc7 firmware):
    > `ipbb add git https://github.com/ipbus/ipbus-firmware -b v1.4`
- Checkout this repository:
    > `ipbb add git ssh://git@gitlab.cern.ch:7999/myharank/bril_histogram.git`
- Create the project using the FC7 top module provided in boards/fc7:
    > `ipbb proj create vivado bril_histogram_tb bril_histogram:boards/fc7`
- Go to the project folder:
    > `cd proj/bril_histogram_tb`
- Create the project (keyword natural is used to order the constraint files in the right way):
    > `ipbb vivado project --natural`
- Optionally - open Vivado and change the configuration in ```bril_histogram_fc7_injector_package```
- Finally, one can run project synthesis/implementation in the Vivado framework

Bonus: 
- Synthesis/implementation using the command line:
    > `ipbb vivado synth -j4 impl -j4`
- And prepare a bitstream:
    > `ipbb vivado package`
