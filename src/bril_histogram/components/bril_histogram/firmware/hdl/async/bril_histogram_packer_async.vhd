----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_core_async - rtl
-- Description: 
--
--     Prepends header and appends error data, also implements a hand-made fifo interface to read data and reset the memory content
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- user macros to create cdc
library xpm;
use xpm.vcomponents.all;

-- bril package
use work.bril_histogram_package.all;

entity bril_histogram_packer_async is
Generic (
    -- identification of the histogram
    HISTOGRAM_TYPE                  : natural;
    HISTOGRAM_ID                    : natural;
    -- memory parameters
    MEMORY_BLOCK_PARAMETERS         : memory_block_parameters_type;
    -- histogram parameters
    NUMBER_OF_BINS                  : natural;
    COUNTER_WIDTH                   : natural;
    INCREMENT_WIDTH                 : natural;
    -- counter width
    ORBIT_BX_COUNTER_WIDTH          : natural;
    -- error parameters
    NUMBER_OF_UNITS                 : natural; -- number of units (modules, chips) connected to the histogrammer, can be zero without the accumulation layers
    NUMBER_OF_WORDS_PER_ERROR       : natural;
    MAX_NUMBER_OF_MASKERROR_WORDS   : natural;
    -- 
    BRAM_OUTPUT_DELAY               : natural
);
Port(
    -- core signals
    reset_i                         : in std_logic; -- sync reset
    clk_i                           : in std_logic; -- processing clock - has to be BRAM_OUTPUT_DELAY+1 times faster then 40Mhz
    -- new histogram detected by the wrapper
    new_histogram_i                 : in std_logic; -- to store external parameters
    -- histogram info
    increment_overflow_i            : in std_logic;
    iteration_counter_i             : in std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    overflow_i                      : in std_logic;
    -- memory interface (clk i)
    ram_addr_a_o                    : out natural range 0 to MEMORY_BLOCK_PARAMETERS.total_bram_size_async-1;  
    ram_din_a_o                     : out std_logic_vector(MEMORY_BLOCK_PARAMETERS.bram_word_width-1 downto 0); 
    ram_we_a_o                      : out std_logic_vector(MEMORY_BLOCK_PARAMETERS.bram_word_width/8-1 downto 0);
    ram_dout_a_i                    : in std_logic_vector(MEMORY_BLOCK_PARAMETERS.bram_word_width-1 downto 0);  
    -- memory interface (fifo clk)
    ram_addr_b_o                    : out natural range 0 to MEMORY_BLOCK_PARAMETERS.total_bram_size_async*(MEMORY_BLOCK_PARAMETERS.bram_word_width/32)-1;  
    ram_din_b_o                     : out std_logic_vector(31 downto 0); 
    ram_we_b_o                      : out std_logic;
    ram_dout_b_i                    : in std_logic_vector(31 downto 0);  
    -- error interface
    err_bl_n_msk_err_words_i        : in std_logic_vector(15 downto 0) := (others => '0');
    err_bl_err_word_i               : in std_logic_vector(31 downto 0) := (others => '0');
    err_bl_rd_en_o                  : out std_logic;
    -- tcds data
    lhc_fill_i                      : in std_logic_vector(31 downto 0);
    cms_run_i                       : in std_logic_vector(31 downto 0);
    lumi_section_i                  : in std_logic_vector(31 downto 0);
    lumi_nibble_i                   : in std_logic_vector(31 downto 0);
    -- fifo interface
    fifo_rd_clk_i                   : in std_logic;
    fifo_rd_en_i                    : in std_logic;
    fifo_empty_o                    : out std_logic;
    fifo_words_cnt_o                : out std_logic_vector(31 downto 0);
    fifo_data_o                     : out std_logic_vector(31 downto 0);
    fifo_data_valid_o               : out std_logic;
    fifo_not_empty_o                : out std_logic -- fifo not ready to accept new histogram
);
end bril_histogram_packer_async;

architecture rtl of bril_histogram_packer_async is

    -- constant for the cdc
    constant CDC_STAGES                             : natural := 4;
    -- for simplicaty calcylate word width fracntion
    constant NUMBER_OF_32BIT_WORDS_PER_MEMORY_CELL  : natural := MEMORY_BLOCK_PARAMETERS.bram_word_width/32;
    constant ERROR_OFFSET                           : natural := (MEMORY_BLOCK_PARAMETERS.HEADER_SIZE + MEMORY_BLOCK_PARAMETERS.TOTAL_COUNTER_WORDS)/NUMBER_OF_32BIT_WORDS_PER_MEMORY_CELL;
    constant ERROR_FIRST_SUB_WORD_ID                : natural := (MEMORY_BLOCK_PARAMETERS.HEADER_SIZE + MEMORY_BLOCK_PARAMETERS.TOTAL_COUNTER_WORDS) rem NUMBER_OF_32BIT_WORDS_PER_MEMORY_CELL;
    

    -- state of the state machine
    type fsm_state_type is (Idle, WaitErrors, StoringHeader, CheckErrors, StoringErrors, WaitForReadout);
    signal fsm_state                        : fsm_state_type;
    
    -- first histogram after reset
    signal if_first_histogram               : std_logic;
    
    -- buffer some values which may change
    signal iteration_counter_buffered       : std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0);
    signal increment_overflow_buffered      : std_logic;
    signal overflow_buffered                : std_logic;
    signal lhc_fill_buffered                : std_logic_vector(31 downto 0);
    signal cms_run_buffered                 : std_logic_vector(31 downto 0);
    signal lumi_section_buffered            : std_logic_vector(31 downto 0);
    signal lumi_nibble_buffered             : std_logic_vector(31 downto 0);
    signal number_of_error_words_buffered   : std_logic_vector(15 downto 0);
    
    -- assemble header data (will be easier to get write it
    signal header_data                      : register_block_32bit_type(MEMORY_BLOCK_PARAMETERS.HEADER_SIZE-1 downto 0);

    -- sotring counter
    signal sub_word_id                      : natural range 0 to NUMBER_OF_32BIT_WORDS_PER_MEMORY_CELL-1;
    signal addr_a_counter                   : natural range 0 to MEMORY_BLOCK_PARAMETERS.total_bram_size_async-1;
    
    -- fifo data handshake
    signal data_ready                       : std_logic;
    signal data_ack                         : std_logic;
    
    -- fifo buffering
    type fifo_fsm_state_type is (FifoNoData, FifoFillBuffer, FifoWaitBuffer, FifoReadout);
    signal fifo_fsm_state                   : fifo_fsm_state_type;
    signal fifo_words_cnt_int               : std_logic_vector(31 downto 0);
    signal fifo_not_empty_int               : std_logic;
    signal reset_fifo_clk                   : std_logic;
    signal data_ready_fifo_clk              : std_logic;
    signal data_ack_fifo_clk                : std_logic;
    signal fifo_buffer                      : register_block_32bit_type(BRAM_OUTPUT_DELAY downto 0);
    signal words_in_buffer                  : natural range 0 to BRAM_OUTPUT_DELAY+1;
    signal bram_not_empty                   : std_logic;
    -- buffer control
    signal read_request_from_fsm            : std_logic;
    signal read_request                     : std_logic;
    signal read_request_delayed             : std_logic; -- defines when to update the fifo buffer
    
    -- fifo bram interface
    signal ram_addr_b_int                   : natural range 0 to MEMORY_BLOCK_PARAMETERS.total_bram_size_async-1;

begin

    --=====================================--
    -- assemble header data (this signals are not changing as they have already been buffered
    --=====================================--
    header_data(0) <= x"FF" & std_logic_vector(to_unsigned(MEMORY_BLOCK_PARAMETERS.header_size, 8)) & std_logic_vector(to_unsigned(MEMORY_BLOCK_PARAMETERS.header_size+MEMORY_BLOCK_PARAMETERS.total_counter_words+to_integer(unsigned(number_of_error_words_buffered)), 16)); -- 12 bits for xFFF pattern, 8 bits for theheader size and 16 bits for total size of the data
    header_data(1) <= std_logic_vector(to_unsigned(HISTOGRAM_TYPE, 6)) & std_logic_vector(to_unsigned(HISTOGRAM_ID, 10)) & std_logic_vector(to_unsigned(NUMBER_OF_BINS, 16)); -- second word of the header, first in the que
    header_data(2) <= std_logic_vector(to_unsigned(COUNTER_WIDTH, 8)) & std_logic_vector(to_unsigned(INCREMENT_WIDTH, 8)) & std_logic_vector(to_unsigned(MEMORY_BLOCK_PARAMETERS.total_counter_words, 16));
    header_data(3) <= overflow_buffered & increment_overflow_buffered & "0000000000" & append_zeros_left(iteration_counter_buffered+1,20); -- counter of the iterations - has to be +1 as it was not incremented yet
    header_data(4) <= lhc_fill_buffered;
    header_data(5) <= cms_run_buffered;
    header_data(6) <= lumi_section_buffered;
    header_data(7) <= lumi_nibble_buffered;
    header_data(8) <= std_logic_vector(to_unsigned(NUMBER_OF_WORDS_PER_ERROR, 8)) & std_logic_vector(to_unsigned(NUMBER_OF_UNITS, 8)) & number_of_error_words_buffered;
    
    --=====================================--
    -- header and error processore
    --=====================================--
    process (clk_i)
    begin
        if rising_edge(clk_i) then
            if reset_i = '1' then
                -- fsm
                fsm_state <= Idle;
                if_first_histogram <= '1';
                -- buffers
                iteration_counter_buffered  <= (others => '0');
                increment_overflow_buffered <= '0';
                overflow_buffered           <= '0';
                lhc_fill_buffered           <= (others => '0');
                cms_run_buffered            <= (others => '0');
                lumi_section_buffered       <= (others => '0');
                lumi_nibble_buffered        <= (others => '0');
                -- error
                number_of_error_words_buffered <= (others => '0');
                err_bl_rd_en_o <= '0';
                --
                addr_a_counter <= 0;
                sub_word_id <= 0;
                -- memory
                ram_addr_a_o <= 0;
                ram_din_a_o  <= (others => '0');
                ram_we_a_o   <= (others => '0');
                --
                data_ready <= '0';
            else
                -- reset write every cycle
                ram_we_a_o <= (others => '0');
                
                -- state machine to prepend header and append errors
                case fsm_state is
                    when Idle =>
                        -- wait for new histogram
                        if new_histogram_i = '1' then
                            if if_first_histogram = '1' then
                                -- skip read out of this iteration
                                if_first_histogram <= '0';
                            else
                                -- buffer the inputs
                                iteration_counter_buffered  <= iteration_counter_i;
                                increment_overflow_buffered <= increment_overflow_i;
                                overflow_buffered           <= overflow_i;
                                lhc_fill_buffered           <= lhc_fill_i;
                                cms_run_buffered            <= cms_run_i;
                                lumi_section_buffered       <= lumi_section_i;
                                lumi_nibble_buffered        <= lumi_nibble_i;
                                --
                                addr_a_counter <= 0;
                                sub_word_id <= 0;
                                
                                -- wait for the numver of error words
                                fsm_state <= WaitErrors;
                            end if;
                        end if;
                        
                    when WaitErrors =>
                        -- assigne number of errors
                        number_of_error_words_buffered <= err_bl_n_msk_err_words_i;
                        -- 
                        fsm_state <= StoringHeader;
                    when StoringHeader =>
                        
                        -- write data
                        ram_addr_a_o <= addr_a_counter;
                        ram_din_a_o(32*sub_word_id + 31 downto 32*sub_word_id + 0) <= header_data(NUMBER_OF_32BIT_WORDS_PER_MEMORY_CELL*addr_a_counter + sub_word_id);
                        ram_we_a_o(4*sub_word_id + 3 downto 4*sub_word_id) <= (others => '1');
                        -- increment counter
                        if NUMBER_OF_32BIT_WORDS_PER_MEMORY_CELL*addr_a_counter + sub_word_id < MEMORY_BLOCK_PARAMETERS.HEADER_SIZE-1 then
                            -- increment addr counter with the last sub word
                            if sub_word_id = NUMBER_OF_32BIT_WORDS_PER_MEMORY_CELL-1 then
                                addr_a_counter <= addr_a_counter + 1;
                                sub_word_id <= 0;
                            else
                                -- change sub word id only
                                sub_word_id <= sub_word_id + 1;
                            end if;
                        else
                            fsm_state <= CheckErrors;
                        end if;
                        
                        
                    when CheckErrors => 
                        -- check if we have some errors to store
                        if number_of_error_words_buffered > 0 then
                            --  set the address
                            addr_a_counter <= ERROR_OFFSET;
                            sub_word_id <= ERROR_FIRST_SUB_WORD_ID;
                            -- assert rd enable already now - fifo has a delay of 1 clock cycle to output the next word
                            err_bl_rd_en_o <= '1';
                            -- store errors
                            fsm_state <= StoringErrors;
                        else
                            -- data read for readout
                            fsm_state <= WaitForReadout;
                        end if;   
                    
                    when StoringErrors =>
                        -- assign error word
                        ram_addr_a_o <= addr_a_counter;
                        ram_din_a_o(32*sub_word_id + 31 downto 32*sub_word_id + 0) <= err_bl_err_word_i;
                        ram_we_a_o(4*sub_word_id + 3 downto 4*sub_word_id) <= (others => '1');
                        
                        -- increment counter
                        if (NUMBER_OF_32BIT_WORDS_PER_MEMORY_CELL*(addr_a_counter-ERROR_OFFSET) + sub_word_id) < number_of_error_words_buffered-1 then -- -1 because one word we already read
                            if sub_word_id = NUMBER_OF_32BIT_WORDS_PER_MEMORY_CELL-1 then
                                addr_a_counter <= addr_a_counter + 1;
                                sub_word_id <= 0;
                            else
                                -- change sub word id only
                                sub_word_id <= sub_word_id + 1;
                            end if;
                            -- request new word
                            err_bl_rd_en_o <= '1';
                        else
                            -- reset
                            addr_a_counter <= 0;
                            sub_word_id <= 0;
                            -- done with new words
                            err_bl_rd_en_o <= '0';
                            -- and go back to idle
                            fsm_state <= WaitForReadout;
                        end if;
                        
                    when WaitForReadout => 
                        -- handshake mode with the fifo interface
                        if data_ack = '1' then
                            -- done
                            data_ready <= '0';
                            --
                            fsm_state <= Idle;
                        else
                            -- wait for the user to read the data
                            data_ready <= '1';
                        end if;
                    
                    when others =>
                        fsm_state <= Idle;
                end case;
            end if;
        end if;
    end process;
    
    --=====================================--
    -- Various CDC between clk_i and fifo_rd_clk_i
    --=====================================--
    -- reset cdc
    cdc_reset: xpm_cdc_sync_rst
    generic map (
        DEST_SYNC_FF => CDC_STAGES,   -- DECIMAL; range: 2-10
        INIT => 1,           -- DECIMAL: init value
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    )
    port map (
        dest_rst => reset_fifo_clk, 
        dest_clk => fifo_rd_clk_i,
        src_rst => reset_i
    );
    
    -- data ready cdc
    cdc_data_ready : xpm_cdc_single
    generic map (
        DEST_SYNC_FF => CDC_STAGES,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1   -- DECIMAL; 0=do not register input, 1=register input
    )
    port map (
        dest_out => data_ready_fifo_clk, 
        dest_clk => fifo_rd_clk_i,
        src_clk => clk_i,
        src_in => data_ready
    );
   
    -- data ack cdc (back to clk_i)
    cdc_data_ack: xpm_cdc_single
    generic map (
        DEST_SYNC_FF => CDC_STAGES,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1   -- DECIMAL; 0=do not register input, 1=register input
    )
    port map (
        dest_out => data_ack, 
        dest_clk => clk_i,
        src_clk => fifo_rd_clk_i,
        src_in => data_ack_fifo_clk
    );
    
    -- cdc for the fifo not empty (back to clk_i)
    cdc_fifo_not_empty: xpm_cdc_single
    generic map (
        DEST_SYNC_FF => CDC_STAGES,   -- DECIMAL; range: 2-10
        INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        SRC_INPUT_REG => 1   -- DECIMAL; 0=do not register input, 1=register input
    )
    port map (
        dest_out => fifo_not_empty_o, 
        dest_clk => clk_i,
        src_clk => fifo_rd_clk_i,
        src_in => fifo_not_empty_int
    );
    
    --=====================================--
    -- handmade fifo wrapper
    -- the key point is that the bram has data delay of 2, whereas we need
    -- FWFT fifo  with data coming out after one clock cycle
    -- so we have to prebuffer data 
    --=====================================--
    -- bram interface out
    ram_addr_b_o <= (ram_addr_b_int + 1) when (fifo_fsm_state = FifoReadout) else ram_addr_b_int; -- to always be one step ahead
    ram_din_b_o <= (others => '0'); -- we are always resetting through this port so no sense to have a separate reg
    ram_we_b_o <= read_request and bram_not_empty; -- erase data everytime read request issued
    
    -- read request output the data from the buffer
    read_request <= '1' when read_request_from_fsm = '1' or (fifo_rd_en_i = '1' and words_in_buffer > 0) else '0';
    
    -- fifo data output
    fifo_empty_o        <= '0' when (words_in_buffer > 0 and fifo_fsm_state = FifoReadout) else '1';
    fifo_not_empty_int  <= '1' when (words_in_buffer > 0 and fifo_fsm_state = FifoReadout) else '0';
    fifo_data_o         <= fifo_buffer(words_in_buffer-1) when words_in_buffer > 0 else (others => '0');
    fifo_data_valid_o   <= '1' when words_in_buffer > 0 else '0'; 
    fifo_words_cnt_o <= fifo_words_cnt_int;
 
    -- fifo state machine
    process (fifo_rd_clk_i) 
    begin
        if rising_edge(fifo_rd_clk_i) then
            if reset_fifo_clk = '1' then
                data_ack_fifo_clk   <= '0';
                --
                fifo_fsm_state      <= FifoNoData;
                -- fifo signals
                fifo_words_cnt_int  <= (others => '0');
                -- bram buffering
                bram_not_empty      <= '0';
                read_request_from_fsm <= '0';
                ram_addr_b_int      <= 0;
            else
                -- by default no change in address
                read_request_from_fsm <= '0';
                ram_addr_b_int      <= ram_addr_b_int;
            
                -- state machine
                case fifo_fsm_state is
                    when FifoNoData =>
                        -- fifo interface
                        fifo_words_cnt_int  <= (others => '0');
                        -- internal
                        data_ack_fifo_clk   <= '0';                    
                        -- if got some data
                        if data_ready_fifo_clk = '1' then
                            -- already read the first reg
                            bram_not_empty      <= '1';
                            read_request_from_fsm <= '1';
                            ram_addr_b_int      <= 0;
                            -- switch the state
                            fifo_fsm_state <= FifoFillBuffer;
                        else
                            bram_not_empty      <= '0';
                            read_request_from_fsm        <= '0';
                            ram_addr_b_int      <= 0;
                        end if;
                        
                    when FifoFillBuffer =>
                        -- read some buffer words
                        if ram_addr_b_int < BRAM_OUTPUT_DELAY then
                            read_request_from_fsm        <= '1';
                            ram_addr_b_int      <= ram_addr_b_int + 1;
                        else
                            -- internal
                            data_ack_fifo_clk   <= '0';    
                            -- done with read, leave address the same
                            fifo_fsm_state <= FifoWaitBuffer;
                        end if;
                        
                    -- need to wait a bit until buffer receives data
                    when FifoWaitBuffer =>
                        if words_in_buffer = BRAM_OUTPUT_DELAY+1 then
                            -- set the nwords
                            fifo_words_cnt_int  <= std_logic_vector(to_unsigned(MEMORY_BLOCK_PARAMETERS.header_size+MEMORY_BLOCK_PARAMETERS.total_counter_words+to_integer(unsigned(number_of_error_words_buffered)), 32));
                            -- done with read, leave address the same
                            fifo_fsm_state <= FifoReadout;
                        end if;
                    
                    when FifoReadout =>
                        
                        -- fifo read
                        if fifo_rd_en_i = '1' then
                            -- read bram
                            if fifo_words_cnt_int > BRAM_OUTPUT_DELAY+2 then
                                -- still not empty
                                bram_not_empty <= '1';
                                -- increment bram address
                                ram_addr_b_int <= ram_addr_b_int + 1;
                            else
                                -- now empty
                                bram_not_empty <= '0';
                            end if;
                            -- decrement number of fifo words
                            if fifo_words_cnt_int > 0 then
                                -- decrement the amount of words in fifo
                                fifo_words_cnt_int <= fifo_words_cnt_int - 1;
                            end if;                            
                        end if;
                        
                        -- check that we are done
                        if fifo_words_cnt_int = 0 then
                            -- now we are done 
                            -- do data ack and wait for the redy to go down
                            if data_ready_fifo_clk = '1' then
                                data_ack_fifo_clk   <= '1';  
                            else
                                data_ack_fifo_clk   <= '0';  
                                -- go backt the idlle state
                                fifo_fsm_state <= FifoNoData;
                            end if;
                        end if;
                      
                    
                    when others =>
                        fifo_fsm_state <= FifoNoData;
                end case;
            end if;
        end if;
    end process;
    
    -- delaying the read request to update buffer
    delay_read_request: entity work.bril_histogram_shift_register
    generic map(
        REGISTER_WIDTH  => 1,
        DELAY           => BRAM_OUTPUT_DELAY
    )
    port map (
        clk_i           => fifo_rd_clk_i,
        enable_i        => bram_not_empty,
        input_i(0)      => read_request,
        output_o(0)     => read_request_delayed
    );
    
    -- fifo buffer updator
    process (fifo_rd_clk_i) 
    begin
        if rising_edge(fifo_rd_clk_i) then
            if reset_fifo_clk = '1' then
                fifo_buffer <= (others => (others => '0'));
                words_in_buffer <= 0;
            else
                -- control the words in buffer
                if fifo_fsm_state = FifoReadout and read_request = '1' and read_request_delayed = '1' then -- simultanous read and write to the buffer
                    words_in_buffer <= words_in_buffer; -- no change
                elsif fifo_fsm_state = FifoReadout and read_request = '1' then -- read something from the buffer
                    words_in_buffer <= words_in_buffer - 1;
                elsif read_request_delayed = '1' then -- added data to the buffer
                    words_in_buffer <= words_in_buffer + 1;
                else -- nothing to do
                    words_in_buffer <= words_in_buffer;
                end if;
            
                -- update the buffer when data are ready
                if read_request_delayed = '1' then
                    fifo_buffer <= fifo_buffer(fifo_buffer'high-1 downto 0) & ram_dout_b_i;
                end if;
            end if;
        end if;
    end process;

end rtl;
