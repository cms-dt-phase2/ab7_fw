----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_wrapper_sync - rtl
-- Description: 
--          Generates histogramming and streaming cores
--          Here we generate a second-level FIFO (optionally), which increases the usage of memory but reduces the bandwidth requirement
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- user macros to create fifo (device agnostic)
library xpm;
use xpm.vcomponents.all;

-- bril package
use work.bril_histogram_package.all;

entity bril_histogram_wrapper_sync is
Generic (
    -- identification of the histogram
    HISTOGRAM_TYPE                  : natural;
    HISTOGRAM_ID                    : natural;
    -- memory
    GENERATE_FIFO                   : boolean;
    -- histogram parameters
    NUMBER_OF_BINS                  : natural;
    COUNTER_WIDTH                   : natural;
    INCREMENT_WIDTH                 : natural;
    -- error parameters
    NUMBER_OF_UNITS                 : natural := 1; -- number of units (modules, chips) connected to the histogrammer
    NUMBER_OF_WORDS_PER_ERROR       : natural := 0;
    MAX_NUMBER_OF_MASKERROR_WORDS   : natural := 0
);
Port(
    -- core signals
    memory_reset_i              : in std_logic; -- reset the memory, has to be released at least 4 clock cycles earlier
    reset_i                     : in std_logic; -- sync reset
    clk_i                       : in std_logic; -- counting clock, typ. 40mhz
    -- tcds signals
    new_histogram_i             : in std_logic; -- start outputting data (orbit reset)
    new_iteration_i             : in std_logic; -- resetting the bin counter (if number of bins is 3564 corresponding to 3564bx - this signal is the bx reset)    
    -- tcds data
    lhc_fill_i                  : in std_logic_vector(31 downto 0);
    cms_run_i                   : in std_logic_vector(31 downto 0);
    lumi_section_i              : in std_logic_vector(31 downto 0);
    lumi_nibble_i               : in std_logic_vector(31 downto 0);     
    -- counter signals  
    increment_overflow_i        : in std_logic := '0';
    increment_i                 : in std_logic_vector(INCREMENT_WIDTH-1 downto 0); -- increment command
    -- fifo interface (optional)
    fifo_rd_clk_i               : in std_logic := '0';
    fifo_rd_en_i                : in std_logic := '0';
    fifo_empty_o                : out std_logic;
    fifo_words_cnt_o            : out std_logic_vector(31 downto 0);
    -- error block interface
    err_bl_n_msk_err_words_i    : in std_logic_vector(15 downto 0) := (others => '0');
    err_bl_err_word_i           : in std_logic_vector(31 downto 0) := (others => '0');
    err_bl_rd_en_o              : out std_logic;
    --
    data_o                      : out std_logic_vector(31 downto 0); -- output of the memory
    data_valid_o                : out std_logic -- write enable
);
end bril_histogram_wrapper_sync;

architecture rtl of bril_histogram_wrapper_sync is

    -- calculate the amount of memory to be allocated for counters and for the fifo       
    constant MEMORY_BLOCK_PARAMETERS    : memory_block_parameters_type := get_memory_block_parameters(COUNTER_WIDTH, NUMBER_OF_BINS, MAX_NUMBER_OF_MASKERROR_WORDS);                 

    -- packed data
    signal core_reset                  : std_logic;
    signal iteration_counter_from_core : natural;
    signal overflow_from_core          : std_logic;
    signal data_from_core              : std_logic_vector(31 downto 0); -- output of the memory
    signal data_valid_from_core        : std_logic; -- write enable
    signal data_from_streamer          : std_logic_vector(31 downto 0); -- output of the streamer
    signal data_valid_from_streamer    : std_logic; -- write enable
    
    -- fifo signals
    signal fifo_data_valid             : std_logic;
    signal fifo_empty                  : std_logic;
    signal fifo_full                   : std_logic;
    signal fifo_rd_data_count          : std_logic_vector(logNceil(MEMORY_BLOCK_PARAMETERS.fifo_depth,2) downto 0);
    signal fifo_wr_data_count          : std_logic_vector(logNceil(MEMORY_BLOCK_PARAMETERS.fifo_depth,2) downto 0);
    
    -- synchrnise empty signal to allow clearing data
    signal fifo_empty_fast             : std_logic;
    
begin

    -- instantiate the histogramming core (sync version)
    bril_histogram_core_sync_inst: entity work.bril_histogram_core_sync
    generic map (
        -- memory 
        MEMORY_BLOCK_PARAMETERS => MEMORY_BLOCK_PARAMETERS,
        -- histogram parameters
        NUMBER_OF_BINS          => NUMBER_OF_BINS,
        COUNTER_WIDTH           => COUNTER_WIDTH,
        INCREMENT_WIDTH         => INCREMENT_WIDTH
    )
    port map (
        -- core signals       
        reset_i                 => reset_i or core_reset,            
        clk_i                   => clk_i,              
        -- tcds signas  
        new_histogram_i         => new_histogram_i,    
        new_iteration_i         => new_iteration_i,    
        -- counter signals
        increment_i             => increment_i,        
        -- output
        iteration_counter_o     => iteration_counter_from_core,  
        overflow_o              => overflow_from_core,                          
        data_o                  => data_from_core,             
        data_valid_o            => data_valid_from_core      
    );
       
    -- instantiate the streamer
    bril_histogram_streamer_sync_inst: entity work.bril_histogram_streamer_sync
    generic map (
       -- identification of the histogram
        HISTOGRAM_TYPE          => HISTOGRAM_TYPE,
        HISTOGRAM_ID            => HISTOGRAM_ID,
        -- memory 
        MEMORY_BLOCK_PARAMETERS => MEMORY_BLOCK_PARAMETERS,
        -- histogram parameters
        NUMBER_OF_BINS          => NUMBER_OF_BINS,
        COUNTER_WIDTH           => COUNTER_WIDTH,
        INCREMENT_WIDTH         => INCREMENT_WIDTH,
        -- error block info
        NUMBER_OF_UNITS             => NUMBER_OF_UNITS,
        NUMBER_OF_WORDS_PER_ERROR   => NUMBER_OF_WORDS_PER_ERROR
    )
    port map (
        -- core signals       
        reset_i                     => reset_i,            
        clk_i                       => clk_i,              
        -- input
        iteration_counter_i         => iteration_counter_from_core,  
        increment_overflow_i        => increment_overflow_i, -- comes one per histogram     
        overflow_i                  => overflow_from_core,
        data_i                      => data_from_core,
        data_valid_i                => data_valid_from_core,
        -- tcds signals
        new_histogram_i             => new_histogram_i,
        new_histogram_reset_i       => core_reset,
        -- tcds data
        lhc_fill_i                  => lhc_fill_i,
        cms_run_i                   => cms_run_i,
        lumi_section_i              => lumi_section_i,
        lumi_nibble_i               => lumi_nibble_i,
        -- error block interface
        err_bl_n_msk_err_words_i    => err_bl_n_msk_err_words_i,
        err_bl_err_word_i           => err_bl_err_word_i,
        err_bl_rd_en_o              => err_bl_rd_en_o,
        -- output
        data_o                      => data_from_streamer,             
        data_valid_o                => data_valid_from_streamer      
    );
    
    -- in case the fifo has to be generated
    gen_fifo: if GENERATE_FIFO generate
        -- xpm_fifo_async: ASynchronous FIFO
        -- Xilinx Parameterized Macro, version 2018.3
        xpm_fifo_sync_inst : xpm_fifo_async
        generic map (
          CDC_SYNC_STAGES => 2,       -- DECIMAL
          DOUT_RESET_VALUE => "0",    -- String
          ECC_MODE => "no_ecc",       -- String
          FIFO_MEMORY_TYPE => "auto", -- String
          FIFO_READ_LATENCY => 0,     -- DECIMAL, has to be 0 for the FWFT
          FIFO_WRITE_DEPTH => MEMORY_BLOCK_PARAMETERS.fifo_depth,   -- DECIMAL, depth that we need
          FULL_RESET_VALUE => 0,      -- DECIMAL
          PROG_EMPTY_THRESH => 10,    -- DECIMAL, we will not use it anyways
          PROG_FULL_THRESH => 10,     -- DECIMAL, will also not use this flag
          RD_DATA_COUNT_WIDTH => logNceil(MEMORY_BLOCK_PARAMETERS.fifo_depth,2)+1,   -- DECIMAL
          READ_DATA_WIDTH => 32,      -- DECIMAL
          READ_MODE => "fwft",        -- String, first word fall through
          RELATED_CLOCKS => 0,        -- Decimal, for the async fifo means that clocks come from different sources (just in case)
          USE_ADV_FEATURES => "1405", -- String, here we enable only rd_data_count, wr_data_count and data_valid
          WAKEUP_TIME => 0,           -- DECIMAL
          WRITE_DATA_WIDTH => 32,     -- DECIMAL
          WR_DATA_COUNT_WIDTH => logNceil(MEMORY_BLOCK_PARAMETERS.fifo_depth,2)+1    -- DECIMAL
        )
        port map (
          -- rd clock domain
          rd_clk => fifo_rd_clk_i,
          rd_en => fifo_rd_en_i,
          data_valid => fifo_data_valid,    
          dout => data_o,
          empty => fifo_empty,
          full => fifo_full,
          rd_data_count => fifo_rd_data_count,
          rd_rst_busy => open,
          -- wr clock domain
          wr_clk => clk_i, 
          wr_en => data_valid_from_streamer,
          din => data_from_streamer, 
          wr_data_count => fifo_wr_data_count,
          wr_rst_busy => open,
          rst => reset_i or memory_reset_i, -- wr clk
          -- not needed ports
          sleep => '0',
          injectsbiterr => '0',
          injectdbiterr => '0'
        );
        
        -- reset handling - if there is still some we should reset the core - no backpressure allowed
        core_reset <= new_histogram_i and (not fifo_empty_fast);
        
        -- fifo empty has to be syncrnised - create cdc
        cdc_fifo_empty: xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 4,   -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 0,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG => 1   -- DECIMAL; 0=do not register input, 1=register input
        )
        port map (
            dest_out => fifo_empty_fast, 
            dest_clk => clk_i,
            src_clk => fifo_rd_clk_i,
            src_in => fifo_empty
        );
        
        -- connecting external signals
        fifo_empty_o <= fifo_empty;
        fifo_words_cnt_o <= std_logic_vector(resize(unsigned(fifo_rd_data_count), 32));
        data_valid_o <= fifo_data_valid and (not fifo_empty);
    end generate gen_fifo;
    
    -- in case no fifo requested
    gen_without_fifo: if (NOT GENERATE_FIFO) generate
        -- just zeros for unused signals
        fifo_empty_o <= '1';
        fifo_empty_fast <= '1';
        fifo_words_cnt_o <= (others => '0');
        -- no resets
        core_reset <= '0';
        -- real data streaming
        data_o <= data_from_streamer;
        data_valid_o <= data_valid_from_streamer;
    end generate gen_without_fifo;

end rtl;
