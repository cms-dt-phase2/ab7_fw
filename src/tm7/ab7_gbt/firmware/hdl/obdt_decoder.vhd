--=================================================================================================--
-- Decodes OBDT data and provides it clocked in the main fpga's lhc family of clocks
--=================================================================================================--
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package obdt_decoder_decl is
  type slv32_v_t is array(natural range <>) of std_logic_vector(31 downto 0);
  type usgn4_v_t is array(natural range <>) of unsigned(3 downto 0);
  type usgn32_v_t is array(natural range <>) of unsigned(31 downto 0);
end package;

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Custom libraries and packages:
library work;
use work.gbt_bank_package.all;
use work.obdt_decoder_decl.all;
use work.bo2.all;

entity obdt_decoder is
  port ( 
    -- Configuration
    chsel                         : in usgn4_v_t(1 to 3);
    conf_unlock_cooldown          : in unsigned(4 downto 0);
    conf_wrong_pattern_cooldown   : in unsigned(4 downto 0);
    -- Stats
    stat_unlock_cool              : out std_logic_vector(3 downto 1);
    stat_wrong_pattern_cool       : out std_logic_vector(3 downto 1);
    stat_duplicate_word_ctr       : out usgn32_v_t(1 to 3);
    -- Input data from gbt
    frameClk                      : in std_logic; -- Frame clock
    headerLocked12                : in std_logic_vector(1 to 12);
    rxData12                      : in gbt_reg84_A(1 to 12);
    rxIsData12                    : in std_logic_vector(1 to 12);
    -- Output data
    clk160                        : in std_logic; -- output clock
    obdt_data                     : out slv32_v_t(1 to 3)
  );
end entity;

architecture structural of obdt_decoder is
  signal tictoc : std_logic_vector(2 downto 0);
  signal rxStrobe160 : std_logic;
  signal stat_duplicate_word_ctr_sig : usgn32_v_t(1 to 3);

begin

-- Generation of a strobe signal that should guarantee that the 160 MHz clock samples stable data
tictoc(0) <= not tictoc(0) when rising_edge(frameClk);
tictoc(2 downto 1) <= tictoc(1 downto 0) when rising_edge(clk160);
rxStrobe160 <= bo2sl( tictoc(2) /= tictoc(1) ) when rising_edge(clk160);

stat_duplicate_word_ctr <= stat_duplicate_word_ctr_sig;

slgen: for sl in 1 to 3 generate
  signal gbt1hot : std_logic_vector(1 to 12);
  signal rxData : std_logic_vector(83 downto 0);
  signal rxIsData : std_logic;
  signal headerLocked : std_logic;
  
begin 

  gen_1_12: for gbt in 1 to 12 generate
    gbt1hot(gbt) <= bo2sl( chsel(sl) = to_unsigned(gbt,4) );
  end generate; 

  process(frameClk)
    variable rxDataVar : std_logic_vector(83 downto 0);
    variable rxIsDataVar : std_logic;
    variable headerLockedVar : std_logic;
  begin
    if rising_edge(frameClk) then
      -- This code essentially selects 1 of the 12 gbt data links for the corresponding output.
      -- rxData <= rxData12( to_integer( chsel(sl) ) )
      -- But, instead of multiplexing, it is more lut-efficient to and every bit with the 1-hot selector and the OR all the results 
      rxDataVar   := (others => '0');
      rxIsDataVar := '0';
      headerLockedVar := '0';
      
      for gbt in 1 to 12 loop
        for i in 83 downto 0 loop
          rxDataVar(i) := rxDataVar(i) or ( rxData12(gbt)(i) and gbt1hot(gbt) );
        end loop;
        rxIsDataVar := rxIsDataVar or ( rxIsData12(gbt) and gbt1hot(gbt) );
        headerLockedVar := headerLockedVar or ( headerLocked12(gbt) and gbt1hot(gbt) );
      end loop;

      rxData    <= rxDataVar;
      rxIsData  <= rxIsDataVar;
      headerLocked  <= headerLockedVar;
    end if;
  end process;
  


  -- Careful with this process. It takes 3 cycles to process each input frame. Because the clk160 is 4 times the frameClk,
  -- there is 1 cycle of margin to account for little variations in the Strobe arrival(it will arrive every 4 cycles,
  -- but sometimes could arrive after 3 or 5 cycles due to frameClk and clk160 being asynchronous. 
  process(clk160)
    variable rxDataShift, rxDataOld, rxDataOld2 : std_logic_vector(83 downto 0);
    variable rxDataThird : std_logic_vector(27 downto 0);
    variable rxIsData160 : std_logic;
    variable unlock_cooldown, wrong_pattern_cooldown : unsigned(31 downto 0); 
    variable unlock_cool, wrong_pattern_cool : std_logic; 
  begin
    if rising_edge(clk160) then
    
      if rxStrobe160 = '1' then
        -- Detection of duplicate non-zero pattern
        if rxDataOld2 /= rxDataOld and rxDataOld /= (rxDataOld'range => '0') and rxDataOld2 /= (rxDataOld'range => '0') then
          stat_duplicate_word_ctr_sig(sl) <= stat_duplicate_word_ctr_sig(sl) + 1; 
        end if;
        rxDataOld2  := rxDataOld;
        rxDataOld   := rxData;
        -- End of detection of duplicate non-zero pattern
        
        rxDataShift := rxData;
        rxIsData160 := rxIsData;
      else
        rxDataShift := x"0000000" & rxDataShift(83 downto 28);
      end if;
      rxDataThird := rxDataShift(27 downto 0);
      
      obdt_data(sl) <= (bo2sl(rxDataThird(27 downto 25) = "100") and unlock_cool and wrong_pattern_cool and rxIsData160) & "0"  -- valid & "0"
                        & "00000" & rxDataThird(7 downto 0)                                                                      -- St & SL & "0" & channel(8bits) 
                        & rxDataThird(19 downto 8) & rxDataThird(24 downto 20);                                                  -- BX(12b) & TDC(5b) 

      -- Verify the header is locked and produce stat for it and bit to turn data into invalid otherwise
      unlock_cool := bo2sl( unlock_cooldown = (unlock_cooldown'range => '0')  );
      stat_unlock_cool(sl) <= unlock_cool ;
      if headerLocked = '1' then
        unlock_cooldown := unlock_cooldown - bo2int( unlock_cooldown > 0 );
      else 
        unlock_cooldown := (others=>'0');
        unlock_cooldown( to_integer(conf_unlock_cooldown) ) := '1' ;
      end if;

      -- Verify input data format is good, and produce stat for it and a bit to turn data into invalid otherwise
      wrong_pattern_cool := bo2sl( wrong_pattern_cooldown = (wrong_pattern_cooldown'range => '0')  );
      stat_wrong_pattern_cool(sl) <= wrong_pattern_cool;
      if rxDataThird = (rxDataThird'range => '0') or rxDataThird(27 downto 25) = "100" then
        wrong_pattern_cooldown := wrong_pattern_cooldown - bo2int( wrong_pattern_cooldown > 0 );
      else 
        wrong_pattern_cooldown := (others=>'0');
        wrong_pattern_cooldown( to_integer(conf_wrong_pattern_cooldown) ) := '1' ;
      end if;
      
    end if;
  end process;

end generate; 

end architecture;
