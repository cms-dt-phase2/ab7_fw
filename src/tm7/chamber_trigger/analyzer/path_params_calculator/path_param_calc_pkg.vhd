-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Analyzer 
---- File       : path_param_calc_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-08
-------------------------------------------------------------------------------
---- Description: Package for components/defined types for calculations on
----              segment path parameters
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package path_param_calc_pkg is

  ---------------------------------------------------------------
  -- Data types
  ---------------------------------------------------------------
  type SignedDistance_arr_t is array (0 to 3) of SignedDistance_t;
  
  ---------------------------------------------------------------
  -- Functions
  ---------------------------------------------------------------
  function slv_from_SgnDistArr(sgnArr : SignedDistance_arr_t) 
    return std_logic_vector;
        
  function SgnDistArr_from_slv(data: std_logic_vector) 
    return SignedDistance_arr_t;
    
  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component PATH_CALC_CELL_X is
    port (
      clk          : in  std_logic;
      rst          : in  std_logic;
      enable       : in  std_logic;
      segment_main : in  DTSegMainParams_t;
      hit_x_pos    : out SignedDistance_arr_t
    );
  end component;

  component CALC_TAN_POS is
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    enable          : in  std_logic;
    main_params     : in  DTSegMainParams_t;
    hit_x_pos       : in  SignedDistance_arr_t;
    four_valid_hits : out std_logic;
    x_coor          : out SignedDistance_t;
    tan_phi_x4096   : out signed(14 downto 0)
  );
  end component;
  
  component CALC_4HITS_TAN_POS is
  port (
    clk           : in  std_logic;
    rst           : in  std_logic;
    enable        : in  std_logic;
    hit_x_pos     : in  SignedDistance_arr_t;
    x_coor        : out SignedDistance_t;
    tan_phi_x4096 : out signed(14 downto 0)
  );
  end component;
  
  component CALC_CHI_SQUARE is
  port (
    clk           : in  std_logic;
    rst           : in  std_logic;
    enable        : in  std_logic;
    main_params   : in  DTSegMainParams_t;
    hit_x_pos     : in  SignedDistance_arr_t;
    x_coor        : in  SignedDistance_t;
    tan_phi_x4096 : in  signed(DTSegment_t.phi_tangent_x4096'length-1 downto 0);
    delta_hit_x   : out DeltaHit_arr_t;
    chi_square    : out unsigned(DTSegment_t.chi_square'length-1 downto 0)
  );
  end component;
  
  component CALC_ABSOLUTE_DEVIATION is
  port (
    clk           : in  std_logic;
    rst           : in  std_logic;
    enable        : in  std_logic;
    main_params   : in  DTSegMainParams_t;
    hit_x_pos     : in  SignedDistance_arr_t;
    x_coor        : in  SignedDistance_t;
    tan_phi_x4096 : in  signed(DTSegment_t.phi_tangent_x4096'length-1 downto 0);
    delta_hit_x   : out DeltaHit_arr_t;
    chi_square    : out unsigned(DTSegment_t.chi_square'length-1 downto 0)
  );
  end component;
  
  component PATH_CALC_BX is
    port (
      clk        : in  std_logic;
      rst        : in  std_logic;
      enable     : in  std_logic;
      bx_time    : in  std_logic_vector(16 downto 0);
      bx_id      : out std_logic_vector(11 downto 0);
      bx_id_prec : out std_logic_vector(2 downto 0)
    );
  end component;
  
end package;

-------------------------------------------------------------------------------
---- Body
-------------------------------------------------------------------------------
package body path_param_calc_pkg is

  function slv_from_SgnDistArr(sgnArr : SignedDistance_arr_t) 
    return std_logic_vector
  is
    variable arr_slv : signed((4 * SGN_DIST_SIZE) - 1 downto 0);
  begin
    arr_slv((4 * SGN_DIST_SIZE) - 1 downto 3 * SGN_DIST_SIZE) := sgnArr(3);
    arr_slv((3 * SGN_DIST_SIZE) - 1 downto 2 * SGN_DIST_SIZE) := sgnArr(2);
    arr_slv((2 * SGN_DIST_SIZE) - 1 downto     SGN_DIST_SIZE) := sgnArr(1);
    arr_slv(     SGN_DIST_SIZE  - 1 downto 0)                 := sgnArr(0);
    
    return std_logic_vector(arr_slv);
  end function;

  function SgnDistArr_from_slv(data: std_logic_vector) 
    return SignedDistance_arr_t
  is
    variable answer : SignedDistance_arr_t;
  begin
    answer(3) := signed(data((4 * SGN_DIST_SIZE) - 1 downto 3 * SGN_DIST_SIZE));
    answer(2) := signed(data((3 * SGN_DIST_SIZE) - 1 downto 2 * SGN_DIST_SIZE));
    answer(1) := signed(data((2 * SGN_DIST_SIZE) - 1 downto     SGN_DIST_SIZE));
    answer(0) := signed(data(     SGN_DIST_SIZE  - 1 downto 0));
  
    return answer;
  end function;
  
end package body;
