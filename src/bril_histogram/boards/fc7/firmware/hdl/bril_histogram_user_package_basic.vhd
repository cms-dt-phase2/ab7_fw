library ieee;
use ieee.std_logic_1164.all;
 
package user_package is

	constant sys_phase_mon_freq      : string   := "160MHz"; -- valid options only "160MHz" or "240MHz"    

   --=== ipb slaves =============--
	constant nbr_usr_slaves				: positive := 4 ;
   
	constant user_ipb_stat_regs		: integer  := 0 ;
	constant user_ipb_ctrl_regs		: integer  := 1 ;
	constant user_ipb_injector_regs : integer  := 2 ;
	constant user_ipb_readout_regs  : integer  := 3 ;

	
end user_package;
   
package body user_package is
end user_package;
