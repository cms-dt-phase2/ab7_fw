----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: dummy_top_wrapper - rtl 
-- Description: 
-- 
--      Dummy top module to be used for test synthesis.
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dummy_top_wrapper is
Port(
    -- core signals
    memory_reset_i      : in std_logic; -- reset the memory, has to be released at least 4 clock cycles earlier
    reset_i             : in std_logic; -- sync reset
    clk_i               : in std_logic; -- counting clock, typ. 40mhz
    -- tcds signals
    new_histogram_i     : in std_logic; -- start outputting data (orbit reset)
    new_iteration_i     : in std_logic; -- resetting the bin counter (if number of bins is 3564 corresponding to 3564bx - this signal is the bx reset)
    -- tcds data
    lhc_fill_i          : in std_logic_vector(31 downto 0);
    cms_run_i           : in std_logic_vector(31 downto 0);
    lumi_section_i      : in std_logic_vector(31 downto 0);
    lumi_nibble_i       : in std_logic_vector(31 downto 0);     
    -- data input interface
    increment_i         : in std_logic_vector(240*3-1 downto 0);
    error_i             : in std_logic_vector(239 downto 0);
    mask_i              : in std_logic_vector(239 downto 0);
    -- fifo interface (optional)
    fifo_rd_clk_i       : in std_logic := '0';
    fifo_rd_en_i        : in std_logic := '0';
    fifo_empty_o        : out std_logic;
    fifo_words_cnt_o    : out std_logic_vector(31 downto 0);
    --
    data_o              : out std_logic_vector(31 downto 0); -- output of the memory
    data_valid_o        : out std_logic -- write enable
);
end dummy_top_wrapper;

architecture rtl of dummy_top_wrapper is

begin

    bril_histogram_accumulation_layer_inst: entity work.bril_histogram_accumulation_layer
    generic map (
        -- identification of the histogram
        HISTOGRAM_TYPE                              => 0,
        HISTOGRAM_ID                                => 0,
        -- memor                                    
        GENERATE_FIFO                               => true,
        -- histogram parameter                      
        NUMBER_OF_BINS                              => 3564,
        COUNTER_WIDTH                               => 21,
        INCREMENT_WIDTH                             => 9,
        -- accumulation parameters
        NUMBER_OF_UNITS                             => 240,
        UNIT_INCREMENT_WIDTH                        => 3,
        NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE      => 4,
        UNIT_ERROR_COUNTER_WIDTH                    => 3,
        STORE_ERRORS                                => false,
        --
        CLOCK_PERIOD                                => 3.125
    )
    port map(
        -- core signals
        memory_reset_i              => memory_reset_i,
        reset_i                     => reset_i,
        clk_i                       => clk_i,
        -- tcds signal            
        new_histogram_i             => new_histogram_i,
        new_iteration_i             => new_iteration_i,
        -- tcds dat                 
        lhc_fill_i                  => lhc_fill_i,
        cms_run_i                   => cms_run_i,
        lumi_section_i              => lumi_section_i,
        lumi_nibble_i               => lumi_nibble_i,
        -- data input interf     
        increment_i                 => increment_i,
        error_i                     => error_i,
        mask_i                      => mask_i,
        -- fifo interface 
        fifo_rd_clk_i               => fifo_rd_clk_i,
        fifo_rd_en_i                => fifo_rd_en_i,
        fifo_empty_o                => fifo_empty_o,
        fifo_words_cnt_o            => fifo_words_cnt_o,
        --                            
        data_o                      => data_o,
        data_valid_o                => data_valid_o
    );

end rtl;
