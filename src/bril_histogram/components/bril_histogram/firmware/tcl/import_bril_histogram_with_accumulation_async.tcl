# basic source files
source import_bril_histogram_basic_async.tcl

# ones needed at the accumulation stage
add_files -norecurse ../hdl/async/bril_histogram_accumulation_layer_async.vhd
add_files -norecurse ../hdl/bril_histogram_error_block.vhd
add_files -norecurse ../hdl/bril_histogram_recursive_accumulator.vhd
