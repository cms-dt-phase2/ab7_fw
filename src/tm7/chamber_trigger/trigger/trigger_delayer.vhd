-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Chamber trigger module
---- File       : trigger_delayer.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-03
-------------------------------------------------------------------------------
---- Description: 
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
library UNISIM;
use UNISIM.vcomponents.all;
--
use work.globalpath_pkg.all;

entity TRIGGER_DELAYER is
  port ( 
    clk                  : in  std_logic;
    rst                  : in  std_logic;
    trigger_in           : in  std_logic;
    conf_TT_test         : in  std_logic;
    conf_rst_TT_rate     : in  std_logic;
    conf_TT_delay        : in  std_logic_vector(6 downto 0);
    bunch_ctr            : in  std_logic_vector(11 downto 0);
    orbit_ctr            : in  std_logic_vector(31 downto 0);
    TT_rate              : out std_logic_vector(15 downto 0);
    chamber_trigger_P3_p : out std_logic;
    chamber_trigger_P3_n : out std_logic;
    dt_trig              : out std_logic
  );
end entity;

architecture TwoProcess of TRIGGER_DELAYER is

  -- Internal signals to be registered.
  type reg_type is record
    technical_trigger_P3 : std_logic;
    technical_trigger    : std_logic;
    TT_shift             : std_logic_vector(63 downto 0);    
    TT_cnt               : std_logic_vector(15 downto 0);
    TT_rate              : std_logic_vector(15 downto 0);
    TT_cycle_cnt         : std_logic_vector(27 downto 0);
  end record;

  constant regDefault : reg_type := (
    technical_trigger_P3 => '0',
    technical_trigger    => '0',
    TT_shift             => (others => '0'),
    TT_cnt               => (others => '0'),
    TT_rate              => (others => '0'),
    TT_cycle_cnt         => (others => '0')
  );
  signal reg, nextReg : reg_type := regDefault;

  -- Auxiliary
  signal chamber_trigger_delayed : std_logic;
  signal TT_shift_mux            : std_logic_vector(64 downto 0);
  signal cycle_end               : std_logic_vector(27 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --  
  dt_trig <= chamber_trigger_delayed;

  P3_OBUFDS_INST : OBUFDS generic map (IOSTANDARD => "LVDS", SLEW => "SLOW")
  port map (O => chamber_trigger_P3_p, OB => chamber_trigger_P3_n,
            I => chamber_trigger_delayed
  );

  --------
  cycle_end <= --x"03D0900" when TT_test = '1' else  -- 0.1 sec
             x"26259FF";                      -- 1 sec

  chamber_trigger_delayed <= TT_shift_mux(to_integer(unsigned(conf_TT_delay)));
  TT_rate                 <= reg.TT_rate;

  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, trigger_in, bunch_ctr, orbit_ctr, conf_TT_test, 
                 conf_rst_TT_rate, cycle_end)
  begin
    -- Default values
    nextReg <= reg;

    ------------------------------------------------------
    -- Trigger selector.
    -- It selects between "test" and "real" trigger signal.
    --
    if conf_TT_test = '1' then
      -- One every 16 orbits
      if (bunch_ctr(11 downto 0) = x"ABC") and (orbit_ctr(3 downto 0) = "0000") 
      then nextReg.technical_trigger <= '1';
      else nextReg.technical_trigger <= '0';
      end if;
   
    else
        nextReg.technical_trigger <= trigger_in;
    end if;

    if reg.technical_trigger_P3 = '0' 
    then nextReg.technical_trigger_P3 <= reg.technical_trigger;
    else nextReg.technical_trigger_P3 <= '0';
    end if;    
    
    ------------------------------------------------------
    -- Trigger shifting selected by configuration.
    -- It's able to delay the trigger signal a fixed number of clock cycles.
    --
    nextReg.TT_shift <= reg.TT_shift(62 downto 0) & reg.technical_trigger_P3;
    
    -- Muxer to select trigger delay
    TT_shift_mux(0) <= reg.technical_trigger_P3;

    TTShift_MUX : for i in 0 to 63 loop
      TT_shift_mux(i + 1) <= reg.TT_shift(i);
    end loop;

    ------------------------------------------------------
    -- Trigger rate calculation for statistics.
    --
    if conf_rst_TT_rate = '1' then
      nextReg.TT_rate      <= (others => '0');
      nextReg.TT_cnt       <= (others => '0'); 
      nextReg.TT_cycle_cnt <= (others => '0');

    elsif reg.TT_cycle_cnt = cycle_end then
      nextReg.TT_rate      <= reg.TT_cnt;
      nextReg.TT_cnt       <= (others => '0'); 
      nextReg.TT_cycle_cnt <= (others => '0');

    else
      nextReg.TT_cycle_cnt <= std_logic_vector(unsigned(reg.TT_cycle_cnt) + 1);

      if reg.technical_trigger_P3 = '1' 
      then nextReg.TT_cnt <= std_logic_vector(unsigned(reg.TT_cnt) + 1);
      end if;

    end if;
  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1' 
      then reg <= regDefault;
      else reg <= nextReg;
      end if;
    end if;
  end process;

end architecture;
