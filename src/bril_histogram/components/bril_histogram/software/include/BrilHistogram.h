// Mykyta Haranko, BRIL, CERN
// BrilHistogram decoder class

// includes
#include <vector>
#include <cstdint>
#include <cstdlib>

// definition of the error class
class BrilHistogramError {

	public:
		BrilHistogramError (uint32_t pOrbitCounter, uint32_t pBxCounter, std::vector<uint16_t> pUnitList) { fOrbitCounter = pOrbitCounter; fBxCounter = pBxCounter; fUnitList = pUnitList; }
		~BrilHistogramError () { fUnitList.clear(); }
		const uint32_t GetOrbitCounter () { return fOrbitCounter; }	
		const uint32_t GetBxCounter () { return fBxCounter; }
		const std::vector<uint16_t> GetUnitList () { return fUnitList; }	
	
	private:
		uint32_t fOrbitCounter; 
		uint32_t fBxCounter; 
		std::vector<uint16_t> fUnitList;
		
};


// bril histo class itself
class BrilHistogram {

	public:
		// basic
		BrilHistogram (std::vector<uint32_t> pDataRaw);
		~BrilHistogram () { fDataRaw.clear(); }

		// get raw data
		const std::vector<uint32_t> GetDataRaw() { return fDataRaw; }
		// histo paramters
		const uint16_t GetNwordsTotal() { return ((fDataRaw.at(0) >> 0) & 0xFFFF); }
		const uint8_t GetHeaderSize() { return ((fDataRaw.at(0) >> 16) & 0xFF); }
		const uint16_t GetNbins() { return ((fDataRaw.at(1) >> 0) & 0xFFFF); }
		const uint16_t GetNunits() { return ((fDataRaw.at(8) >> 16) & 0xFF); }
		const uint16_t GetNcounterWords() { return ((fDataRaw.at(2) >> 0) & 0xFFFF); }
		const uint8_t GetIncrementWidth() { return ((fDataRaw.at(2) >> 16) & 0xFF); }
		const uint8_t GetCounterWidth() { return ((fDataRaw.at(2) >> 24) & 0xFF); }
		const uint8_t GetNwordsPerError() { return ((fDataRaw.at(8) >> 24) & 0xFF); }
		// histo info
		const uint16_t GetHistogramID() { return ((fDataRaw.at(1) >> 16) & 0x3FF); }
		const uint16_t GetHistogramType() { return ((fDataRaw.at(1) >> 26) & 0x3F); }
		// tcds
		const uint32_t GetLhcFill() { return fDataRaw.at(4); }
		const uint32_t GetCmsRun() { return fDataRaw.at(5); }
		const uint32_t GetLumiSection() { return fDataRaw.at(6); }
		const uint32_t GetLumiNibble() { return fDataRaw.at(7); }
		// data itself
		const uint32_t GetOrbitCounter() { return ((fDataRaw.at(3) >> 0) & 0xFFFFF); }
		const bool GetCounterOverflow() { return ((fDataRaw.at(3) >> 31) & 0x1); }
		const bool GetIncrementOverflow() { return ((fDataRaw.at(3) >> 30) & 0x1); }
		// error number of words
		const uint16_t GetNmaskErrorWords() { return ((fDataRaw.at(8) >> 0) & 0xFFFF); }

		// the following methods will be implemented in the cpp file
		const std::vector<uint16_t> GetMaskedUnits();
		const std::vector<BrilHistogramError> GetErrors();
		const std::vector<uint32_t> GetCounters();
		

	private:
		// raw data
		std::vector<uint32_t> fDataRaw;

};

// simple class to generate histograms manually (for testing)
class BrilHistogramManual {

	public:
		// basic
		BrilHistogramManual (uint16_t pNwordsTotal, uint8_t pHeaderSize, uint16_t pNbins, uint16_t pHistogramID, uint16_t pHistogramType, uint16_t pNcounterWords, uint8_t pIncrementWidth, uint8_t pCounterWidth, uint32_t pOrbitCounter, bool pIncrementOverflow, bool pCounterOverflow, uint32_t pLhcFill, uint32_t pCmsRun, uint32_t pLumiSection, uint32_t pLumiNibble, uint16_t pNmaskErrorWords, uint16_t pNunits, uint8_t pNwordsPerError, std::vector<uint32_t> pCounters, std::vector<uint16_t> pMaskedUnits, std::vector<BrilHistogramError> pErrors) : fNwordsTotal(pNwordsTotal), fHeaderSize(pHeaderSize), fNbins(pNbins), fHistogramID(pHistogramID), fHistogramType(pHistogramType), fNcounterWords(pNcounterWords), fIncrementWidth(pIncrementWidth), fCounterWidth(pCounterWidth), fOrbitCounter(pOrbitCounter), fIncrementOverflow(pIncrementOverflow), fCounterOverflow(pCounterOverflow), fLhcFill(pLhcFill), fCmsRun(pCmsRun), fLumiSection(pLumiSection), fLumiNibble(pLumiNibble), fNmaskErrorWords(pNmaskErrorWords), fNunits(pNunits), fNwordsPerError(pNwordsPerError), fCounters(pCounters), fMaskedUnits(pMaskedUnits), fErrors(pErrors) {}
		~BrilHistogramManual () { fCounters.clear(); fMaskedUnits.clear(); fErrors.clear(); }

		// histo paramters
		const uint16_t GetNwordsTotal() { return fNwordsTotal; }
		const uint8_t GetHeaderSize() { return fHeaderSize; }
		const uint16_t GetNbins() { return fNbins; }
		const uint16_t GetNunits() { return fNunits; }
		const uint16_t GetNcounterWords() { return fNcounterWords; }
		const uint8_t GetIncrementWidth() { return fIncrementWidth; }
		const uint8_t GetCounterWidth() { return fCounterWidth; }
		const uint8_t GetNwordsPerError() { return fNwordsPerError; }
		// histo info
		const uint16_t GetHistogramID() { return fHistogramID; }
		const uint16_t GetHistogramType() { return fHistogramType; }
		// tcds
		const uint32_t GetLhcFill() { return fLhcFill; }
		const uint32_t GetCmsRun() { return fCmsRun; }
		const uint32_t GetLumiSection() { return fLumiSection; }
		const uint32_t GetLumiNibble() { return fLumiNibble; }
		// data itself
		const uint32_t GetOrbitCounter() { return fOrbitCounter; }
		const bool GetCounterOverflow() { return fCounterOverflow; }
		const bool GetIncrementOverflow() { return fIncrementOverflow; }
		// error number of words
		const uint16_t GetNmaskErrorWords() { return fNmaskErrorWords; }

		// the following methods will be implemented in the cpp file
		const std::vector<uint16_t> GetMaskedUnits() { return fMaskedUnits; }
		const std::vector<BrilHistogramError> GetErrors() { return fErrors; }
		const std::vector<uint32_t> GetCounters() { return fCounters; }

	private:
		
		// define variables
		uint16_t fNwordsTotal;
		uint8_t fHeaderSize;
		uint16_t fNbins;
		uint16_t fHistogramID;
		uint16_t fHistogramType;
		uint16_t fNcounterWords;
		uint8_t fIncrementWidth;
		uint8_t fCounterWidth;
		uint32_t fOrbitCounter;
		bool fIncrementOverflow;
		bool fCounterOverflow;
		uint16_t fNmaskErrorWords;
		uint16_t fNunits;
		uint8_t fNwordsPerError;
		std::vector<uint32_t> fCounters;
		std::vector<uint16_t> fMaskedUnits;
		std::vector<BrilHistogramError> fErrors;

	public:

		// allow to update tcds data
		uint32_t fLhcFill;
		uint32_t fCmsRun;
		uint32_t fLumiSection;
		uint32_t fLumiNibble;

};


