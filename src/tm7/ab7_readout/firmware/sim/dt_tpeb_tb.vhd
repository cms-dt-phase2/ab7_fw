----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/30/2018 02:11:54 PM
-- Design Name: 
-- Module Name: dt_tpeb_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
--library UNIMACRO;
--use unimacro.Vcomponents.all;

library work;
--use work.ipbus.all;
use work.TM7_readout_decl.all;
--use work.TM7_version.all;
--use work.ipbus_reg_types.all;
--use work.minmax.all;

use work.mp7_ttc_decl.all;
--use work.mp7_data_types.all;


entity dt_tpeb_tb is
--  Port ( );
end dt_tpeb_tb;

architecture Behavioral of dt_tpeb_tb is
    constant BXs : time := 25 ns;
    constant clk160_period : time := BXs/4;

    signal ttc_clk: std_logic;
    signal ttc_rst: std_logic;
    signal clk80: std_logic;
    signal clk160: std_logic;

    signal l1a: std_logic := '0';
    signal bunch_ctr: bctr_t;
    signal evt_ctr_u: eoctr_t := (others => '0');

    signal tp : STD_LOGIC_VECTOR(11 downto 0);
    signal tp_fine : STD_LOGIC_VECTOR(4 downto 0); 

    signal tpff_do : std_logic_vector(63 downto 0);
    signal tpff_empty, tpff_ae, tpff_rden : std_logic;
    signal tpff_evn: std_logic_vector(RO_EVN_WIDTH-1 downto 0);
    signal tpff_rdcount, tpff_wrcount: std_logic_vector(12 downto 0);


begin


dt_tpeb: entity work.dt_tpeb
port map(
    -- readout configuration (semi-static)
    latency         => '0' & x"64",
    window          => '0' & x"20",
    -- reset & clocks
    ttc_rst         => ttc_rst, -- for all clocks
    ttc_clk         => ttc_clk, -- TTC and input data
    midclk          => clk160, -- Circ.Buf.-->FIFO
    clk80           => clk80,
    rdclk           => clk80, --> FIFO read
    -- ttc inputs: ttc_clk clocked
    l1a             => l1a,
    evt_ctr         => evt_ctr_u,
    bunch_ctr       => bunch_ctr,
    -- input data: ttc_clk clocked
    tp              => tp,
    tp_fine         => tp_fine,
    -- output fifo ports: rdclk clocked
    empty           => tpff_empty,
    almost_empty    => tpff_ae,
    rden            => '1',
    do              => tpff_do,
    evnout          => tpff_evn,
    ff_rdcount      => tpff_rdcount,
    ff_wrcount      => tpff_wrcount
    );

-- clocks
process 
begin
        clk160 <= '1';
        clk80 <= '1';
        ttc_clk <= '1';
    wait for clk160_period/2;
        clk160 <= '0';
    wait for clk160_period/2;
        clk160 <= '1';
        clk80 <= '0';
    wait for clk160_period/2;
        clk160 <= '0';
    wait for clk160_period/2;
        clk160 <= '1';
        clk80 <= '1';
        ttc_clk <= '0';
    wait for clk160_period/2;
        clk160 <= '0';
    wait for clk160_period/2;
        clk160 <= '1';
        clk80 <= '0';
    wait for clk160_period/2;
        clk160 <= '0';
    wait for clk160_period/2;
end process;

bunch_ctr_p: process
begin
   if unsigned(bunch_ctr) < 3563 then bunch_ctr <= std_logic_vector(unsigned(bunch_ctr) + 1);
   else bunch_ctr <= (others=>'0');
   end if;    
   wait for BXs;
end process;


stim_p: process
    constant tp_latency : natural := 20;
    constant l1a_latency: natural := 100;
begin
    tpff_rden <= '1';
    ttc_rst <= '0';

    tp <= 
        (others => '1'),
        std_logic_vector(to_unsigned(70-tp_latency,12)) after 70*BXs,
        (others => '1') after (70+1)*BXs,
        std_logic_vector(to_unsigned(75-tp_latency,12)) after 75*BXs,
        (others => '1') after (75+1)*BXs,
        std_logic_vector(to_unsigned(80-tp_latency,12)) after 80*BXs,
        (others => '1') after (80+1)*BXs
         ;

    tp_fine <= (others => '1');

    l1a <= 
        '0',
        '1' after 140*BXs,
        '0' after 141*BXs,
        '1' after 160*BXs,
        '0' after 161*BXs,
        '1' after 170*BXs,
        '0' after 171*BXs
        ;
    evt_ctr_u <= 
        x"00000000",
        x"00000001" after 140*BXs,
        x"00000002" after 160*BXs,
        x"00000003" after 170*BXs
        ;

    wait;
end process;    
    
    
end Behavioral;
