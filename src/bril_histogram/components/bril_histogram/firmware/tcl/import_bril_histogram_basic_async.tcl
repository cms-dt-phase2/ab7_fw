# basic source files
add_files -norecurse ../hdl/bril_histogram_package.vhd
add_files -norecurse ../hdl/bril_histogram_shift_register.vhd
add_files -norecurse ../hdl/async/bril_histogram_prebuffer_async.vhd
add_files -norecurse ../hdl/async/bril_histogram_core_async.vhd
add_files -norecurse ../hdl/async/bril_histogram_packer_async.vhd
add_files -norecurse ../hdl/async/bril_histogram_wrapper_async.vhd

# the timing constraint file
add_files -norecurse ../ucf/bril_histogram_timing.tcl
