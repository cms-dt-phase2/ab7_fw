// Mykyta Haranko, BRIL, CERN
// BrilHistogram decoder class

// includes
#include <BrilHistogram.h>
#include <iostream>

////
// class
////
BrilHistogram::BrilHistogram (std::vector<uint32_t> pDataRaw): fDataRaw(pDataRaw) {
	try {
		// basic check of the header
		if ( ((fDataRaw.at(0) >> 24) & 0xff) != 0xff ) {
			throw "Frame header mismatch";
		}
		// check of the vector size
		if ( GetNwordsTotal() != fDataRaw.size() ) {
			throw "Vector size mismatch";
		}
	} 
	catch (const char *str) {
		std::cout << "Init exception: " << str << std::endl;
		exit(1);
	}
}

////
// get methods
//// 

// get list of masked units (modules, chips, etc)
const std::vector<uint16_t> BrilHistogram::GetMaskedUnits() {

	// create the vector
	std::vector<uint16_t> cMaskedUnitsList;

	// check that we have masks and errors
	if (GetNmaskErrorWords() > 0) {
		try {
			// get position of thr mask in raw vector
			uint16_t cMaskOffset = GetHeaderSize() + GetNcounterWords();
			uint16_t cMaskNwords = GetNwordsPerError();

			// check mask header
			if ( ((fDataRaw.at(cMaskOffset) >> 28) & 0xF) != 0xA ) {
				throw "Mask header mismatch";
			}

			// initial iteration parameters
			uint16_t cCurrentWord = cMaskOffset;
			uint8_t cCurrentBitOffset = 27;

			// iterate over units
			for (uint16_t cUnit = GetNunits(); cUnit > 0; cUnit--) {
				// make sure that we are not exceeding the vector length
				if (cCurrentWord > (cMaskOffset+cMaskNwords-1)) {
					throw "Out of mask range";
				}

				// parse unit bit
				if ( ((fDataRaw.at(cCurrentWord) >> cCurrentBitOffset) & 0x1) == 0x1 ) {
					cMaskedUnitsList.push_back(cUnit-1);
				}

				// update word and offset
				if (cCurrentBitOffset > 0) cCurrentBitOffset -= 1;
				else {
					cCurrentBitOffset = 31;
					cCurrentWord += 1;
				}
			}
		}
		catch (const char *str) {
			std::cout << "Mask parsing exception: " << str << std::endl;
			exit(1);
		}
	
	} 

	// return the list
	return cMaskedUnitsList;

}

// parse errors
const std::vector<BrilHistogramError> BrilHistogram::GetErrors() {

	// create the vector
	std::vector<BrilHistogramError> cErrorList;

	// check that we have errors (GetNwordsPerError() stands for one mask Nwords)
	if (GetNmaskErrorWords() > GetNwordsPerError()) {
		try {
			// get position of thr error in raw vector
			uint16_t cErrorOffset = GetHeaderSize() + GetNcounterWords() + GetNwordsPerError();
			uint16_t cNerrors = (GetNmaskErrorWords() / GetNwordsPerError()) - 1;

			// iterate over errors
			for (uint16_t cErrorId = 0; cErrorId < cNerrors; cErrorId++) {

				// initial iteration parameters
				uint16_t cCurrentWord = cErrorOffset + cErrorId*GetNwordsPerError();

				// check error header
				if ( ((fDataRaw.at(cCurrentWord) >> 28) & 0xF) != 0x5 ) {
					throw "Error header mismatch";
				}

				// parse orbit and bx counter
				uint32_t cOrbitCounter = ((fDataRaw.at(cCurrentWord) >> 12) & 0xFFFF);
				uint32_t cBxCounter = (((fDataRaw.at(cCurrentWord) >> 0) & 0xFFF) << 4) | ((fDataRaw.at(cCurrentWord+1) >> 28) & 0xF);
				std::vector<uint16_t> cUnitList;			
				
				// current bit offset
				cCurrentWord += 1;
				uint8_t cCurrentBitOffset = 27;

				// iterate over units
				for (uint16_t cUnit = GetNunits(); cUnit > 0; cUnit--) {
					// make sure that we are not exceeding the vector length
					if (cCurrentWord > fDataRaw.size()-1) {
						throw "Out of vector range";
					}

					// parse unit bit
					if ( ((fDataRaw.at(cCurrentWord) >> cCurrentBitOffset) & 0x1) == 0x1 ) {
						cUnitList.push_back(cUnit-1);
					}

					// update word and offset
					if (cCurrentBitOffset > 0) cCurrentBitOffset -= 1;
					else {
						cCurrentBitOffset = 31;
						cCurrentWord += 1;
					}
				}

				// create error object
				BrilHistogramError cError(cOrbitCounter, cBxCounter, cUnitList);

				// push it to the vector
				cErrorList.push_back(cError);

			} // end cError id loop
		} 
		catch (const char *str) {
			std::cout << "Error parsing exception: " << str << std::endl;
			exit(1);
		}
	
	} // end if n mask error words

	// return the list
	return cErrorList;

}

// and finally parse counters
const std::vector<uint32_t> BrilHistogram::GetCounters() {

	// vector for counters
	std::vector<uint32_t> cCounters;

	// counter offset (come immidiately after header data)
	uint16_t cCurrentWord = GetHeaderSize();
	uint16_t cCounterLastWord = GetHeaderSize() + GetNcounterWords() - 1;
	
	try {
		// now depending on the counter width parse
		if (GetCounterWidth() <= 16) {
			while (cCurrentWord <= cCounterLastWord) {
				cCounters.push_back((fDataRaw.at(cCurrentWord) >> 0) & 0xFFFF);
				cCounters.push_back((fDataRaw.at(cCurrentWord) >> 16) & 0xFFFF);
				cCurrentWord += 1;
			}
		} else if (GetCounterWidth() <= 32) {
			while (cCurrentWord <= cCounterLastWord) {
				cCounters.push_back(fDataRaw.at(cCurrentWord));
				cCurrentWord += 1;
			}
		} else {
			throw "Wrong counter width - has to be <= 32";
		}

		// check length of the resulting vector
		if ( cCounters.size() != GetNbins() ) {
			throw "Resulting counter vector is smaller than Nbins";
		}
	}
	catch (const char *str) {
		std::cout << "Counter parsing exception: " << str << std::endl;
		exit(1);
	}

	// return
	return cCounters;

}


