# Modified for TM7; original in boards/mp7/base_fw/common/firmware/ucf/
# Area constraints for TM7

# Control region (X1Y2)

create_pblock ctrl
resize_pblock [get_pblocks ctrl] -add {CLOCKREGION_X1Y2:CLOCKREGION_X1Y3}
add_cells_to_pblock [get_pblocks ctrl] [get_cells -quiet [list infra ttc ctrl]]

# There area  few exceptions to the area constraints above due to resource availability
# In the past overwrote main constraint with "LOC" constraint in ISE.  Does not work
# in Vivado.  Use another pblock instead.

create_pblock mmcm_ttc
add_cells_to_pblock [get_pblocks mmcm_ttc] [get_cells ttc/clocks/mmcm]
resize_pblock [get_pblocks mmcm_ttc] -add {CLOCKREGION_X0Y2}

create_pblock mmcm_infra_clocks
add_cells_to_pblock [get_pblocks mmcm_infra_clocks] [get_cells infra/clocks/mmcm]
resize_pblock [get_pblocks mmcm_infra_clocks] -add {CLOCKREGION_X0Y3}

create_pblock mmcm_infra_eth
add_cells_to_pblock [get_pblocks mmcm_infra_eth] [get_cells infra/eth/mmcm]
resize_pblock [get_pblocks mmcm_infra_eth] -add {CLOCKREGION_X1Y2}
