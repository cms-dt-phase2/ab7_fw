-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Multi-channel data mixer
---- File       : dttime_layer_memory.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: Memory and additional logic to store hits' TDC time for
----              only one layer. It's divided into up to 128 different
----              subunits (buffers), each one stores information from an 
----              individual channel (wire).
----              Every buffer has 16 positions in depth.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.mixer_pkg.all;
use work.muxers_pkg.all;
use work.registers_pkg.all;
use work.xil_memory_pkg.all;

entity DTTIME_LAYER_MEMORY is
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    dtTimeLayMemIn  : in  DTTimeLayMemIn_t;
    dtTimeLayMemOut : out DTTimeLayMemOut_t
  );
end entity;

architecture TwoProcess of DTTIME_LAYER_MEMORY is

  constant COUNTER_LENGTH : integer := 4;
  constant COUNTER_ONES   : std_logic_vector(3 downto 0) := (others => '1');
  constant COUNTER_ZEROS  : std_logic_vector(3 downto 0) := (others => '0');

  -- Internal registers.
  type reg_type is record
    time_valid             : std_logic;
    buffer_out_full        : std_logic;
    buffer_out_empty       : std_logic;
    chOut_occupancy        : std_logic_vector(3 downto 0);
    buffers_full           : std_logic_vector(NUM_DTTIME_BUFFERS - 1 downto 0);
    buffers_empty          : std_logic_vector(NUM_DTTIME_BUFFERS - 1 downto 0);
    buffers_overflow       : std_logic_vector(NUM_DTTIME_BUFFERS - 1 downto 0);
    first_element_idx      : counters_arr_t;
    num_elements_to_expire : counters_arr_t;
    num_stored_elements    : counters_arr_t;
  end record;

  constant regDefault : reg_type := (
    time_valid             => '0',
    buffer_out_full        => '0',
    buffer_out_empty       => '1',
    chOut_occupancy        => (others => '0'),
    buffers_full           => (others => '0'),
    buffers_empty          => (others => '1'),
    buffers_overflow       => (others => '0'),
    first_element_idx      => (others => (others => '0')),
    num_elements_to_expire => (others => (others => '0')),
    num_stored_elements    => (others => (others => '0'))
  );
  signal reg, nextReg : reg_type := regDefault;

  signal int_ena_wr : std_logic;
  signal add_rd     : std_logic_vector(10 downto 0);
  signal add_wr     : std_logic_vector(10 downto 0);
  signal tdc_time   : std_logic_vector(16 downto 0);

  -- Inter-components connection signals
  signal buffer_in_full                   : std_logic;
  signal buffer_out_full                  : std_logic;
  signal num_elements_to_expire_shift_out : counters_arr_t;

  type mux_conn_arr_t is array (0 to 3) 
                      of std_logic_vector(NUM_DTTIME_BUFFERS - 1 downto 0);

  signal mux_in_conn_arr  : mux_conn_arr_t;
  signal mux_out_conn_arr : mux_conn_arr_t;

  -- Auxiliary signals
  signal chOut_stored_elements    : std_logic_vector(3 downto 0);
  signal chIn_num_stored_elements : std_logic_vector(3 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  dtTimeLayMemOut.tdc_time             <= tdc_time;
  dtTimeLayMemOut.time_valid           <= reg.time_valid;
  dtTimeLayMemOut.storing_hit          <= int_ena_wr;
  --
  dtTimeLayMemOut.buffer_out_full      <= reg.buffer_out_full;
  dtTimeLayMemOut.buffer_out_empty     <= reg.buffer_out_empty;
  dtTimeLayMemOut.buffers_empty        <= reg.buffers_empty;
  dtTimeLayMemOut.buffers_overflow     <= reg.buffers_overflow;
  dtTimeLayMemOut.channelOut_occupancy <= reg.chOut_occupancy;

  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, dtTimeLayMemIn, chIn_num_stored_elements, buffer_in_full,
    buffer_out_full, num_elements_to_expire_shift_out,chOut_stored_elements
  )
    variable chIn_idx  : integer;
    variable chOut_idx : integer;
    --
    variable add_wr_delta : std_logic_vector(3 downto 0);
    variable add_rd_delta : std_logic_vector(3 downto 0);
    --
    variable current_stored_elements : counters_arr_t;
  begin
    -- Default values
    nextReg    <= reg;
    int_ena_wr <= '0';

    -- Data aliases.
    chIn_idx  := to_integer(unsigned(dtTimeLayMemIn.channelIn_id));
    chOut_idx := to_integer(unsigned(dtTimeLayMemIn.channelOut_id));
    --
    -- The highest address part is devoted to determine the buffer position
    -- inside the whole memory space. The lower part identifies the element
    -- index stored into a specific buffer.
    --
    -- This is the relative displacement inside the incoming buffer.
    add_wr_delta := chIn_num_stored_elements + reg.first_element_idx(chIn_idx);
    add_wr <= dtTimeLayMemIn.channelIn_id  & add_wr_delta;

    -- This is the relative displacement inside the outgoing buffer.
    add_rd_delta := dtTimeLayMemIn.dataOut_idx + 
                    reg.first_element_idx(chOut_idx);

    add_rd <= dtTimeLayMemIn.channelOut_id & add_rd_delta;

    ---------------------------------------
    -- Reading logic.
    --
    -- This logic allows to set 'DTHit valid' flag to 'invalid' on those 
    -- cases where the wire/cell index is outside the chamber limits.
    -- This replaces the C++ version behaviour based on "dummy" cells.
    --
    -- It's neccesary to register the 'time_valid' to delay it one clock cycle
    -- in order to align it with respect to the outgoing time. It is 
    -- intrinsecally delayed because BRAM takes one clock cycle to serve data
    -- after setting the reading address.
    --
    if dtTimeLayMemIn.dataOut_idx   >= chOut_stored_elements    or
       dtTimeLayMemIn.channelOut_id > LAST_AVAILABLE_WIRE_IDX
    then nextReg.time_valid <= '0';
    else nextReg.time_valid <= '1';
    end if;
    
    -- Registers to align signals with outgoing memory data.
    nextReg.buffer_out_full  <= buffer_out_full;
    nextReg.chOut_occupancy  <= reg.num_stored_elements(chOut_idx);
    nextReg.buffer_out_empty <= reg.buffers_empty(chOut_idx);

    ---------------------------------------
    -- Expiring logic and pointers control.
    --
    LOGIC_PER_CHANNEL : for i in 0 to NUM_DTTIME_BUFFERS - 1 loop
    
      -- Useful auxiliary variable
      current_stored_elements(i) := reg.num_stored_elements(i) - 
                                    num_elements_to_expire_shift_out(i);
      --
      -- Element counters and pointers are reduced/moved every expiration 
      -- cycle. The amount to consider in those operations is the number of
      -- elements that were stored in each channel 16 expiration cycles before
      -- current one, that have been propagated along the shif-register chain.
      --
      if dtTimeLayMemIn.expiration_cycle = '1' and 
         reg.buffers_empty(i)            = '0'
      then 
        nextReg.buffers_full(i)      <= '0';
        nextReg.first_element_idx(i) <= reg.first_element_idx(i) + 
                                        num_elements_to_expire_shift_out(i);
        --
        -- When the buffer is full (16 elements stored) is necessary to adjust
        -- in 1 unit the amount of remaining elements in that buffer.
        --
        if reg.buffers_full(i) = '1' and 
           num_elements_to_expire_shift_out(i) /= COUNTER_ZEROS
        then 
          nextReg.num_stored_elements(i) <= current_stored_elements(i) + 1;
        else 
          if current_stored_elements(i) = COUNTER_ZEROS then
            nextReg.buffers_empty(i) <= '1';
          end if;
        
          nextReg.num_stored_elements(i) <= current_stored_elements(i);
        end if;

      end if;
    end loop;

    ---------------------------------------
    -- Writing logic.
    --
    -- Manage elements' counters and overflow detectors.
    if dtTimeLayMemIn.ena_wr = '1' then
      if buffer_in_full = '0' then 
        int_ena_wr                       <= '1';
        nextReg.buffers_empty(chIn_idx)  <= '0';
                  
        if chIn_num_stored_elements = COUNTER_ONES then
          nextReg.buffers_full(chIn_idx) <= '1';
        else
          nextReg.buffers_full(chIn_idx) <= '0';
          nextReg.num_stored_elements(chIn_idx) 
                    <= reg.num_stored_elements(chIn_idx) + 1;
          --
          -- This counter counts the number of elements that was received 
          -- between two consecutive expiration cycles.
          --
          -- Its value is reset whenever one expiration cycle takes place, but
          -- only after inserting it's value in the associated shift register
          -- (see bottom of the code for associated components) that will 
          -- keep the value during 16 cycles.
          --
          -- After that cycles' number the value will be used to move the 
          -- index pointer related to the corresponding wire channel.
          --
          nextReg.num_elements_to_expire(chIn_idx) 
                    <= reg.num_elements_to_expire(chIn_idx) + 1;
        end if;
      else 
        int_ena_wr <= '0';
        --
        -- Bit status for overflow detection. It can only be reset by using the 
        -- "reset_overflow" flag.
        --
        nextReg.buffers_overflow(chIn_idx) <= '1';
      end if;
    end if;

    if dtTimeLayMemIn.expiration_cycle = '1' then
      nextReg.num_elements_to_expire <= (others => (others => '0'));
    end if;
    
    -- Reset overflow flags control
    if dtTimeLayMemIn.reset_overflow = '1' then
      nextReg.buffers_overflow <= (others => '0');
    end if;

  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1' 
      then reg <= regDefault;
      else reg <= nextReg;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  -- Memory block to store up to 128 channels for TDC time (16 elements 
  -- in depth).
  CHANNELS_MEMORY : XIL_BRAM_DUALPORT_D2048_W10_TO_8BITS 
  generic map(
    DATA_WIDTH  => 17,  -- From 10 to 18
    ENA_REG_OUT => 0    -- Enable => 1 / Disable => 0
  ) 
  port map(
    rst      => rst,
    clk_wr   => clk,
    ena_wr   => int_ena_wr,
    add_wr   => add_wr,
    data_in  => dtTimeLayMemIn.tdc_time,
    --
    clk_rd   => clk,
    ena_rd   => dtTimeLayMemIn.ena_rd,
    add_rd   => add_rd,
    data_out => tdc_time
  );

  -- Muxer to select the 'full' status flag from the OUTGOING data channel.
  FULL_OUT_MUX : MUXER128_1 port map (
    sel   => dtTimeLayMemIn.channelOut_id,
    d_in  => reg.buffers_full,
    d_out => buffer_out_full
  );

  -- Muxer to select the 'full' status flag from the INCOMING data channel.
  FULL_IN_MUX : MUXER128_1 port map (
    sel   => dtTimeLayMemIn.channelIn_id,
    d_in  => reg.buffers_full,
    d_out => buffer_in_full
  );
  --
  -- This block of muxers allows to select the elements' counter associated to
  -- the pointed channel, to store a new element (TDC time) from incoming FIFO.
  -- It's built with 4 multiplexers, one for each counter bit, using the 
  -- elementary 1-bit muxer blocks in order to optimze the speed. It allows 
  -- that the synthetizer uses the best routing resources inside Virtex-7
  -- FPGA family.
  --
  STORED_ELEM_MUX : for i in 0 to COUNTER_LENGTH - 1 generate
    BIT_SEL : for j in 0 to NUM_DTTIME_BUFFERS - 1 generate
      mux_in_conn_arr(i)(j)  <= reg.num_stored_elements(j)(i);
      mux_out_conn_arr(i)(j) <= reg.num_stored_elements(j)(i);
    end generate;
  
    MUX_IN_STORED_ELEM : MUXER128_1 port map (
      sel   => dtTimeLayMemIn.channelIn_id,
      d_in  => mux_in_conn_arr(i),
      d_out => chIn_num_stored_elements(i)
    );

    MUX_OUT_STORED_ELEM : MUXER128_1 port map (
      sel   => dtTimeLayMemIn.channelOut_id,
      d_in  => mux_out_conn_arr(i),
      d_out => chOut_stored_elements(i)
    );
  end generate;

  EXPIRE_GEN_SHFREG : for i in 0 to NUM_DTTIME_BUFFERS - 1 generate
    --
    -- These shift registers allow to keep information about which channels have
    -- data, maintaining it during 16 "expiration cycles". Each shift step takes 
    -- place every new BX, so data is kept during 16 BX's. This is the same 
    -- amount than the number of elements that can be stored in each 
    -- channel buffer.
    --
    -- The 'VECTOR_WIDTH' value corresponds to the width of a single element 
    -- comprised in'counters_arr_t'.
    --
    EXPIRE_SHFREG_CONTROL : VECTOR_SHIFT_REGISTER 
    generic map (VECTOR_WIDTH => COUNTER_LENGTH, SHIFT_STEPS => 16)
    port map (
      clk        => clk, 
      enable     => dtTimeLayMemIn.expiration_cycle,
      vector_in  => reg.num_elements_to_expire(i),
      vector_out => num_elements_to_expire_shift_out(i)
    );
  end generate;
  
end architecture;
