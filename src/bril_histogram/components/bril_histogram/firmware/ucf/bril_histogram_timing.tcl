#### timing constraints for the bril_histogram module

## constraints for the cdc

# constraints for the sync implementation
set sync_cell [get_cells -hier -filter {NAME =~ *bril_histogram_wrapper_sync_inst}]
if {$sync_cell != ""} {
    # no constraints needed here
}

# constraints for the async implementation
set async_cell [get_cells -hier -filter {NAME =~ *bril_histogram_wrapper_async_inst}]
if {$async_cell != ""} {
    # get the clock period
    #set PROCESSING_CLOCK_PERIOD [get_property PROCESSING_CLOCK_PERIOD $async_cell ]
    #set FIFO_READ_CLOCK_PERIOD [get_property FIFO_READ_CLOCK_PERIOD $async_cell ]
    # seting delays 
    #set MAX_DELAY_FROM_PROCESSING_CLOCK [expr $PROCESSING_CLOCK_PERIOD - 0.1]
    
    # the fifo fsm is triggered faaar after the buffers are switched - so false path for the port b 
    set_false_path -from [get_pins -hier -filter {NAME =~ *bril_histogram_wrapper_async_inst/buffer_to_write_reg[*]/C}] -to [get_pins -hier -filter {NAME =~ *bril_histogram_wrapper_async_inst/ram_dout_b_to_packer_reg[*]/D}]
    # brams
    set_false_path -from [get_pins -hier -filter {NAME =~ *bril_histogram_wrapper_async_inst/buffer_to_write_reg[*]*/C}] -to [get_pins -hier -filter {NAME =~ *bril_histogram_wrapper_async_inst/bram*/*/*/ADDRBWRADDR[*]}]
    set_false_path -from [get_pins -hier -filter {NAME =~ *bril_histogram_wrapper_async_inst/buffer_to_write_reg[*]*/C}] -to [get_pins -hier -filter {NAME =~ *bril_histogram_wrapper_async_inst/bram*/*/*/WEBWE[*]}]

    # same here - number of error words is saved really on new_histogram
    set_false_path -from [get_pins -hier -filter {NAME =~ *bril_histogram_wrapper_async_inst/bril_histogram_packer_async_inst/number_of_error_words_buffered_reg[*]/C}] -to [get_pins -hier -filter {NAME =~ *bril_histogram_wrapper_async_inst/bril_histogram_packer_async_inst/fifo_words_cnt_int_reg[*]/D}]

}
