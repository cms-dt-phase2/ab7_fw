-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Multiplexers components
---- Design unit: Multiplexers general package
---- File       : muxers_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: Multiplexers component for synthesis optimization
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package muxers_pkg is

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  --
  component MUXER2_1 is
  port (
    sel   : in  std_logic;
    d_in  : in  std_logic_vector(1 downto 0);
    d_out : out std_logic
  );
  end component;

  component MUXER3_1 is
  port (
    sel   : in  std_logic_vector(1 downto 0);
    d_in  : in  std_logic_vector(2 downto 0);
    d_out : out std_logic
  );
  end component;
  
  component MUXER4_1 is
    port (
      sel   : in  std_logic_vector(1 downto 0);
      d_in  : in  std_logic_vector(3 downto 0);
      d_out : out std_logic
    );
  end component;
  
  component MUXER8_1 is
    port (
      sel   : in  std_logic_vector(2 downto 0);
      d_in  : in  std_logic_vector(7 downto 0);
      d_out : out std_logic
    );
  end component;

  component MUXER16_1 is
    port (
      sel   : in  std_logic_vector(3 downto 0);
      d_in  : in  std_logic_vector(15 downto 0);
      d_out : out std_logic
    );
  end component;

  component MUXER32_1 is
  port (
    sel   : in  std_logic_vector(4 downto 0);
    d_in  : in  std_logic_vector(31 downto 0);
    d_out : out std_logic
  );
  end component;

  component MUXER64_1 is
  port (
    sel   : in  std_logic_vector(5 downto 0);
    d_in  : in  std_logic_vector(63 downto 0);
    d_out : out std_logic
  );
  end component;

  --
  -- This muxer is a bit slower than the next one (128:1), because it uses
  -- an additional outgoing LUT6, instead of the outgoing stage MUXF7 inside
  -- an slice.
  -- In some cases it could be recommended to use 128:1, setting unused inputs
  -- to '0'.
  --
  component MUXER96_1 is
  port (
    sel   : in  std_logic_vector(6 downto 0);
    d_in  : in  std_logic_vector(95 downto 0);
    d_out : out std_logic
  );
  end component;
  
  component MUXER128_1 is
  port (
    sel   : in  std_logic_vector(6 downto 0);
    d_in  : in  std_logic_vector(127 downto 0);
    d_out : out std_logic
  );
  end component;
  
end package;
