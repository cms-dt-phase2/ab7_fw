-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Multi-channel data mixer
---- File       : bx_time_converter.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-03
-------------------------------------------------------------------------------
---- Description: It converts bx_time field format from [BX, precision] to
----              a value in "ns".
----              Both are stored into a 17 bits in length field.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.dt_constants_pkg.all;
use work.dt_buffers_pkg.all;

entity BX_TIME_CONVERTER is
  port (
    clk           : in  std_logic;
    rst           : in  std_logic;
    bxTimeConvIn  : in  BXTimeConvIn_t;
    bxTimeConvOut : out BXTimeConvOut_t
  );
end entity;

architecture Behavioural of BX_TIME_CONVERTER is

  type reg_type is record
    hit_valid_1    : std_logic;
    hit_valid_2    : std_logic;
    hit_valid_3    : std_logic;
    bx_fine_time   : unsigned(4 downto 0);
    bx_coarse_time : unsigned(16 downto 0);
    bx_time        : unsigned(16 downto 0);
    --
    -- This items are only to pipeline data.
    -- Some of the record fields will be overwritten at the end of the process
    -- so synthetizer will complain about a bunch of unused bits that will be
    -- removed from the final design, but it doesn't matter.
    -- 
    hit_1          : DTRawHit_t;
    hit_2          : DTRawHit_t;
    hit_3          : DTRawHit_t;
  end record;

  constant regDefault : reg_type := (
    hit_valid_1    => '0',
    hit_valid_2    => '0',
    hit_valid_3    => '0',
    bx_fine_time   => (others => '0'),
    bx_coarse_time => (others => '0'),
    bx_time        => (others => '0'),
    hit_1          => (others => (others => '0')),
    hit_2          => (others => (others => '0')),
    hit_3          => (others => (others => '0'))
  );
  signal reg, nextReg : reg_type := regDefault;

  signal rawHitsFF_wr : std_logic;
  signal outgoing_hit : DTRawHit_t;
  
--   attribute use_dsp48 : string;
--   attribute use_dsp48 of bx_time : signal is "yes";
  
begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  bxTimeConvOut.full   <= bxTimeConvIn.rawHitsFFalmost_full;
  bxTimeConvOut.rawHit <= bxTimeConvIn.rawHit 
                              when bxTimeConvIn.bx_time_convert_ena = '0'
                              else outgoing_hit;

  bxTimeConvOut.rawHitsFF_wr <= bxTimeConvIn.wrEna 
                                    when bxTimeConvIn.bx_time_convert_ena = '0'
                                    else rawHitsFF_wr;

  -- Useful aliases                                  
  rawHitsFF_wr <= reg.hit_valid_3 and not bxTimeConvIn.rawHitsFF_full;
  
  outgoing_hit.tdc_time       <= std_logic_vector(reg.bx_time);
  outgoing_hit.channel_id     <= reg.hit_3.channel_id;
  outgoing_hit.layer_id       <= reg.hit_3.layer_id;
  outgoing_hit.super_layer_id <= reg.hit_3.super_layer_id;

  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, bxTimeConvIn) 
    variable bx_fine   : unsigned(4 downto 0)  := (others => '0');
    variable bx_coarse : unsigned(11 downto 0) := (others => '0');
  begin
    ---------------------------
    -- Clock cycle 1
    ---------------------------
    -- Initial registration
    nextReg.hit_1       <= bxTimeConvIn.rawHit;
    nextReg.hit_valid_1 <= bxTimeConvIn.wrEna;

    -- Aliases
    bx_fine   := unsigned(reg.hit_1.tdc_time(4 downto 0));
    bx_coarse := unsigned(reg.hit_1.tdc_time(16 downto 5));

    ---------------------------
    -- Clock cycle 2
    ---------------------------
    -- Encoder to convert BX fine part into "ns"
    nextReg.bx_fine_time   <= ns_time_from_bx_fine( bx_fine );
    nextReg.bx_coarse_time <= bx_coarse * to_unsigned(LHC_CLK_FREQ, 5);
    nextReg.hit_2          <= reg.hit_1;
    nextReg.hit_valid_2    <= reg.hit_valid_1;
    
    ---------------------------
    -- Clock cycle 3
    ---------------------------
    nextReg.bx_time     <= reg.bx_coarse_time + reg.bx_fine_time;
    nextReg.hit_3       <= reg.hit_2;
    nextReg.hit_valid_3 <= reg.hit_valid_2;
  end process;
  
  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1'
      then reg <= regDefault;
      else reg <= nextReg;
      end if;
    end if;
  end process;
  
end architecture;
