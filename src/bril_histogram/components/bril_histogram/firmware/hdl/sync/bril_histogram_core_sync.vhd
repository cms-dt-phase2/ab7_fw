----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_core_sync - rtl
-- Description: 
-- 
--          Here we generate memory in optimized way: we pack data either 2 counters per words or 1 counter per word
--          The data is streamed out without backpressure (32bit wide bus + data valid)
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- bril package
use work.bril_histogram_package.all;

entity bril_histogram_core_sync is
Generic (
    -- memory parameters
    MEMORY_BLOCK_PARAMETERS     : memory_block_parameters_type;
    -- histogram parameters
    NUMBER_OF_BINS              : natural;
    COUNTER_WIDTH               : natural;
    INCREMENT_WIDTH             : natural
);
Port(
    -- core signals
    reset_i             : in std_logic; -- sync reset
    clk_i               : in std_logic; -- counting clock, typ. 40mhz
    -- tcds signals
    new_histogram_i     : in std_logic; -- start outputting data (orbit reset oc0)
    new_iteration_i     : in std_logic; -- resetting the bin counter (if number of bins is 3564 corresponding to 3564bx - this signal is the bx reset bx0)      
    -- counter signals  
    increment_i         : in std_logic_vector(INCREMENT_WIDTH-1 downto 0); -- increment command
    --
    iteration_counter_o : out natural;
    overflow_o          : out std_logic;
    data_o              : out std_logic_vector(31 downto 0); -- output of the memory
    data_valid_o        : out std_logic -- write enable
);
end bril_histogram_core_sync;

architecture rtl of bril_histogram_core_sync is    
    
    -- memory signals   
    signal ram_addr_counter   : natural range 0 to MEMORY_BLOCK_PARAMETERS.total_bram_size_sync-1;   
    signal ram_addr_a         : natural range 0 to MEMORY_BLOCK_PARAMETERS.total_bram_size_sync-1;   
    signal ram_addr_b         : natural range 0 to MEMORY_BLOCK_PARAMETERS.total_bram_size_sync-1; 
    -- write only for port q
    signal ram_din_a          : std_logic_vector(MEMORY_BLOCK_PARAMETERS.bram_word_width-1 downto 0);   -- RAM input data
    signal ram_we_a          : std_logic; -- Byte-Write enable
    -- read only for port b
    signal ram_dout_b         : std_logic_vector(MEMORY_BLOCK_PARAMETERS.bram_word_width-1 downto 0);   -- RAM output data
    
    -- bin counting
    signal iteration_counter    : natural;
    signal reset_memory_content : std_logic;
    signal send_data            : std_logic;
    
    -- only for w <= 16 width case need to store previous clock cycle (we increment two counters at a time)
    signal doublet_counter                      : natural range 0 to 1;
    signal increment_prev                       : std_logic_vector(INCREMENT_WIDTH-1 downto 0);
 
begin
       
    -- ram 
    bril_histogram_bram_inst: entity work.bril_histogram_bram
    generic map (
        BRAM_WORD_WIDTH     => MEMORY_BLOCK_PARAMETERS.bram_word_width,
        BRAM_DEPTH          => MEMORY_BLOCK_PARAMETERS.total_bram_size_sync
    )
    port map ( 
        clk_a_i             => clk_i,
        clk_b_i             => clk_i, -- same clock for both ports
        -- input port
        ram_addr_a_i        => ram_addr_a,
        ram_din_a_i         => ram_din_a,
        ram_we_a_i          => ram_we_a,
        -- output port
        ram_addr_b_i        => ram_addr_b,
        ram_dout_b_o        => ram_dout_b
    );

    -- here we do bin counting  
    process (clk_i)
    begin
        if rising_edge(clk_i) then
            if reset_i = '1' then
                reset_memory_content <= '1'; -- first iteration of this histogram, always reset memory content
                send_data <= '0'; -- never send data after reset
                iteration_counter <= 0;
            elsif new_iteration_i = '1' then -- defacto new orbit (bx0)
                reset_memory_content <= new_histogram_i; -- if we are still accumulating the NB or NB4 or whatever - don't reset memory content, if it's a new one - reset
                send_data <= new_histogram_i; -- send data if we started a new histogram
                -- also increment the iteration counter
                if new_histogram_i = '1' then
                    iteration_counter <= 0;
                else
                    iteration_counter <= iteration_counter + 1;
                end if;
            end if;
        end if;
    end process;
    -- output iteration counter to the streamer
    iteration_counter_o <= iteration_counter;

    -- packing density
    -- if counter width is w <= 16 - we pack 2 words per memory cell (will require 2x36k brams)
    -- if counter width is 16 < w <= 32 - only one word per memory cell (will require 4x36k brams)
    -- let's be realistic - we are expecting something in range 16-24 bits so the other options are just in case
    gen_2words_per_1cell: if COUNTER_WIDTH <= 16 generate        
        -- best we can do here is to split in 2x36k memory blocks
        -- two words per cell
        process (clk_i)            
        begin
            if rising_edge(clk_i) then

                -- reset of data in
                if reset_i = '1' then
                    --
                    ram_addr_a <= 0;
                    ram_addr_b <= 0;
                    ram_din_a <= (others => '0');
                    ram_we_a <= '0';
                    -- doublet counter
                    ram_addr_counter <= 0;
                    doublet_counter <= 0;
                    --
                    increment_prev <= (others => '0');  
                    -- send data to the readout process
                    overflow_o <= '0';
                    data_o <= (others => '0');
                    data_valid_o <= '0';                 
                else          
                    -- always reset data to readout on the next bx (in case it was written)
                    data_o <= (others => '0');
                    data_valid_o <= '0';  
                           
                    -- now every second clock cycle, the current data is already loaded from the memory so that we can increment
                    if doublet_counter = 1 then  
                        -- in case we were requested to send data before modification - do it
                        if send_data = '1' then
                            data_o <= ram_dout_b;
                            data_valid_o <= '1';
                        end if;
                    
                        -- store first bx     
                        if reset_memory_content = '0' then 
                            if (2**COUNTER_WIDTH-1 - ram_dout_b(COUNTER_WIDTH-1 downto 0)) >= increment_prev then
                                ram_din_a(COUNTER_WIDTH-1 downto 0) <= ram_dout_b(COUNTER_WIDTH-1 downto 0) + increment_prev;
                            else -- overflow condition
                                ram_din_a(COUNTER_WIDTH-1 downto 0) <= (others => '1');
                                overflow_o <= '1';
                            end if;
                        else 
                            -- in case we neet to reset a histogram
                            ram_din_a(COUNTER_WIDTH-1 downto 0) <= std_logic_vector(resize(unsigned(increment_prev), COUNTER_WIDTH)); -- expand width of the increment (as everything else is zeros) 
                        end if;
                        
                        -- store second bx
                        if reset_memory_content = '0' then 
                            if (2**COUNTER_WIDTH-1 - ram_dout_b(COUNTER_WIDTH-1+16 downto 16)) >= increment_i then
                                ram_din_a(COUNTER_WIDTH-1+16 downto 16) <= ram_dout_b(COUNTER_WIDTH-1+16 downto 16) + increment_i;
                            else -- overflow condoition
                                ram_din_a(COUNTER_WIDTH-1+16 downto 16) <= (others => '1');
                                overflow_o <= '1';
                            end if;
                        else -- resetting a histogram
                            ram_din_a(COUNTER_WIDTH-1+16 downto 16) <= std_logic_vector(resize(unsigned(increment_i), COUNTER_WIDTH)); -- expand width of the increment (as everything else is zeros) 
                        end if;
                        -- write
                        ram_addr_a <= ram_addr_counter;
                        -- read
                        if (ram_addr_counter < NUMBER_OF_BINS/2-2) then
                            ram_addr_b <= (ram_addr_counter+2); 
                        else 
                            ram_addr_b <= (ram_addr_counter+2 - NUMBER_OF_BINS/2);
                        end if;
                        -- main counter
                        ram_addr_counter <= ram_addr_counter + 1;
                        -- write enable
                        ram_we_a <= '1';
                    else
                        -- don't write on this bx
                        ram_we_a <= '0';
                    end if;
                    
                    -- store the increment for one lcock cycle 
                    increment_prev <= increment_i;   
                    
                    -- increment doublet counter
                    -- make sure to reset the bram address when we are done
                    if new_histogram_i = '1' or new_iteration_i = '1' then
                        ram_addr_counter <= 0;
                        doublet_counter <= 0;
                        overflow_o <= '0'; -- reset the overflow of the histogram
                    else 
                        -- just in case. should never haven
                        if ram_addr_counter = MEMORY_BLOCK_PARAMETERS.total_bram_size_sync-1 and doublet_counter = 1 then -- doublet has to be finalized
                            ram_addr_counter <= 0;
                            overflow_o <= '0';
                        end if;  
                        
                        -- doublet counter
                        if doublet_counter < 1 then
                            doublet_counter <= doublet_counter + 1;
                        else
                            doublet_counter <= 0;
                        end if;
                    end if; 
                        
                end if;       
            end if;
        end process;
    end generate gen_2words_per_1cell;
        
    gen_1word_per_1cell: if COUNTER_WIDTH > 16 and COUNTER_WIDTH <= 32 generate        
        -- derived from the write address
        ram_addr_b <= (ram_addr_a + 3) when (ram_addr_a < NUMBER_OF_BINS-3) else (ram_addr_a + 3 - NUMBER_OF_BINS);
        -- finally assign data
        process (clk_i)            
        begin
            if rising_edge(clk_i) then

                -- reset of data in
                if reset_i = '1' then
                    --
                    ram_din_a <= (others => '0');
                    ram_we_a <= '0';
                    --
                    ram_addr_a <= 0;
                    ram_addr_counter <= 0;
                    -- send data to the readout process
                    overflow_o <= '0';
                    data_o <= (others => '0');
                    data_valid_o <= '0';                 
                else          
                    -- always reset data to readout on the next bx (in case it was written)
                    data_o <= (others => '0');
                    data_valid_o <= '0';                 
                       
                    -- now every clock cycle, the current data is already loaded from the memory and we can increment
                    -- in case we were requested to send data before modification - do it
                    if send_data = '1' then
                        data_o <= ram_dout_b; 
                        data_valid_o <= '1';
                    end if;
                
                    -- store this bx     
                    if reset_memory_content = '0' then 
                        if (2**COUNTER_WIDTH-1 - ram_dout_b(COUNTER_WIDTH-1 downto 0)) >= increment_i then
                            ram_din_a(COUNTER_WIDTH-1 downto 0) <= ram_dout_b(COUNTER_WIDTH-1 downto 0) + increment_i;
                        else
                            ram_din_a(COUNTER_WIDTH-1 downto 0) <= (others => '1');
                            overflow_o <= '1';
                        end if;
                    else -- in case we neet to reset a histogram
                        ram_din_a(COUNTER_WIDTH-1 downto 0) <= std_logic_vector(resize(unsigned(increment_i), COUNTER_WIDTH)); -- expand width of the increment (as everything else is zeros) 
                    end if;
                    
                    --
                    ram_addr_a <= ram_addr_counter;
                    -- increment the ram_addr
                    if new_histogram_i = '1' or new_iteration_i = '1' then
                        ram_addr_counter <= 0;
                        overflow_o <= '0';
                    else
                        if ram_addr_counter < NUMBER_OF_BINS-1 then
                            ram_addr_counter <= ram_addr_counter + 1;
                        else -- should never happen
                            ram_addr_counter <= 0;
                            overflow_o <= '0';
                        end if;
                    end if;
                    -- write enable
                    ram_we_a <= '1';
                                 
                end if;       
            end if;
        end process;
    end generate gen_1word_per_1cell;
    
end rtl;
