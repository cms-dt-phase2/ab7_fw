# AB7 firmware

AB7 stands for Alpha Backend 7. It is a development backend for the CMS DT phase 2 upgrade, which receives data from the TDC boards (OBDT boards at UXC or, initially, Virtex7 evaluation boards at SX5) and performs both the readout of the hits and the generation of trigger primitives (which are also included in the readout payload).

It runs in the TM7 board for development purposes. Eventually, its functionality will be ported to a different board (thus the "Alpha").

## Structure of the project

### /src/

This is the main sources folder. 

#### /src/tm7/

The sources specific to tm7, and also some files that needed to be modified from the original repos (mp7, ipbus...). The original route should be written on the file to ease backport of changes.

#### /src/cactusupgrades/

This folder should be almost a checkout from the cactusupgrades folder from https://gitlab.cern.ch/cms-cactus/firmware/mp7

The ipbus firmware was originally (in the SVN) hosted together with the rest of the mp7 packages, but later on, it began being used outside of cms, and even outside of cern, and because of that, it eventually started to be hosted at https://github.com/ipbus/ . At early mp7 releases, the ipbus folders are still at cactusupgrades.

The backport of changes from their original sources at the mp7 and ipbus repos is not automatic.
The details of the relationship of files used in this project to their original sources can be found on folder /aux_data/cactusdiffs.md

### /vivado/

This folder contains the vivado project itself. The only versioned files here whould be the vivado project file top.xpr and the final generated bitfile (top.bit).

There should be no source files in this folder. However, for historical reasons, there are some IP cores in the /vivado/top/top.srcs/sources_1/ip folder.

### /aux_data/

Other auxiliary data.

