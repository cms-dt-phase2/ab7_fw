----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
-- Module Name: bril_histogram_readout_ipbus - rtl
-- Description: 
--      !!! Inspired by the ipbus_reg_v module from the ipbus source code (Dave Newbold, March 20111)
--      Ipbus slave to control the injector for the bril histogram testing
--          
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.ipbus_reg_types.all;

entity bril_histogram_injector_ipbus is
port(
    clk				                : in std_logic;
    reset				            : in std_logic;
    ipbus_in			            : in ipb_wbus;
    ipbus_out			            : out ipb_rbus;
    --
    injection_data_o                : out std_logic_vector(31 downto 0);
    injection_reset_pointer_o       : out std_logic;
    injection_wr_en_o               : out std_logic;
    injection_norbits_available_o   : out natural;
    injection_norbits_tocollect_o   : out natural;
    injection_start_o               : out std_logic;
    injection_stop_o                : out std_logic
);
	
end bril_histogram_injector_ipbus;

architecture rtl of bril_histogram_injector_ipbus is

	constant N_REG: positive := 3; -- defined for the injector
	constant ADDR_WIDTH: integer := calc_width(N_REG);

	signal reg: ipb_reg_v(N_REG - 1 downto 0);
	signal ri: ipb_reg_v(2 ** ADDR_WIDTH - 1 downto 0);
	signal sel: integer range 0 to 2 ** ADDR_WIDTH - 1 := 0;

begin

	-- address
	sel <= to_integer(unsigned(ipbus_in.ipb_addr(ADDR_WIDTH - 1 downto 0))) when ADDR_WIDTH > 0 else 0;
	
	-- write and read
	process(clk)
	begin
		if rising_edge(clk) then
			-- register write
			if reset = '1' then
				reg <= (others => (others => '0'));
			elsif ipbus_in.ipb_strobe = '1' and ipbus_in.ipb_write = '1' and sel < N_REG then
				reg(sel) <= ipbus_in.ipb_wdata;
			else
				-- self resetting command word
				reg(2) <= (others => '0');
			end if;
			-- independent
			if ipbus_in.ipb_strobe = '1' and ipbus_in.ipb_write = '1' and sel = 0 then
                injection_data_o <= ipbus_in.ipb_wdata;
                injection_wr_en_o <= '1';
            else
                injection_data_o <= (others => '0');
                injection_wr_en_o <= '0';
            end if;
		end if;
	end process;
	
	-- read assignment
	ri(N_REG - 1 downto 0) <= reg;
	ri(2 ** ADDR_WIDTH - 1 downto N_REG) <= (others => (others => '0'));
	
	-- read
	ipbus_out.ipb_rdata <= ri(sel);
	ipbus_out.ipb_ack <= ipbus_in.ipb_strobe;
	ipbus_out.ipb_err <= '0';

	-- assign output
	injection_norbits_available_o   <= to_integer(unsigned(reg(1)(15 downto 0)));
	injection_norbits_tocollect_o   <= to_integer(unsigned(reg(1)(31 downto 16)));
	injection_start_o               <= reg(2)(0);
	injection_stop_o                <= reg(2)(1);
	injection_reset_pointer_o       <= reg(2)(8);

end rtl;
