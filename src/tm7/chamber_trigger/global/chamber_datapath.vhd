-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Chamber data path
---- File       : chamber_datapath.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-11
-------------------------------------------------------------------------------
---- Description: Process HITS for a given chamber (2 phi superlayers), 
----              generating reconstructed segments/primitives.
----              It allows to select the outgoing data, amongst chamber 
----              primitive, SL1 segment or SL3 segment, by mean of a 
----              configurable selector.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
use work.dt_buffers_pkg.all;
use work.dt_common_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.globalpath_pkg.all;
use work.memory_pkg.all;
use work.sl_matcher_pkg.all;

entity CHAMBER_DATAPATH is
  port (
    clk_fifo_rd        : in  std_logic;
    clk_fifo_wr        : in  std_logic;
    clk_mixer          : in  std_logic;
    clk_analyzer       : in  std_logic;
    clk_dup_filter     : in  std_logic;
    clk_qenhan_filter  : in  std_logic;
    clk_sl_matcher     : in  std_logic;
    --
    rst_fifo_rd        : in  std_logic;
    rst_fifo_wr        : in  std_logic;
    rst_mixer          : in  std_logic;
    rst_analyzer       : in  std_logic;
    rst_dup_filter     : in  std_logic;
    rst_qenhan_filter  : in  std_logic;
    rst_sl_matcher     : in  std_logic;
    --
    chamGlobDatPathIn  : in  ChGlobalDataPathIn_t;
    chamGlobDatPathOut : out ChGlobalDataPathOut_t
  );
end entity;

architecture Behavioural of CHAMBER_DATAPATH is

  -- Modules interconnection signals
  signal fifo_empty_sl        : std_logic_vector(1 downto 0);
  signal fifo_segment_sl      : DTSegment_arr_t;
  signal slPhiMatcherPathOut  : SLPhiMatchDataPathOut_t;
  signal superLayerDatPathOut : SLDataPathOut_arr_t;
  signal ffPrimitiveOut       : DTPrimitiveFifoOut_t;
  
  -- Auxiliary signals
  signal primitive_sel   : DTPrimitive_t;
  signal primitive_aux   : DTPrimitive_t;
  signal primitiveFF_wr  : std_logic;
  signal valid_slphi     : std_logic_vector(1 downto 0);
  signal segmentFFOut_rd : std_logic_vector(1 downto 0);
  
  -- Aliases
  signal inOut_sel : std_logic_vector(1 downto 0);
  signal fifo_wr   : std_logic_vector(1 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  chamGlobDatPathOut.primitive            <= ffPrimitiveOut.primitive;
  chamGlobDatPathOut.primitiveFFOut_empty <= ffPrimitiveOut.empty;
  chamGlobDatPathOut.rawHitsFFPhi_full    <= 
          superLayerDatPathOut(1).rawHitsFFIn_full &
          superLayerDatPathOut(0).rawHitsFFIn_full;

  chamGlobDatPathOut.statistics_sl1 <= superLayerDatPathOut(1).statistics;
  chamGlobDatPathOut.statistics_sl0 <= superLayerDatPathOut(0).statistics;
  
  chamGlobDatPathOut.ffOutgoingPrimitiveStat    <= ffPrimitiveOut.stat;
  chamGlobDatPathOut.ffPostDupFiltPrimitiveStat <= 
          slPhiMatcherPathOut.ffPostDupPrimStat;
          
  ---------------------------------------------------------------
  -- Aliases
  --
  inOut_sel      <= chamGlobDatPathIn.control_regs.ouput_trg_chamber_selector;
  valid_slphi(0) <= not superLayerDatPathOut(0).segmentFFOut_empty;
  valid_slphi(1) <= not superLayerDatPathOut(1).segmentFFOut_empty;

  ---------------------------------------------------------------
  -- Muxers logic for outgoing-FIFO inputs
  --
--   primitive_aux <= primPathCalcOut.primitive;
  primitive_aux <= slPhiMatcherPathOut.primitive;
  
  primitive_sel.valid_segments(0) <= 
        primitive_aux.valid_segments(0) when inOut_sel = CHAMBER_OUTPUT else  
        valid_slphi(0)                  when inOut_sel = SL_PHI0_OUTPUT else
        '0'                             when inOut_sel = SL_PHI1_OUTPUT else
        primitive_aux.valid_segments(0) when inOut_sel = SL_THETA_OUTPUT;

  primitive_sel.valid_segments(1) <= 
        primitive_aux.valid_segments(1) when inOut_sel = CHAMBER_OUTPUT else  
        '0'                             when inOut_sel = SL_PHI0_OUTPUT else
        valid_slphi(1)                  when inOut_sel = SL_PHI1_OUTPUT else
        primitive_aux.valid_segments(1) when inOut_sel = SL_THETA_OUTPUT;

  primitive_sel.paired_segments(0) <= 
        primitive_aux.paired_segments(0) when inOut_sel = CHAMBER_OUTPUT else
        superLayerDatPathOut(0).segment  when inOut_sel = SL_PHI0_OUTPUT else
        DT_SEGMENT_NULL                  when inOut_sel = SL_PHI1_OUTPUT else
        primitive_aux.paired_segments(0) when inOut_sel = SL_THETA_OUTPUT;

  primitive_sel.paired_segments(1) <= 
        primitive_aux.paired_segments(1) when inOut_sel = CHAMBER_OUTPUT else
        DT_SEGMENT_NULL                  when inOut_sel = SL_PHI0_OUTPUT else
        superLayerDatPathOut(1).segment  when inOut_sel = SL_PHI1_OUTPUT else
        primitive_aux.paired_segments(1) when inOut_sel = SL_THETA_OUTPUT;

  primitive_sel.horizontal_position_chamber <= 
        primitive_aux.horizontal_position_chamber 
                    when inOut_sel = CHAMBER_OUTPUT  else
        primitive_aux.horizontal_position_chamber 
                    when inOut_sel = SL_THETA_OUTPUT else
        (others => '0');
      
  primitive_sel.phi_tangent_x4096_chamber <= 
        primitive_aux.phi_tangent_x4096_chamber 
                    when inOut_sel = CHAMBER_OUTPUT  else
        primitive_aux.phi_tangent_x4096_chamber 
                    when inOut_sel = SL_THETA_OUTPUT else
        (others => '0');
      
  primitive_sel.chi_square_chamber <=
        primitive_aux.chi_square_chamber 
                    when inOut_sel = CHAMBER_OUTPUT  else
        primitive_aux.chi_square_chamber 
                    when inOut_sel = SL_THETA_OUTPUT else
        (others => '0');

  primitive_sel.bx_time_chamber <=
        primitive_aux.bx_time_chamber 
                    when inOut_sel = CHAMBER_OUTPUT  else
        primitive_aux.bx_time_chamber 
                    when inOut_sel = SL_THETA_OUTPUT else
        (others => '0');
        
  primitive_sel.bx_id_chamber <=
        primitive_aux.bx_id_chamber 
                    when inOut_sel = CHAMBER_OUTPUT  else
        primitive_aux.bx_id_chamber 
                    when inOut_sel = SL_THETA_OUTPUT else
        (others => '0');

  ---------------------------------------------------------------
  -- Muxers logic for last stage FIFO controls
  --
  fifo_wr(0) <= (not ffPrimitiveOut.full) and valid_slphi(0);
  fifo_wr(1) <= (not ffPrimitiveOut.full) and valid_slphi(1);
          
  primitiveFF_wr <= 
    slPhiMatcherPathOut.primValidFFOut_wr when inOut_sel = CHAMBER_OUTPUT else  
    fifo_wr(0)                            when inOut_sel = SL_PHI0_OUTPUT else
    fifo_wr(1)                            when inOut_sel = SL_PHI1_OUTPUT else
    slPhiMatcherPathOut.primValidFFOut_wr when inOut_sel = SL_THETA_OUTPUT;

  segmentFFOut_rd(0) <= 
    slPhiMatcherPathOut.segmentFF_rd(0) when inOut_sel = CHAMBER_OUTPUT else
    fifo_wr(0)                          when inOut_sel = SL_PHI0_OUTPUT else
    '0'                                 when inOut_sel = SL_PHI1_OUTPUT else
    slPhiMatcherPathOut.segmentFF_rd(0) when inOut_sel = SL_THETA_OUTPUT;

  segmentFFOut_rd(1) <= 
    slPhiMatcherPathOut.segmentFF_rd(1) when inOut_sel = CHAMBER_OUTPUT else
    '0'                                 when inOut_sel = SL_PHI0_OUTPUT else
    fifo_wr(1)                          when inOut_sel = SL_PHI1_OUTPUT else
    slPhiMatcherPathOut.segmentFF_rd(1) when inOut_sel = SL_THETA_OUTPUT;
      
  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  SL_GENERATOR : for i in 0 to 1 generate
    SLAYER_PHI_UNIT : SUPERLAYER_DATAPATH port map (
      clk_fifo_rd          => clk_sl_matcher,
      clk_fifo_wr          => clk_fifo_wr,
      clk_mixer            => clk_mixer,
      clk_analyzer         => clk_analyzer,
      clk_dup_filter       => clk_dup_filter,
      clk_qenhan_filter    => clk_qenhan_filter,
      --
      rst_fifo_rd          => rst_fifo_rd,
      rst_fifo_wr          => rst_fifo_wr,
      rst_mixer            => rst_mixer,
      rst_analyzer         => rst_analyzer,
      rst_dup_filter       => rst_dup_filter,
      rst_qenhan_filter    => rst_qenhan_filter,
      --
      superLayerDatPathIn  => (
              rawHit              => chamGlobDatPathIn.rawHitPhi(i),
              rawHitFFIn_wr       => chamGlobDatPathIn.rawHitFFPhi_wr(i),
              segmentFFOut_rd     => segmentFFOut_rd(i),
              segcandFFIntrcptIn  => chamGlobDatPathIn.fifoInterceptIn(i),
              reset_overflow      => chamGlobDatPathIn.reset_overflow,
              bx_counter          => chamGlobDatPathIn.bx_counter,
              control_regs        => chamGlobDatPathIn.control_regs
      ),
      superLayerDatPathOut => superLayerDatPathOut(i)
    );

    -- Interconnection signals.
    fifo_segment_sl(i) <= superLayerDatPathOut(i).segment;
    fifo_empty_sl(i)   <= superLayerDatPathOut(i).segmentFFOut_empty;
    
    chamGlobDatPathOut.fifoInterceptOut(i) <=
                          superLayerDatPathOut(i).segcandFFIntrcptOut;

  end generate;
  
  INST_SLPHI_MATCH : SLPHI_MATCHER_DATAPATH port map(
    clk                 => clk_sl_matcher,
    rst                 => rst_sl_matcher,
    slPhiMatcherPathIn  => (
          segment_phi          => fifo_segment_sl,
          segmentPhiFFIn_empty => fifo_empty_sl,
          primitiveFFOut_full  => ffPrimitiveOut.full,
          bx_counter           => chamGlobDatPathIn.bx_counter,
          control_regs         => chamGlobDatPathIn.control_regs     
    ),
    slPhiMatcherPathOut => slPhiMatcherPathOut
  );
  
  PRIMITIVE_FFOUT_INST : DTPRIMITIVE_FIFO port map(
      clkRd   => clk_fifo_rd,
      clkWr   => clk_sl_matcher,
      rst     => rst_sl_matcher,
      fifoIn  => (
            primitive => primitive_sel,
            wrEna     => primitiveFF_wr,
            rdEna     => chamGlobDatPathIn.primitiveFFOut_rd
      ),
      fifoOut => ffPrimitiveOut
    );
  
end architecture;

