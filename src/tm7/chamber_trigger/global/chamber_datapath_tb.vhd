library IEEE;
USE IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;
use work.dt_common_pkg.all;
use work.dt_constants_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.globalpath_pkg.all;
use work.float_point_pkg.all;

entity CHAMBER_DATAPATH_TB is
end entity;

architecture Behavior of CHAMBER_DATAPATH_TB is 

  constant LHC_CLK_PERIOD : time := 24.93 ns; -- gives a rational clk_period 
  constant CLK_PERIOD     : time := LHC_CLK_PERIOD / 4.5;
  
  --signal clk40 : std_logic := '1';
  signal clk : std_logic := '1';
  signal rst : std_logic := '1';
  signal bx_counter : unsigned(11 downto 0) := (others => '0');
  
  ------------------------------------------------
  -- UUT ports
  --
  signal  chamGlobDatPathIn  : ChGlobalDataPathIn_t;
  signal  chamGlobDatPathOut : ChGlobalDataPathOut_t;

begin

  -- Instantiate the Unit Under Test (UUT)
  UUT : CHAMBER_DATAPATH
  port map (
    clk_fifo_rd        => clk,
    clk_fifo_wr        => clk,
    clk_mixer          => clk,
    clk_analyzer       => clk,
    clk_dup_filter     => clk,
    clk_qenhan_filter  => clk,
    clk_sl_matcher     => clk,
    rst_fifo_rd        => rst,
    rst_fifo_wr        => rst,
    rst_mixer          => rst,
    rst_analyzer       => rst,
    rst_dup_filter     => rst,
    rst_qenhan_filter  => rst,
    rst_sl_matcher     => rst,
    chamGlobDatPathIn  => chamGlobDatPathIn,
    chamGlobDatPathOut => chamGlobDatPathOut 
  );


  --clk40  <= not clk40 after ( LHC_CLK_PERIOD / 2.0 );
  clk  <= not clk after ( CLK_PERIOD / 2.0 );

  bx_counter <= (bx_counter + 1) mod 3564 after ( LHC_CLK_PERIOD );
  
  --  primitiveFFOut_rd   : std_logic;
  chamGlobDatPathIn.primitiveFFOut_rd <= not chamGlobDatPathOut.primitiveFFOut_empty;

  --  fifoInterceptIn     : SegCandFifoInterceptIn_arr_t;
  chamGlobDatPathIn.fifoInterceptIn(0).segment_candidate <= chamGlobDatPathOut.fifoInterceptOut(0).segment_candidate;
  chamGlobDatPathIn.fifoInterceptIn(0).wrEna <= chamGlobDatPathOut.fifoInterceptOut(0).wrEna;

  chamGlobDatPathIn.fifoInterceptIn(1).segment_candidate <= chamGlobDatPathOut.fifoInterceptOut(1).segment_candidate;
  chamGlobDatPathIn.fifoInterceptIn(1).wrEna <= chamGlobDatPathOut.fifoInterceptOut(1).wrEna;

  --  reset_overflow      : std_logic;
  chamGlobDatPathIn.reset_overflow <= '0';
  
  --  bx_counter          : std_logic_vector(11 downto 0);
  chamGlobDatPathIn.bx_counter <= std_logic_vector(bx_counter) ;
  
  --  control_regs        : DTControlRegs_t;
  chamGlobDatPathIn.control_regs <= (
    candidate_splitter_enable       => '0'                                    , -- std_logic;
    bx_time_convert_ena             => '0'                                    , -- std_logic;
    bx_time_tolerance               => std_logic_vector(to_unsigned(0   ,8))  , -- std_logic_vector(7 downto 0);
    chisqr_threshold                => std_logic_vector(to_unsigned(64  ,16)) , -- std_logic_vector(15 downto 0);
    qfilt_wait_expire_num_bx_stg1   => std_logic_vector(to_unsigned(2   ,6))  , -- std_logic_vector(5 downto 0);
    qfilt_wait_expire_num_bx_stg2   => std_logic_vector(to_unsigned(5   ,6))  , -- std_logic_vector(5 downto 0);
    tanphi_x4096_threshold          => std_logic_vector(to_unsigned(4096,14)) , -- std_logic_vector(13 downto 0);
    slphi_match_bypass_tanphi_match => '0'                                    , -- std_logic;
    slphi_match_tanphi_hardfilter   => '0'                                    , -- std_logic;
    slphi_match_tanphi_x4096_thr    => std_logic_vector(to_unsigned(9000,14)) , -- std_logic_vector(13 downto 0);
    slphi_match_bx_wait_read_offset => std_logic_vector(to_unsigned(10  ,9))  , -- std_logic_vector(8 downto 0);
    slphi_match_mem_bx_wait         => std_logic_vector(to_unsigned(3   ,6))  , -- std_logic_vector(5 downto 0);
    prim_chisqr_hardfilter          => '0'                                    , -- std_logic;
    prim_chisqr_threshold           => std_logic_vector(to_unsigned(640 ,16)) , -- std_logic_vector(15 downto 0);
    primqfilt_wait_expire_num_bx    => std_logic_vector(to_unsigned(2   ,6))  , -- std_logic_vector(5 downto 0);
    ouput_trg_chamber_selector      => "00"                                   , -- std_logic_vector(1 downto 0);
    sl_phi3_x_offset                => to_signed(0,10)                        , -- signed(9 downto 0);
    radio_mult_sin_delta_cmsphi     => UFLOAT32_ZERO                          , -- UFloat32_t;
    radio_mult_cos_delta_cmsphi     => UFLOAT32_ZERO                            -- UFloat32_t;
  );

  process
  begin
    rst <= '1';
    
    chamGlobDatPathIn.rawHitFFPhi_wr      <= "00";
    chamGlobDatPathIn.rawHitPhi           <= (others => DT_RAWHIT_NULL);

    wait for ( clk_period * 15 );

    rst <= '0';

    wait for ( clk_period * 30 );

    chamGlobDatPathIn.rawHitFFPhi_wr      <= "11";
    chamGlobDatPathIn.rawHitPhi           <= (
      0 =>(layer_id=>"00",tdc_time=> std_logic_vector(to_unsigned( 10000,17)),channel_id=> "0000000", super_layer_id  => "00") ,
      1 =>(layer_id=>"00",tdc_time=> std_logic_vector(to_unsigned( 10000,17)),channel_id=> "0000000", super_layer_id  => "00") );

    wait for (clk_period);

    chamGlobDatPathIn.rawHitFFPhi_wr      <= "11";
    chamGlobDatPathIn.rawHitPhi           <= (
      0 =>(layer_id=>"01",tdc_time=> std_logic_vector(to_unsigned( 10000,17)),channel_id=> "0000000", super_layer_id  => "00") ,
      1 =>(layer_id=>"01",tdc_time=> std_logic_vector(to_unsigned( 10000,17)),channel_id=> "0000000", super_layer_id  => "00") );

    wait for (clk_period);

    chamGlobDatPathIn.rawHitFFPhi_wr      <= "11";
    chamGlobDatPathIn.rawHitPhi           <= (
      0 =>(layer_id=>"10",tdc_time=> std_logic_vector(to_unsigned( 10000,17)),channel_id=> "0000000", super_layer_id  => "00") ,
      1 =>(layer_id=>"10",tdc_time=> std_logic_vector(to_unsigned( 10000,17)),channel_id=> "0000000", super_layer_id  => "00") );

    wait for (clk_period);

    chamGlobDatPathIn.rawHitFFPhi_wr      <= "11";
    chamGlobDatPathIn.rawHitPhi           <= (
      0 =>(layer_id=>"11",tdc_time=> std_logic_vector(to_unsigned( 10000,17)),channel_id=> "0000000", super_layer_id  => "00") ,
      1 =>(layer_id=>"11",tdc_time=> std_logic_vector(to_unsigned( 10000,17)),channel_id=> "0000000", super_layer_id  => "00") );

    wait for (clk_period);

    chamGlobDatPathIn.rawHitFFPhi_wr      <= "00";
    chamGlobDatPathIn.rawHitPhi           <= (others => DT_RAWHIT_NULL);

    wait;
    
  end process;

end architecture;
