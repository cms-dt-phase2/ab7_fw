-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Buffers
---- File       : dtprimitive_fifo.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-05
-------------------------------------------------------------------------------
---- Description: FIFO wrapper to store DT Primitives, using a common interface.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
--
use work.commonfunc_pkg.all;
use work.dt_common_pkg.all;
use work.dt_buffers_pkg.all;
use work.xil_memory_pkg.all;

entity DTPRIMITIVE_FIFO is
  port (
    clkRd   : in  std_logic;
    clkWr   : in  std_logic;
    rst     : in  std_logic;
    fifoIn  : in  DTPrimitiveFifoIn_t;
    fifoOut : out DTPrimitiveFifoOut_t
  );
end entity;

architecture Behavioural of DTPRIMITIVE_FIFO is
  signal dOut : std_logic_vector(DTPRIMITIVE_SIZE - 1 downto 0);
  signal dIn  : std_logic_vector(DTPRIMITIVE_SIZE - 1 downto 0);

  signal empty : std_logic;
  signal full  : std_logic;
  
  signal statCountIn  : std_logic_vector(STAT_COUNT_LEN - 1 downto 0);
  signal statCountOut : std_logic_vector(STAT_COUNT_LEN - 1 downto 0);
begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  fifoOut.full      <= full;
  fifoOut.empty     <= empty;
  fifoOut.primitive <= dtPrimitive_from_slv(dOut);
  --
  fifoOut.stat.count_in  <= statCountIn;
  fifoOut.stat.count_out <= statCountOut;
  
  ---------------------------------------------------------------
  -- Auxiliary signals
  --
  dIn <= slv_from_dtPrimitive(fifoIn.primitive);
  
  ---------------------------------------------------------------
  -- Counters for statistics
  --
  RD_STAT : process(clkRd, rst, fifoIn)
  begin
    if clkRd'event and clkRd = '1' then
      if rst = '1' then 
        statCountOut <= (others => '0');
      elsif fifoIn.rdEna = '1' then
        statCountOut <= statCountOut  + '1';
      end if;
    end if;
  end process;

  WR_STAT : process(clkWr, rst, fifoIn)
  begin
    if clkWr'event and clkWr = '1' then
      if rst = '1' then 
        statCountIn <= (others => '0');
      elsif fifoIn.wrEna = '1' then
        statCountIn <= statCountIn  + '1';
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  INTERNAL_FIFO : XIL_FIFO_GENERIC_WRAPPER 
  generic map (DATAWIDTH => DTPRIMITIVE_SIZE, DEPTH => 512)
  port map (
    rst          => rst,
    rdClk        => clkRd,
    wrClk        => clkWr,
    rdEna        => fifoIn.rdEna,
    wrEna        => fifoIn.wrEna,
    data_in      => dIn,
    data_out     => dOut,
    rd_count     => open,
    wr_count     => open,
    almost_empty => open,
    almost_full  => open,
    empty        => empty,
    full         => full
  );
  
end architecture;
