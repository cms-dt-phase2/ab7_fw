-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Quality-based segments filter
---- File       : quality_enhancer_filter.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-03
-------------------------------------------------------------------------------
---- Description: Rejects SEGMENTS based on comparing their quality level,
----              their TDC values and lateralities, and also their
----              chi-square value.
----     
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;
use work.dt_common_pkg.all;
use work.dt_constants_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.filters_pkg.all;
use work.xil_memory_pkg.all;

entity QUALITY_ENHANCER_FILTER is
  port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    expire_bx_num   : in  std_logic_vector(5 downto 0);
    qEnhanFilterIn  : in  SegmentFilterIn_t;
    qEnhanFilterOut : out SegmentFilterOut_t
  );
end entity;

architecture TwoProcess of QUALITY_ENHANCER_FILTER is

  constant MAX_BX_TIMEOUT : std_logic_vector(2 downto 0) := (others => '1');
  constant ADDRESS_ZERO   : std_logic_vector(4 downto 0) := (others => '0');

  type states_t is (STDBY, WRITE_STORED_SEGMENTS, WRITE_CURRENT_SEGMENT, 
                    RD_FIFO_WS);

  -- Internal registers
  type reg_type is record
    state           : states_t;
    clear_filter    : std_logic;
    filter_is_empty : std_logic;
    ffIn_rd         : std_logic;
    bx_counter      : std_logic_vector(11 downto 0);
    mem_add_wr      : std_logic_vector(4 downto 0);
    mem_add_rd      : std_logic_vector(4 downto 0);
    current_segment : DTSegment_t;
  end record;

  constant regDefault : reg_type := (
    state           => STDBY,
    clear_filter    => '0',
    -- Filter is empty after a reset, so no "current segment" is available.
    filter_is_empty => '1',
    ffIn_rd         => '0',
    --
    bx_counter      => (others => '0'),
    mem_add_wr      => (others => '0'),
    mem_add_rd      => (others => '0'),
    current_segment => DT_SEGMENT_NULL
  );                
  signal reg, nextReg : reg_type := regDefault;

  -- Components inter-connection signals
  signal mem_wr       : std_logic;
  signal mem_data_out : std_logic_vector(DTSEGMENT_SIZE - 1 downto 0);

  -- Auxiliary signals
  signal ffOut_wr           : std_logic;
  signal sel_current_to_out : std_logic;
  signal similar_segments   : std_logic;
  signal mux_segment_out    : DTSegment_t;

  -- Aliases
  signal current_bx      : std_logic_vector(11 downto 0);
  signal bx_expire_value : std_logic_vector(11 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  qEnhanFilterOut.segment    <= mux_segment_out;
  qEnhanFilterOut.fifoIn_rd  <= reg.ffIn_rd;
  qEnhanFilterOut.fifoOut_wr <= ffOut_wr;

  ---------------------------------------------------------------
  -- Auxiliary signals
  --
  mux_segment_out <= reg.current_segment when sel_current_to_out = '1' else
                     dtSegment_from_slv(mem_data_out);
  --
  -- This logic compares the last (and best) value detected from a series
  -- with the last segment read from the FIFO, and gives a single bit
  -- ('similar_segments') that indicates if their are consider coincident 
  -- or not.
  --
  -- In the mean time, the criteria is that they share, at least, one or 
  -- more TDC values in the same wire/layer combination.
  --
  similar_segments <= segments_are_similar(qEnhanFilterIn.segment, 
                                           reg.current_segment);

  -- Aliases
  bx_expire_value <= reg.bx_counter + expire_bx_num;
  current_bx      <= qEnhanFilterIn.bx_counter;
                     
  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, qEnhanFilterIn, bx_expire_value, current_bx, 
                 similar_segments)
    variable currentseg_high_q : std_logic;
    variable currentseg_low_q  : std_logic;
    variable inputseg_high_q   : std_logic;
    variable inputseg_low_q    : std_logic;
  begin
    -- Default values
    nextReg            <= reg;
    mem_wr             <= '0';
    ffOut_wr           <= '0';
    sel_current_to_out <= '0';

    ------------------------------------------

    currentseg_high_q := segment_is_high_q(reg.current_segment);
    currentseg_low_q  := segment_is_low_q(reg.current_segment);
    --
    inputseg_high_q   := segment_is_high_q(qEnhanFilterIn.segment);
    inputseg_low_q    := segment_is_low_q(qEnhanFilterIn.segment);
    
    if reg.filter_is_empty = '0' and ( 
      (bx_expire_value <= MAX_BX_IDX and 
          current_bx >= bx_expire_value) 
                                               or
         (bx_expire_value > MAX_BX_IDX and 
          current_bx >= (bx_expire_value - MAX_BX_IDX + 1))
       )
    then
      nextReg.clear_filter <= '1';
    end if; 
    
    -- FSM
    case reg.state is 
      when STDBY =>
        -- Always to "0" until transfering stored data.
        nextReg.mem_add_rd <= (others => '0');

        if qEnhanFilterIn.fifoIn_empty = '0' then
          nextReg.bx_counter <= current_bx;

          if reg.filter_is_empty = '1' then
            nextReg.ffIn_rd         <= '1';          
            nextReg.mem_add_wr      <= (others => '0');
            nextReg.filter_is_empty <= '0';
            nextReg.current_segment <= qEnhanFilterIn.segment;
            nextReg.state           <= RD_FIFO_WS;

          else
            nextReg.clear_filter <= '0';

            if similar_segments = '0' then
              if currentseg_high_q = '1' or reg.mem_add_wr = ADDRESS_ZERO then
                nextReg.state <= WRITE_CURRENT_SEGMENT;
              else
                nextReg.mem_add_wr <= reg.mem_add_wr - '1';
                nextReg.state      <= WRITE_STORED_SEGMENTS;
              end if;

            elsif currentseg_high_q = '1' then
              nextReg.ffIn_rd <= '1';          -- Get next segment
              nextReg.state   <= RD_FIFO_WS;

              if (qEnhanFilterIn.segment.chi_square < 
                  reg.current_segment.chi_square) and inputseg_high_q = '1' 
              then
                -- Replace current segment
                nextReg.current_segment <= qEnhanFilterIn.segment;
              end if;

            -- Store input segment
            else
              nextReg.ffIn_rd         <= '1'; -- Always fetch a new one segment         
              mem_wr                  <= '1';
              nextReg.mem_add_wr      <= reg.mem_add_wr + '1';
              nextReg.current_segment <= qEnhanFilterIn.segment;
              nextReg.state           <= RD_FIFO_WS;
            end if;
          end if;
        --
        -- This condition forces to send the last "current segment" after a
        -- prefixed BX number and the last data also has been sent to 
        -- outgoing FIFO.
        -- This way I avoid keeping a single isolated segment forever 
        -- inside the filter registers.
        -- After a couple of BX's this segment won't be correlated with the
        -- next one incoming.
        --
        elsif reg.clear_filter = '1' then
          nextReg.filter_is_empty <= '1';
          nextReg.bx_counter      <= current_bx;

          if currentseg_low_q = '1' and reg.mem_add_wr /= ADDRESS_ZERO then
            nextReg.mem_add_wr <= reg.mem_add_wr - '1';
            nextReg.state      <= WRITE_STORED_SEGMENTS;
          else
            nextReg.state      <= WRITE_CURRENT_SEGMENT;
          end if;
        end if;

      when WRITE_STORED_SEGMENTS =>
        if qEnhanFilterIn.fifoOut_full = '0' then
          ffOut_wr <= '1';

          if reg.mem_add_rd < reg.mem_add_wr then
            nextReg.mem_add_rd <= reg.mem_add_rd + '1';
          else
            nextReg.state <= WRITE_CURRENT_SEGMENT;
          end if;
        end if;

      when WRITE_CURRENT_SEGMENT =>
        if qEnhanFilterIn.fifoOut_full = '0' then
          ffOut_wr           <= '1';
          sel_current_to_out <= '1';
          --
          -- If we arrive here because "clear filter" mechanism has been 
          -- triggered, it's only possible if input FIFO was empty. So,
          -- in this case, we **DO NOT** try to read the FIFO. It's necessary
          -- to wait until it signals, again, non-empty condition.
          --
          if reg.clear_filter = '0' then
            nextReg.ffIn_rd <= '1';   -- Read "next" fifo data.
          end if;

          nextReg.mem_add_wr      <= (others => '0');
          nextReg.bx_counter      <= current_bx;
          nextReg.clear_filter    <= '0';    -- Always reset "clear_filter".
          nextReg.current_segment <= qEnhanFilterIn.segment;
          nextReg.state           <= RD_FIFO_WS;
        end if;

      when RD_FIFO_WS =>
        nextReg.ffIn_rd <= '0';
        nextReg.state   <= STDBY;
      
    end case;

  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1'
      then reg <= regDefault;
      else reg <= nextReg;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  FILTER_MEM : XIL_DISTRAM_DUALPORT_D32_WXBITS
  generic map (NUM_BITS => DTSEGMENT_SIZE)
  port map (
    clk      => clk,
    wr_ena   => mem_wr,
    add_wr   => reg.mem_add_wr,
    add_rd   => reg.mem_add_rd,
    data_in  => slv_from_dtSegment(reg.current_segment),
    data_out => mem_data_out
  );
  
end architecture;
