--=================================================================================================--
-- Module for TM7 communication with the GBTX chip             
-- Adapted from https://gitlab.cern.ch/gbtsc-fpga-support/gbtsc-example-design
-- By Javier Sastre Alvaro (javier.sastre@ciemat.es)
-- Adapted to 12 GBT links (3 banks x 4 links) by P. De Remigis, A. Navarro, F.Rotondo
--=================================================================================================--
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package tm7_gbt_decl is
  type mgt_ch_t is record
    mgt : positive range 119 downto 117;
    ch  : natural range 1 to 4;
  end record;
  type mgt_ch_v_t is array(1 to 12) of mgt_ch_t;
end package;

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

-- Custom libraries and packages:
use work.bo2.all;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.tm7_gbt_decl.all;
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_exampledesign_package.all;

entity tm7_gbt is
  generic (
    REMAP_RX : mgt_ch_v_t := (
      1 =>  (mgt => 117, ch => 1 ),
      2 =>  (mgt => 117, ch => 2 ),
      3 =>  (mgt => 117, ch => 3 ),
      4 =>  (mgt => 117, ch => 4 ),
      5 =>  (mgt => 118, ch => 1 ),
      6 =>  (mgt => 118, ch => 2 ),
      7 =>  (mgt => 118, ch => 3 ),
      8 =>  (mgt => 118, ch => 4 ),
      9 =>  (mgt => 119, ch => 1 ),
      10 => (mgt => 119, ch => 2 ),
      11 => (mgt => 119, ch => 3 ),
      12 => (mgt => 119, ch => 4 )
    );
    REMAP_TX : mgt_ch_v_t := (
      1 =>  (mgt => 117, ch => 1 ),
      2 =>  (mgt => 117, ch => 2 ),
      3 =>  (mgt => 117, ch => 3 ),
      4 =>  (mgt => 117, ch => 4 ),
      5 =>  (mgt => 118, ch => 1 ),
      6 =>  (mgt => 118, ch => 2 ),
      7 =>  (mgt => 118, ch => 3 ),
      8 =>  (mgt => 118, ch => 4 ),
      9 =>  (mgt => 119, ch => 1 ),
      10 => (mgt => 119, ch => 2 ),
      11 => (mgt => 119, ch => 3 ),
      12 => (mgt => 119, ch => 4 )
    );
    GENERATE_ILAS : std_logic_vector(1 to 12) := x"FFF";
    GENERATE_SC   : std_logic_vector(1 to 12) := x"FFF"
  );
  port (   
    clk40                          : in std_logic; -- Frame clock
    -- vc707_gbt_example_design uses 156 MHz for DRP/SYSCLK, we replace it by 100 MHz
    clk_fr                         : in std_logic;
    --------------
    -- MGT(GTX) --
    --------------
    mgt117_clkn, mgt117_clkp    : in  std_logic;
    mgt117_rxn,  mgt117_rxp     : in  std_logic_vector(0 to 3);
    mgt117_txn,  mgt117_txp     : out std_logic_vector(0 to 3);
    mgt118_clkn, mgt118_clkp    : in  std_logic;
    mgt118_rxn,  mgt118_rxp     : in  std_logic_vector(0 to 3);
    mgt118_txn,  mgt118_txp     : out std_logic_vector(0 to 3);
    mgt119_clkn, mgt119_clkp    : in  std_logic;
    mgt119_rxn,  mgt119_rxp     : in  std_logic_vector(0 to 3);
    mgt119_txn,  mgt119_txp     : out std_logic_vector(0 to 3);

    --------------
    -- ipbus --
    --------------
    ipb_clk           : in std_logic;
    ipb_rst           : in std_logic;
    ipb_in            : in ipb_wbus;
    ipb_out           : out ipb_rbus;
    
    ----------------
    -- data ports --
    ----------------
    resetgbtfpga            : in std_logic;
    txFrameClk_from_txPll_o : out std_logic;
    rxWordClk               : out std_logic_vector(1 to 12);
    rxData                  : out gbt_reg84_A(1 to 12);
    rxIsData                : out std_logic_vector(1 to 12);
    txData                  : in gbt_reg84_A(1 to 12);
    txIsData                : in std_logic_vector(1 to 12);
    header_lock             : out std_logic_vector(1 to 12);
    txFrameClk_lock         : out std_logic
  );
end entity;

architecture structural of tm7_gbt is

  --================================ Signal Declarations ================================--          
  --===============--     
  -- General reset --     
  --===============--     
  signal reset_from_genRst                          : std_logic;    
  
  --===============--
  -- Clocks scheme -- 
  --===============--   
  -- Frame clock:
  signal txFrameClk_from_txPll                    : std_logic;
  signal txFrameClk_lock_from_txPll               : std_logic;
   
  --=========================--
  -- GBT Bank example design --
  --=========================--
  
  -- Control:
  -----------
  signal txPllReset                                 : std_logic;
  
  -- GBT Ports
  
  signal clkp, clkn : std_logic_vector(119 downto 117);
  type slv4asc_v_t is array(119 downto 117) of std_logic_vector(1 to 4);
  signal txp,  txn  : slv4asc_v_t;
  signal rxp,  rxn  : slv4asc_v_t;
  
  -- Data:
  --------
  type gbt_reg84_A_v_t is array(119 downto 117) of gbt_reg84_A(1 to 4);
  signal txData_from_gbtExmplDsgn                   : gbt_reg84_A_v_t;
  signal txDataO_1_12_side                          : gbt_reg84_A(1 to 12);
  signal rxData_from_gbtExmplDsgn                   : gbt_reg84_A_v_t;
  signal rxData_1_12_side                           : gbt_reg84_A(1 to 12);
  signal txData_to_gbtExmplDsgn                     : gbt_reg84_A_v_t;
  signal txDataI_1_12_side                          : gbt_reg84_A(1 to 12);
  signal txIsData_to_gbtExmplDsgn                   : slv4asc_v_t;
  signal rxIsData_from_gbtExmplDsgn                 : slv4asc_v_t;
  type slv4_v_t is array(natural range <>) of std_logic_vector(3 downto 0);
  signal txData_slow_control                        : slv4_v_t(1 to 12);
  
  signal link_ready_from_gbtExmplDsgn               : slv4asc_v_t;

  signal rxReady_from_gbtExmplDsgn                  : slv4asc_v_t;
  signal rxReadyLostFlag_from_gbtExmplDsgn          : slv4asc_v_t;
  signal rxDataErrorSeenFlag_from_gbtExmplDsgn      : slv4asc_v_t;
  signal rxMatchFlag_from_gbtExmplDsgn              : slv4asc_v_t;


  --------------------------------------------------      
  
  signal headerLocked_from_gbtExmplDsgn : slv4asc_v_t;
  signal header_lock_s : std_logic_vector(1 to 12);
  
  --=====================--
  -- Latency measurement --
  --=====================--
  signal shiftTxClock_from_vio              : std_logic;
  signal txShiftCount_from_vio              : std_logic_vector(7 downto 0);
  signal txAligned_from_gbtbank             : slv4asc_v_t;
  signal txAlignComputed_from_gbtbank       : slv4asc_v_t;
  signal txAlignComputed_1_12_side          : std_logic_vector(1 to 12);
  signal txAligned_1_12_side                : std_logic_vector(1 to 12);
  signal txFrameClk_from_gbtExmplDsgn       : slv4asc_v_t;
  signal txFrameClk_1_12_side               : std_logic_vector(1 to 12);
  signal txWordClk_from_gbtExmplDsgn        : slv4asc_v_t;
  signal rxFrameClk_from_gbtExmplDsgn       : slv4asc_v_t;
  signal rxFrameClk_1_12_side               : std_logic_vector(1 to 12);
  signal rxWordClk_from_gbtExmplDsgn        : slv4asc_v_t;
  --------------------------------------------------                                    
  
  -- ILA component  --
  --================--
  -- Vivado synthesis tool does not support mixed-language
  -- Solution: http://www.xilinx.com/support/answers/47454.html
  COMPONENT xlx_k7v7_vivado_debug PORT(
    CLK: in std_logic;
    PROBE0: in std_logic_vector(1 downto 0);
    PROBE1: in std_logic_vector(1 downto 0);
    PROBE2: in std_logic_vector(79 downto 0);
    PROBE3: in std_logic_vector(1 downto 0)
  );
  END COMPONENT;
 
  -- IPBUS REGISTER
  constant N_STAT : integer := 128;
  constant N_CTRL : integer := 128;
  signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);
  signal stat, csrd: ipb_reg_v(N_STAT-1 downto 0);
  signal conf, csrq: ipb_reg_v(N_CTRL-1 downto 0);
  -----------------------------
  -- Aggregate signals that basically vectorize the per-link confs, stats, and strobes
  -----------------------------
  type slv8_v_t  is array(natural range <>) of std_logic_vector( 7 downto 0);
  type slv12_v_t is array(natural range <>) of std_logic_vector(11 downto 0);
  type slv16_v_t is array(natural range <>) of std_logic_vector(15 downto 0);
  type slv32_v_t is array(natural range <>) of std_logic_vector(31 downto 0);
  type usgn20_v_t is array(natural range <>) of unsigned(19 downto 0);
  type usgn12_v_t is array(natural range <>) of unsigned(11 downto 0);
  -- Conf for SC
  signal sc_enable                : std_logic_vector  (1 to 12);
  signal tx_trid                  : slv8_v_t          (1 to 12);
  signal tx_ch                    : slv8_v_t          (1 to 12);
  signal tx_comm                  : slv8_v_t          (1 to 12);
  signal ec_data                  : slv32_v_t         (1 to 12);
  signal send_reset               : std_logic_vector  (1 to 12);
  signal send_connect             : std_logic_vector  (1 to 12);
  signal reset_sc                 : std_logic_vector  (1 to 12);
  signal gbtx_addr                : slv8_v_t          (1 to 12);
  signal ic_rd_wr                 : std_logic_vector  (1 to 12);
  signal read_fifo                : std_logic_vector  (1 to 12);
  signal nb_to_read               : slv16_v_t         (1 to 12);
  signal reg_addr                 : slv16_v_t         (1 to 12);
  signal ic_data                  : slv8_v_t          (1 to 12);
  -- Stats
  signal rx_trid                  : slv8_v_t          (1 to 12);
  signal rx_ch                    : slv8_v_t          (1 to 12);
  signal rx_len                   : slv8_v_t          (1 to 12);
  signal rx_error                 : slv8_v_t          (1 to 12);
  signal rx_ec_data               : slv32_v_t         (1 to 12);
  signal rx_reply_flag            : std_logic_vector  (1 to 12);
  signal tx_ready                 : std_logic_vector  (1 to 12);
  signal rx_empty                 : std_logic_vector  (1 to 12);
  signal rx_gbtx_addr             : slv8_v_t          (1 to 12);
  signal rx_reg                   : slv16_v_t         (1 to 12);
  signal rx_nb                    : slv16_v_t         (1 to 12);
  signal rx_ic_data               : slv8_v_t          (1 to 12);
  signal link_ready               : std_logic_vector  (1 to 12);
  signal tx_aligned               : std_logic_vector  (1 to 12);
  signal rx_gbt_ready             : std_logic_vector  (1 to 12);
  signal rx_gbt_ready_lost_flag   : std_logic_vector  (1 to 12);
  signal rx_data_error_seen       : std_logic_vector  (1 to 12);
  signal rx_match_flag            : std_logic_vector  (1 to 12);
  signal rx_is_data               : std_logic_vector  (1 to 12);
  signal rx_frame_ready           : std_logic_vector  (1 to 12);
  signal no_rxclkout_time         : usgn20_v_t        (1 to 12);
  signal no_rxclkout_ctr          : usgn12_v_t        (1 to 12);
  signal rxWordClk_s              : std_logic_vector  (1 to 12);
  
  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals declaration
  signal conf_dummy, conf_link10_ic_rd_wr, conf_link10_read_fifo, conf_link10_reset_sc, conf_link10_sc_enable,
    conf_link10_send_connect, conf_link10_send_reset, conf_link11_ic_rd_wr, conf_link11_read_fifo,
    conf_link11_reset_sc, conf_link11_sc_enable, conf_link11_send_connect, conf_link11_send_reset,
    conf_link12_ic_rd_wr, conf_link12_read_fifo, conf_link12_reset_sc, conf_link12_sc_enable, conf_link12_send_connect,
    conf_link12_send_reset, conf_link1_ic_rd_wr, conf_link1_read_fifo, conf_link1_reset_sc, conf_link1_sc_enable,
    conf_link1_send_connect, conf_link1_send_reset, conf_link2_ic_rd_wr, conf_link2_read_fifo, conf_link2_reset_sc,
    conf_link2_sc_enable, conf_link2_send_connect, conf_link2_send_reset, conf_link3_ic_rd_wr, conf_link3_read_fifo,
    conf_link3_reset_sc, conf_link3_sc_enable, conf_link3_send_connect, conf_link3_send_reset, conf_link4_ic_rd_wr,
    conf_link4_read_fifo, conf_link4_reset_sc, conf_link4_sc_enable, conf_link4_send_connect, conf_link4_send_reset,
    conf_link5_ic_rd_wr, conf_link5_read_fifo, conf_link5_reset_sc, conf_link5_sc_enable, conf_link5_send_connect,
    conf_link5_send_reset, conf_link6_ic_rd_wr, conf_link6_read_fifo, conf_link6_reset_sc, conf_link6_sc_enable,
    conf_link6_send_connect, conf_link6_send_reset, conf_link7_ic_rd_wr, conf_link7_read_fifo, conf_link7_reset_sc,
    conf_link7_sc_enable, conf_link7_send_connect, conf_link7_send_reset, conf_link8_ic_rd_wr, conf_link8_read_fifo,
    conf_link8_reset_sc, conf_link8_sc_enable, conf_link8_send_connect, conf_link8_send_reset, conf_link9_ic_rd_wr,
    conf_link9_read_fifo, conf_link9_reset_sc, conf_link9_sc_enable, conf_link9_send_connect, conf_link9_send_reset,
    conf_manualResetRx, conf_manualResetTx, stat_dummy, stat_link10_link_ready, stat_link10_rx_data_error_seen,
    stat_link10_rx_empty, stat_link10_rx_frame_ready, stat_link10_rx_gbt_ready, stat_link10_rx_gbt_ready_lost_flag,
    stat_link10_rx_is_data, stat_link10_rx_match_flag, stat_link10_rx_reply_flag, stat_link10_tx_aligned,
    stat_link10_tx_ready, stat_link11_link_ready, stat_link11_rx_data_error_seen, stat_link11_rx_empty,
    stat_link11_rx_frame_ready, stat_link11_rx_gbt_ready, stat_link11_rx_gbt_ready_lost_flag, stat_link11_rx_is_data,
    stat_link11_rx_match_flag, stat_link11_rx_reply_flag, stat_link11_tx_aligned, stat_link11_tx_ready,
    stat_link12_link_ready, stat_link12_rx_data_error_seen, stat_link12_rx_empty, stat_link12_rx_frame_ready,
    stat_link12_rx_gbt_ready, stat_link12_rx_gbt_ready_lost_flag, stat_link12_rx_is_data, stat_link12_rx_match_flag,
    stat_link12_rx_reply_flag, stat_link12_tx_aligned, stat_link12_tx_ready, stat_link1_link_ready,
    stat_link1_rx_data_error_seen, stat_link1_rx_empty, stat_link1_rx_frame_ready, stat_link1_rx_gbt_ready,
    stat_link1_rx_gbt_ready_lost_flag, stat_link1_rx_is_data, stat_link1_rx_match_flag, stat_link1_rx_reply_flag,
    stat_link1_tx_aligned, stat_link1_tx_ready, stat_link2_link_ready, stat_link2_rx_data_error_seen,
    stat_link2_rx_empty, stat_link2_rx_frame_ready, stat_link2_rx_gbt_ready, stat_link2_rx_gbt_ready_lost_flag,
    stat_link2_rx_is_data, stat_link2_rx_match_flag, stat_link2_rx_reply_flag, stat_link2_tx_aligned,
    stat_link2_tx_ready, stat_link3_link_ready, stat_link3_rx_data_error_seen, stat_link3_rx_empty,
    stat_link3_rx_frame_ready, stat_link3_rx_gbt_ready, stat_link3_rx_gbt_ready_lost_flag, stat_link3_rx_is_data,
    stat_link3_rx_match_flag, stat_link3_rx_reply_flag, stat_link3_tx_aligned, stat_link3_tx_ready,
    stat_link4_link_ready, stat_link4_rx_data_error_seen, stat_link4_rx_empty, stat_link4_rx_frame_ready,
    stat_link4_rx_gbt_ready, stat_link4_rx_gbt_ready_lost_flag, stat_link4_rx_is_data, stat_link4_rx_match_flag,
    stat_link4_rx_reply_flag, stat_link4_tx_aligned, stat_link4_tx_ready, stat_link5_link_ready,
    stat_link5_rx_data_error_seen, stat_link5_rx_empty, stat_link5_rx_frame_ready, stat_link5_rx_gbt_ready,
    stat_link5_rx_gbt_ready_lost_flag, stat_link5_rx_is_data, stat_link5_rx_match_flag, stat_link5_rx_reply_flag,
    stat_link5_tx_aligned, stat_link5_tx_ready, stat_link6_link_ready, stat_link6_rx_data_error_seen,
    stat_link6_rx_empty, stat_link6_rx_frame_ready, stat_link6_rx_gbt_ready, stat_link6_rx_gbt_ready_lost_flag,
    stat_link6_rx_is_data, stat_link6_rx_match_flag, stat_link6_rx_reply_flag, stat_link6_tx_aligned,
    stat_link6_tx_ready, stat_link7_link_ready, stat_link7_rx_data_error_seen, stat_link7_rx_empty,
    stat_link7_rx_frame_ready, stat_link7_rx_gbt_ready, stat_link7_rx_gbt_ready_lost_flag, stat_link7_rx_is_data,
    stat_link7_rx_match_flag, stat_link7_rx_reply_flag, stat_link7_tx_aligned, stat_link7_tx_ready,
    stat_link8_link_ready, stat_link8_rx_data_error_seen, stat_link8_rx_empty, stat_link8_rx_frame_ready,
    stat_link8_rx_gbt_ready, stat_link8_rx_gbt_ready_lost_flag, stat_link8_rx_is_data, stat_link8_rx_match_flag,
    stat_link8_rx_reply_flag, stat_link8_tx_aligned, stat_link8_tx_ready, stat_link9_link_ready,
    stat_link9_rx_data_error_seen, stat_link9_rx_empty, stat_link9_rx_frame_ready, stat_link9_rx_gbt_ready,
    stat_link9_rx_gbt_ready_lost_flag, stat_link9_rx_is_data, stat_link9_rx_match_flag, stat_link9_rx_reply_flag,
    stat_link9_tx_aligned, stat_link9_tx_ready : std_logic := '0';

  signal conf_test_pattern_sel : std_logic_vector(1 downto 0) := (others => '0');
  signal conf_loopback : std_logic_vector(2 downto 0) := (others => '0');
  signal conf_link10_gbtx_addr, conf_link10_ic_data, conf_link10_tx_ch, conf_link10_tx_comm, conf_link10_tx_trid,
    conf_link11_gbtx_addr, conf_link11_ic_data, conf_link11_tx_ch, conf_link11_tx_comm, conf_link11_tx_trid,
    conf_link12_gbtx_addr, conf_link12_ic_data, conf_link12_tx_ch, conf_link12_tx_comm, conf_link12_tx_trid,
    conf_link1_gbtx_addr, conf_link1_ic_data, conf_link1_tx_ch, conf_link1_tx_comm, conf_link1_tx_trid,
    conf_link2_gbtx_addr, conf_link2_ic_data, conf_link2_tx_ch, conf_link2_tx_comm, conf_link2_tx_trid,
    conf_link3_gbtx_addr, conf_link3_ic_data, conf_link3_tx_ch, conf_link3_tx_comm, conf_link3_tx_trid,
    conf_link4_gbtx_addr, conf_link4_ic_data, conf_link4_tx_ch, conf_link4_tx_comm, conf_link4_tx_trid,
    conf_link5_gbtx_addr, conf_link5_ic_data, conf_link5_tx_ch, conf_link5_tx_comm, conf_link5_tx_trid,
    conf_link6_gbtx_addr, conf_link6_ic_data, conf_link6_tx_ch, conf_link6_tx_comm, conf_link6_tx_trid,
    conf_link7_gbtx_addr, conf_link7_ic_data, conf_link7_tx_ch, conf_link7_tx_comm, conf_link7_tx_trid,
    conf_link8_gbtx_addr, conf_link8_ic_data, conf_link8_tx_ch, conf_link8_tx_comm, conf_link8_tx_trid,
    conf_link9_gbtx_addr, conf_link9_ic_data, conf_link9_tx_ch, conf_link9_tx_comm, conf_link9_tx_trid,
    stat_link10_rx_ch, stat_link10_rx_error, stat_link10_rx_gbtx_addr, stat_link10_rx_ic_data, stat_link10_rx_len,
    stat_link10_rx_trid, stat_link11_rx_ch, stat_link11_rx_error, stat_link11_rx_gbtx_addr, stat_link11_rx_ic_data,
    stat_link11_rx_len, stat_link11_rx_trid, stat_link12_rx_ch, stat_link12_rx_error, stat_link12_rx_gbtx_addr,
    stat_link12_rx_ic_data, stat_link12_rx_len, stat_link12_rx_trid, stat_link1_rx_ch, stat_link1_rx_error,
    stat_link1_rx_gbtx_addr, stat_link1_rx_ic_data, stat_link1_rx_len, stat_link1_rx_trid, stat_link2_rx_ch,
    stat_link2_rx_error, stat_link2_rx_gbtx_addr, stat_link2_rx_ic_data, stat_link2_rx_len, stat_link2_rx_trid,
    stat_link3_rx_ch, stat_link3_rx_error, stat_link3_rx_gbtx_addr, stat_link3_rx_ic_data, stat_link3_rx_len,
    stat_link3_rx_trid, stat_link4_rx_ch, stat_link4_rx_error, stat_link4_rx_gbtx_addr, stat_link4_rx_ic_data,
    stat_link4_rx_len, stat_link4_rx_trid, stat_link5_rx_ch, stat_link5_rx_error, stat_link5_rx_gbtx_addr,
    stat_link5_rx_ic_data, stat_link5_rx_len, stat_link5_rx_trid, stat_link6_rx_ch, stat_link6_rx_error,
    stat_link6_rx_gbtx_addr, stat_link6_rx_ic_data, stat_link6_rx_len, stat_link6_rx_trid, stat_link7_rx_ch,
    stat_link7_rx_error, stat_link7_rx_gbtx_addr, stat_link7_rx_ic_data, stat_link7_rx_len, stat_link7_rx_trid,
    stat_link8_rx_ch, stat_link8_rx_error, stat_link8_rx_gbtx_addr, stat_link8_rx_ic_data, stat_link8_rx_len,
    stat_link8_rx_trid, stat_link9_rx_ch, stat_link9_rx_error, stat_link9_rx_gbtx_addr, stat_link9_rx_ic_data,
    stat_link9_rx_len, stat_link9_rx_trid : std_logic_vector(7 downto 0) := (others => '0');
  signal conf_link10_nb_to_read, conf_link10_reg_addr, conf_link11_nb_to_read, conf_link11_reg_addr,
    conf_link12_nb_to_read, conf_link12_reg_addr, conf_link1_nb_to_read, conf_link1_reg_addr, conf_link2_nb_to_read,
    conf_link2_reg_addr, conf_link3_nb_to_read, conf_link3_reg_addr, conf_link4_nb_to_read, conf_link4_reg_addr,
    conf_link5_nb_to_read, conf_link5_reg_addr, conf_link6_nb_to_read, conf_link6_reg_addr, conf_link7_nb_to_read,
    conf_link7_reg_addr, conf_link8_nb_to_read, conf_link8_reg_addr, conf_link9_nb_to_read, conf_link9_reg_addr,
    stat_link10_rx_nb, stat_link10_rx_reg, stat_link11_rx_nb, stat_link11_rx_reg, stat_link12_rx_nb,
    stat_link12_rx_reg, stat_link1_rx_nb, stat_link1_rx_reg, stat_link2_rx_nb, stat_link2_rx_reg, stat_link3_rx_nb,
    stat_link3_rx_reg, stat_link4_rx_nb, stat_link4_rx_reg, stat_link5_rx_nb, stat_link5_rx_reg, stat_link6_rx_nb,
    stat_link6_rx_reg, stat_link7_rx_nb, stat_link7_rx_reg, stat_link8_rx_nb, stat_link8_rx_reg, stat_link9_rx_nb,
    stat_link9_rx_reg : std_logic_vector(15 downto 0) := (others => '0');
  signal conf_link10_ec_data, conf_link11_ec_data, conf_link12_ec_data, conf_link1_ec_data, conf_link2_ec_data,
    conf_link3_ec_data, conf_link4_ec_data, conf_link5_ec_data, conf_link6_ec_data, conf_link7_ec_data,
    conf_link8_ec_data, conf_link9_ec_data, stat_link10_rx_ec_data, stat_link11_rx_ec_data, stat_link12_rx_ec_data,
    stat_link1_rx_ec_data, stat_link2_rx_ec_data, stat_link3_rx_ec_data, stat_link4_rx_ec_data, stat_link5_rx_ec_data,
    stat_link6_rx_ec_data, stat_link7_rx_ec_data, stat_link8_rx_ec_data,
    stat_link9_rx_ec_data : std_logic_vector(31 downto 0) := (others => '0');

  signal stat_link10_no_rxclkout_ctr, stat_link11_no_rxclkout_ctr, stat_link12_no_rxclkout_ctr,
    stat_link1_no_rxclkout_ctr, stat_link2_no_rxclkout_ctr, stat_link3_no_rxclkout_ctr, stat_link4_no_rxclkout_ctr,
    stat_link5_no_rxclkout_ctr, stat_link6_no_rxclkout_ctr, stat_link7_no_rxclkout_ctr, stat_link8_no_rxclkout_ctr,
    stat_link9_no_rxclkout_ctr : unsigned(11 downto 0) := (others => '0');
  signal stat_link10_no_rxclkout_time, stat_link11_no_rxclkout_time, stat_link12_no_rxclkout_time,
    stat_link1_no_rxclkout_time, stat_link2_no_rxclkout_time, stat_link3_no_rxclkout_time, stat_link4_no_rxclkout_time,
    stat_link5_no_rxclkout_time, stat_link6_no_rxclkout_time, stat_link7_no_rxclkout_time, stat_link8_no_rxclkout_time,
    stat_link9_no_rxclkout_time : unsigned(19 downto 0) := (others => '0');

  -- End of automatically-generated VHDL code for register "breakout" signals declaration
  --------------------------------------------------------------------------------


begin


  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals assignment

  conf_dummy                 <= conf(16#00#)(0);
  conf_manualResetTx         <= conf(16#00#)(1);
  conf_manualResetRx         <= conf(16#00#)(2);
  conf_loopback              <= conf(16#00#)(5 downto 3);
  conf_test_pattern_sel      <= conf(16#00#)(7 downto 6);
  conf_link1_sc_enable       <= conf(16#08#)(0);
  conf_link1_tx_trid         <= conf(16#09#)(7 downto 0);
  conf_link1_tx_ch           <= conf(16#09#)(15 downto 8);
  conf_link1_tx_comm         <= conf(16#09#)(31 downto 24);
  conf_link1_ec_data         <= conf(16#0A#)(31 downto 0);
  conf_link1_send_reset      <= conf(16#0B#)(0);
  conf_link1_send_connect    <= conf(16#0B#)(1);
  conf_link1_reset_sc        <= conf(16#0B#)(2);
  conf_link1_gbtx_addr       <= conf(16#0C#)(7 downto 0);
  conf_link1_ic_rd_wr        <= conf(16#0C#)(8);
  conf_link1_read_fifo       <= conf(16#0C#)(9);
  conf_link1_nb_to_read      <= conf(16#0C#)(31 downto 16);
  conf_link1_reg_addr        <= conf(16#0D#)(15 downto 0);
  conf_link1_ic_data         <= conf(16#0E#)(7 downto 0);
  conf_link2_sc_enable       <= conf(16#10#)(0);
  conf_link2_tx_trid         <= conf(16#11#)(7 downto 0);
  conf_link2_tx_ch           <= conf(16#11#)(15 downto 8);
  conf_link2_tx_comm         <= conf(16#11#)(31 downto 24);
  conf_link2_ec_data         <= conf(16#12#)(31 downto 0);
  conf_link2_send_reset      <= conf(16#13#)(0);
  conf_link2_send_connect    <= conf(16#13#)(1);
  conf_link2_reset_sc        <= conf(16#13#)(2);
  conf_link2_gbtx_addr       <= conf(16#14#)(7 downto 0);
  conf_link2_ic_rd_wr        <= conf(16#14#)(8);
  conf_link2_read_fifo       <= conf(16#14#)(9);
  conf_link2_nb_to_read      <= conf(16#14#)(31 downto 16);
  conf_link2_reg_addr        <= conf(16#15#)(15 downto 0);
  conf_link2_ic_data         <= conf(16#16#)(7 downto 0);
  conf_link3_sc_enable       <= conf(16#18#)(0);
  conf_link3_tx_trid         <= conf(16#19#)(7 downto 0);
  conf_link3_tx_ch           <= conf(16#19#)(15 downto 8);
  conf_link3_tx_comm         <= conf(16#19#)(31 downto 24);
  conf_link3_ec_data         <= conf(16#1A#)(31 downto 0);
  conf_link3_send_reset      <= conf(16#1B#)(0);
  conf_link3_send_connect    <= conf(16#1B#)(1);
  conf_link3_reset_sc        <= conf(16#1B#)(2);
  conf_link3_gbtx_addr       <= conf(16#1C#)(7 downto 0);
  conf_link3_ic_rd_wr        <= conf(16#1C#)(8);
  conf_link3_read_fifo       <= conf(16#1C#)(9);
  conf_link3_nb_to_read      <= conf(16#1C#)(31 downto 16);
  conf_link3_reg_addr        <= conf(16#1D#)(15 downto 0);
  conf_link3_ic_data         <= conf(16#1E#)(7 downto 0);
  conf_link4_sc_enable       <= conf(16#20#)(0);
  conf_link4_tx_trid         <= conf(16#21#)(7 downto 0);
  conf_link4_tx_ch           <= conf(16#21#)(15 downto 8);
  conf_link4_tx_comm         <= conf(16#21#)(31 downto 24);
  conf_link4_ec_data         <= conf(16#22#)(31 downto 0);
  conf_link4_send_reset      <= conf(16#23#)(0);
  conf_link4_send_connect    <= conf(16#23#)(1);
  conf_link4_reset_sc        <= conf(16#23#)(2);
  conf_link4_gbtx_addr       <= conf(16#24#)(7 downto 0);
  conf_link4_ic_rd_wr        <= conf(16#24#)(8);
  conf_link4_read_fifo       <= conf(16#24#)(9);
  conf_link4_nb_to_read      <= conf(16#24#)(31 downto 16);
  conf_link4_reg_addr        <= conf(16#25#)(15 downto 0);
  conf_link4_ic_data         <= conf(16#26#)(7 downto 0);
  conf_link5_sc_enable       <= conf(16#28#)(0);
  conf_link5_tx_trid         <= conf(16#29#)(7 downto 0);
  conf_link5_tx_ch           <= conf(16#29#)(15 downto 8);
  conf_link5_tx_comm         <= conf(16#29#)(31 downto 24);
  conf_link5_ec_data         <= conf(16#2A#)(31 downto 0);
  conf_link5_send_reset      <= conf(16#2B#)(0);
  conf_link5_send_connect    <= conf(16#2B#)(1);
  conf_link5_reset_sc        <= conf(16#2B#)(2);
  conf_link5_gbtx_addr       <= conf(16#2C#)(7 downto 0);
  conf_link5_ic_rd_wr        <= conf(16#2C#)(8);
  conf_link5_read_fifo       <= conf(16#2C#)(9);
  conf_link5_nb_to_read      <= conf(16#2C#)(31 downto 16);
  conf_link5_reg_addr        <= conf(16#2D#)(15 downto 0);
  conf_link5_ic_data         <= conf(16#2E#)(7 downto 0);
  conf_link6_sc_enable       <= conf(16#30#)(0);
  conf_link6_tx_trid         <= conf(16#31#)(7 downto 0);
  conf_link6_tx_ch           <= conf(16#31#)(15 downto 8);
  conf_link6_tx_comm         <= conf(16#31#)(31 downto 24);
  conf_link6_ec_data         <= conf(16#32#)(31 downto 0);
  conf_link6_send_reset      <= conf(16#33#)(0);
  conf_link6_send_connect    <= conf(16#33#)(1);
  conf_link6_reset_sc        <= conf(16#33#)(2);
  conf_link6_gbtx_addr       <= conf(16#34#)(7 downto 0);
  conf_link6_ic_rd_wr        <= conf(16#34#)(8);
  conf_link6_read_fifo       <= conf(16#34#)(9);
  conf_link6_nb_to_read      <= conf(16#34#)(31 downto 16);
  conf_link6_reg_addr        <= conf(16#35#)(15 downto 0);
  conf_link6_ic_data         <= conf(16#36#)(7 downto 0);
  conf_link7_sc_enable       <= conf(16#38#)(0);
  conf_link7_tx_trid         <= conf(16#39#)(7 downto 0);
  conf_link7_tx_ch           <= conf(16#39#)(15 downto 8);
  conf_link7_tx_comm         <= conf(16#39#)(31 downto 24);
  conf_link7_ec_data         <= conf(16#3A#)(31 downto 0);
  conf_link7_send_reset      <= conf(16#3B#)(0);
  conf_link7_send_connect    <= conf(16#3B#)(1);
  conf_link7_reset_sc        <= conf(16#3B#)(2);
  conf_link7_gbtx_addr       <= conf(16#3C#)(7 downto 0);
  conf_link7_ic_rd_wr        <= conf(16#3C#)(8);
  conf_link7_read_fifo       <= conf(16#3C#)(9);
  conf_link7_nb_to_read      <= conf(16#3C#)(31 downto 16);
  conf_link7_reg_addr        <= conf(16#3D#)(15 downto 0);
  conf_link7_ic_data         <= conf(16#3E#)(7 downto 0);
  conf_link8_sc_enable       <= conf(16#40#)(0);
  conf_link8_tx_trid         <= conf(16#41#)(7 downto 0);
  conf_link8_tx_ch           <= conf(16#41#)(15 downto 8);
  conf_link8_tx_comm         <= conf(16#41#)(31 downto 24);
  conf_link8_ec_data         <= conf(16#42#)(31 downto 0);
  conf_link8_send_reset      <= conf(16#43#)(0);
  conf_link8_send_connect    <= conf(16#43#)(1);
  conf_link8_reset_sc        <= conf(16#43#)(2);
  conf_link8_gbtx_addr       <= conf(16#44#)(7 downto 0);
  conf_link8_ic_rd_wr        <= conf(16#44#)(8);
  conf_link8_read_fifo       <= conf(16#44#)(9);
  conf_link8_nb_to_read      <= conf(16#44#)(31 downto 16);
  conf_link8_reg_addr        <= conf(16#45#)(15 downto 0);
  conf_link8_ic_data         <= conf(16#46#)(7 downto 0);
  conf_link9_sc_enable       <= conf(16#48#)(0);
  conf_link9_tx_trid         <= conf(16#49#)(7 downto 0);
  conf_link9_tx_ch           <= conf(16#49#)(15 downto 8);
  conf_link9_tx_comm         <= conf(16#49#)(31 downto 24);
  conf_link9_ec_data         <= conf(16#4A#)(31 downto 0);
  conf_link9_send_reset      <= conf(16#4B#)(0);
  conf_link9_send_connect    <= conf(16#4B#)(1);
  conf_link9_reset_sc        <= conf(16#4B#)(2);
  conf_link9_gbtx_addr       <= conf(16#4C#)(7 downto 0);
  conf_link9_ic_rd_wr        <= conf(16#4C#)(8);
  conf_link9_read_fifo       <= conf(16#4C#)(9);
  conf_link9_nb_to_read      <= conf(16#4C#)(31 downto 16);
  conf_link9_reg_addr        <= conf(16#4D#)(15 downto 0);
  conf_link9_ic_data         <= conf(16#4E#)(7 downto 0);
  conf_link10_sc_enable      <= conf(16#50#)(0);
  conf_link10_tx_trid        <= conf(16#51#)(7 downto 0);
  conf_link10_tx_ch          <= conf(16#51#)(15 downto 8);
  conf_link10_tx_comm        <= conf(16#51#)(31 downto 24);
  conf_link10_ec_data        <= conf(16#52#)(31 downto 0);
  conf_link10_send_reset     <= conf(16#53#)(0);
  conf_link10_send_connect   <= conf(16#53#)(1);
  conf_link10_reset_sc       <= conf(16#53#)(2);
  conf_link10_gbtx_addr      <= conf(16#54#)(7 downto 0);
  conf_link10_ic_rd_wr       <= conf(16#54#)(8);
  conf_link10_read_fifo      <= conf(16#54#)(9);
  conf_link10_nb_to_read     <= conf(16#54#)(31 downto 16);
  conf_link10_reg_addr       <= conf(16#55#)(15 downto 0);
  conf_link10_ic_data        <= conf(16#56#)(7 downto 0);
  conf_link11_sc_enable      <= conf(16#58#)(0);
  conf_link11_tx_trid        <= conf(16#59#)(7 downto 0);
  conf_link11_tx_ch          <= conf(16#59#)(15 downto 8);
  conf_link11_tx_comm        <= conf(16#59#)(31 downto 24);
  conf_link11_ec_data        <= conf(16#5A#)(31 downto 0);
  conf_link11_send_reset     <= conf(16#5B#)(0);
  conf_link11_send_connect   <= conf(16#5B#)(1);
  conf_link11_reset_sc       <= conf(16#5B#)(2);
  conf_link11_gbtx_addr      <= conf(16#5C#)(7 downto 0);
  conf_link11_ic_rd_wr       <= conf(16#5C#)(8);
  conf_link11_read_fifo      <= conf(16#5C#)(9);
  conf_link11_nb_to_read     <= conf(16#5C#)(31 downto 16);
  conf_link11_reg_addr       <= conf(16#5D#)(15 downto 0);
  conf_link11_ic_data        <= conf(16#5E#)(7 downto 0);
  conf_link12_sc_enable      <= conf(16#60#)(0);
  conf_link12_tx_trid        <= conf(16#61#)(7 downto 0);
  conf_link12_tx_ch          <= conf(16#61#)(15 downto 8);
  conf_link12_tx_comm        <= conf(16#61#)(31 downto 24);
  conf_link12_ec_data        <= conf(16#62#)(31 downto 0);
  conf_link12_send_reset     <= conf(16#63#)(0);
  conf_link12_send_connect   <= conf(16#63#)(1);
  conf_link12_reset_sc       <= conf(16#63#)(2);
  conf_link12_gbtx_addr      <= conf(16#64#)(7 downto 0);
  conf_link12_ic_rd_wr       <= conf(16#64#)(8);
  conf_link12_read_fifo      <= conf(16#64#)(9);
  conf_link12_nb_to_read     <= conf(16#64#)(31 downto 16);
  conf_link12_reg_addr       <= conf(16#65#)(15 downto 0);
  conf_link12_ic_data        <= conf(16#66#)(7 downto 0);
  stat(16#00#)(0)            <= stat_dummy;
  stat(16#08#)(7 downto 0)   <= stat_link1_rx_trid;
  stat(16#08#)(15 downto 8)  <= stat_link1_rx_ch;
  stat(16#08#)(23 downto 16) <= stat_link1_rx_len;
  stat(16#08#)(31 downto 24) <= stat_link1_rx_error;
  stat(16#09#)(31 downto 0)  <= stat_link1_rx_ec_data;
  stat(16#0A#)(0)            <= stat_link1_rx_reply_flag;
  stat(16#0A#)(1)            <= stat_link1_tx_ready;
  stat(16#0A#)(2)            <= stat_link1_rx_empty;
  stat(16#0B#)(7 downto 0)   <= stat_link1_rx_gbtx_addr;
  stat(16#0C#)(15 downto 0)  <= stat_link1_rx_reg;
  stat(16#0C#)(31 downto 16) <= stat_link1_rx_nb;
  stat(16#0D#)(7 downto 0)   <= stat_link1_rx_ic_data;
  stat(16#0E#)(0)            <= stat_link1_link_ready;
  stat(16#0E#)(2)            <= stat_link1_tx_aligned;
  stat(16#0E#)(4)            <= stat_link1_rx_gbt_ready;
  stat(16#0E#)(5)            <= stat_link1_rx_gbt_ready_lost_flag;
  stat(16#0E#)(6)            <= stat_link1_rx_data_error_seen;
  stat(16#0E#)(7)            <= stat_link1_rx_match_flag;
  stat(16#0E#)(8)            <= stat_link1_rx_is_data;
  stat(16#0E#)(10)           <= stat_link1_rx_frame_ready;
  stat(16#0F#)(19 downto 0)  <= std_logic_vector( stat_link1_no_rxclkout_time );
  stat(16#0F#)(31 downto 20) <= std_logic_vector( stat_link1_no_rxclkout_ctr );
  stat(16#10#)(7 downto 0)   <= stat_link2_rx_trid;
  stat(16#10#)(15 downto 8)  <= stat_link2_rx_ch;
  stat(16#10#)(23 downto 16) <= stat_link2_rx_len;
  stat(16#10#)(31 downto 24) <= stat_link2_rx_error;
  stat(16#11#)(31 downto 0)  <= stat_link2_rx_ec_data;
  stat(16#12#)(0)            <= stat_link2_rx_reply_flag;
  stat(16#12#)(1)            <= stat_link2_tx_ready;
  stat(16#12#)(2)            <= stat_link2_rx_empty;
  stat(16#13#)(7 downto 0)   <= stat_link2_rx_gbtx_addr;
  stat(16#14#)(15 downto 0)  <= stat_link2_rx_reg;
  stat(16#14#)(31 downto 16) <= stat_link2_rx_nb;
  stat(16#15#)(7 downto 0)   <= stat_link2_rx_ic_data;
  stat(16#16#)(0)            <= stat_link2_link_ready;
  stat(16#16#)(2)            <= stat_link2_tx_aligned;
  stat(16#16#)(4)            <= stat_link2_rx_gbt_ready;
  stat(16#16#)(5)            <= stat_link2_rx_gbt_ready_lost_flag;
  stat(16#16#)(6)            <= stat_link2_rx_data_error_seen;
  stat(16#16#)(7)            <= stat_link2_rx_match_flag;
  stat(16#16#)(8)            <= stat_link2_rx_is_data;
  stat(16#16#)(10)           <= stat_link2_rx_frame_ready;
  stat(16#17#)(19 downto 0)  <= std_logic_vector( stat_link2_no_rxclkout_time );
  stat(16#17#)(31 downto 20) <= std_logic_vector( stat_link2_no_rxclkout_ctr );
  stat(16#18#)(7 downto 0)   <= stat_link3_rx_trid;
  stat(16#18#)(15 downto 8)  <= stat_link3_rx_ch;
  stat(16#18#)(23 downto 16) <= stat_link3_rx_len;
  stat(16#18#)(31 downto 24) <= stat_link3_rx_error;
  stat(16#19#)(31 downto 0)  <= stat_link3_rx_ec_data;
  stat(16#1A#)(0)            <= stat_link3_rx_reply_flag;
  stat(16#1A#)(1)            <= stat_link3_tx_ready;
  stat(16#1A#)(2)            <= stat_link3_rx_empty;
  stat(16#1B#)(7 downto 0)   <= stat_link3_rx_gbtx_addr;
  stat(16#1C#)(15 downto 0)  <= stat_link3_rx_reg;
  stat(16#1C#)(31 downto 16) <= stat_link3_rx_nb;
  stat(16#1D#)(7 downto 0)   <= stat_link3_rx_ic_data;
  stat(16#1E#)(0)            <= stat_link3_link_ready;
  stat(16#1E#)(2)            <= stat_link3_tx_aligned;
  stat(16#1E#)(4)            <= stat_link3_rx_gbt_ready;
  stat(16#1E#)(5)            <= stat_link3_rx_gbt_ready_lost_flag;
  stat(16#1E#)(6)            <= stat_link3_rx_data_error_seen;
  stat(16#1E#)(7)            <= stat_link3_rx_match_flag;
  stat(16#1E#)(8)            <= stat_link3_rx_is_data;
  stat(16#1E#)(10)           <= stat_link3_rx_frame_ready;
  stat(16#1F#)(19 downto 0)  <= std_logic_vector( stat_link3_no_rxclkout_time );
  stat(16#1F#)(31 downto 20) <= std_logic_vector( stat_link3_no_rxclkout_ctr );
  stat(16#20#)(7 downto 0)   <= stat_link4_rx_trid;
  stat(16#20#)(15 downto 8)  <= stat_link4_rx_ch;
  stat(16#20#)(23 downto 16) <= stat_link4_rx_len;
  stat(16#20#)(31 downto 24) <= stat_link4_rx_error;
  stat(16#21#)(31 downto 0)  <= stat_link4_rx_ec_data;
  stat(16#22#)(0)            <= stat_link4_rx_reply_flag;
  stat(16#22#)(1)            <= stat_link4_tx_ready;
  stat(16#22#)(2)            <= stat_link4_rx_empty;
  stat(16#23#)(7 downto 0)   <= stat_link4_rx_gbtx_addr;
  stat(16#24#)(15 downto 0)  <= stat_link4_rx_reg;
  stat(16#24#)(31 downto 16) <= stat_link4_rx_nb;
  stat(16#25#)(7 downto 0)   <= stat_link4_rx_ic_data;
  stat(16#26#)(0)            <= stat_link4_link_ready;
  stat(16#26#)(2)            <= stat_link4_tx_aligned;
  stat(16#26#)(4)            <= stat_link4_rx_gbt_ready;
  stat(16#26#)(5)            <= stat_link4_rx_gbt_ready_lost_flag;
  stat(16#26#)(6)            <= stat_link4_rx_data_error_seen;
  stat(16#26#)(7)            <= stat_link4_rx_match_flag;
  stat(16#26#)(8)            <= stat_link4_rx_is_data;
  stat(16#26#)(10)           <= stat_link4_rx_frame_ready;
  stat(16#27#)(19 downto 0)  <= std_logic_vector( stat_link4_no_rxclkout_time );
  stat(16#27#)(31 downto 20) <= std_logic_vector( stat_link4_no_rxclkout_ctr );
  stat(16#28#)(7 downto 0)   <= stat_link5_rx_trid;
  stat(16#28#)(15 downto 8)  <= stat_link5_rx_ch;
  stat(16#28#)(23 downto 16) <= stat_link5_rx_len;
  stat(16#28#)(31 downto 24) <= stat_link5_rx_error;
  stat(16#29#)(31 downto 0)  <= stat_link5_rx_ec_data;
  stat(16#2A#)(0)            <= stat_link5_rx_reply_flag;
  stat(16#2A#)(1)            <= stat_link5_tx_ready;
  stat(16#2A#)(2)            <= stat_link5_rx_empty;
  stat(16#2B#)(7 downto 0)   <= stat_link5_rx_gbtx_addr;
  stat(16#2C#)(15 downto 0)  <= stat_link5_rx_reg;
  stat(16#2C#)(31 downto 16) <= stat_link5_rx_nb;
  stat(16#2D#)(7 downto 0)   <= stat_link5_rx_ic_data;
  stat(16#2E#)(0)            <= stat_link5_link_ready;
  stat(16#2E#)(2)            <= stat_link5_tx_aligned;
  stat(16#2E#)(4)            <= stat_link5_rx_gbt_ready;
  stat(16#2E#)(5)            <= stat_link5_rx_gbt_ready_lost_flag;
  stat(16#2E#)(6)            <= stat_link5_rx_data_error_seen;
  stat(16#2E#)(7)            <= stat_link5_rx_match_flag;
  stat(16#2E#)(8)            <= stat_link5_rx_is_data;
  stat(16#2E#)(10)           <= stat_link5_rx_frame_ready;
  stat(16#2F#)(19 downto 0)  <= std_logic_vector( stat_link5_no_rxclkout_time );
  stat(16#2F#)(31 downto 20) <= std_logic_vector( stat_link5_no_rxclkout_ctr );
  stat(16#30#)(7 downto 0)   <= stat_link6_rx_trid;
  stat(16#30#)(15 downto 8)  <= stat_link6_rx_ch;
  stat(16#30#)(23 downto 16) <= stat_link6_rx_len;
  stat(16#30#)(31 downto 24) <= stat_link6_rx_error;
  stat(16#31#)(31 downto 0)  <= stat_link6_rx_ec_data;
  stat(16#32#)(0)            <= stat_link6_rx_reply_flag;
  stat(16#32#)(1)            <= stat_link6_tx_ready;
  stat(16#32#)(2)            <= stat_link6_rx_empty;
  stat(16#33#)(7 downto 0)   <= stat_link6_rx_gbtx_addr;
  stat(16#34#)(15 downto 0)  <= stat_link6_rx_reg;
  stat(16#34#)(31 downto 16) <= stat_link6_rx_nb;
  stat(16#35#)(7 downto 0)   <= stat_link6_rx_ic_data;
  stat(16#36#)(0)            <= stat_link6_link_ready;
  stat(16#36#)(2)            <= stat_link6_tx_aligned;
  stat(16#36#)(4)            <= stat_link6_rx_gbt_ready;
  stat(16#36#)(5)            <= stat_link6_rx_gbt_ready_lost_flag;
  stat(16#36#)(6)            <= stat_link6_rx_data_error_seen;
  stat(16#36#)(7)            <= stat_link6_rx_match_flag;
  stat(16#36#)(8)            <= stat_link6_rx_is_data;
  stat(16#36#)(10)           <= stat_link6_rx_frame_ready;
  stat(16#37#)(19 downto 0)  <= std_logic_vector( stat_link6_no_rxclkout_time );
  stat(16#37#)(31 downto 20) <= std_logic_vector( stat_link6_no_rxclkout_ctr );
  stat(16#38#)(7 downto 0)   <= stat_link7_rx_trid;
  stat(16#38#)(15 downto 8)  <= stat_link7_rx_ch;
  stat(16#38#)(23 downto 16) <= stat_link7_rx_len;
  stat(16#38#)(31 downto 24) <= stat_link7_rx_error;
  stat(16#39#)(31 downto 0)  <= stat_link7_rx_ec_data;
  stat(16#3A#)(0)            <= stat_link7_rx_reply_flag;
  stat(16#3A#)(1)            <= stat_link7_tx_ready;
  stat(16#3A#)(2)            <= stat_link7_rx_empty;
  stat(16#3B#)(7 downto 0)   <= stat_link7_rx_gbtx_addr;
  stat(16#3C#)(15 downto 0)  <= stat_link7_rx_reg;
  stat(16#3C#)(31 downto 16) <= stat_link7_rx_nb;
  stat(16#3D#)(7 downto 0)   <= stat_link7_rx_ic_data;
  stat(16#3E#)(0)            <= stat_link7_link_ready;
  stat(16#3E#)(2)            <= stat_link7_tx_aligned;
  stat(16#3E#)(4)            <= stat_link7_rx_gbt_ready;
  stat(16#3E#)(5)            <= stat_link7_rx_gbt_ready_lost_flag;
  stat(16#3E#)(6)            <= stat_link7_rx_data_error_seen;
  stat(16#3E#)(7)            <= stat_link7_rx_match_flag;
  stat(16#3E#)(8)            <= stat_link7_rx_is_data;
  stat(16#3E#)(10)           <= stat_link7_rx_frame_ready;
  stat(16#3F#)(19 downto 0)  <= std_logic_vector( stat_link7_no_rxclkout_time );
  stat(16#3F#)(31 downto 20) <= std_logic_vector( stat_link7_no_rxclkout_ctr );
  stat(16#40#)(7 downto 0)   <= stat_link8_rx_trid;
  stat(16#40#)(15 downto 8)  <= stat_link8_rx_ch;
  stat(16#40#)(23 downto 16) <= stat_link8_rx_len;
  stat(16#40#)(31 downto 24) <= stat_link8_rx_error;
  stat(16#41#)(31 downto 0)  <= stat_link8_rx_ec_data;
  stat(16#42#)(0)            <= stat_link8_rx_reply_flag;
  stat(16#42#)(1)            <= stat_link8_tx_ready;
  stat(16#42#)(2)            <= stat_link8_rx_empty;
  stat(16#43#)(7 downto 0)   <= stat_link8_rx_gbtx_addr;
  stat(16#44#)(15 downto 0)  <= stat_link8_rx_reg;
  stat(16#44#)(31 downto 16) <= stat_link8_rx_nb;
  stat(16#45#)(7 downto 0)   <= stat_link8_rx_ic_data;
  stat(16#46#)(0)            <= stat_link8_link_ready;
  stat(16#46#)(2)            <= stat_link8_tx_aligned;
  stat(16#46#)(4)            <= stat_link8_rx_gbt_ready;
  stat(16#46#)(5)            <= stat_link8_rx_gbt_ready_lost_flag;
  stat(16#46#)(6)            <= stat_link8_rx_data_error_seen;
  stat(16#46#)(7)            <= stat_link8_rx_match_flag;
  stat(16#46#)(8)            <= stat_link8_rx_is_data;
  stat(16#46#)(10)           <= stat_link8_rx_frame_ready;
  stat(16#47#)(19 downto 0)  <= std_logic_vector( stat_link8_no_rxclkout_time );
  stat(16#47#)(31 downto 20) <= std_logic_vector( stat_link8_no_rxclkout_ctr );
  stat(16#48#)(7 downto 0)   <= stat_link9_rx_trid;
  stat(16#48#)(15 downto 8)  <= stat_link9_rx_ch;
  stat(16#48#)(23 downto 16) <= stat_link9_rx_len;
  stat(16#48#)(31 downto 24) <= stat_link9_rx_error;
  stat(16#49#)(31 downto 0)  <= stat_link9_rx_ec_data;
  stat(16#4A#)(0)            <= stat_link9_rx_reply_flag;
  stat(16#4A#)(1)            <= stat_link9_tx_ready;
  stat(16#4A#)(2)            <= stat_link9_rx_empty;
  stat(16#4B#)(7 downto 0)   <= stat_link9_rx_gbtx_addr;
  stat(16#4C#)(15 downto 0)  <= stat_link9_rx_reg;
  stat(16#4C#)(31 downto 16) <= stat_link9_rx_nb;
  stat(16#4D#)(7 downto 0)   <= stat_link9_rx_ic_data;
  stat(16#4E#)(0)            <= stat_link9_link_ready;
  stat(16#4E#)(2)            <= stat_link9_tx_aligned;
  stat(16#4E#)(4)            <= stat_link9_rx_gbt_ready;
  stat(16#4E#)(5)            <= stat_link9_rx_gbt_ready_lost_flag;
  stat(16#4E#)(6)            <= stat_link9_rx_data_error_seen;
  stat(16#4E#)(7)            <= stat_link9_rx_match_flag;
  stat(16#4E#)(8)            <= stat_link9_rx_is_data;
  stat(16#4E#)(10)           <= stat_link9_rx_frame_ready;
  stat(16#4F#)(19 downto 0)  <= std_logic_vector( stat_link9_no_rxclkout_time );
  stat(16#4F#)(31 downto 20) <= std_logic_vector( stat_link9_no_rxclkout_ctr );
  stat(16#50#)(7 downto 0)   <= stat_link10_rx_trid;
  stat(16#50#)(15 downto 8)  <= stat_link10_rx_ch;
  stat(16#50#)(23 downto 16) <= stat_link10_rx_len;
  stat(16#50#)(31 downto 24) <= stat_link10_rx_error;
  stat(16#51#)(31 downto 0)  <= stat_link10_rx_ec_data;
  stat(16#52#)(0)            <= stat_link10_rx_reply_flag;
  stat(16#52#)(1)            <= stat_link10_tx_ready;
  stat(16#52#)(2)            <= stat_link10_rx_empty;
  stat(16#53#)(7 downto 0)   <= stat_link10_rx_gbtx_addr;
  stat(16#54#)(15 downto 0)  <= stat_link10_rx_reg;
  stat(16#54#)(31 downto 16) <= stat_link10_rx_nb;
  stat(16#55#)(7 downto 0)   <= stat_link10_rx_ic_data;
  stat(16#56#)(0)            <= stat_link10_link_ready;
  stat(16#56#)(2)            <= stat_link10_tx_aligned;
  stat(16#56#)(4)            <= stat_link10_rx_gbt_ready;
  stat(16#56#)(5)            <= stat_link10_rx_gbt_ready_lost_flag;
  stat(16#56#)(6)            <= stat_link10_rx_data_error_seen;
  stat(16#56#)(7)            <= stat_link10_rx_match_flag;
  stat(16#56#)(8)            <= stat_link10_rx_is_data;
  stat(16#56#)(10)           <= stat_link10_rx_frame_ready;
  stat(16#57#)(19 downto 0)  <= std_logic_vector( stat_link10_no_rxclkout_time );
  stat(16#57#)(31 downto 20) <= std_logic_vector( stat_link10_no_rxclkout_ctr );
  stat(16#58#)(7 downto 0)   <= stat_link11_rx_trid;
  stat(16#58#)(15 downto 8)  <= stat_link11_rx_ch;
  stat(16#58#)(23 downto 16) <= stat_link11_rx_len;
  stat(16#58#)(31 downto 24) <= stat_link11_rx_error;
  stat(16#59#)(31 downto 0)  <= stat_link11_rx_ec_data;
  stat(16#5A#)(0)            <= stat_link11_rx_reply_flag;
  stat(16#5A#)(1)            <= stat_link11_tx_ready;
  stat(16#5A#)(2)            <= stat_link11_rx_empty;
  stat(16#5B#)(7 downto 0)   <= stat_link11_rx_gbtx_addr;
  stat(16#5C#)(15 downto 0)  <= stat_link11_rx_reg;
  stat(16#5C#)(31 downto 16) <= stat_link11_rx_nb;
  stat(16#5D#)(7 downto 0)   <= stat_link11_rx_ic_data;
  stat(16#5E#)(0)            <= stat_link11_link_ready;
  stat(16#5E#)(2)            <= stat_link11_tx_aligned;
  stat(16#5E#)(4)            <= stat_link11_rx_gbt_ready;
  stat(16#5E#)(5)            <= stat_link11_rx_gbt_ready_lost_flag;
  stat(16#5E#)(6)            <= stat_link11_rx_data_error_seen;
  stat(16#5E#)(7)            <= stat_link11_rx_match_flag;
  stat(16#5E#)(8)            <= stat_link11_rx_is_data;
  stat(16#5E#)(10)           <= stat_link11_rx_frame_ready;
  stat(16#5F#)(19 downto 0)  <= std_logic_vector( stat_link11_no_rxclkout_time );
  stat(16#5F#)(31 downto 20) <= std_logic_vector( stat_link11_no_rxclkout_ctr );
  stat(16#60#)(7 downto 0)   <= stat_link12_rx_trid;
  stat(16#60#)(15 downto 8)  <= stat_link12_rx_ch;
  stat(16#60#)(23 downto 16) <= stat_link12_rx_len;
  stat(16#60#)(31 downto 24) <= stat_link12_rx_error;
  stat(16#61#)(31 downto 0)  <= stat_link12_rx_ec_data;
  stat(16#62#)(0)            <= stat_link12_rx_reply_flag;
  stat(16#62#)(1)            <= stat_link12_tx_ready;
  stat(16#62#)(2)            <= stat_link12_rx_empty;
  stat(16#63#)(7 downto 0)   <= stat_link12_rx_gbtx_addr;
  stat(16#64#)(15 downto 0)  <= stat_link12_rx_reg;
  stat(16#64#)(31 downto 16) <= stat_link12_rx_nb;
  stat(16#65#)(7 downto 0)   <= stat_link12_rx_ic_data;
  stat(16#66#)(0)            <= stat_link12_link_ready;
  stat(16#66#)(2)            <= stat_link12_tx_aligned;
  stat(16#66#)(4)            <= stat_link12_rx_gbt_ready;
  stat(16#66#)(5)            <= stat_link12_rx_gbt_ready_lost_flag;
  stat(16#66#)(6)            <= stat_link12_rx_data_error_seen;
  stat(16#66#)(7)            <= stat_link12_rx_match_flag;
  stat(16#66#)(8)            <= stat_link12_rx_is_data;
  stat(16#66#)(10)           <= stat_link12_rx_frame_ready;
  stat(16#67#)(19 downto 0)  <= std_logic_vector( stat_link12_no_rxclkout_time );
  stat(16#67#)(31 downto 20) <= std_logic_vector( stat_link12_no_rxclkout_ctr );

  -- End of automatically-generated VHDL code for register "breakout" signals assignment
  --------------------------------------------------------------------------------


-- There's a python script in the fw repo (src/tm7/tm7_gbt/addr_table) to automatically generate this following fan-out code for convenience
( stat_link1_rx_trid                 , stat_link2_rx_trid                 , stat_link3_rx_trid                 , stat_link4_rx_trid                 , stat_link5_rx_trid                 , stat_link6_rx_trid                 , stat_link7_rx_trid                 , stat_link8_rx_trid                 , stat_link9_rx_trid                 , stat_link10_rx_trid                , stat_link11_rx_trid                , stat_link12_rx_trid                ) <= rx_trid;
( stat_link1_rx_ch                   , stat_link2_rx_ch                   , stat_link3_rx_ch                   , stat_link4_rx_ch                   , stat_link5_rx_ch                   , stat_link6_rx_ch                   , stat_link7_rx_ch                   , stat_link8_rx_ch                   , stat_link9_rx_ch                   , stat_link10_rx_ch                  , stat_link11_rx_ch                  , stat_link12_rx_ch                  ) <= rx_ch;
( stat_link1_rx_len                  , stat_link2_rx_len                  , stat_link3_rx_len                  , stat_link4_rx_len                  , stat_link5_rx_len                  , stat_link6_rx_len                  , stat_link7_rx_len                  , stat_link8_rx_len                  , stat_link9_rx_len                  , stat_link10_rx_len                 , stat_link11_rx_len                 , stat_link12_rx_len                 ) <= rx_len;
( stat_link1_rx_error                , stat_link2_rx_error                , stat_link3_rx_error                , stat_link4_rx_error                , stat_link5_rx_error                , stat_link6_rx_error                , stat_link7_rx_error                , stat_link8_rx_error                , stat_link9_rx_error                , stat_link10_rx_error               , stat_link11_rx_error               , stat_link12_rx_error               ) <= rx_error;
( stat_link1_rx_ec_data              , stat_link2_rx_ec_data              , stat_link3_rx_ec_data              , stat_link4_rx_ec_data              , stat_link5_rx_ec_data              , stat_link6_rx_ec_data              , stat_link7_rx_ec_data              , stat_link8_rx_ec_data              , stat_link9_rx_ec_data              , stat_link10_rx_ec_data             , stat_link11_rx_ec_data             , stat_link12_rx_ec_data             ) <= rx_ec_data;
( stat_link1_rx_reply_flag           , stat_link2_rx_reply_flag           , stat_link3_rx_reply_flag           , stat_link4_rx_reply_flag           , stat_link5_rx_reply_flag           , stat_link6_rx_reply_flag           , stat_link7_rx_reply_flag           , stat_link8_rx_reply_flag           , stat_link9_rx_reply_flag           , stat_link10_rx_reply_flag          , stat_link11_rx_reply_flag          , stat_link12_rx_reply_flag          ) <= rx_reply_flag;
( stat_link1_tx_ready                , stat_link2_tx_ready                , stat_link3_tx_ready                , stat_link4_tx_ready                , stat_link5_tx_ready                , stat_link6_tx_ready                , stat_link7_tx_ready                , stat_link8_tx_ready                , stat_link9_tx_ready                , stat_link10_tx_ready               , stat_link11_tx_ready               , stat_link12_tx_ready               ) <= tx_ready;
( stat_link1_rx_empty                , stat_link2_rx_empty                , stat_link3_rx_empty                , stat_link4_rx_empty                , stat_link5_rx_empty                , stat_link6_rx_empty                , stat_link7_rx_empty                , stat_link8_rx_empty                , stat_link9_rx_empty                , stat_link10_rx_empty               , stat_link11_rx_empty               , stat_link12_rx_empty               ) <= rx_empty;
( stat_link1_rx_gbtx_addr            , stat_link2_rx_gbtx_addr            , stat_link3_rx_gbtx_addr            , stat_link4_rx_gbtx_addr            , stat_link5_rx_gbtx_addr            , stat_link6_rx_gbtx_addr            , stat_link7_rx_gbtx_addr            , stat_link8_rx_gbtx_addr            , stat_link9_rx_gbtx_addr            , stat_link10_rx_gbtx_addr           , stat_link11_rx_gbtx_addr           , stat_link12_rx_gbtx_addr           ) <= rx_gbtx_addr;
( stat_link1_rx_reg                  , stat_link2_rx_reg                  , stat_link3_rx_reg                  , stat_link4_rx_reg                  , stat_link5_rx_reg                  , stat_link6_rx_reg                  , stat_link7_rx_reg                  , stat_link8_rx_reg                  , stat_link9_rx_reg                  , stat_link10_rx_reg                 , stat_link11_rx_reg                 , stat_link12_rx_reg                 ) <= rx_reg;
( stat_link1_rx_nb                   , stat_link2_rx_nb                   , stat_link3_rx_nb                   , stat_link4_rx_nb                   , stat_link5_rx_nb                   , stat_link6_rx_nb                   , stat_link7_rx_nb                   , stat_link8_rx_nb                   , stat_link9_rx_nb                   , stat_link10_rx_nb                  , stat_link11_rx_nb                  , stat_link12_rx_nb                  ) <= rx_nb;
( stat_link1_rx_ic_data              , stat_link2_rx_ic_data              , stat_link3_rx_ic_data              , stat_link4_rx_ic_data              , stat_link5_rx_ic_data              , stat_link6_rx_ic_data              , stat_link7_rx_ic_data              , stat_link8_rx_ic_data              , stat_link9_rx_ic_data              , stat_link10_rx_ic_data             , stat_link11_rx_ic_data             , stat_link12_rx_ic_data             ) <= rx_ic_data;
( stat_link1_link_ready              , stat_link2_link_ready              , stat_link3_link_ready              , stat_link4_link_ready              , stat_link5_link_ready              , stat_link6_link_ready              , stat_link7_link_ready              , stat_link8_link_ready              , stat_link9_link_ready              , stat_link10_link_ready             , stat_link11_link_ready             , stat_link12_link_ready             ) <= link_ready;
( stat_link1_tx_aligned              , stat_link2_tx_aligned              , stat_link3_tx_aligned              , stat_link4_tx_aligned              , stat_link5_tx_aligned              , stat_link6_tx_aligned              , stat_link7_tx_aligned              , stat_link8_tx_aligned              , stat_link9_tx_aligned              , stat_link10_tx_aligned             , stat_link11_tx_aligned             , stat_link12_tx_aligned             ) <= tx_aligned;
( stat_link1_rx_gbt_ready            , stat_link2_rx_gbt_ready            , stat_link3_rx_gbt_ready            , stat_link4_rx_gbt_ready            , stat_link5_rx_gbt_ready            , stat_link6_rx_gbt_ready            , stat_link7_rx_gbt_ready            , stat_link8_rx_gbt_ready            , stat_link9_rx_gbt_ready            , stat_link10_rx_gbt_ready           , stat_link11_rx_gbt_ready           , stat_link12_rx_gbt_ready           ) <= rx_gbt_ready;
( stat_link1_rx_gbt_ready_lost_flag  , stat_link2_rx_gbt_ready_lost_flag  , stat_link3_rx_gbt_ready_lost_flag  , stat_link4_rx_gbt_ready_lost_flag  , stat_link5_rx_gbt_ready_lost_flag  , stat_link6_rx_gbt_ready_lost_flag  , stat_link7_rx_gbt_ready_lost_flag  , stat_link8_rx_gbt_ready_lost_flag  , stat_link9_rx_gbt_ready_lost_flag  , stat_link10_rx_gbt_ready_lost_flag , stat_link11_rx_gbt_ready_lost_flag , stat_link12_rx_gbt_ready_lost_flag ) <= rx_gbt_ready_lost_flag;
( stat_link1_rx_data_error_seen      , stat_link2_rx_data_error_seen      , stat_link3_rx_data_error_seen      , stat_link4_rx_data_error_seen      , stat_link5_rx_data_error_seen      , stat_link6_rx_data_error_seen      , stat_link7_rx_data_error_seen      , stat_link8_rx_data_error_seen      , stat_link9_rx_data_error_seen      , stat_link10_rx_data_error_seen     , stat_link11_rx_data_error_seen     , stat_link12_rx_data_error_seen     ) <= rx_data_error_seen;
( stat_link1_rx_match_flag           , stat_link2_rx_match_flag           , stat_link3_rx_match_flag           , stat_link4_rx_match_flag           , stat_link5_rx_match_flag           , stat_link6_rx_match_flag           , stat_link7_rx_match_flag           , stat_link8_rx_match_flag           , stat_link9_rx_match_flag           , stat_link10_rx_match_flag          , stat_link11_rx_match_flag          , stat_link12_rx_match_flag          ) <= rx_match_flag;
( stat_link1_rx_is_data              , stat_link2_rx_is_data              , stat_link3_rx_is_data              , stat_link4_rx_is_data              , stat_link5_rx_is_data              , stat_link6_rx_is_data              , stat_link7_rx_is_data              , stat_link8_rx_is_data              , stat_link9_rx_is_data              , stat_link10_rx_is_data             , stat_link11_rx_is_data             , stat_link12_rx_is_data             ) <= rx_is_data;
( stat_link1_rx_frame_ready          , stat_link2_rx_frame_ready          , stat_link3_rx_frame_ready          , stat_link4_rx_frame_ready          , stat_link5_rx_frame_ready          , stat_link6_rx_frame_ready          , stat_link7_rx_frame_ready          , stat_link8_rx_frame_ready          , stat_link9_rx_frame_ready          , stat_link10_rx_frame_ready         , stat_link11_rx_frame_ready         , stat_link12_rx_frame_ready         ) <= rx_frame_ready;
( stat_link1_no_rxclkout_time        , stat_link2_no_rxclkout_time        , stat_link3_no_rxclkout_time        , stat_link4_no_rxclkout_time        , stat_link5_no_rxclkout_time        , stat_link6_no_rxclkout_time        , stat_link7_no_rxclkout_time        , stat_link8_no_rxclkout_time        , stat_link9_no_rxclkout_time        , stat_link10_no_rxclkout_time       , stat_link11_no_rxclkout_time       , stat_link12_no_rxclkout_time       ) <= no_rxclkout_time;
( stat_link1_no_rxclkout_ctr         , stat_link2_no_rxclkout_ctr         , stat_link3_no_rxclkout_ctr         , stat_link4_no_rxclkout_ctr         , stat_link5_no_rxclkout_ctr         , stat_link6_no_rxclkout_ctr         , stat_link7_no_rxclkout_ctr         , stat_link8_no_rxclkout_ctr         , stat_link9_no_rxclkout_ctr         , stat_link10_no_rxclkout_ctr        , stat_link11_no_rxclkout_ctr        , stat_link12_no_rxclkout_ctr        ) <= no_rxclkout_ctr;


  
--------------------------------------
--------------------------------------
-- IPbus register
--------------------------------------
--------------------------------------

sync_reg: entity work.ipbus_syncreg_v
  generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
  port map(
    clk => ipb_clk,
    rst => ipb_rst,
    ipb_in => ipb_in,
    ipb_out => ipb_out,
    slv_clk => txFrameClk_from_txPll,
    d => csrd,
    q => csrq,
    --rstb => stat_stb,
    stb => ctrl_stb        );

conf <= csrq;
csrd <= stat;

--===============--
-- General reset -- 
--===============--
genRst: entity work.xlx_k7v7_reset
  generic map (
    CLK_FREQ    => 25e3)
  port map (    
    CLK_I       => txFrameClk_from_txPll, 
    RESET1_B_I  => '1', 
    RESET2_B_I  => not resetgbtfpga,
    RESET_O     => reset_from_genRst 
  ); 

txFrameClk_from_txPll   <= clk40;
txFrameClk_from_txPll_o <= txFrameClk_from_txPll;

txFrameClk_lock_from_txPll  <= '1';
txFrameClk_lock             <= txFrameClk_lock_from_txPll;

clkn <= ( mgt119_clkn    , mgt118_clkn      , mgt117_clkn    );
clkp <= ( mgt119_clkp    , mgt118_clkp      , mgt117_clkp    );
rxn  <= ( mgt119_rxn     , mgt118_rxn       , mgt117_rxn     );
rxp  <= ( mgt119_rxp     , mgt118_rxp       , mgt117_rxp     );
        ( mgt119_txn     , mgt118_txn       , mgt117_txn     ) <= txn;
        ( mgt119_txp     , mgt118_txp       , mgt117_txp     ) <= txp;

-- GBT Bank example design --
gbt_gen: for mgt in 117 to 119 generate
  -- MGT(GTX) reference clock:     
  signal mgtRefClk_from_IbufdsGte2                : std_logic;

begin
  
  -- MGT(GTX) reference clock:
  ----------------------------
  -- Comment: * The MGT reference clock MUST be provided by an external clock
  -- generator.
  --          * The MGT reference clock frequency must be 120MHz for the latency
  -- optimized GBT bank. 
  mgtRefClk_ge: ibufds_gte2
  port map (
    O       => mgtRefClk_from_IbufdsGte2,
    ODIV2   => open,
    CEB     => '0',
    I       => clkp(mgt),
    IB      => clkn(mgt)
  );
  
  
  
  gbtExmplDsgn_inst: entity work.xlx_k7v7_gbt_example_design
    generic map(
      NUM_LINKS                                   => NUM_LINK_Conf,            -- Up to 4
      TX_OPTIMIZATION                             => TX_OPTIMIZATION_Conf,     -- LATENCY_OPTIMIZED or STANDARD
      RX_OPTIMIZATION                             => RX_OPTIMIZATION_Conf,     -- LATENCY_OPTIMIZED or STANDARD
      TX_ENCODING                                 => TX_ENCODING_Conf,         -- GBT_FRAME or WIDE_BUS
      RX_ENCODING                                 => RX_ENCODING_Conf,         -- GBT_FRAME or WIDE_BUS
      
      DATA_GENERATOR_ENABLE                       => DATA_GENERATOR_ENABLE_Conf,
      DATA_CHECKER_ENABLE                         => DATA_CHECKER_ENABLE_Conf,
      MATCH_FLAG_ENABLE                           => MATCH_FLAG_ENABLE_Conf,
      CLOCKING_SCHEME                             => CLOCKING_SCHEME_Conf
    )
    port map (
    --==============--
    -- Clocks       --
    --==============--
    FRAMECLK_40MHZ                                => txFrameClk_from_txPll,
    XCVRCLK                                       => mgtRefClk_from_IbufdsGte2,
    
    TX_FRAMECLK_O                                 => txFrameClk_from_gbtExmplDsgn(mgt),
    TX_WORDCLK_O                                  => txWordClk_from_gbtExmplDsgn(mgt),    
    RX_FRAMECLK_O                                 => rxFrameClk_from_gbtExmplDsgn(mgt),
    RX_WORDCLK_O                                  => rxWordClk_from_gbtExmplDsgn(mgt),
  
    --==============--
    -- Reset        --
    --==============--
    GBTBANK_GENERAL_RESET_I                       => reset_from_genRst,
    GBTBANK_MANUAL_RESET_TX_I                     => conf_manualResetTx,
    GBTBANK_MANUAL_RESET_RX_I                     => conf_manualResetRx,
    
    --==============--
    -- Serial lanes --
    --==============--
    GBTBANK_MGT_RX_P                              => rxp(mgt),
    GBTBANK_MGT_RX_N                              => rxn(mgt),
    GBTBANK_MGT_TX_P                              => txp(mgt),
    GBTBANK_MGT_TX_N                              => txn(mgt),
    
    --==============--
    -- Data             --
    --==============--        
    GBTBANK_GBT_DATA_I                            => txData_to_gbtExmplDsgn(mgt),
    GBTBANK_WB_DATA_I                             => (others => (others => '0')) ,
    
    TX_DATA_O                                     => txData_from_gbtExmplDsgn(mgt),            
    WB_DATA_O                                     => open,
    
    GBTBANK_GBT_DATA_O                            => rxData_from_gbtExmplDsgn(mgt),
    GBTBANK_WB_DATA_O                             => open,
    
    --==============--
    -- Reconf.         --
    --==============--
    GBTBANK_MGT_DRP_RST                           => '0',
    GBTBANK_MGT_DRP_CLK                           => clk_fr,
    
    --==============--
    -- TX ctrl        --
    --==============--
    TX_ENCODING_SEL_i                             => (others => '0'),
    GBTBANK_TX_ISDATA_SEL_I                       => txIsData_to_gbtExmplDsgn(mgt),
    GBTBANK_TEST_PATTERN_SEL_I                    => not conf_test_pattern_sel, -- '00' in register 'test_pattern_sel' for real data
    
    --==============--
    -- RX ctrl      --
    --==============--
    RX_ENCODING_SEL_i                             => (others => '0'),
    GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I          => (others => '0'),
    GBTBANK_RESET_DATA_ERRORSEEN_FLAG_I           => (others => '0'),
    GBTBANK_RXFRAMECLK_ALIGNPATTER_I              => (others => '0'),
    GBTBANK_RXBITSLIT_RSTONEVEN_I                 => (others => '0'),
    --==============--
    -- TX Status    --
    --==============--
    GBTBANK_LINK_READY_O                          => link_ready_from_gbtExmplDsgn(mgt),
    GBTBANK_TX_MATCHFLAG_O                        => open,
    GBTBANK_TX_ALIGNED_O                          => txAligned_from_gbtbank(mgt),
    GBTBANK_TX_ALIGNCOMPUTED_O                    => txAlignComputed_from_gbtbank(mgt),
    
    --==============--
    -- RX Status    --
    --==============--
    GBTBANK_GBTRX_READY_O                         => rxReady_from_gbtExmplDsgn(mgt),
    GBTBANK_GBTRXREADY_LOST_FLAG_O                => rxReadyLostFlag_from_gbtExmplDsgn(mgt),
    GBTBANK_RXDATA_ERRORSEEN_FLAG_O               => rxDataErrorSeenFlag_from_gbtExmplDsgn(mgt),
    GBTBANK_RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O  => open,
    GBTBANK_RX_MATCHFLAG_O                        => rxMatchFlag_from_gbtExmplDsgn(mgt),
    GBTBANK_RX_ISDATA_SEL_O                       => rxIsData_from_gbtExmplDsgn(mgt),
    GBTBANK_RX_ERRORDETECTED_O                    => open,
    GBTBANK_RX_BITMODIFIED_FLAG_O                 => open,
    RX_FRAMECLK_RDY_O                             => open,
    GBTBANK_RXBITSLIP_RST_CNT_O                   => open,
    mgt_headerlocked_o                            => headerLocked_from_gbtExmplDsgn(mgt),

    --==============--
    -- XCVR ctrl    --
    --==============--
    GBTBANK_LOOPBACK_I                            => conf_loopback,
    
    GBTBANK_TX_POL                                => (others => '0'),
    GBTBANK_RX_POL                                => (others => '0')
  );

end generate;

  
gen_remap_mgts: for i in 1 to 12 generate

  rxData_1_12_side(i)           <= rxData_from_gbtExmplDsgn               (REMAP_RX(i).mgt)(REMAP_RX(i).ch);
  rx_is_data(i)                 <= rxIsData_from_gbtExmplDsgn             (REMAP_RX(i).mgt)(REMAP_RX(i).ch); 
  header_lock_s(i)              <= headerLocked_from_gbtExmplDsgn         (REMAP_RX(i).mgt)(REMAP_RX(i).ch);
  rxWordClk_s(i)                <= rxWordClk_from_gbtExmplDsgn            (REMAP_RX(i).mgt)(REMAP_RX(i).ch);
  rx_gbt_ready(i)               <= rxReady_from_gbtExmplDsgn              (REMAP_RX(i).mgt)(REMAP_RX(i).ch);
  rx_gbt_ready_lost_flag(i)     <= rxReadyLostFlag_from_gbtExmplDsgn      (REMAP_RX(i).mgt)(REMAP_RX(i).ch);
  rx_data_error_seen(i)         <= rxDataErrorSeenFlag_from_gbtExmplDsgn  (REMAP_RX(i).mgt)(REMAP_RX(i).ch);
  rx_match_flag(i)              <= rxMatchFlag_from_gbtExmplDsgn          (REMAP_RX(i).mgt)(REMAP_RX(i).ch);
  rxFrameClk_1_12_side(i)       <= rxFrameClk_from_gbtExmplDsgn           (REMAP_RX(i).mgt)(REMAP_RX(i).ch);

  link_ready(i)                 <= link_ready_from_gbtExmplDsgn           (REMAP_TX(i).mgt)(REMAP_TX(i).ch);
  txAlignComputed_1_12_side(i)  <= txAlignComputed_from_gbtbank           (REMAP_TX(i).mgt)(REMAP_TX(i).ch);
  txAligned_1_12_side(i)        <= txAligned_from_gbtbank                 (REMAP_TX(i).mgt)(REMAP_TX(i).ch);
  txFrameClk_1_12_side(i)       <= txFrameClk_from_gbtExmplDsgn           (REMAP_TX(i).mgt)(REMAP_TX(i).ch);
  txDataO_1_12_side(i)          <= txData_from_gbtExmplDsgn               (REMAP_TX(i).mgt)(REMAP_TX(i).ch);

  txIsData_to_gbtExmplDsgn  (REMAP_TX(i).mgt)(REMAP_TX(i).ch) <= txIsData(i);
  txData_to_gbtExmplDsgn    (REMAP_TX(i).mgt)(REMAP_TX(i).ch) <= txDataI_1_12_side(i);

end generate;

rxIsData <= rx_is_data;
rxData  <= rxData_1_12_side;
tx_aligned <= txAligned_1_12_side;
rxWordClk <= rxWordClk_s;

gen_rxclk_lock: for i in 1 to 12 generate
  signal rxWordClk_cycle_counter, rxWordClk_cycle_counter_clkxing, cycle_diff : unsigned(3 downto 0);
  signal rxWordClk_locked : std_logic;
  signal tictoc20 : std_logic_vector(4 downto 0); -- for crossing clk domain
begin

  process (rxWordClk_s(i))
    variable rxWordClk_prescale : natural range 2 downto 0;
  begin
    if rising_edge(rxWordClk_s(i)) then
      tictoc20(tictoc20'high downto 1) <= tictoc20(tictoc20'high-1 downto 0);
      if tictoc20(4) /= tictoc20(3) then -- Sync the rx clk counter to make a safe clock domain crossing
        rxWordClk_cycle_counter_clkxing <= rxWordClk_cycle_counter;
      end if;
      
      if rxWordClk_prescale = 2 then
        rxWordClk_cycle_counter <= rxWordClk_cycle_counter + 1;
        rxWordClk_prescale := 0;
      else
        rxWordClk_prescale := rxWordClk_prescale + 1;
      end if;
    end if;
  end process;
  
  process (clk40)
    -- observed that the clock divergence method used takes up to 10 us to unlock...
    -- we force 4096 prescale on 40 MHz, that makes for a 100 us stabilization time
    constant PRESCALE_BITS : natural := 12;
    variable unlockedness, prescale : natural range 2**PRESCALE_BITS-1 downto 0;
    variable lock_pipe : std_logic;
    variable clk40_cycle_counter : unsigned(3 downto 0);
    variable last_unlock_timer : unsigned(31 downto 0);
    variable time_since_diff_last_seen : usgn12_v_t(15 downto 0);
    variable diffs_ctr : natural range 16 downto 0;
  begin
    if rising_edge(clk40) then
      tictoc20(0) <= not tictoc20(0);
    
      cycle_diff <= clk40_cycle_counter - rxWordClk_cycle_counter;
      diffs_ctr := 0;
      for i in 15 downto 0 loop
        if time_since_diff_last_seen(i)(time_since_diff_last_seen(i)'high) = '0' then 
          time_since_diff_last_seen(i) := time_since_diff_last_seen(i) + 1;
          diffs_ctr := diffs_ctr + 1;
        end if;
      end loop;
      time_since_diff_last_seen(to_integer(cycle_diff)) := (others => '0');
      clk40_cycle_counter := clk40_cycle_counter + 1;
      
      if diffs_ctr > 3 then
      
        time_since_diff_last_seen := (others => (others => '1'));
      
        unlockedness := 2**PRESCALE_BITS-1;
        last_unlock_timer := (others => '0');
      elsif unlockedness > 0 then
        unlockedness := unlockedness - 1;
      end if;
      
      last_unlock_timer := last_unlock_timer + bo2int(last_unlock_timer /= (last_unlock_timer'range => '1'));

      rxWordClk_locked <= bo2sl( unlockedness=0 );

      if prescale = 0 then
        if lock_pipe = '1' and rxWordClk_locked = '0' then
          no_rxclkout_ctr(i) <= no_rxclkout_ctr(i) + 1;
        end if;
        lock_pipe := rxWordClk_locked;
        
        if rxWordClk_locked = '0' then
          no_rxclkout_time(i) <= no_rxclkout_time(i) + 1;
        end if;
        
      end if;
      prescale := prescale - 1;
      
    end if;
  end process;

  header_lock(i) <= rxWordClk_locked;

end generate;

gen_debug: for i in 1 to 12 generate

  gen_ila_if: if GENERATE_ILAS(i) = '1' generate
  
    -- * Note!! TX and RX DATA do not share the same ILA module (txIla and rxIla respectively) 
    --   because when receiving RX DATA from another board with a different reference clock, the 
    --   TX_FRAMECLK/TX_WORDCLK domains are asynchronous with respect to the RX_FRAMECLK/RX_WORDCLK domains.        
  
    txILa: xlx_k7v7_vivado_debug
      port map (
        CLK => txFrameClk_1_12_side(i),
        PROBE0 => txDataO_1_12_side(i)(83 downto 82),
        PROBE1 => txDataO_1_12_side(i)(81 downto 80),
        PROBE2 => txDataO_1_12_side(i)(79 downto 0), --8b10b support removed
        PROBE3(0) => txAlignComputed_1_12_side(i),
        PROBE3(1) => txAligned_1_12_side(i) );  
    
    
    rxIla: xlx_k7v7_vivado_debug
      port map (
        CLK => rxFrameClk_1_12_side(i),
        PROBE0 => rxData_1_12_side(i)(83 downto 82), 
        PROBE1 => rxData_1_12_side(i)(81 downto 80), 
        PROBE2 => rxData_1_12_side(i)(79 downto 0), 
        PROBE3(0) => rx_is_data(i),
        PROBE3(1) => headerLocked_from_gbtExmplDsgn(REMAP_RX(i).mgt)(REMAP_RX(i).ch) );

  end generate;

end generate;

-----------------------------------------------------------------------------------
---------------------------------- SLOW CONTROL -----------------------------------
-----------------------------------------------------------------------------------
-- There's a python script in the fw repo (src/tm7/tm7_gbt/addr_table) to automatically generate this following fan-out code for convenience
sc_enable    <= ( conf_link1_sc_enable     , conf_link2_sc_enable     , conf_link3_sc_enable     , conf_link4_sc_enable     , conf_link5_sc_enable     , conf_link6_sc_enable     , conf_link7_sc_enable     , conf_link8_sc_enable     , conf_link9_sc_enable     , conf_link10_sc_enable    , conf_link11_sc_enable    , conf_link12_sc_enable     );
tx_trid      <= ( conf_link1_tx_trid       , conf_link2_tx_trid       , conf_link3_tx_trid       , conf_link4_tx_trid       , conf_link5_tx_trid       , conf_link6_tx_trid       , conf_link7_tx_trid       , conf_link8_tx_trid       , conf_link9_tx_trid       , conf_link10_tx_trid      , conf_link11_tx_trid      , conf_link12_tx_trid       );
tx_ch        <= ( conf_link1_tx_ch         , conf_link2_tx_ch         , conf_link3_tx_ch         , conf_link4_tx_ch         , conf_link5_tx_ch         , conf_link6_tx_ch         , conf_link7_tx_ch         , conf_link8_tx_ch         , conf_link9_tx_ch         , conf_link10_tx_ch        , conf_link11_tx_ch        , conf_link12_tx_ch         );
tx_comm      <= ( conf_link1_tx_comm       , conf_link2_tx_comm       , conf_link3_tx_comm       , conf_link4_tx_comm       , conf_link5_tx_comm       , conf_link6_tx_comm       , conf_link7_tx_comm       , conf_link8_tx_comm       , conf_link9_tx_comm       , conf_link10_tx_comm      , conf_link11_tx_comm      , conf_link12_tx_comm       );
ec_data      <= ( conf_link1_ec_data       , conf_link2_ec_data       , conf_link3_ec_data       , conf_link4_ec_data       , conf_link5_ec_data       , conf_link6_ec_data       , conf_link7_ec_data       , conf_link8_ec_data       , conf_link9_ec_data       , conf_link10_ec_data      , conf_link11_ec_data      , conf_link12_ec_data       );
send_reset   <= ( conf_link1_send_reset    , conf_link2_send_reset    , conf_link3_send_reset    , conf_link4_send_reset    , conf_link5_send_reset    , conf_link6_send_reset    , conf_link7_send_reset    , conf_link8_send_reset    , conf_link9_send_reset    , conf_link10_send_reset   , conf_link11_send_reset   , conf_link12_send_reset    );
send_connect <= ( conf_link1_send_connect  , conf_link2_send_connect  , conf_link3_send_connect  , conf_link4_send_connect  , conf_link5_send_connect  , conf_link6_send_connect  , conf_link7_send_connect  , conf_link8_send_connect  , conf_link9_send_connect  , conf_link10_send_connect , conf_link11_send_connect , conf_link12_send_connect  );
reset_sc     <= ( conf_link1_reset_sc      , conf_link2_reset_sc      , conf_link3_reset_sc      , conf_link4_reset_sc      , conf_link5_reset_sc      , conf_link6_reset_sc      , conf_link7_reset_sc      , conf_link8_reset_sc      , conf_link9_reset_sc      , conf_link10_reset_sc     , conf_link11_reset_sc     , conf_link12_reset_sc      );
gbtx_addr    <= ( conf_link1_gbtx_addr     , conf_link2_gbtx_addr     , conf_link3_gbtx_addr     , conf_link4_gbtx_addr     , conf_link5_gbtx_addr     , conf_link6_gbtx_addr     , conf_link7_gbtx_addr     , conf_link8_gbtx_addr     , conf_link9_gbtx_addr     , conf_link10_gbtx_addr    , conf_link11_gbtx_addr    , conf_link12_gbtx_addr     );
ic_rd_wr     <= ( conf_link1_ic_rd_wr      , conf_link2_ic_rd_wr      , conf_link3_ic_rd_wr      , conf_link4_ic_rd_wr      , conf_link5_ic_rd_wr      , conf_link6_ic_rd_wr      , conf_link7_ic_rd_wr      , conf_link8_ic_rd_wr      , conf_link9_ic_rd_wr      , conf_link10_ic_rd_wr     , conf_link11_ic_rd_wr     , conf_link12_ic_rd_wr      );
read_fifo    <= ( conf_link1_read_fifo     , conf_link2_read_fifo     , conf_link3_read_fifo     , conf_link4_read_fifo     , conf_link5_read_fifo     , conf_link6_read_fifo     , conf_link7_read_fifo     , conf_link8_read_fifo     , conf_link9_read_fifo     , conf_link10_read_fifo    , conf_link11_read_fifo    , conf_link12_read_fifo     );
nb_to_read   <= ( conf_link1_nb_to_read    , conf_link2_nb_to_read    , conf_link3_nb_to_read    , conf_link4_nb_to_read    , conf_link5_nb_to_read    , conf_link6_nb_to_read    , conf_link7_nb_to_read    , conf_link8_nb_to_read    , conf_link9_nb_to_read    , conf_link10_nb_to_read   , conf_link11_nb_to_read   , conf_link12_nb_to_read    );
reg_addr     <= ( conf_link1_reg_addr      , conf_link2_reg_addr      , conf_link3_reg_addr      , conf_link4_reg_addr      , conf_link5_reg_addr      , conf_link6_reg_addr      , conf_link7_reg_addr      , conf_link8_reg_addr      , conf_link9_reg_addr      , conf_link10_reg_addr     , conf_link11_reg_addr     , conf_link12_reg_addr      );
ic_data      <= ( conf_link1_ic_data       , conf_link2_ic_data       , conf_link3_ic_data       , conf_link4_ic_data       , conf_link5_ic_data       , conf_link6_ic_data       , conf_link7_ic_data       , conf_link8_ic_data       , conf_link9_ic_data       , conf_link10_ic_data      , conf_link11_ic_data      , conf_link12_ic_data       );

gen_sc: for i in 1 to 12 generate
  signal reset_reply_flag_s, reset_reply_flag_f1, reset_reply_flag_f2, reset_reply_flag_and : std_logic := '0';
  signal sca_rx_done, start_write, start_read: std_logic;
begin
  -- the stat_reply_flag must be updated with the value of sca_rx_done (asserted after a SCA read/write opertation. 
  -- Also the stat_reply_flag must be reset after it is read.
  -- reset signal syncronization needed between ipbus/gbt clock domains.    
  gen_sc_if: if GENERATE_SC(i) = '1' generate
  
    process(ipb_clk)
    begin   
      if rising_edge(ipb_clk) then
        if ipb_in.ipb_strobe = '1' and ipb_in.ipb_write = '0' and ipb_in.ipb_addr(2 downto 0)= "011" then -- register 3 (was 6), replicated every 8, so width is 3.
          reset_reply_flag_s     <= '1';
        else
          reset_reply_flag_s     <= '0';
        end if;        
      end if;
    end process;
    
    process(txFrameClk_from_txPll)
    begin
      if rising_edge(txFrameClk_from_txPll) then
        reset_reply_flag_f1 <= reset_reply_flag_s;
        reset_reply_flag_f2 <= reset_reply_flag_f1;
        reset_reply_flag_and <= not(reset_reply_flag_f1) and reset_reply_flag_f2;
      end if;
    end process;
    
    process(txFrameClk_from_txPll) is
    begin
      if (rising_edge(txFrameClk_from_txPll)) then
        if reset_reply_flag_and = '1' then
          rx_reply_flag(i) <= '0';
        else
          if sca_rx_done = '1' then
            rx_reply_flag(i) <= '1';
          end if;
        end if;
      end if;
    end process;
    
    -- module for gbtx-sca communication. Inputs/outputs could come from jtag or ipbus interface
                                                                                   
    gbtsc_inst: entity work.gbtsc_top
    generic map(
      -- IC configuration
      g_IC_COUNT          => 1,
      g_IC_FIFO_DEPTH     => 1,
      -- EC configuration
      g_SCA_COUNT         => 1
    )
    port map(
      -- Clock & reset
      tx_clk_i                => txFrameClk_from_txPll,
      rx_clk_i                => txFrameClk_from_txPll,
      reset_i                 => reset_sc(i),
      
      -- IC configuration        
      tx_GBTx_address_i(0)    => gbtx_addr(i),
      tx_register_addr_i(0)   => reg_addr(i),
      tx_nb_to_be_read_i(0)   => nb_to_read(i),
      
      -- IC Status
      tx_ready_o(0)           => tx_ready(i),
      rx_empty_o(0)           => rx_empty(i),
      
      rx_gbtx_addr_o(0)       => rx_gbtx_addr(i),
      rx_mem_ptr_o(0)         => rx_reg(i),
      rx_nb_of_words_o(0)     => rx_nb(i),
          
      -- IC FIFO control
      tx_wr_i(0)              => ctrl_stb(8*i+6),
      tx_data_to_gbtx_i(0)    => ic_data(i),
      
      rx_rd_i(0)              => read_fifo(i),
      rx_data_from_gbtx_o(0)  => rx_ic_data(i),
      
      -- IC control
      ic_enable_i             => "1",
      tx_start_write_i        => start_write ,
      tx_start_read_i         => start_read  ,
          
      -- SCA control
      sca_enable_i            => "1",
      start_reset_cmd_i       => send_reset(i),
      start_connect_cmd_i     => send_connect(i),
      start_command_i         => ctrl_stb(i*8+2),
      inject_crc_error        => '0',
      
      -- SCA command
      tx_address_i            => x"00",
      tx_transID_i            => tx_trid(i),
      tx_channel_i            => tx_ch(i),
      tx_command_i            => tx_comm(i),
      tx_data_i               => ec_data(i),
      
      rx_received_o(0)        => sca_rx_done,
      rx_address_o(0)         => open,
      rx_control_o(0)         => open,
      rx_transID_o(0)         => rx_trid(i),
      rx_channel_o(0)         => rx_ch(i),
      rx_len_o(0)             => rx_len(i),
      rx_error_o(0)           => rx_error(i),
      rx_data_o(0)            => rx_ec_data(i),
    
      -- EC line
      ec_data_o(0)            => txData_slow_control(i)(1 downto 0),
      ec_data_i(0)            => rxData_1_12_side(i)(81 downto 80),
      
      -- IC lines
      ic_data_o(0)            => txData_slow_control(i)(3 downto 2),
      ic_data_i(0)            => rxData_1_12_side(i)(83 downto 82)
    );
    
    start_write <=  ctrl_stb(8*i+5)  and ic_rd_wr(i);
    start_read  <=  ctrl_stb(8*i+5)  and not ic_rd_wr(i);    
  
  end generate;
  
  txDataI_1_12_side(i)(83 downto 80) <= txData_slow_control(i) when sc_enable(i) = '1' else txData(i)(83 downto 80);  
  txDataI_1_12_side(i)(79 downto 0) <= txData(i)(79 downto 0);  
  
end generate;


   
end architecture;