-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Memory components
---- Design unit: Memory package
---- File       : memory_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2012-06
-------------------------------------------------------------------------------
---- Description: Memory/FIFO related packages
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package memory_pkg is

  ---------------------------------------------------------------
  -- Data types
  ---------------------------------------------------------------
  
  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  -- RAM simple
  component RAM is
    -- DATAWIDTH units -> bits // DEPTH units -> num. words
    generic (DATAWIDTH : integer := 32; DEPTH : integer := 32);
    port (
      clk  : in  std_logic;
      rst  : in  std_logic;
      ena  : in  std_logic;
      wrt  : in  std_logic;
      add  : in  std_logic_vector(getBusLines(DEPTH) - 1 downto 0);
      dIn  : in  std_logic_vector(DATAWIDTH - 1 downto 0);
      dOut : out std_logic_vector(DATAWIDTH - 1 downto 0)
    );
  end component;

  -- RAM de doble puerto
  component RAM_DUALPORT is
  -- DATAWIDTH units -> bits // DEPTH -> num. words
  generic (DATAWIDTH : integer := 32; DEPTH : integer := 32);
  port (
    -- Port A
    clk_a  : in  std_logic;
    rst_a  : in  std_logic;
    ena_a  : in  std_logic;
    wrt_a  : in  std_logic;
    add_a  : in  std_logic_vector(getBusLines(DEPTH) - 1 downto 0);
    dIn_a  : in  std_logic_vector(DATAWIDTH - 1 downto 0);
    dOut_a : out std_logic_vector(DATAWIDTH - 1 downto 0);
    --
    -- Port B
    clk_b  : in  std_logic;
    rst_b  : in  std_logic;
    ena_b  : in  std_logic;
    wrt_b  : in  std_logic;
    add_b  : in  std_logic_vector(getBusLines(DEPTH) - 1 downto 0);
    dIn_b  : in  std_logic_vector(DATAWIDTH - 1 downto 0);
    dOut_b : out std_logic_vector(DATAWIDTH - 1 downto 0)
  );
  end component;

  -- FIFO con un solo CLK
  component FIFO is
  -- DATAWIDTH units -> bits // DEPTH units -> num. words
  generic (DATAWIDTH : integer := 32; DEPTH : integer := 32);
  port (
    -- Clock and resets
    rst     : in  std_logic;
    clk     : in  std_logic;
    -- Control and data signals
    rdEna   : in  std_logic;
    wrEna   : in  std_logic;
    dataIn  : in  std_logic_vector(DATAWIDTH - 1 downto 0);
    dataOut : out std_logic_vector(DATAWIDTH - 1 downto 0);
    empty   : out std_logic;
    full    : out std_logic
  );
  end component;

  component FIFO_FALLTHROUGH is
  -- DATAWIDTH units -> bits // DEPTH units -> num. words
  generic (DATAWIDTH : integer := 32; DEPTH : integer := 32);
  port (
    -- Clock and resets
    rst     : in  std_logic;
    clk     : in  std_logic;
    -- Control and data signals
    rdEna   : in  std_logic;
    wrEna   : in  std_logic;
    dataIn  : in  std_logic_vector(DATAWIDTH - 1 downto 0);
    dataOut : out std_logic_vector(DATAWIDTH - 1 downto 0);
    empty   : out std_logic;
    full    : out std_logic
  );
  end component;
  
  -- FIFO con CLK de lectura y escritura separadas
  component FIFO_DUALPORT is
  -- DATAWIDTH units -> bits // DEPTH units -> num. words
  generic (DATAWIDTH : integer := 32; DEPTH : integer := 32);
  port (
    -- Clock and resets
    rst     : in  std_logic;
    rdClk   : in  std_logic;
    wrClk   : in  std_logic;
    -- Control and data signals
    rdEna   : in  std_logic;
    wrEna   : in  std_logic;
    dataIn  : in  std_logic_vector(DATAWIDTH - 1 downto 0);
    dataOut : out std_logic_vector(DATAWIDTH - 1 downto 0);
    empty   : out std_logic;
    full    : out std_logic
  );
  end component;
  
  component DUMMY_FIFO_FFT is
    generic (DATA_SIZE : integer := 16);
    port(
      clk     : in  std_logic;
      rst     : in  std_logic;
      rdEna   : in  std_logic;
      wrEna   : in  std_logic;
      dataIn  : in  std_logic_vector(DATA_SIZE - 1 downto 0);
      dataOut : out std_logic_vector(DATA_SIZE - 1 downto 0);
      empty   : out std_logic;
      full    : out std_logic
    );
  end component;

  component DUMMY_FIFO_FFT2 is
    generic (
      DATA_SIZE : integer := 16; 
      NUM_ITEMS : integer := 8      -- Greater or equal (and power of) 2.
    );
    port(
      clk     : in  std_logic;
      rst     : in  std_logic;
      rdEna   : in  std_logic;
      wrEna   : in  std_logic;
      dataIn  : in  std_logic_vector(DATA_SIZE - 1 downto 0);
      dataOut : out std_logic_vector(DATA_SIZE - 1 downto 0);
      empty   : out std_logic;
      full    : out std_logic
    );
  end component;

end package;
