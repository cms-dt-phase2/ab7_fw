-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Buffers
---- File       : dt_buffers_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-03
-------------------------------------------------------------------------------
---- Description: Package for components/defined types of different data 
----              buffers
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_constants_pkg.all;
use work.dt_common_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package dt_buffers_pkg is

  ---------------------------------------------------------------
  -- Data types
  ---------------------------------------------------------------
  type BXTimeConvIn_t is record
    rawHit               : DTRawHit_t;
    wrEna                : std_logic;
    rawHitsFF_full       : std_logic;
    rawHitsFFalmost_full : std_logic;
    bx_time_convert_ena  : std_logic;
  end record;
  
  type BXTimeConvOut_t is record
    rawHit       : DTRawHit_t;
    full         : std_logic;
    rawHitsFF_wr : std_logic;
  end record;

  type DTRawHitFifoIn_t is record
    rawHit              : DTRawHit_t;
    wrEna               : std_logic;
    rdEna               : std_logic;
    bx_time_convert_ena : std_logic;
  end record;

  type DTRawHitFifoOut_t is record
    rawHit : DTRawHit_t;
    full   : std_logic;
    empty  : std_logic;
    stat   : DTFifoStat_t;
  end record;

  type DTHitFifoIn_t is record
    hit   : DTHit_t;
    wrEna : std_logic;
    rdEna : std_logic;
  end record;

  type DTHitFifoOut_t is record
    hit   : DTHit_t;
    full  : std_logic;
    empty : std_logic;
  end record;

  type DTSegCandidateFifoIn_t is record
    segment_candidate : DTSegCandidate_t;
    wrEna             : std_logic;
    rdEna             : std_logic;
  end record;

  type DTSegCandidateFifoOut_t is record
    segment_candidate : DTSegCandidate_t;
    full              : std_logic;
    empty             : std_logic;
    stat              : DTFifoStat_t;
  end record;
  
  type DTSegMainParamsFifoIn_t is record
    segment_main : DTSegMainParams_t;
    wrEna        : std_logic;
    rdEna        : std_logic;
  end record;

  type DTSegMainParamsFifoOut_t is record
    segment_main : DTSegMainParams_t;
    full         : std_logic;
    empty        : std_logic;
    stat         : DTFifoStat_t;
  end record;
  
  type DTSegmentFifoIn_t is record
    segment : DTSegment_t;
    wrEna   : std_logic;
    rdEna   : std_logic;
  end record;

  type DTSegmentFifoOut_t is record
    segment : DTSegment_t;
    full    : std_logic;
    empty   : std_logic;
    stat    : DTFifoStat_t;
  end record;

  type DTPrimitiveFifoIn_t is record
    primitive : DTPrimitive_t;
    wrEna     : std_logic;
    rdEna     : std_logic;
  end record;

  type DTPrimitiveFifoOut_t is record
    primitive : DTPrimitive_t;
    full      : std_logic;
    empty     : std_logic;
    stat      : DTFifoStat_t;
  end record;

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component BX_TIME_CONVERTER is
  port (
    clk           : in  std_logic;
    rst           : in  std_logic;
    bxTimeConvIn  : in  BXTimeConvIn_t;
    bxTimeConvOut : out BXTimeConvOut_t
  );
  end component;

  component DTRAWHIT_FIFO is
  port (
    clkRd   : in  std_logic;
    clkWr   : in  std_logic;
    rst     : in  std_logic;
    fifoIn  : in  DTRawHitFifoIn_t;
    fifoOut : out DTRawHitFifoOut_t
  );
  end component;

  component DTRAWHIT_GLUE_FIFO is
  port (
    clk     : in  std_logic;
    rst     : in  std_logic;
    fifoIn  : in  DTRawHitFifoIn_t;
    fifoOut : out DTRawHitFifoOut_t
  );
  end component;
  
  component DTHIT_FIFO is
  port (
    clkRd   : in  std_logic;
    clkWr   : in  std_logic;
    rst     : in  std_logic;
    fifoIn  : in  DTHitFifoIn_t;
    fifoOut : out DTHitFifoOut_t
  );
  end component;

  component DTSEGCANDIDATE_FIFO is
  port (
    clkRd   : in  std_logic;
    clkWr   : in  std_logic;
    rst     : in  std_logic;
    fifoIn  : in  DTSegCandidateFifoIn_t;
    fifoOut : out DTSegCandidateFifoOut_t
  );
  end component;

  component DTSEGCANDIDATE_GLUE_FIFO is
  port (
    clk     : in  std_logic;
    rst     : in  std_logic;
    fifoIn  : in  DTSegCandidateFifoIn_t;
    fifoOut : out DTSegCandidateFifoOut_t
  );
  end component;
  
  component DTSEGMAINPARAM_FIFO is
  port (
    clkRd   : in  std_logic;
    clkWr   : in  std_logic;
    rst     : in  std_logic;
    fifoIn  : in  DTSegMainParamsFifoIn_t;
    fifoOut : out DTSegMainParamsFifoOut_t
  );
  end component;

  component DTSEGMENT_FIFO is
  port (
    clkRd   : in  std_logic;
    clkWr   : in  std_logic;
    rst     : in  std_logic;
    fifoIn  : in  DTSegmentFifoIn_t;
    fifoOut : out DTSegmentFifoOut_t
  );
  end component;

  component DTSEGMENT_GLUE_FIFO is
  port (
    clk     : in  std_logic;
    rst     : in  std_logic;
    fifoIn  : in  DTSegmentFifoIn_t;
    fifoOut : out DTSegmentFifoOut_t
  );
  end component;

  component DTPRIMITIVE_FIFO is
    port (
      clkRd   : in  std_logic;
      clkWr   : in  std_logic;
      rst     : in  std_logic;
      fifoIn  : in  DTPrimitiveFifoIn_t;
      fifoOut : out DTPrimitiveFifoOut_t
    );
  end component;
  
  component DTPRIMITIVE_GLUE_FIFO is
  port (
    clk     : in  std_logic;
    rst     : in  std_logic;
    fifoIn  : in  DTPrimitiveFifoIn_t;
    fifoOut : out DTPrimitiveFifoOut_t
  );
  end component;
  
  component DTSEGMENT_MATCHER_MEMORY is
    port (
      rst       : in  std_logic;
      clk_wr    : in  std_logic;
      ena_wr    : in  std_logic;
      add_wr    : in  std_logic_vector(9 downto 0);
      segmentIn : in  DTSegment_t;
      --
      clk_rd     : in  std_logic;
      ena_rd     : in  std_logic;
      add_rd     : in  std_logic_vector(9 downto 0);
      segmentOut : out DTSegment_t
    );
  end component;
  
  ---------------------------------------------------------------
  -- Functions
  ---------------------------------------------------------------
  function ns_time_from_bx_fine(bx_fine : unsigned(4 downto 0))
    return unsigned;
  
end package;

-------------------------------------------------------------------------------
---- Body
-------------------------------------------------------------------------------
package body dt_buffers_pkg is

  function ns_time_from_bx_fine(bx_fine : unsigned(4 downto 0))
  return unsigned
  is
    variable bx_fine_time : unsigned(4 downto 0);
  begin
    case bx_fine is
      when "00000" => bx_fine_time := to_unsigned( 0, 5);
      when "00001" => bx_fine_time := to_unsigned( 1, 5);
      when "00010" => bx_fine_time := to_unsigned( 2, 5);
      when "00011" => bx_fine_time := to_unsigned( 3, 5);
      when "00100" => bx_fine_time := to_unsigned( 3, 5);
      when "00101" => bx_fine_time := to_unsigned( 4, 5);
      when "00110" => bx_fine_time := to_unsigned( 5, 5);
      when "00111" => bx_fine_time := to_unsigned( 6, 5);
      when "01000" => bx_fine_time := to_unsigned( 7, 5);
      when "01001" => bx_fine_time := to_unsigned( 8, 5);
      when "01010" => bx_fine_time := to_unsigned( 8, 5);
      when "01011" => bx_fine_time := to_unsigned( 9, 5);
      when "01100" => bx_fine_time := to_unsigned(10, 5);
      when "01101" => bx_fine_time := to_unsigned(11, 5);
      when "01110" => bx_fine_time := to_unsigned(12, 5);
      when "01111" => bx_fine_time := to_unsigned(13, 5);
      --
      when "10000" => bx_fine_time := to_unsigned(13, 5);
      when "10001" => bx_fine_time := to_unsigned(14, 5);
      when "10010" => bx_fine_time := to_unsigned(15, 5);
      when "10011" => bx_fine_time := to_unsigned(16, 5);
      when "10100" => bx_fine_time := to_unsigned(17, 5);
      when "10101" => bx_fine_time := to_unsigned(18, 5);
      when "10110" => bx_fine_time := to_unsigned(18, 5);
      when "10111" => bx_fine_time := to_unsigned(19, 5);
      when "11000" => bx_fine_time := to_unsigned(20, 5);
      when "11001" => bx_fine_time := to_unsigned(21, 5);
      when "11010" => bx_fine_time := to_unsigned(22, 5);
      when "11011" => bx_fine_time := to_unsigned(23, 5);
      when "11100" => bx_fine_time := to_unsigned(23, 5);
      when "11101" => bx_fine_time := to_unsigned(24, 5);
      when "11110" => bx_fine_time := to_unsigned(25, 5);
      when "11111" => bx_fine_time := to_unsigned(25, 5);
      --
      when others  => bx_fine_time := "XXXXX";
    end case;

    return bx_fine_time;
  end function;

end package body;
