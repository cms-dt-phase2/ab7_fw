# uhal
import uhal
# sleep
from time import sleep
# os to get folder
import os

## fc7 interface class
class fc7_interface:
	# init class
	def __init__(self, *args, **kwargs):
		if len(args) == 1:
			self.uri = agrs[0]
		elif len(args) == 2 and args[0] == "udp":
			self.uri = "ipbusudp-2.0://" + args[1] + ":50001"
		elif len(args) == 3 and args[0] == "tcp":
			self.uri = "chtcp-2.0://" + args[1] + ":10203?target=" + args[2] + ":50001"
		else:
			self.uri = None
			return -1
		# set log level
		uhal.setLogLevelTo(uhal.LogLevel.ERROR)
		# assign vars
		self.name = "fc7"
		self.address_table = "file://../addr_table/fc7_user_address_table.xml"
		# init hardware
		self.hw = uhal.getDevice(self.name, self.uri, self.address_table)
	
	# simple read
	def read(self, reg_name):
		# read the value
		reg = self.hw.getNode(reg_name).read()
		# transaction
		self.hw.dispatch()
		# return
		return reg.value() 

	# simple write
	def write(self, reg_name, value):
		# write the value
		self.hw.getNode(reg_name).write(value)
		# transaction
		self.hw.dispatch()

	# read block
	def readBlock(self, reg_name, size):
		# read the value
		reg = self.hw.getNode(reg_name).readBlock(size)
		# transaction
		self.hw.dispatch()
		# return
		return reg.value() 

	# write block
	def writeBlock(self, reg_name, data):
		# read the value
		reg = self.hw.getNode(reg_name).writeBlock(data)
		# transaction
		self.hw.dispatch()

## get the 
connection_file = open("fc7_connection.dat", "r+")
lines = connection_file.readlines()
protocol = None
controlhub = None
board_ip = None
for line in lines:
	name, value = line.split()[0].split("=")
	if name == "protocol":
		protocol = value
	elif name == "controlhub":
		controlhub = value
	elif name == "board_ip":
		board_ip = value

if protocol and board_ip:
	if protocol == "udp":
		fc7 = fc7_interface("udp",board_ip)
	elif protocol == "tcp":
		if controlhub:
			fc7 = fc7_interface("tcp",controlhub,board_ip)
		else:
			print("ERROR No controlhub address was found")
	else:
		print("ERROR Wrong protocol name")
else:
	print("ERROR No protocol or board_ip description were found")


