-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Memory components
---- Design unit: Dual port FIFO wrapper for Xilinx Macros
---- File       : xil_fifo_generic_wrapper.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-08
-------------------------------------------------------------------------------
---- Description: A generic FIFO for two clock domains. 
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
--
library UNISIM;
use UNISIM.vcomponents.all;
-- 
library UNIMACRO;
use UNIMACRO.vcomponents.all;
--
use work.commonfunc_pkg.all;
use work.xil_memory_pkg.all;

entity XIL_FIFO_GENERIC_WRAPPER is
  generic (
    -- Options: "VIRTEX5, "VIRTEX6", "7SERIES"
    DEVICE                  : string := "7SERIES";
    -- Sets almost int_full threshold
    ALMOST_FULL_OFFSET      : bit_vector(15 downto 0):= X"0080";
    -- Sets the almost int_empty threshold     
    ALMOST_EMPTY_OFFSET     : bit_vector(15 downto 0):= X"0080";
    -- DATAWIDTH units -> bits
    DATAWIDTH               : integer := 32;
     -- DEPTH units -> num. words (512, 1024, 2048, 4096, 8192)
    DEPTH                   : integer := 512;
    FIRST_WORD_FALL_THROUGH : boolean := TRUE
  );
  port (
    -- Clock and resets
    rst          : in  std_logic;
    rdClk        : in  std_logic;
    wrClk        : in  std_logic;
    -- Control and data input signals
    rdEna        : in  std_logic;
    wrEna        : in  std_logic;
    data_in      : in  std_logic_vector(DATAWIDTH - 1 downto 0);
    -- Output signals.
    data_out     : out std_logic_vector(DATAWIDTH - 1 downto 0);
    rd_count     : out std_logic_vector(getBusLines(DEPTH) - 1 downto 0);
    wr_count     : out std_logic_vector(getBusLines(DEPTH) - 1 downto 0);
    almost_empty : out std_logic;
    almost_full  : out std_logic;
    empty        : out std_logic;
    full         : out std_logic
  );
end entity;

architecture RTL of XIL_FIFO_GENERIC_WRAPPER is

  constant NUM_INST   : integer := get_fifo_num_inst  (DEPTH, DATAWIDTH);
  constant DATAW_MAIN : integer := get_fifo_dataw_main(DEPTH, DATAWIDTH);
  constant DATAW_LAST : integer := get_fifo_dataw_last(DEPTH, DATAWIDTH);
  constant SIZE_MAIN  : string  := get_fifo_size_main (DEPTH, DATAWIDTH);
  constant SIZE_LAST  : string  := get_fifo_size_last (DEPTH, DATAWIDTH);

  type count_arr_t is array (NUM_INST - 1 downto 0)
                   of std_logic_vector(getBusLines(DEPTH) - 1 downto 0);
                 
  signal int_rdcount : count_arr_t;
  signal int_wrcount : count_arr_t;
  --
  signal dOut : std_logic_vector(DATAWIDTH - 1 downto 0);
  signal dIn  : std_logic_vector(DATAWIDTH - 1 downto 0);
  --
  signal int_empty     : std_logic_vector(NUM_INST-1 downto 0):=(others => '1');
  signal int_alm_empty : std_logic_vector(NUM_INST-1 downto 0):=(others => '1');
  signal int_full      : std_logic_vector(NUM_INST-1 downto 0):=(others => '0');
  signal int_alm_full  : std_logic_vector(NUM_INST-1 downto 0):=(others => '0');

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  empty        <= int_empty     (0);
  almost_empty <= int_alm_empty (0);
  full         <= int_full      (0);
  almost_full  <= int_alm_full  (0);
  rd_count     <= int_rdcount   (0);
  wr_count     <= int_wrcount   (0);

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  MAIN_BLOCK : if NUM_INST >= 2 generate
    FF_MULTI_GENERATE : for i in 0 to NUM_INST - 2 generate
      FF0 : FIFO_DUALCLOCK_MACRO
      generic map (
        DEVICE                  => DEVICE,
        ALMOST_FULL_OFFSET      => ALMOST_FULL_OFFSET,   
        ALMOST_EMPTY_OFFSET     => ALMOST_EMPTY_OFFSET,   
        DATA_WIDTH              => DATAW_MAIN,
        FIFO_SIZE               => SIZE_MAIN,
        FIRST_WORD_FALL_THROUGH => FIRST_WORD_FALL_THROUGH
      )
      port map (
        ALMOSTEMPTY => int_alm_empty(i),
        ALMOSTFULL  => int_alm_full(i), 
        DO          => data_out((DATAW_MAIN*(i+1)) - 1 downto DATAW_MAIN * i),
        EMPTY       => int_empty(i),       
        FULL        => int_full(i),        
        RDCOUNT     => int_rdcount(i),    
        RDERR       => open,        
        WRCOUNT     => int_wrcount(i),    
        WRERR       => open,        
        DI          => data_in((DATAW_MAIN*(i+1)) - 1 downto DATAW_MAIN * i),
        RDCLK       => rdClk,       
        RDEN        => rdEna,       
        RST         => rst,         
        WRCLK       => wrClk,       
        WREN        => wrEna        
      );
    end generate;
  end generate;

  LAST_FF : FIFO_DUALCLOCK_MACRO
  generic map (
    DEVICE                  => DEVICE,
    ALMOST_FULL_OFFSET      => ALMOST_FULL_OFFSET,   
    ALMOST_EMPTY_OFFSET     => ALMOST_EMPTY_OFFSET,   
    DATA_WIDTH              => DATAW_LAST,
    FIFO_SIZE               => SIZE_LAST, 
    FIRST_WORD_FALL_THROUGH => FIRST_WORD_FALL_THROUGH)
  port map (
    ALMOSTEMPTY => int_alm_empty(NUM_INST - 1),
    ALMOSTFULL  => int_alm_full(NUM_INST - 1), 
    DO          => data_out(DATAWIDTH - 1 downto DATAW_MAIN * (NUM_INST - 1)),
    EMPTY       => int_empty(NUM_INST - 1),
    FULL        => int_full(NUM_INST - 1), 
    RDCOUNT     => int_rdcount(NUM_INST - 1),
    RDERR       => open,     
    WRCOUNT     => int_wrcount(NUM_INST - 1), 
    WRERR       => open,     
    DI          => data_in(DATAWIDTH - 1 downto DATAW_MAIN * (NUM_INST - 1)),
    RDCLK       => rdClk,    
    RDEN        => rdEna,    
    RST         => rst,      
    WRCLK       => wrClk,    
    WREN        => wrEna     
  );
  
end architecture;
