-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Splitters
---- File       : splitters_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2020-01
-------------------------------------------------------------------------------
---- Description: Package for components/defined types of different splitters
----              units
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
use work.dt_common_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package splitters_pkg is

  ---------------------------------------------------------------
  -- Data types
  ---------------------------------------------------------------
  type CandidateSplitterIn_t is record
    segment_candidate : DTSegCandidate_t;
    fifoIn_empty      : std_logic;
    fifoOut_full      : std_logic;
  end record;

  type CandidateSplitterOut_t is record
    segment_candidate : DTSegCandidate_t;
    fifoIn_rd         : std_logic;
    fifoOut_wr        : std_logic;
  end record;

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component CANDIDATE_SPLITTER is
  port (
    rst             : in  std_logic;
    clk             : in  std_logic;
    candSplitterIn  : in  CandidateSplitterIn_t;
    candSplitterOut : out CandidateSplitterOut_t
  );
  end component;

end package;
