-- ipbus_ram_fabric_seqam
--
-- Generic memory that is accessed as a RAM by ipbus, but as a sequential access memory from FPGA fabric
-- The data width for ipbus is 32, but fabric 
-- Should lead to an inferred block RAM in Xilinx parts with modern tools

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.ipbus_reg_types.all;

entity ipbus_ram_fabric_seqam is
  generic(
    IPBUS_ADDR_WIDTH: natural;
    FABRIC_RATIO_LOG2: natural
  );
  port(
    ipb_clk: in std_logic;
    ipb_rst: in std_logic;
    ipb_in: in ipb_wbus;
    ipb_out: out ipb_rbus;
    fabric_clk: in std_logic;
    we: in std_logic := '0';
    advance: in std_logic := '0';
    zeroaddr : in std_logic := '0';
    d: in std_logic_vector(32*(2**FABRIC_RATIO_LOG2)-1 downto 0) := (others => '0');
    q: out std_logic_vector(32*(2**FABRIC_RATIO_LOG2)-1 downto 0)
  );
  
end entity;

architecture rtl of ipbus_ram_fabric_seqam is

  signal ipb_rdata : ipb_reg_v(2**FABRIC_RATIO_LOG2-1 downto 0);
  signal ack : std_logic_vector(2**FABRIC_RATIO_LOG2-1 downto 0);

begin

  block_gen: for sel_block in 0 to 2 ** FABRIC_RATIO_LOG2 - 1 generate
    type ram_array is array(2 ** (IPBUS_ADDR_WIDTH-FABRIC_RATIO_LOG2) - 1 downto 0) of std_logic_vector(31 downto 0);
    shared variable ram: ram_array;
  begin

    process(ipb_clk)
      variable sel_addr : integer range 0 to 2 ** (IPBUS_ADDR_WIDTH-FABRIC_RATIO_LOG2) - 1;
    begin
      if rising_edge(ipb_clk) then
        sel_addr := to_integer(unsigned(ipb_in.ipb_addr(IPBUS_ADDR_WIDTH - 1 downto FABRIC_RATIO_LOG2)));
        
        ipb_rdata(sel_block) <= ram(sel_addr); -- Order of statements is important to infer read-first RAM!
        if ipb_in.ipb_strobe='1' and ipb_in.ipb_write='1' then
          if (FABRIC_RATIO_LOG2 = 0) or  (to_integer(unsigned(ipb_in.ipb_addr(FABRIC_RATIO_LOG2 - 1 downto 0))) = sel_block) then
            ram(sel_addr) := ipb_in.ipb_wdata;
          end if;
        end if;
        ack(sel_block) <= ipb_in.ipb_strobe and not ack(sel_block);
      end if;
    end process;
    
    process(fabric_clk)
      variable addr : unsigned( IPBUS_ADDR_WIDTH-FABRIC_RATIO_LOG2 downto 0 );
      variable sel_addr : integer range 0 to 2 ** (IPBUS_ADDR_WIDTH-FABRIC_RATIO_LOG2) - 1;
    begin
      if rising_edge(fabric_clk) then
        
        if zeroaddr = '1' then      addr := (others => '0');
        elsif advance = '1' then    addr := addr + 1;
        end if;
        sel_addr := to_integer(addr);
  
        q(32*(sel_block+1)-1 downto 32*sel_block) <= ram(sel_addr); -- Order of statements is important to infer read-first RAM!
        if we = '1' then
          ram(sel_addr) := d(32*(sel_block+1)-1 downto 32*sel_block);
        end if;
  
      end if;
    end process;

  end generate;

  ipb_out.ipb_ack   <=        ack(to_integer(unsigned(ipb_in.ipb_addr(FABRIC_RATIO_LOG2 - 1 downto 0)))) when FABRIC_RATIO_LOG2 /= 0 else ack(0);
  ipb_out.ipb_rdata <=  ipb_rdata(to_integer(unsigned(ipb_in.ipb_addr(FABRIC_RATIO_LOG2 - 1 downto 0)))) when FABRIC_RATIO_LOG2 /= 0 else ipb_rdata(0);
  ipb_out.ipb_err <= '0';

end architecture;
