-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Primitive builder 
---- File       : path_param_calc_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-05
-------------------------------------------------------------------------------
---- Description: Package for components/defined types for calculations on
----              primitive path parameters
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package primitive_calc_modules_pkg is

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component CALC_PRIMITIVE_TAN_POS_BX_TIME is
  port (
    clk                   : in  std_logic;
    rst                   : in  std_logic;
    enable                : in  std_logic;
    paired_segments       : in  DTSegment_arr_t;
    sl_phi3_x_offset      : in  signed(9 downto 0);
    bx_time_chamber       : out std_logic_vector(16 downto 0);
    x_coor_chamber        : out SignedDistance_t;
    tan_phi_chamber_x4096 : out signed(14 downto 0)
  );
  end component;

  component CALC_PRIMITIVE_CHI_SQUARE is
    port (
      clk                   : in  std_logic;
      rst                   : in  std_logic;
      enable                : in  std_logic;
      paired_segments       : in  DTSegment_arr_t;
      x_coor_chamber        : in  SignedDistance_t;
      sl_phi3_x_offset      : in  signed(9 downto 0);
      tan_phi_chamber_x4096 : in  signed(14 downto 0);
      chi_square_chamber    : out unsigned(23 downto 0)
    );
  end component;
  
end package;

