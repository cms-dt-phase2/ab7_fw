-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Super layers phi segment matcher
---- File       : sequential_prior_enc_32_5.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-06
-------------------------------------------------------------------------------
---- Description: Sequential priority encoder 32:5
----
----              It is able to jump from one index position (with its value
----              equal to '1') to the next one, each time the "next_item"
----              signal is pulsed HIGH.
----
----              Using the BYPASS_INPUT_REG configuration, it allows to NOT 
----              store (bypass) input data in its internal registers which 
----              it's useful in some cases where calling  module has a 
-----             previous registration step. This way it saves the first clock 
----              cycle, with 'load' signal to HIGH, needed to store the 
----              values.
----
----              To pulse "rst" or "load" is mandatory after changing the input
----              values ("data"), even it were configure with BYPASS_INPUT_REG 
----              to 'true' because internal selector resgister must be set 
----              to their initial default values.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.commonfunc_pkg.all;
use work.mixer_pkg.all;

entity SEQUENTIAL_PRIOR_ENC_32_5 is
  generic (BYPASS_INPUT_REG : boolean := false);
  port (
    clk       : in  std_logic;
    rst       : in  std_logic;
    load      : in  std_logic;
    next_item : in  std_logic;
    data      : in  std_logic_vector(31 downto 0);
    -- Registered data after "load".
    data_out  : out std_logic_vector(31 downto 0);
    valid     : out std_logic;
    code      : out std_logic_vector(4 downto 0)
  );
end entity;

architecture Behavioural of SEQUENTIAL_PRIOR_ENC_32_5 is

  signal valid_reg        : std_logic;
  signal code_reg         : std_logic_vector(4 downto 0);
  signal data_reg         : std_logic_vector(31 downto 0);
  signal mux_selector_reg : std_logic_vector(31 downto 0);
  --
  -- This signal has an extra bit to allow adding an "invalid" code when its
  -- value is all '1'. So only bits from 0 to 4 will be used as real code.
  --
  signal code_aux     : std_logic_vector(5 downto 0);
  signal bypass_muxer : std_logic_vector(31 downto 0);
  signal value_muxer  : std_logic_vector(31 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals 
  --
  data_out <= data_reg;   -- Useful as an ordinary value registration
  valid    <= valid_reg;
  code     <= code_reg;
  
  ---------------------------------------------------------------
  -- Combinational logic
  --
  --
  -- Muxers to allow setting a '0' value after being selected each vector 
  -- index position with its original value to '1'
  --
  MUX_GEN : for i in 0 to 31 generate
    bypass_muxer(i) <= data_reg(i) when BYPASS_INPUT_REG = false else data(i);

    value_muxer(i)  <= bypass_muxer(i) when mux_selector_reg(i) = '1' else '0';
  end generate;

  -- Priority code generator
  code_aux <= 
          std_logic_vector(to_unsigned( 0, 6)) when value_muxer( 0) = '1' else
          std_logic_vector(to_unsigned( 1, 6)) when value_muxer( 1) = '1' else
          std_logic_vector(to_unsigned( 2, 6)) when value_muxer( 2) = '1' else
          std_logic_vector(to_unsigned( 3, 6)) when value_muxer( 3) = '1' else
          std_logic_vector(to_unsigned( 4, 6)) when value_muxer( 4) = '1' else
          std_logic_vector(to_unsigned( 5, 6)) when value_muxer( 5) = '1' else
          std_logic_vector(to_unsigned( 6, 6)) when value_muxer( 6) = '1' else
          std_logic_vector(to_unsigned( 7, 6)) when value_muxer( 7) = '1' else
          std_logic_vector(to_unsigned( 8, 6)) when value_muxer( 8) = '1' else
          std_logic_vector(to_unsigned( 9, 6)) when value_muxer( 9) = '1' else
          --
          std_logic_vector(to_unsigned(10, 6)) when value_muxer(10) = '1' else
          std_logic_vector(to_unsigned(11, 6)) when value_muxer(11) = '1' else
          std_logic_vector(to_unsigned(12, 6)) when value_muxer(12) = '1' else
          std_logic_vector(to_unsigned(13, 6)) when value_muxer(13) = '1' else
          std_logic_vector(to_unsigned(14, 6)) when value_muxer(14) = '1' else
          std_logic_vector(to_unsigned(15, 6)) when value_muxer(15) = '1' else
          std_logic_vector(to_unsigned(16, 6)) when value_muxer(16) = '1' else
          std_logic_vector(to_unsigned(17, 6)) when value_muxer(17) = '1' else
          std_logic_vector(to_unsigned(18, 6)) when value_muxer(18) = '1' else
          std_logic_vector(to_unsigned(19, 6)) when value_muxer(19) = '1' else          
          --
          std_logic_vector(to_unsigned(20, 6)) when value_muxer(20) = '1' else
          std_logic_vector(to_unsigned(21, 6)) when value_muxer(21) = '1' else
          std_logic_vector(to_unsigned(22, 6)) when value_muxer(22) = '1' else
          std_logic_vector(to_unsigned(23, 6)) when value_muxer(23) = '1' else
          std_logic_vector(to_unsigned(24, 6)) when value_muxer(24) = '1' else
          std_logic_vector(to_unsigned(25, 6)) when value_muxer(25) = '1' else
          std_logic_vector(to_unsigned(26, 6)) when value_muxer(26) = '1' else
          std_logic_vector(to_unsigned(27, 6)) when value_muxer(27) = '1' else
          std_logic_vector(to_unsigned(28, 6)) when value_muxer(28) = '1' else
          std_logic_vector(to_unsigned(29, 6)) when value_muxer(29) = '1' else          
          --
          std_logic_vector(to_unsigned(30, 6)) when value_muxer(30) = '1' else
          std_logic_vector(to_unsigned(31, 6)) when value_muxer(31) = '1' else
          (others => '1');
          
  ---------------------------------------------------------------
  -- Sequential/registering logic
  --
  process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1' then
        data_reg <= (others => '0');
      elsif load = '1' then
        data_reg <= data;
      end if;

      if rst = '1' or load = '1' then
        mux_selector_reg <= (others => '1');
      elsif next_item = '1' then
        mux_selector_reg( to_integer(unsigned(code_aux(4 downto 0))) ) <= '0';
      end if;
    
      if rst = '1' then
        code_reg  <= (others => '0');
        valid_reg <= '0';
      elsif next_item = '1' then
        code_reg <= code_aux(4 downto 0);

        -- When all code bits are '1' the code is not valid.
        if and_bits_slv( code_aux ) = '1' 
        then valid_reg <= '0';
        else valid_reg <= '1';
        end if;

      end if;
    end if;
  end process;
  
end architecture;
