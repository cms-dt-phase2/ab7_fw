-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Arithmetics
---- File       : divisor_b6q15_lut_signed.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-08
-------------------------------------------------------------------------------
---- Description: LUT to get the inverse of a number quatized by a 15 bit base
----              Values have an additional bit to consider the sign
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity DIVISOR_B6Q15_LUT_SIGNED is
  port(
    value          : in  signed(6 downto 0);
    value_inverted : out signed(15 downto 0)
  );
end entity;

architecture RTL of DIVISOR_B6Q15_LUT_SIGNED is
  constant LEN_VAL : integer:= value'length;
  constant LEN_INV : integer:= value_inverted'length;

  type data_lut_t is array (0 to 2**LEN_VAL - 1) 
                  of integer range -(2**(LEN_INV - 1)) to 2**(LEN_INV - 1) - 1;

  constant DATA_LUT : data_lut_t := (
    --
    -- This are the quantized values (a/b) for possitive numbers (value's 
    -- highest bit = 0, where a = 2^15 and b = (0 to 64) <=> 2^6
    --
    -- Values order is crytical.
    --
    32767,  
    32767, 16384, 10923,  8192,  6554,  5461,  4681,   4096,   3641,   3277,   
     2979,  2731,  2521,  2341,  2185,  2048,  1928,   1820,   1725,   1638,   
     1560,  1489,  1425,  1365,  1311,  1260,  1214,   1170,   1130,   1092,   
     1057,  1024,   993,   964,   936,   910,   886,    862,    840,    819,    
      799,   780,   762,   745,   728,   712,   697,    683,    669,    655,    
      643,   630,   618,   607,   596,   585,   575,    565,    555,    546, 
      537,   529,   520,
    --               
    -- And here they are the quatized values associated to negative input
    -- values        
    --               
     -512,  -520,  -529,  -537,
     -546,  -555,  -565,  -575,  -585,  -596,  -607,   -618,   -630,   -643,  
     -655,  -669,  -683,  -697,  -712,  -728,  -745,   -762,   -780,   -799,  
     -819,  -840,  -862,  -886,  -910,  -936,  -964,   -993,  -1024,  -1057, 
    -1092, -1130, -1170, -1214, -1260, -1311, -1365,  -1425,  -1489,  -1560, 
    -1638, -1725, -1820, -1928, -2048, -2185, -2341,  -2521,  -2731,  -2979, 
    -3277, -3641, -4096, -4681, -5461, -6554, -8192, -10923, -16384, -32767
  );
  
  -- Types: auto|block|distributed
  attribute ram_style: string;
  attribute ram_style of DATA_LUT : constant is "distributed";
  
  -- Signal to clarify the code.
  signal aux_u : unsigned(LEN_VAL - 1 downto 0);

begin
  aux_u          <= unsigned(std_logic_vector(value));
  value_inverted <= to_signed(DATA_LUT(to_integer(aux_u)), LEN_INV);
  
end architecture;