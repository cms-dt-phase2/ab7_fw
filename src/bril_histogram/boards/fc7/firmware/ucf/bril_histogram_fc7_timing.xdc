# timing constraints to allow proper clock domain crossing
set_false_path -from [get_pins {usr/ctrl_regs_inst/user_reset_delay_line_reg[3]/C}] -to [get_pins usr/global_reset_clk40_presync_reg/D]
set_false_path -from [get_pins usr/injector_regs_inst/reg_reg[2][0]/C] -to [get_pins usr/bril_histogram_fc7_injector_inst/injection_start_pre_sync_reg/D]
set_false_path -from [get_pins usr/injector_regs_inst/reg_reg[2][1]/C] -to [get_pins usr/bril_histogram_fc7_injector_inst/injection_stop_pre_sync_reg/D]
set_max_delay -from [get_pins {usr/injector_regs_inst/reg_reg[1][*]/C}] -to [get_pins {usr/bril_histogram_fc7_injector_inst/injection_nbx_available_int_reg[*]/D}] 32.000
set_max_delay -from [get_pins {usr/injector_regs_inst/reg_reg[1][*]/C}] -to [get_pins {usr/bril_histogram_fc7_injector_inst/injection_norbits_tocollect_int_reg[*]/D}] 32.000
set_max_delay -from [get_pins {usr/bril_histogram_fc7_injector_inst/injection_norbits_tocollect_int_reg[*]/C}] 6.250
set_max_delay -from [get_pins {usr/bril_histogram_fc7_injector_inst/injection_nbx_available_int_reg[*]/C}] 6.250