-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Floating point arithmetics
---- File       : float_point_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-09
-------------------------------------------------------------------------------
---- Description: Package for floating point types and operations (IEEE-754)
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package float_point_pkg is

  ---------------------------------------------------------------
  -- Constants
  ---------------------------------------------------------------
  constant UFLOAT32_EXP_ZERO : unsigned(7 downto 0) := x"00";
  constant UFLOAT32_EXP_INF  : unsigned(7 downto 0) := x"FF";
  constant UFLOAT32_EXP_BIAS : unsigned(7 downto 0) := x"7F";
  constant UFLOAT32_EXP_MAX  : unsigned(7 downto 0) := x"FE";
  constant UFLOAT32_EXP_MIN  : unsigned(7 downto 0) := x"01";

  constant MANTIS_ZERO : unsigned(22 downto 0) := (others=> '0');
  constant MANTIS_ONES : unsigned(22 downto 0) := (others=> '1');

  ---------------------------------------------------------------
  -- Types
  ---------------------------------------------------------------
  --
  -- IEEE 754-2008 format.
  --
  type UFloat32_t is record
    sign     : std_logic;
    exponent : unsigned(7 downto 0);
    mantissa : unsigned(22 downto 0);
  end record;

  constant UFLOAT32_SIZE : integer := 1 + UFloat32_t.exponent'length + 
                                          UFloat32_t.mantissa'length;

  -- Values with special meaning.
  constant UFLOAT32_NAN  : UFloat32_t := ('0', UFLOAT32_EXP_INF,  MANTIS_ONES);
  constant UFLOAT32_INF  : UFloat32_t := ('0', UFLOAT32_EXP_INF,  MANTIS_ZERO);
  constant UFLOAT32_ZERO : UFloat32_t := ('0', UFLOAT32_EXP_ZERO, MANTIS_ZERO);
  --
  constant UFLOAT32_ONE  : UFloat32_t := ('0', UFLOAT32_EXP_BIAS, MANTIS_ZERO);
  constant UFLOAT32_PI   : UFloat32_t := ('0', UFLOAT32_EXP_BIAS + 1, 
                                               "10010010000111111011010");

  ---------------------------------------------------------------
  -- Functions
  ---------------------------------------------------------------
  function slv_from_ufloat32(value : UFloat32_t) return std_logic_vector;
  function ufloat32_from_slv(vector: std_logic_vector) return UFloat32_t;

  function ufloat32_invert_sign(value : UFloat32_t) return UFloat32_t;

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component SIGNED_TO_UFLOAT32 is
  generic (POINT_POS : integer range 0 to 32 := 0);
  port(
    clk    : in  std_logic;
    rst    : in  std_logic;
    enable : in  std_logic;
    value  : in  signed(32 downto 0);
    result : out UFloat32_t
  );
  end component;

  component UFLOAT32_ADD is
  port(
    clk    : in  std_logic;
    rst    : in  std_logic;
    enable : in  std_logic;
    value1 : in  UFloat32_t;
    value2 : in  UFloat32_t;
    result : out UFloat32_t
  );
  end component;

  component UFLOAT32_PRODUCT is
  port(
    clk    : in  std_logic;
    rst    : in  std_logic;
    enable : in  std_logic;
    value1 : in  UFloat32_t;
    value2 : in  UFloat32_t;
    result : out UFloat32_t
  );
  end component;

  component UFLOAT32_NR_DIV_LUT is
  port(
    value          : in  unsigned(9 downto 0);
    value_inverted : out unsigned(13 downto 0)
  );
  end component;

  component UFLOAT32_DIVISION is
  port(
    clk         : in  std_logic;
    rst         : in  std_logic;
    enable      : in  std_logic;
    numerator   : in  UFloat32_t;
    denominator : in  UFloat32_t;
    result      : out UFloat32_t
  );
  end component;

  component UFLOAT32_TAN_A_PLUS_B is
  port (
    clk    : in  std_logic;
    rst    : in  std_logic;
    enable : in  std_logic;
    tan_A  : in  UFloat32_t;
    tan_B  : in  UFloat32_t;
    result : out UFloat32_t
  );
  end component;

  component UFLOAT32_POW2 is
  port(
    clk    : in  std_logic;
    rst    : in  std_logic;
    enable : in  std_logic;
    power  : in  signed(8 downto 0);
    value  : in  UFloat32_t;
    result : out UFloat32_t
  );
  end component;
  
  component UFLOAT32_TEST is
  port(
    clk     : in  std_logic;
    rst     : in  std_logic;
    enable  : in  std_logic;
    value1  : in  signed(32 downto 0);
    value2  : in  signed(32 downto 0);
    power   : in  signed(8 downto 0);
    sum     : out UFloat32_t;
    product : out UFloat32_t;
    division: out UFloat32_t;
    pow2    : out UFloat32_t
  );
  end component;

end package;

-------------------------------------------------------------------------------
---- Body
-------------------------------------------------------------------------------
package body float_point_pkg is

  function slv_from_ufloat32(value : UFloat32_t) return std_logic_vector
  is
    variable result : std_logic_vector(31 downto 0);
  begin
    result(31)           := value.sign;
    result(30 downto 23) := std_logic_vector(value.exponent);
    result(22 downto 0)  := std_logic_vector(value.mantissa);

    return result;
  end function;

  function ufloat32_from_slv(vector: std_logic_vector) return UFloat32_t
  is
    variable result : UFloat32_t;
  begin
    result.sign     := vector(31);
    result.exponent := unsigned(vector(30 downto 23));
    result.mantissa := unsigned(vector(22 downto 0));
  
    return result;
  end function;

  function ufloat32_invert_sign(value : UFloat32_t) return UFloat32_t
  is
    variable result : UFloat32_t;
  begin
    if value.sign = '1' 
    then result.sign := '0';
    else result.sign := '1';
    end if;

    result.exponent := value.exponent;
    result.mantissa := value.mantissa;

    return result;
  end function;

end package body;
