----------------------------------------------------------------------------------
-- Readout data types
-- �lvaro Navarro, CIEMAT, 2015
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

package TM7_readout_decl is

  constant RO_BXN_WIDTH : integer := 12;
  constant RO_EVN_WIDTH : integer := 24;
  constant RO_ORN_WIDTH : integer := 16;

end TM7_readout_decl;

package body TM7_readout_decl is

end TM7_readout_decl;
