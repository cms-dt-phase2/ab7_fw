----------------------------------------------------------------------------------
-- QuickWord FIFO
-- It's to FWFT fifos what FWFT are to regular ones.
-- Pre-reads one more word so that you have it available sooner
-- �lvaro Navarro, CIEMAT, 2016
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

Library UNIMACRO;
use UNIMACRO.vcomponents.all;

library work;

entity qwfifo is
    Generic (
        -- Maximum depth using BRAM_SDP_MACRO is 13, with must use 36Kb
        -- For 8 < depth_log2 < 13, 18Kb is used, because of better granularity in resource utilization
        DATA_WIDTH : natural := 8;
        DEPTH_LOG2 : natural := 11;
        ALMOST_FULL_OFFSET : bit_vector(15 downto 0) := X"0080";
        ALMOST_EMPTY_OFFSET : bit_vector(15 downto 0) := X"0080"
        );
    port (
        ALMOSTEMPTY   : out std_logic;
        ALMOSTFULL    : out std_logic;
        RDCOUNT       : out std_logic_vector(DEPTH_LOG2 - 1 downto 0);
        WRCOUNT       : out std_logic_vector(DEPTH_LOG2 - 1 downto 0);
        DO            : out std_logic_vector(DATA_WIDTH-1 downto 0);
        EMPTY         : out std_logic;
        FULL          : out std_logic;
        DI            : in std_logic_vector(DATA_WIDTH-1 downto 0);
        RDCLK         : in std_logic;
        RDEN          : in std_logic;
        RST           : in std_logic;
        WRCLK         : in std_logic;
        WREN          : in std_logic
       );
       
end qwfifo;

architecture Behavioral of qwfifo is

    signal ff_rden, ff_empty, empty_held : std_logic := '0';
    signal ff_do, do_held : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');


begin

    multi_bram_fifo_dualclock_inst : entity work.multi_bram_fifo_dualclock
    generic map (
        DATA_WIDTH => DATA_WIDTH,
        DEPTH_LOG2 => DEPTH_LOG2,
        FIRST_WORD_FALL_THROUGH => TRUE)
    port map (
        ALMOSTEMPTY => ALMOSTEMPTY,
        ALMOSTFULL => ALMOSTFULL,
        RDCOUNT => RDCOUNT,
        WRCOUNT => WRCOUNT,
        
        RST => RST,                   -- 1-bit input reset
        
        WRCLK => WRCLK,               -- 1-bit input write clock
        FULL => FULL,                 -- 1-bit output full
        WREN => WREN,                  -- 1-bit input write enable
        DI => DI,                    -- Input data, width defined by DATA_WIDTH parameter
        
        RDCLK => RDCLK,        -- 1-bit input read clock
        EMPTY => ff_empty,       -- 1-bit output empty
        RDEN => ff_rden,         -- 1-bit input read enable
        DO => ff_do              -- Output data, width defined by DATA_WIDTH parameter
    );

    ff_rden <= RDEN or empty_held;
    
    DO <= ff_do when ff_rden = '1' else do_held;
    EMPTY <= ff_empty when ff_rden = '1' else empty_held;
    
    process(RDCLK)
    begin
        if rising_edge(RDCLK) then
            if RST = '1' then
                do_held <= (others => '0');
                empty_held <= '1';
            else
                if ff_rden = '1' then
                    do_held <= ff_do;
                    empty_held <= ff_empty;
                end if;
            end if;
         end if;
    end process;

end Behavioral;
