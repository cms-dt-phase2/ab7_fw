library IEEE;
USE IEEE.std_logic_1164.all;
USE IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use STD.textio.all;
use STD.standard.all;
--
use work.commonfunc_pkg.all;
use work.dt_common_pkg.all;
use work.dt_constants_pkg.all;
use work.globalpath_pkg.all;

entity TB_SUPERLAYER_DATAPATH_FILE is
end entity;

architecture Behavior of TB_SUPERLAYER_DATAPATH_FILE is 

  file input_file  : text open read_mode  is "/home/jm/inputdata.txt";
  file output_file : text open write_mode is "/home/jm/outputdata.txt";

  type t_integer_array is array(integer range <> ) of integer;  

  constant clk_period  : time := 5.55 ns; -- Clock period
  constant BLANK_LINE  : string := "";
  constant WAIT_NUM_BX : std_logic_vector(11 downto 0) 
                       := std_logic_vector(to_unsigned(100, 12));

  ------------------------------------------------
  -- Auxiliary signals
  --
  signal rst             : std_logic := '0';
  signal clk             : std_logic := '0';
  signal lhc_clk         : std_logic := '0';
  signal enable          : std_logic := '0';
  signal se_fini         : std_logic := '0';
  signal wait_reading    : std_logic := '0';
  --
  signal bx_counter      : std_logic_vector(11 downto 0) := (others => '0');
  signal trigger_bx      : std_logic_vector(11 downto 0) := (others => '0');
  --                     
  signal rawHit          : DTRawHit_t;
  signal rawHitFFIn_wr   : std_logic := '0';
  signal segmentFFOut_rd : std_logic := '0';

  ------------------------------------------------
  -- Component signals
  --
  signal superLayerDatPathIn  : SuperLayerDataPathIn_t;
  signal superLayerDatPathOut : SuperLayerDataPathOut_t;

begin
  -- Instantiate the Unit Under Test (UUT)
  UUT: SUPERLAYER_DATAPATH port map (
    clk_fifo_rd          => clk, 
    clk_fifo_wr          => clk, 
    clk_mixer            => clk, 
    clk_analyzer         => clk, 
    clk_dup_filter       => clk, 
    clk_qenhan_filter    => clk, 
    rst                  => rst, 
    superLayerDatPathIn  => superLayerDatPathIn,
    superLayerDatPathOut => superLayerDatPathOut 
  );

  superLayerDatPathIn.rawHit          <= rawHit;
  superLayerDatPathIn.rawHitFFIn_wr   <= rawHitFFIn_wr;
  superLayerDatPathIn.segmentFFOut_rd <= segmentFFOut_rd;
  superLayerDatPathIn.reset_overflow  <= '0';
  superLayerDatPathIn.control_regs    <= (
        bx_time_tolerance      => std_logic_vector(to_unsigned(30, 8)),
        chisqr_threshold       => std_logic_vector(to_unsigned(3, 16)),
        tanphi_x4096_threshold => (others => '1'),
--         tanphi_x4096_threshold => std_logic_vector(to_unsigned(4096, 14)), --1
--         tanphi_x4096_threshold => std_logic_vector(to_unsigned(2458, 14)), -- 0.6
        bx_counter             => bx_counter,
        bx_time_convert_ena    => '0'
  );

  -- Clock process
  LHC_CLK_PROCESS : process
  begin
    lhc_clk <= not lhc_clk;
    wait for 12.5 ns;
  end process;
  
  -- Bunch-crossing counter process
--   BX_COUNTER_PROCESS : process(lhc_clk, rst, enable)
  BX_COUNTER_PROCESS : process(lhc_clk, rst)
  begin
    if lhc_clk'event and lhc_clk = '1' then
      if rst = '1' or unsigned(bx_counter) = to_unsigned(MAX_BX_IDX, 12) then
        bx_counter <= (others => '0');
      else
        bx_counter <= bx_counter + '1';
      end if;
    end if;
  end process;

  -- Clock process
  CLK_PROCESS : process
  begin
    clk <= not clk;
    wait for clk_period/2;
  end process;

  -- Reading results process
  RD_FIFO_PROCESS : process(clk, rst, enable)
    variable row_out  : line;
    variable hits     : HitsSegment_t;
    variable main     : DTSegMainParams_t;
    variable segment  : DTSegment_t;
  begin
    if clk'event and clk = '1'then
      if rst = '1' then
        segmentFFOut_rd <= '0';
      elsif enable = '1' then

        -- Only one cycle active.
        if segmentFFOut_rd = '1' then
          segmentFFOut_rd <= '0';
        
          -- Useful aliases
          segment := superLayerDatPathOut.segment;
          main    := superLayerDatPathOut.segment.main_params;
          hits    := superLayerDatPathOut.segment.main_params.hits;
          --
          -- Build data line 
          --
          -- Quality and BX
          write(row_out, to_integer(unsigned(main.quality)), 
                std.textio.right, 1);
          write(row_out, string'(", "));
          write(row_out, to_integer(unsigned(main.bx_time)), 
                std.textio.right, 5);
--           write(row_out, string'(", "));
--           write(row_out, to_integer(unsigned(main.bx_time)) / 25, 
--                 std.textio.right, 4);
          write(row_out, string'(", "));
          write(row_out, to_integer(unsigned(segment.bx_id)), 
                std.textio.right, 1);
          write(row_out, string'(", "));
          
          
          -- Wires ID
          for i in 0 to 3 loop
            write(row_out, to_integer(unsigned(hits(i).channel_id)), 
                  std.textio.right, 2);
            write(row_out, string'(", "));
          end loop;
          
          -- TDC times
          for i in 0 to 3 loop
            write(row_out, to_integer(unsigned(hits(i).dt_time.tdc_time)), 
                  std.textio.right, 5);
            write(row_out, string'(", "));
          end loop;
        
          -- Laterality combination (vector is flipped)
          for i in 0 to 3 loop
            write(row_out, to_bitvector(main.laterality_combination)(i), 
                  std.textio.right, 1);
            write(row_out, string'(", "));
          end loop;

          -- Position, angle and chi^2
          write(row_out, 
                real(to_integer(signed(segment.horizontal_position))) / 16.0, 
                std.textio.right, 0, 2);

          write(row_out, string'(", "));
          write(row_out, 
                real(to_integer(signed(segment.phi_tangent_x4096))) / 4096.0, 
                std.textio.right, 0, 5);

--           write(row_out, string'(", "));
--           for i in 14 downto 0 loop
--             write(row_out, 
--                   to_bitvector(std_logic_vector(segment.phi_tangent_x4096))(i), 
--                   std.textio.right, 1);
--           end loop;
          
          write(row_out, string'(", "));
          write(row_out, real(to_integer(unsigned(segment.chi_square))) / 4096.0, 
                std.textio.right, 0, 1);
          
          -- Write data line to file
          writeline(output_file, row_out);   
        
        elsif superLayerDatPathOut.segmentFFOut_empty = '0' then
          segmentFFOut_rd <= '1';
        end if;

      end if;
    end if;
  end process;

  -- Writing inputs process
  WR_DATA_PROC : process(clk, rst, enable, bx_counter)
    variable row_in      : line;
    variable v_data_read : t_integer_array(1 to 3);  
  begin
    if clk'event and clk = '1' then
      if rst = '1' then
        rawHitFFIn_wr <= '0';
        wait_reading  <= '0';
        trigger_bx    <= (others => '0');
        v_data_read   := (others => 0);

      elsif enable = '1' then

        if(not endfile(input_file)) and wait_reading = '0' then
          readline(input_file, row_in);   -- Read data line from file

          if string(row_in) /= BLANK_LINE then
            rawHitFFIn_wr <= '1';
            --
            -- Split data into different fields:
            --    Assumed:  1 -> layer_id
            --              2 -> channel_id
            --              3 -> TDC time
            --
            for kk in 1 to 3 loop
              read(row_in, v_data_read(kk));
            end loop;

            rawHit <= (
              tdc_time       => std_logic_vector(to_unsigned(v_data_read(3), 17)),
              channel_id     => std_logic_vector(to_unsigned(v_data_read(2), 7)),
              layer_id       => std_logic_vector(to_unsigned(v_data_read(1), 2)),
              super_layer_id => (others => '0')
            );
          else
            if unsigned(bx_counter + WAIT_NUM_BX) >= to_unsigned(MAX_BX_IDX, 12)
            then 
              trigger_bx <= bx_counter + WAIT_NUM_BX - 
                            std_logic_vector(to_unsigned(MAX_BX_IDX, 12));
            else 
              trigger_bx <= bx_counter + WAIT_NUM_BX;
            end if;
          
            wait_reading  <= '1';
            rawHitFFIn_wr <= '0';
          end if;

        else
          rawHitFFIn_wr <= '0';
          se_fini       <= '1';
        end if;

        if wait_reading = '1' and trigger_bx = bx_counter 
        then wait_reading <= '0';
        end if;

      end if;
    end if;
  end process;

  CONTROL_PROCESS: process
  begin
    enable <= '0';

    -- Initial RESET
    rst <= '1';
    wait for 300 ns; -- hold reset
    rst <= '0';
    
--     wait until 300 ns;
    wait until bx_counter = std_logic_vector(to_unsigned(100, 12));

    enable <= '1';

    wait;
  end process;

end architecture;
