----------------------------------------------------------------------------------
-- Injects and Monitors some of the data path signals
-- Alvaro Navarro, CIEMAT, 2019
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


library work;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_ab7_spy.all;
use work.mp7_ttc_decl.all;
use work.obdt_decoder_decl.all;
use work.globalpath_pkg.all; -- trigger fifo intercepts
use work.dt_common_pkg.all; -- Cela's Trigger data types
use work.BXarithmetic.all;
use work.bo2.all;

entity ab7_spy is
  port(
    clk40           : in std_logic;
    clk160          : in std_logic;
    -- ipbus signals
    ipb_clk         : in std_logic;
    ipb_rst         : in std_logic;
    ipb_in          : in ipb_wbus;
    ipb_out         : out ipb_rbus;
    -- Hit data
    hit_data_in     : in  slv32_v_t(1 to 3);
    hit_data_out    : out slv32_v_t(1 to 3);
    -- Primitives data
    tpg_clk          : in std_logic;
    tp              : in std_logic_vector(DTPRIMITIVE_SIZE-1 downto 0);
    tp_valid        : in std_logic; 
    -- Other fifo intercepts; note the in<-->out swap: outside of this module the naming corresponds to trigger module directions
    fifoInterceptIn : in  SegCandFifoInterceptOut_arr_t;
    fifoInterceptOut: out SegCandFifoInterceptIn_arr_t;
    -- Other signals
    orb_ctr         : in eoctr_t;
    bunch_ctr       : in bctr_t
  );
end entity;

architecture Behavioral of ab7_spy is

  -- Signals for injectMode
  signal ipb_to_slaves            : ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves          : ipb_rbus_array(N_SLAVES - 1 downto 0);
  
  -- MEMORIES SIZES
  constant HITSMEM_RATIO_LOG2   : integer := 1 ; -- for 32*(2**1) = 64 write width
  constant HITSMEM_DEPTH        : integer := 13; -- the used memory mode only uses RAMB36E1 with 32bit witdh, 1024 is the minimum depth
  constant HITSMEM_ADDR_WIDTH   : integer := HITSMEM_RATIO_LOG2 + HITSMEM_DEPTH;

  constant PRIMSMEM_RATIO_LOG2  : integer := 5 ; -- for 32*(2**5) = 1024 write width (tp is 571 in width and we add the orbit & bx of arrival of the primitive)
  constant PRIMSMEM_DEPTH       : integer := 10; -- the used memory mode only uses RAMB36E1 with 32bit witdh, 1024 is the minimum depth
  constant PRIMSMEM_ADDR_WIDTH  : integer := PRIMSMEM_RATIO_LOG2 + PRIMSMEM_DEPTH;
  
  constant MIXERMEM_RATIO_LOG2  : integer := 3 ; -- for 32*(2**3) = 256 write width (segcand is 128 in width and we add the bx of arrival of the segment candidate)
  constant MIXERMEM_DEPTH       : integer := 10; -- the used memory mode only uses RAMB36E1 with 32bit witdh, 1024 is the minimum depth
  constant MIXERMEM_ADDR_WIDTH  : integer := MIXERMEM_RATIO_LOG2 + MIXERMEM_DEPTH;
  
  -- AUX signals for loop-generated memories
  type int_arr_t is array(natural range <>) of integer;
  -- Hits
  constant N_SLV_FROM_HIT_SL : int_arr_t(1 to 3) := (N_SLV_HITS_MEM_1, N_SLV_HITS_MEM_2, N_SLV_HITS_MEM_3);
  type stat_hitsmem_ctr_t is array (3 downto 1) of unsigned(HITSMEM_DEPTH-1 downto 0);
  signal stat_hitsmem_ctr : stat_hitsmem_ctr_t;
  -- Mixer
  constant N_SLV_FROM_MIXER_INDEX : int_arr_t(0 to 1) := (N_SLV_MIXER_MEM_0, N_SLV_MIXER_MEM_1);
  type stat_mixermem_ctr_t is array (1 downto 0) of unsigned(MIXERMEM_DEPTH-1 downto 0);
  signal stat_mixermem_ctr : stat_mixermem_ctr_t;
  
  -- Signals for the ipbus register
  constant N_REGS : integer := 4;
  constant N_STAT : integer := N_REGS;
  constant N_CTRL : integer := N_REGS;
  signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);
  signal stat : ipb_reg_v(N_STAT-1 downto 0);
  signal conf : ipb_reg_v(N_CTRL-1 downto 0);

  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals declaration
  signal conf_hitInjectMode_enable, conf_hitInjectMode_select, conf_mixerInjectMode_enable,
    conf_mixerInjectMode_select, conf_snapshotMode_enable : std_logic := '0';

  signal stat_mixermem_ctr_0, stat_mixermem_ctr_1, stat_primsmem_ctr : unsigned(9 downto 0) := (others => '0');
  signal stat_hitsmem_ctr_1, stat_hitsmem_ctr_2, stat_hitsmem_ctr_3 : unsigned(12 downto 0) := (others => '0');

  -- End of automatically-generated VHDL code for register "breakout" signals declaration
  --------------------------------------------------------------------------------


begin


  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals assignment

  conf_snapshotMode_enable    <= conf(16#00#)(0);
  conf_hitInjectMode_select   <= conf(16#00#)(1);
  conf_hitInjectMode_enable   <= conf(16#00#)(2);
  conf_mixerInjectMode_select <= conf(16#00#)(3);
  conf_mixerInjectMode_enable <= conf(16#00#)(4);
  stat(16#00#)(9 downto 0)    <= std_logic_vector( stat_primsmem_ctr );
  stat(16#00#)(28 downto 16)  <= std_logic_vector( stat_hitsmem_ctr_1 );
  stat(16#01#)(12 downto 0)   <= std_logic_vector( stat_hitsmem_ctr_2 );
  stat(16#01#)(28 downto 16)  <= std_logic_vector( stat_hitsmem_ctr_3 );
  stat(16#02#)(9 downto 0)    <= std_logic_vector( stat_mixermem_ctr_0 );
  stat(16#02#)(25 downto 16)  <= std_logic_vector( stat_mixermem_ctr_1 );

  -- End of automatically-generated VHDL code for register "breakout" signals assignment
  --------------------------------------------------------------------------------

  -- ipbus address decode
	fabric: entity work.ipbus_fabric_sel
    generic map(
    	NSLV => N_SLAVES,
    	SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      sel => ipbus_sel_ab7_spy(ipb_in.ipb_addr),
      ipb_to_slaves => ipb_to_slaves,
      ipb_from_slaves => ipb_from_slaves
    );

  --------------------------------------
  -- IPbus register
  --------------------------------------
  reg: entity work.ipbus_syncreg_v
    generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
    port map(
      clk => ipb_clk,
      rst => ipb_rst,
      ipb_in => ipb_to_slaves(N_SLV_CSR),
      ipb_out => ipb_from_slaves(N_SLV_CSR),
      slv_clk => clk40,
      d => stat,
      q => conf,
      --rstb => stat_stb,
      stb => ctrl_stb
    );

  --------------------------------------
  -- IPbus hit memory for injection and spy 
  --------------------------------------
  stat_hitsmem_ctr_1  <= stat_hitsmem_ctr(1);
  stat_hitsmem_ctr_2  <= stat_hitsmem_ctr(2);
  stat_hitsmem_ctr_3  <= stat_hitsmem_ctr(3);
  
  hits_mem_gen: for i in 1 to 3 generate
  
    signal hitsmem_q, hitsmem_d : std_logic_vector(32*(2**HITSMEM_RATIO_LOG2)-1 downto 0);
    signal hitsmem_we, hitsmem_advance, hitsmem_zeroaddr : std_logic;
    
    signal hit_data_inject : std_logic_vector(31 downto 0);
    
  begin
  
    hits_mem: entity work.ipbus_ram_fabric_seqam
      generic map(
        IPBUS_ADDR_WIDTH => HITSMEM_ADDR_WIDTH ,
        FABRIC_RATIO_LOG2 => HITSMEM_RATIO_LOG2
      )
      port map(
        ipb_clk     => ipb_clk,
        ipb_rst     => ipb_rst,
        ipb_in      => ipb_to_slaves  (N_SLV_FROM_HIT_SL(i)),
        ipb_out     => ipb_from_slaves(N_SLV_FROM_HIT_SL(i)),
        fabric_clk  => clk160,
        we          => hitsmem_we,
        advance     => hitsmem_advance,
        zeroaddr    => hitsmem_zeroaddr,
        d           => hitsmem_d,
        q           => hitsmem_q
      );
      
    hitsmem_zeroaddr  <= not ( conf_hitInjectMode_enable or conf_snapshotMode_enable );
    hitsmem_we        <= conf_snapshotMode_enable and hit_data_in(i)(31);
    
    hitsmem_d(31 downto  0) <= hit_data_in(i) ;
    hitsmem_d(55 downto 32) <= orb_ctr(11 downto 0) & bunch_ctr ;
  
      
    process(clk160)
      -- hitsmem QuickWord
      variable hitsmem_qw_cool: std_logic := '0';
      variable hitsmem_qw_q : std_logic_vector(hitsmem_q'range) := (others => '0');
      -- hitsmem QuickWord
    begin
      if rising_edge(clk160) then
        hitsmem_advance <= '0';
  
        -----------------
        -- INJECT MODE --
        -----------------
        if conf_hitInjectMode_select = '1' then hit_data_out(i) <= hit_data_inject;
        else                                    hit_data_out(i) <= hit_data_in(i);
        end if; 
      
        hit_data_inject <= (others => '0');
  
        if conf_hitInjectMode_enable = '0' then
          hitsmem_qw_cool := '1';
        elsif hitsmem_qw_cool = '1' then
          hitsmem_qw_cool := '0';
          hitsmem_advance <= '1';
        else
          if hitsmem_advance = '1' then
            hitsmem_qw_q := hitsmem_q;
          end if;
          
          if hitsmem_qw_q(47 downto 32) =  orb_ctr(3 downto 0) & bunch_ctr then
            hit_data_inject <= hitsmem_qw_q(31 downto 0);
            hitsmem_advance <= '1';
          end if;
        end if;
        
        -------------------
        -- SNAPSHOT MODE --
        -------------------
        if conf_snapshotMode_enable = '0' then
          stat_hitsmem_ctr(i) <= (others => '0'); 
        elsif hitsmem_we = '1' and stat_hitsmem_ctr(i) /= (stat_hitsmem_ctr(i)'range => '1') then
          stat_hitsmem_ctr(i) <= stat_hitsmem_ctr(i) + 1;
          hitsmem_advance <= '1';  -- advance after it has been written to; stop advancing ath the end of memory (latest will be overwritten)
        end if;
  
      end if;
    end process;
  
  end generate;

  --------------------------------------
  -- IPbus primitives memory for spy
  --------------------------------------
  prims_mem_gen: if TRUE generate

    signal primsmem_d : std_logic_vector(32*(2**PRIMSMEM_RATIO_LOG2)-1 downto 0);
    signal primsmem_we, primsmem_zeroaddr, primsmem_advance : std_logic;
  
  begin
    
    prims_mem: entity work.ipbus_ram_fabric_seqam
      generic map(
        IPBUS_ADDR_WIDTH => PRIMSMEM_ADDR_WIDTH,
        FABRIC_RATIO_LOG2 => PRIMSMEM_RATIO_LOG2
      )
      port map(
        ipb_clk     => ipb_clk,
        ipb_rst     => ipb_rst,
        ipb_in      => ipb_to_slaves(N_SLV_PRIMS_MEM),
        ipb_out     => ipb_from_slaves(N_SLV_PRIMS_MEM),
        fabric_clk  => tpg_clk,
        we          => primsmem_we,
        advance     => primsmem_advance,
        zeroaddr    => primsmem_zeroaddr,
        d           => primsmem_d,
        q           => open
      );
  
    primsmem_zeroaddr <= not ( conf_hitInjectMode_enable or conf_mixerInjectMode_enable or conf_snapshotMode_enable );
    primsmem_we <= ( conf_hitInjectMode_enable or conf_mixerInjectMode_enable or conf_snapshotMode_enable ) and tp_valid;
    
    primsmem_d(11 downto  0) <= bunch_ctr ;
    primsmem_d(23 downto 12) <= orb_ctr(11 downto 0) ;
    primsmem_d(23 + DTPRIMITIVE_SIZE downto 24) <= tp;
  
    process(tpg_clk)
    begin
      if rising_edge(tpg_clk) then
        primsmem_advance <= '0';
      
        if conf_hitInjectMode_enable = '0' and conf_mixerInjectMode_enable = '0' and conf_snapshotMode_enable = '0' then
          stat_primsmem_ctr <= (others => '0'); 
        elsif primsmem_we = '1' and stat_primsmem_ctr /= (stat_primsmem_ctr'range => '1') then
          stat_primsmem_ctr <= stat_primsmem_ctr + 1;
          primsmem_advance <= '1';  -- advance after it has been written to; stop advancing ath the end of memory (latest will be overwritten)
        end if;
      end if;
    end process;
    
  end generate;

  --------------------------------------
  -- IPbus mixer memory for injection and spy 
  --------------------------------------
  stat_mixermem_ctr_0  <= stat_mixermem_ctr(0);
  stat_mixermem_ctr_1  <= stat_mixermem_ctr(1);
  
  mixer_mem_gen: for i in 0 to 1 generate
  
    signal mixermem_q, mixermem_d : std_logic_vector(32*(2**MIXERMEM_RATIO_LOG2)-1 downto 0);
    signal mixermem_we, mixermem_advance, mixermem_zeroaddr : std_logic;
    
    signal mixer_data_inject : std_logic_vector(DTSEG_CANDIDATE_SIZE-1 downto 0);
    signal mixer_wren_inject : std_logic;
    
    signal hitInjectMode_enable_reclk, mixerInjectMode_enable_reclk,
      snapshotMode_enable_reclk, mixerInjectMode_select_reclk : std_logic;

    signal arrival : std_logic_vector(23 downto 0);
    
  begin
  
    process (fifoInterceptIn(i).clk)
    begin
      if rising_edge(fifoInterceptIn(i).clk) then
        hitInjectMode_enable_reclk    <= conf_hitInjectMode_enable        ;
        mixerInjectMode_enable_reclk  <= conf_mixerInjectMode_enable      ;
        snapshotMode_enable_reclk     <= conf_snapshotMode_enable         ;
        mixerInjectMode_select_reclk  <= conf_mixerInjectMode_select      ;
        arrival                       <= orb_ctr(11 downto 0) & bunch_ctr ;
      end if;
    end process;
  
  
    mixer_mem: entity work.ipbus_ram_fabric_seqam
      generic map(
        IPBUS_ADDR_WIDTH => MIXERMEM_ADDR_WIDTH ,
        FABRIC_RATIO_LOG2 => MIXERMEM_RATIO_LOG2
      )
      port map(
        ipb_clk     => ipb_clk,
        ipb_rst     => ipb_rst,
        ipb_in      => ipb_to_slaves  (N_SLV_FROM_MIXER_INDEX(i)),
        ipb_out     => ipb_from_slaves(N_SLV_FROM_MIXER_INDEX(i)),
        fabric_clk  => fifoInterceptIn(i).clk,
        we          => mixermem_we,
        advance     => mixermem_advance,
        zeroaddr    => mixermem_zeroaddr,
        d           => mixermem_d,
        q           => mixermem_q
      );
      
    mixermem_zeroaddr  <= not ( hitInjectMode_enable_reclk or mixerInjectMode_enable_reclk or snapshotMode_enable_reclk );
    mixermem_we        <= ( hitInjectMode_enable_reclk or snapshotMode_enable_reclk ) and fifoInterceptIn(i).wrEna;
    
    mixermem_d(DTSEG_CANDIDATE_SIZE-1 downto  0) <= slv_from_dtSegCandidate( fifoInterceptIn(i).segment_candidate  ) ;
    mixermem_d(DTSEG_CANDIDATE_SIZE+24-1 downto DTSEG_CANDIDATE_SIZE) <= arrival;
  
      
    process(fifoInterceptIn(i).clk)
      variable mixermem_started : boolean := false;
      -- mixermem QuickWord
      variable mixermem_qw_cool: std_logic := '0';
      variable mixermem_qw_q : std_logic_vector(mixermem_q'range) := (others => '0');
      -- mixermem QuickWord
    begin
      if rising_edge(fifoInterceptIn(i).clk) then
        mixermem_advance <= '0';
  
        -----------------
        -- INJECT MODE --
        -----------------
        if mixerInjectMode_select_reclk = '1' then
          fifoInterceptOut(i).segment_candidate  <= dtSegCandidate_from_slv( mixer_data_inject );
          fifoInterceptOut(i).wrEna              <= mixer_wren_inject;
        else
          fifoInterceptOut(i).segment_candidate  <= fifoInterceptIn(i).segment_candidate;
          fifoInterceptOut(i).wrEna              <= fifoInterceptIn(i).wrEna;
        end if; 
      
        mixer_data_inject <= (others => '0');
        mixer_wren_inject <= '0';
  
        if mixerInjectMode_enable_reclk = '0' then
          mixermem_qw_cool := '1';
          mixermem_started := false;
        elsif mixermem_qw_cool = '1' then
          mixermem_qw_cool := '0';
          mixermem_advance <= '1';
        else
          if mixermem_advance = '1' then
            mixermem_qw_q := mixermem_q;
          end if;
          
          if (not mixermem_started and mixermem_qw_q(DTSEG_CANDIDATE_SIZE+12 downto DTSEG_CANDIDATE_SIZE+1) =  bunch_ctr) then
            mixermem_started := true;
          elsif ( mixermem_started and mixermem_qw_q(DTSEG_CANDIDATE_SIZE) =  '1' ) then
            mixer_data_inject <= mixermem_qw_q(DTSEG_CANDIDATE_SIZE-1 downto 0);
            mixer_wren_inject <= '1';
            mixermem_advance <= '1';
          end if;
        end if;
        
        -------------------
        -- SNAPSHOT MODE --
        -------------------
        if hitInjectMode_enable_reclk = '0' and snapshotMode_enable_reclk = '0' then
          stat_mixermem_ctr(i) <= (others => '0'); 
        elsif mixermem_we = '1' and stat_mixermem_ctr(i) /= (stat_mixermem_ctr(i)'range => '1') then
          stat_mixermem_ctr(i) <= stat_mixermem_ctr(i) + 1;
          mixermem_advance <= '1';  -- advance after it has been written to; stop advancing ath the end of memory (latest will be overwritten)
        end if;

      end if;
    end process;
  
  end generate;


end architecture;
