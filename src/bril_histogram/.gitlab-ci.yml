## variables to be defined in gitlab ui

# CONTROLHUB_ADDRESS - address of the controlhub to which the fc7 is connected
# BOARD_IP - ip address of the fc7 using for testing

## templates

# build template to run on the runner
.build:fw_name: &build_definition
  only:
    variables:
      - $CI_PROJECT_NAMESPACE == "myharank"
  variables: &build_template_vars
    # Otherwise lock file of cancelled runs might stall future check outs
    GIT_STRATEGY: none
    ARTIFACTS_FOLDER: "public/counter_${COUNTER_WIDTH}_increment_${INCREMENT_WIDTH}"
  tags:  # tags to differentiate runners able to run the job
    - cmsbril-runner-fw
  before_script:
    # timestamp 
    - export CI_JOB_TIMESTAMP=$(date +%F)
    # vivado
    - export XILINXD_LICENSE_FILE=2112@localhost,2112@lxlicen01.cern.ch,2112@lxlicen02.cern.ch,2112@lxlicen03.cern.ch
    - source /opt/Xilinx/Vivado/2018.3/settings64.sh
    # cactus variables
    - export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:/opt/cactus/lib
    - export PATH=$PATH:/opt/cactus/bin
    # ipbb
    - source /opt/ipbb/env.sh
    # python3
    - alias python='/usr/bin/python3'
    # ssh conf
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add
    # just print the current folder
    - pwd    
  script:
    # create an ipbus folder
    - rm -rf ipbb_bril_histogram_fc7_test
    - ipbb init ipbb_bril_histogram_fc7_test
    - cd ipbb_bril_histogram_fc7_test
    # add git repos
    - ipbb add git ssh://git@gitlab.cern.ch:7999/myharank/fc7-firmware.git -b ipbb_dev
    - ipbb add git https://github.com/ipbus/ipbus-firmware -b v1.4
    - ipbb add git ssh://git@gitlab.cern.ch:7999/${CI_PROJECT_NAMESPACE}/bril_histogram.git -b ${CI_COMMIT_REF_NAME}
    # init the project
    - ipbb proj create vivado bril_histogram_tb bril_histogram:boards/fc7
    # modify the configuration
    - cd src/bril_histogram/boards/fc7/firmware/hdl/
    - rm bril_histogram_fc7_injector_package.vhd
    # new file
    - echo "-- Automatically generated file - IPBus Build" >> bril_histogram_fc7_injector_package.vhd
    - echo "library IEEE;" >> bril_histogram_fc7_injector_package.vhd
    - echo "use IEEE.STD_LOGIC_1164.ALL;" >> bril_histogram_fc7_injector_package.vhd
    - echo "package bril_histogram_fc7_injector_package is" >> bril_histogram_fc7_injector_package.vhd
    - echo "constant ASYNC_MODE :boolean := ${ASYNC_MODE};" >> bril_histogram_fc7_injector_package.vhd
    - echo "constant NUMBER_OF_BINS :natural := ${NBINS};" >> bril_histogram_fc7_injector_package.vhd
    - echo "constant COUNTER_WIDTH :natural := ${COUNTER_WIDTH};" >> bril_histogram_fc7_injector_package.vhd
    - echo "constant INCREMENT_WIDTH :natural := ${INCREMENT_WIDTH};" >> bril_histogram_fc7_injector_package.vhd
    - echo "constant NUMBER_OF_UNITS :natural := ${NUMBER_OF_UNITS};" >> bril_histogram_fc7_injector_package.vhd
    - echo "constant UNIT_INCREMENT_WIDTH :natural := ${UNIT_INCREMENT_WIDTH};" >> bril_histogram_fc7_injector_package.vhd
    - echo "constant NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE :natural := ${NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE};" >> bril_histogram_fc7_injector_package.vhd
    - echo "constant UNIT_ERROR_COUNTER_WIDTH :natural := ${UNIT_ERROR_COUNTER_WIDTH};" >> bril_histogram_fc7_injector_package.vhd
    - echo "constant STORE_ERRORS :boolean := ${STORE_ERRORS};" >> bril_histogram_fc7_injector_package.vhd
    - echo "end bril_histogram_fc7_injector_package;" >> bril_histogram_fc7_injector_package.vhd
    # go back
    - cd -
    # create it
    - cd proj/bril_histogram_tb
    - ipbb vivado project --natural
    # and compile
    - ipbb vivado synth -j4 impl -j4
    - ipbb vivado package
    # artefacts
    - cd ${CI_PROJECT_DIR}
    - mkdir -p ${ARTIFACTS_FOLDER}
    - cp ipbb_bril_histogram_fc7_test/proj/bril_histogram_tb/package/src/top.bit ${ARTIFACTS_FOLDER}/
    - cp ipbb_bril_histogram_fc7_test/proj/bril_histogram_tb/vivado*.log ${ARTIFACTS_FOLDER}/
  artifacts:
    paths:
      - ${ARTIFACTS_FOLDER}

# load firmware template to run on the runner
.load_fw:fw_name: &load_fw_definition
  tags:  # tags to differentiate runners able to run the job
    - cmsbril-runner-sw
  only:
    variables:
      - $CI_PROJECT_NAMESPACE == "myharank"
  variables:
  before_script:
    # cactus variables
    - export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:/opt/cactus/lib
    - export PATH=$PATH:/opt/cactus/bin
  timeout: 5m
  script:
    # load firmware image
    - fc7-d19c.exe -i ${BOARD_IP} -f ${ARTIFACTS_FOLDER}/top.bit -n new_file_1.bin
    - rm ${ARTIFACTS_FOLDER}/top.bit
    - sleep 5
# test template to run on the runner
.test:fw_name: &test_definition
  tags:  # tags to differentiate runners able to run the job
    - cmsbril-runner-sw
  only:
    variables:
      - $CI_PROJECT_NAMESPACE == "myharank"
  variables: &test_template_vars
    # Otherwise lock file of cancelled runs might stall future check outs
    GIT_STRATEGY: clone
  before_script:
    # cactus variables
    - export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:/opt/cactus/lib
    - export PATH=$PATH:/opt/cactus/bin
    # python3
    - alias python='/usr/bin/python3'   
    - export PYTHONPATH=$PYTHONPATH:${CI_PROJECT_DIR}/components/bril_histogram/software
    - export PYTHONPATH=$PYTHONPATH:${CI_PROJECT_DIR}/components/bril_histogram/software/lib
  script:
    # test with python
    - cd ${CI_PROJECT_DIR}/boards/fc7/software
    # update connection file
    - rm fc7_connection.dat
    - echo "protocol=tcp" >> fc7_connection.dat
    - echo "controlhub=${CONTROLHUB_ADDRESS}" >> fc7_connection.dat
    - echo "board_ip=${BOARD_IP}" >> fc7_connection.dat
    # make python bindings
    - make -C ${CI_PROJECT_DIR}/components/bril_histogram/software py
    # now test
    - if [ "${STORE_ERRORS}" = "true" ]; then export STORE_ERRORS_INT=1; else export STORE_ERRORS_INT=0; fi;
    - python3 -u test_bril_histogram.py ${NBINS} ${COUNTER_WIDTH} ${INCREMENT_WIDTH} ${NUMBER_OF_UNITS} ${UNIT_INCREMENT_WIDTH} ${UNIT_ERROR_COUNTER_WIDTH} ${STORE_ERRORS_INT} ${NUMBER_OF_STORED_ORBITS} ${NUMBER_OF_COLLECTED_ORBITS} ${NITERATIONS} ${COUNTER_OVERFLOW_TEST}

## finally our configuration

## ASYNC IMPLEMENTATION TEST
# counter 16 increment 5
build:async_counter_16_increment_5_320mhz:
  <<: *build_definition
  stage: build
  variables: &async_counter_16_increment_5_320mhz_vars
    <<: *build_template_vars
    ASYNC_MODE: "true"
    NBINS: "3564" 
    COUNTER_WIDTH: "16"
    INCREMENT_WIDTH: "5"
    NUMBER_OF_UNITS: "5"
    UNIT_INCREMENT_WIDTH: "3"
    NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE: "4"
    UNIT_ERROR_COUNTER_WIDTH: "3"
    STORE_ERRORS: "false"

load_fw:async_counter_16_increment_5_320mhz:
  <<: *load_fw_definition
  stage: load_fw
  needs:  
    - job: build:async_counter_16_increment_5_320mhz
  variables:
    <<: *async_counter_16_increment_5_320mhz_vars

test:async_counter_16_increment_5_320mhz:
  <<: *test_definition
  stage: test
  needs:
    - job: load_fw:async_counter_16_increment_5_320mhz
  variables:
    <<: *async_counter_16_increment_5_320mhz_vars
    <<: *test_template_vars
    NUMBER_OF_STORED_ORBITS: "8"
    NUMBER_OF_COLLECTED_ORBITS: "2048"
    NITERATIONS: "1000"
    COUNTER_OVERFLOW_TEST: "0"

test:async_ovf_counter_16_increment_5_320mhz:
  <<: *test_definition
  stage: test
  needs:
    - job: load_fw:async_counter_16_increment_5_320mhz
  variables:
    <<: *async_counter_16_increment_5_320mhz_vars
    <<: *test_template_vars
    NUMBER_OF_STORED_ORBITS: "8"
    NUMBER_OF_COLLECTED_ORBITS: "8192"
    NITERATIONS: "100"
    COUNTER_OVERFLOW_TEST: "1"

# counter 21 increment 7
build:async_counter_21_increment_7_320mhz:
  <<: *build_definition
  stage: build
  variables: &async_counter_21_increment_7_320mhz_vars
    <<: *build_template_vars
    ASYNC_MODE: "true"
    NBINS: "3564" 
    COUNTER_WIDTH: "21"
    INCREMENT_WIDTH: "7"
    NUMBER_OF_UNITS: "39"
    UNIT_INCREMENT_WIDTH: "2"
    NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE: "4"
    UNIT_ERROR_COUNTER_WIDTH: "3"
    STORE_ERRORS: "true"

load_fw:async_counter_21_increment_7_320mhz:
  <<: *load_fw_definition
  stage: load_fw
  needs:  
    - job: build:async_counter_21_increment_7_320mhz
  variables:
    <<: *async_counter_21_increment_7_320mhz_vars

test:async_counter_21_increment_7_320mhz:
  <<: *test_definition
  stage: test
  needs:  
    - job: load_fw:async_counter_21_increment_7_320mhz
  variables:
    <<: *async_counter_21_increment_7_320mhz_vars
    <<: *test_template_vars
    NUMBER_OF_STORED_ORBITS: "8"
    NUMBER_OF_COLLECTED_ORBITS: "4096"
    NITERATIONS: "1000"
    COUNTER_OVERFLOW_TEST : "0"

test:async_ovf_counter_21_increment_7_320mhz:
  <<: *test_definition
  stage: test
  needs:  
    - job: load_fw:async_counter_21_increment_7_320mhz
  variables:
    <<: *async_counter_21_increment_7_320mhz_vars
    <<: *test_template_vars
    NUMBER_OF_STORED_ORBITS: "8"
    NUMBER_OF_COLLECTED_ORBITS: "30000"
    NITERATIONS: "100"
    COUNTER_OVERFLOW_TEST: "1"


## SYNC IMPLEMENTATION TEST
# counter 21 increment 7
build:counter_21_increment_7_320mhz:
  <<: *build_definition
  stage: build
  variables: &counter_21_increment_7_320mhz_vars
    <<: *build_template_vars
    ASYNC_MODE: "false"
    NBINS: "3564" 
    COUNTER_WIDTH: "21"
    INCREMENT_WIDTH: "7"
    NUMBER_OF_UNITS: "39"
    UNIT_INCREMENT_WIDTH: "2"
    NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE: "4"
    UNIT_ERROR_COUNTER_WIDTH: "3"
    STORE_ERRORS: "true"

load_fw:counter_21_increment_7_320mhz:
  <<: *load_fw_definition
  stage: load_fw
  needs:  
    - job: build:counter_21_increment_7_320mhz
  variables:
    <<: *counter_21_increment_7_320mhz_vars

test:counter_21_increment_7_320mhz:
  <<: *test_definition
  stage: test
  needs:  
    - job: load_fw:counter_21_increment_7_320mhz
  variables:
    <<: *counter_21_increment_7_320mhz_vars
    <<: *test_template_vars
    NUMBER_OF_STORED_ORBITS: "8"
    NUMBER_OF_COLLECTED_ORBITS: "4096"
    NITERATIONS: "1000"
    COUNTER_OVERFLOW_TEST : "0"

test:ovf_counter_21_increment_7_320mhz:
  <<: *test_definition
  stage: test
  needs:  
    - job: load_fw:counter_21_increment_7_320mhz
  variables:
    <<: *counter_21_increment_7_320mhz_vars
    <<: *test_template_vars
    NUMBER_OF_STORED_ORBITS: "8"
    NUMBER_OF_COLLECTED_ORBITS: "30000"
    NITERATIONS: "100"
    COUNTER_OVERFLOW_TEST: "1"

# counter 16 increment 5
build:counter_16_increment_5_320mhz:
  <<: *build_definition
  stage: build
  variables: &counter_16_increment_5_320mhz_vars
    <<: *build_template_vars
    ASYNC_MODE: "false"
    NBINS: "3564" 
    COUNTER_WIDTH: "16"
    INCREMENT_WIDTH: "5"
    NUMBER_OF_UNITS: "5"
    UNIT_INCREMENT_WIDTH: "3"
    NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE: "4"
    UNIT_ERROR_COUNTER_WIDTH: "3"
    STORE_ERRORS: "false"

load_fw:counter_16_increment_5_320mhz:
  <<: *load_fw_definition
  stage: load_fw
  needs:  
    - job: build:counter_16_increment_5_320mhz
  variables:
    <<: *counter_16_increment_5_320mhz_vars

test:counter_16_increment_5_320mhz:
  <<: *test_definition
  stage: test
  needs:
    - job: load_fw:counter_16_increment_5_320mhz
  variables:
    <<: *counter_16_increment_5_320mhz_vars
    <<: *test_template_vars
    NUMBER_OF_STORED_ORBITS: "8"
    NUMBER_OF_COLLECTED_ORBITS: "2048"
    NITERATIONS: "1000"
    COUNTER_OVERFLOW_TEST: "0"

test:ovf_counter_16_increment_5_320mhz:
  <<: *test_definition
  stage: test
  needs:
    - job: load_fw:counter_16_increment_5_320mhz
  variables:
    <<: *counter_16_increment_5_320mhz_vars
    <<: *test_template_vars
    NUMBER_OF_STORED_ORBITS: "8"
    NUMBER_OF_COLLECTED_ORBITS: "8192"
    NITERATIONS: "100"
    COUNTER_OVERFLOW_TEST: "1"

# cleaning stage
cleaning:
  stage: cleaning
  tags:  # tags to differentiate runners able to run the job
    - cmsbril-runner-sw
  script:
    - rm -rf public	

## stages defined here
stages:
  - build
  - load_fw
  - test
  - cleaning

