-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Superlayer PHI matcher data path
---- File       : slphi_matcher_datapath.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-06
-------------------------------------------------------------------------------
---- Description: Matches SEGMENTS from 2 phi superlayers,
----              generating reconstructed primitives.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.dt_buffers_pkg.all;
use work.filters_pkg.all;
use work.globalpath_pkg.all;
use work.memory_pkg.all;
use work.registers_pkg.all;
use work.sl_matcher_pkg.all;
use work.xil_memory_pkg.all;

entity SLPHI_MATCHER_DATAPATH is
  port (
    clk                 : in  std_logic;
    rst                 : in  std_logic;
    slPhiMatcherPathIn  : in  SLPhiMatchDataPathIn_t;
    slPhiMatcherPathOut : out SLPhiMatchDataPathOut_t
  );
end entity;

architecture Behavioural of SLPHI_MATCHER_DATAPATH is

  -- Modules interconnection signals
  signal slphiMatchOut          : SupLayPhiMatcherOut_t;
  signal primPathCalcOut        : PrimitivePathCalculatorOut_t;
  signal primGluFifoOut_stg1    : DTPrimitiveFifoOut_t;
  signal primGluFifoOut_stg2    : DTPrimitiveFifoOut_t;
  signal primDupFilterOut       : PrimitiveFilterOut_t;
  signal primChi2SplitFilterOut : PrimitiveFilterOut_t;
  signal ffPrimitiveOut         : DTPrimitiveFifoOut_t;
  signal primQualFilterOut      : PrimitiveFilterOut_t;
  --
  signal primDupFilterOut_postQ : PrimitiveFilterOut_t;
  signal ffPrimitiveOut_postQ   : DTPrimitiveFifoOut_t;
  --
  signal dummyff_rdEna   : std_logic;
  signal dummyff_wrEna   : std_logic;
  signal dummyff_empty   : std_logic;
  signal dummyff_full    : std_logic;
  signal dummyff_dataIn  : std_logic_vector(DTSEGMENT_ARR_SIZE + 1 downto 0);
  signal dummyff_dataOut : std_logic_vector(DTSEGMENT_ARR_SIZE + 1 downto 0);
  signal dummyff_dataOut_2lsb : std_logic_vector(1 downto 0);
  signal paired_segments_slv : 
        std_logic_vector(DTSEGMENT_ARR_SIZE - 1 downto 0);

  signal sl_phi3_x_offset : std_logic_vector(9 downto 0);
  --
  signal bx_counter_phimatch : std_logic_vector(11 downto 0);
  
begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
--   slPhiMatcherPathOut.primitive         <= primQualFilterOut.primitive;
--   slPhiMatcherPathOut.primValidFFOut_wr <= primQualFilterOut.fifoOut_wr;
  slPhiMatcherPathOut.primitive         <= primDupFilterOut_postQ.primitive;
  slPhiMatcherPathOut.primValidFFOut_wr <= primDupFilterOut_postQ.fifoOut_wr;
  slPhiMatcherPathOut.segmentFF_rd      <= slphiMatchOut.segmentFF_rd;
--   slPhiMatcherPathOut.ffPostDupPrimStat <= ffPrimitiveOut.stat;
  slPhiMatcherPathOut.ffPostDupPrimStat <= ffPrimitiveOut_postQ.stat;
  
  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  BXCNT_SYNC_PHIMATCH : VECTOR_SHIFT_REGISTER
  generic map (VECTOR_WIDTH => 12, SHIFT_STEPS => 2)
  port map (clk => clk, enable => '1', 
            vector_in  => slPhiMatcherPathIn.bx_counter,
            vector_out => bx_counter_phimatch
  );
  
  PHI_MATCHING: SUPLAY_PHI_MATCHER port map (
    clk           => clk, 
    rst           => rst,
    slphiMatchIn  => (
            control_regs     => slPhiMatcherPathIn.control_regs,
            bx_counter       => bx_counter_phimatch,
            fifo_segment_sl  => slPhiMatcherPathIn.segment_phi,
            fifo_empty_sl    => slPhiMatcherPathIn.segmentPhiFFIn_empty,
            primitiveFF_full => dummyff_full
    ),
    slphiMatchOut => slphiMatchOut
  );
  
  dummyff_dataIn <= slv_from_DTSegmentsArr(slphiMatchOut.paired_segments) & 
                                           slphiMatchOut.valid_segments;
  
--   GLUE_DUMMY_FIFO : XIL_FIFO_GENERIC_WRAPPER 
--   generic map (DATAWIDTH => DTSEGMENT_ARR_SIZE + 2, DEPTH => 512)
--   port map (
--     rst          => rst,
--     rdClk        => clk,
--     wrClk        => clk,
--     rdEna        => primPathCalcOut.pairSegmentFFIn_rd,
--     wrEna        => slphiMatchOut.primitiveFF_wr,
--     data_in      => dummyff_dataIn,
--     data_out     => dummyff_dataOut,
--     rd_count     => open,
--     wr_count     => open,
--     almost_empty => open,
--     almost_full  => open,
--     empty        => dummyff_empty,
--     full         => dummyff_full
--   );
                                           
--   GLUE_DUMMY_FIFO : DUMMY_FIFO_FFT
--   generic map (DATA_SIZE => DTSEGMENT_ARR_SIZE + 2)
  GLUE_DUMMY_FIFO : DUMMY_FIFO_FFT2 
  generic map (DATA_SIZE => DTSEGMENT_ARR_SIZE + 2, NUM_ITEMS => 4)
  port map (
    clk     => clk,
    rst     => rst,
    rdEna   => primPathCalcOut.pairSegmentFFIn_rd,
    wrEna   => slphiMatchOut.primitiveFF_wr,
    dataIn  => dummyff_dataIn,
    dataOut => dummyff_dataOut,
    empty   => dummyff_empty,
    full    => dummyff_full
  );
  
  paired_segments_slv <= dummyff_dataOut(DTSEGMENT_ARR_SIZE + 1 downto 2);
  --
  -- Synthesizer complains about "sl_phi3_x_offset" signal because it's 
  -- crossing a clock domain and is unable to adjust timing, even when a 
  -- "false path" is set in the constraints file. So I add a double 
  -- registration by the faster clock (the one commanding this module) in order 
  -- to try to avoid that complain, and, also, the lack of good timing 
  -- arrangement.  
  -- Really, "false path" should be enough because this signal is only for
  -- configuration, so it's not changed during trigger operation.
  --
  CLOCK_SYNCH_PHI_X_OFFSET : VECTOR_SHIFT_REGISTER
  generic map (VECTOR_WIDTH => 10, SHIFT_STEPS => 2)
  port map (
    clk        => clk,    
    enable     => '1', 
    vector_in  => 
        std_logic_vector(slPhiMatcherPathIn.control_regs.sl_phi3_x_offset),
    vector_out => sl_phi3_x_offset
  );
  
  PRIM_PATH_CALC : PRIMITIVE_PATH_CALCULATOR port map (
    clk             => clk,
    rst             => rst,
    primPathCalcIn  => (
          paired_segments       => dtSegmentsArr_from_slv(paired_segments_slv),
          valid_segments        => dummyff_dataOut_2lsb,
          sl_phi3_x_offset      => signed(sl_phi3_x_offset), 
          primValidFFOut_full   => primGluFifoOut_stg1.full,
          pairSegmentFFIn_empty => dummyff_empty
    ),
    primPathCalcOut => primPathCalcOut
  );
  
  dummyff_dataOut_2lsb <= dummyff_dataOut(1 downto 0);
  
  PRIM_GLUE_FIFO_STG1 : DTPRIMITIVE_GLUE_FIFO port map (
    clk     => clk,
    rst     => rst,
    fifoIn  => (
          primitive => primPathCalcOut.primitive,
          wrEna     => primPathCalcOut.primValidFFOut_wr,
          rdEna     => primChi2SplitFilterOut.fifoIn_rd
    ),
    fifoOut => primGluFifoOut_stg1
  );
  
  PRIM_CHI2_SPLIT_FILTER : PRIMITIVE_CHI2_SPLITTING_FILTER
  port map (
    clk               => clk,
    rst               => rst,
    hard_filtering    => slPhiMatcherPathIn.control_regs.prim_chisqr_hardfilter,
    chisqr_threshold  => 
          slPhiMatcherPathIn.control_regs.prim_chisqr_threshold,
    --
    primFilterIn      => (
          primitive     => primGluFifoOut_stg1.primitive,
          fifoIn_empty  => primGluFifoOut_stg1.empty,
          fifoOut_full  => primGluFifoOut_stg2.full,
          bx_counter    => bx_counter_phimatch
    ),
    primFilterOut => primChi2SplitFilterOut
  );
  
  PRIM_GLUE_FIFO_STG2 : DTPRIMITIVE_GLUE_FIFO port map (
    clk     => clk,
    rst     => rst,
    fifoIn  => (
          primitive => primChi2SplitFilterOut.primitive,
          wrEna     => primChi2SplitFilterOut.fifoOut_wr,
          rdEna     => primDupFilterOut.fifoIn_rd
    ),
    fifoOut => primGluFifoOut_stg2
  );
  
  PRIM_DUP_FILTER : PRIMITIVE_DUPLICATED_SHIFTING_FILTER
  generic map (MEM_ELEMENTS => 8)
  port map (
    clk               => clk,
    rst               => rst,
    primFilterIn      => (
          primitive     => primGluFifoOut_stg2.primitive,
          fifoIn_empty  => primGluFifoOut_stg2.empty,
          fifoOut_full  => ffPrimitiveOut.full,
          bx_counter    => bx_counter_phimatch
    ),
    primFilterOut => primDupFilterOut
  );
  --
  -- Here I don't use a "glue fifo" because primitive quality filter might
  -- use several clock cycle when it detects a group of primitives, belonging
  -- to the same family, that must be sent outside. In this case, it would
  -- stop the whole processing chain until those primitives were sent.
  -- With this ordinary primitive chain has a large enough buffer to cope
  -- with this situation without stopping this module.
  --
  PRIMITIVE_POSTDUP_INST : DTPRIMITIVE_FIFO port map(
    clkRd   => clk,
    clkWr   => clk,
    rst     => rst,
    fifoIn  => (
          primitive => primDupFilterOut.primitive,
          wrEna     => primDupFilterOut.fifoOut_wr,
          rdEna     => primQualFilterOut.fifoIn_rd
    ),
    fifoOut => ffPrimitiveOut
  );
  
  PRIM_QUAL_FILTER : PRIMITIVE_QUALITY_FILTER port map (
    clk               => clk,
    rst               => rst,
    expire_bx_num     => 
          slPhiMatcherPathIn.control_regs.primqfilt_wait_expire_num_bx,
    --
    primQualFilterIn  => (
          primitive     => ffPrimitiveOut.primitive,
          fifoIn_empty  => ffPrimitiveOut.empty,
--           fifoOut_full  => slPhiMatcherPathIn.primitiveFFOut_full,
          fifoOut_full  => ffPrimitiveOut_postQ.full,
          bx_counter    => bx_counter_phimatch
    ),
    primQualFilterOut => primQualFilterOut
  );

  -- Switch between glue/normal fifo by commenting appropriate lines before rst
  -- Glue fifo is ok because the consumer is generally faster than the producer
  
  --PRIMITIVE_POSTQUALITY_INST : DTPRIMITIVE_FIFO port map(
  PRIMITIVE_POSTQUALITY_INST : DTPRIMITIVE_GLUE_FIFO port map(
    --clkRd   => clk,
    --clkWr   => clk,
    clk     => clk,
    rst     => rst,
    fifoIn  => (
          primitive => primQualFilterOut.primitive,
          wrEna     => primQualFilterOut.fifoOut_wr,
          rdEna     => primDupFilterOut_postQ.fifoIn_rd
    ),
    fifoOut => ffPrimitiveOut_postQ
  );

  PRIM_DUP_FILTER_POSTQUALITY : PRIMITIVE_DUPLICATED_SHIFTING_FILTER
  generic map (MEM_ELEMENTS => 8)
  port map (
    clk               => clk,
    rst               => rst,
    primFilterIn      => (
          primitive     => ffPrimitiveOut_postQ.primitive,
          fifoIn_empty  => ffPrimitiveOut_postQ.empty,
          fifoOut_full  => slPhiMatcherPathIn.primitiveFFOut_full,
          bx_counter    => bx_counter_phimatch
    ),
    primFilterOut => primDupFilterOut_postQ
  );
  
end architecture;

