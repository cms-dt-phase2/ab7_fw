-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Memory components
---- Design unit: Single port FIFO
---- File       : xil_fifo_builtin_v7.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-08
-------------------------------------------------------------------------------
---- Description: A generic FIFO for two clock domains. Data output is ready
----              one cycle after enabling reading.
----              
----              This is a wrapper for a Virtex 7 family Xilinx-specific
----              FIFO macro, preserving the interface for my own data types.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
--
library UNISIM;
use UNISIM.vcomponents.all;
-- 
library UNIMACRO;
use UNIMACRO.vcomponents.all;
--
use work.commonfunc_pkg.all;

entity XIL_FIFO_BUILTIN_V7 is
  generic (
    -- Sets almost full threshold
    ALMOST_FULL_OFFSET      : bit_vector(15 downto 0):= X"0080";
    -- Sets the almost empty threshold     
    ALMOST_EMPTY_OFFSET     : bit_vector(15 downto 0):= X"0080";
    -- Target BRAM, "18Kb" or "36Kb" 
    FIFO_SIZE               : string  := "18Kb";
    -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
    DATAWIDTH               : integer := 36;  -- DATAWIDTH units -> bits
    DEPTH                   : integer := 512; -- DEPTH units -> num. words
    FIRST_WORD_FALL_THROUGH : boolean := TRUE
  );
  port (
    -- Clock and resets
    rst          : in  std_logic;
    rdClk        : in  std_logic;
    wrClk        : in  std_logic;
    -- Control and data signals
    rdEna        : in  std_logic;
    wrEna        : in  std_logic;
    dataIn       : in  std_logic_vector(DATAWIDTH - 1 downto 0);
    dataOut      : out std_logic_vector(DATAWIDTH - 1 downto 0);
    almost_empty : out std_logic;
    almost_full  : out std_logic;
    empty        : out std_logic;
    full         : out std_logic
  );
end entity;

architecture RTL of XIL_FIFO_BUILTIN_V7 is
  constant COUNT_WD : integer := getBusLines(DEPTH);

  signal rd_count : std_logic_vector(COUNT_WD - 1 downto 0);
  signal wr_count : std_logic_vector(COUNT_WD - 1 downto 0);
begin
  --
  -- FIFO_SYNC_MACRO: Synchronous First-In, First-Out (FIFO) RAM Buffer
  --                  Virtex-7
  -- Xilinx HDL Language Template, version 14.7
  --
  -- Note -  This Unimacro model assumes the port directions to be "downto". 
  --         Simulation of this model with "to" in the port directions 
  --         could lead to erroneous results.
  --
  -----------------------------------------------------------------
  -- DATA_WIDTH | FIFO_SIZE | FIFO Depth | RDCOUNT/WRCOUNT Width --
  -- ===========|===========|============|=======================--
  --   37-72    |  "36Kb"   |     512    |         9-bit         --
  --   19-36    |  "36Kb"   |    1024    |        10-bit         --
  --   19-36    |  "18Kb"   |     512    |         9-bit         --
  --   10-18    |  "36Kb"   |    2048    |        11-bit         --
  --   10-18    |  "18Kb"   |    1024    |        10-bit         --
  --    5-9     |  "36Kb"   |    4096    |        12-bit         --
  --    5-9     |  "18Kb"   |    2048    |        11-bit         --
  --    1-4     |  "36Kb"   |    8192    |        13-bit         --
  --    1-4     |  "18Kb"   |    4096    |        12-bit         --
  -----------------------------------------------------------------
  --
  FIFO_DUALCLOCK_MACRO_INST : FIFO_DUALCLOCK_MACRO
  generic map (
    DEVICE                  => "7SERIES", -- "VIRTEX5, "VIRTEX6", "7SERIES"
    -- Sets almost full threshold
    ALMOST_FULL_OFFSET      => ALMOST_FULL_OFFSET,   
    -- Sets the almost empty threshold     
    ALMOST_EMPTY_OFFSET     => ALMOST_EMPTY_OFFSET,   
    -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
    DATA_WIDTH              => DATAWIDTH,
    FIFO_SIZE               => FIFO_SIZE, -- Target BRAM, "18Kb" or "36Kb" 
    FIRST_WORD_FALL_THROUGH => FIRST_WORD_FALL_THROUGH)
  port map (
    ALMOSTEMPTY => almost_empty,-- 1-bit output almost empty
    ALMOSTFULL  => almost_full, -- 1-bit output almost full
    DO          => dataOut,     -- Output data, width -> DATA_WIDTH parameter
    EMPTY       => empty,       -- 1-bit output empty
    FULL        => full,        -- 1-bit output full
    RDCOUNT     => rd_count,    -- Output read count, width -> FIFO depth
    RDERR       => open,        -- 1-bit output read error
    WRCOUNT     => wr_count,    -- Output write count, width -> FIFO depth
    WRERR       => open,        -- 1-bit output write error
    DI          => dataIn,      -- Input data, width -> DATA_WIDTH parameter
    RDCLK       => rdClk,       -- 1-bit input clock
    RDEN        => rdEna,       -- 1-bit input read enable
    RST         => rst,         -- 1-bit input reset
    WRCLK       => wrClk,       -- 1-bit input clock
    WREN        => wrEna        -- 1-bit input write enable
  );
end architecture;
