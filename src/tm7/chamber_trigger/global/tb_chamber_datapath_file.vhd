library IEEE;
USE IEEE.std_logic_1164.all;
USE IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use STD.textio.all;
use STD.standard.all;
--
use work.commonfunc_pkg.all;
use work.dt_common_pkg.all;
use work.dt_constants_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.globalpath_pkg.all;
use work.simulation_pkg.all;
use work.float_point_pkg.all;

entity TB_CHAMBER_DATAPATH_FILE is
end entity;

architecture Behavior of TB_CHAMBER_DATAPATH_FILE is 

  file input_file  : text open read_mode  is "/home/jm/current_test/input-chamber.txt";
  file output_file : text open write_mode is "/home/jm/current_test/output-chamber.txt";

  type t_integer_array is array(integer range <> ) of integer;  

  constant BLANK_LINE  : string := "";
  constant WAIT_NUM_BX : std_logic_vector(11 downto 0) 
--                        := std_logic_vector(to_unsigned(50, 12));
                       := std_logic_vector(to_unsigned(30, 12));

  ------------------------------------------------
  -- Auxiliary signals
  --
  signal rst             : std_logic := '0';
  signal clk             : std_logic := '0';
  signal mix_clk         : std_logic := '0';
  signal lhc_clk         : std_logic := '0';
  signal enable          : std_logic := '0';
  signal se_fini         : std_logic := '0';
  signal wait_reading    : std_logic := '0';
  --
  signal bx_counter      : std_logic_vector(11 downto 0) := (others => '0');
  signal trigger_bx      : std_logic_vector(11 downto 0) := (others => '0');
  --                     
  signal rawHit          : DTRawHit_t := DT_RAWHIT_NULL;
  signal rawHitFFIn_wr   : std_logic_vector(1 downto 0) := (others => '0');
  signal primitiveFFOut_rd : std_logic := '0';
  --
  signal data_selector   : std_logic_vector(1 downto 0);

  ------------------------------------------------
  -- Component signals
  --
  signal chamGlobDatPathIn    : ChGlobalDataPathIn_t;
  signal chamGlobDatPathOut   : ChGlobalDataPathOut_t;

begin

  data_selector <= CHAMBER_OUTPUT;
--   data_selector <= SL_PHI0_OUTPUT;
--   data_selector <= SL_PHI1_OUTPUT;

  -- Instantiate the Unit Under Test (UUT)
  UUT : CHAMBER_DATAPATH port map (
    clk_fifo_rd        => clk, 
    clk_fifo_wr        => clk, 
    clk_mixer          => mix_clk, 
--     clk_mixer          => clk, 
    clk_analyzer       => clk, 
    clk_dup_filter     => clk, 
    clk_qenhan_filter  => clk, 
    clk_sl_matcher     => clk, 
    rst                => rst, 
    chamGlobDatPathIn  => chamGlobDatPathIn,
    chamGlobDatPathOut => chamGlobDatPathOut
  );

  chamGlobDatPathIn.rawHitPhi(0)      <= rawHit;
  chamGlobDatPathIn.rawHitPhi(1)      <= rawHit;
  chamGlobDatPathIn.rawHitFFPhi_wr    <= rawHitFFIn_wr;
  chamGlobDatPathIn.primitiveFFOut_rd <= primitiveFFOut_rd;
  chamGlobDatPathIn.reset_overflow    <= '0';
  chamGlobDatPathIn.bx_counter        <= bx_counter;
  chamGlobDatPathIn.control_regs      <= (
      bx_time_convert_ena             => '0',
      bx_time_tolerance               => std_logic_vector(to_unsigned(30, 8)),
      --
      -- "Chi square" thresholds have 10 bits for integer part in "mm ^ 2" 
      -- and 6 LSB for fractional part.
      -- Basically, it's necessary to multiply the limit value in "mm ^2" by
      -- 64.
      --
      chisqr_threshold                => std_logic_vector(to_unsigned(640, 16)),
      tanphi_x4096_threshold          => (others => '1'),
--         tanphi_x4096_threshold => std_logic_vector(to_unsigned(4096, 14)), --1
--         tanphi_x4096_threshold => std_logic_vector(to_unsigned(2458, 14)), -- 0.6
      qfilt_wait_expire_num_bx_stg1   => std_logic_vector(to_unsigned(8, 6)), 
      qfilt_wait_expire_num_bx_stg2   => std_logic_vector(to_unsigned(4, 6)), 
      --
      slphi_match_bypass_tanphi_match => '0',
      slphi_match_tanphi_hardfilter   => '0',
--       slphi_match_tanphi_x4096_thr    => std_logic_vector(to_unsigned(2048, 14)),
      slphi_match_tanphi_x4096_thr    => std_logic_vector(to_unsigned(455, 14)),
      slphi_match_bx_wait_read_offset => std_logic_vector(to_unsigned(4, 9)),
      slphi_match_mem_bx_wait         => std_logic_vector(to_unsigned(22, 6)),
      --
      prim_chisqr_hardfilter          => '0',
      prim_chisqr_threshold           => std_logic_vector(to_unsigned(5200, 16)),
--         prim_chisqr_threshold           => (others => '1'),
      --
      primqfilt_wait_expire_num_bx    => std_logic_vector(to_unsigned(8, 6)),
      --
      ouput_trg_chamber_selector      => data_selector,
      --
--         sl_phi3_x_offset                => to_signed(90, 10),
--       sl_phi3_x_offset                => to_signed(-42, 10),
      sl_phi3_x_offset                => to_signed(21, 10),
      --
      radio_mult_sin_delta_cmsphi     => UFLOAT32_ZERO, 
      radio_mult_cos_delta_cmsphi     => UFLOAT32_ZERO
  );

  -- Clock process
  LHC_CLK_PROCESS : process
  begin
    lhc_clk <= not lhc_clk;
    wait for 12.5 ns;
  end process;

  -- Bunch-crossing counter process
--   BX_COUNTER_PROCESS : process(lhc_clk, rst, enable)
  BX_COUNTER_PROCESS : process(lhc_clk, rst)
  begin
    if lhc_clk'event and lhc_clk = '1' then
      if rst = '1' or unsigned(bx_counter) = to_unsigned(MAX_BX_IDX, 12) then
        bx_counter <= (others => '0');
      else
        bx_counter <= bx_counter + '1';
      end if;
    end if;
  end process;

  -- Clock process
  CLK_PROCESS : process
  begin
    clk <= not clk;
    wait for 2.775 ns;      -- 180 MHz
  end process;

  MIX_CLK_PROCESS : process
  begin
    mix_clk <= not mix_clk;
--     wait for 2.27 ns;       -- 220 MHz
--     wait for 2.44 ns;       -- 205 MHz
    wait for 2.775 ns;       -- 180 MHz
  end process;

  -- Reading results process
  RD_FIFO_PROCESS : process(clk, rst, enable, data_selector)
    variable primitive : DTPrimitive_t;
    variable aux_line  : line;
  begin
    if clk'event and clk = '1'then
      if rst = '1' then
        primitiveFFOut_rd <= '0';
      elsif enable = '1' then

        -- Only one cycle active.
        if primitiveFFOut_rd = '1' then
          primitiveFFOut_rd <= '0';

          -- Useful aliases
          primitive := chamGlobDatPathOut.primitive;

          -- Write data line to file
          if data_selector /= SL_PHI0_OUTPUT then
            writeline(output_file, 
                  build_segment_text_line(primitive.paired_segments(1)));
          end if;

          if data_selector /= SL_PHI1_OUTPUT then
            writeline(output_file, 
                  build_segment_text_line(primitive.paired_segments(0)));
          end if;
            
          if data_selector = CHAMBER_OUTPUT then
            writeline(output_file, build_primdata_text_line(primitive));
            write(aux_line, string'(" "));
            writeline(output_file, aux_line);
          end if;

        elsif chamGlobDatPathOut.primitiveFFOut_empty = '0' then
          primitiveFFOut_rd <= '1';
        end if;

      end if;
    end if;
  end process;

  -- Writing inputs process
  WR_DATA_PROC : process(clk, rst, enable, bx_counter)
    variable row_in      : line;
    variable v_data_read : t_integer_array(1 to 4);
    variable super_layer : std_logic_vector(1 downto 0);

  begin
    if clk'event and clk = '1' then
      if rst = '1' then
        wait_reading  <= '0';
        rawHitFFIn_wr <= (others => '0');
        trigger_bx    <= (others => '0');
        v_data_read   := (others => 0);

      elsif enable = '1' then

        if(not endfile(input_file)) and wait_reading = '0' then
          readline(input_file, row_in);   -- Read data line from file

          if string(row_in) /= BLANK_LINE then
            --
            -- Split data into different fields:
            --    Assumed:  1 -> super_layer_id (only to select input fifo)
            --              2 -> layer_id
            --              3 -> channel_id
            --              4 -> TDC time
            --
            for kk in 1 to 4 loop
              read(row_in, v_data_read(kk));
            end loop;

            super_layer := std_logic_vector(to_unsigned(v_data_read(1), 2));
            
            if super_layer = "00" then
              rawHitFFIn_wr(0) <= '1';
              rawHitFFIn_wr(1) <= '0';
            elsif super_layer = "10" then
              rawHitFFIn_wr(0) <= '0';
              rawHitFFIn_wr(1) <= '1';
            else
              rawHitFFIn_wr(0) <= '0';
              rawHitFFIn_wr(1) <= '0';
            end if;

            rawHit <= (
              tdc_time       => std_logic_vector(to_unsigned(v_data_read(4), 17)),
              channel_id     => std_logic_vector(to_unsigned(v_data_read(3), 7)),
              layer_id       => std_logic_vector(to_unsigned(v_data_read(2), 2)),
              super_layer_id => super_layer
            );

          else
            if unsigned(bx_counter + WAIT_NUM_BX) >= to_unsigned(MAX_BX_IDX, 12)
            then 
              trigger_bx <= bx_counter + WAIT_NUM_BX - 
                            std_logic_vector(to_unsigned(MAX_BX_IDX, 12));
            else 
              trigger_bx <= bx_counter + WAIT_NUM_BX;
            end if;
          
            wait_reading  <= '1';
            rawHitFFIn_wr <=  (others => '0');
          end if;

        else
          rawHitFFIn_wr <=  (others => '0');
          se_fini       <= '1';
        end if;

        if wait_reading = '1' and trigger_bx = bx_counter 
        then wait_reading <= '0';
        end if;

      end if;
    end if;
  end process;

  CONTROL_PROCESS: process
  begin
    enable <= '0';

    -- Initial RESET
    rst <= '1';
    wait for 300 ns; -- hold reset
    rst <= '0';
    
--     wait until 300 ns;
    wait until bx_counter = std_logic_vector(to_unsigned(10, 12));

    enable <= '1';

    wait;
  end process;

end architecture;
