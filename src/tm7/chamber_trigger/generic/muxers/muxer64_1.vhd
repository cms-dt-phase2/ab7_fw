-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Multiplexers components
---- Design unit: 64:1 bits multiplexer
---- File       : muxer64_1.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: Multiplexer 64:1 bit implemented to optimize synthesis 
----              resources and speed.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
use work.muxers_pkg.all;

entity MUXER64_1 is
  port (
    sel   : in  std_logic_vector(5 downto 0);
    d_in  : in  std_logic_vector(63 downto 0);
    d_out : out std_logic
  );
end entity;

architecture Behavioural of MUXER64_1 is
  signal int_d_out: std_logic_vector(3 downto 0);
begin
  MUX_CHAIN_GEN : for i in 0 to 3 generate
    MUX16_INST : MUXER16_1 port map (
      sel   => sel(3 downto 0),
      d_in  => d_in((16 * (i+1)) - 1 downto 16 * i),
      d_out => int_d_out(i)
    );
  end generate;

  MUX_OUT : MUXER4_1 port map (
    sel   => sel(5 downto 4),
    d_in  => int_d_out,
    d_out => d_out
  );
  
end architecture;
