-- ---------------------------------------------------------------------------------------
-- Stores data received non-monotonically and performs readout of this data
-- �lvaro Navarro, CIEMAT
-- ---------------------------------------------------------------------------------------
-- All the data received is stored in the main RAM in the order received
-- Incoming data can come un-ordered, so this module has to take this into account
-- When the L1A arrives, we want to have a pointer to where to start reading from the RAM
-- This address is not necessarily the address of the first data with bx in the window, because a later hit may have been
-- stored in a previous address due to the non-monotonicity.
-- So what is done is: whenever data arrives whose BX is higher than any other previously (recently) received, 
-- its address is stored in Startpointer_FIFO. It remains at the FIFO's first word until we are sure not to need it anymore:
--      local_bx - latency > input_bx
-- When the L1A arrives, the address to start searching is the one at the first word of this Startpointer_FIFO, and this info
-- is stored in the L1A FIFO waiting for processing
-- SUMMARY OF MEMORIES HYERARCHY:
--              ram_inst                : mem_addr --> ( input_bx & data )
--              startpointer_fifo_inst  : ( mem_addr & input_bx ) [12+MEMDEPTH bits]
--              l1a_fifo_inst           : ( mem_addr, l1a_evn ) [MEMDEPTH + 24 bits]
-- ---------------------------------------------------------------------------------------
-- TIMING AND CONSTRAINING PARAMETERS
-- BX of the data and the current BX counters are approximately in sync (except for a fixed offset)
-- The data corresponding to BX n will reach this module after a certain variable delay:
-- (n + mindelay, n + maxdelay)
-- It is important to make sure that the L1A must arrive later than the entirety of the search window
-- latency > window + maxdelay + max_disorder + [some margin]
--
-- startpointer_cooldown. In order to see whether a BX is newer than any other previously received, it is compared to the latest
-- value that was stored in the startpointer_fifo. But if there is silence for a long time, it may not be clear only from this comparison.
-- so if a long time has passed (startpointer_cooldown) since the last write, it is assumed that the bx is newer than what is already stored.
--
-- There is no control of the output fifo occupancy. If full, data will just overflow without notice
-- ---------------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.top_decl.all;
use work.bo2.all;
use work.BXarithmetic.all;
use work.minmax.all;

entity nonmonotonic_data_window_read is
  generic (
    DATA_WIDTH      : positive := 13;
    EVN_WIDTH       : positive := 24;
    MEMDEPTH        : positive := 16;
    SPEAK_SLOWLY    : positive := 1; -- When valid data is output, it will remain this many cycles frozen
    LHC_BUNCH_COUNT : natural := 3564
  );
  port(
    -- readout configuration (semi-static)
    latency                 : in unsigned( 11 downto 0 );
    window                  : in unsigned( 11 downto 0 );
    procrastination         : in unsigned( 11 downto 0 );
    startpointer_cooldown   : in unsigned( 11 downto 0 );
    max_disorder            : in unsigned( 11 downto 0 );
    -- ttc inputs: ttc_clk clocked
    ttc_clk                 : in std_logic; -- TTC and input data
    ttc_rst                 : in std_logic; -- for all clocks
    l1a                     : in std_logic;
    evt_ctr                 : in std_logic_vector( EVN_WIDTH-1 downto 0 );
    bunch_ctr               : in std_logic_vector( 11 downto 0 );
    -- input data
    input_clk               : in std_logic;
    input_wr                : in std_logic;
    input_bx                : in std_logic_vector( 11 downto 0 );
    input_data              : in std_logic_vector( DATA_WIDTH-1 downto 0 );
    -- output ports: rdclk clocked
    output_clk              : in std_logic;
    output_inevent          : out boolean;
    output_valid            : out boolean; -- marks pauses due to words being read but not needed (out of window due to non-monotonicity)
    output_evn              : out std_logic_vector( EVN_WIDTH-1 downto 0 );
    output_bx               : out std_logic_vector( 11 downto 0 );
    output_data             : out std_logic_vector( DATA_WIDTH-1 downto 0 )
  );
end entity;

architecture Behavioral of nonmonotonic_data_window_read is
  -- RAM
  signal wadd, radd : unsigned(MEMDEPTH-1 downto 0) := (others => '0');
  signal mem_rden : std_logic;
  signal mem_din, mem_dout: std_logic_vector(DATA_WIDTH + 11 downto 0);
  -- breakout
  signal mem_bx   : unsigned( 11 downto 0 );
  signal mem_data : std_logic_vector( DATA_WIDTH-1 downto 0 );

  -- StartPointer FIFO
  constant STARTPOINTER_FF_WIDTH : integer := MEMDEPTH + 12;
  signal startpointer_ff_di, startpointer_ff_do : std_logic_vector(STARTPOINTER_FF_WIDTH-1 downto 0);
  signal startpointer_ff_empty, startpointer_ff_wren, startpointer_ff_rden : std_logic;
  signal startpointer_rdctr,startpointer_wrctr : std_logic_vector(8 downto 0);
  
  -- Procrastination memory
  constant PROC_MEM_WIDTH : integer := 1 + EVN_WIDTH + 12;
  signal procr_mem_wadd, procr_mem_radd : std_logic_vector(11 downto 0); 
  signal procr_mem_din, procr_mem_dout  : std_logic_vector(PROC_MEM_WIDTH-1 downto 0);
  signal procr_mem_valid : std_logic;
  -- breakout
  signal procr_mem_l1a : std_logic;
  signal procr_mem_evn : std_logic_vector(EVN_WIDTH-1 downto 0);
  signal procr_mem_bcn : std_logic_vector(11 downto 0);
  
  --L1A FIFO
  constant L1AFIFO_WIDTH : integer := 1 + MEMDEPTH + EVN_WIDTH + 12;
  signal l1afifo_do,l1afifo_di : std_logic_vector(L1AFIFO_WIDTH-1 downto 0);
  signal l1afifo_empty, l1afifo_rden, l1afifo_wren : std_logic := '0';
  signal l1aff_rdctr, l1aff_wrctr : std_logic_vector(9 downto 0);
  -- breakout
  signal l1afifo_bcn : unsigned(11 downto 0);
  signal l1afifo_evn : std_logic_vector(EVN_WIDTH-1 downto 0);
  signal l1afifo_add : std_logic_vector(MEMDEPTH-1 downto 0);
  signal l1afifo_val : std_logic;
  
  -- other
  signal inevent : boolean;

begin
  --------------------------------------
  -- RAM that stores all the hits information
  --------------------------------------
  ram_inst : entity work.multi_bram_sdp 
  generic map(
    DATA_WIDTH      => 12 + DATA_WIDTH,
    DEPTH_LOG2      => MEMDEPTH,
    MIN_OUT_MUX     => TRUE
    )
  port map(
    wclk  => input_clk,
    wen   => input_wr,
    wadd  => std_logic_vector(wadd),
    din   => mem_din,
    
    rclk  => output_clk,
    ren   => mem_rden,
    radd  => std_logic_vector(radd),
    dout  => mem_dout
    );
  
  mem_din   <= input_data & input_bx;
  mem_data  <= mem_dout( mem_dout'high downto 12 );
  mem_bx    <= unsigned( mem_dout( 11 downto 0 ) );

  --------------------------------------
  -- Preparation of main RAM's wadd and input to startpointer_fifo
  --------------------------------------
  process(input_clk)
    variable last_startpointer  : unsigned(11 downto 0) := (others => '0');
    variable startpointer_hot   : unsigned(startpointer_cooldown'range) := (others => '0');
    variable startpointer_wr    : boolean;
  begin
    if rising_edge(input_clk) then
      wadd <= wadd + bo2int(input_wr='1');
      -- Current data will be an startpointer if it is higher than previous startpointer or if the previous startpointer is older than the cooldown
      -- > LHC_BUNCH_COUNT / 2 actually evaluates if it is negative, because of the circular arithmetic of BXminus
      startpointer_wr := (input_wr = '1') and ( (BXminus(unsigned(input_bx), last_startpointer) < LHC_BUNCH_COUNT / 2) or (startpointer_hot = 0 ) );
      if startpointer_wr then
        last_startpointer := BXplus(unsigned(input_bx),to_unsigned(1,12)); -- add one so that only later hits are considered
        startpointer_hot := startpointer_cooldown;
      elsif startpointer_hot > 0 then startpointer_hot := startpointer_hot - 1;
      end if;
      startpointer_ff_wren <= bo2sl(startpointer_wr);
      startpointer_ff_di <= std_logic_vector(wadd) & input_bx;
    end if;
  end process;

  --------------------------------------
  -- FIFO with next (bx -> start pointer)
  --------------------------------------
  -- 18 < STARTPOINTER_FF_WIDTH = MEMDEPTH + 12 < 37, so with one BR18, depth is going to be 512, which is larger than the expected latency
  Startpointer_FIFO_inst : entity work.qwfifo
  generic map (
    DATA_WIDTH => STARTPOINTER_FF_WIDTH,
    DEPTH_LOG2 => 9 )
  port map (
    ALMOSTEMPTY => open,
    ALMOSTFULL  => open,
    RDCOUNT     => startpointer_rdctr,
    WRCOUNT     => startpointer_wrctr,
      
    RST         => ttc_rst,               -- 1-bit input reset
      
    WRCLK       => input_clk,             -- 1-bit input write clock
    FULL        => open,                  -- 1-bit output full
    WREN        => startpointer_ff_wren,  -- 1-bit input write enable
    DI          => startpointer_ff_di,    -- Input data, width defined by DATA_WIDTH parameter
      
    RDCLK       => ttc_clk,               -- 1-bit input read clock
    EMPTY       => startpointer_ff_empty, -- 1-bit output empty
    RDEN        => startpointer_ff_rden,  -- 1-bit input read enable
    DO          => startpointer_ff_do     -- Output data, width defined by DATA_WIDTH parameter
    );

  process(ttc_clk)
    variable window_start, startpointer_window_offset: unsigned(11 downto 0);
  begin
    if rising_edge(ttc_clk) then
      window_start := BXminus( unsigned(procr_mem_bcn) , latency );
      startpointer_window_offset :=  BXminus( unsigned(startpointer_ff_do(11 downto 0)) , window_start );
      -- > LHC_BUNCH_COUNT / 2 actually evaluates if it is negative, because of the circular arithmetic of BXminus
      startpointer_ff_rden <= bo2sl( (startpointer_ff_empty = '0') and (startpointer_window_offset > LHC_BUNCH_COUNT / 2 ) ) or (not procr_mem_valid); 
    end if;
  end process;

  -----------------------------------------
  -- Procrastination and FIFO for the L1As 
  -----------------------------------------
  procr_mem_inst : entity work.multi_bram_sdp 
  generic map(
    DATA_WIDTH      => PROC_MEM_WIDTH,
    DEPTH_LOG2      => 12,
    MIN_OUT_MUX     => TRUE
    )
  port map(
    wclk  => ttc_clk,
    wen   => '1',
    wadd  => procr_mem_wadd,
    din   => procr_mem_din,
    
    rclk  => ttc_clk,
    ren   => '1',
    radd  => procr_mem_radd,
    dout  => procr_mem_dout
    );
  
  procr_mem_wadd <= bunch_ctr;
  procr_mem_din  <= l1a & evt_ctr(EVN_WIDTH-1 downto 0) & bunch_ctr;
  -- minimum procrastination is 3 to be safe and be sure to read a value recently written, not from 1 orbit ago
  procr_mem_radd <= std_logic_vector( BXminus( unsigned( bunch_ctr ), maximum(procrastination, to_unsigned(3,12)) ) );
  
  procr_mem_bcn <= procr_mem_dout(11 downto 0);
  procr_mem_evn <= procr_mem_dout(PROC_MEM_WIDTH - 2 downto 12);
  procr_mem_l1a <= procr_mem_dout(PROC_MEM_WIDTH - 1);
  
  process(ttc_clk)
    variable procr_mem_warmup : integer range LHC_BUNCH_COUNT - 1 downto 0;
  begin
    if rising_edge(ttc_clk) then
      if ttc_rst = '1' then
        procr_mem_warmup := LHC_BUNCH_COUNT - 1;
        procr_mem_valid <= '0';
      else
        if procr_mem_warmup /= 0 then
          procr_mem_warmup := procr_mem_warmup - 1;
        else
          procr_mem_valid <= '1';
        end if;
      end if;
    end if;
  end process;
  
  -- width will be between 37 and 72
  l1a_fifo_inst : entity work.qwfifo
  generic map (
      DATA_WIDTH => L1AFIFO_WIDTH,
      DEPTH_LOG2 => 10)
  port map (
      ALMOSTEMPTY => open,
      ALMOSTFULL  => open,
      RDCOUNT     => l1aff_rdctr,
      WRCOUNT     => l1aff_wrctr,
        
      RST         => ttc_rst,             -- 1-bit input reset
        
      WRCLK       => ttc_clk,             -- 1-bit input write clock
      FULL        => open,                -- 1-bit output full
      WREN        => l1afifo_wren,        -- 1-bit input write enable
      DI          => l1afifo_di,          -- Input data, width defined by DATA_WIDTH parameter
        
      RDCLK       => output_clk,              -- 1-bit input read clock
      EMPTY       => l1afifo_empty,       -- 1-bit output empty
      RDEN        => l1afifo_rden,        -- 1-bit input read enable
      DO          => l1afifo_do           -- Output data, width defined by DATA_WIDTH parameter
      );
      
  l1afifo_wren <= procr_mem_l1a and procr_mem_valid;

  l1afifo_di(   11 downto 0                                   ) <= procr_mem_bcn;
  l1afifo_di(   12+EVN_WIDTH-1 downto 12                      ) <= procr_mem_evn;
  l1afifo_di(   12+EVN_WIDTH+MEMDEPTH-1 downto 12+EVN_WIDTH   ) <= startpointer_ff_do(startpointer_ff_do'high downto 12);
  l1afifo_di(   l1afifo_do'high                               ) <= startpointer_ff_empty;
  
  l1afifo_bcn <= unsigned(  l1afifo_do(   11 downto 0                                   ) );
  l1afifo_evn <=            l1afifo_do(   12+EVN_WIDTH-1 downto 12                      ) ;
  l1afifo_add <=            l1afifo_do(   12+EVN_WIDTH+MEMDEPTH-1 downto 12+EVN_WIDTH   ) ;
  l1afifo_val <=      not   l1afifo_do(   l1afifo_do'high                               ) ; -- marks startpointer_ff was empty when l1a arrived
  
  -----------------------------------------
  -- Reading and creating the output event
  -----------------------------------------
  process(output_clk)
    variable window_start, extwindow_start, mem_bx_offset, extmem_bx_offset : unsigned(11 downto 0);
    variable inactive_timer : positive range SPEAK_SLOWLY downto 0 := 0;
    variable radd_wadd_equal_pipe : boolean := True;
  begin
    if rising_edge(output_clk) then
      if ttc_rst = '1' then
        l1afifo_rden <= '0';
        mem_rden <= '0';
        inactive_timer := 0;
        inevent <= false;
        output_valid <= false;
        radd <= (others => '0');
        radd_wadd_equal_pipe := True;
      else
        -- only one data has to be read from the l1afifo (so has to be zeroed also in inactive cycles)
        l1afifo_rden <= '0';
        -- mem_bx_offset = how deep we are into the readout window
        mem_bx_offset :=   BXminus( mem_bx , window_start );
        -- extmem_bx_offset = how deep we are into the extended window, 
        -- i.e., the window of data we may encounter interleaved with the data we want to read
        extmem_bx_offset :=  BXminus( mem_bx , extwindow_start );
        
        -- radd is incremented only during active cycles,
        -- except when the inactivity is a cycle at the beginning of a SPEAK_SLOWLY = 1 event, introduced to wait for the data to come
        radd <= radd + bo2int( inactive_timer = 0 or SPEAK_SLOWLY = 1 );
        mem_rden <= '1';

        if inactive_timer > 0 then
          inactive_timer := inactive_timer - 1;
        else
          inactive_timer := SPEAK_SLOWLY - 1;
          
          -- Always outputs data, but there is a valid flag
          output_data <= mem_data;
          output_bx <= std_logic_vector( mem_bx );

          output_valid <= false;
          
          if inevent then
            if (mem_bx_offset < window) and (not radd_wadd_equal_pipe) then
              output_valid <= true;
            elsif (extmem_bx_offset >= window + 2*max_disorder) or (radd_wadd_equal_pipe) then
              inevent <= false;
            -- else case would correspond to data inside extended window but outside of readout window
            end if;
            
          elsif l1afifo_empty = '0' then
            l1afifo_rden <= '1';
            if l1afifo_val = '1' then
              inevent <= true;
              output_evn <= l1afifo_evn;
              window_start    :=  BXminus( l1afifo_bcn , latency );
              extwindow_start :=  BXminus( window_start , max_disorder );
              radd <= unsigned(l1afifo_add);
              -- inactive_timer must be at least 1, even for the case of SPEAK_SLOWLY = 1, because the memory is "cold" 
              inactive_timer := inactive_timer + bo2int( inactive_timer = 0 );
            end if;
          end if; 
          
        end if; -- active/inactive cycle
        
        radd_wadd_equal_pipe := (radd = wadd);

      end if; -- rst vs regular operation
    end if; -- rising_edge
  end process;
  
  output_inevent <= inevent;

end architecture;
