-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Super layers phi segment matcher
---- File       : suplay_phi_matcher.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-03
-------------------------------------------------------------------------------
---- Description: Segment matcher for super layers phi core
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.dt_constants_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.encoders_pkg.all;
use work.sl_matcher_pkg.all;

entity SUPLAY_PHI_MATCHER is
  port (
    clk           : in  std_logic;
    rst           : in  std_logic;
    slphiMatchIn  : in  SupLayPhiMatcherIn_t;
    slphiMatchOut : out SupLayPhiMatcherOut_t
  );
end entity;

architecture TwoProcess of SUPLAY_PHI_MATCHER is

  constant BX3563 : unsigned(11 downto 0) := to_unsigned(MAX_BX_IDX, 12);
  constant ZERO32 : std_logic_vector(31 downto 0) := (others => '0');

  -- 2 wait-states until memory module suplies the requested information.
  constant MAX_MEM_NUM_WS : unsigned(2 downto 0) := to_unsigned(2, 3);
  
  -- FSM states
  type state_type is (STBY, WAIT_BX_WINDOW_OFFSET, LATCH_WINDOW_WS,
                      RETRIEVE_DATA_FROM_FIFO, SELECT_BX_BUFFER, 
                      VALIDATE_BX_BUFFER, GET_SEG_FROM_MEM, 
                      SELECT_NEXT_ITEMS, VALIDATE_SEGMENTS, 
                      COMBINE_SEGMENTS, SEND_SECOND_SEGMENT);

  type SupLaySegmMemOut_arr_t is array (0 to 1) of SupLaySegmMemOut_t;
  type ItemPtr_arr_t          is array (0 to 1) of SegPointer_t;

  -- Internal signals to be registered.
  type reg_type is record
    state               : state_type;
    mem_rd              : std_logic;
    start_storing       : std_logic;
    bx_counter_lsb      : std_logic;
    tan_in_range        : std_logic;
    bx_good             : std_logic;
    mem_ws_count        : unsigned(2 downto 0);
    bx_wait_cnt         : std_logic_vector(9 downto 0);
    bx_window_center    : signed(12 downto 0);
    bx_window_low       : std_logic_vector(11 downto 0);
    bx_window_high      : std_logic_vector(11 downto 0);
    buffers_to_process  : std_logic_vector(BX_WINDOW_WIDTH - 1 downto 0);
    seg_item_ptr        : ItemPtr_arr_t;                 
  end record;

  constant regDefault : reg_type := (
    state               => STBY,
    mem_rd              => '0',
    start_storing       => '0',
    bx_counter_lsb      => '0',
    tan_in_range        => '0',
    bx_good             => '0',
    mem_ws_count        => (others => '0'),
    bx_wait_cnt         => (others => '0'),
    bx_window_center    => (others => '0'),
    bx_window_low       => (others => '0'),
    bx_window_high      => (others => '0'),
    buffers_to_process  => (others => '0'),
    seg_item_ptr        => (others => (others => '0'))
  );
  signal reg, nextReg : reg_type := regDefault;

  signal fifo_wr      : std_logic;
  signal send_sl0     : std_logic;
  signal send_sl1     : std_logic;
  signal rst_memories : std_logic;

  -- Components inter-connection signals
  signal slSegMemOut : SupLaySegmMemOut_arr_t;
  --
  signal next_buf      : std_logic := '0';
  signal valid_buf     : std_logic;
  signal bx_buffer_ptr : std_logic_vector(4 downto 0);

  -- Alisases 
  signal segment_sl    : DTSegment_arr_t;
  signal fifo_empty_sl : std_logic_vector(1 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  slphiMatchOut.paired_segments(1) 
          <= slSegMemOut(1).segment   when send_sl1 = '1' else DT_SEGMENT_NULL;
          
  slphiMatchOut.valid_segments(1) 
          <= slSegMemOut(1).seg_valid when send_sl1 = '1' else '0';
  --
  slphiMatchOut.paired_segments(0) 
          <= slSegMemOut(0).segment   when send_sl0 = '1' else DT_SEGMENT_NULL;

  slphiMatchOut.valid_segments(0)  
          <= slSegMemOut(0).seg_valid when send_sl0 = '1' else '0';

  --
  slphiMatchOut.segmentFF_rd <= slSegMemOut(1).fifo_rd & 
                                slSegMemOut(0).fifo_rd;
                                
  slphiMatchOut.primitiveFF_wr <= fifo_wr;

  ---------------------------------------------------------------
  -- Aliases
  --
  segment_sl    <= slphiMatchIn.fifo_segment_sl;
  fifo_empty_sl <= slphiMatchIn.fifo_empty_sl;
  
  ---------------------------------------------------------------
  -- Combinational process
  --
  COMB : process(reg, slphiMatchIn, fifo_empty_sl, segment_sl, slSegMemOut,
                 valid_buf, bx_buffer_ptr)
    variable int_bx_buffer_ptr   : integer;
    variable bx_window_low       : signed(12 downto 0);
    variable bx_window_high      : signed(12 downto 0);
    variable bx_window_center    : signed(12 downto 0);
  begin
    -- Default values
    nextReg      <= reg;
    fifo_wr      <= '0';
    send_sl0     <= '0';
    send_sl1     <= '0';
    rst_memories <= '0';
    next_buf     <= '0';
    
    -- General registers
    nextReg.bx_counter_lsb <= slphiMatchIn.bx_counter(0);
    
    -- Aliases
    int_bx_buffer_ptr := to_integer(unsigned(bx_buffer_ptr));
    --
    -- This selects the first BX_ID detected after data goes into one of the
    -- PHI SL's fifos. It will choose the lower (older) one, which will be used 
    -- as central value for the BX acceptance window. 
    -- This window will be used by memory components to retrieve from its 
    -- associated fifos (or not) the segments that were currently available 
    -- on them.
    --
    if fifo_empty_sl(1) = '0' and fifo_empty_sl(0) = '0' then
      --
      -- It's necessary to consider the case where the lower "bx_id" segment,
      -- usually the oldest one, is in fact the newer one because the BX 3563
      -- frontier has been overtaken. In those cases, higher "bx_id" segment
      -- is actually the older.
      --
      if segment_sl(0).bx_id <= segment_sl(1).bx_id 
         or
           (BX3563 - unsigned(segment_sl(0).bx_id) 
                   + unsigned(segment_sl(1).bx_id) < to_unsigned(15, 12)
           )
      then
        bx_window_center := signed('0' & segment_sl(0).bx_id);
      else
        bx_window_center := signed('0' & segment_sl(1).bx_id);
      end if;

    elsif fifo_empty_sl(0) = '0' then
      bx_window_center := signed('0' & segment_sl(0).bx_id);
    else
      --  
      -- When no FIFO's have data this value will be the default one. It could
      -- be considered wrong in the case SL3 fifo wouldn't have any data, but
      -- FSM logic keeps its STDBY state until at least one fifo have data, so
      -- this default value won't affect the overall logic even in the wrong
      -- case.
      --
      bx_window_center := signed('0' & segment_sl(1).bx_id);
    end if;
    --
    -- BX acceptance window limits. It will have 32 BX's in width. This way
    -- it's guaranteed that all segments from a family are stored together
    -- in a processing cycle.
    --
    if reg.bx_window_center + 15 > MAX_BX_IDX 
    then bx_window_high := MAX_BX_IDX - (reg.bx_window_center + 15) - 1;
    else bx_window_high := reg.bx_window_center + 15;
    end if;
    
    if reg.bx_window_center - 16 < 0
    then bx_window_low := MAX_BX_IDX - (reg.bx_window_center - 16) + 1;
    else bx_window_low := reg.bx_window_center - 16;
    end if;

    -- FSM to control segments' gathering process
    case reg.state is
      when STBY =>
        --
        -- This signal is necessary to reset all internal memories registers
        -- in order to avoid getting HIGH "stopped" signal from any of them
        -- after any other previous processing cycles. Otherwise, memories 
        -- have no time to reset that "stopped" signal when "start_storing"
        -- goes HIGH, delivering a wrong behaviour in this FSM.
        --
        rst_memories         <= '1';
        nextReg.mem_ws_count <= (others => '0');
        nextReg.bx_wait_cnt  <= (others => '0');
        nextReg.seg_item_ptr <= (others => (others => '0'));
        
        if (fifo_empty_sl(1) = '0' or fifo_empty_sl(0) = '0')
        then 
          nextReg.state <= WAIT_BX_WINDOW_OFFSET;
        end if;

      when WAIT_BX_WINDOW_OFFSET =>
        if reg.bx_counter_lsb /= slphiMatchIn.bx_counter(0) then
          nextReg.bx_wait_cnt <= reg.bx_wait_cnt + '1';
        end if;
        --
        -- We wait here a certain amount of BX cycles to ensure that all 
        -- segments from a family have had enough time to be processed and
        -- stored inside the incoming FIFO's.
        --
        if reg.bx_wait_cnt >= 
           slphiMatchIn.control_regs.slphi_match_bx_wait_read_offset
        then
          nextReg.bx_window_center <= bx_window_center;
          nextReg.state            <= LATCH_WINDOW_WS;
        end if; 

      -- Additional wait-state to allow timing adjustment.
      when LATCH_WINDOW_WS =>
        -- Both values will be positive, so it's safe to remove the sign bit
        nextReg.bx_window_low <= 
                std_logic_vector(bx_window_low(11 downto 0));

        nextReg.bx_window_high <= 
                std_logic_vector(bx_window_high(11 downto 0));

        nextReg.start_storing <= '1';
        nextReg.state         <= RETRIEVE_DATA_FROM_FIFO;

      when RETRIEVE_DATA_FROM_FIFO =>
        nextReg.start_storing <= '0';
        --
        -- Wait until both SL's memory components have finished retrieving 
        -- data from FIFOs. 
        -- FSM in memory component MUST ENSURE that it moves to HIGH this
        -- flag ("stopped") after, at least, a maximum amount of BX's after 
        -- arriving this current state. Otherwise this module will keep 
        -- stuck in this state, failing the whole data path.
        --
        if slSegMemOut(0).stopped = '1' and slSegMemOut(1).stopped = '1' then 
          --
          -- The building way for the memory modules logic MUST guarantee that,
          -- for a given processing cycle, they store only a "family of 
          -- segments" (segments that belongs to a single event). After that
          -- only buffers with data will be processed to save processing time.
          --
          nextReg.buffers_to_process <= (not slSegMemOut(0).buffers_empty) or 
                                        (not slSegMemOut(1).buffers_empty);

          nextReg.state <= SELECT_BX_BUFFER;
        end if;

      when SELECT_BX_BUFFER =>
        next_buf <= '1';
        
        if reg.buffers_to_process = ZERO32 then
          nextReg.state <= STBY;
        else
          nextReg.state <= VALIDATE_BX_BUFFER;
        end if;

      when VALIDATE_BX_BUFFER =>
        if valid_buf = '0' then
          nextReg.state <= STBY;
        else
          nextReg.mem_rd <= '1';
          nextReg.state  <= GET_SEG_FROM_MEM;
        end if;
        
      when SELECT_NEXT_ITEMS => 
        ----------------------------------------------
        -- Loop over superlayer 0
        --
        --
        -- It could be possible that, despite of certain BX buffers must be 
        -- processed, only one of the SL's has segments while the other one
        -- is empty. We need to cope with this situation.
        --
        -- Fields "last_items_in_bx_buf" only make sense when associated buffers
        -- are not empty.
        --
        if slSegMemOut(0).buffers_empty(int_bx_buffer_ptr) = '0' and
              reg.seg_item_ptr(0) < 
              slSegMemOut(0).last_items_in_bx_buf(int_bx_buffer_ptr)
        then
          nextReg.seg_item_ptr(0) <= reg.seg_item_ptr(0) + '1';
          nextReg.mem_rd          <= '1';
          nextReg.state           <= GET_SEG_FROM_MEM;
        else 
          nextReg.seg_item_ptr(0) <= (others => '0');

          ----------------------------------------------
          -- Loop over superlayer 1
          --
          if slSegMemOut(1).buffers_empty(int_bx_buffer_ptr) = '0'  and
                reg.seg_item_ptr(1) < 
                slSegMemOut(1).last_items_in_bx_buf(int_bx_buffer_ptr)
          then
            nextReg.seg_item_ptr(1) <= reg.seg_item_ptr(1) + '1';
            nextReg.mem_rd          <= '1';
            nextReg.state           <= GET_SEG_FROM_MEM;
          else
            next_buf                <= '1';
            nextReg.seg_item_ptr(1) <= (others => '0');
            nextReg.state           <= VALIDATE_BX_BUFFER;
          end if;

        end if;
      --
      -- In the new "SUPLAY_SEGMENT_MEMORY" after setting the item to be 
      -- retrieved it's necessary to wait a few clock cycles to get data.
      -- Currently this value is 3 (2 waited by the counter + 1 from state 
      -- change)
      --
      when GET_SEG_FROM_MEM => 
        nextReg.mem_rd       <= '0';
        nextReg.mem_ws_count <= reg.mem_ws_count + 1;
        
        if reg.mem_ws_count >= MAX_MEM_NUM_WS then
          nextReg.mem_ws_count <= (others => '0');
          nextReg.state        <= VALIDATE_SEGMENTS;
        end if;
      --
      -- This state has been inserted because the length of old 
      -- "COMBINE_SEGMENTS" combinational logic is large enough to avoid a 
      -- good clock timing.
      --
      when VALIDATE_SEGMENTS =>
        nextReg.state <= COMBINE_SEGMENTS;
        nextReg.bx_good 
            <= segments_with_good_bx(
                  slSegMemOut(0).segment,  slSegMemOut(1).segment,
                  slSegMemOut(0).seg_valid,slSegMemOut(1).seg_valid);

        nextReg.tan_in_range 
            <= tangents_in_range(
                  slSegMemOut(0).segment,   slSegMemOut(1).segment,
                  slSegMemOut(0).seg_valid, slSegMemOut(1).seg_valid,
                  slphiMatchIn.control_regs.slphi_match_tanphi_x4096_thr);

      when COMBINE_SEGMENTS =>
        --
        -- The new memory schema symplifies its internal structure and allows
        -- matching segments with +/- 1 BX, but under certain situations may
        -- provide pairs of segments with no correspondant BX's.
        -- This first condition (function) avoids to send those type of 
        -- wrong matches.
        --
        if reg.bx_good = '1' then

          -- If "tan(phi)" filtering is disabled, primitive is always written.
          if slphiMatchIn.control_regs.slphi_match_bypass_tanphi_match = '1'
          then
            if slphiMatchIn.primitiveFF_full = '0' then 
              fifo_wr       <= '1';
              send_sl1      <= '1';
              send_sl0      <= '1';
              nextReg.state <= SELECT_NEXT_ITEMS;
            end if;

          else
            --
            -- Angular criteria.
            --
            -- This logic identifies if both segments (from both SL phi) have a
            -- "tan(phi)" within a maximum threshold window. If only one of the
            -- segments is "valid" (not null) the validation function always
            -- returns "in range". This is done to allow segments with a certain
            -- BX that do not have a counterpart in the other SL to trigger 
            -- giving a half-filled primitive.
            --
            if reg.tan_in_range = '1' then
              if slphiMatchIn.primitiveFF_full = '0' then 
                fifo_wr       <= '1';
                send_sl1      <= '1';
                send_sl0      <= '1';
                nextReg.state <= SELECT_NEXT_ITEMS;
              end if;

            --
            -- If there is no angular criteria agreement both segments are sent 
            -- separately.
            --
            elsif slphiMatchIn.control_regs.slphi_match_tanphi_hardfilter = '0'
            then

              if slphiMatchIn.primitiveFF_full = '0' then 
                fifo_wr       <= '1';
                send_sl1      <= '1';
                send_sl0      <= '0';
                nextReg.state <= SEND_SECOND_SEGMENT;
              end if;

            else
              nextReg.state <= SELECT_NEXT_ITEMS;
            end if;
          end if;
          
        else
          nextReg.state <= SELECT_NEXT_ITEMS;
        end if;

      when SEND_SECOND_SEGMENT =>
        send_sl1 <= '0';
        send_sl0 <= '1';

        if slphiMatchIn.primitiveFF_full = '0' then 
          fifo_wr       <= '1';
          nextReg.state <= SELECT_NEXT_ITEMS;
        end if;

    end case;

  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1'
      then reg <= regDefault;
      else reg <= nextReg;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  -- Memories for segments.
  --
  -- i = 0 -> superlayer phi 1 // i = 1 -> superlayer phi 3
  PHI_LAYER_MEM_GEN : for i in 0 to 1 generate
    LAYER_MEMORY_SL1 : SUPLAY_SEGMENT_MEMORY port map(
      clk         => clk,
      rst         => rst or rst_memories,
      slSegMemIn  => (
            segment                 => segment_sl(i),
            fifo_empty              => fifo_empty_sl(i),
            start_storing           => reg.start_storing,
            slphi_match_mem_bx_wait => 
                  slphiMatchIn.control_regs.slphi_match_mem_bx_wait,
            --
            bx_counter              => slphiMatchIn.bx_counter,
            bx_window_low           => reg.bx_window_low,
            bx_window_high          => reg.bx_window_high,
            --                      
            mem_rd                  => reg.mem_rd,
            bx_buffer_ptr           => bx_buffer_ptr,
            seg_item_ptr            => reg.seg_item_ptr(i)
      ),
      slSegMemOut => slSegMemOut(i)
    );
  end generate;

  -- Priority encoder for buffers selection.
  BUFF_PRIOR_ENC : SEQUENTIAL_PRIOR_ENC_32_5 
  generic map (BYPASS_INPUT_REG => true)
  port map (
      clk       => clk,
      rst       => rst or rst_memories,
      load      => '0',       -- Not necessary when BYPASS_INPUT_REG is true
      next_item => next_buf,
      data      => reg.buffers_to_process,
      data_out  => open,
      valid     => valid_buf,
      code      => bx_buffer_ptr
    );
  
end architecture;
