-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Multiplexers components
---- Design unit: 16:1 bits multiplexer
---- File       : muxer16_1.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: Multiplexer 16:1 bit implemented to optimize synthesis 
----              resources and speed.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
use work.muxers_pkg.all;

entity MUXER16_1 is
  port (
    sel   : in  std_logic_vector(3 downto 0);
    d_in  : in  std_logic_vector(15 downto 0);
    d_out : out std_logic
  );
end entity;

architecture Behavioural of MUXER16_1 is
begin
  process(sel, d_in)
  begin
    case sel is
      when "0000" => d_out <= d_in(0);
      when "0001" => d_out <= d_in(1);
      when "0010" => d_out <= d_in(2);
      when "0011" => d_out <= d_in(3);
      when "0100" => d_out <= d_in(4);
      when "0101" => d_out <= d_in(5);
      when "0110" => d_out <= d_in(6);
      when "0111" => d_out <= d_in(7);
      when "1000" => d_out <= d_in(8);
      when "1001" => d_out <= d_in(9);
      when "1010" => d_out <= d_in(10);
      when "1011" => d_out <= d_in(11);
      when "1100" => d_out <= d_in(12);
      when "1101" => d_out <= d_in(13);
      when "1110" => d_out <= d_in(14);
      when "1111" => d_out <= d_in(15);
      when others => d_out <= 'X';
    end case;
  end process;

end architecture;
