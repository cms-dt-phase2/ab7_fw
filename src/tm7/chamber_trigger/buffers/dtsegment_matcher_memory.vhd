-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Buffers
---- File       : dtsegment_matcher_memory.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-03
-------------------------------------------------------------------------------
---- Description: Memory module to support segments management into the 
----              superlayers phi matcher.
----              It can store up to 1024 segments.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
use work.dt_common_pkg.all;
use work.xil_memory_pkg.all;

entity DTSEGMENT_MATCHER_MEMORY is
  port (
    rst       : in  std_logic;
    clk_wr    : in  std_logic;
    ena_wr    : in  std_logic;
    add_wr    : in  std_logic_vector(9 downto 0);
    segmentIn : in  DTSegment_t;
    --
    clk_rd     : in  std_logic;
    ena_rd     : in  std_logic;
    add_rd     : in  std_logic_vector(9 downto 0);
    segmentOut : out DTSegment_t
  );
end entity;

architecture Behavioural of DTSEGMENT_MATCHER_MEMORY is
  constant DWIDTH : integer := 18;

  constant NUMBER_OF_18KB_BLOCKS : positive := 
                          ((DTSEGMENT_SIZE - 1) / DWIDTH) + 1;

  signal dIn  : std_logic_vector(DTSEGMENT_SIZE - 1 downto 0);
  signal dOut : std_logic_vector(DTSEGMENT_SIZE - 1 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  segmentOut <= dtSegment_from_slv(dOut);
  
  ---------------------------------------------------------------
  -- Auxiliary signals
  --
  dIn <= slv_from_dtSegment(segmentIn);
  
  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  MEM_FIFO_GENER : for i in 0 to NUMBER_OF_18KB_BLOCKS - 2 generate  
    -- Fully-used memory blocks
    INTERNAL_MEM : XIL_BRAM_DUALPORT_D1024_W18_TO_10BITS 
    generic map (
      DATA_WIDTH  => DWIDTH,
      ENA_REG_OUT => 0) 
    port map (
      rst      => rst, 
      clk_wr   => clk_wr,
      ena_wr   => ena_wr,
      add_wr   => add_wr,
      data_in  => dIn((DWIDTH * (i+1)) - 1 downto DWIDTH * i),
      clk_rd   => clk_rd,
      ena_rd   => ena_rd,
      add_rd   => add_rd,
      data_out => dOut((DWIDTH * (i+1)) - 1 downto DWIDTH * i)
    );
  end generate;

  INTERNAL_MEM_3 : XIL_BRAM_DUALPORT_D1024_W18_TO_10BITS 
  generic map (
    DATA_WIDTH  => DTSEGMENT_SIZE - DWIDTH*(NUMBER_OF_18KB_BLOCKS-1),
    ENA_REG_OUT => 0) 
  port map (
    rst      => rst, 
    clk_wr   => clk_wr,
    ena_wr   => ena_wr,
    add_wr   => add_wr,
    data_in  => dIn(DTSEGMENT_SIZE - 1 downto DWIDTH*(NUMBER_OF_18KB_BLOCKS-1)),
    clk_rd   => clk_rd,
    ena_rd   => ena_rd,
    add_rd   => add_rd,
    data_out => dOut(DTSEGMENT_SIZE - 1 downto DWIDTH*(NUMBER_OF_18KB_BLOCKS-1))
  );

end architecture;
