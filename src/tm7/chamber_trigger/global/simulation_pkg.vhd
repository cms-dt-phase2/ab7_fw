-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Common components
---- File       : simulation_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-05
-------------------------------------------------------------------------------
---- Description: Package for functions useful for simulation TB's.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use STD.textio.all;
use STD.standard.all;
--
use work.dt_common_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package simulation_pkg is

  function build_segment_text_line(segment : DTSegment_t)
    return line;

  function build_primdata_text_line(primitive : DTPrimitive_t) return line;
    
end package;

-------------------------------------------------------------------------------
---- Body
-------------------------------------------------------------------------------
package body simulation_pkg is

  function build_segment_text_line(segment : DTSegment_t) 
  return line 
  is
    variable row_out : line;
    variable hits    : HitsSegment_t;
    variable main    : DTSegMainParams_t;
  begin
    main := segment.main_params;
    hits := segment.main_params.hits;
    --
    -- Build data line 
    --
    -- Quality and BX
    write(row_out, to_integer(unsigned(main.quality)), 
          std.textio.right, 1);
    write(row_out, string'(", "));
    write(row_out, to_integer(unsigned(main.bx_time)), 
          std.textio.right, 5);
    write(row_out, string'(", "));
    write(row_out, to_integer(unsigned(segment.bx_id)), 
          std.textio.right, 1);
    write(row_out, string'(", "));
    
    -- Wires ID
    for i in 0 to 3 loop
      write(row_out, to_integer(unsigned(hits(i).channel_id)), 
            std.textio.right, 2);
      write(row_out, string'(", "));
    end loop;
    
    -- TDC times
    for i in 0 to 3 loop
      write(row_out, to_integer(unsigned(hits(i).dt_time.tdc_time)), 
            std.textio.right, 5);
      write(row_out, string'(", "));
    end loop;
  
    -- Laterality combination (vector is flipped)
    for i in 0 to 3 loop
      write(row_out, to_bitvector(main.laterality_combination)(i), 
            std.textio.right, 1);
      write(row_out, string'(", "));
    end loop;

    -- Position, angle and chi^2
    write(row_out, 
          real(to_integer(signed(segment.horizontal_position))) / 16.0, 
          std.textio.right, 0, 4);

    write(row_out, string'(", "));
    write(row_out, 
          real(to_integer(signed(segment.phi_tangent_x4096))) / 4096.0, 
          std.textio.right, 0, 6);

    write(row_out, string'(", "));
    write(row_out, real(to_integer(unsigned(segment.chi_square))) / 1024.0, 
          std.textio.right, 0, 3);

--     write(row_out, string'(", "));
--     for i in 0 to 3 loop
--       write(row_out, real(to_integer(signed(segment.delta_hit_x(i)))) / 4.0, 
--             std.textio.right, 0, 3);
--       if i < 3 then
--         write(row_out, string'(", "));
--       end if;
--     end loop;
    
    return row_out;
  end function;

  function build_primdata_text_line(primitive : DTPrimitive_t) 
  return line
  is
    variable row_out : line;
  begin
    write(row_out, to_integer(unsigned(primitive.bx_time_chamber)), 
          std.textio.right, 1);

    write(row_out, string'(", "));
    write(row_out, to_integer(unsigned(primitive.bx_id_chamber)), 
          std.textio.right, 1);

    -- Position, angle and chi^2
    write(row_out, string'(", "));
    write(row_out, 
          real(to_integer(signed(primitive.horizontal_position_chamber))) / 16.0, 
          std.textio.right, 0, 4);

    write(row_out, string'(", "));
    write(row_out, 
          real(to_integer(signed(primitive.phi_tangent_x4096_chamber))) / 4096.0, 
          std.textio.right, 0, 6);

    write(row_out, string'(", "));
    write(row_out, real(to_integer(unsigned(primitive.chi_square_chamber))) / 1024.0, 
          std.textio.right, 0, 3);
    
    return row_out;
  end function;
  
end package body;
