-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Memory components
---- Design unit: Memory package
---- File       : xil_memory_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-09
-------------------------------------------------------------------------------
---- Description: Memory/FIFO related packages based on Xilinx macros
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
--
use work.commonfunc_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package xil_memory_pkg is

  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  -- Dual clock FIFO with FIRST WORD FALL THROUGH
  --
  component XIL_FIFO_BUILTIN_V7 is
    generic (
      -- Sets almost full threshold
      ALMOST_FULL_OFFSET      : bit_vector(15 downto 0):= X"0080";
      -- Sets the almost empty threshold     
      ALMOST_EMPTY_OFFSET     : bit_vector(15 downto 0):= X"0080";
      -- Target BRAM, "18Kb" or "36Kb" 
      FIFO_SIZE               : string  := "18Kb";
      -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
      DATAWIDTH               : integer := 36;  -- DATAWIDTH units -> bits
      DEPTH                   : integer := 512; -- DEPTH units -> num. words
      FIRST_WORD_FALL_THROUGH : boolean := TRUE
    );
    port (
      -- Clock and resets
      rst          : in  std_logic;
      rdClk        : in  std_logic;
      wrClk        : in  std_logic;
      -- Control and data signals
      rdEna        : in  std_logic;
      wrEna        : in  std_logic;
      dataIn       : in  std_logic_vector(DATAWIDTH - 1 downto 0);
      dataOut      : out std_logic_vector(DATAWIDTH - 1 downto 0);
      almost_empty : out std_logic;
      almost_full  : out std_logic;
      empty        : out std_logic;
      full         : out std_logic
    );
  end component;
  
  component XIL_WRAPPER_FIFO_FWFT_1REG is
    generic (
      -- Sets almost full threshold
      ALMOST_FULL_OFFSET      : bit_vector(15 downto 0):= X"0080";
      -- Target BRAM, "18Kb" or "36Kb" 
      FIFO_SIZE               : string  := "18Kb";
      -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
      DATAWIDTH               : integer := 36;  -- DATAWIDTH units -> bits
      DEPTH                   : integer := 512  -- DEPTH units -> num. words
    );
    port (
      -- Clock and resets
      rst         : in  std_logic;
      rdClk       : in  std_logic;
      wrClk       : in  std_logic;
      -- Control and data signals
      rdEna       : in  std_logic;
      wrEna       : in  std_logic;
      dataIn      : in  std_logic_vector(DATAWIDTH - 1 downto 0);
      dataOut     : out std_logic_vector(DATAWIDTH - 1 downto 0);
      almost_full : out std_logic;
      empty       : out std_logic;
      full        : out std_logic
    );
  end component;

  component XIL_DISTRAM_D32_W2XBITS is
    generic (HALF_BITS : integer := 16);
    port (
      clk     : in  std_logic;
      wr_ena  : in  std_logic;
      add_in  : in  std_logic_vector(4 downto 0);
      data_in : in  std_logic_vector((2 * HALF_BITS) - 1 downto 0);
      data_out: out std_logic_vector((2 * HALF_BITS) - 1 downto 0)
    );
  end component;

  component XIL_DISTRAM_D32_WXBITS is
    generic (NUM_BITS : integer := 16);
    port (
      clk     : in  std_logic;
      wr_ena  : in  std_logic;
      add_in  : in  std_logic_vector(4 downto 0);
      data_in : in  std_logic_vector(NUM_BITS - 1 downto 0);
      data_out: out std_logic_vector(NUM_BITS - 1 downto 0)
    );
  end component;

  component XIL_DISTRAM_DUALPORT_D32_WXBITS is
    generic (NUM_BITS : integer := 16);
    port (
      clk     : in  std_logic;
      wr_ena  : in  std_logic;
      add_wr  : in  std_logic_vector(4 downto 0);
      add_rd  : in  std_logic_vector(4 downto 0);
      data_in : in  std_logic_vector(NUM_BITS - 1 downto 0);
      data_out: out std_logic_vector(NUM_BITS - 1 downto 0)
    );
  end component;

  component XIL_BRAM_DUALPORT_D4096_W5_TO_9BITS is
    generic (
      DATA_WIDTH  : integer := 8; -- From 5 to 9
      ENA_REG_OUT : integer := 0  -- Enable => 1 / Disable => 0
    ); 
    port (
      rst     : in  std_logic;
      clk_wr  : in  std_logic;
      ena_wr  : in  std_logic;
      add_wr  : in  std_logic_vector(11 downto 0);
      data_in : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
      --
      clk_rd  : in  std_logic;
      ena_rd  : in  std_logic;
      add_rd  : in  std_logic_vector(11 downto 0);
      data_out: out std_logic_vector(DATA_WIDTH - 1 downto 0)
    );
  end component;

  component XIL_BRAM_DUALPORT_D2048_W10_TO_8BITS is
  generic (
    DATA_WIDTH  : integer := 17; -- From 10 to 18
    ENA_REG_OUT : integer := 0   -- Enable => 1 / Disable => 0
  ); 
  port (
    rst     : in  std_logic;
    clk_wr  : in  std_logic;
    ena_wr  : in  std_logic;
    add_wr  : in  std_logic_vector(10 downto 0);
    data_in : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
    --
    clk_rd  : in  std_logic;
    ena_rd  : in  std_logic;
    add_rd  : in  std_logic_vector(10 downto 0);
    data_out: out std_logic_vector(DATA_WIDTH - 1 downto 0)
  );
  end component;

  component XIL_BRAM_DUALPORT_D512_W72_TO_37BITS is
  generic (
    DATA_WIDTH  : integer := 72; -- From 37 to 72
    ENA_REG_OUT : integer := 0   -- Enable => 1 / Disable => 0
  ); 
  port (
    rst     : in  std_logic;
    clk_wr  : in  std_logic;
    ena_wr  : in  std_logic;
    add_wr  : in  std_logic_vector(8 downto 0);
    data_in : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
    --
    clk_rd  : in  std_logic;
    ena_rd  : in  std_logic;
    add_rd  : in  std_logic_vector(8 downto 0);
    data_out: out std_logic_vector(DATA_WIDTH - 1 downto 0)
  );
  end component;

  component XIL_BRAM_DUALPORT_D1024_W36_TO_19BITS is
  generic (
    DATA_WIDTH  : integer := 36; -- From 19 to 36
    ENA_REG_OUT : integer := 0   -- Enable => 1 / Disable => 0
  ); 
  port (
    rst     : in  std_logic;
    clk_wr  : in  std_logic;
    ena_wr  : in  std_logic;
    add_wr  : in  std_logic_vector(9 downto 0);
    data_in : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
    --
    clk_rd  : in  std_logic;
    ena_rd  : in  std_logic;
    add_rd  : in  std_logic_vector(9 downto 0);
    data_out: out std_logic_vector(DATA_WIDTH - 1 downto 0)
  );
  end component;
  
  component XIL_BRAM_DUALPORT_D1024_W18_TO_10BITS is
  generic (
    DATA_WIDTH  : integer := 18; -- From 10 to 18
    ENA_REG_OUT : integer := 0   -- Enable => 1 / Disable => 0
  ); 
  port (
    rst     : in  std_logic;
    clk_wr  : in  std_logic;
    ena_wr  : in  std_logic;
    add_wr  : in  std_logic_vector(9 downto 0);
    data_in : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
    --
    clk_rd  : in  std_logic;
    ena_rd  : in  std_logic;
    add_rd  : in  std_logic_vector(9 downto 0);
    data_out: out std_logic_vector(DATA_WIDTH - 1 downto 0)
  );
  end component;
  
  component XIL_FIFO_GENERIC_WRAPPER is
  generic (
    -- Options: "VIRTEX5, "VIRTEX6", "7SERIES"
    DEVICE                  : string := "7SERIES";
    -- Sets almost int_full threshold
    ALMOST_FULL_OFFSET      : bit_vector(15 downto 0):= X"0080";
    -- Sets the almost int_empty threshold     
    ALMOST_EMPTY_OFFSET     : bit_vector(15 downto 0):= X"0080";
    -- DATAWIDTH units -> bits
    DATAWIDTH               : integer := 32;
     -- DEPTH units -> num. words (512, 1024, 2048, 4096, 8192)
    DEPTH                   : integer := 512;
    FIRST_WORD_FALL_THROUGH : boolean := TRUE
  );
  port (
    -- Clock and resets
    rst          : in  std_logic;
    rdClk        : in  std_logic;
    wrClk        : in  std_logic;
    -- Control and data input signals
    rdEna        : in  std_logic;
    wrEna        : in  std_logic;
    data_in      : in  std_logic_vector(DATAWIDTH - 1 downto 0);
    -- Output signals.
    data_out     : out std_logic_vector(DATAWIDTH - 1 downto 0);
    rd_count     : out std_logic_vector(getBusLines(DEPTH) - 1 downto 0);
    wr_count     : out std_logic_vector(getBusLines(DEPTH) - 1 downto 0);
    almost_empty : out std_logic;
    almost_full  : out std_logic;
    empty        : out std_logic;
    full         : out std_logic
  );
  end component;

  ---------------------------------------------------------------
  -- Procedures
  ---------------------------------------------------------------
  
  function get_fifo_num_inst  (DEPTH, DATAWIDTH : integer) return integer;
  function get_fifo_dataw_main(DEPTH, DATAWIDTH : integer) return integer;
  function get_fifo_dataw_last(DEPTH, DATAWIDTH : integer) return integer;
  function get_fifo_size_main (DEPTH, DATAWIDTH : integer) return string;
  function get_fifo_size_last (DEPTH, DATAWIDTH : integer) return string;

  procedure get_fifo_params (
    DEPTH      : in  integer;
    DATAWIDTH  : in  integer;
    NUM_INST   : out integer;
    DATAW_MAIN : out integer;
    DATAW_LAST : out integer;
    SIZE_MAIN  : out string;
    SIZE_LAST  : out string
  );

end package;

package body xil_memory_pkg is

  function get_fifo_num_inst(DEPTH, DATAWIDTH : integer) 
  return integer
  is
    variable NUM_INST, DATAW_MAIN, DATAW_LAST : integer;
    variable SIZE_MAIN, SIZE_LAST : string(1 to 4) := "36Kb";
  begin
    get_fifo_params( DEPTH, DATAWIDTH, NUM_INST, DATAW_MAIN, DATAW_LAST,
                     SIZE_MAIN, SIZE_LAST );
    
    assert false report "FIFO Configuration NUM INSTANCES: " &
                         integer'image(NUM_INST)
                 severity note;
    
    return NUM_INST;
  end function;

  function get_fifo_dataw_main(DEPTH, DATAWIDTH : integer) 
  return integer
  is
    variable NUM_INST, DATAW_MAIN, DATAW_LAST : integer;
    variable SIZE_MAIN, SIZE_LAST : string(1 to 4) := "36Kb";
  begin
    get_fifo_params( DEPTH, DATAWIDTH, NUM_INST, DATAW_MAIN, DATAW_LAST,
                     SIZE_MAIN, SIZE_LAST );
    
    assert false report "FIFO Configuration DATAWIDTH MAIN: " &
                         integer'image(DATAW_MAIN)
                 severity note;

    return DATAW_MAIN;
  end function;

  function get_fifo_dataw_last(DEPTH, DATAWIDTH : integer) 
  return integer
  is
    variable NUM_INST, DATAW_MAIN, DATAW_LAST : integer;
    variable SIZE_MAIN, SIZE_LAST : string(1 to 4) := "36Kb";
  begin
    get_fifo_params( DEPTH, DATAWIDTH, NUM_INST, DATAW_MAIN, DATAW_LAST,
                     SIZE_MAIN, SIZE_LAST );
    
    assert false report "FIFO Configuration DATAWIDTH LAST: " &
                         integer'image(DATAW_LAST)
                 severity note;

    return DATAW_LAST;
  end function;

  function get_fifo_size_main(DEPTH, DATAWIDTH : integer) 
  return string
  is
    variable NUM_INST, DATAW_MAIN, DATAW_LAST : integer;
    variable SIZE_MAIN, SIZE_LAST : string(1 to 4) := "36Kb";
  begin
    get_fifo_params( DEPTH, DATAWIDTH, NUM_INST, DATAW_MAIN, DATAW_LAST,
                     SIZE_MAIN, SIZE_LAST );

    assert false report "FIFO Configuration SIZE MAIN: " & SIZE_MAIN
                 severity note;
    
    return SIZE_MAIN;
  end function;

  function get_fifo_size_last(DEPTH, DATAWIDTH : integer) 
  return string
  is
    variable NUM_INST, DATAW_MAIN, DATAW_LAST : integer;
    variable SIZE_MAIN, SIZE_LAST : string(1 to 4) := "36Kb";
  begin
    get_fifo_params( DEPTH, DATAWIDTH, NUM_INST, DATAW_MAIN, DATAW_LAST,
                     SIZE_MAIN, SIZE_LAST );
    
    assert false report "FIFO Configuration SIZE LAST: " & SIZE_LAST
                 severity note;
    
    return SIZE_LAST;
  end function;
  
  -----------------------------------------------------------------
  -- DATA_WIDTH | FIFO_SIZE | FIFO Depth | RDCOUNT/WRCOUNT Width --
  -- ===========|===========|============|=======================--
  --   37-72    |  "36Kb"   |     512    |         9-bit         --
  --   19-36    |  "36Kb"   |    1024    |        10-bit         --
  --   19-36    |  "18Kb"   |     512    |         9-bit         --
  --   10-18    |  "36Kb"   |    2048    |        11-bit         --
  --   10-18    |  "18Kb"   |    1024    |        10-bit         --
  --    5-9     |  "36Kb"   |    4096    |        12-bit         --
  --    5-9     |  "18Kb"   |    2048    |        11-bit         --
  --    1-4     |  "36Kb"   |    8192    |        13-bit         --
  --    1-4     |  "18Kb"   |    4096    |        12-bit         --
  -----------------------------------------------------------------
  procedure get_fifo_params (
    DEPTH      : in  integer;
    DATAWIDTH  : in  integer;
    NUM_INST   : out integer;
    DATAW_MAIN : out integer;
    DATAW_LAST : out integer;
    SIZE_MAIN  : out string;
    SIZE_LAST  : out string
  )
  is
    variable MAX_36, MIN_36, MAX_18, MIN_18 : integer;
    variable int_num_instances : integer;
    variable int_div_module    : integer;
    variable int_dataw_main    : integer;
    variable int_dataw_last    : integer;
    variable delta_min_mod     : integer;
    variable int_size_main     : string(1 to 4) := "36Kb";
    variable int_size_last     : string(1 to 4) := "36Kb";
  begin
    case DEPTH is
      when 512 =>
        MAX_36 := 72; MIN_36 := 37;
        MAX_18 := 36; MIN_18 := 19;
      when 1024 =>
        MAX_36 := 36; MIN_36 := 19;
        MAX_18 := 18; MIN_18 := 10;
      when 2048 =>
        MAX_36 := 18; MIN_36 := 10;
        MAX_18 :=  9; MIN_18 :=  5;
      when 4096 =>
        MAX_36 :=  9; MIN_36 :=  5;
        MAX_18 :=  4; MIN_18 :=  1;
      when 8192 =>
        MAX_36 :=  4; MIN_36 :=  1;
        MAX_18 :=  0; MIN_18 :=  0;
      when others =>
        assert false 
            report "FIFO DEPTH must be: 512, 1024, 2048, 4096 or 8192" 
            severity error;
    end case;

    if DATAWIDTH < MIN_18 then
        assert false 
            report "FIFO DATAWIDTH incompatible with FIFO DEPTH" 
            severity error;
    end if;

    int_dataw_main    := MAX_36;
    int_num_instances := DATAWIDTH  /  MAX_36;
    int_div_module    := DATAWIDTH mod MAX_36;

    if int_div_module < MIN_18 then
      if int_div_module /= 0 then
        delta_min_mod := integer( ceil(real(MIN_18 - int_div_module) / 
                                       real(int_num_instances)) );

        int_dataw_main    := int_dataw_main - delta_min_mod;
        int_num_instances := DATAWIDTH  /  int_dataw_main;
        int_div_module    := DATAWIDTH mod int_dataw_main;
      else
        assert false 
            report "Unable to find good FIFO PARAMETERS combination"
            severity error;
        
      end if;
    end if;

    if int_div_module = 0 then
      int_size_main  := "36Kb";
      int_size_last  := "36Kb";
      int_dataw_last := 0;
    elsif int_div_module >= MIN_36 then
      int_size_main  := "36Kb";
      int_size_last  := "36Kb";
      int_dataw_last := int_div_module;
    elsif (int_div_module >= MIN_18) and (int_div_module < MIN_36) then
      int_size_main  := "36Kb";
      int_size_last  := "18Kb";
      int_dataw_last := int_div_module;
    end if;

    NUM_INST   := int_num_instances + 1;
    DATAW_MAIN := int_dataw_main;
    DATAW_LAST := int_dataw_last;
    SIZE_MAIN  := int_size_main;
    SIZE_LAST  := int_size_last;

  end procedure;

end package body;
