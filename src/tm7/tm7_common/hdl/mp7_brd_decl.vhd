-- Modified for TM7; original in boards/mp7/base_fw/mp7_690es/firmware/hdl

-- mp7_brd_decl
--
-- Defines constants for the whole device
--
-- Dave Newbold, June 2014

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package mp7_brd_decl is

	constant BOARD_REV: std_logic_vector(7 downto 0) := X"D7";
	constant N_REGION: integer := 3;
	constant ALIGN_REGION: integer := 1;
	constant CROSS_REGION: integer := 2;
	constant N_REFCLK: integer := 1;

end mp7_brd_decl;
