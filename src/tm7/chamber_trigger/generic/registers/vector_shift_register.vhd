-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Registers
---- File       : vector_shift_register.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-06
-------------------------------------------------------------------------------
---- Description: Shift register for a std_logic_vector
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
--
use work.registers_pkg.all;

entity VECTOR_SHIFT_REGISTER is
  generic (VECTOR_WIDTH : integer := 16; SHIFT_STEPS : integer := 4);
  port(
    clk        : in  std_logic;
    enable     : in  std_logic;
    vector_in  : in  std_logic_vector(VECTOR_WIDTH - 1 downto 0);
    vector_out : out std_logic_vector(VECTOR_WIDTH - 1 downto 0)
  );
end entity;

architecture RTL of VECTOR_SHIFT_REGISTER is
begin

  SHIF_REG_GEN : for i in 0 to VECTOR_WIDTH - 1 generate
    B_SHIFT : BIT_SHIFT_REGISTER 
      generic map (SHIFT_STEPS => SHIFT_STEPS)
      port map (
        clk     => clk,
        enable  => enable,
        bit_in  => vector_in(i),
        bit_out => vector_out(i)
    );
  end generate;

end architecture;
