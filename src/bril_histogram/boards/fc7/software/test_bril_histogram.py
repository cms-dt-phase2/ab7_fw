import numpy as np
import matplotlib.pyplot as plt
import time
import sys
from math import log2
from math import ceil
import queue
import threading

# fc7 interface
from fc7_interface import *
# pybindings for the bril histo decoder
from bril_histogram_pybindings import BrilHistogramError
from bril_histogram_pybindings import BrilHistogram
from bril_histogram_pybindings import BrilHistogramManual
# fifo readout
from bril_histogram_readout_ipbus import *

def print_histogram(histogram):
		header = "HISTOGRAM PRINTOUT"

		header += "\nHeader:"

		header += "\n\tTCDS data:"
		header += "\n\t\tLHC Fill: " + str(histogram.GetLhcFill())
		header += "\n\t\tCMS Run: " + str(histogram.GetCmsRun())
		header += "\n\t\tLumi Section: " + str(histogram.GetLumiSection())
		header += "\n\t\tLumi Nibble: " + str(histogram.GetLumiNibble())

		header += "\n\tHistogram header:"
		header += "\n\t\tTotal words: " + str(histogram.GetNwordsTotal())
		header += "\n\t\tHeader size: " + str(histogram.GetHeaderSize())
		header += "\n\t\tNumber of counter words: " + str(histogram.GetNcounterWords())
		header += "\n\t\tNumber of mask and error words: " + str(histogram.GetNmaskErrorWords())
		header += "\n\t\tNumber of word per error: " + str(histogram.GetNwordsPerError())
		header += "\n\t\tHistogram id: " + str(histogram.GetHistogramID())
		header += "\n\t\tHistogram type: " + str(histogram.GetHistogramType())
		header += "\n\t\tIncrement width: " + str(histogram.GetIncrementWidth())
		header += "\n\t\tCounter width: " + str(histogram.GetCounterWidth())
		header += "\n\t\tOrbit counter: " + str(histogram.GetOrbitCounter())
		header += "\n\t\tCounter overflow: " + str(histogram.GetCounterOverflow())
		header += "\n\t\tIncrement overflow: " + str(histogram.GetIncrementOverflow())
		header += "\n\t\tNumber of units: " + str(histogram.GetNunits())

		counter_data = "\n\nNon-zero counter bins:"
		counter_data += ("\n\t{0:15s} {1:15s}".format("Bin", "Counts"))
		counters = histogram.GetCounters()
		for bin_id in range(histogram.GetNbins()):
			if counters[bin_id] > 0:
				counter_data += ("\n{0:15d} {1:15d}".format(bin_id, counters[bin_id]))

		mask_errors = "\n\nUnit mask:"
		mask_errors += ("\n\t{0:15s} {1:15s}".format("Unit", "Masked"))
		for unit in histogram.GetMaskedUnits():
			mask_errors += ("\n{0:15d} {1:15d}".format(unit, 1))

		if len(histogram.GetErrors()) > 0:
			mask_errors += "\n\nErrors:"
			mask_errors += ("\n\t{0:15s} {1:15s} {2:30s}".format("Orbit", "BX", "Module list"))
			for error in histogram.GetErrors():
				mask_errors += ("\n{0:15d} {1:15d}".format(error.GetOrbitCounter(), error.GetBxCounter()))
				mask_errors += "\t\t\t"
				for unit in error.GetUnitList():
					mask_errors += str(unit)
					mask_errors += " "

		print(header+mask_errors)

def compare_histograms(expected, real):
	match = True
	if expected.GetNwordsTotal() != real.GetNwordsTotal():
		match = False
		print ("ERROR Total number of words does not match")
	if expected.GetHeaderSize() != real.GetHeaderSize():
		match = False
		print ("ERROR Header size doesnot match")
	if expected.GetNbins() != real.GetNbins():
		match = False
		print ("ERROR nbins doesnot match")
	if expected.GetHistogramID() != real.GetHistogramID():
		match = False
		print ("ERROR histogram id does not match")
	if expected.GetHistogramType() != real.GetHistogramType():
		match = False
		print ("ERROR histogram type does not match")     
	if expected.GetNcounterWords() != real.GetNcounterWords():
		match = False
		print ("ERROR ncounter words does not match")     
	if expected.GetIncrementWidth() != real.GetIncrementWidth():
		match = False
		print ("ERROR increment width does not match")     
	if expected.GetCounterWidth() != real.GetCounterWidth():
		match = False
		print ("ERROR counter width does not match")  
	if expected.GetOrbitCounter() != real.GetOrbitCounter():
		match = False
		print ("ERROR orbit counter does not match")  
	if expected.GetIncrementOverflow() != real.GetIncrementOverflow():
		match = False
		print ("ERROR increment overflow does not match")     
	elif (real.GetIncrementOverflow() == 1):
		print ("NOTE increment overflow has happened - firmware correctly detected it")   
	if expected.GetCounterOverflow() != real.GetCounterOverflow():
		match = False
		print ("ERROR counter overflow does not match")     
	elif (real.GetCounterOverflow() == 1):
		print ("NOTE counter overflow has happened - firmware correctly detected it")
	if expected.GetLhcFill() != real.GetLhcFill():
		match = False
		print ("ERROR lhc fill does not match")     
	if expected.GetCmsRun() != real.GetCmsRun():
		match = False
		print ("ERROR cms run does not match")     
	if expected.GetLumiSection() != real.GetLumiSection():
		match = False
		print ("ERROR lumi section does not match")  
	if expected.GetLumiNibble() != real.GetLumiNibble():
		match = False
		print ("ERROR lumi nibble doesnot match")   
	if expected.GetNmaskErrorWords() != real.GetNmaskErrorWords():
		match = False
		print ("ERROR number of mask error words doesnot match")
	if expected.GetNwordsPerError() != real.GetNwordsPerError():
		match = False
		print ("ERROR number of words per error doesnot match")
	if expected.GetNunits() != real.GetNunits():
		match = False
		print ("ERROR number of units units doesnot match")
	
	# now counters
	mismatch_counter = 0
	expected_counters = expected.GetCounters()
	real_counters = real.GetCounters()
	for bin_id in range(len(expected_counters)):
		if expected_counters[bin_id] != real_counters[bin_id]:
			match = False
			if mismatch_counter < 10:
				print("Bin ", bin_id, " count mismatch, expected: ", expected_counters[bin_id], ", got: ", real_counters[bin_id])
			elif mismatch_counter == 10:
				print("Reached suppression limit, further mismatches are not printed")
			mismatch_counter = mismatch_counter + 1
	if mismatch_counter > 0:
		print("Total bins mismatched: ", mismatch_counter)

	# mask
	expected_masks = expected.GetMaskedUnits()
	real_masks = real.GetMaskedUnits()
	if len(expected_masks) != len(real_masks):
		match = False
		print ("ERROR Number of masked units mismatch")
	else:
		for unit in range(len(expected_masks)):
			if expected_masks[unit] != real_masks[unit]:
				match = False
				print ("ERROR Mask for the unit ", expected_masks[unit], " does not match")
	
	# errors
	expected_errors = expected.GetErrors()
	real_errors = real.GetErrors()
	if len(expected_errors) != len(real_errors):
		match = False
		print ("ERROR Number of detected errors mismatch")
	else:
		for error_id in range(len(expected_errors)):
			if expected_errors[error_id].GetOrbitCounter() != real_errors[error_id].GetOrbitCounter() or expected_errors[error_id].GetBxCounter() != real_errors[error_id].GetBxCounter():
				match = False
				print("ERROR Counter mismatch for the error")
			else:
				if len(expected_errors[error_id].GetUnitList()) != len(real_errors[error_id].GetUnitList()):
					match = False
					print("ERROR Different number of units fired in a given error")
				else:
					for unit in range(len(expected_errors[error_id].GetUnitList())):
						if expected_errors[error_id].GetUnitList()[unit] != real_errors[error_id].GetUnitList()[unit]:
							match = False
							print("ERROR Mismatch of the unit ids fired in a given error")

	# result
	if (not match):
		print("ERORR Two histograms do not match")
		if True:
			print("Expected:")
			print_histogram(expected)
			print("\nReal:")
			print_histogram(real)

	return match
		
## fc7 global

def global_reset():
	command = (0x1 << 0)
	fc7.write("user_ctrl.global_command", command)

## bril histogram specific

def configure_injection(NUMBER_OF_STORED_ORBITS, NUMBER_OF_COLLECTED_ORBITS, NBINS, COUNTER_WIDTH, INCREMENT_WIDTH, NUMBER_OF_UNITS, UNIT_INCREMENT_WIDTH, UNIT_ERROR_COUNTER_WIDTH, STORE_ERRORS):

	# init internal histogram
	bins = np.arange(0,NBINS,1)
	counts = np.zeros(NBINS, dtype="int")
	bram_words = []

	#failing module imitation
	random_error_unit_id = np.random.randint(0,NUMBER_OF_UNITS)
	random_error_orbit = np.random.randint(0,NUMBER_OF_STORED_ORBITS)
	random_error_bx1 = np.random.randint(0,NBINS)
	random_error_bx2 = np.random.randint(0,NBINS)
	counts_masked = np.zeros(NBINS, dtype="int")
	
	# generate data
	increment_ovf = 0
	increment_ovf_masked = 0
	for orbit in range(NUMBER_OF_STORED_ORBITS): 
		#print(orbit)
		for bx in range(NBINS):
			# first assign some bins
			total_increment = 0
			total_increment_masked = 0
			unit_increment = np.zeros(NUMBER_OF_UNITS, dtype="int")
			for unit in range(NUMBER_OF_UNITS):
				unit_increment[unit] = int(np.random.normal(0.8*2**UNIT_INCREMENT_WIDTH, 0.14*2**UNIT_INCREMENT_WIDTH))
				if unit_increment[unit] < 0:
					unit_increment[unit] = 0
				if unit_increment[unit] > 2**UNIT_INCREMENT_WIDTH-1:
					unit_increment[unit] = 2**UNIT_INCREMENT_WIDTH-1
				if not (orbit == random_error_orbit and (bx == random_error_bx1 or bx == random_error_bx2) and unit == random_error_unit_id):
					total_increment += unit_increment[unit]
				if unit != random_error_unit_id: # it will be already masked 
					total_increment_masked += unit_increment[unit]
			if total_increment > 2**INCREMENT_WIDTH - 1:
				increment_ovf = 1
				total_increment = 2**INCREMENT_WIDTH - 1
			if total_increment_masked > 2**INCREMENT_WIDTH - 1:
				increment_ovf_masked = 1
				total_increment_masked = 2**INCREMENT_WIDTH - 1
			# here we store bram and normal count
			counts[bx] = counts[bx] + total_increment
			counts_masked[bx] = counts_masked[bx] + total_increment_masked
			# calculate increment word
			increment_words = np.zeros(8, dtype="int")
			for unit in range(NUMBER_OF_UNITS):
				unit_error = 0
				if orbit == random_error_orbit and (bx == random_error_bx1 or bx == random_error_bx2) and unit == random_error_unit_id:
					unit_error = 1
				increment_words[int(unit/8)] = increment_words[int(unit/8)] | ((0x1 & unit_error) << 3+4*(unit%8))
				increment_words[int(unit/8)] = increment_words[int(unit/8)] | ((0x7 & unit_increment[unit]) << 4*(unit%8))
			# append
			for word in increment_words:
				#if bx < 10:
				#	print(bx, hex(word))
				bram_words.append(int(word))
			
	# fast write
	injection_reset_pointer()
	sleep(0.01)
	fc7.writeBlock("bril_histogram.injector.bram_word", bram_words)
	
	# also tell the injector about the amount of histograms
	fc7.write("bril_histogram.injector.norbits_available", NUMBER_OF_STORED_ORBITS)
	fc7.write("bril_histogram.injector.norbits_tocollect", NUMBER_OF_COLLECTED_ORBITS)
	
	# multiply our hitogram to make sure to get same result with fpga
	multiplier = int(NUMBER_OF_COLLECTED_ORBITS/NUMBER_OF_STORED_ORBITS)
	counter_ovf = 0
	counter_ovf_masked = 0
	for bx in range(NBINS):
		# normal
		if counts[bx]*multiplier > (2**COUNTER_WIDTH-1):
			counts[bx] = 2**COUNTER_WIDTH-1
			counter_ovf = 1
		else:
			counts[bx] *= multiplier
		# masked
		if counts_masked[bx]*multiplier > (2**COUNTER_WIDTH-1):
			counts_masked[bx] = 2**COUNTER_WIDTH-1
			counter_ovf_masked = 1
		else:
			counts_masked[bx] *= multiplier

	# fill errors
	errors = []
	# there is an option in firmware to spare memory and not store the errors (just the masks)
	if STORE_ERRORS:
		for orbit in range(NUMBER_OF_COLLECTED_ORBITS):
			if orbit%NUMBER_OF_STORED_ORBITS == random_error_orbit:
				if random_error_bx1 < random_error_bx2:
					if len(errors) < 2**UNIT_ERROR_COUNTER_WIDTH:
						errors.append(BrilHistogramError(orbit,random_error_bx1,[random_error_unit_id]))
					if len(errors) < 2**UNIT_ERROR_COUNTER_WIDTH:
						errors.append(BrilHistogramError(orbit,random_error_bx2,[random_error_unit_id]))
				elif random_error_bx1 > random_error_bx2:
					if len(errors) < 2**UNIT_ERROR_COUNTER_WIDTH:
						errors.append(BrilHistogramError(orbit,random_error_bx2,[random_error_unit_id]))
					if len(errors) < 2**UNIT_ERROR_COUNTER_WIDTH:
						errors.append(BrilHistogramError(orbit,random_error_bx1,[random_error_unit_id]))
				else:
					if len(errors) < 2**UNIT_ERROR_COUNTER_WIDTH:
						errors.append(BrilHistogramError(orbit,random_error_bx1,[random_error_unit_id]))			

	# create event
	header_size = 9
	nbins = NBINS
	histogram_id = 0
	histogram_type = 0
	if COUNTER_WIDTH <= 16:
		ncounter_words = int(NBINS/2)
	elif COUNTER_WIDTH <= 32:
		ncounter_words = int(NBINS)
	else:
		ncounter_words = 0
	increment_width = INCREMENT_WIDTH
	counter_width = COUNTER_WIDTH
	orbit_counter = NUMBER_OF_COLLECTED_ORBITS
	lhc_fill = 0
	cms_run = 0
	lumi_section = 0
	lumi_nibble = 0
	number_of_units = NUMBER_OF_UNITS
	number_of_words_per_error = 2**ceil(log2(int((4 + 2*16 + NUMBER_OF_UNITS+31)/32)))
	number_of_mask_error_words = (1+len(errors))*number_of_words_per_error # one for mask 
	total_nwords = header_size + ncounter_words + number_of_mask_error_words
	mask = [] #unsigned byte

	# here
	expected_histogram = BrilHistogramManual(total_nwords, header_size, nbins, histogram_id, histogram_type, ncounter_words, increment_width, counter_width, orbit_counter, increment_ovf, counter_ovf, lhc_fill, cms_run, lumi_section, lumi_nibble, number_of_mask_error_words, number_of_units, number_of_words_per_error, counts, mask, errors)
	
	# calculate a bit different masked histgoram parameters
	number_of_mask_error_words_masked = (1)*number_of_words_per_error
	total_nwords_masked = header_size + ncounter_words + number_of_mask_error_words_masked
	mask_masked = [] #unsigned byte
	mask_masked.append(random_error_unit_id)
	errors_masked = []
	expected_histogram_masked = BrilHistogramManual(total_nwords_masked, header_size, nbins, histogram_id, histogram_type, ncounter_words, increment_width, counter_width, orbit_counter, increment_ovf_masked, counter_ovf_masked, lhc_fill, cms_run, lumi_section, lumi_nibble, number_of_mask_error_words_masked, number_of_units, number_of_words_per_error, counts_masked, mask_masked, errors_masked)

	return expected_histogram, expected_histogram_masked

def injection_start():
	command = (0x1 << 0)
	fc7.write("bril_histogram.injector.command", command)

def injection_stop():
	command = (0x1 << 1)
	fc7.write("bril_histogram.injector.command", command)

def injection_reset_pointer():
	command = (0x1 << 8)
	fc7.write("bril_histogram.injector.command", command)

## threading here
class Test:
	def __init__(self, NBINS = 3564, COUNTER_WIDTH = 21, INCREMENT_WIDTH = 8, NUMBER_OF_UNITS = 16, UNIT_INCREMENT_WIDTH = 3, UNIT_ERROR_COUNTER_WIDTH = 3, STORE_ERRORS = 1, NUMBER_OF_STORED_ORBITS = 8, NUMBER_OF_COLLECTED_ORBITS = 4096, NITERATIONS = 10, COUNTER_OVERFLOW_TEST = 0):
		# 
		print("Running with following arguments:")
		print("NBINS: ", NBINS)
		print("COUNTER_WIDTH: ", COUNTER_WIDTH)
		print("INCREMENT_WIDTH: ", INCREMENT_WIDTH)
		print("NUMBER_OF_UNITS: ", NUMBER_OF_UNITS)
		print("UNIT_INCREMENT_WIDTH: ", UNIT_INCREMENT_WIDTH)
		print("UNIT_ERROR_COUNTER_WIDTH: ", UNIT_ERROR_COUNTER_WIDTH)
		print("STORE_ERRORS: ", bool(STORE_ERRORS))
		print("NUMBER_OF_STORED_ORBITS: ", NUMBER_OF_STORED_ORBITS)
		print("NUMBER_OF_COLLECTED_ORBITS: ", NUMBER_OF_COLLECTED_ORBITS)

		# configure
		global_reset()
		sleep(0.01)
		self.expected_histogram, self.expected_histogram_masked = configure_injection(NUMBER_OF_STORED_ORBITS, NUMBER_OF_COLLECTED_ORBITS, NBINS, COUNTER_WIDTH, INCREMENT_WIDTH, NUMBER_OF_UNITS, UNIT_INCREMENT_WIDTH, UNIT_ERROR_COUNTER_WIDTH, STORE_ERRORS)

		# run
		self.success = self.run(NITERATIONS, COUNTER_OVERFLOW_TEST)

	def data_readout_worker(self):
		while self.fRunning:
			# read fifo
			fifo_data = read_fifo(fc7)

			# check for error
			if fifo_data == None:
				print("No data received, timeout")
				self.fDataQueue.put(None)
			else:
				self.fDataQueue.put(fifo_data)


	def run(self, NITERATIONS = 10, COUNTER_OVERFLOW_TEST = 0):
		print("NITERATIONS: ", NITERATIONS)
		print("IS COUNTER OVERFLOW TEST: ", COUNTER_OVERFLOW_TEST)

		# stop junst in case
		injection_stop()
		
		# start data readout thread
		self.fRunning = True
		self.fDataQueue = queue.Queue()
		ReadoutThread = threading.Thread(target=self.data_readout_worker)
		ReadoutThread.daemon = True
		ReadoutThread.start()

		# time
		start_time = time.time()
		
		# start (injection and processing)
		injection_start()

		iteration_counter = 0
		fail_counter = 0
		counter_overflow_counter = 0
		
		while True:
			# also do termination condition
			if self.fRunning and (iteration_counter == NITERATIONS):
				self.fRunning = False
				ReadoutThread.join()
			elif self.fRunning == False: # readout thread already joined, put stop signatura in the data
				self.fDataQueue.put(None)

			# process data
			fifo_data = self.fDataQueue.get()
			if fifo_data is None:
				break
			else:
				# parse header
				fpga_histogram = BrilHistogram(fifo_data)

				# now compare
				if fpga_histogram != None:
					hist_to_compare = self.expected_histogram if (iteration_counter == 0) else self.expected_histogram_masked
					if compare_histograms(hist_to_compare, fpga_histogram):
						#print(fpga_histogram)
						pass
					else:
						fail_counter += 1
					counter_overflow_counter += fpga_histogram.GetCounterOverflow()
				else:
					fail_counter += 1

				if (iteration_counter%(NITERATIONS/20) == 0):
					print("Iteration #", iteration_counter)

					
				# increment nibbles
				if self.expected_histogram.fLumiNibble < 63:
					self.expected_histogram.fLumiNibble += 1
				else:
					self.expected_histogram.fLumiNibble = 0
					self.expected_histogram.fLumiSection += 1
				# also keep masked in sync
				if self.expected_histogram_masked.fLumiNibble < 63:
					self.expected_histogram_masked.fLumiNibble += 1
				else:
					self.expected_histogram_masked.fLumiNibble = 0
					self.expected_histogram_masked.fLumiSection += 1

				# task done
				iteration_counter += 1
				self.fDataQueue.task_done()

		# thatit
		#self.fDataQueue.join()

		# done
		total_time = time.time() - start_time
		print("Total time: ", total_time, " seconds, per iteration: ", total_time/iteration_counter, " seconds")
		print("Number of fails: ", fail_counter)

		# calculate return value
		if COUNTER_OVERFLOW_TEST == 1 and counter_overflow_counter == 0:
			print("ERROR This test is marked as counter overflow test, but no overflow was detected actually")
			return -2
		if fail_counter > 0:
			return -1
		# return otherwise fine
		return 0

# in case it was not imported but ran
if __name__ == "__main__":
	if (len(sys.argv) == 1):
		# call without args
		pass
	elif (len(sys.argv) == (11+1)):
		test = Test(int(sys.argv[1]),int(sys.argv[2]),int(sys.argv[3]),int(sys.argv[4]),int(sys.argv[5]),int(sys.argv[6]),int(sys.argv[7]),int(sys.argv[8]),int(sys.argv[9]),int(sys.argv[10]),int(sys.argv[11]))
		sys.exit(test.success)
	else:
		print("11 arguments are required in the following order: python test_bril_histogram.py NBINS COUNTER_WIDTH INCREMENT_WIDTH NUMBER_OF_UNITS UNIT_INCREMENT_WIDTH UNIT_ERROR_COUNTER_WIDTH STORE_ERRORS NUMBER_OF_STORED_ORBITS NUMBER_OF_COLLECTED_ORBITS NITERATIONS COUNTER_OVERFLOW_TEST")
		sys.exit(-1)

		

