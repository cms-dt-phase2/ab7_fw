-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Arithmetics
---- File       : divisor_b6q18_lut_signed.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
----              Adapted from original b6q15 by A. Navarro
---- Company    : CIEMAT
---- Created    : 2018-08
-------------------------------------------------------------------------------
---- Description: LUT to get the inverse of a number quatized by a 18 bit base
----              Values have an additional bit to consider the sign
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity DIVISOR_B6Q18_LUT_SIGNED is
  port(
    value          : in  signed(6 downto 0);
    value_inverted : out signed(18 downto 0)
  );
end entity;

architecture RTL of DIVISOR_B6Q18_LUT_SIGNED is
  constant LEN_VAL : integer:= value'length;
  constant LEN_INV : integer:= value_inverted'length;

  type data_lut_t is array (0 to 2**LEN_VAL - 1) 
                  of integer range -(2**(LEN_INV - 1)) to 2**(LEN_INV - 1) - 1;

  constant DATA_LUT : data_lut_t := (
     262143,  262143,  131072,   87381,   65536,   52429,   43691,   37449,
      32768,   29127,   26214,   23831,   21845,   20165,   18725,   17476,
      16384,   15420,   14564,   13797,   13107,   12483,   11916,   11398,
      10923,   10486,   10082,    9709,    9362,    9039,    8738,    8456,
       8192,    7944,    7710,    7490,    7282,    7085,    6899,    6722,
       6554,    6394,    6242,    6096,    5958,    5825,    5699,    5578,
       5461,    5350,    5243,    5140,    5041,    4946,    4855,    4766,
       4681,    4599,    4520,    4443,    4369,    4297,    4228,    4161,
      -4096,   -4161,   -4228,   -4297,   -4369,   -4443,   -4520,   -4599,
      -4681,   -4766,   -4855,   -4946,   -5041,   -5140,   -5243,   -5350,
      -5461,   -5578,   -5699,   -5825,   -5958,   -6096,   -6242,   -6394,
      -6554,   -6722,   -6899,   -7085,   -7282,   -7490,   -7710,   -7944,
      -8192,   -8456,   -8738,   -9039,   -9362,   -9709,  -10082,  -10486,
     -10923,  -11398,  -11916,  -12483,  -13107,  -13797,  -14564,  -15420,
     -16384,  -17476,  -18725,  -20165,  -21845,  -23831,  -26214,  -29127,
     -32768,  -37449,  -43691,  -52429,  -65536,  -87381, -131072, -262143
     );
  
  -- Types: auto|block|distributed
  attribute ram_style: string;
  attribute ram_style of DATA_LUT : constant is "distributed";
  
  -- Signal to clarify the code.
  signal aux_u : unsigned(LEN_VAL - 1 downto 0);

begin
  aux_u          <= unsigned(std_logic_vector(value));
  value_inverted <= to_signed(DATA_LUT(to_integer(aux_u)), LEN_INV);
  
end architecture;