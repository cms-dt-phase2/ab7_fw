-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Analyzer 
---- File       : analyzer_pkg.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-03
-------------------------------------------------------------------------------
---- Description: Package for components/defined types of the segments' 
----              analyzer
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.commontypes_pkg.all;
use work.dt_common_pkg.all;
use work.commonfunc_pkg.all;
use work.dt_null_objects_constants_pkg.all;

-------------------------------------------------------------------------------
---- Header
-------------------------------------------------------------------------------
package analyzer_pkg is

  ---------------------------------------------------------------
  -- Data types
  ---------------------------------------------------------------
  
  constant TDCTIME_REDUCED_SIZE : integer := 10;
  -- the coarse and reduced parts of the timestamp overlap in 1 bit
  constant TDCTIME_COARSE_SIZE : integer :=
    TDCTime_t'length - TDCTIME_REDUCED_SIZE + 1;
  
  type reduced_times_arr_t is array(3 downto 0) of
    std_logic_vector(TDCTIME_REDUCED_SIZE-1 downto 0); 
  
  constant FIT_CONSTANTS_SIZE : natural := 183;
  type lat_consts_t is record
    latcomb         : std_logic_vector(3 downto 0);
    constants       : std_logic_vector(FIT_CONSTANTS_SIZE-1 downto 0);
  end record;

  constant LAT_CONSTS_NULL : lat_consts_t := (
    latcomb   => "0000",
    constants => (others => '0')
    );
  
  type coeff_arr_4_t is array(3 downto 0) of signed(3 downto 0);
  type coeff_arr_5_t is array(3 downto 0) of signed(4 downto 0);
  
  type t0_fit_constants_t is record
    coeff : coeff_arr_4_t;
    add   : signed(12 downto 0);
    mult  : unsigned(15 downto 0);
  end record;
  
  type pos_fit_constants_t is record
    coeff : coeff_arr_5_t;
    add   : signed(18 downto 0); 
    mult  : unsigned(15 downto 0);
  end record;
  
  type slope_fit_constants_t is record
    coeff : coeff_arr_4_t;
    add   : signed(23 downto 0); 
    mult  : unsigned(13 downto 0);
  end record;
  
  type slope_xhh_fit_constants_t is record
    coeff : coeff_arr_4_t;
    add   : signed(15 downto 0); 
    mult  : unsigned(12 downto 0);
  end record;
  
  type fit_constants_t is record
    t0        : t0_fit_constants_t ;
    pos       : pos_fit_constants_t ;
    slope_xhh : slope_xhh_fit_constants_t ;
    slope     : slope_fit_constants_t ;
  end record;
  
  type lat_consts_arr_t is array (0 to 5) of lat_consts_t;

  constant LAT_CONSTS_ARR_NULL : lat_consts_arr_t :=
    (others => LAT_CONSTS_NULL );

  type xwire_mm_t is array (3 downto 0) of signed(6 downto 0);

  type segFitIn_t is record
    hits                   : HitsSegment_t;
    xwire_mm               : xwire_mm_t;
    coarse_offset          : std_logic_vector(TDCTIME_COARSE_SIZE-1 downto 0);
    reduced_times          : reduced_times_arr_t;
    coarse_pos             : SignedDistance_t;
    latcomb                : std_logic_vector(3 downto 0);
    constants              : std_logic_vector(FIT_CONSTANTS_SIZE-1 downto 0);
  end record;
  
  constant segFitIn_NULL : segFitIn_t := (
    hits              => DT_HITS_SEG_NULL,
    xwire_mm          => (others => (others => '0')),
    coarse_offset     => (others => '0'),
    reduced_times     => (others => (others => '0')),
    coarse_pos        => (others => '0'),
    latcomb           => (others => '0'),
    constants         => (others => '0')
  );
  

  type segFitOut_t is record
    latcomb_valid : std_logic;
    --
    laterality_combination : std_logic_vector(3 downto 0);
    bx_time : std_logic_vector(DTSegMainParams_t.bx_time'length - 1 downto 0);
    phi_tangent_x4096   : signed(14 downto 0);
    horizontal_position : SignedDistance_t;
    delta_hit_x         : DeltaHit_arr_t;
    chi_square          : unsigned(23 downto 0);
    bx_id               : std_logic_vector(11 downto 0);
    bx_id_prec          : std_logic_vector(2 downto 0);
  end record;

  type segFitOut_arr_t is array (0 to 5) of segFitOut_t;

  type AnLatcombTrunkIn_t is record
    control_regs      : DTControlRegs_t;
    segment_candidate : DTSegCandidate_t;
    segCandFFIn_empty : std_logic;
  end record;

  type AnLatcombTrunkOut_t is record
    segCandFFIn_rd       : std_logic;
    hits                 : HitsSegment_t;
    segFitOut_arr        : segFitOut_arr_t;
  end record;

  type AnalyzerIn_t is record
    control_regs       : DTControlRegs_t;
    segment_candidate  : DTSegCandidate_t;
    segCandFFIn_empty  : std_logic;
    segValidFFOut_full : std_logic;
  end record;

  type AnalyzerOut_t is record
    segment          : DTSegment_t;
    segCandFFIn_rd   : std_logic;
    segValidFFOut_wr : std_logic;
    ffMainParamStat  : DTFifoStat_t;
  end record;
  
  ---------------------------------------------------------------
  -- Functions
  ---------------------------------------------------------------
  function decode_fit_constants( constants_slv : std_logic_vector )
      return fit_constants_t;
      
  ---------------------------------------------------------------
  -- Components
  ---------------------------------------------------------------
  component LAYOUT_VALIDS_TO_LATCOMB_CONSTANTS_ENCODER is
    port (
      cell_horiz_layout : in  CellHorizLayout_arr_t;
      valid             : in  std_logic_vector(3 downto 0);
      lat_consts_arr    : out lat_consts_arr_t
    );
  end component;
  
  component SEGMENT_FITTER is
    port (
      clk          : in  std_logic;
      rst          : in  std_logic;
      enable       : in  std_logic;
      control      : in  DTControlRegs_t;
      segFitIn     : in  segFitIn_t;
      segFitOut    : out segFitOut_t
    );
  end component;

  component ANALYZER_LATCOMB_TRUNK is
    port (
      clk               : in  std_logic;
      rst               : in  std_logic;
      enable            : in  std_logic;
      AnLatcombTrunkIn  : in  AnLatcombTrunkIn_t;
      AnLatcombTrunkOut : out AnLatcombTrunkOut_t
    );
  end component;

  component ANALYZER is
    port (
      clk      : in  std_logic;
      rst      : in  std_logic;
      segAnIn  : in  AnalyzerIn_t;
      segAnOut : out AnalyzerOut_t
    );
  end component;
  
end package;

-------------------------------------------------------------------------------
---- Body
-------------------------------------------------------------------------------
package body analyzer_pkg is

  function decode_fit_constants( constants_slv : std_logic_vector )
    return fit_constants_t
  is
    variable result : fit_constants_t;
    variable index : natural := FIT_CONSTANTS_SIZE;
  begin
    -- T0
    for i in 0 to 3 loop
      result.t0.coeff(i) := signed(constants_slv(index-1 downto  index - result.t0.coeff(i)'length));
      index := index - result.t0.coeff(i)'length;
    end loop;
    result.t0.add := signed(constants_slv(index-1 downto  index - result.t0.add'length));
    index := index - result.t0.add'length;
    result.t0.mult := unsigned(constants_slv(index-1 downto  index - result.t0.mult'length));
    index := index - result.t0.mult'length;
    -- POS
    for i in 0 to 3 loop
      result.pos.coeff(i) := signed(constants_slv(index-1 downto  index - result.pos.coeff(i)'length));
      index := index - result.pos.coeff(i)'length;
    end loop;
    result.pos.add := signed(constants_slv(index-1 downto  index - result.pos.add'length));
    index := index - result.pos.add'length;
    result.pos.mult := unsigned(constants_slv(index-1 downto  index - result.pos.mult'length));
    index := index - result.pos.mult'length;
    -- SLOPE AND SLOPE_XHH
    for i in 0 to 3 loop
      result.slope.coeff(i) := signed(constants_slv(index-1 downto  index - result.slope.coeff(i)'length));
      index := index - result.slope.coeff(i)'length;
    end loop;
    result.slope.add := signed(constants_slv(index-1 downto  index - result.slope.add'length));
    index := index - result.slope.add'length;
    result.slope.mult := unsigned(constants_slv(index-1 downto  index - result.slope.mult'length));
    index := index - result.slope.mult'length;
    
    for i in 0 to 3 loop
      result.slope_xhh.coeff(i) := result.slope.coeff(i);
    end loop;
    result.slope_xhh.add := signed(constants_slv(index-1 downto  index - result.slope_xhh.add'length));
    index := index - result.slope_xhh.add'length;
    result.slope_xhh.mult := unsigned(constants_slv(index-1 downto  index - result.slope_xhh.mult'length));
    index := index - result.slope_xhh.mult'length;
    
    return result;
  end function;

end package body;
