--============================================================================--
--#######################   Module Information   #############################--
--============================================================================--
--
-- Company          : CERN (EP-ESE-BE)
-- Engineer         : Julian Mendez <julian.mendez@cern.ch>
--
-- Project Name     : GBT-SC module (IC and EC fields)
-- Module Name      : IC_rx
--
-- Language         : VHDL
--
-- Target Device    : Device agnostic
-- Tool version     : -
--
-- Version          : 1.0
--
-- Description      : GBTx internal control - RX management
--
-- Versions history : DATE      VERS.   AUTHOR      DESCRIPTION
--
--                    05/04/17  1.0     J. Mendez   First .vhd module definition
--
-- Add. Comments    : -
--                                                                              
--============================================================================--
--############################################################################--
--============================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--============================================================================--
--############################   Entity   ####################################--
--============================================================================--

entity ic_rx is
    generic (
        g_FIFO_DEPTH:  integer := 10
    );
    port (
        -- Clock and reset
        rx_clk_i        : in  std_logic;
        reset_i         : in  std_logic;

        -- Status
        empty_o         : out std_logic;

        gbtx_addr_o     : out std_logic_vector(7 downto 0);
        mem_ptr_o       : out std_logic_vector(15 downto 0);
        nb_of_words_o   : out std_logic_vector(15 downto 0);

        -- Internal FIFO
        rd_i            : in  std_logic;
        data_o          : out std_logic_vector(7 downto 0);

        -- IC lines
        rx_data_i       : in std_logic_vector(1 downto 0)    
   );   
end ic_rx;

--============================================================================--
--#########################   Architecture   #################################-- 
--============================================================================--
architecture behaviour of ic_rx is

    -- Signals
    signal byte_des             : std_logic_vector(7 downto 0);
    signal wr                   : std_logic;
    signal new_word             : std_logic;
    signal to_be_read_s         : std_logic;
    
    -- Components
    component ic_deserializer is
        generic (
            g_WORD_SIZE     : integer := 8;
            g_DELIMITER     : std_logic_vector(7 downto 0) := "01111110"
        );
        port (
            clk_i           : in std_logic;
            reset_i         : in std_logic;

            -- Data
            data_i          : in  std_logic_vector(1 downto 0);
            data_o          : out std_logic_vector((g_WORD_SIZE-1) downto 0);

            -- Status
            write_o         : out std_logic;        
            new_word_o      : out std_logic
        );
    end component ic_deserializer;
    
    component ic_rx_fifo is
        generic (
            g_FIFO_DEPTH    : integer := 10;
            g_WORD_SIZE     : integer := 8
        );
        port (
            clk_i           : in  std_logic;
            reset_i         : in  std_logic;

            -- Data
            data_i          : in  std_logic_vector((g_WORD_SIZE-1) downto 0);
            data_o          : out std_logic_vector((g_WORD_SIZE-1) downto 0);

            -- Control
            new_word_i      : in  std_logic;
            write_i         : in  std_logic;
            read_i          : in  std_logic;
            
            --Status
            gbtx_addr_o     : out std_logic_vector(7 downto 0);
            mem_ptr_o       : out std_logic_vector(15 downto 0);
            nb_of_words_o   : out std_logic_vector(15 downto 0);
            
            to_be_read_o    : out std_logic        
        );
    end component ic_rx_fifo;

begin                 --========####   Architecture Body   ####========-- 

    -- RX Deserializer
    Deserializer_inst: ic_deserializer
        generic map(
            g_WORD_SIZE         => 8
        )
        port map(
            clk_i               => rx_clk_i,
            reset_i             => reset_i,

            -- Data
            data_i              => rx_data_i,
            data_o              => byte_des,

            -- Status        
            write_o             => wr,
            new_word_o          => new_word
        );

    -- RX Fifo
    ic_rx_fifo_inst: ic_rx_fifo
        generic map(
            g_WORD_SIZE         => 8,
            g_FIFO_DEPTH        => g_FIFO_DEPTH
        )
        port map(
            clk_i               => rx_clk_i,
            reset_i             => reset_i,

            -- Data
            data_i              => byte_des,
            data_o              => data_o,

            -- Control
            write_i             => wr,
            read_i              => rd_i,
            new_word_i          => new_word,

            -- Status
            to_be_read_o        => to_be_read_s,
            gbtx_addr_o         => gbtx_addr_o,
            mem_ptr_o           => mem_ptr_o,
            nb_of_words_o       => nb_of_words_o
        );

        empty_o <= not(to_be_read_s);

end behaviour;
--============================================================================--
--############################################################################--
--============================================================================--