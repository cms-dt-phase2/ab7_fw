----------------------------------------------------------------------------------
-- Company: BRIL, CERN
-- Engineer: Mykyta Haranko (mykyta.haranko@cern.ch)
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package bril_histogram_package is

    -- useful type
    type register_block_32bit_type is array (natural range <>) of std_logic_vector(31 downto 0);

    -- calculate the total number of memory blocks, words, words per block
    -- define type first
    type memory_block_parameters_type is
    record
        -- histogram params
        counters_per_word   : natural;
        total_counter_words : natural; -- number of words use for counters
        bram_word_width     : natural; -- width of the bram word
        total_bram_size_sync  : natural; 
        total_bram_size_async : natural;
        header_size         : natural; -- size of the header
        fifo_depth          : natural; -- needed counter depth
        -- total_size
        total_package_size  : natural; -- the total size of the whole package
    end record;
    
    -- erro block parameter
    type error_block_parameters_type is
    record
        ERROR_HEADER_WIDTH             : natural;
        ERROR_WORD_WIDTH               : natural;
        N_32BIT_WORDS_PER_ERROR_WORD   : natural;
        MAX_MASKERROR_WORDS            : natural;
        ERROR_FIFO_DEPTH               : natural;
    end record;
    
    -- define functions
    function divide_roundup (val_i          : in natural;
                             denominator_i  : in natural) return natural;
    function get_memory_depth (total_words_i : in natural) return natural;
    function get_memory_block_parameters (counter_width_i       : in natural;
                                          nbins_i               : in natural;
                                          max_maskerror_words_i : in natural) return memory_block_parameters_type;
    function get_error_block_parameters (   STORE_ERRORS              : in boolean;
                                            ORBIT_BX_COUNTER_WIDTH    : in natural;
                                            NUMBER_OF_UNITS           : in natural;
                                            UNIT_ERROR_COUNTER_WIDTH  : in natural) return error_block_parameters_type;
  
    -- log
    function logNceil ( value : in natural;
                        N     : in natural) return natural;
    -- this two methods append zeros from the right or from the left (besically expand the vectors)
    function append_zeros_right (   vector_i        : in std_logic_vector;
                                    target_size_i   : in natural) return std_logic_vector;
    function append_zeros_left (    vector_i        : in std_logic_vector;
                                    target_size_i   : in natural) return std_logic_vector;
                                                                                   
end bril_histogram_package;

package body bril_histogram_package is

    -- divide round up 
    function divide_roundup (val_i          : in natural;
                             denominator_i  : in natural) return natural is
    begin
        if (val_i rem denominator_i) = 0 then
            return val_i/denominator_i;
        else
            return val_i/denominator_i + 1; 
        end if;
    end function divide_roundup;

    -- now memory params
    function get_memory_block_parameters (counter_width_i       : in natural;
                                          nbins_i               : in natural;
                                          max_maskerror_words_i : in natural) return memory_block_parameters_type is
        variable result : memory_block_parameters_type := (counters_per_word => 0, total_counter_words => 0, bram_word_width => 0, total_bram_size_sync => 0, total_bram_size_async => 0, header_size => 0, fifo_depth => 0, total_package_size => 0);
    begin
        -- always same defined once
        result.header_size := 9;

        -- conditional size
        if counter_width_i <= 16 then
            result.counters_per_word := 2;
            result.total_counter_words := nbins_i/2; -- 2 counters per memory cell
            result.bram_word_width := 32;
        elsif counter_width_i <= 32 then
            result.counters_per_word := 1;
            result.total_counter_words := nbins_i; -- 1 counter per memory cell
            result.bram_word_width := 32;
        else
            -- woops something bad happened, we were not supposed to get here
            report "BRIL histogram error: Counter width is too big. The width of more than 32 bits is not supported."
            severity FAILURE;
        end if;  
        -- calculate essentials
        result.total_bram_size_sync := divide_roundup(result.total_counter_words, result.bram_word_width/32);
        result.total_package_size := result.header_size + result.total_counter_words + max_maskerror_words_i;
        result.total_bram_size_async := divide_roundup(result.total_counter_words + result.header_size + max_maskerror_words_i, result.bram_word_width/32);
        result.fifo_depth := get_memory_depth(result.total_package_size);
        -- finally return
        return result;      
    end function get_memory_block_parameters;

    -- error block parameetrs
    function get_error_block_parameters (   STORE_ERRORS              : in boolean;
                                            ORBIT_BX_COUNTER_WIDTH    : in natural;
                                            NUMBER_OF_UNITS           : in natural;
                                            UNIT_ERROR_COUNTER_WIDTH  : in natural) return error_block_parameters_type is
        variable result : error_block_parameters_type;
    begin
        -- we will calculate a basic word anyways
        result.ERROR_HEADER_WIDTH             := 4 + ORBIT_BX_COUNTER_WIDTH + ORBIT_BX_COUNTER_WIDTH; -- 4 for the sync word, 16,16 for the orbit and bx counter respecitvely
        result.ERROR_WORD_WIDTH               := result.ERROR_HEADER_WIDTH + NUMBER_OF_UNITS;
        result.N_32BIT_WORDS_PER_ERROR_WORD   := 2**logNceil((result.ERROR_WORD_WIDTH+31)/32, 2); -- looks complicated but in fact fifo write_width has to be proportional to 32 (read_width). like this we are calculating the smaller width that can fit
        -- and max error words we will calculate only we are storing error, otherwise just mask
        if STORE_ERRORS then
            result.MAX_MASKERROR_WORDS            := 1 + NUMBER_OF_UNITS*(2**UNIT_ERROR_COUNTER_WIDTH); -- we store masks in the beginning which is basically result.N_32BIT_WORDS_PER_ERROR_WORD, plust we allow (2**UNIT_ERROR_COUNTER_WIDTH) errors per unit
            result.ERROR_FIFO_DEPTH               := 2**logNceil(result.MAX_MASKERROR_WORDS,2);
        else
            result.MAX_MASKERROR_WORDS            := 1; -- we store masks only
            result.ERROR_FIFO_DEPTH               := 1;
        end if;
        --
        return result;
    end function get_error_block_parameters;
    
    -- get depth of the fifo
    function get_memory_depth (total_words_i : in natural) return natural is
        -- power
        variable power_of_2 : natural := 0;
    begin
        -- init
        power_of_2 := 10;
        -- now get it
        for i in 10 to 17 loop
            if total_words_i > 2**i then
                power_of_2 := i+1;
            end if;
        end loop;
        --
        if power_of_2 > 17 then
            -- not good, exceeded the maximal size of fifo
            report "BRIL histogram error: Exceeded the maximal possible depth of fifo. Check your bining please."
            severity FAILURE;
            -- 
            return 0;
        else 
            -- all fine, return the value
            return (2**power_of_2);
        end if;
    end function get_memory_depth;
    
    -- logNceil funciton 
    function logNceil ( value : in natural;
                        N     : in natural) return natural is
        variable temp_val   : natural;
        variable result     : natural;
    begin
        -- init
        temp_val := value;
        result := 0;
        -- calculate
        while temp_val > 1 loop
            result := result + 1;
            temp_val := (temp_val + N-1)/N; -- safe division
        end loop;
        --
        return result; 
    end function;
    
    --
    function append_zeros_right (   vector_i        : in std_logic_vector;
                                    target_size_i   : in natural) return std_logic_vector is
        constant zeros      : std_logic_vector(target_size_i-vector_i'length - 1 downto 0) := (others => '0'); 
    begin
        return vector_i & zeros;
    end function;
    --
    function append_zeros_left (    vector_i        : in std_logic_vector;
                                    target_size_i   : in natural) return std_logic_vector is
        constant zeros      : std_logic_vector(target_size_i-vector_i'length - 1 downto 0) := (others => '0'); 
    begin
        return zeros & vector_i;
    end function;
                                          
end bril_histogram_package;