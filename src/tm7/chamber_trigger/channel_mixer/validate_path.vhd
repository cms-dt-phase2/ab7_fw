-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Multi-channel data mixer
---- File       : validate_path.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-10
-------------------------------------------------------------------------------
---- Description: Encoder that determines if the candidate path has enough hits
----              (at least three, in three different layers) to form a 
----              possible valid muon path.
----
----              It takes the 4 channels from a path candidate and seeks
----              into the channels' "empty" registers looking for enough 
----              amount of non-empty. 
----
----              In order to avoid redundant validations it assumes that each 
----              channel under analysis has one hit at least, and looks for 
----              two other channels also with hits. This simplifies a bit the 
----              encoder and should improve a bit the speed (I hope).
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_constants_pkg.all;
use work.mixer_pkg.all;

entity VALIDATE_PATH is
  port (
    path_candidate : in  sgn_channels_path_t;
    layer          : in  std_logic_vector(1 downto 0);
    ch_empty_lay0  : in  std_logic_vector(127 downto 0);
    ch_empty_lay1  : in  std_logic_vector(127 downto 0);
    ch_empty_lay2  : in  std_logic_vector(127 downto 0);
    ch_empty_lay3  : in  std_logic_vector(127 downto 0);
    valid          : out std_logic
  );
end entity;

architecture Behavioural of VALIDATE_PATH is

  constant LAST_WIRE_VALID : signed(7 downto 0) 
                           := to_signed(NUM_CH_PER_LAYER - 1, 8);

  type channel_array_t is array (3 downto 0) of std_logic_vector(6 downto 0);
  signal channels : channel_array_t;
  
begin
  ---------------------------------------------------------------
  -- Auxiliary signals
  --
  channels(3) <= std_logic_vector(path_candidate(3)(6 downto 0));
  channels(2) <= std_logic_vector(path_candidate(2)(6 downto 0));
  channels(1) <= std_logic_vector(path_candidate(1)(6 downto 0));
  channels(0) <= std_logic_vector(path_candidate(0)(6 downto 0));
  
  process(path_candidate, layer, ch_empty_lay0, ch_empty_lay1, ch_empty_lay2, 
          ch_empty_lay3, channels)
  begin
    --
    -- If any of the "channel idx", belonging to the candidate path, is not 
    -- in the wires' index range, the result is directly considered "invalid".
    --
    if path_candidate(0) < 0 or 
       path_candidate(1) < 0 or  
       path_candidate(2) < 0 or 
       path_candidate(3) < 0 or
       path_candidate(0) > LAST_WIRE_VALID or 
       path_candidate(1) > LAST_WIRE_VALID or  
       path_candidate(2) > LAST_WIRE_VALID or 
       path_candidate(3) > LAST_WIRE_VALID
    then valid <= '0';
    else
      -- This encoder considers any layer excluding the current one.
      case layer is
        when "00" => 
          if (ch_empty_lay1(to_integer(unsigned(channels(1)))) = '0' and 
              ch_empty_lay2(to_integer(unsigned(channels(2)))) = '0')   or
             (ch_empty_lay1(to_integer(unsigned(channels(1)))) = '0' and 
              ch_empty_lay3(to_integer(unsigned(channels(3)))) = '0')   or 
             (ch_empty_lay2(to_integer(unsigned(channels(2)))) = '0' and 
              ch_empty_lay3(to_integer(unsigned(channels(3)))) = '0')
          then valid <= '1';
          else valid <= '0';
          end if;
          
        when "01" =>
          if (ch_empty_lay0(to_integer(unsigned(channels(0)))) = '0' and 
              ch_empty_lay2(to_integer(unsigned(channels(2)))) = '0')   or
             (ch_empty_lay0(to_integer(unsigned(channels(0)))) = '0' and 
              ch_empty_lay3(to_integer(unsigned(channels(3)))) = '0')   or 
             (ch_empty_lay2(to_integer(unsigned(channels(2)))) = '0' and 
              ch_empty_lay3(to_integer(unsigned(channels(3)))) = '0')
          then valid <= '1';
          else valid <= '0';
          end if;
          
        when "10" =>
          if (ch_empty_lay0(to_integer(unsigned(channels(0)))) = '0' and 
              ch_empty_lay1(to_integer(unsigned(channels(1)))) = '0')   or
             (ch_empty_lay0(to_integer(unsigned(channels(0)))) = '0' and 
              ch_empty_lay3(to_integer(unsigned(channels(3)))) = '0')   or 
             (ch_empty_lay1(to_integer(unsigned(channels(1)))) = '0' and 
              ch_empty_lay3(to_integer(unsigned(channels(3)))) = '0')
          then valid <= '1';
          else valid <= '0';
          end if;

        when "11" =>
          if (ch_empty_lay0(to_integer(unsigned(channels(0)))) = '0' and 
              ch_empty_lay1(to_integer(unsigned(channels(1)))) = '0')   or
             (ch_empty_lay0(to_integer(unsigned(channels(0)))) = '0' and 
              ch_empty_lay2(to_integer(unsigned(channels(2)))) = '0')   or 
             (ch_empty_lay1(to_integer(unsigned(channels(1)))) = '0' and 
              ch_empty_lay2(to_integer(unsigned(channels(2)))) = '0')
          then valid <= '1';
          else valid <= '0';
          end if;

        when others => 
          valid <= '0';
      end case;
    end if;
  end process;
  
end architecture;
