-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Chamber trigger module
---- File       : bx_synchronizer.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2019-02
-------------------------------------------------------------------------------
---- Description: 
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.dt_constants_pkg.all;
use work.trigger_pkg.all;
use work.xil_memory_pkg.all;

entity BX_SYNCHRONIZER is
  port ( 
    clk240             : in  std_logic;
    rst                : in  std_logic;
    enable             : in  std_logic;
    bunch_ctr          : in  std_logic_vector(11 downto 0);
    bx_cnt_offset      : in  std_logic_vector(11 downto 0);
    segment            : in  DTSegment_t;
    fifoIn_empty       : in  std_logic;
    fifoIn_rd          : out std_logic;
    dt_trig            : out std_logic;
    readout_data_valid : out std_logic;
    readout_data       : out std_logic_vector(DTSEGMENT_SIZE - 1 downto 0)
);
end entity;

architecture TwoProcess of BX_SYNCHRONIZER is

  type state_type is (READING_OUT, CHECKING_TRIGGER);

  -- Internal signals to be registered.
  type reg_type is record
    state              : state_type;
    stop_readout       : std_logic;
    trigger_in_this_bx : std_logic;
    last_bx            : std_logic_vector(11 downto 0);
  end record;

  constant regDefault : reg_type := (
    state              => READING_OUT,
    stop_readout       => '0',
    trigger_in_this_bx => '0',
    last_bx            => (others => '0')
  );
  signal reg, nextReg : reg_type := regDefault;

  -- Auxiliary
  signal changing_bx   : std_logic;
  signal data_in_valid : std_logic;
  
  -- Components interconnections
  signal ena_wr        : std_logic;
  signal ena_rd        : std_logic;
  signal trigger_valid : std_logic;
  signal mem_data_out  : std_logic_vector(4 downto 0);
  signal effective_bx  : unsigned(11 downto 0);

begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --  
  fifoIn_rd          <= ena_wr;
  dt_trig            <= reg.trigger_in_this_bx;
  readout_data_valid <= ena_wr;
  readout_data       <= 
        slv_from_dtSegment(segment);

  ---------------------------------------------------------------
  -- Read-OUT logic
  --  
  data_in_valid <= not fifoIn_empty;
  ena_wr        <= enable and data_in_valid and not reg.stop_readout;
  --
  trigger_valid <= data_in_valid when reg.stop_readout = '0' else '0';

  ---------------------------------------------------------------
  -- Triggering logic
  --
  -- Determines if the BX is changing refered to the previous one.
  changing_bx <= '0' when reg.last_bx = bunch_ctr else '1';
  ena_rd      <= enable and changing_bx;

  COMB : process(reg, bunch_ctr, bx_cnt_offset, mem_data_out, changing_bx)
  begin
    -- Default values
    nextReg <= reg;

    nextReg.last_bx <= bunch_ctr;

    ------------------------------------------------------
    -- BX Offset management.
    -- Mechanism to correlate BX value calculated by the algorithm with
    -- the current value of the BX counter plus a configurable offset.
    --
    if unsigned(bunch_ctr) >= unsigned(bx_cnt_offset) then 
      effective_bx <= unsigned(bunch_ctr) - unsigned(bx_cnt_offset);
    else 
      effective_bx <= to_unsigned(MAX_BX_IDX + 1, 12) + 
                          (unsigned(bx_cnt_offset) - unsigned(bunch_ctr));
    end if;
    
    case reg.state is
      when READING_OUT =>
        if changing_bx = '1' then
          nextReg.trigger_in_this_bx <= '0';
          --
          nextReg.stop_readout <= '1';
          nextReg.state        <= CHECKING_TRIGGER;
        end if;

      when CHECKING_TRIGGER =>
        nextReg.trigger_in_this_bx <= mem_data_out(0);
        --
        nextReg.stop_readout <= '0';
        nextReg.state        <= READING_OUT;
    
    end case;
  end process;

  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk240, rst)
  begin
    if clk240 = '1' and clk240'event then
      if rst = '1' then 
        reg <= regDefault;
      elsif enable = '1'  then 
        reg <= nextReg;
      end if;
    end if;
  end process;

  ---------------------------------------------------------------
  -- COMPONENTS
  ---------------------------------------------------------------
  --
  -- This memory block stores a '1' into 'bx_counter' position if there
  -- is a valid trigger primitive with a BX equal to that 'bx_counter'.
  -- Otherwise it stores a '0'.
  -- Memory depth is 4096 (one of the allowed values for Xilinx macros) because
  -- we need up to 3564 memory position, one per possible BX.
  TRIGGER_VALID_INDEX_MEM : XIL_BRAM_DUALPORT_D4096_W5_TO_9BITS 
  generic map(
    DATA_WIDTH  => 5,  -- From 5 to 9
    ENA_REG_OUT => 0   -- Enable => 1 / Disable => 0
  ) 
  port map(
    rst      => rst,
    clk_wr   => clk240,
    ena_wr   => ena_wr,
    add_wr   => segment.bx_id,
    data_in  => "0000" & trigger_valid,
    --
    clk_rd   => clk240,
    ena_rd   => ena_rd,
    add_rd   => std_logic_vector(effective_bx),
    data_out => mem_data_out
  );

end architecture;
