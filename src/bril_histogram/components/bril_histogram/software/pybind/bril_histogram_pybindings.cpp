#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "BrilHistogram.h"

PYBIND11_MODULE(bril_histogram_pybindings, m) {
    m.doc() = "pybind11 for the BrilHistogram decoder module";

    pybind11::class_<BrilHistogramError>(m, "BrilHistogramError")
        .def(pybind11::init< uint32_t, uint32_t, std::vector<uint16_t> > ())
        .def("GetOrbitCounter", &BrilHistogramError::GetOrbitCounter)
        .def("GetBxCounter", &BrilHistogramError::GetBxCounter)
	.def("GetUnitList", &BrilHistogramError::GetUnitList);

    pybind11::class_<BrilHistogram>(m, "BrilHistogram")
        .def(pybind11::init<std::vector<uint32_t> > ())
	.def("GetDataRaw", &BrilHistogram::GetDataRaw)
	.def("GetNwordsTotal", &BrilHistogram::GetNwordsTotal)
        .def("GetHeaderSize", &BrilHistogram::GetHeaderSize)
        .def("GetNbins", &BrilHistogram::GetNbins)
	.def("GetNunits", &BrilHistogram::GetNunits)
	.def("GetNcounterWords", &BrilHistogram::GetNcounterWords)
	.def("GetIncrementWidth", &BrilHistogram::GetIncrementWidth)
	.def("GetCounterWidth", &BrilHistogram::GetCounterWidth)
	.def("GetNwordsPerError", &BrilHistogram::GetNwordsPerError)
	.def("GetHistogramID", &BrilHistogram::GetHistogramID)
	.def("GetHistogramType", &BrilHistogram::GetHistogramType)
	.def("GetLhcFill", &BrilHistogram::GetLhcFill)
	.def("GetCmsRun", &BrilHistogram::GetCmsRun)
	.def("GetLumiSection", &BrilHistogram::GetLumiSection)
	.def("GetLumiNibble", &BrilHistogram::GetLumiNibble)
	.def("GetOrbitCounter", &BrilHistogram::GetOrbitCounter)
	.def("GetCounterOverflow", &BrilHistogram::GetCounterOverflow)
	.def("GetIncrementOverflow", &BrilHistogram::GetIncrementOverflow)
	.def("GetNmaskErrorWords", &BrilHistogram::GetNmaskErrorWords)
	.def("GetMaskedUnits", &BrilHistogram::GetMaskedUnits)
	.def("GetErrors", &BrilHistogram::GetErrors)
	.def("GetCounters", &BrilHistogram::GetCounters);

   pybind11::class_<BrilHistogramManual>(m, "BrilHistogramManual")
        .def(pybind11::init<uint16_t, uint8_t, uint16_t, uint16_t, uint16_t, uint16_t, uint8_t, uint8_t, uint32_t, bool, bool, uint32_t, uint32_t, uint32_t, uint32_t, uint16_t, uint16_t, uint8_t, std::vector<uint32_t>, std::vector<uint16_t>, std::vector<BrilHistogramError> > ())
        .def("GetNwordsTotal", &BrilHistogramManual::GetNwordsTotal)
	.def("GetHeaderSize", &BrilHistogramManual::GetHeaderSize)
        .def("GetNbins", &BrilHistogramManual::GetNbins)
	.def("GetNunits", &BrilHistogramManual::GetNunits)
	.def("GetNcounterWords", &BrilHistogramManual::GetNcounterWords)
	.def("GetIncrementWidth", &BrilHistogramManual::GetIncrementWidth)
	.def("GetCounterWidth", &BrilHistogramManual::GetCounterWidth)
	.def("GetNwordsPerError", &BrilHistogramManual::GetNwordsPerError)
	.def("GetHistogramID", &BrilHistogramManual::GetHistogramID)
	.def("GetHistogramType", &BrilHistogramManual::GetHistogramType)
	.def("GetLhcFill", &BrilHistogramManual::GetLhcFill)
	.def("GetCmsRun", &BrilHistogramManual::GetCmsRun)
	.def("GetLumiSection", &BrilHistogramManual::GetLumiSection)
	.def("GetLumiNibble", &BrilHistogramManual::GetLumiNibble)
	.def("GetOrbitCounter", &BrilHistogramManual::GetOrbitCounter)
	.def("GetCounterOverflow", &BrilHistogramManual::GetCounterOverflow)
	.def("GetIncrementOverflow", &BrilHistogramManual::GetIncrementOverflow)
	.def("GetNmaskErrorWords", &BrilHistogramManual::GetNmaskErrorWords)
	.def("GetMaskedUnits", &BrilHistogramManual::GetMaskedUnits)
	.def("GetErrors", &BrilHistogramManual::GetErrors)
	.def("GetCounters", &BrilHistogramManual::GetCounters)
	.def_readwrite("fLhcFill", &BrilHistogramManual::fLhcFill)
	.def_readwrite("fCmsRun", &BrilHistogramManual::fCmsRun)
	.def_readwrite("fLumiSection", &BrilHistogramManual::fLumiSection)
	.def_readwrite("fLumiNibble", &BrilHistogramManual::fLumiNibble);

}
