-------------------------------------------------------------------------------
---- Project    : CMS DT Upgrade Phase 2
---- Subproject : Muon trajectory reconstruction
---- Design unit: Splitters
---- File       : candidate_splitter.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2020-01
-------------------------------------------------------------------------------
---- Description: Splits a 4-hit segment candidate into all possible 3-hit
----              subgroups plus incoming 4-hit segment candidate itself
----              Any other 3-hit segment is sent directly to the output.
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--
use work.dt_common_pkg.all;
use work.dt_null_objects_constants_pkg.all;
use work.splitters_pkg.all;

entity CANDIDATE_SPLITTER is
  port (
    rst             : in  std_logic;
    clk             : in  std_logic;
    candSplitterIn  : in  CandidateSplitterIn_t;
    candSplitterOut : out CandidateSplitterOut_t
  );
end entity;

architecture TwoProcess of CANDIDATE_SPLITTER is

  constant ALL_ONES : std_logic_vector(3 downto 0) := (others => '1');
  
  type state_type is (STBY, SPLIT_GROUP);
  type Segment_SubGroups_t is array (0 to 4) of DTSegCandidate_t;
  
  type reg_type is record
    state        : state_type;
    current_item : unsigned(2 downto 0);
  end record;

  constant regDefault : reg_type := (
    state        => STBY,
    current_item => (others => '0')
  );
  signal reg, nextReg : reg_type := regDefault;
  
  signal subgroups_mux : Segment_SubGroups_t;
  signal valid_hits    : std_logic_vector(3 downto 0);
  signal fifoIn_rd     : std_logic;
  signal fifoOut_wr    : std_logic;
  
begin
  ---------------------------------------------------------------
  -- Outgoing signals
  --
  candSplitterOut.fifoIn_rd         <= fifoIn_rd;
  candSplitterOut.fifoOut_wr        <= fifoOut_wr;
  candSplitterOut.segment_candidate <= 
          subgroups_mux(to_integer(reg.current_item));

  ---------------------------------------------------------------
  -- Subgroups muxer.
  -- 5 items in total. Item 0 is reserved for the original candidate in itself.
  --
  subgroups_mux(0) <= candSplitterIn.segment_candidate;
  
  MUXERS_COMPOSER : for i in 0 to 3 generate
    -- Incoming segment candidate hits "valid" flag aggregation
    valid_hits(i) <= candSplitterIn.segment_candidate.hits(i).dt_time.valid;
  
    -- All subgroups with the same "cell layout".
    subgroups_mux(i + 1).cell_horiz_layout 
          <= candSplitterIn.segment_candidate.cell_horiz_layout;

    HIT_AGREGGATOR : for j in 0 to 3 generate

      NULL_HIT : if j = i generate
        subgroups_mux(i + 1).hits(j) <= DT_HIT_NULL;
      end generate;

      NOT_NULL_HITS : if j /= i generate
        subgroups_mux(i + 1).hits(j) <= 
                candSplitterIn.segment_candidate.hits(j);
      end generate;
      
    end generate;
  end generate;
  
  ---------------------------------------------------------------
  -- Combinatory process
  --
  COMB : process(reg, candSplitterIn, valid_hits)
  begin
    -- Default values
    nextReg    <= reg;
    fifoIn_rd  <= '0';
    fifoOut_wr <= '0';

    case reg.state is
      when STBY =>
        nextReg.current_item <= (others => '0');

        if candSplitterIn.fifoIn_empty = '0' and 
           candSplitterIn.fifoOut_full = '0'
        then 
          fifoOut_wr <= '1';

          if valid_hits = ALL_ONES then
            nextReg.current_item <= reg.current_item + 1;
            nextReg.state        <= SPLIT_GROUP;
          else
            fifoIn_rd <= '1';
          end if;
        end if;

      when SPLIT_GROUP =>
        if candSplitterIn.fifoOut_full = '0' then
          fifoOut_wr           <= '1';
          nextReg.current_item <= reg.current_item + 1;
        
          if reg.current_item >= 4 then
            fifoIn_rd            <= '1';
            nextReg.current_item <= (others => '0');
            nextReg.state        <= STBY;
          end if;
        end if; 
      end case;

  end process;
  
  ---------------------------------------------------------------
  -- Sequential process
  --
  SEC : process(clk, rst)
  begin
    if clk = '1' and clk'event then
      if rst = '1'
      then reg <= regDefault;
      else reg <= nextReg;
      end if;
    end if;
  end process;

end architecture;
    
