-- IEEE VHDL standard library:
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- Library work
library work;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_ab7_lumi.all;
use work.mp7_ttc_decl.all;

-- bril package
use work.bril_histogram_package.all;

-- This is the LHC structure
--
--   (0)(1).(DEB)(0)(1).(DEB)...(0)(1).(DEB)(0)(1).(DEB)(0)(1).(DEB)...(0)(1).(DEB)...(0)(1).(DEB)(0)(1).(DEB)...(0)(1).(DEB)(0)(1)...
--   [    0     ][     1    ]...[   4095   ][   4096   ][   4097   ]...[   8191   ]...[  258048  ][  258049  ]...[  262143  ][  262144  ]...
--   {            Nibble  1                }{            Nibble  2                }...{            Nibble 64                }{            Nibble 65                }...
--   <                                                 Lumi Section  1                                                      ><          Lumi Section  2
--  ^                                      ^                                      ^  ^                                      ^
-- BGo0                                   BGo0                                 BGo0  BGo0                                  BGo0
-- OC0 
-- Start
-- BC0 every 3564 BX
--
-- (1 BX)                           =   25 ns (40    MHz),  counter 12 bits,  0-3563 (N/A @BRIL module)
-- [1 Orbit]          = 3564 BX     = 89.1 us (11.22 kHz),  counter 32 bits   0-...  (N/A @BRIL module)
-- {1 Nibble}         = 4096 orbits =  365 ms (2.74   Hz),  counter  6 bits   1-64   (32b @BRIL module)
-- <1 Lumi Section>   = 64 Nibbles  = 23.3 s  (42    mHz),  counter 12 bits   1-...  (32b @BRIL module)


entity ab7_lumi is
  port(
    -- ipbus
    ipb_clk             : in std_logic;
    ipb_rst             : in std_logic;
    ipb_in              : in ipb_wbus;
    ipb_out             : out ipb_rbus;
    -- ttc
    clk                 : in  std_logic;
    rst                 : in  std_logic;  
    ttc_cmd             : in std_logic_vector(7 downto 0);
    -- 
    tt                  : in std_logic
    );
end entity;

architecture Behavioural of ab7_lumi is

  -- IPBUS BRANCHES
  signal ipb_to_slaves  : ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves: ipb_rbus_array(N_SLAVES - 1 downto 0);
  -- IPBUS REGISTER
  constant N_STAT : integer := 4;
  constant N_CTRL : integer := 4;
  signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);
  signal stat: ipb_reg_v(N_STAT-1 downto 0);
  signal conf: ipb_reg_v(N_CTRL-1 downto 0);

  signal lumi_section           : std_logic_vector(31 downto 0);
  signal lumi_nibble            : std_logic_vector(31 downto 0);
  signal reset_histogram        : std_logic;
  signal oc0_signal             : std_logic;
  signal bc0_signal             : std_logic;
  signal bg0_signal             : std_logic; 
  signal bril_data              : std_logic_vector(31 downto 0);
  signal bril_fifo_rd_en        : std_logic; 
  signal bril_fifo_empty        : std_logic; 
  signal bril_fifo_words_cnt    : std_logic_vector(31 downto 0); 
  signal bril_fifo_data_valid   : std_logic;
 
  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals declaration
  signal conf_manual_reset_n : std_logic := '0';

  signal conf_fill_number, conf_run_number, stat_lumi_nibble,
    stat_lumi_section : std_logic_vector(31 downto 0) := (others => '0');

  -- End of automatically-generated VHDL code for register "breakout" signals declaration
  --------------------------------------------------------------------------------


begin


  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals assignment

  conf_run_number           <= conf(16#00#)(31 downto 0);
  conf_fill_number          <= conf(16#01#)(31 downto 0);
  conf_manual_reset_n       <= conf(16#02#)(0);
  stat(16#00#)(31 downto 0) <= stat_lumi_section;
  stat(16#01#)(31 downto 0) <= stat_lumi_nibble;

  -- End of automatically-generated VHDL code for register "breakout" signals assignment
  --------------------------------------------------------------------------------

  --------------------------------------
  --------------------------------------
  -- IPbus
  --------------------------------------
  --------------------------------------
  
  -- ipbus address decode
  fabric: entity work.ipbus_fabric_sel
    generic map(
      NSLV => N_SLAVES,
      SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      sel => ipbus_sel_ab7_lumi(ipb_in.ipb_addr),
      ipb_to_slaves => ipb_to_slaves,
      ipb_from_slaves => ipb_from_slaves
    );
  
  sync_reg: entity work.ipbus_syncreg_v
  generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
  port map(
    clk => ipb_clk,
    rst => ipb_rst,
    ipb_in => ipb_to_slaves(N_SLV_CSR),
    ipb_out => ipb_from_slaves(N_SLV_CSR),
    slv_clk => clk,
    d => stat,
    q => conf,
    --rstb => stat_stb,
    stb => open
  );

  --------------------------------------
  --------------------------------------
  -- TTC DECODER
  --------------------------------------
  --------------------------------------

  ttc_decoder_inst: entity work.ttc_lumi_decoder
  port map(
    clk             => clk             ,
    rst             => rst             ,
    ttc_cmd         => ttc_cmd         ,
    oc0_signal      => oc0_signal      , 
    bc0_signal      => bc0_signal      ,   
    bg0_signal      => bg0_signal      ,
    lumi_nibble_o   => lumi_nibble     ,  
    lumi_section_o  => lumi_section   
  );
  
  stat_lumi_section <= lumi_section;
  stat_lumi_nibble <= lumi_nibble;

  --------------------------------------
  --------------------------------------
  -- BRIL MODULES
  --------------------------------------
  --------------------------------------

  bril_histogram_wrapper_sync_inst: entity work.bril_histogram_wrapper_sync
	generic map(
     -- identification of the histogram
     HISTOGRAM_TYPE                  => 0,
     HISTOGRAM_ID                    => 1,
     -- memory
     GENERATE_FIFO                   => true,
     -- histogram parameters
     NUMBER_OF_BINS                  => 3564,
     COUNTER_WIDTH                   => 16,
     INCREMENT_WIDTH                 => 1
  )
  port map(
    -- core signals
    -- Active-high reset of the FIFOs, has to be released at least 
    -- 8 clk_i cycles before releasing the reset_i
    memory_reset_i                  => rst,
    -- Active-high reset of the histogram module. Kept high until data collection 
    -- begins. The following bunch crossing after releasing reset_i is BX0
    reset_i                         => (not conf_manual_reset_n),
    -- Free-running clock, maximum frequency is 320MHz. All the input signals 
    -- (apart from FIFO control) are synchronised to this clock
    clk_i                           => clk,


    -- tcds signals
    -- Also known as new_nibble - start the new iteration of the histogram, 
    -- normally sent on BX3563 together with new_iteration_i
    -- (next clock cycle is orbit 0, bx0)
    new_histogram_i                 => bg0_signal,
    -- Also known as bx0 - the next clock cycle is bx0 (typically sent at BX3563)
    new_iteration_i                 => bc0_signal,


    -- tcds data -- stored by the module when new_histogram_i is received
    lhc_fill_i                      => conf_fill_number,
    cms_run_i                       => conf_run_number,
    lumi_section_i                  => lumi_section,
    lumi_nibble_i                   => lumi_nibble,


    -- counter signals
    increment_i(0)                  => tt, --trigger       


    -- fifo interface (optional)
    fifo_rd_clk_i                   => ipb_clk,   
    fifo_rd_en_i                    => bril_fifo_rd_en,    
    fifo_empty_o                    => bril_fifo_empty,    
    fifo_words_cnt_o                => bril_fifo_words_cnt,
    --
    data_o                          => bril_data,
    data_valid_o                    => bril_fifo_data_valid
  );

  readout_regs_inst: entity work.bril_histogram_readout_ipbus
  port map(
    clk                   => ipb_clk, -- ipbus clock                            
    reset                 => ipb_rst, -- reset in the ipbus clock domain                 
    ipbus_in              => ipb_to_slaves(N_SLV_BRIL),  -- ipbus input (SLV_BRIL_HISTOGRAM_READOUT is the slave id)
    ipbus_out             => ipb_from_slaves(N_SLV_BRIL),  -- ipbus output (SLV_BRIL_HISTOGRAM_READOUT is the slave id)
    -- fifo status
    fifo_empty_i          => bril_fifo_empty, -- connect to the respective bril histogram port
    fifo_data_valid_i     => bril_fifo_data_valid, -- connect to the respective bril histogram port
    fifo_words_cnt_i      => bril_fifo_words_cnt, -- connect to the respective bril histogram port
    -- fifo read interface
    fifo_rd_en_o          => bril_fifo_rd_en, -- connect to the respective bril histogram port
    fifo_data_i           => bril_data -- connect to the respective bril histogram port
  );
  
  
end architecture;
