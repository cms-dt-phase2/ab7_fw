-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Arithmetics
---- File       : divisor_b6q20_lut.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
----              Adapted from original b6q15 by A. Navarro
---- Company    : CIEMAT
---- Created    : 2018-06
-------------------------------------------------------------------------------
---- Description: LUT to get the inverse of a number quatized by a 15 bit base
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--

entity DIVISOR_B6Q20_LUT is
  port(
    value          : in  std_logic_vector(5 downto 0);
    value_inverted : out std_logic_vector(19 downto 0)
  );
end entity;

architecture RTL of DIVISOR_B6Q20_LUT is
  constant LEN_VAL  : integer:= value'length;
  constant LEN_INV : integer:= value_inverted'length;

  type data_lut_t is array (0 to 2**LEN_VAL - 1) 
                  of integer range 0 to 2**LEN_INV - 1;

  constant DATA_LUT : data_lut_t := (
     1048575, 1048575,  524288,  349525,  262144,  209715,  174763,  149797,
      131072,  116508,  104858,   95325,   87381,   80660,   74898,   69905,
       65536,   61681,   58254,   55188,   52429,   49932,   47663,   45590,
       43691,   41943,   40330,   38836,   37449,   36158,   34953,   33825,
       32768,   31775,   30840,   29959,   29127,   28340,   27594,   26887,
       26214,   25575,   24966,   24385,   23831,   23302,   22795,   22310,
       21845,   21400,   20972,   20560,   20165,   19784,   19418,   19065,
       18725,   18396,   18079,   17772,   17476,   17190,   16913,   16644
  );

begin
  value_inverted <= 
      std_logic_vector(
          to_unsigned(DATA_LUT(to_integer(unsigned(value))), LEN_INV)
      );
end architecture;