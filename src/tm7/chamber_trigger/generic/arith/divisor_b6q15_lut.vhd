-------------------------------------------------------------------------------
---- Project    : VHDL Library
---- Subproject : Common packages
---- Design unit: Arithmetics
---- File       : divisor_b6q15_lut.vhd
----
---- Author     : Jose-Manuel Cela Ruiz <josemanuel.cela@ciemat.es>
---- Company    : CIEMAT
---- Created    : 2018-06
-------------------------------------------------------------------------------
---- Description: LUT to get the inverse of a number quatized by a 15 bit base
-------------------------------------------------------------------------------
--
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
--

entity DIVISOR_B6Q15_LUT is
  port(
    value          : in  std_logic_vector(5 downto 0);
    value_inverted : out std_logic_vector(14 downto 0)
  );
end entity;

architecture RTL of DIVISOR_B6Q15_LUT is
  constant LEN_VAL  : integer:= value'length;
  constant LEN_INV : integer:= value_inverted'length;

  type data_lut_t is array (0 to 2**LEN_VAL - 1) 
                  of integer range 0 to 2**LEN_INV - 1;

  constant DATA_LUT : data_lut_t := (
    32767, 32767, 16384, 10923,  8192,  6554,  5461,  4681,  4096,  3641,  
     3277,  2979,  2731,  2521,  2341,  2185,  2048,  1928,  1820,  1725, 
     1638,  1560,  1489,  1425,  1365,  1311,  1260,  1214,  1170,  1130, 
     1092,  1057,  1024,   993,   964,   936,   910,   886,   862,   840, 
      819,   799,   780,   762,   745,   728,   712,   697,   683,   669, 
      655,   643,   630,   618,   607,   596,   585,   575,   565,   555, 
      546,   537,   529,   520
  );

begin
  value_inverted <= 
      std_logic_vector(
          to_unsigned(DATA_LUT(to_integer(unsigned(value))), LEN_INV)
      );
end architecture;